package com.squareup.picasso;

/**
 * Created by Trushit on 13/06/17.
 */

public class PicassoTools {

    public static void clearCache (Picasso p) {
        p.cache.clear();
    }
}
