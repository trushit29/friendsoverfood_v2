package com.friendsoverfood.android.SignIn;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.EditProfileNew.EditProfileNewActivity;
import com.friendsoverfood.android.Fcm.AlarmReceiver;
import com.friendsoverfood.android.Fcm.AlarmService;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.Welcome.WelcomeActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Trushit on 24/02/17.
 */

public class SignInActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "SIGN_IN_ACTIVITY";

    private TextView txtTitle, txtDontHaveAnAccount, txtForgotPassword, txtPrivacyPolicy;
    //    private CheckBox txtPrivacyPolicy;
    private RelativeLayout relativeRoot;
    private LoginButton btnLoginFb;
    private Button btnSignInSignUp, btnSignInSignUpScrollInside, btnSignUpSignInWithEmail;
    private RelativeLayout btnLoginGoogle, relativeSignUpWithEmail;
    private ImageView imgClose;
    private TextInputLayout tilEmail, tilPassword, tilFirstName, tilLastName, tilMobileNumber, tilBirthDay;
    private AppCompatEditText edtEmail, edtPassword, edtFirstName, edtLastName, edtMobileNumber, edtBirthDay;
    private LinearLayout linearName;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    public DatabaseReference mDatabaseMessages;
    private DatabaseReference mDatabase;
    public Target loadtarget;
//    private boolean isToFetchProfileDetails = false;

    private boolean isSignIn = true;

    // Facebook Login Variables
    private CallbackManager callbackManager;

    // Google Login Variables
    private GoogleApiClient mGoogleApiClient;
    private final int RC_SIGN_IN = 9001;

    public DatePickerDialog.OnDateSetListener dateListener;
    public Calendar calendarBirthDay;
    public String dateBirthDat = "";
    public DatePickerDialog dialogDatePickerBirthDay;

    private String strReferCode = "", strMessageId = "", strFirebaseMessageReceived = "", strFriendIdReceived = "";
    public ArrayList<ChatListVO> matchList = new ArrayList<ChatListVO>();
    public AlarmManager alarams;
    public UserProfileAPI profileSocialSignIn = null;
    public boolean isFromReferCode = false, isReferCodeFromSocialSignIn = false;
    public ChatListVO OtherUsers = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(false);
//        setTitleSupportActionBar(getString(R.string.title_welcome));

        /*setDrawerMenu();*/

        /*setDrawerMenu();
        lockDrawerMenu();*/

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_sign_in_activity, getMiddleContent());

        relativeRoot = (RelativeLayout) view.findViewById(R.id.relativeLayoutSignInActivityRoot);
        txtDontHaveAnAccount = (TextView) view.findViewById(R.id.textViewSignInActivityDontHaveAnAccount);
        txtPrivacyPolicy = (TextView) view.findViewById(R.id.textViewSignInActivityPrivacyPolicy);
        txtForgotPassword = (TextView) view.findViewById(R.id.textViewSignInActivityForgotPassword);
        txtTitle = (TextView) view.findViewById(R.id.textViewSignInActivityTitle);
        btnLoginFb = (LoginButton) view.findViewById(R.id.buttonSignInActivityFacebook);
        btnLoginGoogle = (RelativeLayout) view.findViewById(R.id.buttonSignInActivityGoogle);
        relativeSignUpWithEmail = (RelativeLayout) view.findViewById(R.id.relativeLayoutSignInActivitySignUpWithEmail);
        btnSignUpSignInWithEmail = (Button) view.findViewById(R.id.buttonSignInActivitySignUpWithEmail);
        btnSignInSignUp = (Button) view.findViewById(R.id.buttonSignInActivitySignInSignUp);
        btnSignInSignUpScrollInside = (Button) view.findViewById(R.id.buttonSignInActivitySignInSignUpScrollInside);
        imgClose = (ImageView) view.findViewById(R.id.imageViewSignInActivityClose);

        linearName = (LinearLayout) view.findViewById(R.id.linearLayoutSignInActivityName);
        tilFirstName = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityFirstName);
        tilLastName = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityLastName);
        tilMobileNumber = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityMobileNumber);
        tilBirthDay = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityBirthday);
        tilEmail = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityEmail);
        tilPassword = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignInActivityPassword);

        edtEmail = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityEmail);
        edtFirstName = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityFirstName);
        edtLastName = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityLastName);
        edtMobileNumber = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityMobileNumber);
        edtPassword = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityPassword);
        edtBirthDay = (AppCompatEditText) view.findViewById(R.id.editTextSignInActivityBirthday);

        if (getIntent().hasExtra("isSignIn") && getIntent().getBooleanExtra("isSignIn", true)) {
            isSignIn = true;
        } else {
            isSignIn = false;
        }

        strReferCode = getIntent().hasExtra(getString(R.string.api_response_param_key_refercode))
                ? getIntent().getStringExtra(getString(R.string.api_response_param_key_refercode)) : "";
        strMessageId = getIntent().hasExtra(getString(R.string.api_response_param_key_messageid))
                ? getIntent().getStringExtra(getString(R.string.api_response_param_key_messageid)) : "";
        Log.d(TAG, "Refer code for sign up: " + strReferCode + ", Message Id: " + strMessageId);

        // Configure sign-in to request the user's ID, email address, and basic profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().requestProfile()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN)).requestScopes(new Scope(Scopes.PLUS_ME)).requestScopes(new Scope(Scopes.PROFILE)).build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        calendarBirthDay = Calendar.getInstance();

        initFacebookLogin();

//        setAllTypefaceDINMedium(btnRegister);
//        setAllTypefaceDINMedium(btnSignIn);
        initLayout();
        setListener();
        setAllTypefaceMontserratRegular(view);
        tilPassword.setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"));
        edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"));
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
    }

    private void initLayout() {
        tilPassword.setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"));
        edtPassword.setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"));
        edtEmail.setText("");
        edtPassword.setText("");
        edtFirstName.setText("");
        edtLastName.setText("");
        edtPassword.setText("");

        if (tilEmail.isErrorEnabled() && tilEmail.getError() != null) {
            tilEmail.setError(null);
        }

        if (tilPassword.isErrorEnabled() && tilPassword.getError() != null) {
            tilPassword.setError(null);
        }

        if (tilFirstName.isErrorEnabled() && tilFirstName.getError() != null) {
            tilFirstName.setError(null);
        }

        if (tilLastName.isErrorEnabled() && tilLastName.getError() != null) {
            tilLastName.setError(null);
        }

        if (tilMobileNumber.isErrorEnabled() && tilMobileNumber.getError() != null) {
            tilMobileNumber.setError(null);
        }

        if (tilBirthDay.isErrorEnabled() && tilBirthDay.getError() != null) {
            tilBirthDay.setError(null);
        }

        dateListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarBirthDay.set(Calendar.YEAR, year);
                calendarBirthDay.set(Calendar.MONTH, monthOfYear);
                calendarBirthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                edtBirthDay.setText(String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth) + "/" + String.valueOf(year));
                dateBirthDat = edtBirthDay.getText().toString();

            }

        };

        // Add ClickableSpan Privacy Policy & Terms & Conditions here.
        txtPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        txtPrivacyPolicy.setText(Html.fromHtml(getString(R.string.i_agree_to_the) + " " + getString(R.string.terms_and_conditions) + " " + getString(R.string.and)
                + " " + getString(R.string.privacy_policy)));
        // Add spannable
        Spannable spanPrivacyPolicy = (Spannable) txtPrivacyPolicy.getText();
        ClickableSpan clickableSpanPrivacyPolicy = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Log.d(TAG, "Clickable span privacy policy.");

            }
        };

        ClickableSpan clickableSpanTerms = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Log.d(TAG, "Clickable span terms.");

            }
        };

        int startPos1 = getString(R.string.i_agree_to_the).trim().length() + 1;
        int endPos1 = startPos1 + getString(R.string.terms_and_conditions).trim().length();
        spanPrivacyPolicy.setSpan(clickableSpanTerms, startPos1, endPos1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        int startPos2 = endPos1 + 1 + getString(R.string.and).trim().length() + 1;
        int endPos2 = startPos2 + getString(R.string.privacy_policy).trim().length();
        spanPrivacyPolicy.setSpan(clickableSpanPrivacyPolicy, startPos2, endPos2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (isSignIn) {
            btnSignUpSignInWithEmail.setText(getString(R.string.sign_up_with_email));
            edtEmail.requestFocus();
            txtTitle.setText(getString(R.string.sign_in));
            btnSignInSignUp.setText(getString(R.string.sign_in));
            btnSignInSignUpScrollInside.setText(getString(R.string.sign_in));
            txtForgotPassword.setVisibility(View.VISIBLE);
            linearName.setVisibility(View.GONE);
            tilMobileNumber.setVisibility(View.GONE);
            tilBirthDay.setVisibility(View.GONE);
            // Add ClickableSpan SignIn/SignUp textview here.
            txtDontHaveAnAccount.setMovementMethod(LinkMovementMethod.getInstance());
            txtDontHaveAnAccount.setText(Html.fromHtml(getString(R.string.dont_have_an_account) + " " + getString(R.string.sign_up)), TextView.BufferType.SPANNABLE);
            // Add spannable
            Spannable spanSignUp = (Spannable) txtDontHaveAnAccount.getText();
            ClickableSpan clickableSpanSignUp = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    Log.d(TAG, "Clickable span.");
                    isSignIn = false;
                    initLayout();
                }
            };

            int startPos = getString(R.string.dont_have_an_account).trim().length() + 1;
            int endPos = startPos + getString(R.string.sign_up).trim().length();
            spanSignUp.setSpan(clickableSpanSignUp, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            btnSignUpSignInWithEmail.setText(getString(R.string.sign_in_with_email));
            edtFirstName.requestFocus();
            txtTitle.setText(getString(R.string.sign_up));
            btnSignInSignUp.setText(getString(R.string.sign_up));
            btnSignInSignUpScrollInside.setText(getString(R.string.sign_up));
            txtForgotPassword.setVisibility(View.GONE);
            linearName.setVisibility(View.VISIBLE);
            tilMobileNumber.setVisibility(View.VISIBLE);
            tilBirthDay.setVisibility(View.VISIBLE);
            // Add ClickableSpan SignIn/SignUp textview here.
            txtDontHaveAnAccount.setMovementMethod(LinkMovementMethod.getInstance());
            txtDontHaveAnAccount.setText(Html.fromHtml(getString(R.string.already_have_an_account) + " " + getString(R.string.sign_in)), TextView.BufferType.SPANNABLE);
            // Add spannable
            Spannable spanSignUp = (Spannable) txtDontHaveAnAccount.getText();
            ClickableSpan clickableSpanSignUp = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    Log.d(TAG, "Clickable span.");
                    isSignIn = true;
                    initLayout();
                }
            };

            int startPos = getString(R.string.already_have_an_account).trim().length() + 1;
            int endPos = startPos + getString(R.string.sign_in).trim().length();
            spanSignUp.setSpan(clickableSpanSignUp, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        relativeRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = relativeRoot.getRootView().getHeight() - relativeRoot.getHeight();

                if (heightDiff > Constants.convertDpToPixels(200)) { // if more than 200 dp, it's probably a keyboard...
                    Log.d(TAG, "Keyboard up. Height: " + heightDiff + ", In dp: " + Constants.convertPixelsToDp(heightDiff));
                    if (btnSignInSignUp.getVisibility() == View.VISIBLE) {
                        btnSignInSignUp.setVisibility(View.GONE);
                        btnSignInSignUpScrollInside.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (btnSignInSignUpScrollInside.getVisibility() == View.VISIBLE) {
                        btnSignInSignUpScrollInside.setVisibility(View.GONE);
                        btnSignInSignUp.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    private void initFacebookLogin() {
        /*if (txtPrivacyPolicy.isChecked()) {*/
        callbackManager = CallbackManager.Factory.create();
//        btnLoginFb.setReadPermissions("email, public_profile, user_friends", "user_birthday","user_education_history","user_about_me","user_religion_politics","user_work_history","user_relationships");
        btnLoginFb.setReadPermissions("email, public_profile, user_friends", "user_birthday", "user_education_history", "user_about_me",
                "user_work_history", "user_relationships", "user_photos");
        btnLoginFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final LoginResult loginResult1 = loginResult;
                showProgress(getString(R.string.loading));
                Log.i("FACEBOOK_LOGIN_RESPONSE", "Access Token: " + loginResult.getAccessToken().getToken() + ", Session ACCESS_TOKEN: " + AccessToken
                        .getCurrentAccessToken().getToken());

                handleFacebookAccessToken(AccessToken.getCurrentAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.d(TAG, "FB Login Respone: " + response.toString());
                            // Application code
                            final String birthday = !object.isNull("birthday") ? object.getString("birthday") : ""; // 09/29/1992 - MM/dd/yyyy

                            final String lastName = !object.isNull("last_name") ? object.getString("last_name") : "";
                            final String bio = !object.isNull("bio") ? object.getString("bio") : "";
                            /*final String location = !object.isNull("location") && !object.getJSONObject("location").isNull("name")
                                    ? object.getJSONObject("location").getString("name") : "";*/
                            final String about = !object.isNull("about") ? object.getString("about") : "";
                            final String gender = !object.isNull("gender") ? object.getString("gender") : "";
//                            final String work = !object.isNull("work") ? object.getString("work") : "";
                            final String name = !object.isNull("name") ? object.getString("name") : "";
                            final String religion = !object.isNull("religion") ? object.getString("religion") : "";
                            final String relationship_status = !object.isNull("relationship_status") ? object.getString("relationship_status") : "";
                            String pictureFb = !object.isNull("picture") && !object.getJSONObject("picture").isNull("data")
                                    && !object.getJSONObject("picture").getJSONObject("data").isNull("url")
                                    ? object.getJSONObject("picture").getJSONObject("data").getString("url").trim() : "";
                            String id = !object.isNull("id") ? object.getString("id") : "";
//                            final String email = !object.isNull("email") ? object.getString("email") : id;
                            final String email = !object.isNull("email") ? object.getString("email") : id;
                            final String firstName = !object.isNull("first_name") ? object.getString("first_name") : "";
                            if (!id.trim().isEmpty()) {
                                pictureFb = "https://graph.facebook.com/" + id + "/picture?type=large";
                            } else {
                                pictureFb = !object.isNull("picture") && !object.getJSONObject("picture").isNull("data")
                                        && !object.getJSONObject("picture").getJSONObject("data").isNull("url")
                                        ? object.getJSONObject("picture").getJSONObject("data").getString("url").trim() : "";
                            }

                            // Get formatted bitrh date here.
                            /*String dob = "";
                            if (!birthday.trim().isEmpty()) {
                                try {
                                    Date birthdate = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(birthday);
                                   *//* dob = new SimpleDateFormat("dd-MM-yyyy").format(birthdate).trim();*//*
                                    dob = String.valueOf(Constants.convertTimestampTo10DigitsOnly(birthdate.getTime(), true));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }*/

                            String fullName = "";
                            if (!firstName.trim().isEmpty() && !firstName.trim().equalsIgnoreCase("null")) {
                                fullName = firstName.trim();
                            }

                            if (!lastName.trim().isEmpty() && !lastName.trim().equalsIgnoreCase("null")) {
                                fullName = fullName + " " + lastName.trim();
                            }

                            JSONArray work = !object.isNull("work") ? object.getJSONArray("work") : new JSONArray();

                            for (int i = 0; i < work.length(); i++) {

                                JSONObject work1 = work.getJSONObject(i);

                                JSONObject employer = !work1.isNull("employer")
                                        ? work1.getJSONObject("employer") : new JSONObject();
                                String nameEmployer = !employer.isNull("name") ? employer.getString("name") : "";

                                if (i == 0) {

                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMPLOYER, nameEmployer);
                                    SharedPreferenceUtil.save();
                                }

                                JSONObject position = !work1.isNull("position")
                                        ? work1.getJSONObject("position") : new JSONObject();
                                String Positionname = !position.isNull("name") ? position.getString("name") : "";

                                if (i == 0) {
                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, Positionname);
                                    SharedPreferenceUtil.save();
                                }

                                JSONArray education = !object.isNull("education") ? object.getJSONArray("education") : new JSONArray();

                                for (int j = 0; j < education.length(); j++) {
                                    JSONObject educationdetail = education.getJSONObject(j);

                                    String type = !educationdetail.isNull("type") ? educationdetail.getString("type") : "";

                                    JSONObject school = !educationdetail.isNull("school")
                                            ? educationdetail.getJSONObject("school") : new JSONObject();
                                    String nameSchool = !school.isNull("name") ? school.getString("name") : "";
                                    if (type.equalsIgnoreCase("College")) {
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, nameSchool);
                                        SharedPreferenceUtil.save();
                                    } else if (j == education.length()) {
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, nameSchool);
                                        SharedPreferenceUtil.save();
                                    }
                                }
                            }

                            if (!email.trim().isEmpty()) {
                                SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, birthday);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, email);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, gender);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, firstName);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, lastName);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, pictureFb);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "Facebook");
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, loginResult1.getAccessToken().getToken());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, about);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, religion);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, relationship_status);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, id.trim());
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, id.trim());
                                SharedPreferenceUtil.save();
                                stopProgress();

                                onSuccessRegisterOrLogin();
                            } else {
                                stopProgress();

                                if (LoginManager.getInstance() != null) {
                                    LoginManager.getInstance().logOut();
                                }
                                SharedPreferenceUtil.clear();
                                SharedPreferenceUtil.save();
                                showAlertDialogPositiveButtonOnly(getString(R.string.sign_in), getString(R.string.email_permission_is_required_to_sign_in));
                            }
                        } catch (JSONException e) {
                            stopProgress();
                            e.printStackTrace();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));
                            SharedPreferenceUtil.clear();
                            SharedPreferenceUtil.save();

                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        } catch (Exception e) {
                            stopProgress();
                            e.printStackTrace();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));

                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut();
                            }

                        }
                    }
                });
                Bundle parameters = new Bundle();
//                parameters.putString("fields", "bio,about,birthday,email,first_name,gender,id,last_name,name,link,picture");
                parameters.putString("fields", "about,birthday,email,first_name,gender,id,last_name,name,link,age_range,education,religion,work,relationship_status");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(context, "You have cancelled login with facebook", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
               /* Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();*/
                showSnackBarMessageOnly(e.getMessage());
            }
        });
        /*} else {
            showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
        }*/
    }

    private void setListener() {
        btnLoginGoogle.setOnClickListener(this);
        btnSignInSignUp.setOnClickListener(this);
        btnSignInSignUpScrollInside.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        txtForgotPassword.setOnClickListener(this);
        relativeSignUpWithEmail.setOnClickListener(this);
        edtBirthDay.setOnClickListener(this);

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtEmail.getText().toString().trim().length() > 0) {
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                }
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtPassword.getText().toString().trim().length() > 0) {
                    tilPassword.setError(null);
                    tilPassword.setErrorEnabled(false);
                }
            }
        });

        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtFirstName.getText().toString().trim().length() > 0) {
                    tilFirstName.setError(null);
                    tilFirstName.setErrorEnabled(false);
                }
            }
        });

        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtLastName.getText().toString().trim().length() > 0) {
                    tilLastName.setError(null);
                    tilLastName.setErrorEnabled(false);
                }
            }
        });

        edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtMobileNumber.getText().toString().trim().length() > 0) {
                    tilMobileNumber.setError(null);
                    tilMobileNumber.setErrorEnabled(false);
                }
            }
        });

        edtBirthDay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtBirthDay.getText().toString().trim().length() > 0) {
                    tilBirthDay.setError(null);
                    tilBirthDay.setErrorEnabled(false);
                }
            }
        });

        edtFirstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    edtLastName.requestFocus();
                }
                return false;
            }
        });

        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    if (isSignIn) {
                        /*if (txtPrivacyPolicy.isChecked()) {*/
                        if (validateSignIn()) {
                            loginWithCredApi();
                        }
                        /*} else {
                            showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
                        }*/
                    } else {
                        edtMobileNumber.requestFocus();
                    }
                }
                return false;
            }
        });

        edtMobileNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    if (!isSignIn) {
                       /* if (txtPrivacyPolicy.isChecked()) {*/
                        if (validateSignUp()) {
                            sendRequestSignUpAPI();
                        }
                        /*} else {
                            showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
                        }*/
                    } else {
                    }
                }
                return false;
            }
        });
    }

    private boolean validateSignIn() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if (edtEmail.getText().toString().trim().isEmpty()) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.email_is_required));
            return false;
        } else if (!isValidEmail(edtEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.please_enter_valid_email));
            return false;
        } else if (edtPassword.getText().toString().trim().isEmpty()) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.password_is_requierd));
            return false;
        } else if (edtPassword.getText().toString().trim().length() < 8) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.password_must_contains_eight_characters));
            return false;
        } else {
            return true;
        }
    }

    private boolean validateSignUp() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if (edtFirstName.getText().toString().trim().isEmpty()) {
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getString(R.string.first_name_is_required));
            return false;
        } else if (edtLastName.getText().toString().trim().isEmpty()) {
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getString(R.string.last_name_is_required));
            return false;
        } else if (edtEmail.getText().toString().trim().isEmpty()) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.email_is_required));
            return false;
        } else if (!isValidEmail(edtEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.please_enter_valid_email));
            return false;
        } else if (edtPassword.getText().toString().trim().isEmpty()) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.password_is_requierd));
            return false;
        } else if (edtPassword.getText().toString().trim().length() < 8) {
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.password_must_contains_eight_characters));
            return false;
        } else if (edtMobileNumber.getText().toString().trim().isEmpty()) {
            tilMobileNumber.setErrorEnabled(true);
            tilMobileNumber.setError(getString(R.string.mobile_number_is_required));
            return false;
        } else if (edtBirthDay.getText().toString().trim().isEmpty()) {
            tilBirthDay.setErrorEnabled(true);
            tilBirthDay.setError(getString(R.string.birthday_is_required));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "On resume called for alarm receiver to start.");

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.relativeLayoutSignInActivitySignUpWithEmail:
                if (isSignIn) {
                    isSignIn = false;
                    initLayout();
                } else {
                    isSignIn = true;
                    initLayout();
                }
                break;

            case R.id.buttonSignInActivityGoogle:
               /* if (txtPrivacyPolicy.isChecked()) {*/
                signInWithGoogle();
                /*} else {
                    showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
                }*/
                break;

            case R.id.buttonSignInActivitySignInSignUp:
                if (isSignIn) {
                    /*if (txtPrivacyPolicy.isChecked()) {*/
                    if (validateSignIn()) {
                        loginWithCredApi();
                    }
                    /*} else {
                        showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
                    }*/
                } else {
                    /*if (txtPrivacyPolicy.isChecked()) {*/
                    if (validateSignUp()) {
                        sendRequestSignUpAPI();
                    }
                    /*} else {
                        showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
                    }*/
                }
                break;

            case R.id.buttonSignInActivitySignInSignUpScrollInside:
                if (isSignIn) {
                    if (validateSignIn()) {
                        loginWithCredApi();
                    }
                } else {
                    if (validateSignUp()) {
                        sendRequestSignUpAPI();
                    }
                }
                break;

            case R.id.imageViewSignInActivityClose:
                onBackPressed();
                break;

            case R.id.textViewSignInActivityForgotPassword:
                /*intent = new Intent(context, ForgotPasswordActivity.class);
                startActivity(intent);*/
                showDialogForgotPassword();
                break;

            case R.id.editTextSignInActivityBirthday:
                Calendar c = Calendar.getInstance();
//                calendarBirthDay = stringToCalender(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, ""));
                dialogDatePickerBirthDay = new DatePickerDialog(this, dateListener,
                        calendarBirthDay.get(Calendar.YEAR), calendarBirthDay.get(Calendar.MONTH), calendarBirthDay.get(Calendar.DAY_OF_MONTH));
                dialogDatePickerBirthDay.getDatePicker().setMaxDate(c.getTimeInMillis());
                dialogDatePickerBirthDay.show();
                break;

            default:
                break;

        }
    }

    private void signInWithGoogle() {
        if (NetworkUtil.isOnline(context)) {
            showProgress(getString(R.string.loading));
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } else {
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        /*updateUI(false);*/
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        /*updateUI(false);*/
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
//                Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//                SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, person.getBirthday());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, person.getGender());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "Google");
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "");
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT, person.getAboutMe());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELIGION, "");
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP_STATUS, "");
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, person.getId());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, person.getDisplayName());
//                SharedPreferenceUtil.save();
            } else {
                stopProgress();
//                showSnackBarMessageOnly(getString(R.string.sign_in_with_google_canceled_by_user));
                signOut();
                revokeAccess();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            stopProgress();
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d(TAG, "Name: " + acct.getDisplayName() + ", Email: " + acct.getEmail() + ", PhotoURL: " + acct.getPhotoUrl());

            String firstName = acct.getGivenName();
            String lastName = acct.getFamilyName();

//            TODO save to database
//            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, person.getBirthday());
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, person.getGender());
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "Google");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT, person.getAboutMe());
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELIGION, "");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP_STATUS, "");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, person.getId());

            // SharedPreferenceUtil.save();
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, acct.getPhotoUrl() != null ? acct.getPhotoUrl().toString() : "");
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, acct.getFamilyName() != null ? acct.getFamilyName() : "");
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, acct.getGivenName() != null ? acct.getGivenName() : "");
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, acct.getEmail() != null ? acct.getEmail() : "");
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "Google");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, acct.getDisplayName() != null ? acct.getDisplayName() : "");
            SharedPreferenceUtil.save();

            loginWithGoogleApi();
//            if (acct.getDisplayName() != null) {
//                String[] displayName = acct.getDisplayName().split(" ");
//                for (int i = 0; i < displayName.length; i++) {
//                    if (i == 0) {
//                        firstName = displayName[0].trim();
//                    } else {
//                        lastName = lastName + " " + displayName[i].trim();
//                    }
//                }
//            }

            signOut();
            revokeAccess();
            stopProgress();
            /*updateUI(true);*/
        } else {
            // Signed out, show unauthenticated UI.
            /*updateUI(false);*/
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.some_error_occured));
            resetValuesOnLogout();
        }
    }

    public void sendRequestAddReferCode() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_addreference));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_reference), strReferCode.trim());
        params.put(getString(R.string.api_param_key_messageid), strMessageId.trim());

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_ADDREFERENCE, this);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_GOOGLE_SIGN_IN || action == Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN) {
                Log.d("RESPONSE_LOGIN_SOCIAL", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        // path to /data/data/yourapp/app_data/imageDir
                        ContextWrapper cw = new ContextWrapper(getApplicationContext());
                        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                        deleteRecursive(directory);

                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            dataInner = data.getJSONObject(i);
                        }

                        if (dataInner != null) {
                            profileSocialSignIn = new UserProfileAPI(context, dataInner);

                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profileSocialSignIn.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profileSocialSignIn.sessionid.trim());

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profileSocialSignIn.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profileSocialSignIn.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profileSocialSignIn.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profileSocialSignIn.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profileSocialSignIn.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profileSocialSignIn.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profileSocialSignIn.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profileSocialSignIn.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profileSocialSignIn.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profileSocialSignIn.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profileSocialSignIn.isregistered);
                            SharedPreferenceUtil.save();

                            // Save Firebae Message & Friend object here.
//                            firebase_message
                            strFirebaseMessageReceived = !dataInner.isNull(getString(R.string.api_response_param_key_firebase_message))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_firebase_message)).trim() : "";
                            JSONObject friends = !dataInner.isNull(getString(R.string.api_response_param_key_friends))
                                    ? dataInner.getJSONObject(getString(R.string.api_response_param_key_friends)) : new JSONObject();
                            strFriendIdReceived = !friends.isNull(getString(R.string.api_response_param_key_id))
                                    ? friends.getString(getString(R.string.api_response_param_key_id)).trim() : "";

                            Intent intentAlarm = new Intent(context, AlarmReceiver.class);
                            // create the object of @AlarmManager
                            alarams = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            // set the alarm for particular time
                            alarams.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                                    PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

                            // Call friend list api here, if refer id, message id & friend id are fetched.
                            if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()) {
                                if (strFriendIdReceived.trim().isEmpty()) {
                                    Log.d(TAG, "Friend id is found empty. Call add refer code api here.");
                                    isReferCodeFromSocialSignIn = true;
                                    sendRequestAddReferCode();
                                } else {
                                    callFriendsListApiForSocialSignIn();
                                }
                            } else {
                                callFriendsListApiForSocialSignIn();
                            }
                        } else {
                            stopProgress();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_LOGIN_DB) {
                Log.d("RESPONSE_LOGIN_API", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        // path to /data/data/yourapp/app_data/imageDir
                        ContextWrapper cw = new ContextWrapper(getApplicationContext());
                        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                        deleteRecursive(directory);

                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            dataInner = data.getJSONObject(i);
                        }

                        if (dataInner != null) {
                            UserProfileAPI profile = new UserProfileAPI(context, dataInner);

                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, profile.email.trim());

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);

                            if (!SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_ISREGISTERED, false)) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            }

                            SharedPreferenceUtil.save();

                            if (!profile.isregistered) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                                // Split location and save current latitude & longitude of user.
                                String[] location = profile.location.trim().split(",");
                                if (location.length == 2) {
                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim());
                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
                                }

                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                                SharedPreferenceUtil.save();

                                Intent intentAlarm = new Intent(context, AlarmReceiver.class);
                                // create the object of @AlarmManager
                                alarams = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                // set the alarm for particular time
                                alarams.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                                        PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

//                                showToast(getString(R.string.you_are_successfully_logged_in));
                               /* // TODO Forward user to TasteBuds profile screen.
                                intent = new Intent(context, TasteBudsSuggestionsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();*/

                                // Call friend list api here, if refer id, message id & friend id are fetched.
                                if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()) {
                                    if (strFriendIdReceived.trim().isEmpty()) {
                                        Log.d(TAG, "Friend id is found empty. Call add refer code api here.");
                                        sendRequestAddReferCode();
                                    } else {
                                        callFriendsListApi();
                                    }
                                } else {
                                    callFriendsListApi();
                                }
                            } else {
//                                showToast(getString(R.string.you_are_successfully_logged_in));
                                /*// TODO Save Profile of user, as user is registered for the first time.
                                sendRequestSaveSocialProfileAPI();*/
                                //perform sign in to firebase first.
                                mAuth.signInWithEmailAndPassword(profile.email.trim(), getString(R.string.password_default_for_firebase_authentication))
                                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                if (task.isSuccessful()) {
                                                    // Sign in success, update UI with the signed-in user's information
                                                    Log.d(TAG, "signInWithEmail:success");
                                                    FirebaseUser user = mAuth.getCurrentUser();
                                                    // TODO Save Profile of user, as user is registered for the first time.
                                                    sendRequestSaveSocialProfileAPI();
                                                } else {
                                                    // If sign in fails, display a message to the user.
                                                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                                                    // TODO Save Profile of user, as user is registered for the first time.
                                                    sendRequestSaveSocialProfileAPI();
                                                }
                                            }
                                        });
                            }
                        } else {
                            stopProgress();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_USERS_ME) {
                Log.d("RESPONSE_USERS_ME_API", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONObject data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_data)) : null;

                        if (data != null) {
                            /*String id = !data.isNull(getString(R.string.api_response_param_key_id))
                                    ? data.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String firstname = !data.isNull(getString(R.string.api_response_param_key_first_name))
                                    ? data.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                            String lastname = !data.isNull(getString(R.string.api_response_param_key_last_name))
                                    ? data.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                            String sex = !data.isNull(getString(R.string.api_response_param_key_gender))
                                    ? data.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                            String birthdate = !data.isNull(getString(R.string.api_response_param_key_birthdate))
                                    ? data.getString(getString(R.string.api_response_param_key_birthdate)).trim() : "";
                            String profilepic = !data.isNull(getString(R.string.api_response_param_key_profilepic))
                                    ? data.getString(getString(R.string.api_response_param_key_profilepic)).trim() : "";
                            String accounttype = !data.isNull(getString(R.string.api_response_param_key_accounttype))
                                    ? data.getString(getString(R.string.api_response_param_key_accounttype)).trim() : "";
                            String devicetype = !data.isNull(getString(R.string.api_response_param_key_devicetype))
                                    ? data.getString(getString(R.string.api_response_param_key_devicetype)).trim() : "";
                            String devicetoken = !data.isNull(getString(R.string.api_response_param_key_devicetoken))
                                    ? data.getString(getString(R.string.api_response_param_key_devicetoken)).trim() : "";
                            String username = !data.isNull(getString(R.string.api_response_param_key_username))
                                    ? data.getString(getString(R.string.api_response_param_key_username)).trim() : "";
                            JSONArray received = !data.isNull(getString(R.string.api_response_param_key_received))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_received)) : new JSONArray();
                            JSONArray sent = !data.isNull(getString(R.string.api_response_param_key_sent))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_sent)) : new JSONArray();
                            JSONArray matches = !data.isNull(getString(R.string.api_response_param_key_matches))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_matches)) : new JSONArray();
                            JSONArray matchesdetails = !data.isNull(getString(R.string.api_response_param_key_matchesdetails))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_matchesdetails)) : new JSONArray();

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, firstname.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, lastname.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, sex.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, birthdate.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profilepic.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, accounttype.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETYPE, devicetype.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, devicetoken.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, username.trim());
                            SharedPreferenceUtil.save();*/

                            stopProgress();
//                            intent = new Intent(this, SelectFavFood.class);
                           /* supportFinishAfterTransition();*/
                        } else {
                            stopProgress();
                            showSnackBarMessageOnly(errormessage.trim());
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_UPDATE_DEVICE_TOKEN) {
                Log.d("RESPONSE_UPDATE_REFRESH_TOKEN_API", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONObject data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_data)) : null;

                        if (data != null) {
                           /* String id = !data.isNull(getString(R.string.api_response_param_key_id))
                                    ? data.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String firstname = !data.isNull(getString(R.string.api_response_param_key_firstname))
                                    ? data.getString(getString(R.string.api_response_param_key_firstname)).trim() : "";
                            String lastname = !data.isNull(getString(R.string.api_response_param_key_lastname))
                                    ? data.getString(getString(R.string.api_response_param_key_lastname)).trim() : "";
                            String sex = !data.isNull(getString(R.string.api_response_param_key_sex))
                                    ? data.getString(getString(R.string.api_response_param_key_sex)).trim() : "";
                            String birthdate = !data.isNull(getString(R.string.api_response_param_key_birthdate))
                                    ? data.getString(getString(R.string.api_response_param_key_birthdate)).trim() : "";
                            String profilepic = !data.isNull(getString(R.string.api_response_param_key_profilepic))
                                    ? data.getString(getString(R.string.api_response_param_key_profilepic)).trim() : "";
                            String accounttype = !data.isNull(getString(R.string.api_response_param_key_accounttype))
                                    ? data.getString(getString(R.string.api_response_param_key_accounttype)).trim() : "";
                            String devicetype = !data.isNull(getString(R.string.api_response_param_key_devicetype))
                                    ? data.getString(getString(R.string.api_response_param_key_devicetype)).trim() : "";
                            String devicetoken = !data.isNull(getString(R.string.api_response_param_key_devicetoken))
                                    ? data.getString(getString(R.string.api_response_param_key_devicetoken)).trim() : "";
                            String occupation = !data.isNull(getString(R.string.api_response_param_key_occupation))
                                    ? data.getString(getString(R.string.api_response_param_key_occupation)).trim() : "";
                            String education = !data.isNull(getString(R.string.api_response_param_key_education))
                                    ? data.getString(getString(R.string.api_response_param_key_education)).trim() : "";
                            String employer = !data.isNull(getString(R.string.api_response_param_key_employer))
                                    ? data.getString(getString(R.string.api_response_param_key_employer)).trim() : "";
                            String religion = !data.isNull(getString(R.string.api_response_param_key_religion))
                                    ? data.getString(getString(R.string.api_response_param_key_religion)).trim() : "";
                            String relationshipstatus = !data.isNull(getString(R.string.api_response_param_key_relationshipstatus))
                                    ? data.getString(getString(R.string.api_response_param_key_relationshipstatus)).trim() : "";
                            boolean prefnotification = !data.isNull(getString(R.string.api_response_param_key_prefnotification))
                                    ? data.getBoolean(getString(R.string.api_response_param_key_prefnotification)) : true;
                            boolean prefclearmessages = !data.isNull(getString(R.string.api_response_param_key_prefclearmessages))
                                    ? data.getBoolean(getString(R.string.api_response_param_key_prefclearmessages)) : true;
                            boolean prefshowage = !data.isNull(getString(R.string.api_response_param_key_prefshowage))
                                    ? data.getBoolean(getString(R.string.api_response_param_key_prefshowage)) : true;
                            String profileimage1 = !data.isNull(getString(R.string.api_response_param_key_profileimage1))
                                    ? data.getString(getString(R.string.api_response_param_key_profileimage1)).trim() : "";
                            String profileimage2 = !data.isNull(getString(R.string.api_response_param_key_profileimage2))
                                    ? data.getString(getString(R.string.api_response_param_key_profileimage2)).trim() : "";
                            String profileimage3 = !data.isNull(getString(R.string.api_response_param_key_profileimage3))
                                    ? data.getString(getString(R.string.api_response_param_key_profileimage3)).trim() : "";
                            String profileimage4 = !data.isNull(getString(R.string.api_response_param_key_profileimage4))
                                    ? data.getString(getString(R.string.api_response_param_key_profileimage4)).trim() : "";
                            String username = !data.isNull(getString(R.string.api_response_param_key_username))
                                    ? data.getString(getString(R.string.api_response_param_key_username)).trim() : "";
                            JSONArray received = !data.isNull(getString(R.string.api_response_param_key_received))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_received)) : new JSONArray();
                            JSONArray sent = !data.isNull(getString(R.string.api_response_param_key_sent))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_sent)) : new JSONArray();
                            JSONArray matches = !data.isNull(getString(R.string.api_response_param_key_matches))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_matches)) : new JSONArray();
                            JSONArray matchesdetails = !data.isNull(getString(R.string.api_response_param_key_matchesdetails))
                                    ? data.getJSONArray(getString(R.string.api_response_param_key_matchesdetails)) : new JSONArray();

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, firstname.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, lastname.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, sex.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, birthdate.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profilepic.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, accounttype.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETYPE, devicetype.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMPLOYER, employer.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELIGION, religion.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, relationshipstatus.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, username.trim());
                            SharedPreferenceUtil.save();*/

                            stopProgress();
//                            intent = new Intent(this, SelectFavFood.class);
                           /* supportFinishAfterTransition();*/
                        } else {
                            stopProgress();
                            showSnackBarMessageOnly(errormessage.trim());
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d("RESPONSE_EDIT_PROFILE_SOCIAL_SIGN_IN", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            stopProgress();
                            // TODO Forward user to TasteBuds profile screen or Home Screen according to preferences saved.
                            if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_ISREGISTERED, false)) {
                               /* intent = new Intent(context, EditProfileNewActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                                startActivity(intent);
                                supportFinishAfterTransition();*/

                                intent = new Intent(this, TasteBudsSuggestionsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("isFromRegister", true);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } else {
                                if (isFromReferCode) {
                                    // Forward user to chat screen from here.
                                    Intent intent = new Intent(SignInActivity.this, ChatDetailsActivity.class);
                                    intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUsers);
                                    intent.putExtra("isFromNotification", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    supportFinishAfterTransition();
                                } else {
                                    intent = new Intent(context, HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                                    startActivity(intent);
                                    supportFinishAfterTransition();
                                }
                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_SIGN_UP) {
                Log.d("RESPONSE_SIGN_UP_API", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    boolean isregistered = true;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        String email = "", id = "", profilepic1 = "";
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            email = !data.isNull(context.getResources().getString(R.string.api_response_param_key_email))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_email)).trim() : "";
                            id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            profilepic1 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                            isregistered = !settings.isNull("isregistered") ? settings.getBoolean("isregistered") : true;

                            // Save Firebae Message & Friend object here.
//                            firebase_message
                            strFirebaseMessageReceived = !data.isNull(getString(R.string.api_response_param_key_firebase_message))
                                    ? data.getString(getString(R.string.api_response_param_key_firebase_message)).trim() : "";
                            JSONObject friends = !data.isNull(getString(R.string.api_response_param_key_friends))
                                    ? data.getJSONObject(getString(R.string.api_response_param_key_friends)) : new JSONObject();
                            strFriendIdReceived = !friends.isNull(getString(R.string.api_response_param_key_id))
                                    ? friends.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                        }


                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, isregistered);
                        SharedPreferenceUtil.save();

                        /*stopProgress();
                        showSnackBarMessageOnly(getString(R.string.you_have_successfully_registered));
                        isSignIn = true;
                        initLayout();
                        edtEmail.setText(email.trim());*/

                        if (currentUser != null) {
                            if (FirebaseAuth.getInstance() != null) {
                                FirebaseAuth.getInstance().signOut();
                            }
                        }


//                        final String senderId = id.trim(), senderDp = profilepic1.trim();

                        // Register user to firebase here.
                        mAuth.createUserWithEmailAndPassword(email, getString(R.string.password_default_for_firebase_authentication))
                                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "createUserWithEmail:success");
                                            FirebaseUser user = mAuth.getCurrentUser();


                                            // Calling sign in api & login user in the app
                                            stopProgress();
                                            loginWithCredApi();
                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "createUserWithEmail:failure", task.getException());

                                            // Calling sign in api & login user in the app
                                            stopProgress();
                                            loginWithCredApi();
                                        }
                                        // ...
                                    }
                                });
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                //perform sign in to firebase first.
                mAuth.signInWithEmailAndPassword(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, "").trim(),
                        getString(R.string.password_default_for_firebase_authentication))
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    Log.d(TAG, "signInWithEmail:success");
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    // TODO Save Profile of user, as user is registered for the first time.

                                    OtherUsers = null;
                                    try {
                                        // Send a message to firebase
                                        if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                                                && !strFriendIdReceived.trim().isEmpty()) {
                                            JSONObject jsonObjectResponse = null;


                                            jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                                    ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                                            Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                                            JSONObject settings = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_settings))
                                                    ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                                            boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                                    ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                                            String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                                    ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                                            if (status) {
                                                matchList = new ArrayList<>();
                                                JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                                        ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                                                JSONObject dataInner = null;
                                                for (int i = 0; i < data.length(); i++) {
                                                    ChatListVO item = new ChatListVO();
                                                    try {
                                                        dataInner = data.getJSONObject(i);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                                            ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                                    String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                                            ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                                    String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                                            ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                                    String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                                            ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                                    item.setMatchid(matchId);
                                                    item.setFriend1(friend1);
                                                    item.setFriend2(friend2);

                                                    JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                                            ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                                                    for (int j = 0; j < details.length(); j++) {

                                                        JSONObject detailInner = details.getJSONObject(j);
                                                        String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                                                ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                                        String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                                                ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                                        String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                                                ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                                        String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                                                ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                                        String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                                                ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                                        item.setUserId(userId);
                                                        item.setFirstName(first_name);
                                                        item.setLastName(lastName);
                                                        item.setProfilePic(profilePic);
                                                        item.setGender(gender);
                                                        item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                                                    }
                                                    matchList.add(item);

                                                }

                                            }

                                            // Get user object here.
                                            for (int i = 0; i < matchList.size(); i++) {
                                                OtherUsers = matchList.get(i);
                                            }


                                            // Prepare POJO Message VO from JSON Array
                                            JSONArray array = new JSONArray(strFirebaseMessageReceived);
                                            for (int i = 0; i < array.length(); i++) {
                                                String strMessage = array.getString(i);
                                                Calendar c = Calendar.getInstance();
                                                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                                Gson gson = new Gson();
                                                MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                                                message.setRecieverId(SharedPreferenceUtil.getString(Constants
                                                        .LOGGED_IN_USER_USER_ID, "").trim());
                                                message.setRecieverDP(SharedPreferenceUtil.getString(Constants
                                                        .LOGGED_IN_USER_PROFILEPIC1, "").trim());
                                                message.setDelivered(String.valueOf(timestampToSave));

//                                                                Log.d(TAG, "Set sender details here: ID: " + senderId + ", senderDp: " + senderDp);

                                                // Insert message to firebase here.
                                                String key = mDatabase.child("posts").push().getKey();
                                                mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                                                // Add key here
                                                message.setKey(key);


                                                /*// Get details of sender user here.
                                                ChatListVO OtherUsers = new ChatListVO();
                                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                OtherUsers.setUserId(message.getSenderId().trim());
//                                                                OtherUsers.setFirstName(message.get);*/
                                                if (OtherUsers != null) {
                                                    OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                    OtherUsers.setUserId(message.getSenderId().trim());
                                                }

                                                if (!isFromReferCode) {
                                                    Log.d(TAG, "NOtification on invite called.");
//                                                                if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)) {
                                                    sendNotification(message.getMsg().trim(), message.getMsg().trim(), "message", OtherUsers);
                                                }
//                                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        stopProgress();
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        stopProgress();
                                        e.printStackTrace();
                                    } finally {
                                        sendRequestSaveSocialProfileAPI();
                                    }
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                                    // TODO Save Profile of user, as user is registered for the first time.
                                    sendRequestSaveSocialProfileAPI();
                                }
                            }
                        });
                /*stopProgress();*/
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN && response != null) {
                Log.i(TAG, "ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN: " + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                try {
                    // Send a message to firebase
                    if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                            && !strFriendIdReceived.trim().isEmpty()) {
                        JSONObject jsonObjectResponse = null;

                        OtherUsers = null;
                        jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                        Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                        JSONObject settings = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_settings))
                                ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                        boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                        String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                        if (status) {
                            matchList = new ArrayList<>();
                            JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                    ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                            JSONObject dataInner = null;
                            for (int i = 0; i < data.length(); i++) {
                                ChatListVO item = new ChatListVO();
                                try {
                                    dataInner = data.getJSONObject(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                item.setMatchid(matchId);
                                item.setFriend1(friend1);
                                item.setFriend2(friend2);

                                JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                        ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                                for (int j = 0; j < details.length(); j++) {

                                    JSONObject detailInner = details.getJSONObject(j);
                                    String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                    String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                    String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                    String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                    String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                    item.setUserId(userId);
                                    item.setFirstName(first_name);
                                    item.setLastName(lastName);
                                    item.setProfilePic(profilePic);
                                    item.setGender(gender);
                                    item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                                }
                                matchList.add(item);

                            }

                        }

                        // Get user object here.
                        for (int i = 0; i < matchList.size(); i++) {
                            OtherUsers = matchList.get(i);
                        }

                        // Prepare POJO Message VO from JSON Array
                        JSONArray array = new JSONArray(strFirebaseMessageReceived);
                        for (int i = 0; i < array.length(); i++) {
                            String strMessage = array.getString(i);

                            Gson gson = new Gson();
                            MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                            message.setRecieverId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());
                            message.setRecieverDP(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());

                            // Insert message to firebase here.
                            String key = mDatabase.child("posts").push().getKey();
                            mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                            // Add key here
                            message.setKey(key);

                           /* // Get details of sender user here.
                            ChatListVO OtherUsers = new ChatListVO();
                            OtherUsers.setUserId(message.getSenderId().trim());
                            OtherUsers.setProfilePic(message.getSenderDp().trim());*/
//                                                                OtherUsers.setFirstName(message.get);

                            if (OtherUsers != null) {
                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                OtherUsers.setUserId(message.getSenderId().trim());
                            }

//                                        if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)) {
                            if (!isFromReferCode) {
                                Log.d(TAG, "NOtification on invite called.");
                                sendNotification(message.getMsg().trim(), message.getMsg().trim(), "message", OtherUsers);
                            }
//                                        }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    if (!profileSocialSignIn.isregistered) {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profileSocialSignIn.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profileSocialSignIn.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profileSocialSignIn.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profileSocialSignIn.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profileSocialSignIn.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profileSocialSignIn.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profileSocialSignIn.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profileSocialSignIn.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profileSocialSignIn.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profileSocialSignIn.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profileSocialSignIn.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().trim().replaceAll(",", "."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profileSocialSignIn.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profileSocialSignIn.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profileSocialSignIn.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profileSocialSignIn.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1,
                                !profileSocialSignIn.profilepic1.trim().isEmpty() ? appendUrl(profileSocialSignIn.profilepic1.trim()) :
                                        (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim().isEmpty()
                                                ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim() : ""));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profileSocialSignIn.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profileSocialSignIn.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profileSocialSignIn.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profileSocialSignIn.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profileSocialSignIn.profilepic6.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profileSocialSignIn.profilepic7.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profileSocialSignIn.profilepic8.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profileSocialSignIn.profilepic9.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profileSocialSignIn.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profileSocialSignIn.reference.trim());
                        SharedPreferenceUtil.save();
//                                showToast(getString(R.string.you_are_successfully_logged_in));

                        // TODO Save Profile of user, when user logs in. So that device token and otehr info is always updated in db.
                        sendRequestSaveSocialProfileAPI();

                                /*// TODO Forward user to TasteBuds profile screen.
                                intent = new Intent(context, TasteBudsSuggestionsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();*/
                    } else {
//                                showToast(getString(R.string.you_are_successfully_logged_in));

//                        if (action == Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN) { // TODO Check if all pictures are empty,
                        // then look for all facebook profile pictures and then save user profile to server.
                        Log.d(TAG, "Social profile pic: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim()
                                + ", Profile pic: " + profileSocialSignIn.profilepic1.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1,
                                !profileSocialSignIn.profilepic1.trim().isEmpty() ? appendUrl(profileSocialSignIn.profilepic1.trim()) :
                                        (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim().isEmpty()
                                                ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim() : ""));
                        SharedPreferenceUtil.save();
                        Log.d(TAG, "Social profile pic: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());

                        if (profileSocialSignIn.profilepic2.trim().isEmpty() &&
                                profileSocialSignIn.profilepic3.trim().isEmpty() && profileSocialSignIn.profilepic4.trim().isEmpty()
                                && profileSocialSignIn.profilepic4.trim().isEmpty() && profileSocialSignIn.profilepic5.trim().isEmpty()
                                && profileSocialSignIn.profilepic6.trim().isEmpty() && profileSocialSignIn.profilepic7.trim().isEmpty()
                                && profileSocialSignIn.profilepic8.trim().isEmpty() && profileSocialSignIn.profilepic9.trim().isEmpty()) {
                            showProgress(getString(R.string.loading));
                            getFacebookAlbumsForProfilePicture("");
                        }
                        /*} else { // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        }*/
                    }
                }
            } else if (action == Constants.ACTION_CODE_API_ADDREFERENCE && response != null) {
                Log.d(TAG, "ACTION_CODE_API_ADD_REFERENCE: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        String email = "", id = "", profilepic1 = "";
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            email = !data.isNull(context.getResources().getString(R.string.api_response_param_key_email))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_email)).trim() : "";
                            id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            profilepic1 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";


                            // Save Firebae Message & Friend object here.
//                            firebase_message
                            strFirebaseMessageReceived = !data.isNull(getString(R.string.api_response_param_key_firebase_message))
                                    ? data.getString(getString(R.string.api_response_param_key_firebase_message)).trim() : "";
                            JSONObject friends = !data.isNull(getString(R.string.api_response_param_key_friends))
                                    ? data.getJSONObject(getString(R.string.api_response_param_key_friends)) : new JSONObject();
                            strFriendIdReceived = !friends.isNull(getString(R.string.api_response_param_key_id))
                                    ? friends.getString(getString(R.string.api_response_param_key_id)).trim() : "";

                            // If friend id is valid, send message to firebase here and forward user to chat screen directly.
                            try {
                                // Send a message to firebase
                                if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                                        && !strFriendIdReceived.trim().isEmpty()) {
                                    JSONObject jsonObjectResponseFriend = null;

                                    ChatListVO OtherUsers = null;
                                    jsonObjectResponseFriend = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                            ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                                    Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponseFriend.toString().trim());
                                    JSONObject settingsFriend = !jsonObjectResponseFriend.isNull(getString(R.string.api_response_param_key_settings))
                                            ? jsonObjectResponseFriend.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                                    boolean statusFriend = !settingsFriend.isNull(getString(R.string.api_response_param_key_success))
                                            ? settingsFriend.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                                    String errormessageFriend = !settingsFriend.isNull(getString(R.string.api_response_param_key_errormessage))
                                            ? settingsFriend.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                                    if (statusFriend) {
                                        matchList = new ArrayList<>();
                                        JSONArray dataFriend = !jsonObjectResponseFriend.isNull(getString(R.string.api_response_param_key_data))
                                                ? jsonObjectResponseFriend.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                                        JSONObject dataInner = null;
                                        for (int x = 0; x < dataFriend.length(); x++) {
                                            ChatListVO item = new ChatListVO();
                                            try {
                                                dataInner = dataFriend.getJSONObject(x);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                            String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                            String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                            String statusFriendInner = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                            item.setMatchid(matchId);
                                            item.setFriend1(friend1);
                                            item.setFriend2(friend2);

                                            JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                                    ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();

                                            for (int j = 0; j < details.length(); j++) {
                                                JSONObject detailInner = details.getJSONObject(j);
                                                String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                                String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                                String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                                String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                                String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                                item.setUserId(userId);
                                                item.setFirstName(first_name);
                                                item.setLastName(lastName);
                                                item.setProfilePic(profilePic);
                                                item.setGender(gender);
                                                item.setStatus(!statusFriendInner.trim().isEmpty() ? statusFriendInner.trim() : "0");
                                            }
                                            matchList.add(item);

                                        }

                                    }

                                    // Get user object here.
                                    boolean isFriendExists = false;
                                    for (int x = 0; x < matchList.size(); x++) {
                                        if (strFriendIdReceived.trim().equalsIgnoreCase(matchList.get(x).getMatchid())) {
                                            isFriendExists = true;
                                            OtherUsers = matchList.get(x);
                                        }
                                    }


                                    // Prepare POJO Message VO from JSON Array
                                    if (isFriendExists) {
                                        JSONArray array = new JSONArray(strFirebaseMessageReceived);
                                        for (int x = 0; x < array.length(); x++) {
                                            String strMessage = array.getString(x);
                                            Calendar c = Calendar.getInstance();
                                            long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                            Gson gson = new Gson();
                                            MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                                            message.setRecieverId(SharedPreferenceUtil.getString(Constants
                                                    .LOGGED_IN_USER_USER_ID, "").trim());
                                            message.setRecieverDP(SharedPreferenceUtil.getString(Constants
                                                    .LOGGED_IN_USER_PROFILEPIC1, "").trim());
                                            message.setDelivered(String.valueOf(timestampToSave));

//                                                                Log.d(TAG, "Set sender details here: ID: " + senderId + ", senderDp: " + senderDp);

                                            // Insert message to firebase here.
                                            String key = mDatabase.child("posts").push().getKey();
                                            mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                                            // Add key here
                                            message.setKey(key);


                                                /*// Get details of sender user here.
                                                ChatListVO OtherUsers = new ChatListVO();
                                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                OtherUsers.setUserId(message.getSenderId().trim());
//                                                                OtherUsers.setFirstName(message.get);*/
                                            if (OtherUsers != null) {
                                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                OtherUsers.setUserId(message.getSenderId().trim());
                                            }
                                        }

                                        // Forward user to chat screen from here.
                                        Intent intent = new Intent(SignInActivity.this, ChatDetailsActivity.class);
                                        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUsers);
                                        intent.putExtra("isFromNotification", true);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        supportFinishAfterTransition();
                                    } else {
                                        if (isReferCodeFromSocialSignIn) {
                                            isFromReferCode = true;
                                            callFriendsListApiForSocialSignIn();
                                        } else {
                                            isFromReferCode = true;
                                            callFriendsListApi();
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                stopProgress();
                                e.printStackTrace();

                                // TODO Forward user to MAP screen
                                intent = new Intent(context, HomeActivity.class);
                                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } catch (Exception e) {
                                stopProgress();
                                e.printStackTrace();

                                // TODO Forward user to MAP screen
                                intent = new Intent(context, HomeActivity.class);
                                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } finally {

                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void onSuccessRegisterOrLogin() {

        loginWithFaceBookApi();
       /* intent = new Intent(context, TasteBudsSuggestionsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        supportFinishAfterTransition();*/
    }

    private void loginWithFaceBookApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress(getString(R.string.loading));

            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_socialsignin));
            params.put(getString(R.string.api_param_key_last_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, ""));
            params.put(getString(R.string.api_param_key_first_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, ""));
            params.put(getString(R.string.api_param_key_email), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, ""));
            params.put(getString(R.string.api_param_key_password), getString(R.string.api_param_value_password_default));
            params.put(getString(R.string.api_param_key_mobile), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_MOBILE, "123"));
            params.put(getString(R.string.api_param_key_reference), !strReferCode.trim().isEmpty() ? strReferCode.trim() : "");
            params.put(getString(R.string.api_param_key_messageid), !strMessageId.trim().isEmpty() ? strMessageId.trim() : "");
            params.put(getString(R.string.api_param_key_devicetype), "android");
//            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, ""));
//            params.put(getString(R.string.api_param_key_accounttype), "Facebook");
//            params.put(getString(R.string.api_param_key_profilepic), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, ""));
//            params.put(getString(R.string.api_param_key_birthdate), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "")); // Use facebook id as username
//            params.put(getString(R.string.api_param_key_sex), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, ""));
//            params.put(getString(R.string.api_param_key_username), !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USERNAME, "").trim().isEmpty()
//                    ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USERNAME, "").trim()
//                    : SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim());
//            params.put(getString(R.string.api_param_key_social_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, ""));
//            params.put(getString(R.string.api_param_key_education), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, ""));
//            params.put(getString(R.string.api_param_key_religion), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELIGION, ""));
//            params.put(getString(R.string.api_param_key_employer), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMPLOYER, ""));
//            params.put(getString(R.string.api_param_key_relationshipstatus), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP_STATUS, ""));
//            params.put(getString(R.string.api_param_key_occupation), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, ""));
//            params.put(getString(R.string.api_param_key_about), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT, ""));

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN, SignInActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));

            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
        }
    }

    public void callFriendsListApi() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_FRIEND_LIST, SignInActivity.this);
    }

    public void callFriendsListApiForSocialSignIn() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN, SignInActivity.this);
    }

    private void loginWithCredApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress(getString(R.string.loading));
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_signin));
            params.put(getString(R.string.api_param_key_email), edtEmail.getText().toString().trim());
            params.put(getString(R.string.api_param_key_password), edtPassword.getText().toString().trim());
            params.put(getString(R.string.api_param_key_reference), !strReferCode.trim().isEmpty() ? strReferCode.trim() : "");
            params.put(getString(R.string.api_param_key_messageid), !strMessageId.trim().isEmpty() ? strMessageId.trim() : "");
            params.put(getString(R.string.api_param_key_devicetype), "android");
            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_LOGIN_DB, SignInActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));

            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
        }
    }

    private void loginWithGoogleApi() {
        if (NetworkUtil.isOnline(context)) {
//                                showProgress(getString(R.string.loading));

            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_socialsignin));
            params.put(getString(R.string.api_param_key_last_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, ""));
            params.put(getString(R.string.api_param_key_first_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, ""));
            params.put(getString(R.string.api_param_key_email), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, ""));
            params.put(getString(R.string.api_param_key_password), getString(R.string.api_param_value_password_default));
            params.put(getString(R.string.api_param_key_reference), !strReferCode.trim().isEmpty() ? strReferCode.trim() : "");
            params.put(getString(R.string.api_param_key_messageid), !strMessageId.trim().isEmpty() ? strMessageId.trim() : "");
            params.put(getString(R.string.api_param_key_mobile), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_MOBILE, "123"));
            params.put(getString(R.string.api_param_key_devicetype), "android");

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_GOOGLE_SIGN_IN, SignInActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));

            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
        }
    }

    /*private void sendRequestLoginToDb() {
        params = new HashMap<>();
//        params.put(getString(R.string.api_param_key_username), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, "").trim());
        params.put(getString(R.string.api_param_key_username), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, "").trim());
        params.put(getString(R.string.api_param_key_password), getString(R.string.api_param_value_password_default).trim());

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url) + getString(R.string.api_users_login), params,
                Constants.ACTION_CODE_API_LOGIN_DB, SignInActivity.this);
    }*/

    private void sendRequestGetUserProfileById() {
       /* params = new HashMap<>();
        params.put(getString(R.string.api_param_key_username), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, "").trim());
        params.put(getString(R.string.api_param_key_password), getString(R.string.api_param_value_password_default).trim());*/

        new HttpRequestSingleton(context, getString(R.string.api_base_url) + getString(R.string.api_users_me), null,
                Constants.ACTION_CODE_API_USERS_ME, SignInActivity.this);
        /*new HttpRequestSingletonPost(context, getString(R.string.api_base_url) + getString(R.string.api_users_login), params,
                Constants.ACTION_CODE_API_LOGIN_DB, AppIntroHomeActivity.this);*/
    }

    private void sendRequestSignUpAPI() {
        if (NetworkUtil.isOnline(context)) {
            showProgress(getString(R.string.loading));
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_signup));
            params.put(getString(R.string.api_param_key_last_name), edtLastName.getText().toString().trim());
            params.put(getString(R.string.api_param_key_first_name), edtFirstName.getText().toString().trim());
            params.put(getString(R.string.api_param_key_email), edtEmail.getText().toString().trim());
            params.put(getString(R.string.api_param_key_password), edtPassword.getText().toString().trim());
            params.put(getString(R.string.api_param_key_mobile), edtMobileNumber.getText().toString().trim());
            params.put(getString(R.string.api_param_key_dob), edtBirthDay.getText().toString().trim());
            params.put(getString(R.string.api_param_key_reference), !strReferCode.trim().isEmpty() ? strReferCode.trim() : "");
            params.put(getString(R.string.api_param_key_messageid), !strMessageId.trim().isEmpty() ? strMessageId.trim() : "");

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_SIGN_UP, SignInActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    private void sendRequestSaveSocialProfileAPI() {
        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        params.put(getString(R.string.api_param_key_devicetype), "android");
        fields = fields + getString(R.string.api_param_key_devicetype);

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_gender);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_gender);
            }
            params.put(getString(R.string.api_param_key_gender), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_dob);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_dob);
            }
            params.put(getString(R.string.api_param_key_dob), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_occupation);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_occupation);
            }
            params.put(getString(R.string.api_param_key_occupation), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_education);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_education);
            }
            params.put(getString(R.string.api_param_key_education), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_relationship);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_relationship);
            }
            params.put(getString(R.string.api_param_key_relationship), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_about_me);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_about_me);
            }
            params.put(getString(R.string.api_param_key_about_me), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_ethnicity);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_ethnicity);
            }
            params.put(getString(R.string.api_param_key_ethnicity), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_access_token);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_access_token);
            }
            params.put(getString(R.string.api_param_key_access_token), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_social_id);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_social_id);
            }
            params.put(getString(R.string.api_param_key_social_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic1);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic1);
            }
            params.put(getString(R.string.api_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic2);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic2);
            }
            params.put(getString(R.string.api_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic3);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic3);
            }
            params.put(getString(R.string.api_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic4);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic4);
            }
            params.put(getString(R.string.api_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic5);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic5);
            }
            params.put(getString(R.string.api_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic6);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic6);
            }
            params.put(getString(R.string.api_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic7);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic7);
            }
            params.put(getString(R.string.api_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic8);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic8);
            }
            params.put(getString(R.string.api_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic9);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic9);
            }
            params.put(getString(R.string.api_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim());
        }

        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "");
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }

        if (!SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_devicetoken);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_devicetoken);
            }
            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim());
        }
        params.put(getString(R.string.api_param_key_fields), fields.trim());
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, SignInActivity.this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Get list of all facebook albums on user registration by facebook
     *
     * @param after
     */
    private void getFacebookAlbumsForProfilePicture(String after) {
        GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/me/albums", new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse response) {
                Log.d(TAG, "Facebook albums response: " + response.toString().trim());
                // Insert your code here
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                                /*stopProgress();*/
                            JSONObject jsonObjResponse = response.getJSONObject();

                            JSONArray data = !jsonObjResponse.isNull("data") ? jsonObjResponse.getJSONArray("data") : new JSONArray();
                            JSONObject paging = !jsonObjResponse.isNull("paging") ? jsonObjResponse.getJSONObject("paging") : null;

                            String albumId = "";
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject objData = data.getJSONObject(i);
                                String name = !objData.isNull("name") ? objData.getString("name").trim() : "";
                                if (name.trim().equalsIgnoreCase("Profile Pictures")) {
                                    albumId = !objData.isNull("id") ? objData.getString("id").trim() : "";
                                }
                            }

                            if (!albumId.trim().isEmpty()) {
                                callPhotosAPI(albumId.trim(), "");
                            } else {
                                if (paging != null) {
                                    String next = !paging.isNull("next") ? paging.getString("next").trim() : "";
                                    if (!next.trim().isEmpty()) {
                                        Uri uri = Uri.parse(next.trim());
                                        String after = uri.getQueryParameter("after").trim();
                                        if (after != null && !after.trim().isEmpty()) {
                                            getFacebookAlbumsForProfilePicture(after.trim());
                                        } else {
                                            stopProgress();
                                            // TODO Save Profile of user, as user is registered for the first time.
                                            sendRequestSaveSocialProfileAPI();
                                        }
                                    } else {
                                        stopProgress();
                                        // TODO Save Profile of user, as user is registered for the first time.
                                        sendRequestSaveSocialProfileAPI();
                                    }
                                } else {
                                    stopProgress();
                                    // TODO Save Profile of user, as user is registered for the first time.
                                    sendRequestSaveSocialProfileAPI();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            stopProgress();
                            // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        } catch (Exception e) {
                            e.printStackTrace();
                            stopProgress();
                            // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        }
                    }
                });
            }
        });

        Bundle parameters = new Bundle();
        if (!after.trim().isEmpty())
            parameters.putString("after", after.trim());
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Get all photos of Facebook album, by album Id
     *
     * @param albumId Id of album to fetch photos for.
     * @param after   after
     */
    private void callPhotosAPI(String albumId, String after) {
/*//        *//*showProgress(getString(R.string.loading));*//**//**/
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/" + albumId.trim() + "/photos", new GraphRequest.Callback() {
                @Override
                public void onCompleted(final GraphResponse response) {
                    // Insert your code here
                    Log.d(TAG, "Response photos by album id: " + response.toString().trim());
                    // Insert your code here
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                /*stopProgress();*/
                                JSONObject jsonObjResponse = response.getJSONObject();

                                JSONArray data = !jsonObjResponse.isNull("data") ? jsonObjResponse.getJSONArray("data") : new JSONArray();
                                JSONObject paging = !jsonObjResponse.isNull("paging") ? jsonObjResponse.getJSONObject("paging") : null;
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject objData = data.getJSONObject(i);
                                    JSONArray images = !objData.isNull("images") ? objData.getJSONArray("images") : new JSONArray();

                                    String source = "";
                                    for (int j = 0; j < images.length(); j++) {
                                        JSONObject objImage = images.getJSONObject(j);
//                                        image.setHeight(!objImage.isNull("height") ? objImage.getLong("height") : 0);
//                                        image.setWidth(!objImage.isNull("width") ? objImage.getLong("width") : 0);
//                                        image.setSource(!objImage.isNull("source") ? objImage.getString("source") : "");
                                        if (source.trim().isEmpty()) {
                                            source = !objImage.isNull("source") ? objImage.getString("source") : "";
                                            break;
                                        }
                                    }

                                    if (source.trim().isEmpty()) {
                                        source = !objData.isNull("picture") ? objData.getString("picture").trim() : "";
                                    }

                                    if (!source.trim().isEmpty()) {
                                        if (i == 1) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 2) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 3) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 4) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 5) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 6) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 7) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 8) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, source.trim());
                                            SharedPreferenceUtil.save();
                                        }
//                                     else if (i == 5) {
//                                            objProfile.setImage5(source.trim());
//                                        } else if (i == 6) {
//                                            objProfile.setImage6(source.trim());
//                                        }
                                    }
//                                    photo.setId(!objData.isNull("id") ? objData.getString("id").trim() : "");
//                                    photo.setPicture(!objData.isNull("picture") ? objData.getString("picture").trim() : "");
                                }

                                // TODO Save Profile of user, as user is registered for the first time.
                                sendRequestSaveSocialProfileAPI();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "images,picture");
            if (!after.trim().isEmpty())
                parameters.putString("after", after.trim());
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
    }

    public void handleFacebookAccessToken(AccessToken token) {
        Log.d("SIGNUP_FRAGMENT", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d("", "signInWithCredential:onComplete:" + task.isSuccessful());

                // If sign in fails, display a message to the user. If sign in succeeds
                // the auth state listener will be notified and logic to handle the
                // signed in user can be handled in the listener.
                if (!task.isSuccessful()) {
                    Log.w("", "signInWithCredential", task.getException());
                    Toast.makeText(getApplicationContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }

                // ...
            }
        });
    }

    public void showDialogForgotPassword() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_custom_dialog_forgot_password, null);
        dialogBuilder.setView(dialogView);

        final EditText edtEmail = (EditText) dialogView.findViewById(R.id.editTextCustomDialogForgotPassword);

        dialogBuilder.setTitle(getString(R.string.forgot_password));
        dialogBuilder.setMessage(getString(R.string.submit_your_email_address_to_reset_password));
        dialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // TODO Submit email address to resend the password
                dialog.dismiss();
            }
        });
        dialogBuilder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Dismiss dialog here
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private Calendar stringToCalender(String dates) {
        String string = dates;
        String pattern = "MM/dd/yyyy";
        Date date = null;
        Calendar calendar = Calendar.getInstance();
        try {
            if (!string.trim().isEmpty()) {
                date = new SimpleDateFormat(pattern).parse(string);

                if (date != null) {
                    System.out.println(date); // Wed Mar 09 03:02:10 BOT 2011
                }

                calendar.setTime(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return calendar;
        }
    }

   /* @Override
    public void onConfigurationChanged(Configuration newConfig) {

        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;

        Log.d(TAG, "onConfigChange. Height: " + heightScreen + ", Width: " + widthScreen);
        super.onConfigurationChanged(newConfig);


    }*/

    private void sendNotification(String text, String messageBody, String type, ChatListVO OtherUser) {
        List<String> listTemp = new ArrayList<>(Arrays.asList(SharedPreferenceUtil.getString(Constants.NOTIFICATION_MESSAGES, "").trim().split(",")));
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < listTemp.size(); i++) {
            if (!listTemp.get(i).trim().isEmpty()) {
                list.add(listTemp.get(i).trim());
            }
        }

        if (!text.trim().isEmpty()) {
            list.add(text.trim());
        } else {
            list.add(text.trim());
        }

        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, android.text.TextUtils.join(",", list));
        SharedPreferenceUtil.save();

//            Intent intent = new Intent(this, ListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);

//        Intent intent = new Intent(SignInActivity.this, ChatListActivity.class);
        Intent intent = new Intent(SignInActivity.this, ChatDetailsActivity.class);
        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
        intent.putExtra("isFromNotification", true);
//        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser); 
//        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList); 
//        intent.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());

        PendingIntent contentIntent = null;
        contentIntent = PendingIntent.getActivity(getApplicationContext(), (int) (Math.random() * 100), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setTicker(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setSmallIcon(R.mipmap.ic_notification_big)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(OtherUser.getFirstName() + " " + OtherUser.getLastName())
                .setContentText(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setAutoCancel(true)
                .setNumber(list.size())
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(contentIntent)
                .setColor(getResources().getColor(R.color.colorPrimaryDark));

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(notificationBuilder).setBigContentTitle(getString(R.string.app_name));
        if (list.size() == 1) {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notification));
            pictureStyleNotification(inboxStyle, text, type, notificationBuilder, OtherUser);

        } else {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

            Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notification.defaults |= Notification.DEFAULT_LIGHTS;
            notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

            Log.d(TAG, "NOtification on invite. Sending notification.");

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
            notificationManager.notify(0, notification);

            // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
            sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/
        }

        for (int i = 0; i < list.size(); i++) {
            inboxStyle.addLine(list.get(i));
        }
        inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

        Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
        notificationManager.notify(0, notification);

        // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
        sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

    }

    public void pictureStyleNotification(final NotificationCompat.InboxStyle inboxStyle, final String text, final String type,
                                         final NotificationCompat.Builder notificationBuilder, final ChatListVO OtherUser) {
        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                notificationBuilder.setLargeIcon(bitmap);

                Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.defaults |= Notification.DEFAULT_LIGHTS;
                notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

                Log.d(TAG, "NOtification on invite. Sending notification.");

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
                notificationManager.notify(0, notification);

                // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
                sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };

        if (!OtherUser.getProfilePic().trim().isEmpty()) {
            Picasso.with(getApplicationContext())
                    .load(OtherUser.getProfilePic())
                    .into(loadtarget);
        }
    }

    private void sendLocalBroadcastManager(String body, String type) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_NOTIFICATION_RECEIVED);
        i.putExtra("type", type);
        i.putExtra("message", body.trim());
      /*  if (action == ACTION_TABLE_INVITE || action == ACTION_ACCEPTED_INVITE || action == ACTION_DINING_STARTED) {
            i.putExtra("restaurant", restaurantDetails);
            i.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());
        }*/
        LocalBroadcastManager.getInstance(SignInActivity.this).sendBroadcast(i);
    }

}
