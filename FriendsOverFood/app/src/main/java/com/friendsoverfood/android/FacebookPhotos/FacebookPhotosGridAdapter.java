package com.friendsoverfood.android.FacebookPhotos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 05/07/16.
 */
public class FacebookPhotosGridAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<FacebookPhotoVO> list;
    private Intent intent;
    private FacebookPhotosActivity activity;

    public FacebookPhotosGridAdapter(Context context, ArrayList<FacebookPhotoVO> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.activity = (FacebookPhotosActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_facebook_photos_grid_adapter, parent, false);

            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.imageViewFacebookPhotosGridAdapter);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final int pos = position;

        int sizeImage = (activity.widthScreen - Constants.convertDpToPixels(22)) / 3;

        // Add LayoutParams for image here.
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(sizeImage, sizeImage);
        holder.imgPhoto.setLayoutParams(lp);

        if (!list.get(position).getPicture().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getPicture().trim()).resize(sizeImage, sizeImage).centerCrop()
//                    .transform(new CircleTransform())
                    .error(R.drawable.ic_user_profile_avatar).placeholder(R.drawable.ic_user_profile_avatar)
                    .into(holder.imgPhoto, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d("FACEBOOK_ALBUMS_COVER_PHOTO", "Success loading profile picture: " + list.get(pos).getPicture().trim());
                        }

                        @Override
                        public void onError() {
                            Log.d("FACEBOOK_ALBUMS_COVER_PHOTO", "Error loading profile picture: " + list.get(pos).getPicture().trim());
                        }
                    });
        }

//        final ViewHolder finalHolder = holder;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Set activity result here.
                Intent intent = new Intent();
                intent.putExtra(Constants.INTENT_PARAM_FB_ALBUM, list.get(pos));
                activity.setResult(Activity.RESULT_OK, intent);
                activity.supportFinishAfterTransition();
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public ImageView imgPhoto;
    }
}
