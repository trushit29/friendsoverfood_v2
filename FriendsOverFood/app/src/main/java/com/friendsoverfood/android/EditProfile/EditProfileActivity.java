package com.friendsoverfood.android.EditProfile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.R;

/**
 * Created by Trushit on 11/07/17.
 */

public class EditProfileActivity extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListners();
        setVisibilityActionBar(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setTitleSupportActionBar(getString(R.string.edit_profile));*/

    }

    private void setListners() {

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//
        view = inflater.inflate(R.layout.layout_edit_profile_activity, getMiddleContent());

    }

    private void initLayoutSuggestionsChips() {

    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO Get tastebuds list of user
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;

        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);

    }
}
