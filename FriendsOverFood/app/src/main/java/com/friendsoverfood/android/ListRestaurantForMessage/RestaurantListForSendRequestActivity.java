package com.friendsoverfood.android.ListRestaurantForMessage;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Trushit on 27/02/17.
 */

public class RestaurantListForSendRequestActivity extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "RESTAURANTS_LIST_FOR_SEND_REQUEST_ACTIVITY";

    public RecyclerView rvRestaurants;
    public GridLayoutManager llmRecyclerView;
    public RestaurantListForSendRequestAdapter adapter;
    public ArrayList<RestaurantListVO> listRestaurant = new ArrayList<>();
    public ArrayList<RestaurantListVO> listRestaurantSelected = new ArrayList<>();
    public TextView txtSend, txtSelectRestaurant, txtMessageRequestCount;
    public LinearLayout linearMessageRequest, linearMessageRequestSend;
    public EditText edtMessageRequestCount, edtMessageRequestChat;
    public ImageView imgSend, imgMessageRequestList, imgMessageRequestSend;
    public RelativeLayout relativeChatFooter;

    // Variables for custom action bar
    public View viewCustomActionBar;
    public EditText txtSearchTastebuds;
    public LinearLayout linearSearchTastebuds;
    public ImageView imgSearchTastebudsCancel;
    public TextView txtCancel, txtSearch;
    public String searchKeyWord = "";
    public String strSearchRestaurantQueryPrevious = "";
    public String strQuerySearchTastebuds = "";

    // Variables related paging & scrolling
    public int posToScrollRestaurants = 0, posRestaurantListMatrixAPI = -1, posRest = 0; // countToSkip = 0,
    public String strPageTokenRestaurantList = "";
    public boolean isEndOfResult = false;

    public int heightRootAdapter = 0;
    public ChatListVO chatSelected;
    public ArrayList<ChatListVO> listUsers = new ArrayList<>();
    public ArrayList<MessageVO> listChats = new ArrayList<>();

    // Friend request related variables
    public String matchIdNewRequest = "";
    public String strChatMessage = "";
    public int countRestaurantMessageSent = -1;

    // Firebase chat related variables
    private DatabaseReference mDatabase;
    public DatabaseReference mDatabaseMessages;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public CountDownTimer timerSearchSuggestions;
    public boolean isTimerCalled = false;
    public EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager scrollListenerRestaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        setTitleSupportActionBar(getString(R.string.title_restaurants_list));
        setTitleSupportActionBar("");
        setDrawerMenu();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        /*setDrawerMenu();
        lockDrawerMenu();*/

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_restaurant_list_send_request_activity, getMiddleContent());

        rvRestaurants = (RecyclerView) view.findViewById(R.id.recyclerViewRestaurantListSendRequestActivity);
        txtSend = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestActivitySend);
        imgSend = (ImageView) view.findViewById(R.id.imageViewRestaurantListSendRequestActivitySend);
        txtSelectRestaurant = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestActivitySelectRestaurants);

        linearMessageRequest = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListSendRequestActivityRequestMessage);
        linearMessageRequestSend = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListSendRequestActivitySend);
        edtMessageRequestCount = (EditText) view.findViewById(R.id.editTextRestaurantListSendRequestActivityRequestMessage);
        txtMessageRequestCount = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestActivityRequestMessageCounts);
        relativeChatFooter = (RelativeLayout) view.findViewById(R.id.relativeChatFooter);
        edtMessageRequestChat = (EditText) view.findViewById(R.id.editTextChatDetailsActivityMessage);
        imgMessageRequestList = (ImageView) view.findViewById(R.id.imageViewRestoList);
        imgMessageRequestSend = (ImageView) view.findViewById(R.id.imageViewChatDetailsActivitySendMessage);

        llmRecyclerView = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        rvRestaurants.setLayoutManager(llmRecyclerView);
        rvRestaurants.setHasFixedSize(false);

        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_list_send_reqeust_activity, linearToolBarCustomLayout);
        linearToolBarCustomLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
//        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setCustomView(viewCustomActionBar);
//        Toolbar parent =(Toolbar) viewCustomActionBar.getParent();
        toolbarLayoutRoot.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        toolbarLayoutRoot.setContentInsetsAbsolute(0, 0);
        toolbarLayoutRoot.setContentInsetsRelative(0, 0);
        toolbarLayoutRoot.setContentInsetStartWithNavigation(0);

        if (getIntent().hasExtra(Constants.INTENT_CHAT_SELECTED)) {
            chatSelected = (ChatListVO) getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED);
            txtSelectRestaurant.setText(getString(R.string.select_upto_5_restaurant_and_share_with) + " " + chatSelected.getFirstName());
        }

        if (getIntent().hasExtra(Constants.INTENT_CHAT_LIST)) {
            listUsers = (ArrayList<ChatListVO>) getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST);
        }

        /*if (getIntent().hasExtra(Constants.INTENT_CHAT_MESSAGE)) {
            strChatMessage = getIntent().getStringExtra(Constants.INTENT_CHAT_MESSAGE);
        }*/

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
//        mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");

        edtMessageRequestChat.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //This sets a textview to the current length
                if (edtMessageRequestChat.getText().toString().trim().length() > 0) {
                    edtMessageRequestChat.setError(null);
                }

                strChatMessage = edtMessageRequestChat.getText().toString().trim();

//                    txtMessageRequestCount.setText(String.valueOf(edtMessageRequestChat.getText().toString().length()) + " " + getString(R.string.of_160_characters));
            }

            public void afterTextChanged(Editable s) {
            }
        });

        // TODO Hide resources if member is friend
        if (chatSelected.getStatus().trim().equalsIgnoreCase("0")) {
            txtSelectRestaurant.setText(getString(R.string.select_upto_5_restaurant_and_share_with) + " " + chatSelected.getFirstName());
            txtSelectRestaurant.setVisibility(View.VISIBLE);
            linearMessageRequest.setVisibility(View.VISIBLE);
            imgMessageRequestList.setVisibility(View.GONE);

//            edtMessageRequestCount.setSelection(edtMessageRequestCount.getText().toString().trim().length());

            edtMessageRequestCount.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //This sets a textview to the current length
                    if (edtMessageRequestCount.getText().toString().trim().length() > 0) {
                        edtMessageRequestCount.setError(null);
                    }

                    strChatMessage = edtMessageRequestCount.getText().toString().trim();

                    txtMessageRequestCount.setText(String.valueOf(edtMessageRequestCount.getText().toString().length()) + " " + getString(R.string.of_160_characters));
                }

                public void afterTextChanged(Editable s) {
                }
            });

            edtMessageRequestCount.setText(getString(R.string.hi_exclamation) + " " + chatSelected.getFirstName().trim() + "! "
                    + getString(R.string.hey_please_accept_my_friend_request));
            edtMessageRequestCount.setSelection(edtMessageRequestCount.getText().toString().trim().length());
            txtMessageRequestCount.setText(String.valueOf(edtMessageRequestCount.getText().toString().length()) + " " + getString(R.string.of_160_characters));

            edtMessageRequestChat.setText(getString(R.string.hi_exclamation) + " " + chatSelected.getFirstName().trim() + "! "
                    + getString(R.string.hey_please_accept_my_friend_request));
            edtMessageRequestChat.setSelection(edtMessageRequestChat.getText().toString().trim().length());
//            txtMessageRequestCount.setText(String.valueOf(edtMessageRequestChat.getText().toString().length()) + " " + getString(R.string.of_160_characters));
        } else {
            txtSelectRestaurant.setText(getString(R.string.select_restaurants_and_share_with) + " " + chatSelected.getFirstName());
            txtSelectRestaurant.setVisibility(View.VISIBLE);
            linearMessageRequest.setVisibility(View.GONE);
            imgMessageRequestList.setVisibility(View.VISIBLE);
        }

        setListener();

        setUpSearchViewTastebuds();
        resetAndCallRestaurantListAPI();

        setAllTypefaceMontserratRegular(view);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setListener() {
        txtSend.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        imgMessageRequestSend.setOnClickListener(this);
        imgMessageRequestList.setOnClickListener(this);
    }

    private void setUpSearchViewTastebuds() {

        txtSearchTastebuds = (EditText) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebuds);
        linearSearchTastebuds = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivityTastebuds);
        imgSearchTastebudsCancel = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityTastebudsCancel);

        txtCancel = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebudsCancel);
        txtSearch = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebudsSearch);

        if (getIntent().hasExtra("querySearchRestaurants") && getIntent().getStringExtra("querySearchRestaurants") != null
                && !getIntent().getStringExtra("querySearchRestaurants").trim().isEmpty()) {
            searchKeyWord = getIntent().getStringExtra("querySearchRestaurants").trim();
            txtSearchTastebuds.setText(searchKeyWord.trim());
            txtSearchTastebuds.setSelection(searchKeyWord.trim().length());
        } else {
            /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {*/
            if (txtSearchTastebuds != null) {
                txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
                txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
            }
            /*}*/
        }

        txtSearch.setEnabled(false);
        txtSearchTastebuds.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
                    imgSearchTastebudsCancel.setVisibility(View.VISIBLE);
                    txtSearch.setEnabled(true);
                    /*imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                    if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                        searchKeyWord = s.toString().trim();
                    }

                    Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();*/
                } else {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                    SharedPreferenceUtil.save();

                    if (!strSearchRestaurantQueryPrevious.trim().equalsIgnoreCase(s.toString().trim())) {
                        txtSearch.setEnabled(true);
                    } else {
                        txtSearch.setEnabled(false);
                    }

                    imgSearchTastebudsCancel.setVisibility(View.GONE);
                    searchKeyWord = "";
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                strSearchRestaurantQueryPrevious = s.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtSearchTastebuds.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || (actionId == EditorInfo.IME_ACTION_DONE)
                        || (actionId == EditorInfo.IME_ACTION_NEXT) || (actionId == EditorInfo.IME_ACTION_GO))) {
                    /*Log.i(TAG,"Enter pressed");*/
                    final String s = txtSearchTastebuds.getText().toString().trim();

                    txtSearch.setEnabled(false);

                /*relativeSearchPopup.setVisibility(View.GONE);
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }*/

                    if (!s.toString().trim().isEmpty()) {
                        imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                        if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                            searchKeyWord = s.toString().trim();
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, searchKeyWord.trim());
                            SharedPreferenceUtil.save();
                        }

                        Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        resetAndCallRestaurantListAPI();
                    } else {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                        SharedPreferenceUtil.save();

                        imgSearchTastebudsCancel.setVisibility(View.GONE);
                        searchKeyWord = "";
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        resetAndCallRestaurantListAPI();
                    }
                }
                return false;
            }
        });

        imgSearchTastebudsCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearchTastebuds.setText("");
            }
        });

        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String s = txtSearchTastebuds.getText().toString().trim();

                txtSearch.setEnabled(false);

                /*relativeSearchPopup.setVisibility(View.GONE);
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }*/

                if (!s.toString().trim().isEmpty()) {
                    imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                    if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                        searchKeyWord = s.toString().trim();
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, searchKeyWord.trim());
                        SharedPreferenceUtil.save();
                    }

                    Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();
                } else {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                    SharedPreferenceUtil.save();

                    imgSearchTastebudsCancel.setVisibility(View.GONE);
                    searchKeyWord = "";
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();
                }
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                /*relativeSearchPopup.setVisibility(View.GONE);
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }*/
            }
        });
    }

    private void resetAndCallRestaurantListAPI() {
//        countToSkip = 0;
        posToScrollRestaurants = 0;
        posRestaurantListMatrixAPI = -1;
        strPageTokenRestaurantList = "";
        isEndOfResult = false;

        int posTemp = listRestaurant.size();

        listRestaurant.clear();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        GoogleRestaurentListApi();
    }

    public void GoogleRestaurentListApi() {
        if (NetworkUtil.isOnline(context)) {
            if (!isEndOfResult) {
                showProgress(getString(R.string.loading));
                double radiusInMeters = Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                        .trim().replaceAll(",", ".").trim()) * 1000;
                params = new HashMap<>();
                params.put(getString(R.string.google_api_param_key_), getString(R.string.google_api_param_key_value));
                params.put(getString(R.string.google_api_param_key_location), "" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")
                        + "," + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
                params.put(getString(R.string.google_api_param_key_radius), String.valueOf(radiusInMeters));
//            params.put(getString(R.string.google_api_param_key_rankby), "distance");
//            params.put(getString(R.string.google_api_param_key_keyword), "restaurant");
                params.put(getString(R.string.google_api_param_key_type), "restaurant");
                if (!strPageTokenRestaurantList.trim().isEmpty()) {
                    params.put(getString(R.string.google_api_param_key_pagetoken), strPageTokenRestaurantList.trim());
                }

                if (!searchKeyWord.isEmpty()) {
                    params.put(getString(R.string.google_api_param_key_keyword), searchKeyWord.trim());
                }

                new HttpRequestSingleton(context, getString(R.string.google_api_list_url), params,
                        Constants.ACTION_CODE_GOOGLE_LIST_API, RestaurantListForSendRequestActivity.this);
            }
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    /*public void setAdapterRestaurantListGoogle() {
        if (adapter == null) {
            adapter = new RestaurantListForSendRequestAdapter(context, listRestaurant);
            rvRestaurants.setAdapter(adapter);
            setScrollListenerFriendsList();
        } else {
            adapter.notifyDataSetChanged();
        }
    }*/

    public void setScrollListenerFriendsList() {
        // Scroll listener to call paging at the end of scroll
        if (scrollListenerRestaurants == null) {
            scrollListenerRestaurants = new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerView) {
                @Override
                public void onLoadMore(int lastItemScrolled) {
                    Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);
                /*GoogleRestaurentListApi();*/
                    if (!isTimerCalled) {
                        timerSearchSuggestions = new CountDownTimer(2000, 2000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                Log.d("ON_LOAD_MORE", "On Finish called: ");
                                isTimerCalled = false;
                                GoogleRestaurentListApi();
                            }
                        };
                        timerSearchSuggestions.start();
                        isTimerCalled = true;
                    }
                }

                @Override
                public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
                    if (firstVisibleItem != -1) {
                    /*Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount);
                    if (rvRestaurants.getAdapter().getItemCount() - visibleItemCount == firstVisibleItem && !isEndOfResult) {
                        Log.d(TAG, "Calling paging for restaurants list.");
                        GoogleRestaurentListApi();
                    }*/
                    }
                }

                @Override
                public void onLoadMoreTopItems() {
                }
            };
            rvRestaurants.addOnScrollListener(scrollListenerRestaurants);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (txtSearchTastebuds != null) {
            txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
            txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.textViewRestaurantListSendRequestActivitySend:
            case R.id.imageViewRestaurantListSendRequestActivitySend:
            case R.id.imageViewChatDetailsActivitySendMessage:

                /*listRestaurantSelected.clear();
                for (int i = 0; i < listRestaurant.size(); i++) {
                    if (listRestaurant.get(i).isSelected()) {
                        listRestaurantSelected.add(listRestaurant.get(i));
                    }
                }*/

                /*Intent intent = new Intent();
                if (chatSelected != null)
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                setResult(RESULT_OK, intent);
                supportFinishAfterTransition();*/

                if (chatSelected.getStatus().trim().equalsIgnoreCase("0")) {
//                    if (edtMessageRequestCount.getText().toString().trim().length() > 0) {
                    if (edtMessageRequestChat.getText().toString().trim().length() > 0) {
                        // TODO Send friend request and message here.
                        sendFriendRequestToUserSuggestion();
                    } else {
                        edtMessageRequestChat.setError(getString(R.string.please_type_a_message_first));
//                        showToast(getString(R.string.please_type_a_message_first));
                    }
                } else {
                    // TODO Send selected restaurants as messages here.
                    Log.d(TAG, "Send a chat message here: " + strChatMessage.trim());
                    if (!strChatMessage.trim().isEmpty()) {
                        WriteMessageText(strChatMessage.trim());
                    } else {
                        if (listRestaurantSelected.size() > 0) {
                            countRestaurantMessageSent = 0;
                            WriteMessageResto(listRestaurantSelected.get(0));
                        } else {
                            Intent intent = new Intent();
                            if (chatSelected != null)
                                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                            intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//                            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false);
                            setResult(RESULT_CANCELED, intent);
                            supportFinishAfterTransition();
                        }
                    }
                }
                break;

            case R.id.imageViewRestoList:
                Intent intent = new Intent();
                if (chatSelected != null)
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//                intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false);
                setResult(RESULT_CANCELED, intent);
                supportFinishAfterTransition();
                break;

            default:
                break;

        }
    }

    public void sendFriendRequestToUserSuggestion() {
        showProgress(context.getResources().getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_sendfriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), chatSelected.getUserId().trim());

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST, this);
    }

    public void callFriendsListApi() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_GOOGLE_LIST_API) {
                Log.d(TAG, "ACTION_CODE_GOOGLE_LIST_API: " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";
                    String next_page_token = !jsonObjectResponse.isNull("next_page_token") ? jsonObjectResponse.getString("next_page_token").trim() : "";
                    strPageTokenRestaurantList = next_page_token;

                    if (status.trim().equalsIgnoreCase("OK")) {
                        JSONArray Results = !jsonObjectResponse.isNull("results") ? jsonObjectResponse.getJSONArray("results") : new JSONArray();

                        for (int i = 0; i < listRestaurantSelected.size(); i++) {
                            boolean isExist = false;
                            for (int j = 0; j < listRestaurant.size(); j++) {
                                if (listRestaurantSelected.get(i).getPlaceid().trim().equalsIgnoreCase(listRestaurant.get(j).getPlaceid().trim())) {
                                    isExist = true;
                                }
                            }

                            if (!isExist) {
                                listRestaurant.add(listRestaurantSelected.get(i));
                            }
                        }

                        int posBefore = listRestaurant.size();
                        int countAdded = 0;
                        for (int i = 0; i < Results.length(); i++) {
                            RestaurantListVO restovo = new RestaurantListVO();
                            JSONObject data = Results.getJSONObject(i);
                            JSONObject geomatry = !data.isNull(getString(R.string.google_response_param_key_geometry))
                                    ? data.getJSONObject(getString(R.string.google_response_param_key_geometry)) : new JSONObject();
                            JSONObject location = !geomatry.isNull(getString(R.string.google_response_param_key_location))
                                    ? geomatry.getJSONObject(getString(R.string.google_response_param_key_location)) : new JSONObject();

                            restovo.setLatitude(!location.isNull(getString(R.string.google_response_param_key_lat))
                                    ? location.getString(getString(R.string.google_response_param_key_lat)).trim() : "");
                            restovo.setLongitude(!location.isNull(getString(R.string.google_response_param_key_lng))
                                    ? location.getString(getString(R.string.google_response_param_key_lng)).trim() : "");

                            Log.d(TAG, "Location: " + location.toString() + ", Lat: " + location.getString(getString(R.string.google_response_param_key_lat))
                                    + ", Long: " + location.getString(getString(R.string.google_response_param_key_lng)));

                            restovo.setPlaceid(!data.isNull("place_id") ? data.getString("place_id").trim() : "");

                            restovo.setName(!data.isNull(getString(R.string.google_response_param_key_name))
                                    ? data.getString(getString(R.string.google_response_param_key_name)).trim() : "");
                            JSONObject opening_hours = !data.isNull(getString(R.string.google_response_opening_hours))
                                    ? data.getJSONObject(getString(R.string.google_response_opening_hours)) : new JSONObject();
                            restovo.setOpen_now(!opening_hours.isNull(getString(R.string.google_response_open_now))
                                    ? opening_hours.getString(getString(R.string.google_response_open_now)).trim() : "");
                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");

                            JSONArray photos = !data.isNull("photos") ? data.getJSONArray("photos") : new JSONArray();

                            for (int j = 0; j < photos.length(); j++) {
                                JSONObject photosObj = photos.getJSONObject(j);

                                String photo_reference = !photosObj.isNull(getString(R.string.google_response_param_key_photo_reference))
                                        ? photosObj.getString(getString(R.string.google_response_param_key_photo_reference)).trim() : "";
//                                Log.i("photo_reference", "photo_reference" + photo_reference);
                                restovo.setPhotoReference("https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + widthScreen + "&photoreference="
                                        + photo_reference + "&key=AIzaSyCwErSCCBZN2fL0y9znT3wCY7qrhBxd0JI");

                            }

                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");
                            restovo.setRatings(!data.isNull(getString(R.string.google_response_param_key_ratings))
                                    ? data.getString(getString(R.string.google_response_param_key_ratings)).trim() : "");
                            JSONArray types = !data.isNull("types") ? data.getJSONArray("types") : new JSONArray();

                            restovo.setReference(!data.isNull(getString(R.string.google_response_param_key_reference))
                                    ? data.getString(getString(R.string.google_response_param_key_reference)).trim() : "");

                            Location locationA = new Location("point A");

                            locationA.setLatitude(Double.valueOf(restovo.getLatitude()));
                            locationA.setLongitude(Double.valueOf(restovo.getLongitude()));

                            Location locationB = new Location("point B");

                            locationB.setLatitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")));
                            locationB.setLongitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")));

                            float distance = locationA.distanceTo(locationB);
                            Log.i("distnce", "" + distance);

                            boolean isRestoVoExists = false;

                            /*if (listRestaurant.size() == 0 && i == 0) {
                                restovo.setSelected(true);
                            }*/

                            if (distance > Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MIN_DISTANCE, "1")) * 1000
                                    && Float.valueOf(restovo.getRatings().isEmpty() ? "0" : restovo.getRatings()) >=
                                    Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_PREFER_RATING, "0"))) {
                                for (int j = 0; j < listRestaurant.size(); j++) {
                                    if (listRestaurant.get(j).getPlaceid().trim().equalsIgnoreCase(restovo.getPlaceid().trim())) {

                                        isRestoVoExists = true;
                                    }
                                }

                                if (!isRestoVoExists) {
                                    if (types.toString().contains("lodging") || types.toString().contains("spa")) {

                                    } else {
                                        countAdded++;
                                        listRestaurant.add(restovo);
                                    }
                                }
                            }
                        }

                        if (posRestaurantListMatrixAPI < listRestaurant.size()) {
                            GetTimeMeasureBetweenCoOrdinates();
                        } else {
                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                            /*if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            } else {
                                adapter = new ChatUsersListAdapter(context, listRestaurant);
                            }*/
                        }

                        // Sort the list according to isSelected
//                        Collections.sort(listRestaurant, new HashMapComparatorBoolean());

                        if (Results.length() > 0 && !strPageTokenRestaurantList.trim().isEmpty()) {
                            isEndOfResult = false;
                        } else {
                            isEndOfResult = true;
                        }

                        if (adapter == null) {
                            adapter = new RestaurantListForSendRequestAdapter(context, listRestaurant);
                            rvRestaurants.setAdapter(adapter);
                            setScrollListenerFriendsList();
                        } else {
                            adapter.notifyItemRangeInserted(posBefore, countAdded);
                            adapter.notifyDataSetChanged();
                        }
                        stopProgress();

                    } else {
                        stopProgress();
//                        showSnackBarMessageOnly(getString(R.string.some_error_occured));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(e.getMessage());
                }

            } else if (action == Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonObjectSend = dataArray.getJSONObject(i);

                            matchIdNewRequest = !jsonObjectSend.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? jsonObjectSend.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            String statusChat = !jsonObjectSend.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                                    ? jsonObjectSend.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";

                            // Update chat selected object here.
                            chatSelected.setMatchid(matchIdNewRequest);
                            chatSelected.setStatus(statusChat);
                        }


                        /*if (posSelected < listUsersNearByClicked.size()) {
                            listUsersNearByClicked.get(posSelected).setFriendstatus("Sent");
                            adapter.notifyItemChanged(posSelected);
                            adapter.notifyDataSetChanged();
                        }*/

                        stopProgress();
                        callFriendsListApi();
//                        showSnackBarMessageOnly(context.getString(R.string.friend_request_sent_successfully));
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
                Log.i(TAG, "Friend list api response: " + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();
                stopProgress();

                JSONObject jsonObjectResponse = null;
                try {
                    jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                        SharedPreferenceUtil.save();

                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                        WriteMessageText(strChatMessage.trim());

                        /*Intent intent = new Intent();
                        if (chatSelected != null)
                            intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                        intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                        setResult(RESULT_OK, intent);
                        supportFinishAfterTransition();*/
                    } else {
                        showSnackBarMessageOnly(errormessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (action == Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API) {
                stopProgress();
                Log.d(TAG, "DISTANCE MATRIX API RESPONSE: " + response);

                try {
                    // Parse API response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";

                    if (status.trim().equalsIgnoreCase("OK")) {
                        // Success. Update result for time travel in list here.
                        JSONArray rows = !jsonObjectResponse.isNull("rows") ? jsonObjectResponse.getJSONArray("rows") : new JSONArray();
                        for (int i = 0; i < rows.length(); i++) {
                            JSONObject row = rows.getJSONObject(i);
                            JSONArray elements = !row.isNull("elements") ? row.getJSONArray("elements") : new JSONArray();
                            for (int j = 0; j < elements.length(); j++) {
                                JSONObject element = elements.getJSONObject(j);
                                String statusElement = !element.isNull("status") ? element.getString("status").trim() : "";
                                if (statusElement.trim().equalsIgnoreCase("OK")) {
                                    JSONObject duration = !element.isNull("duration") ? element.getJSONObject("duration") : new JSONObject();
                                    String text = !duration.isNull("text") ? duration.getString("text").trim() : "";
                                    if (!text.trim().isEmpty()) {
                                        /*txtTimeToReach.setText(text);*/
                                        listRestaurant.get(posRestaurantListMatrixAPI).setTimeToReach(text);

                                        // Update adapter here.
                                        if (adapter != null) {
                                            adapter.notifyItemChanged(posRestaurantListMatrixAPI);
                                            adapter.notifyDataSetChanged();
                                        } else {
                                            adapter = new RestaurantListForSendRequestAdapter(context, listRestaurant);
                                            rvRestaurants.setAdapter(adapter);
                                            setScrollListenerFriendsList();
                                        }

                                        if (posRestaurantListMatrixAPI != -1) {
                                            posRestaurantListMatrixAPI++;

                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        } else {
                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        }
                                    } else {
                                        /*txtTimeToReach.setText("");*/
                                        if (posRestaurantListMatrixAPI != -1) {
                                            posRestaurantListMatrixAPI++;

                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        } else {
                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        }
                                    }
                                } else {
                                    /*txtTimeToReach.setText("");*/
                                    if (posRestaurantListMatrixAPI != -1) {
                                        posRestaurantListMatrixAPI++;

                                        if (posRestaurantListMatrixAPI < listRestaurant.size())
                                            GetTimeMeasureBetweenCoOrdinates();
                                        else {
                                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                            // Sort the list according to isSelected
//                                            Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                        }
                                    } else {
                                        if (posRestaurantListMatrixAPI < listRestaurant.size())
                                            GetTimeMeasureBetweenCoOrdinates();
                                        else {
                                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                            // Sort the list according to isSelected
//                                            Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (posRestaurantListMatrixAPI != -1) {
                            posRestaurantListMatrixAPI++;

                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                GetTimeMeasureBetweenCoOrdinates();
                            else {

                            }
                        } else {
                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                GetTimeMeasureBetweenCoOrdinates();
                            else {

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } finally {

                    int posNew = -1;

                }
            } else if (action == Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION) {
                Log.d(TAG, "RESPONSE_SEND_NOTIFICATION_API: " + response);

            }
        }
    }

    public class ComparatorDistanceAscending implements Comparator<RestaurantListVO> {
        public int compare(RestaurantListVO first, RestaurantListVO second) {
//            Log.d("SORT_CHAT_LIST", "First: " + first.getFirstName() + " " + first.lastName + ", Timestamp: " + first.getTimestampLastSeen() + ", Second: " +
//                    second.getFirstName() + " " + second.lastName + ", Timestamp: " + second.getTimestampLastSeen());

//            long firstValue = first.getTimestampLastSeen();
            double firstTime = -1;
            double secondTime = -1;
            int returnPos = 0;
            try {
//                if (!first.isSelected() && !second.isSelected()) {
                if (!first.getTimeToReach().trim().isEmpty() && !second.getTimeToReach().trim().isEmpty()) {
                    String[] timeToReachFirst = first.getTimeToReach().trim().split(" ");
                    String[] timeToReachSecond = second.getTimeToReach().trim().split(" ");

                    if (timeToReachFirst.length > 0)
                        firstTime = Double.parseDouble(timeToReachFirst[0].trim().replaceAll(",", "."));

                    if (timeToReachSecond.length > 0)
                        secondTime = Double.parseDouble(timeToReachSecond[0].trim().replaceAll(",", "."));

                    if (firstTime != -1 && secondTime != -1) {
                        returnPos = firstTime > secondTime ? 1 : firstTime < secondTime ? -1 : 0;
                    } else {
                        returnPos = 0;
                    }
                }
//                }
            } catch (Exception e) {
                e.printStackTrace();
                returnPos = 0;
            } finally {
                return returnPos;
            }
        }
    }

    private void WriteMessageText(String msg) {
        Calendar c = Calendar.getInstance();
        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(msg);
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("text");
//        message.setRestoVO(null);

        String key = mDatabase.child("posts").push().getKey();
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child(key).setValue(message);

        // Add key here
        message.setKey(key);

        // Send chat message notification here.
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_sendnotification));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_chatid), chatSelected.getMatchid().trim());
            params.put(getString(R.string.api_param_key_messageid), key);
            params.put(getString(R.string.api_param_key_message), message.getMsg());
            params.put(getString(R.string.api_param_key_receiverid), message.getRecieverId().trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION, RestaurantListForSendRequestActivity.this);
        }

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);

        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                listUsers.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
            }
        }

        if (listRestaurantSelected.size() > 0) {
            countRestaurantMessageSent = 0;
            WriteMessageResto(listRestaurantSelected.get(0));
        } else {
            Intent intent = new Intent();
            if (chatSelected != null)
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
            intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();


        }

        /*setAdapterChatsList();*/
    }

    public void WriteMessageResto(RestaurantListVO RestoListVO) {
        Calendar c = Calendar.getInstance();
        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(getString(R.string.sent_you_a_restaurant));
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("restaurant");
        message.setRestoVo(RestoListVO);

        String key = mDatabase.child("posts").push().getKey();
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child(key).setValue(message);

        // Add key here
        message.setKey(key);

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);

        // Send chat message notification here.
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_sendnotification));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_chatid), chatSelected.getMatchid().trim());
            params.put(getString(R.string.api_param_key_messageid), key);
            params.put(getString(R.string.api_param_key_message), message.getMsg());
            params.put(getString(R.string.api_param_key_receiverid), message.getRecieverId().trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION, RestaurantListForSendRequestActivity.this);
        }

        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                listUsers.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
            }
        }

        if (countRestaurantMessageSent < listRestaurantSelected.size() - 1) {
            countRestaurantMessageSent++;
            WriteMessageResto(listRestaurantSelected.get(countRestaurantMessageSent));
        } else {
            Intent intent = new Intent();
            if (chatSelected != null)
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
            intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onBackPressed() {
//        Log.d(TAG, "On back pressed...");
//        intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
        if (getIntent().hasExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST)
                && getIntent().getBooleanExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false)) {
//            Log.d(TAG, "On back press result cancel.");
            Intent intent = new Intent();
            if (chatSelected != null)
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
            intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//                            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false);
            setResult(RESULT_CANCELED, intent);
            supportFinishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }

    public class HashMapComparatorBoolean implements
            Comparator<RestaurantListVO> {

        public int compare(RestaurantListVO first, RestaurantListVO second) {
            boolean b1 = first.isSelected();
            boolean b2 = second.isSelected();

            return (b1 && !b2) ? -1
                    : (!b1 && b2) ? 1
                    : 0;
            /*return Boolean.compare(b1,b2);*/
        }
    }

    public void GetTimeMeasureBetweenCoOrdinates() {
        if (posRestaurantListMatrixAPI == -1) {
            posRestaurantListMatrixAPI = 0;
        }

        if (posRestaurantListMatrixAPI < listRestaurant.size()) {
            if (listRestaurant.get(posRestaurantListMatrixAPI).getTimeToReach().trim().isEmpty()) {
                String destination = "";

                destination = listRestaurant.get(posRestaurantListMatrixAPI).getLatitude() + "," + listRestaurant.get(posRestaurantListMatrixAPI).getLongitude();

                String origin = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                        + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");

                if (!destination.trim().isEmpty() && !destination.trim().equalsIgnoreCase(",") && !origin.trim().equalsIgnoreCase(",")) {
                    if (NetworkUtil.isOnline(context)) {
                        HashMap<String, String> params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
                        params.put(getString(R.string.api_param_key_origins), origin.trim());
                        params.put(getString(R.string.api_param_key_destinations), destination.trim());
                        params.put(getString(R.string.api_param_key_units), getString(R.string.api_param_value_units));
                        params.put(getString(R.string.google_api_param_key_departure_time),
                                String.valueOf(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true)));
                        params.put(getString(R.string.google_api_param_key_traffic_model), getString(R.string.api_param_value_best_guess));
                        params.put(getString(R.string.api_param_key_key), getString(R.string.google_api_key));

                        new HttpRequestSingleton(context, getString(R.string.google_distance_matrix_api_url), params,
                                Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API, RestaurantListForSendRequestActivity.this);
                    } else {
//                activity.stopProgress();
//                showSnackBarMessageOnly(getString(R.string.internet_not_available));
                    }
                } else {

                }
            }
        }
    }
}
