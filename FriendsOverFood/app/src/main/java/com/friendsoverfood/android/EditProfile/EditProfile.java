package com.friendsoverfood.android.EditProfile;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.appevents.AppEventsLogger;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.FacebookPhotos.FacebookAlbumsListActivity;
import com.friendsoverfood.android.FacebookPhotos.FacebookPhotoVO;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.Manifest;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.SignIn.TasteBudsSuggestionsActivity;
import com.friendsoverfood.android.SignIn.TastebudChipVO;
import com.friendsoverfood.android.SignIn.TastebudsChipViewAdapter;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.gson.Gson;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.plumillonforge.android.chipview.OnChipClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class EditProfile extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private ChipView text_chip_layout;
    private ChipViewAdapter adapterLayout;
    private TextView txtViewNoTastebuds;
    private TextView textViewEditTasteBuds;
    private List<TastebudChipVO> listUserTastebuds = new ArrayList<>();
    public Bitmap bitmapCameraUpload = null;
    public ImageView imgForUploadSelected = null;
    public ImageView imgAddButtonForUploadSelected = null;
    private ImageView imageViewEditProfileActivityUserImage4, imageViewEditProfileActivityAddUserImage5, imageViewEditProfileActivityUserImage5,
            imageViewEditProfileActivityAddUserImage3, imageViewEditProfileActivityUserImage3, imageViewEditProfileActivityAddUserImage4,
            imageViewEditProfileActivityAddUserImage2, imageViewEditProfileActivityUserImage2, imageViewEditProfileActivityAddUserImage1,
            imageViewEditProfileActivityUserImage1;
    private Button btnSave;
    public EditText editTextName, editTextLastName, editTextAbout, editTextRelationShipStatus, editTextEthnicity, editTextOccupation;
    private final String TAG = "EDIT_PROFILE_ACTIVITY";
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListners();
        setVisibilityActionBar(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        /*final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setTitleSupportActionBar(getString(R.string.edit_profile));*/

        AppEventsLogger.activateApp(this);
        if (ContextCompat.checkSelfPermission(context,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                showDialogAlertRequestPermissionLocation("Permission Required", "Allow FOF to access your photos to upload in your profile");

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    imageViewEditProfileActivityAddUserImage1.setVisibility(View.GONE);
                    imageViewEditProfileActivityAddUserImage2.setVisibility(View.GONE);
                    imageViewEditProfileActivityAddUserImage3.setVisibility(View.GONE);
                    imageViewEditProfileActivityAddUserImage4.setVisibility(View.GONE);
                    imageViewEditProfileActivityAddUserImage5.setVisibility(View.GONE);


                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void setListners() {
        text_chip_layout.setOnClickListener(this);
        textViewEditTasteBuds.setOnClickListener(this);
        imageViewEditProfileActivityAddUserImage1.setOnClickListener(this);
        imageViewEditProfileActivityAddUserImage2.setOnClickListener(this);
        imageViewEditProfileActivityAddUserImage3.setOnClickListener(this);
        imageViewEditProfileActivityAddUserImage4.setOnClickListener(this);
        imageViewEditProfileActivityAddUserImage5.setOnClickListener(this);
        btnSave.setOnClickListener(this);

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//
        view = inflater.inflate(R.layout.layout_edit_profile_activity, getMiddleContent());

        text_chip_layout = (ChipView) view.findViewById(R.id.text_chip_layout_edit);
        // Adapter
        adapterLayout = new TastebudsChipViewAdapter(this);

        txtViewNoTastebuds = (TextView) view.findViewById(R.id.txtViewNoTastebuds);
        textViewEditTasteBuds = (TextView) view.findViewById(R.id.textViewEditTasteBuds);
        imageViewEditProfileActivityUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage1);
        imageViewEditProfileActivityUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage2);
        imageViewEditProfileActivityUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage3);
        imageViewEditProfileActivityUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage4);
        imageViewEditProfileActivityUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage5);
        imageViewEditProfileActivityAddUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage1);
        imageViewEditProfileActivityAddUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage2);
        imageViewEditProfileActivityAddUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage3);
        imageViewEditProfileActivityAddUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage4);
        imageViewEditProfileActivityAddUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage5);
        editTextOccupation = (EditText) view.findViewById(R.id.editTextOccupation);
        editTextEthnicity = (EditText) view.findViewById(R.id.editTextEthnicity);
        editTextRelationShipStatus = (EditText) view.findViewById(R.id.editTextRelationShipStatus);
        editTextAbout = (EditText) view.findViewById(R.id.editTextAbout);
        editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        editTextName = (EditText) view.findViewById(R.id.editTextName);
        btnSave = (Button) view.findViewById(R.id.btnSaveEditProfile);

        /*listUserTastebuds.add(new TastebudChipVO("American", 0));
        listUserTastebuds.add(new TastebudChipVO("Chinese", 0));
        listUserTastebuds.add(new TastebudChipVO("Thai", 0));
        listUserTastebuds.add(new TastebudChipVO("Continental", 0));
        listUserTastebuds.add(new TastebudChipVO("Indonesian", 0));
        listUserTastebuds.add(new TastebudChipVO("French", 0));
        listUserTastebuds.add(new TastebudChipVO("Indian", 0));
        listUserTastebuds.add(new TastebudChipVO("Italian", 0));
        listUserTastebuds.add(new TastebudChipVO("Vietnamese", 0));
        listUserTastebuds.add(new TastebudChipVO("Singapore", 0));
        listUserTastebuds.add(new TastebudChipVO("Spanish", 0));
        listUserTastebuds.add(new TastebudChipVO("Korean", 0));
        listUserTastebuds.add(new TastebudChipVO("Labanese", 0));
        adapterLayout = new TastebudsChipViewAdapter(this);*/


        /*initLayoutSuggestionsChips();*/

        text_chip_layout.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
                Log.d("clicked", "clicked");
                TastebudChipVO tag = (TastebudChipVO) chip;
                tag.setType(tag.getType() == 0 ? 1 : 0);
                text_chip_layout.refresh();
                for (int i = 0; i < listUserTastebuds.size(); i++) {
                    if (listUserTastebuds.get(i).getText().equalsIgnoreCase(tag.getText())) {
                        listUserTastebuds.remove(i);
                        initLayoutSuggestionsChips();
                    }
                }
            }
        });

    }

    private void initLayoutSuggestionsChips() {

        // Custom layout and background colors
        text_chip_layout.setAdapter(adapterLayout);
        text_chip_layout.setChipLayoutRes(R.layout.chip_close_right);
        text_chip_layout.setChipBackgroundColor(getResources().getColor(R.color.red_bg_chip));
        text_chip_layout.setChipBackgroundColorSelected(getResources().getColor(R.color.white_bg_chip));

        if (listUserTastebuds.size() > 0) {
            ArrayList<Chip> listChips = new ArrayList<>();
            for (int i = 0; i < listUserTastebuds.size(); i++) {
                listChips.add(new TastebudChipVO(listUserTastebuds.get(i).getName().trim(), listUserTastebuds.get(i).getType()));
            }

            text_chip_layout.setChipList(listChips);
            txtViewNoTastebuds.setVisibility(View.GONE);
            text_chip_layout.setVisibility(View.VISIBLE);
        /*text_chip_layout.setChipList(listUserTastebuds);*/
        } else {
            txtViewNoTastebuds.setVisibility(View.VISIBLE);
            text_chip_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO Get tastebuds list of user
        ApiViewProfile();
        sendRequestGetTastebudsForUser();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId())

        {
            case R.id.textViewEditTasteBuds:
                intent = new Intent(this, TasteBudsSuggestionsActivity.class);
                intent.putExtra("isFromEditProfile", true);
                startActivity(intent);
                break;
            case R.id.imageViewEditProfileActivityAddUserImage1:
                if (!imageViewEditProfileActivityAddUserImage1.isSelected()) {
                    Constants.flagImageUpload = 1;
                    imgForUploadSelected = imageViewEditProfileActivityUserImage1;
                    imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage1;
                    openUploadImageFromDeviceIntent();
                } else {
//                    Picasso.with(context)
//                            .load(R.drawable.kaka)
//                            .resize(Constants.convertDpToPixels(160),Constants.convertDpToPixels(160))
//                            .centerCrop()
//                            .placeholder(R.drawable.ic_user_profile_avatar)
//                            .error(R.drawable.ic_user_profile_avatar)
//                            .into(imageViewEditProfileActivityUserImage1);
                    imageViewEditProfileActivityUserImage1.setImageDrawable(null);
                    imageViewEditProfileActivityAddUserImage1.setSelected(false);
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, "");
                    SharedPreferenceUtil.save();

                }
                break;
            case R.id.imageViewEditProfileActivityAddUserImage2:
                if (!imageViewEditProfileActivityAddUserImage2.isSelected()) {
                    imgForUploadSelected = imageViewEditProfileActivityUserImage2;
                    imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage2;
                    Constants.flagImageUpload = 2;
                    openUploadImageFromDeviceIntent();
                } else {
                    imageViewEditProfileActivityUserImage2.setImageDrawable(null);
                    imageViewEditProfileActivityAddUserImage2.setSelected(false);
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, "");
                    SharedPreferenceUtil.save();

                }
                break;
            case R.id.imageViewEditProfileActivityAddUserImage3:
                if (!imageViewEditProfileActivityAddUserImage3.isSelected()) {
                    imgForUploadSelected = imageViewEditProfileActivityUserImage3;
                    imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage3;
                    Constants.flagImageUpload = 3;
                    openUploadImageFromDeviceIntent();
                } else {
                    imageViewEditProfileActivityUserImage3.setImageDrawable(null);
                    imageViewEditProfileActivityAddUserImage3.setSelected(false);
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, "");
                    SharedPreferenceUtil.save();

                }
                break;
            case R.id.imageViewEditProfileActivityAddUserImage4:
                if (!imageViewEditProfileActivityAddUserImage4.isSelected()) {
                    imgForUploadSelected = imageViewEditProfileActivityUserImage4;
                    imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage4;
                    Constants.flagImageUpload = 4;
                    openUploadImageFromDeviceIntent();
                } else {
                    imageViewEditProfileActivityUserImage4.setImageDrawable(null);
                    imageViewEditProfileActivityAddUserImage4.setSelected(false);
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, "");
                    SharedPreferenceUtil.save();

                }
                break;
            case R.id.imageViewEditProfileActivityAddUserImage5:
                if (!imageViewEditProfileActivityAddUserImage5.isSelected()) {
                    imgForUploadSelected = imageViewEditProfileActivityUserImage5;
                    imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage5;
                    Constants.flagImageUpload = 5;
                    openUploadImageFromDeviceIntent();
                } else {
                    imageViewEditProfileActivityUserImage5.setImageDrawable(null);
                    imageViewEditProfileActivityAddUserImage5.setSelected(false);
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, "");
                    SharedPreferenceUtil.save();

                }
            case R.id.btnSaveEditProfile:
                if (isValidFields())
                    ApiEditProfile();
                break;
            default:
                break;

        }
    }

    private boolean isValidFields() {
        if (editTextName.getText().toString().trim().length() == 0) {
            showSnackBarMessageOnly(getString(R.string.first_name_is_required));
            return false;

        } else if (editTextLastName.getText().toString().trim().length() == 0) {
            showSnackBarMessageOnly(getString(R.string.last_name_is_required));
            return false;
        } else
            return true;
    }

    private void openUploadImageFromDeviceIntent() {
//        http://stackoverflow.com/questions/4455558/allow-user-to-select-camera-or-gallery-for-image
        /*// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        root.mkdirs();

        // Generate File name here
        String strDate = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)).format(new Date());

        final String fname = "IMG_" + strDate + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);*/

        String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        Log.d("CAMERA_PICTURE_FILE_NAME", "" + fileName);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        Constants.outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Constants.outputFileUri);
            cameraIntents.add(intent);
        }

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            Intent intent = new Intent(context, FacebookAlbumsListActivity.class);
            Log.i("FAcebookAccesstoken", "Found");
//        intent.setPackage("Facebook");
//        intent.setComponent(new ComponentName("FACEBOOK", "FACEBOOK"));
            cameraIntents.add(intent);
        } else {
            Log.i("FAcebookAccesstoken", "NotFound");
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.upload_image_using));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        // Facebook Image Intent

        startActivityForResult(chooserIntent, Constants.REQUEST_CODE_SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SELECT_PICTURE) {
                FacebookPhotoVO photoFb = null;
                boolean isCamera;
                boolean isFacebook = false;
//                if (data == null) {
                if (data != null && data.hasExtra(Constants.INTENT_PARAM_FB_ALBUM)) {
                    isCamera = false;
                    isFacebook = true;

                    photoFb = (FacebookPhotoVO) data.getSerializableExtra(Constants.INTENT_PARAM_FB_ALBUM);
                } else {
                    isFacebook = false;
                    if (data == null || data.getData() == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }
                }

                Uri selectedImageUri;
                if (isFacebook) {
                    // Update selected image from facebook to local profile object.
                    // Get image url here
                    String imageUrlFb = "";
                    for (int i = 0; i < photoFb.getImages().size(); i++) {
                        if (imageUrlFb.trim().isEmpty()) {
                            imageUrlFb = photoFb.getImages().get(i).getSource().trim();
                            break;
                        } else {
                            break;
                        }
                    }

                    if (imageUrlFb.trim().isEmpty()) {
                        imageUrlFb = photoFb.getPicture().trim();
                    }

                    Log.d("IMAGE_URL_FROM_FACEBOOK", "" + imageUrlFb);

                    if (Constants.flagImageUpload == 1) {
                        setImageFromUrl(imageViewEditProfileActivityUserImage1, imageUrlFb.trim(), imageViewEditProfileActivityAddUserImage1);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 2) {
                        // Image 2
                        setImageFromUrl(imageViewEditProfileActivityUserImage2, imageUrlFb.trim(), imageViewEditProfileActivityAddUserImage2);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 3) {
                        // Image 3
                        setImageFromUrl(imageViewEditProfileActivityUserImage3, imageUrlFb.trim(), imageViewEditProfileActivityAddUserImage3);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 4) {
                        // Image 4
                        setImageFromUrl(imageViewEditProfileActivityUserImage4, imageUrlFb.trim(), imageViewEditProfileActivityAddUserImage4);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 5) {
                        // Image 5
                        setImageFromUrl(imageViewEditProfileActivityUserImage5, imageUrlFb.trim(), imageViewEditProfileActivityAddUserImage5);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, imageUrlFb.trim());
                        SharedPreferenceUtil.save();

                    }

                } else if (isCamera) {
                    selectedImageUri = Constants.outputFileUri;
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Camera result.");
                    String[] projection = {MediaStore.Images.Media.DATA};
                   /* Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null);
                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String picturePath = cursor.getString(column_index_data);*/
                    Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
                    decodeFile(picturePath, 1);

                      /* Bitmap bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath());*/
                    if (bitmapCameraUpload != null && imgForUploadSelected != null) {
                        imgForUploadSelected.setImageBitmap(bitmapCameraUpload);
                        imgAddButtonForUploadSelected.setSelected(true);
                    } else {
                        Log.d("ON_ACTIVITY_RESULT", (bitmapCameraUpload == null ? "Selected image Bitmap found null." : "Selected image view found null to set bitmap."));
                    }

//                    showProgress(getString(R.string.loading));
                    ImageUploadApi(picturePath);
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Gallery result.");
                    try {
                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
                        decodeFile(picturePath, 1);

                         /* Bitmap bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath());*/
                        if (bitmapCameraUpload != null && imgForUploadSelected != null) {
                            imgForUploadSelected.setImageBitmap(bitmapCameraUpload);
                            imgAddButtonForUploadSelected.setSelected(true);
                        } else {
                            Log.d("ON_ACTIVITY_RESULT", (bitmapCameraUpload == null ? "Selected image Bitmap found null." : "Selected image view found null to set " +
                                    "bitmap."));
                        }

//                        showProgress(getString(R.string.loading));
                        ImageUploadApi(picturePath);
//                        callOtherImageUploadAPI(picturePath,Constants.flagImageUpload);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == 102) {

            }
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled Image capture
            showSnackBarMessageOnly(getString(R.string.image_upload_cancelled));
        }
    }

    private void decodeFile(String filePath, int scale) {
        // Decode image size
        try {
            Log.i("DECODE_FILE", "FilePath: " + filePath);
            /*BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, o);*/

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            bitmapCameraUpload = BitmapFactory.decodeFile(filePath, o2);
            /*imageForMyPhotoUpload.setImageBitmap(bitmap);*/
        } catch (Exception e) {
            e.printStackTrace();
            decodeFile(filePath, scale * 2);
//            showToast("Error");
        }
    }

    private void setImageFromUrl(final ImageView imageView, final String imgUrl, final ImageView addButton) {
        if (imgUrl != null && !imgUrl.isEmpty()) {
            Picasso.with(context).load(imgUrl.trim()).resize(Constants.convertDpToPixels(160), Constants.convertDpToPixels(160))
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            addButton.setSelected(true);
                            Log.d(TAG, "success loading restaurant pic for Card: " + imgUrl);
                        }

                        @Override
                        public void onError() {
                            Log.d(TAG, "Error loading restaurant pic for Card: " + imgUrl);
                        }
                    });
        }

    }

    private void ApiViewProfile() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_profile_api).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, EditProfile.this);
    }

    private void ApiEditProfile() {
        String fields = "";
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
        params.put(getString(R.string.api_param_key_first_name), editTextName.getText().toString().trim());
        params.put(getString(R.string.api_param_key_last_name), editTextLastName.getText().toString().trim());
        params.put(getString(R.string.api_param_key_about), editTextAbout.getText().toString().trim());
        params.put(getString(R.string.api_param_key_occupation), editTextOccupation.getText().toString().trim());
        params.put(getString(R.string.api_param_key_ethnicity), editTextEthnicity.getText().toString().trim());
        params.put(getString(R.string.api_param_key_relationship), editTextRelationShipStatus.getText().toString().trim());
        params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
        params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
        params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
        params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
        params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
        params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, ""));
        params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, ""));
        params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, ""));
        params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, ""));

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_about);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_about);

        }
        if (!editTextAbout.getText().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_about), editTextAbout.getText().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_about), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_occupation);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_occupation);
        }
        if (!editTextOccupation.getText().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_occupation), editTextOccupation.getText().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_occupation), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_relationship);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_relationship);
        }
        if (!editTextRelationShipStatus.getText().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_relationship), editTextRelationShipStatus.getText().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_relationship), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_ethnicity);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_ethnicity);
        }
        if (!editTextEthnicity.getText().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_ethnicity), editTextEthnicity.getText().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_ethnicity), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic1);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic1);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic1), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic2);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic2);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic2), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic3);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic3);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic3), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic4);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic4);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic4), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic5);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic5);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic5), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic6);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic6);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic6), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic7);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic7);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic7), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic8);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic8);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic8), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic9);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic9);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic9), "null");
        }

        // TODO Get list of tastebuds id and save it to server in comma separated text
        if (listUserTastebuds.size() > 0) {
            String strTastebuds = "";
            for (int i = 0; i < listUserTastebuds.size(); i++) {
                if (strTastebuds.trim().isEmpty()) {
                    strTastebuds = listUserTastebuds.get(i).getId();
                } else {
                    strTastebuds = strTastebuds + "," + listUserTastebuds.get(i).getId();
                }
            }

            params.put(getString(R.string.api_param_key_testbuds), strTastebuds);
            fields = fields + "," + getString(R.string.api_param_key_testbuds);
        } else {
            params.put(getString(R.string.api_param_key_testbuds), "null");
            fields = fields + "," + getString(R.string.api_param_key_testbuds);
        }

        params.put(getString(R.string.api_param_key_fields), fields);
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, EditProfile.this);
    }

    public String fields() {
        return getString(R.string.api_param_key_first_name) + "," +
                getString(R.string.api_param_key_last_name) + "," +
                getString(R.string.api_param_key_about) + "," +
                getString(R.string.api_param_key_relationship) + "," +
                getString(R.string.api_param_key_ethnicity) + "," +
                getString(R.string.api_response_param_key_profilepic1) + "," +
                getString(R.string.api_response_param_key_profilepic2) + "," +
                getString(R.string.api_response_param_key_profilepic3) + "," +
                getString(R.string.api_response_param_key_profilepic4) + "," +
                getString(R.string.api_response_param_key_profilepic5) + "," +
                getString(R.string.api_param_key_occupation);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (action == Constants.ACTION_CODE_PROFILE || action == Constants.ACTION_CODE_EDIT_PROFILE) {
            Log.i("Response", "" + response);
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        dataInner = data.getJSONObject(i);
                    }
                    if (dataInner != null) {
                        UserProfileAPI profile = new UserProfileAPI(context, dataInner);

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profile.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, profile.is_receive_messages_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, profile.is_receive_invitation_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                        SharedPreferenceUtil.save();
                        showValuesToFields();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();
                    }

                } else {
                    showSnackBarMessageOnly(errormessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (action == Constants.ACTION_CODE_API_GET_USER_TASTEBUDS) {
            Log.d("RESPONSE_TASTEBUDS_USER", "Response: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                if (status) {
                    listUserTastebuds = new ArrayList<>();
                    JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject data = dataArray.getJSONObject(i);
                        TastebudChipVO chip = new TastebudChipVO();
                        chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
                        chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
                        chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
                        chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
                        chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
                        chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                        /*chip.setType(1);*/
                        listUserTastebuds.add(chip);
                            /*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*/
                    }

                    // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
                    initLayoutSuggestionsChips();
                    stopProgress();
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            }
        }
    }

    private void showValuesToFields() {
        editTextName.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, ""));
        editTextLastName.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, ""));
        editTextAbout.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, ""));
        editTextRelationShipStatus.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, ""));
        editTextOccupation.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, ""));
        editTextEthnicity.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, ""));

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            setImageFromUrl(imageViewEditProfileActivityUserImage1, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""), imageViewEditProfileActivityAddUserImage1);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").isEmpty() && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage2, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""), imageViewEditProfileActivityAddUserImage2);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").isEmpty() && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage3, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""), imageViewEditProfileActivityAddUserImage3);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").isEmpty() && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage4, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""), imageViewEditProfileActivityAddUserImage4);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").isEmpty() && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage5, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""), imageViewEditProfileActivityAddUserImage5);
        }

    }

    private interface FileUploader {
        @Multipart
        @POST("/post.php")
        void upload(@Part("action") TypedString action, @Part("userid") TypedString userid, @Part("sessionid") TypedString sessionid, @Part("image") TypedString image,
                    @Part("file") TypedFile photo, retrofit.Callback<Object> callback);
    }

    private void ImageUploadApi(String selectedImagePath) {

        String selectedImage = "";
        try {
            TypedFile photo1 = null, photo2 = null, photo3 = null, photo4 = null, photo5 = null, file = null;
            if (Constants.flagImageUpload == 1) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic1";
            } else if (Constants.flagImageUpload == 2) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic2";
            } else if (Constants.flagImageUpload == 3) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic3";
            } else if (Constants.flagImageUpload == 4) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic4";
            } else if (Constants.flagImageUpload == 5) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic5";
            }else if (Constants.flagImageUpload == 6) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic6";
            }else if (Constants.flagImageUpload == 7) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic7";
            }else if (Constants.flagImageUpload == 8) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic8";
            }else if (Constants.flagImageUpload == 9) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic9";
            } else {
                stopProgress();
                return;
            }

            String strURL = getString(R.string.api_base_url_upload_images_retrofit);
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint((strURL))
                    .setLogLevel(RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("UPLOAD_PHOTO_URL"))
                    .build();

            FileUploader uploader = adapter.create(FileUploader.class);

            TypedString action = new TypedString("uploadimage");
            TypedString userid = new TypedString(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            TypedString sessionid = new TypedString(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            TypedString image = new TypedString(selectedImage);
            uploader.upload(action, userid, sessionid, image, photo1,
                    new retrofit.Callback<Object>() {
                        @Override
                        public void success(Object response, Response response2) {
                            try {

                                String gsonResponse = new Gson().toJson(response);
                                Log.d("GsonResponse", "" + gsonResponse.toString());

                                // Parse response here
                                JSONObject jsonObjectResponse = new JSONObject(gsonResponse);
                                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                                if (status) {
                                    JSONObject data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                            ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_data)) : null;

                                    if (data != null) {
                                        UserProfileAPI profile = new UserProfileAPI(context, data);

                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                                        // Split location and save current latitude & longitude of user.
                                        String[] location = profile.location.trim().split(",");
                                        if (location.length == 2) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                                        }

                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, profile.is_receive_messages_notifications);
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, profile.is_receive_invitation_notifications);
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profile.profilepic6.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profile.profilepic7.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profile.profilepic8.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profile.profilepic9.trim()));
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                                        SharedPreferenceUtil.save();
                                        showValuesToFields();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();

                                    }

                                }

                            } catch (JSONException e) {
                                stopProgress();
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("ERROR", "");
                            stopProgress();
                            error.printStackTrace();

                        }
                    });

        } catch (Exception e) {
            stopProgress();
            e.printStackTrace();
        }
    }

//    private void updatePic(int position, String url) {
//        HashMap<String, String> params = new HashMap<>();
//
//        if (position == 1)
//            params.put(getString(R.string.api_response_param_key_profilepic1), url);
//        else if (position == 2)
//            params.put(getString(R.string.api_response_param_key_profilepic2), url);
//        else if (position == 3)
//            params.put(getString(R.string.api_response_param_key_profilepic3), url);
//        else if (position == 4)
//            params.put(getString(R.string.api_response_param_key_profilepic4), url);
//        else if (position == 5)
//            params.put(getString(R.string.api_response_param_key_profilepic5), url);
//
//        new HttpRequestSingletonPut(EditProfile.this,
//                getString(R.string.api_base_url) + getString(R.string.api_user) + "/" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim(),
//                params, Constants.ACTION_CODE_API_SETTINGS, this);
//    }

    private void sendRequestGetTastebudsForUser() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_mytestbuds));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_GET_USER_TASTEBUDS, EditProfile.this);
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        intent = new Intent(mContext, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        supportFinishAfterTransition();
    }

    protected void showDialogAlertRequestPermissionLocation(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }
}
