package com.friendsoverfood.android.Home;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.baoyz.actionsheet.ActionSheet;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.DaysPickerVO;
import com.friendsoverfood.android.RestaurantsList.RestaurantsListActivity;
import com.friendsoverfood.android.RestaurantsList.TimePickerVO;
import com.friendsoverfood.android.SignIn.TasteBudsSuggestionsActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.friendsoverfood.android.util.EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager;
import com.friendsoverfood.android.util.NetworkUtil;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.Algorithm;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Trushit on 22/04/17.
 */

public class HomeActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult>, OnMapReadyCallback, ClusterManager.OnClusterClickListener<UserClusterItemVO>,
        ClusterManager.OnClusterInfoWindowClickListener<UserClusterItemVO>, ClusterManager.OnClusterItemClickListener<UserClusterItemVO>,
        ClusterManager.OnClusterItemInfoWindowClickListener<UserClusterItemVO>, ActionSheet.ActionSheetListener {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "HOME_ACTIVITY";

    public RecyclerView rvUsers;
    public RelativeLayout relativeHomeRoot;
    public LinearLayoutManager llmRecyclerView;
    public UsersHorizontalRecyclerAdapter adapter;
    private ArrayList<UserClusterItemVO> listUsersNearBy = new ArrayList<>();
    private ArrayList<UserClusterItemVO> listUsersNearByClicked = new ArrayList<>();
    private ImageView imgViewSendRequest;
    public NumberPicker numberPickerDay, numberPickerTime;
    private TextView textViewDatePicker;
    public Button btnDone;
    public Calendar calDateSelected = Calendar.getInstance();
    public LinearLayout linearLayout, linearLayoutTimePicker, linearLayoutDatePicker;
    public ArrayList<DaysPickerVO> listDaysPicker = new ArrayList<>();
    public ArrayList<TimePickerVO> listTimePicker = new ArrayList<>();
    private ArrayList<ChatListVO> FriendList = new ArrayList<>();
    public ImageView imageViewFooterPeopleDownArrow, imageViewFooterDatePickerDownArrow, imgreject, imgaccept;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private ClusterManager<UserClusterItemVO> mClusterManager;
    private ClusterItemRenderer mClusterItemRenderer;
    public int posSelected = 0;
    public View viewCustomActionBar;

    // Search View Variables for Tastebuds & Location
//    public SearchView searchViewTastebuds; // , searchViewLocation;
//    public SearchManager searchManagerTasteBuds;// , searchManagerLocation;
    public TextView txtChangeLocation;
    public RelativeLayout relativeChangeLocation;
    //    public ImageView imgChangeLocationFilterOption;
    //    private PlaceAutocompleteAdapter mAdapter;
    public EditText autoCompleteTextViewChangeLocation, txtSearchTastebuds;
    public LinearLayout linearSearchTastebuds, linearSearchLocation;
    public ImageView imgSearchTastebudsCancel, imgSearchLocationCancel, imgSearchCurrentLocation;
    public ListView listViewGoogleSearchLocation;
    public SearchLocationListAdapter adapterSearchLocation;
    /**
     * Current results returned by google place autocomplete.
     */
    private ArrayList<AutocompletePrediction> mResultList = new ArrayList<>();
    public TextView txtSearchCommon;
    public LinearLayout linearSearchCustom;
    public final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    public LatLng selectedLocationLatLong;
    public RelativeLayout relativeSearchPopup;
    public LinearLayout linearSearchCancel;
    public TextView txtSearch, txtCancel;
    private String placeName = "", strSearchLocationQuery = "";
    public boolean isPickerSelected = false;
    public boolean isfromButton = false;
    public String matchIdNewRequest;
    /**
     * The bounds used for Places Geo Data autocomplete API requests.
     */
    private LatLngBounds mBounds;

    final double HEADING_NORTH_EAST = 45;
    final double HEADING_SOUTH_WEST = 215;
    final double diagonalBoundsSize = 1000; // 1km

    /**
     * The autocomplete filter used to restrict queries to a specific set of place types.
     */
    private AutocompleteFilter mPlaceFilter;

    // Timer for search
    public CountDownTimer timerSearchTastebuds, timerSearchLocation;
    public String strQuerySearchTastebuds = "", strQuerySearchLocation = "";

    // Filter & Map options
    public LinearLayout linearList, linearFilter, linearTastebuds;

    // Variables for Google API Client & Location services.
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    public boolean isLocationReceivedOnce = false;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public int mMaxValue = 100, mMinValue = 0, mMinAge = 40, mMaxAge = 80;
    public int distanceSelected = 0;

    public final int MAX_DISTANCE_RANGE_KM = 160; // 100 Mi. * 1.60934
    public final int MAX_DISTANCE_RANGE_MI = 100; // 1 Km * 0.621371

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    public LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Constant used in the location settings dialog.
     */
    public final int REQUEST_CHECK_SETTINGS = 0x1;

    public double currentLatitude = 0.0, currentLongitude = 0.0, locationChangeLatitude = 0.0, locationChangeLongitude = 0.0;
    public int posToScrollRestaurantList = 0;
    public UserClusterItemVO posSelectedClusterItem = null;
    public Cluster<UserClusterItemVO> posSelectedCluster = null;
    public String age1 = "18";
    public String age2 = "100";
    public String distance1 = "5";
    public String distance2 = "100";

    // Floating action menu variables
    public FloatingActionMenu menuFabListFilter;
    public FloatingActionButton fabList;
    public FloatingActionButton fabFilterHome;
    public ImageView imgGradientFab;

    // Paging Variables for User Suggestions
    public final int COUNT_PAGE_LIMIT_USER_SUGGESTIONS = 20;
    public int countSkipUserSuggestions = 0;
    public boolean isCurrentLocationEnabled = true;
    public int posToGetDistanceMatrix = 0;
    public int posToGetMutualFriends = 0;
    public Dialog dialogMutualFriends;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    // Variable to disable connect of Google API Client onResume method.
    public boolean isDisableConnectOnPause = false;

    // Check if location is updated on screen or not.
    /*public boolean isLocationUpdatedOnce = false;*/

    // Recycler view height variable for setting my location button above recycler view
    public double heightRecyclerViewUsers = 0.0;

    // Objects for Local Broadcast manager & Push notifications handling in the app.
    public boolean mIsReceiverRegistered = false, mIsReceiverRegisteredRequestLocal = false;
    public MyBroadcastReceiver mReceiver, mReceiverRequestLocal;
    public BroadcastReceiver mNotificationReceiver, mNotificationReceiverRequestLocal;

    public final double KM_TO_MILE = 0.621371;
    public final double MILE_TO_KM = 1.60934;


    // Selected card in center in recycler view
    public float firstItemWidthCard;
    public float paddingCard;
    public float itemWidthCard;
    public int allPixelsCard;
    public int finalWidthCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
        setTitleSupportActionBar(getString(R.string.map));
        /*setDrawerMenu();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(3); // Set search icon selected

        /*setDrawerMenu();
        lockDrawerMenu();*/
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_home_activity, getMiddleContent());

        rvUsers = (RecyclerView) view.findViewById(R.id.recyclerViewHomeActivity);
        relativeSearchPopup = (RelativeLayout) view.findViewById(R.id.relativeLayoutHomeActivitySearchPopup);
        listViewGoogleSearchLocation = (ListView) view.findViewById(R.id.listViewGoogleSearchLocation);
        relativeHomeRoot = (RelativeLayout) view.findViewById(R.id.relativeLayoutHomeActivityRoot);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragmentHomeActivity));
//        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragmentRestaurantMapsActivity);
        mapFragment.getMapAsync(this);

        changePositionMyLocationButtomMaps();

        linearList = (LinearLayout) view.findViewById(R.id.linearLayoutHomeActivityList);
        linearTastebuds = (LinearLayout) view.findViewById(R.id.linearLayoutHomeActivityTastebuds);
        linearFilter = (LinearLayout) view.findViewById(R.id.linearLayoutHomeActivityFilter);
        menuFabListFilter = (FloatingActionMenu) view.findViewById(R.id.floatingActionMenuListFilter);
        fabList = (FloatingActionButton) view.findViewById(R.id.fabList);
        fabFilterHome = (FloatingActionButton) view.findViewById(R.id.fabFilterHome);
        imgGradientFab = (ImageView) view.findViewById(R.id.imageViewListActivityGradient);

       /* llmRecyclerView = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvUsers.setLayoutManager(llmRecyclerView);
        rvUsers.setHasFixedSize(false);*/
        setUsersRecyclerViewScrolling();

        // TODO Set custom layout to support action bar here.
//        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_list_activity, null);
        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_maps_activity, linearToolBarCustomLayout);
        linearToolBarCustomLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
//        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setCustomView(viewCustomActionBar);
//        Toolbar parent =(Toolbar) viewCustomActionBar.getParent();
        toolbarLayoutRoot.setPadding(0, 0, 0, 0); //for tab otherwise give space in tab
        toolbarLayoutRoot.setContentInsetsAbsolute(0, 0);
        toolbarLayoutRoot.setContentInsetsRelative(0, 0);
        toolbarLayoutRoot.setContentInsetStartWithNavigation(0);

        linearToolBarCustomLayout.setPadding(0, 0, 0, 0);

        txtTitleToolbarCentered.setVisibility(View.GONE);

//        menuFabListFilter.hideMenuButton(false);
        createCustomAnimation();

        setUpSearchViewCommon();
        setUpSearchViewTastebuds();
        setUpSearchViewLocation();

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            isCurrentLocationEnabled = false;
            String strLocationStringChanged = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, "");
            if (!strLocationStringChanged.trim().isEmpty() && !autoCompleteTextViewChangeLocation.getText().toString().trim().equalsIgnoreCase(strLocationStringChanged)) {
                currentLatitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0")
                        .trim().replaceAll(",", "."));
                currentLongitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0")
                        .trim().replaceAll(",", "."));
                autoCompleteTextViewChangeLocation.setText(strLocationStringChanged.trim());
                strQuerySearchLocation = strLocationStringChanged.trim();
                autoCompleteTextViewChangeLocation.setSelection(strLocationStringChanged.trim().length());
            }
        }

        /*setUpMapOnScreen(false);*/
        setListener();
        setAllTypefaceMontserratRegular(view);

        if (getMap() != null) {
            getMap().clear();
        }
        /*isLocationReceivedOnce = false;*/

        // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationPermissionGrantedOrNot();

        mGoogleApiClient.connect();

        // Moved from onResume() to init()
        registerReceiver(gpsReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            isCurrentLocationEnabled = false;
            String strLocationStringChanged = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, "");
            if (!strLocationStringChanged.trim().isEmpty()
                    && !autoCompleteTextViewChangeLocation.getText().toString().trim().equalsIgnoreCase(strLocationStringChanged)) {
                currentLatitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0")
                        .trim().replaceAll(",", "."));
                currentLongitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0")
                        .trim().replaceAll(",", "."));
                autoCompleteTextViewChangeLocation.setText(strLocationStringChanged.trim());
                strQuerySearchLocation = strLocationStringChanged.trim();
                autoCompleteTextViewChangeLocation.setSelection(strLocationStringChanged.trim().length());
            }

        }

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {*/
        if (txtSearchTastebuds != null) {
            txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
            txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
        }

        if (txtChangeLocation != null) {
            txtChangeLocation.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
        }




        /*}*/
    }

    private void sendRequestUserSuggestions() {
//        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_usersuggestion));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_skip), String.valueOf(countSkipUserSuggestions));
        params.put(getString(R.string.api_param_key_limit), String.valueOf(COUNT_PAGE_LIMIT_USER_SUGGESTIONS));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_USER_SUGGESTIONS, HomeActivity.this);
    }

    private void setUpMapOnScreen(boolean isListToReset) {
        if (getMap() != null) {

            getMap().clear();

//            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));
            Log.d(TAG, "Setup map on screen - Lat: " + currentLatitude + ", Lng: " + currentLongitude);
//            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10.0f));
            /*double distanceFilter = 0.0;
            if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server)).trim()
                    .toLowerCase().contains("mi")) {
                distanceFilter = (MILE_TO_KM * 1000 *
                        Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "").trim().replaceAll(",", ".")))
                        + (2 * MILE_TO_KM * 1000);
                Log.d(TAG, "Calculate zoom level as per the filter distance Meters: " + distanceFilter);
            } else {
                distanceFilter = (1000 *
                        Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "").trim().replaceAll(",", ".")))
                        + (2 * 1000);
                Log.d(TAG, "Calculate zoom level as per the filter distance zoom level Meters: " + distanceFilter);
            }
            getMap().moveCamera(getZoomForDistance(new LatLng(currentLatitude, currentLongitude), distanceFilter));*/
            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10.0f));

            if (isListToReset) {
                posToGetDistanceMatrix = 0;
                if (adapter != null) {
                    int sizeBefore = adapter.getItemCount();
                }
                /*listUsersNearBy.clear();*/
                /*if (adapter != null) {
                    adapter.notifyItemRangeRemoved(0, sizeBefore);
                    adapter.notifyDataSetChanged();
                }*/

                mClusterManager = new ClusterManager<UserClusterItemVO>(this, getMap());
                mClusterItemRenderer = new ClusterItemRenderer();
                mClusterManager.setRenderer(mClusterItemRenderer);
                getMap().setOnCameraIdleListener(mClusterManager);
                getMap().setOnMarkerClickListener(mClusterManager);
                getMap().setOnInfoWindowClickListener(mClusterManager);
                mClusterManager.setOnClusterClickListener(this);
//                mClusterManager.setOnClusterInfoWindowClickListener(this);
                mClusterManager.setOnClusterItemClickListener(this);
//                mClusterManager.setOnClusterItemInfoWindowClickListener(this);

               /* mClusterManager.getClusterMarkerCollection().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return false;
                    }
                });

                mClusterManager.getMarkerCollection().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return false;
                    }
                });*/

                getMap().setOnCameraIdleListener(mClusterManager);

                if (isListToReset) {
                    countSkipUserSuggestions = 0;
                }
                sendRequestUserSuggestions();
//                try {
//                    InputStream inputStream = getResources().openRawResource(R.raw.radar_search);
//                    List<UserClusterItemVO> items = new MyUsersReader().read(inputStream);
//                    for (int i = 0; i < 10; i++) {
//                        double offset = i / 60d;
//                        for (UserClusterItemVO item : items) {
//                            listUsersNearBy.add(item);
//                        }
//                    }
//
//                    mClusterManager.addItems(listUsersNearBy);
//                    mClusterManager.cluster();
//
////                    // Set Recycler view adapter here.
////                    if (adapter != null) {
////                        adapter.notifyItemRangeInserted(0, listUsersNearBy.size());
////                        adapter.notifyDataSetChanged();
////                    } else {
////                        adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearBy);
////                        rvUsers.setAdapter(adapter);
////                    }
////                    setScrollListenerFriendsList();
//                } catch (JSONException e) {
//            /*Toast.makeText(this, "Problem reading list of markers.", Toast.LENGTH_LONG).show();*/
//                    e.printStackTrace();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        }
    }

    private void setListener() {
        linearFilter.setOnClickListener(this);
        linearList.setOnClickListener(this);
        linearTastebuds.setOnClickListener(this);
        fabList.setOnClickListener(this);
        fabFilterHome.setOnClickListener(this);
        imgGradientFab.setOnClickListener(this);

//        searchViewTastebuds.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(final String newText) {
////                Log.i("SEARCH_QUERY_STRING", "" + newText);
//                if (timerSearchTastebuds != null) {
//                    timerSearchTastebuds.cancel();
//                }
//
//                timerSearchTastebuds = new CountDownTimer(1000, 500) {
//                    @Override
//                    public void onTick(long millisUntilFinished) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        if (!newText.trim().isEmpty()) {
//                            if (!newText.trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
//                                strQuerySearchTastebuds = newText.trim();
//                            }
//
//                            Log.d(TAG, "Tastebuds Search timer finished. Text: " + newText.trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());
//
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                            // TODO Add all the matching query items to search list from original list.
//
//                            // Begin search here.
//                            /*if (listViewSearch != null) {
//                                listViewSearch.setVisibility(View.GONE);
//                            }
//
//                            if (txtNoResults != null) {
//                                txtNoResults.setVisibility(View.GONE);
//                            }
//
//                            if (progressBarLoading != null) {
//                                progressBarLoading.setVisibility(View.VISIBLE);
//                            }*/
//                        } else {
//                            strQuerySearchTastebuds = "";
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                        }
//                    }
//                };
//                timerSearchTastebuds.start();
//                return true;
//            }
//        });

//        searchViewLocation.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(final String newText) {
////                Log.i("SEARCH_QUERY_STRING", "" + newText);
//                if (timerSearchLocation != null) {
//                    timerSearchLocation.cancel();
//                }
//
//                timerSearchLocation = new CountDownTimer(1000, 500) {
//                    @Override
//                    public void onTick(long millisUntilFinished) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        if (!newText.trim().isEmpty()) {
//                            if (!newText.trim().equalsIgnoreCase(strQuerySearchLocation.trim())) {
//                                strQuerySearchLocation = newText.trim();
//                            }
//
//                            Log.d(TAG, "Location Search timer finished. Text: " + newText.trim() + ", SearchQuery: " + strQuerySearchLocation.trim());
//
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                            // TODO Add all the matching query items to search list from original list.
//
//                            // Begin search here.
//                            /*if (listViewSearch != null) {
//                                listViewSearch.setVisibility(View.GONE);
//                            }
//
//                            if (txtNoResults != null) {
//                                txtNoResults.setVisibility(View.GONE);
//                            }
//
//                            if (progressBarLoading != null) {
//                                progressBarLoading.setVisibility(View.VISIBLE);
//                            }*/
//                        } else {
//                            strQuerySearchLocation = "";
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                        }
//                    }
//                };
//                timerSearchLocation.start();
//                return true;
//            }
//        });
    }

    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (menuFabListFilter.isOpened()) {
                    imgGradientFab.setVisibility(View.VISIBLE);
                } else {
                    imgGradientFab.setVisibility(View.GONE);
                }

                menuFabListFilter.getMenuIconView().setImageResource(menuFabListFilter.isOpened()
                        ? R.drawable.ic_close_white : R.drawable.selector_app_icon_fab_white_red_selected_small);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        menuFabListFilter.setIconToggleAnimatorSet(set);
    }

    public void setScrollListenerFriendsList() {
        // Scroll listener to call paging at the end of scroll
        rvUsers.addOnScrollListener(new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerView) {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                synchronized (this) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        calculatePositionAndScrollCard(recyclerView);
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                allPixelsCard += dx;
            }

            @Override
            public void onLoadMore(int lastItemScrolled) {
                Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);

            }

            @Override
            public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
                Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount + ", First Item Details ==> "
                        + " Name: " + adapter.getItem(firstVisibleItem).getFirstname() + " " + adapter.getItem(firstVisibleItem).getLastname()
                        + ", Location: " + adapter.getItem(firstVisibleItem).getLocationString());
                if (firstVisibleItem != -1) {
                    posToScrollRestaurantList = firstVisibleItem;

                    /** ======== Trushit ==========  **/
                    /** Instead of checking posInList, compare user id for exact accuracy, as the list is updating on every cluster click. So posInList
                     *  might be incorrect. **/
//                    if (posSelectedClusterItem != null && posToScrollRestaurantList != posSelectedClusterItem.getPosInList()) {
                    if (posSelectedClusterItem != null &&
                            !adapter.getItem(posToScrollRestaurantList).getId().trim().equalsIgnoreCase(posSelectedClusterItem.getId().trim())) {
                        // TODO deselect previous item selected.
                        if (posSelectedClusterItem != null) {
                            if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                                listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(false);
                            }
                        }

                        if (posSelectedClusterItem != null) {
                            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
                                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedCluster),
                                        mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
                            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
                                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
                            }
                        }

                        // TODO Check if current list item selected is under a cluster or an individual cluster item.
                        posSelectedCluster = null;
                        boolean isClusterOrItemFound = false;
                        Log.d(TAG, "Markers list size: " + mClusterManager.getClusterMarkerCollection().getMarkers().size());
                        for (Marker marker : mClusterManager.getClusterMarkerCollection().getMarkers()) {
                            Log.d(TAG, "Marker - Snippet: " + marker.getSnippet());
                            Cluster<UserClusterItemVO> cluster = mClusterItemRenderer.getCluster(marker);
//                        Log.d(TAG, "Cluster from marker: " + cluster);

                            if (cluster != null) {
                                for (UserClusterItemVO item : cluster.getItems()) {
//                                    if (posToScrollRestaurantList == item.getPosInList()) {

                                    /** ======== Trushit ==========  **/
                                    /** Instead of checking posInList, compare user id for exact accuracy, as the list is updating on every cluster click. So posInList
                                     *  might be incorrect. **/
//                                    if (adapter.getItem(posToScrollRestaurantList).getPosInList() == item.getPosInList() && !isClusterOrItemFound) {
                                    if (adapter.getItem(posToScrollRestaurantList).getId().trim().equalsIgnoreCase(item.getId()) && !isClusterOrItemFound) {
//                                        Log.d(TAG, "Marker cluster item matched pos in list: " + item.getPosInList()
//                                                + ", Item scrolled: " + adapter.getItem(posToScrollRestaurantList).getPosInList());
                                        Log.d(TAG, "Marker cluster item matched user id in list: " + item.getId()
                                                + ", Item scrolled user id: " + adapter.getItem(posToScrollRestaurantList).getId());
                                        isClusterOrItemFound = true;
                                        posSelectedCluster = cluster;
                                        posSelectedClusterItem = item;
                                    }
                                }
                            }
                        }

                        // Cluster items fetching implemented by this link:
                        // https://stackoverflow.com/questions/22449438/google-maps-utility-how-to-get-all-markers-from-clustermanager
                        try {
                            Field field = mClusterManager.getClass().getDeclaredField("mAlgorithm");
                            field.setAccessible(true);
                            Object mAlgorithm = field.get(mClusterManager);
                            List<UserClusterItemVO> markers = new ArrayList<>(((Algorithm<UserClusterItemVO>) mAlgorithm).getItems());

                            for (int x = 0; x < markers.size(); x++) {
//                                    if (posToScrollRestaurantList == item.getPosInList()) {
                                /** ======== Trushit ==========  **/
                                /** Instead of checking posInList, compare user id for exact accuracy, as the list is updating on every cluster click. So posInList
                                 *  might be incorrect. **/
//                                if (adapter.getItem(posToScrollRestaurantList).getPosInList() == markers.get(x).getPosInList() && !isClusterOrItemFound) {
                                if (adapter.getItem(posToScrollRestaurantList).getId().trim().equalsIgnoreCase(markers.get(x).getId().trim()) && !isClusterOrItemFound) {
//                                    Log.d(TAG, "Marker individual cluster item matched pos in list: " + markers.get(x).getPosInList()
//                                            + ", Item scrolled: " + adapter.getItem(posToScrollRestaurantList).getPosInList()
//                                            + ", First Item Details ==> Location: " + adapter.getItem(posToScrollRestaurantList).getLocationString()
//                                            + ", Name: " + adapter.getItem(posToScrollRestaurantList).getFirstname()
//                                            + " " + adapter.getItem(posToScrollRestaurantList).getLastname());
                                    Log.d(TAG, "Marker individual cluster item matched user id: " + markers.get(x).getId()
                                            + ", Item scrolled user id: " + adapter.getItem(posToScrollRestaurantList).getId()
                                            + ", First Item Details ==> Location: " + adapter.getItem(posToScrollRestaurantList).getLocationString()
                                            + ", Name: " + adapter.getItem(posToScrollRestaurantList).getFirstname()
                                            + " " + adapter.getItem(posToScrollRestaurantList).getLastname());
                                    isClusterOrItemFound = true;
//                                    posSelectedCluster = markers.get(x).;
                                    posSelectedClusterItem = markers.get(x);
                                }
                            }
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        posSelectedClusterItem = listUsersNearBy.get(posToScrollRestaurantList);
//                        posSelectedClusterItem = listUsersNearBy.get(posToScrollRestaurantList);

                        // TODO Set cluster or cluster item as selected
                        if (posSelectedClusterItem != null) {
                            if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                                listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(true);
                            }
                        }

                        if (posSelectedClusterItem != null) {
                            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
                                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedCluster),
                                        mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
                            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
                                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
                            }
                        }

                        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items nside of bounds, then animate to center of the bounds.
                        // Create the builder to collect all essential cluster items for the bounds.
                        LatLngBounds.Builder builder = LatLngBounds.builder();
                        builder.include(posSelectedClusterItem.getPosition());

                        // Get the LatLngBounds
                        final LatLngBounds bounds = builder.build();

                        // Animate camera to the bounds
                        try {
                            if (getMap() != null) {
                                /** TRUSHIT - Map zoom level settings to zoom out slowly **/
                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(posSelectedClusterItem.getPosition())      // Sets the center of the map to Mountain View
                                        .zoom(getMap().getCameraPosition().zoom)                   // Sets the zoom
                                        .bearing(getMap().getCameraPosition().bearing)                // Sets the orientation of the camera to east
                                        .tilt(getMap().getCameraPosition().tilt)                   // Sets the tilt of the camera to 30 degrees
                                        .build();                   // Creates a CameraPosition from the builder
                                getMap().animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                                getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

               /* if (visibleItemCount > 1) {
                    Log.d("SCROLL_MANUAL", "Scrolling to position: " + posToScrollRestaurantList);
                    llmRecyclerView.smoothScrollToPosition(rvRestaurants, null, posToScrollRestaurantList);
                    llmRecyclerView.scrollToPosition(posToScrollRestaurantList);
                    *//*rvRestaurants.smoothScrollToPosition(posToScrollRestaurantList);*//*
                }*/
            }

            @Override
            public void onLoadMoreTopItems() {
            }
        });
    }

    private void setUpSearchViewTastebuds() {
        /*searchViewTastebuds = (SearchView) viewCustomActionBar.findViewById(R.id.searchViewCustomToolbarRestaurantMapsActivityTasteBuds);
        searchViewTastebuds.setVisibility(View.VISIBLE);
        searchViewTastebuds.requestFocusFromTouch();
        searchViewTastebuds.setActivated(true);
        searchViewTastebuds.onActionViewExpanded();
        searchViewTastebuds.setIconifiedByDefault(false);
//        searchViewSuggestions.setFocusable(false);
        searchViewTastebuds.setIconified(false);
        searchViewTastebuds.clearFocus();
        searchViewTastebuds.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search) + "</font>"));

        searchManagerTasteBuds = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchManagerLocation = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        // TODO Change text color of search view
        EditText searchEditText = (EditText) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHint(getString(R.string.tastebuds_search_hint));
        searchEditText.setTextSize(14);
        searchEditText.setPadding(0, 0, 0, 0);
        searchEditText.setTextColor(getResources().getColor(R.color.white_selector));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white_selector));

        // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchEditText, R.drawable.shape_edittext_cursor_green);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Change search magnification icons here.
        ImageView magImage = (ImageView) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        magImage.setImageResource(R.drawable.ic_search_edittext_white);

        // TODO Customize search view close button here
        ImageView closeImage = (ImageView) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
//        closeImage.setBackgroundResource(R.drawable.desired_icon);
        closeImage.setColorFilter(getResources().getColor(R.color.white_selector));

        // TODO Set bottom line to search view for all OS versions.
//        searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_src_text).setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
//        searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_src_text).getBackground()
//                .setColorFilter(getResources().getColor(R.color.transparent), PorterDuff.Mode.SRC_ATOP);
        searchViewTastebuds.setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
        searchViewTastebuds.getBackground().setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);

        searchViewTastebuds.setSearchableInfo(searchManagerTasteBuds.getSearchableInfo(getComponentName()));
        setAllTypefaceMontserratLight(searchEditText);

        searchViewTastebuds.requestFocusFromTouch();

        searchViewTastebuds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);
            }
        });*/

        txtCancel = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityTastebudsCancel);
        txtSearch = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityTastebudsSearch);

        txtSearchTastebuds = (EditText) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityTastebuds);
        linearSearchTastebuds = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarUserMapsActivityTastebuds);
        imgSearchTastebudsCancel = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarUserMapsActivityTastebudsCancel);

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {*/
        txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
        txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
        /*}*/

        txtSearchTastebuds.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
                    imgSearchTastebudsCancel.setVisibility(View.VISIBLE);
                } else {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                    SharedPreferenceUtil.save();

                    imgSearchTastebudsCancel.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtSearchTastebuds.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getAction() == KeyEvent.ACTION_DOWN))
                        || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    Log.d(TAG, "Keyboard done button pressed.");
                    /*editTextLastName.requestFocus();*/

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    String s = txtSearchTastebuds.getText().toString().trim();
                    if (!s.trim().isEmpty()) {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, s.trim());
                        SharedPreferenceUtil.save();

                        intent = new Intent(context, RestaurantsListActivity.class);
                        intent.putExtra("querySearchRestaurants", s.toString().trim());
                        startActivity(intent);
                    } else {
                        relativeSearchPopup.setVisibility(View.GONE);
                        linearSearchLocation.setVisibility(View.GONE);
                        linearSearchCancel.setVisibility(View.GONE);
                        linearSearchTastebuds.setVisibility(View.GONE);
                        /*txtChangeLocation.setVisibility(View.VISIBLE);*/
                        relativeChangeLocation.setVisibility(View.VISIBLE);

                        if (menuFabListFilter.isOpened()) {
                            menuFabListFilter.close(true);
                        }
                    }
                }
                return false;
            }
        });

        imgSearchTastebudsCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearchTastebuds.setText("");
            }
        });

        /*txtSearchTastebuds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);
            }
        });*/
    }

    private void setUpSearchViewCommon() {
        txtSearchCommon = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityCommon);
        linearSearchCustom = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarUserMapsActivitySearch);

        /*txtSearchCommon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        txtSearchCommon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txtSearchCommon.getRight() - txtSearchCommon.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // Open filter screen here.
                        Log.d(TAG, "Filter clicked.");

                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void setUpSearchViewLocation() {
        LatLng centre = new LatLng(85, -180);

        LatLng northEast = SphericalUtil.computeOffset(centre, diagonalBoundsSize / 2, HEADING_NORTH_EAST);
        LatLng southWest = SphericalUtil.computeOffset(centre, diagonalBoundsSize / 2, HEADING_SOUTH_WEST);
        mBounds = new LatLngBounds(southWest, northEast);
//        txtChangeLocation = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityChangeLocation);
        linearSearchCancel = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarUserMapsActivitySearchCancel);
        linearSearchLocation = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarUserMapsActivityLocation);
        imgSearchLocationCancel = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarUserMapsActivityLocationCancel);
        imgSearchCurrentLocation = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarMapsActivityCurrentLocation);

        txtChangeLocation = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityChangeLocation);
        relativeChangeLocation = (RelativeLayout) viewCustomActionBar.findViewById(R.id.relativeLayoutCustomToolbarUserMapsActivityChangeLocation);
//        imgChangeLocationFilterOption = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarUserMapsActivityFilter);
        autoCompleteTextViewChangeLocation = (EditText) viewCustomActionBar.findViewById(R.id.autoCompleteTextViewCustomToolbarUserMapsActivityChangeLocation);

//        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {
        txtChangeLocation.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
//        }

//        txtChangeLocation.setOnClickListener(new View.OnClickListener() {
        relativeChangeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*try {*/
                    /*Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(HomeActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);*/
                /*txtChangeLocation.setVisibility(View.GONE);*/
                relativeChangeLocation.setVisibility(View.GONE);
                linearSearchCancel.setVisibility(View.VISIBLE);
                linearSearchTastebuds.setVisibility(View.VISIBLE);
                linearSearchLocation.setVisibility(View.VISIBLE);
                autoCompleteTextViewChangeLocation.requestFocus();
                relativeSearchPopup.setVisibility(View.VISIBLE);
                /*} catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }*/
            }
        });

//        imgChangeLocationFilterOption.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialogFilterOptionsMap();
//            }
//        });

        relativeSearchPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);
                linearSearchLocation.setVisibility(View.GONE);
                linearSearchCancel.setVisibility(View.GONE);
                linearSearchTastebuds.setVisibility(View.GONE);
//                txtChangeLocation.setVisibility(View.VISIBLE);
                relativeChangeLocation.setVisibility(View.VISIBLE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });

        autoCompleteTextViewChangeLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "TEXT CHANGED: " + s.toString().trim() + ", Query: " + strSearchLocationQuery);
                if (!s.toString().trim().isEmpty() && !s.toString().trim().equalsIgnoreCase(strSearchLocationQuery)) {
                    imgSearchLocationCancel.setVisibility(View.VISIBLE);
                    strSearchLocationQuery = s.toString().trim();
                    /*// do something long
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            mResultList = getAutocomplete(strSearchLocationQuery);
                            Handler handler = new Handler();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(TAG, "Notify Location search change. List size: " + TextUtils.join(", ", mResultList));
                                    if (mResultList != null && mResultList.size() > 0) {
                                        if (adapterSearchLocation != null) {
                                            adapterSearchLocation.notifyDataSetChanged();
                                        } else {
                                            adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                                            listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                                        }
                                    } else {
                                        if (adapterSearchLocation != null) {
                                            adapterSearchLocation.notifyDataSetChanged();
                                        } else {
                                            if (mResultList != null) {
                                                adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                                                listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    };
                    new Thread(runnable).start();*/
                    new GetPlacesResultsInBackground().execute(strSearchLocationQuery);
                } else if (s.toString().trim().isEmpty()) {
                    imgSearchLocationCancel.setVisibility(View.GONE);
                    if (mResultList != null) {
                        mResultList.clear();
                        if (adapterSearchLocation != null) {
                            adapterSearchLocation.notifyDataSetChanged();
                        } else {
                            adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                            listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                        }
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        autoCompleteTextViewChangeLocation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getAction() == KeyEvent.ACTION_DOWN))
                        || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    Log.d(TAG, "Keyboard done button pressed.");
                    /*editTextLastName.requestFocus();*/

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    String s = autoCompleteTextViewChangeLocation.getText().toString().trim();
                    Log.d(TAG, "Query for location: " + s.trim());

                    if (!s.toString().trim().isEmpty() && !s.toString().trim().equalsIgnoreCase(strSearchLocationQuery)) {
                        imgSearchLocationCancel.setVisibility(View.VISIBLE);
                        strSearchLocationQuery = s.toString().trim();
                        new GetPlacesResultsInBackground().execute(strSearchLocationQuery);
                    } else if (s.toString().trim().isEmpty()) {
                        imgSearchLocationCancel.setVisibility(View.GONE);
                        if (mResultList != null) {
                            mResultList.clear();
                            if (adapterSearchLocation != null) {
                                adapterSearchLocation.notifyDataSetChanged();
                            } else {
                                adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                                listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                            }
                        }

                        relativeSearchPopup.setVisibility(View.GONE);
                        linearSearchLocation.setVisibility(View.GONE);
                        linearSearchCancel.setVisibility(View.GONE);
                        linearSearchTastebuds.setVisibility(View.GONE);
//                        txtChangeLocation.setVisibility(View.VISIBLE);
                        relativeChangeLocation.setVisibility(View.VISIBLE);

                        if (menuFabListFilter.isOpened()) {
                            menuFabListFilter.close(true);
                        }
                    }
                }
                return false;
            }
        });

        imgSearchLocationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strSearchLocationQuery = "";
                autoCompleteTextViewChangeLocation.setText("");
                autoCompleteTextViewChangeLocation.setSelection(autoCompleteTextViewChangeLocation.getText().toString().trim().length());
            }
        });

        imgSearchCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCurrentLocationEnabled = true;
                startLocationUpdates();

                currentLatitude = locationChangeLatitude;
                currentLongitude = locationChangeLongitude;

                isCurrentLocationEnabled = true;

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.save();

                getFullAddressForCurrentLocation(currentLatitude, currentLongitude, true);
            }
        });

        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                String s = txtSearchTastebuds.getText().toString().trim();
                if (!s.trim().isEmpty()) {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, s.trim());
                    SharedPreferenceUtil.save();

                    intent = new Intent(context, RestaurantsListActivity.class);
                    intent.putExtra("querySearchRestaurants", s.toString().trim());
                    startActivity(intent);
                }
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);
                linearSearchLocation.setVisibility(View.GONE);
                linearSearchCancel.setVisibility(View.GONE);
                linearSearchTastebuds.setVisibility(View.GONE);
//                txtChangeLocation.setVisibility(View.VISIBLE);
                relativeChangeLocation.setVisibility(View.VISIBLE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });


        /*searchViewLocation = (SearchView) viewCustomActionBar.findViewById(R.id.searchViewCustomToolbarRestaurantMapsActivityLocation);
        searchViewLocation.setVisibility(View.VISIBLE);
        searchViewLocation.requestFocusFromTouch();
        searchViewLocation.setActivated(true);
        searchViewLocation.onActionViewExpanded();
        searchViewLocation.setIconifiedByDefault(false);
//        searchViewSuggestions.setFocusable(false);
        searchViewLocation.setIconified(false);
        searchViewLocation.clearFocus();
        searchViewLocation.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search) + "</font>"));

        searchManagerLocation = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        // TODO Change text color of search view
        EditText searchEditTextLocation = (EditText) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditTextLocation.setHint(getString(R.string.current_location));
//        searchEditTextLocation.setText(getString(R.string.current_location));
        searchEditTextLocation.setTextSize(14);
        searchEditTextLocation.setPadding(0, 0, 0, 0);
        searchEditTextLocation.setTextColor(getResources().getColor(R.color.white_selector));
        searchEditTextLocation.setHintTextColor(getResources().getColor(R.color.white_selector));

        // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchEditTextLocation, R.drawable.shape_edittext_cursor_green);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Change search magnification icons here.
        ImageView magImage = (ImageView) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        magImage.setImageResource(R.drawable.ic_location_white_small);

        // TODO Customize search view close button here
        ImageView closeImage = (ImageView) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
//        closeImage.setBackgroundResource(R.drawable.desired_icon);
        closeImage.setColorFilter(getResources().getColor(R.color.white_selector));

        // TODO Set bottom line to search view for all OS versions.
//        searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_src_text).setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);

        searchViewLocation.setSearchableInfo(searchManagerLocation.getSearchableInfo(getComponentName()));
        setAllTypefaceMontserratLight(searchEditTextLocation);*/
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {

        switch (index) {
            case 0:
//                Send friend request
                sendFriendRequestToUserSuggestion();
                break;
            case 1:
                showTimePickerDialog();
                break;
            default:
                break;

        }

    }

    public void sendFriendRequestToUserSuggestion() {
        showProgress(context.getResources().getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_sendfriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), adapter.getItem(posSelected).getId().trim());

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST, HomeActivity.this);
    }

    private void showTimePickerDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(
                Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);//set height(300) and width(match_parent) here, ie (width,height)

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dislogView = inflater.inflate(R.layout.layout_dialog_date_time_picker, null);
        numberPickerDay = (NumberPicker) dislogView.findViewById(R.id.numberPickerDay);
        numberPickerTime = (NumberPicker) dislogView.findViewById(R.id.numberPickerTime);
        btnDone = (Button) dislogView.findViewById(R.id.btnDone);
        linearLayout = (LinearLayout) dislogView.findViewById(R.id.linearLayout);
        linearLayoutDatePicker = (LinearLayout) dislogView.findViewById(R.id.linearLayoutDatePicker);
        linearLayoutTimePicker = (LinearLayout) dislogView.findViewById(R.id.linearLayoutTimePicker);
        imageViewFooterDatePickerDownArrow = (ImageView) dislogView.findViewById(R.id.imageViewFooterDatePickerDownArrow);
        imageViewFooterPeopleDownArrow = (ImageView) dislogView.findViewById(R.id.imageViewFooterPeopleDownArrow);
        textViewDatePicker = (TextView) dislogView.findViewById(R.id.textViewDatePicker);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        setPickerForDay();

        dialog.setContentView(dislogView, dialogParams);
        dialog.show();
    }

    public void setPickerForTime() {
        Calendar calToday = Calendar.getInstance();
        int posSelectedTime = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar calStartTime = Calendar.getInstance();
        calStartTime.setTime(calDateSelected.getTime());
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR) && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) {
            int currentMinute = calStartTime.get(Calendar.MINUTE);
            int diffToAdd = 30 - (currentMinute % 30);

            calStartTime.add(Calendar.MINUTE, diffToAdd);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        } else {
            calStartTime.set(Calendar.HOUR_OF_DAY, 0);
            calStartTime.set(Calendar.MINUTE, 0);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        }

        Calendar calEndTime = Calendar.getInstance();
        calEndTime.setTime(calDateSelected.getTime());
        calEndTime.set(Calendar.HOUR_OF_DAY, 23);
        calEndTime.set(Calendar.MINUTE, 59);
        calEndTime.set(Calendar.SECOND, 59);
        calEndTime.set(Calendar.MILLISECOND, 999);

        Log.d(TAG, "Start time: " + calStartTime.getTime() + ", End time: " + calEndTime.getTime());

        // Clear list.
        listTimePicker.clear();
        numberPickerTime.setDisplayedValues(null);
        while (calStartTime.getTimeInMillis() <= calEndTime.getTimeInMillis()) {
            String strTime = new SimpleDateFormat("hh:mm aa").format(calStartTime.getTime());
            Log.d(TAG, "Time to add: " + strTime + ", Date: " + calStartTime.getTime());
            TimePickerVO obj = new TimePickerVO();
            obj.setCalendarTime(calStartTime);
            obj.setStrTime(strTime.trim());
            listTimePicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calStartTime.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calStartTime.get(Calendar.DAY_OF_YEAR)
                        && calDateSelected.get(Calendar.HOUR_OF_DAY) == calStartTime.get(Calendar.HOUR_OF_DAY)
                        && calDateSelected.get(Calendar.MINUTE) == calStartTime.get(Calendar.MINUTE)) {
                    posSelectedTime = listTimePicker.size() - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                calStartTime.add(Calendar.MINUTE, 30);
            }
        }

        String[] strDays = new String[listTimePicker.size()];
        for (int i = 0; i < listTimePicker.size(); i++) {
            strDays[i] = listTimePicker.get(i).getStrTime().trim();
        }

        numberPickerTime.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerTime.setMaxValue(strDays.length - 1);
            numberPickerTime.setWrapSelectorWheel(false);
            numberPickerTime.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerTime);
            numberPickerTime.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    calDateSelected.setTime(listTimePicker.get(newVal).getCalendarTime().getTime());
                }
            });
            numberPickerTime.setValue(posSelectedTime);
        }
    }

    public void setPickerForDay() {
        int posSelectedDay = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Prepare days list here. Days list will be only for a week.
        listDaysPicker.clear();
        numberPickerDay.setDisplayedValues(null);
        for (int i = 0; i < 7; i++) {
            Calendar calDay = Calendar.getInstance();
            calDay.add(Calendar.DAY_OF_YEAR, i);
            String strDay = new SimpleDateFormat("EEEE").format(calDay.getTime());
            Log.d(TAG, "Day to add: " + strDay + ", Date: " + calDay.getTime());
            DaysPickerVO obj = new DaysPickerVO();
            obj.setCalendarDay(calDay);
            if (i == 0) {
                obj.setStrDay(getString(R.string.today));
            } else if (i == 1) {
                obj.setStrDay(getString(R.string.tomorrow));
            } else {
                obj.setStrDay(strDay.trim());
            }
            listDaysPicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calDay.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calDay.get(Calendar.DAY_OF_YEAR)) {
                    posSelectedDay = i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listDaysPicker.size()];
        for (int i = 0; i < listDaysPicker.size(); i++) {
            strDays[i] = listDaysPicker.get(i).getStrDay().trim();
        }

        numberPickerDay.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerDay.setMaxValue(strDays.length - 1);
            numberPickerDay.setWrapSelectorWheel(false);
            numberPickerDay.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerDay);

            // TODO Set time picker here as 1st position is selected by default for picker.
            if (!isFromIntent) {
                calDateSelected.setTime(listDaysPicker.get(0).getCalendarDay().getTime());
            }
            setPickerForTime();
            updateDateTitleSelectedFromPicker();

            numberPickerDay.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    // TODO Set time picker here depending on date selected.
                    Log.d(TAG, "Day selected at pos " + newVal + " is: " + listDaysPicker.get(newVal).getCalendarDay().getTime());
                    calDateSelected.setTime(listDaysPicker.get(newVal).getCalendarDay().getTime());
                    updateDateTitleSelectedFromPicker();
                    setPickerForTime();
                }
            });

            numberPickerDay.setValue(posSelectedDay);
        }
    }

    public void updateDateTitleSelectedFromPicker() {
        // TODO Set Selected date in action bar
        Calendar calToday = Calendar.getInstance();
        int selectedDayOfYearCalendar = calDateSelected.get(Calendar.DAY_OF_YEAR);
        int tomorrow = calToday.get(Calendar.DAY_OF_YEAR) + 1;
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) { // Selected date is of today
            textViewDatePicker.setText(getString(R.string.today));
        } else if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && tomorrow == selectedDayOfYearCalendar) {
            textViewDatePicker.setText(getString(R.string.tomorrow));
        } else {
            String dayOfMonth = String.valueOf(calDateSelected.get(Calendar.DAY_OF_MONTH));
            int day = calDateSelected.get(Calendar.DAY_OF_MONTH);
            String charAtLastPosition = dayOfMonth.substring(dayOfMonth.length() - 1);
            if (Integer.parseInt(charAtLastPosition) == 1 && day != 11) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>st</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 2 && day != 12) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>nd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 3 && day != 13) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>rd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>th</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            }
        }
    }

    protected class GetPlacesResultsInBackground extends AsyncTask<String, Void, ArrayList<AutocompletePrediction>> {

        protected ArrayList<AutocompletePrediction> doInBackground(String... urls) {
            try {
                String strSearhLocationQuery = urls[0].trim();
                ArrayList<AutocompletePrediction> mResultList = getAutocomplete(strSearchLocationQuery);
                return mResultList;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(ArrayList<AutocompletePrediction> result) {
            if (result != null) {
                Log.d(TAG, "Notify Location search change. List size: " + TextUtils.join(", ", mResultList));
                if (mResultList != null) {
                    mResultList.clear();
                    mResultList.addAll(result);
                }

                if (mResultList != null && mResultList.size() > 0) {
                    if (adapterSearchLocation != null) {
                        adapterSearchLocation.notifyDataSetChanged();
                    } else {
                        adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                        listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                    }
                } else {
                    if (adapterSearchLocation != null) {
                        adapterSearchLocation.notifyDataSetChanged();
                    } else {
                        if (mResultList != null) {
                            adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                            listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                        }
                    }
                }
                return;
            } else {
                /*stopProgress();*/
                Log.d(TAG, "Null results received from Place Autocomplete Adapter.");
                mResultList = new ArrayList<>();
                if (adapterSearchLocation != null) {
                    adapterSearchLocation.notifyDataSetChanged();
                } else {
                    if (mResultList != null) {
                        adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                        listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                    }
                }
                return;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        callFriendsListApiUpdateOnly();

        if (!mIsReceiverRegistered) {
            mNotificationReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    Log.d(TAG, "Notification register onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

               /* SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, "");
                SharedPreferenceUtil.save();

                // Clear all the notifications in notification bar
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();*/
                    if (intent.hasExtra("action") && !intent.getStringExtra("action").trim().isEmpty()) {
                        if (intent.getStringExtra("action").trim().equalsIgnoreCase("1")) { // Friend request accepted. Notify data set changed here.
                            if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                                UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action accepted. Friend ID: " + user.getId().trim());
                                for (int i = 0; i < adapter.getItemCount(); i++) {
                                    if (adapter.getItem(i).getId().trim().equalsIgnoreCase(user.getId().trim())) {
                                        Log.d(TAG, "Friend request action accepted. Friend ID: " + user.getId().trim()
                                                + ", Card ID: " + adapter.getItem(i).getId().trim());
                                        adapter.getItem(i).setFriendstatus("Friend");
                                        UserClusterItemVO userVO = adapter.getItem(i);
                                        userVO.setFriendstatus("Friend");
//                                    adapter.notifyItemChanged(i);
                                        adapter.notifyItemChanged(i, userVO);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } else if (intent.getStringExtra("action").trim().equalsIgnoreCase("2")) { // Friend request received. Notify data set changed here.
                            if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                                UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action received. Friend ID: " + user.getId().trim());
                                for (int i = 0; i < adapter.getItemCount(); i++) {
                                    if (adapter.getItem(i).getId().trim().equalsIgnoreCase(user.getId().trim())) {
                                        Log.d(TAG, "Friend request action received. Friend ID: " + user.getId().trim()
                                                + ", Card ID: " + adapter.getItem(i).getId().trim());
                                        adapter.getItem(i).setFriendstatus("Pending");
//                                    adapter.notifyItemChanged(i);
//                                    adapter.notifyDataSetChanged();

                                        UserClusterItemVO userVO = adapter.getItem(i);
                                        userVO.setFriendstatus("Pending");
                                        adapter.notifyItemChanged(i, userVO);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    }

                }
            };

            LocalBroadcastManager.getInstance(context).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.FILTER_NOTIFICATION_RECEIVED));

            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("refreshUI"));
            mIsReceiverRegistered = true;
        }

        if (!mIsReceiverRegisteredRequestLocal) {
            mNotificationReceiverRequestLocal = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    Log.d(TAG, "Broadcast Receiver Local Request Action onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

                    if (intent.hasExtra("action") && !intent.getStringExtra("action").trim().isEmpty()) {
                        if (intent.getStringExtra("action").trim().equalsIgnoreCase("1")) { // Friend request accepted. Notify data set changed here.
                            if (intent.hasExtra("otherUserId") && intent.getStringExtra("otherUserId") != null) {
                                String otherUserId = intent.getStringExtra("otherUserId");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action accepted. Friend ID: " + otherUserId.trim());

                                for (int i = 0; i < adapter.getItemCount(); i++) {
                                    if (adapter.getItem(i).getId().trim().equalsIgnoreCase(otherUserId.trim())) {
                                        Log.d(TAG, "Friend request action accepted. Friend ID: " + otherUserId.trim()
                                                + ", Card ID: " + adapter.getItem(i).getId().trim());
                                        adapter.getItem(i).setFriendstatus("Friend");
                                        UserClusterItemVO userVO = adapter.getItem(i);
                                        userVO.setFriendstatus("Friend");
//                                    adapter.notifyItemChanged(i);
                                        adapter.notifyItemChanged(i, userVO);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        } else if (intent.getStringExtra("action").trim().equalsIgnoreCase("2")) { // Friend request rejected. Notify data set changed
                            // here.
                            if (intent.hasExtra("otherUserId") && intent.getSerializableExtra("otherUserId") != null) {
                                String otherUserId = intent.getStringExtra("otherUserId");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action rejected. Friend ID: " + otherUserId.trim());

                                for (int i = 0; i < adapter.getItemCount(); i++) {
                                    if (adapter.getItem(i).getId().trim().equalsIgnoreCase(otherUserId.trim())) {
                                        Log.d(TAG, "Friend request action rejected. Friend ID: " + otherUserId.trim()
                                                + ", Card ID: " + adapter.getItem(i).getId().trim());
                                        adapter.getItem(i).setFriendstatus("");
                                        UserClusterItemVO userVO = adapter.getItem(i);
                                        userVO.setFriendstatus("");
//                                    adapter.notifyItemChanged(i);
                                        adapter.notifyItemChanged(i, userVO);
                                        adapter.notifyDataSetChanged();
                                        /*adapter.removeItemAtPosition(i);*/
                                    }
                                }
                            }
                        }
                    }
                }
            };


            LocalBroadcastManager.getInstance(HomeActivity.this)
                    .registerReceiver(mNotificationReceiverRequestLocal, new IntentFilter(Constants.FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED));

            if (mReceiverRequestLocal == null)
                mReceiverRequestLocal = new MyBroadcastReceiver();
            registerReceiver(mReceiverRequestLocal, new IntentFilter("refreshList"));
            mIsReceiverRegisteredRequestLocal = true;
        }

        checkUpdateStatusOfCards();

        /*registerReceiver(gpsReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));*/

        /*if (mClusterManager != null) {
            mClusterManager.clearItems();
        }*/

       /* // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }*/

    }

    // Check and update status of user cards from friend list saved in local.
    private void checkUpdateStatusOfCards() {
        // Fetch all the match details from Shared Preferences here.
        /*showProgress(getString(R.string.loading));*/
        JSONObject jsonObjectResponse = null;
        ArrayList<ChatListVO> matchList = new ArrayList<>();
        try {
            jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                    ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
            Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
            JSONObject settings = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_settings))
                    ? jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings)) : new JSONObject();
            boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                    ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
            String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                    ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
            if (status) {
                JSONArray data = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                        ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();

                JSONObject dataInner = null;
                for (int i = 0; i < data.length(); i++) {
                    try {
                        dataInner = data.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String matchId = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                            ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                    String friend1 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend1))
                            ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend1)).trim() : "";
                    String friend2 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend2))
                            ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend2)).trim() : "";
                    String statusFriend = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                            ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";

                    /*if (statusFriend.trim().equalsIgnoreCase("Friend")) {
                        statusFriend = "1";
                    } else if (statusFriend.trim().equalsIgnoreCase("Sent")
                            || statusFriend.trim().equalsIgnoreCase("Pending")) {
                        statusFriend = "0";
                    }*/

                    if (adapter != null) {
                        if (friend1.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                            for (int x = 0; x < adapter.getItemCount(); x++) {
                                if (adapter.getItem(x).getId().trim().equalsIgnoreCase(friend2.trim())) {
                                /*Log.d(TAG, "Friend request action sent. Friend ID: " + chatSelected.getUserId().trim()
                                        + ", Card ID: " + adapter.getItem(i).getId().trim() + ", Status: " + chatSelected.getStatus());*/
                                    if (statusFriend.trim().equalsIgnoreCase("1")) {
                                        adapter.getItem(x).setFriendstatus("Friend");
                                    } else if (statusFriend.trim().equalsIgnoreCase("0")) {
                                        adapter.getItem(x).setFriendstatus("Sent");
                                    }
                                /*UserClusterItemVO userVO = adapter.getItem(i);
                                userVO.setFriendstatus("Sent");*/
                                    adapter.notifyItemChanged(x);
                                /*adapter.notifyItemChanged(i, userVO);*/
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        } else {
                            for (int x = 0; x < adapter.getItemCount(); x++) {
                                if (adapter.getItem(x).getId().trim().equalsIgnoreCase(friend1.trim())) {
                                /*Log.d(TAG, "Friend request action sent. Friend ID: " + chatSelected.getUserId().trim()
                                        + ", Card ID: " + adapter.getItem(i).getId().trim() + ", Status: " + chatSelected.getStatus());*/
                                    if (statusFriend.trim().equalsIgnoreCase("1")) {
                                        adapter.getItem(x).setFriendstatus("Friend");
                                    } else if (statusFriend.trim().equalsIgnoreCase("0")) {
                                        adapter.getItem(x).setFriendstatus("Pending");
                                    }

                                    adapter.notifyItemChanged(x);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                }
                /*stopProgress();*/
            } else {
                /*stopProgress();*/
            }

            /*for (int i = 0; i < adapter.getItemCount(); i++) {
                if (adapter.getItem(i).getId().trim().equalsIgnoreCase(chatSelected.getUserId().trim())) {
                    Log.d(TAG, "Friend request action sent. Friend ID: " + chatSelected.getUserId().trim()
                            + ", Card ID: " + adapter.getItem(i).getId().trim() + ", Status: " + chatSelected.getStatus());
                    adapter.getItem(i).setFriendstatus("Sent");
                    UserClusterItemVO userVO = adapter.getItem(i);
                    userVO.setFriendstatus("Sent");
//                                    adapter.notifyItemChanged(i);
                    adapter.notifyItemChanged(i, userVO);
                    adapter.notifyDataSetChanged();
                }
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
            /*stopProgress();*/
        } catch (Exception e) {
            e.printStackTrace();
            /*stopProgress();*/
        }
    }

    @Override
    public void onBackPressed() {
        if (relativeSearchPopup.getVisibility() == View.VISIBLE) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            relativeSearchPopup.setVisibility(View.GONE);
            linearSearchLocation.setVisibility(View.GONE);
            linearSearchTastebuds.setVisibility(View.GONE);
            linearSearchCancel.setVisibility(View.GONE);
//            txtChangeLocation.setVisibility(View.VISIBLE);
            relativeChangeLocation.setVisibility(View.VISIBLE);

            if (menuFabListFilter.isOpened()) {
                menuFabListFilter.close(true);
            }
        } else {
            super.onBackPressed();
//            supportFinishAfterTransition();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.linearLayoutHomeActivityList:
                /*intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);*/

                intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);
                break;

            case R.id.linearLayoutHomeActivityTastebuds:
//                intent = new Intent(context, RestaurantsListActivity.class);
//                startActivity(intent);

                intent = new Intent(this, TasteBudsSuggestionsActivity.class);
//                intent.putExtra("isFromEditProfile", true);
                startActivity(intent);
                break;

            case R.id.linearLayoutHomeActivityFilter:
                showDialogFilterOptionsMap();
                break;

            case R.id.imageViewRestaurantMapActivityBackground:

                break;

            case R.id.imageViewListActivityGradient:
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                } else {
                    menuFabListFilter.open(true);
                }
                break;


            case R.id.fabList:
                menuFabListFilter.toggle(true);

                intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);
                break;

            case R.id.fabFilterHome:
                menuFabListFilter.toggle(true);

                // Create custom dialog object
//                final Dialog dialogFilter = new Dialog(this);
//                dialogFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                LinearLayout.LayoutParams dialogFilterParams = new LinearLayout.LayoutParams(Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);
//                //set height(300) and width(match_parent) here, ie (width,height)
//
//                LayoutInflater inflaterFilter = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View dislogViewFilter = inflaterFilter.inflate(R.layout.layout_dialog_age_filter, null);
//
//                Button btnSearchFilter = (Button) dislogViewFilter.findViewById(R.id.btnSearch);
//                final Button btnFemaleFilter = (Button) dislogViewFilter.findViewById(R.id.btnFemale);
//                final Button btnMaleFilter = (Button) dislogViewFilter.findViewById(R.id.btnMale);
//                final Button btnLgbtFilter = (Button) dislogViewFilter.findViewById(R.id.btnLgbt);
//                final TextView textViewGenderFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewGender);
//                final TextView textViewAgeRangeFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewAgeRange);
//                final TextView textViewDistanceFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewDistance);
//                final RangeBar rangebarAgeFilter = (RangeBar) dislogViewFilter.findViewById(R.id.rangebarAge);
//                final SeekBar seekBarSearchDistanceMap = (SeekBar) dislogViewFilter.findViewById(R.id.seekBarSearchDistanceMap);
//                final ImageView imageViewCloseFilter = (ImageView) dislogViewFilter.findViewById(R.id.imageViewClose);
//
//                seekBarSearchDistanceMap.setProgress(Integer.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "")));
//                rangebarAgeFilter.setRangePinsByValue(Float.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "")),
//                        Float.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "")));
//                distanceSelected = Integer.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, ""));
//
//                if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
//                        .trim().toLowerCase().contains("mi")) {
//                    textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));
//                    seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_MI);
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
//                        .trim().toLowerCase().contains("km")) {
//                    textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));
//                    seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_KM);
//                } else {
//                    textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));
//                    seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_MI);
//                }
//
//                textViewAgeRangeFilter.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "")
//                        + "-" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "")); // + " Yrs"
//
//                // TODO Retrive users gender prefrence and set button selected By default male is selected
////                if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PREFER, "").trim().toLowerCase().equalsIgnoreCase("male")) {
//                if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").trim().toLowerCase()
//                        .equalsIgnoreCase(getString(R.string.gender_male_value))) {
//                    btnMaleFilter.setSelected(true);
//                    btnFemaleFilter.setSelected(false);
//                    btnLgbtFilter.setSelected(false);
//                    textViewGenderFilter.setText(getString(R.string.gender_male));
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
//                        .equalsIgnoreCase(getString(R.string.gender_female_value))) {
//                    btnMaleFilter.setSelected(false);
//                    btnLgbtFilter.setSelected(false);
//                    btnFemaleFilter.setSelected(true);
//                    textViewGenderFilter.setText(getString(R.string.gender_female));
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
//                        .equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
//                    btnMaleFilter.setSelected(false);
//                    btnFemaleFilter.setSelected(false);
//                    btnLgbtFilter.setSelected(true);
//                    textViewGenderFilter.setText(getString(R.string.gender_lgbt));
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
//                        .equalsIgnoreCase(getString(R.string.gender_male_female_value))) {
//                    btnMaleFilter.setSelected(true);
//                    btnFemaleFilter.setSelected(true);
//                    btnLgbtFilter.setSelected(false);
//                    textViewGenderFilter.setText(getString(R.string.gender_male_female));
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
//                        .equalsIgnoreCase(getString(R.string.gender_male_lgbt_value))) {
//                    btnMaleFilter.setSelected(true);
//                    btnFemaleFilter.setSelected(false);
//                    btnLgbtFilter.setSelected(true);
//                    textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
//                } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
//                        .equalsIgnoreCase(getString(R.string.gender_female_lgbt_value))) {
//                    btnMaleFilter.setSelected(false);
//                    btnFemaleFilter.setSelected(true);
//                    btnLgbtFilter.setSelected(true);
//                    textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
//                } else {
//                    btnMaleFilter.setSelected(true);
//                    btnFemaleFilter.setSelected(true);
//                    btnLgbtFilter.setSelected(true);
//                    /*textViewGenderFilter.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, ""));*/
//                    textViewGenderFilter.setText(getString(R.string.gender_all));
//                }
//
//                btnMaleFilter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        isfromButton = true;
//
//                        if (btnMaleFilter.isSelected()) {
//                            if (btnFemaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
//                                btnMaleFilter.setSelected(false);
//                                textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnFemaleFilter.isSelected()) {
//                                    btnMaleFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_female));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnLgbtFilter.isSelected()) {
//                                    btnMaleFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                                /*textViewGenderFilter.setText(getString(R.string.gender_female));
//                                btnFemaleFilter.setSelected(true);
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
//                                SharedPreferenceUtil.save();*/
//                            }
//                        } else {
//                            btnMaleFilter.setSelected(true);
//                            if (btnFemaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
//                                textViewGenderFilter.setText(getString(R.string.gender_all));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnFemaleFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_male_female));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnLgbtFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                } else {
//                                    textViewGenderFilter.setText(getString(R.string.gender_male));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                            }
//                        }
//                    }
//                });
//
//                btnFemaleFilter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        isfromButton = true;
//                        /*if (btnFemaleFilter.isSelected()) {
//                            btnFemaleFilter.setSelected(false);
//                            if (btnMaleFilter.isSelected()) {
//                                textViewGenderFilter.setText(getString(R.string.gender_male));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                textViewGenderFilter.setText(getString(R.string.gender_male));
//                                btnMaleFilter.setSelected(true);
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
//                                SharedPreferenceUtil.save();
//                            }
//                        } else {
//                            btnFemaleFilter.setSelected(true);
//                            if (btnMaleFilter.isSelected()) {
//                                textViewGenderFilter.setText(getString(R.string.gender_all));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                textViewGenderFilter.setText(getString(R.string.gender_female));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
//                                SharedPreferenceUtil.save();
//                            }
//                        }*/
//
//                        if (btnFemaleFilter.isSelected()) {
//                            if (btnMaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
//                                btnFemaleFilter.setSelected(false);
//                                textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnMaleFilter.isSelected()) {
//                                    btnFemaleFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_male));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnLgbtFilter.isSelected()) {
//                                    btnFemaleFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                            }
//                        } else {
//                            btnFemaleFilter.setSelected(true);
//                            if (btnMaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
//                                textViewGenderFilter.setText(getString(R.string.gender_all));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnMaleFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_male_female));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnLgbtFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                } else {
//                                    textViewGenderFilter.setText(getString(R.string.gender_female));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                            }
//                        }
//                    }
//                });
//
//                btnLgbtFilter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        isfromButton = true;
//
//                        if (btnLgbtFilter.isSelected()) {
//                            if (btnMaleFilter.isSelected() && btnFemaleFilter.isSelected()) {
//                                btnLgbtFilter.setSelected(false);
//                                textViewGenderFilter.setText(getString(R.string.gender_male_female));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnMaleFilter.isSelected()) {
//                                    btnLgbtFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_male));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnFemaleFilter.isSelected()) {
//                                    btnLgbtFilter.setSelected(false);
//                                    textViewGenderFilter.setText(getString(R.string.gender_female));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                            }
//                        } else {
//                            btnLgbtFilter.setSelected(true);
//                            if (btnMaleFilter.isSelected() && btnFemaleFilter.isSelected()) {
//                                textViewGenderFilter.setText(getString(R.string.gender_all));
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
//                                SharedPreferenceUtil.save();
//                            } else {
//                                if (btnMaleFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                } else if (btnFemaleFilter.isSelected()) {
//                                    textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                } else {
//                                    textViewGenderFilter.setText(getString(R.string.gender_lgbt));
//                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
//                                    SharedPreferenceUtil.save();
//                                }
//                            }
//                        }
//                    }
//                });
//
//                age1 = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16");
//                age2 = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100");
//
//                rangebarAgeFilter.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
//                    @Override
//                    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
//                        Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
//                        age1 = leftPinValue;
//                        age2 = rightPinValue;
////                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE1, aage1.trim());
////                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE2, age2.trim());
////                SharedPreferenceUtil.save();savetextViewAgeRange.setText(age1 + "-" + age2);
//                        try {
//                            if (Integer.parseInt(age1.trim().replaceAll(",", ".")) < 16) {
//                                age1 = "16";
//                            }
//
//                            if (Integer.parseInt(age1.trim().replaceAll(",", ".")) > 100) {
//                                age1 = "100";
//                            }
//
//                            if (Integer.parseInt(age2.trim().replaceAll(",", ".")) > 100) {
//                                age2 = "100";
//                            }
//
//                            if (Integer.parseInt(age2.trim().replaceAll(",", ".")) < 16) {
//                                age2 = "16";
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, String.valueOf(age1));
//                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, String.valueOf(age2));
//                        SharedPreferenceUtil.save();
//
//                        textViewAgeRangeFilter.setText(age1 + " - " + age2); // + "Yrs"
//                    }
//                });
//
//                seekBarSearchDistanceMap.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                    @Override
//                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                        if (fromUser) { // If the changes are from the User only then accept the
//                            // changes
//                    /* Instead of -> if (progress != 0) { */
////                    distanceSelected = Integer.valueOf(activity.profile.getSearch_distance());
//                            distanceSelected = (int) ((float) progress / 100 * (mMaxValue - mMinValue) + mMinValue);
//                            distanceSelected *= 10;
//                            distanceSelected = Math.round(distanceSelected);
//                            distanceSelected /= 10;
//                            Log.i("distanceSelected", "" + distanceSelected);
//                        }
//
//                        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, "").trim().toLowerCase()
//                                .contains(getResources().getString(R.string.distance_unit_km_value_server))) {
////                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
////                    SharedPreferenceUtil.save();
//                            if (distanceSelected > 160) {
//                                distanceSelected = 160;
//                            }
//
//                            textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));
//                            if (!isfromButton)
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                            SharedPreferenceUtil.save();
//                        } else {
//                    /*convertedtoMiValue = Constants.convertKmtoMi(mCurrentValue);
//                    txtSearchDistance.setText(Integer.toString(convertedtoMiValue) + " Mi");*/
//                            if (distanceSelected > 100) {
//                                distanceSelected = 100;
//                            }
//
//                            textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getString(R.string.prefer_distance_mi));
//                            if (!isfromButton)
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                            SharedPreferenceUtil.save();
////                    distanceSelected = Constants.convertMitoKm(distanceSelected);
////                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
////                    SharedPreferenceUtil.save();
//                        }
//                    }
//
//                    @Override
//                    public void onStartTrackingTouch(SeekBar seekBar) {
//
//                    }
//
//                    @Override
//                    public void onStopTrackingTouch(SeekBar seekBar) {
//
//                    }
//                });
//
//                btnSearchFilter.setSelected(true);
//                btnSearchFilter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialogFilter.dismiss();
//
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                        relativeSearchPopup.setVisibility(View.GONE);
//                        linearSearchLocation.setVisibility(View.GONE);
//                        linearSearchCancel.setVisibility(View.GONE);
//                        linearSearchTastebuds.setVisibility(View.GONE);
//                        txtChangeLocation.setVisibility(View.VISIBLE);
//
//                        if (menuFabListFilter.isOpened()) {
//                            menuFabListFilter.close(true);
//                        }
//
//                        ApiEditProfile();
//                    }
//                });
//
//                imageViewCloseFilter.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialogFilter.dismiss();
//
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                        relativeSearchPopup.setVisibility(View.GONE);
//                        linearSearchLocation.setVisibility(View.GONE);
//                        linearSearchCancel.setVisibility(View.GONE);
//                        linearSearchTastebuds.setVisibility(View.GONE);
//                        txtChangeLocation.setVisibility(View.VISIBLE);
//
//                        if (menuFabListFilter.isOpened()) {
//                            menuFabListFilter.close(true);
//                        }
//                    }
//                });
//
//                dialogFilter.setContentView(dislogViewFilter, dialogFilterParams);
//                dialogFilter.show();
//
////                Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
////                // if decline button is clicked, close the custom dialog
////                declineButton.setOnClickListener(new OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        // Close dialog
////                        dialog.dismiss();
//                    }
//                });
                break;

            default:
                break;

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) { // Check for the integer request code originally supplied to startResolutionForResult().
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.d(TAG, "User agreed to make required location settings changes.");
                    startLocationUpdates();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.d(TAG, "User chose not to make required location settings changes.");
                    break;
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Place place = PlaceAutocomplete.getPlace(context, data);
                    setPlaceDetails(place);
                    break;
                case Activity.RESULT_CANCELED:
                    // The user canceled the operation.
                    Place placeCancelled = null;
                    setPlaceDetails(placeCancelled);
                    break;
            }
        } else if (requestCode == Constants.ACTION_CODE_SEND_NEW_REQUEST) {
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (data.hasExtra(Constants.INTENT_CHAT_SELECTED) && data.getSerializableExtra(Constants.INTENT_CHAT_SELECTED) != null) {
                        ChatListVO chatSelected = (ChatListVO) data.getSerializableExtra(Constants.INTENT_CHAT_SELECTED);
                        for (int i = 0; i < adapter.getItemCount(); i++) {
                            if (adapter.getItem(i).getId().trim().equalsIgnoreCase(chatSelected.getUserId().trim())) {
                                Log.d(TAG, "Friend request action sent. Friend ID: " + chatSelected.getUserId().trim()
                                        + ", Card ID: " + adapter.getItem(i).getId().trim() + ", Status: " + chatSelected.getStatus());
                                adapter.getItem(i).setFriendstatus("Sent");
                                UserClusterItemVO userVO = adapter.getItem(i);
                                userVO.setFriendstatus("Sent");
//                                    adapter.notifyItemChanged(i);
                                adapter.notifyItemChanged(i, userVO);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                    break;

                case Activity.RESULT_CANCELED:

                    break;
            }
        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_USER_SUGGESTIONS) {
                Log.d(TAG, "RESPONSE_USER_SUGGESTIONS_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        if (countSkipUserSuggestions == 0) {
                            posToGetDistanceMatrix = 0;

                            listUsersNearBy.clear();
                            listUsersNearByClicked.clear();

                            if (adapter != null) {
                                int sizeBefore = adapter.getItemCount();
                                adapter.notifyItemRangeRemoved(0, sizeBefore);
                                adapter.notifyDataSetChanged();
                            }

                            mClusterManager.clearItems();

                        }

                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            UserClusterItemVO obj = new UserClusterItemVO();
                            JSONObject data = dataArray.getJSONObject(i);

                            String title = null;
                            String snippet = null;

                            // Add & Set user suggestions to list & show it on map later.
                            String id = !data.isNull("id") ? data.getString("id").trim() : "0";
                            String gender = !data.isNull("gender") ? data.getString("gender").trim() : "";
                            String first_name = !data.isNull("first_name") ? data.getString("first_name").trim() : "";
                            String last_name = !data.isNull("last_name") ? data.getString("last_name").trim() : "";
                            String social_id = !data.isNull("social_id") ? data.getString("social_id").trim() : "";
                            String account_type = !data.isNull("account_type") ? data.getString("account_type").trim() : "";
                            String mobile = !data.isNull("mobile") ? data.getString("mobile").trim() : "";
                            String dob = !data.isNull("dob") ? data.getString("dob").trim() : "";
                            String occupation = !data.isNull("occupation") ? data.getString("occupation").trim() : "";
                            String education = !data.isNull("education") ? data.getString("education").trim() : "";
                            String relationship = !data.isNull("relationship") ? data.getString("relationship").trim() : "";
                            String about_me = !data.isNull("about_me") ? data.getString("about_me").trim() : "";
                            String ethnicity = !data.isNull("ethnicity") ? data.getString("ethnicity").trim() : "";
                            String location = !data.isNull("location") ? data.getString("location").trim() : "";
                            String location_string = !data.isNull("location_string") ? data.getString("location_string").trim() : "";
                            String profilepic1 = !data.isNull("profilepic1") ? data.getString("profilepic1").trim() : "";
                            String profilepic2 = !data.isNull("profilepic2") ? data.getString("profilepic2").trim() : "";
                            String profilepic3 = !data.isNull("profilepic3") ? data.getString("profilepic3").trim() : "";
                            String profilepic4 = !data.isNull("profilepic4") ? data.getString("profilepic4").trim() : "";
                            String profilepic5 = !data.isNull("profilepic5") ? data.getString("profilepic5").trim() : "";
                            String profilepic6 = !data.isNull("profilepic6") ? data.getString("profilepic6").trim() : "";
                            String profilepic7 = !data.isNull("profilepic7") ? data.getString("profilepic7").trim() : "";
                            String profilepic8 = !data.isNull("profilepic8") ? data.getString("profilepic8").trim() : "";
                            String profilepic9 = !data.isNull("profilepic9") ? data.getString("profilepic9").trim() : "";
                            String testbuds = !data.isNull("testbuds") ? data.getString("testbuds").trim() : "";
                            String createdat = !data.isNull("createdat") ? data.getString("createdat").trim() : "";
                            String updatedat = !data.isNull("updatedat") ? data.getString("updatedat").trim() : "";
                            String friendstatus = !data.isNull("friendstatus") ? data.getString("friendstatus").trim() : "";
                            JSONArray mutualfriends = !data.isNull("mutualfriends") ? data.getJSONArray("mutualfriends") : new JSONArray();

                            double lat = 0.0;
                            double lng = 0.0;
                            String[] locationArray = location.split(",");
                            if (locationArray.length > 1) {
                                lat = !locationArray[0].trim().isEmpty() ? Double.parseDouble(locationArray[0].trim()) : 0.0;
                                lng = !locationArray[1].trim().isEmpty() ? Double.parseDouble(locationArray[1].trim()) : 0.0;
                            }

                            title = first_name + " " + last_name;
                            snippet = gender;


                            /*// Static list of tastebuds to display in cards
                            ArrayList<String> listTastebuds = new ArrayList<>();
                            listTastebuds.add("Pizza");
                            listTastebuds.add("Continental");
                            listTastebuds.add("Gujarati");
                            listTastebuds.add("Punjabi");
                            listTastebuds.add("Chinese");*/
                            obj.setId(id.trim());
                            obj.setFirstname(first_name.trim());
                            obj.setLastname(last_name.trim());
                            obj.setName(first_name + " " + last_name);
                            obj.setSocialId(social_id);
                            obj.setAccount_type(account_type);
                            obj.setLocationString(location_string); // i.e. 10 Minutes away
                            obj.setmPosition(new LatLng(lat, lng));
//                            obj.setmSnippet(snippet);
                            obj.setMobile(mobile);
                            obj.setDob(dob);
                            obj.setOccupation(occupation);
                            obj.setEducation(education);
                            obj.setRelationship(relationship);
                            obj.setAbout(about_me);
                            obj.setSex(gender);
                            obj.setEthnicity(ethnicity);
                            obj.setProfilepic1(profilepic1);
                            obj.setProfilepic2(profilepic2);
                            obj.setProfilepic3(profilepic3);
                            obj.setProfilepic4(profilepic4);
                            obj.setProfilepic5(profilepic5);
                            obj.setProfilepic6(profilepic6);
                            obj.setProfilepic7(profilepic7);
                            obj.setProfilepic8(profilepic8);
                            obj.setProfilepic9(profilepic9);
                            // obj.setListTasteBuds(listTastebuds); // Array List of tastebuds. In cards, no need to tastebuds details. Hence only ID will be matched &
                            // percentage match will be displayed.
                            obj.setTastebudsIds(testbuds.trim());
                            obj.setCreatedat(createdat);
                            obj.setUpdatedat(updatedat);
                            obj.setFriendstatus(friendstatus.trim());
                            obj.setMutualFriends(mutualfriends.length());

                            ArrayList<MutualFriendsParcelableVO> MutualFriendList = new ArrayList<>();
                            for (int j = 0; j < mutualfriends.length(); j++) {
                                try {
                                    JSONObject jsonObjMutualFriends = mutualfriends.getJSONObject(j);
                                    MutualFriendsParcelableVO objMutualFriends = new MutualFriendsParcelableVO();
                                    objMutualFriends.setId(!jsonObjMutualFriends.isNull("id") ? jsonObjMutualFriends.getString("id") : "");
                                    objMutualFriends.setFirst_name(!jsonObjMutualFriends.isNull("first_name") ? jsonObjMutualFriends.getString("first_name") : "");
                                    objMutualFriends.setLast_name(!jsonObjMutualFriends.isNull("last_name") ? jsonObjMutualFriends.getString("last_name") : "");
                                    objMutualFriends.setSocial_id(!jsonObjMutualFriends.isNull("social_id") ? jsonObjMutualFriends.getString("social_id") : "");
                                    objMutualFriends.setProfilepic1(!jsonObjMutualFriends.isNull("profilepic1") ? jsonObjMutualFriends.getString("profilepic1") : "");
                                    objMutualFriends.setProfilepic2(!jsonObjMutualFriends.isNull("profilepic2") ? jsonObjMutualFriends.getString("profilepic2") : "");
                                    objMutualFriends.setProfilepic3(!jsonObjMutualFriends.isNull("profilepic3") ? jsonObjMutualFriends.getString("profilepic3") : "");
                                    objMutualFriends.setProfilepic4(!jsonObjMutualFriends.isNull("profilepic4") ? jsonObjMutualFriends.getString("profilepic4") : "");
                                    objMutualFriends.setProfilepic5(!jsonObjMutualFriends.isNull("profilepic5") ? jsonObjMutualFriends.getString("profilepic5") : "");
                                    objMutualFriends.setProfilepic6(!jsonObjMutualFriends.isNull("profilepic6")
                                            ? jsonObjMutualFriends.getString("profilepic6") : "");
                                    objMutualFriends.setProfilepic7(!jsonObjMutualFriends.isNull("profilepic7")
                                            ? jsonObjMutualFriends.getString("profilepic7") : "");
                                    objMutualFriends.setProfilepic8(!jsonObjMutualFriends.isNull("profilepic8")
                                            ? jsonObjMutualFriends.getString("profilepic8") : "");
                                    objMutualFriends.setProfilepic9(!jsonObjMutualFriends.isNull("profilepic9")
                                            ? jsonObjMutualFriends.getString("profilepic9") : "");
                                    MutualFriendList.add(objMutualFriends);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            obj.setMutualFriendList(MutualFriendList);

                            /*// Generate a random number for match level, only for static execution.
                            Random r = new Random();
                            int level = r.nextInt(3);
                            if (level > 2) {
                                level = 2;
                            }*/

                            // Get Tastebuds IDs & match it to db
                            String[] tasteBudsCurrentUser = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_TASTEBUDS, "").trim().split(",");
                            String[] tasteBudsUser = testbuds.trim().split(",");
                          /*  Log.d(TAG, "Tastebuds current user: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_TASTEBUDS, "").trim()
                                    + "\nUser: " + testbuds.trim());*/
                            int countMatch = 0;
                            int percentage = 0;
                            if (tasteBudsCurrentUser.length > tasteBudsUser.length) {
                                for (int x = 0; x < tasteBudsCurrentUser.length; x++) {
                                    boolean isMatch = false;
                                    for (int y = 0; y < tasteBudsUser.length; y++) {
                                        if (tasteBudsUser[y].trim().equalsIgnoreCase(tasteBudsCurrentUser[x].trim())
                                                && !tasteBudsUser[y].trim().isEmpty() && !tasteBudsCurrentUser[x].trim().isEmpty()) {
                                            isMatch = true;
//                                        Log.d(TAG, "Match: "+tasteBudsUser[y].trim());
                                        }
                                    }

                                    if (isMatch) {
                                        countMatch++;
                                    }
                                }

                                // Calculate percentage from counts of match & total tastebuds of user from suggestion
                                percentage = (100 * countMatch) / tasteBudsCurrentUser.length;
                            } else {
                                for (int x = 0; x < tasteBudsUser.length; x++) {
                                    boolean isMatch = false;
                                    for (int y = 0; y < tasteBudsCurrentUser.length; y++) {
                                        if (tasteBudsCurrentUser[y].trim().equalsIgnoreCase(tasteBudsUser[x].trim())
                                                && !tasteBudsCurrentUser[y].trim().isEmpty() && !tasteBudsUser[x].trim().isEmpty()) {
                                            isMatch = true;
//                                        Log.d(TAG, "Match: "+tasteBudsCurrentUser[y].trim());
                                        }
                                    }

                                    if (isMatch) {
                                        countMatch++;
                                    }
                                }

                                // Calculate percentage from counts of match & total tastebuds of user from suggestion
                                percentage = (100 * countMatch) / tasteBudsUser.length;
                            }


                            Log.d(TAG, "Percentage match: " + percentage + "% for position: " + i + ", Count Match: " + countMatch
                                    + ", Total x: " + tasteBudsCurrentUser.length);
//                            int matchLevel = 0;
//                            if (percentage < 30) {
//                                matchLevel = 0;
//                            } else if (percentage >= 20 && percentage < 70) {
//                                matchLevel = 1;
//                            } else {
//                                matchLevel = 2;
//                            }
                            obj.setTastebudsmatchlevel(percentage);
                            /*if (i % 2 == 0) {
                                obj.setProfilepicurl("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQi5eHnCA-p3TkvERaIdHS-FZe7xKzgdMjpFrUlmj3LjyBTq94zRg");
                            } else {
                                obj.setProfilepicurl("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSrJJzPBkqiGnX7tlpHwW7yP0VpSHaxFM0WV_xTH2SAW8SC9ZLi");
                            }*/

                            obj.setPosInList(i);
                            if (i == 0) {
                                obj.setSelected(true);
                            } else {
                                obj.setSelected(false);
                            }

                            listUsersNearBy.add(obj);

                        }

//
                        // Set Recycler view adapter here.
                       /* if (adapter != null) {
                            adapter.notifyItemRangeInserted(0, listUsersNearBy.size());
                            adapter.notifyDataSetChanged();
                        } else {*/
                        adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearBy);
                        rvUsers.setAdapter(adapter);
//                        adapter.setSelecteditem(adapter.getItemCount() - 1);
                        /*}*/
                        setScrollListenerFriendsList();

                        changePositionMyLocationButtomMaps();

                        if (countSkipUserSuggestions == 0) {
                            getMap().clear();
                            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10.0f));
                        }

                        mClusterManager.addItems(listUsersNearBy);
                        mClusterManager.cluster();

                        if (dataArray.length() >= COUNT_PAGE_LIMIT_USER_SUGGESTIONS) {
                            countSkipUserSuggestions = countSkipUserSuggestions + COUNT_PAGE_LIMIT_USER_SUGGESTIONS;
                            sendRequestUserSuggestions();
                        } else {
                            // TODO Stop paging here.
                            stopProgress();
                            /*if (adapter != null) {
                                int sizeBefore = adapter.getItemCount();
                                adapter.notifyItemRangeRemoved(0, sizeBefore);
                                adapter.notifyDataSetChanged();
                            }
                            listUsersNearByClicked.clear();

                            // Set Recycler view adapter here.
                            listUsersNearByClicked.addAll(listUsersNearBy);
                            if (adapter != null) {
                                adapter.notifyItemRangeInserted(0, listUsersNearByClicked.size());
                                adapter.notifyDataSetChanged();
                            } else {
                                adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
                                rvUsers.setAdapter(adapter);
                            }*/

                            // TODO Start calling distance matrix api to get time between current user & user in suggestion
                            posToGetDistanceMatrix = 0;
                            GetTimeMeasureBetweenCoOrdinates(posToGetDistanceMatrix);
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d("RESPONSE_EDIT_PROFILE_SOCIAL_SIGN_IN", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            /*// TODO Forward user to TasteBuds profile screen.
                            intent = new Intent(context, TasteBudsSuggestionsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            supportFinishAfterTransition();*/
                            stopProgress();

                            // TODO Restrict user suggestion call due to duplicate calls in life cycle.
//                            getString(R.string.loading);
                            showProgress(getString(R.string.loading));
                            setUpMapOnScreen(true);
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API) {
                stopProgress();
                Log.d(TAG, "DISTANCE MATRIX API RESPONSE: " + response);

                try {
                    // Parse API response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";

                    if (status.trim().equalsIgnoreCase("OK")) {
                        // Success. Update result for time travel in list here.
                        JSONArray rows = !jsonObjectResponse.isNull("rows") ? jsonObjectResponse.getJSONArray("rows") : new JSONArray();
                        for (int i = 0; i < rows.length(); i++) {
                            JSONObject row = rows.getJSONObject(i);
                            JSONArray elements = !row.isNull("elements") ? row.getJSONArray("elements") : new JSONArray();
                            for (int j = 0; j < elements.length(); j++) {
                                JSONObject element = elements.getJSONObject(j);
                                String statusElement = !element.isNull("status") ? element.getString("status").trim() : "";
                                if (statusElement.trim().equalsIgnoreCase("OK")) {
                                    JSONObject duration = !element.isNull("duration") ? element.getJSONObject("duration") : new JSONObject();
                                    String text = !duration.isNull("text") ? duration.getString("text").trim() : "";
                                    if (!text.trim().isEmpty()) {
                                        Log.d(TAG, "Matrix API ID Pos: " + posToGetDistanceMatrix + ", List Size: " + listUsersNearBy.size());
                                        if (posToGetDistanceMatrix < listUsersNearBy.size()) {
                                            listUsersNearBy.get(posToGetDistanceMatrix).setTimeToReach(text.trim());
                                            for (int k = 0; k < listUsersNearByClicked.size(); k++) {
                                                if (listUsersNearByClicked.get(k).getId().trim()
                                                        .equalsIgnoreCase(listUsersNearBy.get(posToGetDistanceMatrix).getId().trim())) {
//                                                    Log.d(TAG, "Distance match for user position: "+k);
                                                    Log.d(TAG, "Matrix API ID: " + listUsersNearBy.get(posToGetDistanceMatrix).getId() + " "
                                                            + "\nUser Id: " + listUsersNearByClicked.get(k).getId());
                                                    listUsersNearByClicked.get(k).setTimeToReach(text.trim());
                                                    if (adapter != null) {
                                                        Log.d(TAG, "Matrix API ID Notify data item change for position: " + k);
                                                        adapter.notifyItemChanged(k);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                            if (adapter != null) {
                                                adapter.notifyItemChanged(posToGetDistanceMatrix);
                                                adapter.notifyDataSetChanged();
                                            }
                                        }
                                    } else {
                                        if (posToGetDistanceMatrix < listUsersNearBy.size()) {
                                            listUsersNearBy.get(posToGetDistanceMatrix).setTimeToReach("");
                                            for (int k = 0; k < listUsersNearByClicked.size(); k++) {
                                                if (listUsersNearByClicked.get(k).getId().trim()
                                                        .equalsIgnoreCase(listUsersNearBy.get(posToGetDistanceMatrix).getId().trim())) {
                                                    listUsersNearByClicked.get(k).setTimeToReach("");
                                                    if (adapter != null) {
                                                        adapter.notifyItemChanged(k);
                                                        adapter.notifyDataSetChanged();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (posToGetDistanceMatrix < listUsersNearBy.size()) {
                                        listUsersNearBy.get(posToGetDistanceMatrix).setTimeToReach("");
                                        for (int k = 0; k < listUsersNearByClicked.size(); k++) {
                                            if (listUsersNearByClicked.get(k).getId().trim()
                                                    .equalsIgnoreCase(listUsersNearBy.get(posToGetDistanceMatrix).getId().trim())) {
                                                listUsersNearByClicked.get(k).setTimeToReach("");
                                                if (adapter != null) {
                                                    adapter.notifyItemChanged(k);
                                                    adapter.notifyDataSetChanged();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (posToGetDistanceMatrix < listUsersNearBy.size() - 1) {
                                posToGetDistanceMatrix++;
                                GetTimeMeasureBetweenCoOrdinates(posToGetDistanceMatrix);
                            } else if (posToGetDistanceMatrix == listUsersNearBy.size() - 1) {
                                posToGetMutualFriends = 0;
                                FacebookMutualFriendAPI(posToGetMutualFriends);
                            }
                        }
                    } else {
                        if (posToGetDistanceMatrix < listUsersNearBy.size()) {
                            listUsersNearBy.get(posToGetDistanceMatrix).setTimeToReach("");
                        }
                        for (int k = 0; k < listUsersNearByClicked.size(); k++) {
                            if (listUsersNearByClicked.get(k).getId().trim().equalsIgnoreCase(listUsersNearBy.get(posToGetDistanceMatrix).getId().trim())) {
                                listUsersNearByClicked.get(k).setTimeToReach("");
                                if (adapter != null) {
                                    adapter.notifyItemChanged(k);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }

                        if (posToGetDistanceMatrix < listUsersNearBy.size() - 1) {
                            posToGetDistanceMatrix++;
                            GetTimeMeasureBetweenCoOrdinates(posToGetDistanceMatrix);
                        } else if (posToGetDistanceMatrix == listUsersNearBy.size() - 1) {
                            posToGetMutualFriends = 0;
                            FacebookMutualFriendAPI(posToGetMutualFriends);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } finally {

                }
            } else if (action == Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonObjectSend = dataArray.getJSONObject(i);

                            matchIdNewRequest = !jsonObjectSend.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? jsonObjectSend.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";

                        }

                        if (posSelected < listUsersNearByClicked.size()) {
                            listUsersNearByClicked.get(posSelected).setFriendstatus("Sent");
                            adapter.notifyItemChanged(posSelected);
                            adapter.notifyDataSetChanged();
                        }

                        stopProgress();
                        callFriendsListApi();

                        showSnackBarMessageOnly(context.getString(R.string.friend_request_sent_successfully));

                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
                Log.d(TAG, "Friend list api response: " + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();
                stopProgress();

                JSONObject jsonObjectResponse = null;
                try {
                    jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            ChatListVO item = new ChatListVO();
                            try {
                                dataInner = data.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                            String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                            String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                            item.setMatchid(matchId);
                            item.setFriend1(friend1);
                            item.setFriend2(friend2);
                            item.setStatus(statusFriend);

                            JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                    ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                            for (int j = 0; j < details.length(); j++) {

                                JSONObject detailInner = details.getJSONObject(j);
                                String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                        ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                        ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                        ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                        ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                        ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                item.setUserId(userId);
                                item.setFirstName(first_name);
                                item.setLastName(lastName);
                                item.setProfilePic(profilePic);
                                item.setGender(gender);

                            }

                            FriendList.add(item);

                            /*for (int j = 0; j < FriendList.size(); j++) {
                                if (FriendList.get(j).getMatchid().equalsIgnoreCase(matchIdNewRequest)) {
                                    intent = new Intent(context, ChatDetailsActivity.class);
                                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, FriendList.get(j));
                                    intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
//                    context.startActivity(intent);
                                    startActivity(intent);
                                    *//*startActivityForResult(intent, Constants.ACTION_CODE_CHAT_USERS_LIST);*//*
                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                }

                            }*/
                        }

                        ChatListVO itemDummy = new ChatListVO();
                        itemDummy.setMatchid("-1");
                        itemDummy.setFriend1(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                        itemDummy.setFriend2(adapter.getItem(posSelected).getId().trim());
                        itemDummy.setStatus("0");
                        itemDummy.setUserId(adapter.getItem(posSelected).getId().trim());
                        itemDummy.setFirstName(adapter.getItem(posSelected).getFirstname().trim());
                        itemDummy.setLastName(adapter.getItem(posSelected).getLastname().trim());
                        itemDummy.setProfilePic(adapter.getItem(posSelected).getProfilepic1().trim());
                        itemDummy.setGender(adapter.getItem(posSelected).getSex().trim());

                        /*intent = new Intent(context, ChatDetailsActivity.class);
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, itemDummy);
                        intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
//                    context.startActivity(intent);
                        startActivity(intent);*/

                        /*Log.d(TAG, "First Name to send requst: "+adapter.getItem(posSelected).getFirstname().trim()
                                +", "+itemDummy.getFirstName().trim());*/

                        intent = new Intent(context, ChatDetailsActivity.class);
                        intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, itemDummy);
                        intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
//                        activity.startActivity(intent);
                        startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
//                        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        overridePendingTransition(0, 0);

                        /*intent = new Intent(context, RestaurantListForSendRequestActivity.class);
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, itemDummy);
                        intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
                        intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
//                intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
                        startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);*/


                    /*if (adapterChatList == null) {
                        adapterChatList = new AdapterChatHomeListAdapter(context, itemList);
                        listViewChatHome.setAdapter(adapterChatList);
                    } else {
                        adapterChatList.notifyDataSetChanged();
                    }*/
                    } else {
                        showSnackBarMessageOnly(errormessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if ((action == Constants.ACTION_CODE_PROFILE || action == Constants.ACTION_CODE_EDIT_PROFILE) && response != null) {
                Log.d("ResponseProfile", "" + response);
                stopProgress();

                JSONObject jsonObjectResponse = null;
                try {
                    jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            dataInner = data.getJSONObject(i);
                        }
                        if (dataInner != null) {
                            UserProfileAPI profile = new UserProfileAPI(context, dataInner);

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profile.profilepic6.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profile.profilepic7.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profile.profilepic8.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profile.profilepic9.trim()));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();

                            // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                            showProgress(getString(R.string.loading));
                            setUpMapOnScreen(true);
                        }

                    } else {
                        showSnackBarMessageOnly(errormessage);
                        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                        showProgress(getString(R.string.loading));
                        setUpMapOnScreen(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                    showProgress(getString(R.string.loading));
                    setUpMapOnScreen(true);
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST_UPDATE_ONLY && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                checkUpdateStatusOfCards();
                /*stopProgress();*/
            }
        } else {
            stopProgress();
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    public void callFriendsListApiUpdateOnly() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST_UPDATE_ONLY, HomeActivity.this);
    }

    public void callFriendsListApi() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, HomeActivity.this);
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, 0 /* clientId */, this)
                .build();
    }

    /**
     * Sets up the location request. Android has two location request settings: {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update interval (5 seconds), the Fused Location Provider API returns location updates
     * that are accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

                .setInterval(120 * 1000)        // 120 seconds, in milliseconds
                .setFastestInterval(60 * 1000); // 60 second, in milliseconds
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        if (!isLocationReceivedOnce) {
            Log.d(TAG, "Checking location settings...");

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);
            result.setResultCallback(this);
        }
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private boolean isLocationPermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void checkLocationPermissionGrantedOrNot() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) ||
                        !shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    showDialogAlertRequestPermissionLocation(getString(R.string.request_permission),
                            getString(R.string.location_permission_is_required));
                    /*requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);*/
                    return;
                }

                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
            } else {
                checkLocationSettings();
            }
        } else {
            checkLocationSettings();
        }
    }

    protected void showDialogAlertRequestPermissionLocation(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Location permission granted...");
                    // permission was granted, yay! Do the  camera related task you need to do.
                   /* showSnackBar(getString(R.string.you_have_successfully_granted_required_permissions));*/
                        /*checkLocationSettings();*/
                    /*PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);
                    result.setResultCallback(HomeActivity.this);*/

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            /*checkLocationSettings();*/
                            if (mGoogleApiClient == null) {
                                // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
                                buildGoogleApiClient();
                            }

                            createLocationRequest();
                            buildLocationSettingsRequest();
                            checkLocationPermissionGrantedOrNot();

                            if (!mGoogleApiClient.isConnected())
                                mGoogleApiClient.connect();
                        }
                    });
                    /*if(mGoogleApiClient != null){
                        if(mGoogleApiClient.isConnected()){
                            mGoogleApiClient.disconnect();
                        }else if(mGoogleApiClient.isConnecting()){
                            mGoogleApiClient.disconnect();
                        }
                    }

                    // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
                    buildGoogleApiClient();
                    createLocationRequest();
                    buildLocationSettingsRequest();
                    checkLocationPermissionGrantedOrNot();

                    mGoogleApiClient.connect();*/
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    /*supportFinishAfterTransition();*/
                    Toast.makeText(context, getString(R.string.you_have_not_granted_location_permission), Toast.LENGTH_SHORT).show();

                    // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                    /*setUpMapOnScreen(true);*/
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            if (!isDisableConnectOnPause) {
                Log.d(TAG, "Google API Client connected");
                if (isLocationPermissionGranted()) {
                    Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                    if (mapFragment != null) {
                        mapFragment.getMapAsync(this);
                    }

                    if (location == null) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, HomeActivity.this);
                    } else {
                        //If everything went fine lets get latitude and longitude
                        Log.d(TAG, "Location using last location. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude()
                                + ", isCurrentLocationEnabled: " + isCurrentLocationEnabled);
//                    if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude() && isCurrentLocationEnabled) {

                        locationChangeLatitude = location.getLatitude();
                        locationChangeLongitude = location.getLongitude();

                        if (isCurrentLocationEnabled) {
                            isCurrentLocationEnabled = true;
                            currentLatitude = location.getLatitude();
                            currentLongitude = location.getLongitude();
                            String lat = String.valueOf(currentLatitude);
                            String lng = String.valueOf(currentLongitude);

//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, lat.trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, lng.trim().replaceAll(",", "."));
                            SharedPreferenceUtil.save();

                            // Save current location of user to server by using edit profile api
                            showProgress(getString(R.string.loading));
                            getFullAddressForCurrentLocation(currentLatitude, currentLongitude, true);
                        /*sendRequestSaveProfileLocationAPI("");*/
                        } else {
                            // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                            showProgress(getString(R.string.loading));
                            setUpMapOnScreen(true);
                        }

                   /* // Save user latitude & longitude in background using edit profile API.
                    sendRequestEditProfileDetails();*/

                    /*stopProgress();*/

                        if (!isLocationReceivedOnce) {
                            isLocationReceivedOnce = true;
                        }
                    }
//                }
                } else {
                }
            } else {
                isDisableConnectOnPause = false;
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            stopProgress();
        } catch (Exception e) {
            e.printStackTrace();
            stopProgress();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Suspended");

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location updated. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude() +
                ", Current Lat: " + currentLatitude + ", Current Longitude: " + currentLongitude + ", isCurrentLocationEnabled: " + isCurrentLocationEnabled);
                /*+ ", isLocationUpdatedOnce: " + isLocationUpdatedOnce*/
        stopProgress();

        locationChangeLatitude = location.getLatitude();
        locationChangeLongitude = location.getLongitude();

        /*if (!isLocationUpdatedOnce) {
            isLocationUpdatedOnce = true;
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.save();

            *//* // Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails(); *//*

            stopProgress();

            if (!isLocationReceivedOnce) {
                isLocationReceivedOnce = true;
            }

            boolean isToReloadScreen = true;
            getFullAddressForCurrentLocation(currentLatitude, currentLongitude, isToReloadScreen);
        } else {*/
        if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude() && isCurrentLocationEnabled) {
            Location loc1 = new Location("");
            loc1.setLongitude(currentLongitude);
            loc1.setLatitude(currentLatitude);

            Location loc2 = new Location("");
            loc2.setLatitude(locationChangeLatitude);
            loc2.setLongitude(locationChangeLongitude);

            float distanceInMeters = loc1.distanceTo(loc2);
            Log.d(TAG, "Location changed distance in meters: " + distanceInMeters);
            boolean isToReloadScreen = false;
            if (distanceInMeters >= 250) {
                isToReloadScreen = true;
            }

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
//            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.save();

            /* // Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails(); */

            stopProgress();

            if (!isLocationReceivedOnce) {
                isLocationReceivedOnce = true;
            }

            getFullAddressForCurrentLocation(currentLatitude, currentLongitude, isToReloadScreen);

           /* setUpMapOnScreen(true);*/
        }
        /*}*/
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Failed");
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(HomeActivity.this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
                stopProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
            stopProgress();
        }

    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        Log.d(TAG, "Result received: " + locationSettingsResult.getStatus());
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                    status.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    Log.d(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*mGoogleApiClient.connect();*/
    }

    @Override
    public void onPause() {
        /*if (mClusterManager != null) {
            mClusterManager.clearItems();
        }

        isLocationReceivedOnce = false;*/

        /*// Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }

        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();*/

        /*if (gpsReceiver != null) {
            unregisterReceiver(gpsReceiver);
        }*/

        /*try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(context).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {*/
        isDisableConnectOnPause = true;
        super.onPause();
        /*}*/


        /*super.onPause();*/
    }

    @Override
    protected void onDestroy() {
        try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(context).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }

            if (mNotificationReceiverRequestLocal != null)
                LocalBroadcastManager.getInstance(HomeActivity.this).unregisterReceiver(mNotificationReceiverRequestLocal);

            if (mIsReceiverRegisteredRequestLocal) {
                unregisterReceiver(mReceiverRequestLocal);
                mReceiverRequestLocal = null;
                mIsReceiverRegisteredRequestLocal = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onDestroy();
        }
    }

    @Override
    public void onStop() {
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }

        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();

        super.onStop();
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                }
            });
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        if (isLocationPermissionGranted()) {
            /*if (mGoogleApiClient != null && mLocationRequest != null && mGoogleApiClient.isConnected()) {*/
            Log.d(TAG, "Location permission granted. Start location updates here.");
//                checkLocationSettings();
            try {
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, HomeActivity.this)
                            .setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    if (!isLocationReceivedOnce) {
                                        setUpMapOnScreen(false);
                                    }
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*} else {
                // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
                if (mGoogleApiClient == null) {
                    buildGoogleApiClient();
                    createLocationRequest();
                    buildLocationSettingsRequest();
                    checkLocationPermissionGrantedOrNot();

                    mGoogleApiClient.connect();
                }
            }*/
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap map) {
        /*if (mMap != null) {
            return;
        }*/
        mMap = map;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Map Custom Style parsing failed.");
            }

            if (isLocationPermissionGranted()) {
                // Enable current location of user on map
                mMap.setMyLocationEnabled(true);

                // Enable current location of user on map
                mMap.getUiSettings().setMapToolbarEnabled(false);

                mMap.getUiSettings().setZoomGesturesEnabled(true);

//                changePositionMyLocationButtomMaps();

                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        isCurrentLocationEnabled = true;
                        startLocationUpdates();

                        currentLatitude = locationChangeLatitude;
                        currentLongitude = locationChangeLongitude;

                        /*isLocationUpdatedOnce = true;*/
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                        SharedPreferenceUtil.save();

                        getFullAddressForCurrentLocation(currentLatitude, currentLongitude, true);
                        return false;
                    }
                });



                /*// get reference to my location icon
                View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

                // and next place it, for example, on bottom right (as Google Maps app)
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                rlp.addRule(RelativeLayout.ABOVE, rvUsers.getId());
                rlp.setMargins(0, 0, Constants.convertDpToPixels(16), Constants.convertDpToPixels(16));*/

                changePositionMyLocationButtomMaps();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
        /*setUpMapOnScreen(true);*/

        /*mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LATITUDE, "0")),
                Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LONGITUDE, "0"))), 11));*/

       /* mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent intent = new Intent(CafeListMapActivity.this, CafeActivity.class);
                startActivity(intent);
                return true;
            }
        });*/
        /*mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
            }
        });*/
    }

    protected GoogleMap getMap() {
        return mMap;
    }

//    public class MyUsersReader {
//        /*
//         * This matches only once in whole input,
//         * so Scanner.next returns whole InputStream as a String.
//         * http://stackoverflow.com/a/5445161/2183804
//         */
//        private static final String REGEX_INPUT_BOUNDARY_BEGINNING = "\\A";
//
//        public List<UserClusterItemVO> read(InputStream inputStream) throws JSONException {
//            List<UserClusterItemVO> items = new ArrayList<>();
//            String json = new Scanner(inputStream).useDelimiter(REGEX_INPUT_BOUNDARY_BEGINNING).next();
//            JSONArray array = new JSONArray(json);
//            for (int i = 0; i < array.length(); i++) {
//                String title = null;
//                String snippet = null;
//                JSONObject object = array.getJSONObject(i);
//                double lat = object.getDouble("lat");
//                double lng = object.getDouble("lng");
//                if (!object.isNull("title")) {
//                    title = object.getString("title");
//                }
//                if (!object.isNull("snippet")) {
//                    snippet = object.getString("snippet");
//                }
//
//                // Static list of tastebuds to display in cards
//                ArrayList<String> listTastebuds = new ArrayList<>();
//                listTastebuds.add("Pizza");
//                listTastebuds.add("Continental");
//                listTastebuds.add("Gujarati");
//                listTastebuds.add("Punjabi");
//                listTastebuds.add("Chinese");
//
//                UserClusterItemVO obj = new UserClusterItemVO();
//                obj.setName("User " + (i + 1));
//                obj.setLocationString("10 Mins away");
//                obj.setmPosition(new LatLng(lat, lng));
//                obj.setmSnippet(snippet);
//                obj.setListTasteBuds(listTastebuds);
//
//                // Generate a random number for match level, only for static execution.
//                Random r = new Random();
//                int level = r.nextInt(3);
//                if (level > 2) {
//                    level = 2;
//                }
//                obj.setTastebudsmatchlevel(level);
//                if (i % 2 == 0) {
//                    obj.setProfilepicurl("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQi5eHnCA-p3TkvERaIdHS-FZe7xKzgdMjpFrUlmj3LjyBTq94zRg");
//                } else {
//                    obj.setProfilepicurl("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSrJJzPBkqiGnX7tlpHwW7yP0VpSHaxFM0WV_xTH2SAW8SC9ZLi");
//                }
//
//                obj.setPosInList(i);
//                if (i == 0) {
//                    obj.setSelected(true);
//                } else {
//                    obj.setSelected(false);
//                }
//
//                items.add(obj);
//            }
//            return items;
//        }
//    }

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class ClusterItemRenderer extends DefaultClusterRenderer<UserClusterItemVO> {
        private final IconGenerator mIconGenerator = new IconGenerator(context.getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
        //        private final ImageView mImageView;
//        private final ImageView mClusterImageView;
        private final TextView txtClusterCount;
        private final ImageView imgPin;
        private final View viewClusterItem;
//        private final int mDimensionWidth, mDimensionHeight;

        public ClusterItemRenderer() {
            super(context.getApplicationContext(), getMap(), mClusterManager);

            viewClusterItem = inflater.inflate(R.layout.layout_marker_custom_user_maps_activity, null);
            mClusterIconGenerator.setContentView(viewClusterItem);
            mClusterIconGenerator.setBackground(null);
            txtClusterCount = (TextView) viewClusterItem.findViewById(R.id.amu_text);
            imgPin = (ImageView) viewClusterItem.findViewById(R.id.imageViewMarkerCustomUserMaps);
//            imgViewEventIcon = (ImageView) multiProfile.findViewById(R.id.imgViewEventIcon);

//            mImageView = new ImageView(getApplicationContext());
//            mDimensionWidth = (int) getResources().getDimension(R.dimen.marker_places_width);
//            mDimensionHeight = (int) getResources().getDimension(R.dimen.marker_places_height);
//            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
//            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
//            mImageView.setPadding(padding, padding, padding, padding);
            /*mIconGenerator.setContentView(multiProfile);*/
        }

        @Override
        protected void onBeforeClusterItemRendered(UserClusterItemVO item, MarkerOptions markerOptions) {
            // Draw a single event.
            // Set the info window to show their name.
//            mImageView.setImageResource(person.profilePhoto);
//            txtPlacesCount.setText(item.c);
//            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf("1"));
//            Log.d(TAG, "onBeforeClusterItemRendered. Restaurant Name: " + item.getName());
            txtClusterCount.setVisibility(View.VISIBLE);
            txtClusterCount.setText("1");

            boolean isToLoadCluster = false;
            if (posSelectedCluster == null && posSelectedClusterItem == null) {
                isToLoadCluster = true;
            }

            if (isToLoadCluster) {
                // TODO Set first item to selected & deselect previous item selected.
                if (posSelectedClusterItem != null) {
                    if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                        listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(false);
                    }
                }

                item.setSelected(false);
                posSelectedCluster = null;
                posSelectedClusterItem = item;

                // TODO Reset users list adapter as per cluster item click. User within cluster will be displayed in recycler view at bottom of the screen.
                listUsersNearByClicked.clear();
                /*if (adapter != null) {
                    int sizeBefore = adapter.getItemCount();
                    adapter.notifyItemRangeChanged(0, sizeBefore);
                    adapter.notifyItemRangeRemoved(0, sizeBefore);
                    adapter.notifyDataSetChanged();
                }*/

                // TODO Add every cluster item to list
                listUsersNearByClicked.add(item);

                // Set Recycler view adapter here.
                /*adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
                rvUsers.setAdapter(adapter);
                setScrollListenerFriendsList();
                Log.d(TAG, "List_user_suggestions: " + adapter.getItemCount());*/

                changePositionMyLocationButtomMaps();
            }

            /*if (item.isSelected()) {
                posSelectedClusterItem = item;
//                if (rvUsers != null && llmRecyclerView != null && posToScrollRestaurantList != posSelectedClusterItem.getPosInList()) {
//                    llmRecyclerView.smoothScrollToPosition(rvUsers, null, posSelectedClusterItem.getPosInList());
//                    llmRecyclerView.scrollToPositionWithOffset(item.getPosInList(), 0);
//                }
            }*/
//            posSelectedCluster = null;

            if (item.getTastebudsmatchlevel() >= 70) {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_green);
            } else if (item.getTastebudsmatchlevel() < 70 && item.getTastebudsmatchlevel() >= 30) {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_blue);
            } else {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_red);
            }

            viewClusterItem.setSelected(item.isSelected());
            /*imgViewEventIcon.setVisibility(View.VISIBLE);
            imgViewEventIcon.setImageResource(setEventIcon(item.eventDetails.get("cats")));*/
            Bitmap icon = mClusterIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
            markerOptions.snippet("");

            /*if (getMap() != null) {
                LatLng coordinate = item.getmPosition(); //Store these lat lng values somewhere. These should be constant.
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(coordinate, getMap().getCameraPosition().zoom);
                getMap().animateCamera(location);
            }*/

//            .title(String.valueOf(item.getPosInList()).trim());

//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(item.eventDetails.containsKey("name") ? item.eventDetails.get("name").trim() : "");
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<UserClusterItemVO> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
//            Log.d(TAG, "onBeforeClusterRendered. Cluster: " + cluster.toString() + ", Size: " + cluster.getSize());

            txtClusterCount.setVisibility(View.VISIBLE);
            txtClusterCount.setText(String.valueOf(cluster.getSize()));
//                imgViewEventIcon.setImageResource(R.drawable.ic_event_music);

            boolean isToLoadCluster = false;
            if (posSelectedCluster == null && posSelectedClusterItem == null) {
                isToLoadCluster = true;
            }

            if (isToLoadCluster) {
                // TODO deselect previous item selected.
//        if (posSelectedClusterItem < listUsersNearBy.size()) {
                if (posSelectedClusterItem != null) {
                    if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                        listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(false);
                    }
                }

                // TODO Reset users list adapter as per cluster item click. User within cluster will be displayed in recycler view at bottom of the screen.
                listUsersNearByClicked.clear();
                /*if (adapter != null) {
                    int sizeBefore = adapter.getItemCount();
                    adapter.notifyItemRangeChanged(0, sizeBefore);
                    adapter.notifyItemRangeRemoved(0, sizeBefore);
                    adapter.notifyDataSetChanged();
                }*/

                // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items nside of bounds, then animate to center of the bounds.
                // Create the builder to collect all essential cluster items for the bounds.
                for (UserClusterItemVO item : cluster.getItems()) {
                    posSelectedClusterItem = item;
                    posSelectedCluster = cluster;

                    // TODO Add every cluster item to list
                    listUsersNearByClicked.add(item);
                }

                /*// Set Recycler view adapter here.
                adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
                rvUsers.setAdapter(adapter);
                setScrollListenerFriendsList();*/
                changePositionMyLocationButtomMaps();
                /*Log.d(TAG, "List_user_suggestions: " + adapter.getItemCount());*/
            }

            // TODO Set last item from cluster as selected
            if (posSelectedClusterItem != null) {
                if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                    listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(true);
                }
            }

            boolean isClusterItemSelected = false;

            int sumPercentage = 0;
            for (UserClusterItemVO item : cluster.getItems()) {
                sumPercentage = sumPercentage + item.getTastebudsmatchlevel();
                if (item.isSelected()) {
                    isClusterItemSelected = true;
                    posSelectedClusterItem = item;
                    posSelectedCluster = cluster;
                }
            }

            double levelAverage = sumPercentage / cluster.getSize();

            Log.d("CLUSTER_ITEM_FINAL_LEVEL", "Average percentage for cluster: " + levelAverage);
            viewClusterItem.setSelected(isClusterItemSelected);

            if (levelAverage >= 70) {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_green);
            } else if (levelAverage < 70 && levelAverage >= 30) {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_blue);
            } else {
                imgPin.setImageResource(R.drawable.selector_marker_custom_user_red);
            }

            Bitmap icon = mClusterIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
            markerOptions.snippet(String.valueOf(cluster.getSize()));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<UserClusterItemVO> cluster) {
        // Show a toast with some info when the cluster is clicked.
        /*String firstName = cluster.getItems().iterator().next().name;
        Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();*/

        // TODO deselect previous item selected.
//        if (posSelectedClusterItem < listUsersNearBy.size()) {
        if (posSelectedClusterItem != null) {
            if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(false);
            }
        }

        if (posSelectedClusterItem != null) {
            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
                reloadMarker(cluster, mClusterItemRenderer.getMarker(posSelectedCluster), mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
//                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
                reloadMarker(cluster, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
            }
        }

        // TODO Reset users list adapter as per cluster item click. User within cluster will be displayed in recycler view at bottom of the screen.
        listUsersNearByClicked.clear();
        if (adapter != null) {
            int sizeBefore = adapter.getItemCount();
            adapter.notifyItemRangeChanged(0, sizeBefore);
            adapter.notifyItemRangeRemoved(0, sizeBefore);
            adapter.notifyDataSetChanged();

            /*// TEST FOR CLUSTER CLICK
            Log.d(TAG, "Adapter update: " + adapter.getItemCount());
            adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
            rvUsers.setAdapter(adapter);
            Log.d(TAG, "Adapter update: " + adapter.getItemCount());*/
        }

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items nside of bounds, then animate to center of the bounds.
        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (UserClusterItemVO item : cluster.getItems()) {
            builder.include(item.getPosition());
            posSelectedClusterItem = item;
            posSelectedCluster = cluster;
            Log.d(TAG, "Position in list for items: " + item.getPosInList());

            /*if (rvUsers != null && llmRecyclerView != null && posToScrollRestaurantList != posSelectedClusterItem.getPosInList()) {*/
                /*llmRecyclerView.smoothScrollToPosition(rvUsers, null, posSelectedClusterItem.getPosInList());
                llmRecyclerView.scrollToPositionWithOffset(item.getPosInList(), 0);*/

            // TODO Add every cluster item to list
            listUsersNearByClicked.add(item);
            /*}*/
        }

        // Set Recycler view adapter here.
        /*if (adapter != null) {
            adapter.notifyItemRangeInserted(0, listUsersNearByClicked.size());
            adapter.notifyDataSetChanged();
        } else {*/
        adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
        rvUsers.setAdapter(adapter);
        /*}*/
        setScrollListenerFriendsList();

        changePositionMyLocationButtomMaps();
        Log.d(TAG, "List_user_suggestions: " + adapter.getItemCount());

        // TODO Set last item from cluster as selected
        if (posSelectedClusterItem != null) {
            if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(true);
            }
        }

        if (posSelectedClusterItem != null) {
            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
//                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedCluster), mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
                reloadMarker(cluster, mClusterItemRenderer.getMarker(posSelectedCluster), mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
//                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
                reloadMarker(cluster, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
            }
        }

        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            if (getMap() != null) {
                /** TRUSHIT - Map zoom level settings to zoom out slowly **/
//                getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, Constants.convertDpToPixels(75)));
//                getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, Constants.convertDpToPixels(100)));
                /*getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(),
                        (float) Math.floor(getMap().getCameraPosition().zoom + 1)), 300, null);*/
                getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(),
                        (float) (getMap().getCameraPosition().zoom + 0.5)), 300, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        mClusterManager.clearItems();
//        mClusterManager.addItems(listUsersNearBy);
        mClusterManager.cluster();

        Log.d(TAG, "Cluster clicked: " + cluster.getSize());
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<UserClusterItemVO> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(UserClusterItemVO item) {
        // Does nothing, but you could go into the user's profile page, for example.
        // TODO Set first item to selected & deselect previous item selected.
        if (posSelectedClusterItem != null) {
            if (posSelectedClusterItem.getPosInList() < listUsersNearBy.size()) {
                listUsersNearBy.get(posSelectedClusterItem.getPosInList()).setSelected(false);
            }
        }

        if (posSelectedClusterItem != null) {
            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedCluster),
                        mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
            }
        }

        item.setSelected(true);
        posSelectedCluster = null;
        posSelectedClusterItem = item;
        /*if (rvUsers != null && llmRecyclerView != null && posToScrollRestaurantList != posSelectedClusterItem.getPosInList()) {
            llmRecyclerView.smoothScrollToPosition(rvUsers, null, posSelectedClusterItem.getPosInList());
            llmRecyclerView.scrollToPositionWithOffset(item.getPosInList(), 0);
        }*/

        // TODO Reset users list adapter as per cluster item click. User within cluster will be displayed in recycler view at bottom of the screen.
        listUsersNearByClicked.clear();
        if (adapter != null) {
            int sizeBefore = adapter.getItemCount();
            adapter.notifyItemRangeChanged(0, sizeBefore);
            adapter.notifyItemRangeRemoved(0, sizeBefore);
            adapter.notifyDataSetChanged();

            /*// TEST FOR CLUSTER CLICK
            Log.d(TAG, "Adapter update: " + adapter.getItemCount());
            adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
            rvUsers.setAdapter(adapter);
            Log.d(TAG, "Adapter update: " + adapter.getItemCount());*/
        }

        // TODO Add every cluster item to list
        listUsersNearByClicked.add(item);

        // Set Recycler view adapter here.
        /*if (adapter != null) {
            adapter.notifyItemRangeInserted(0, listUsersNearByClicked.size());
            adapter.notifyItemRangeChanged(0, listUsersNearByClicked.size());
            adapter.notifyDataSetChanged();
        } else {*/
        adapter = new UsersHorizontalRecyclerAdapter(context, listUsersNearByClicked);
        rvUsers.setAdapter(adapter);
        /*}*/
        setScrollListenerFriendsList();

        changePositionMyLocationButtomMaps();

        Log.d(TAG, "List_user_suggestions: " + adapter.getItemCount() + ", List size: " + listUsersNearByClicked.size());

        if (posSelectedClusterItem != null) {
            if (posSelectedCluster != null && mClusterItemRenderer.getMarker(posSelectedCluster) != null) {
                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedCluster),
                        mClusterItemRenderer.getMarker(posSelectedCluster).getSnippet().trim());
            } else if (mClusterItemRenderer.getMarker(posSelectedClusterItem) != null) {
                reloadMarker(posSelectedClusterItem, mClusterItemRenderer.getMarker(posSelectedClusterItem), "1");
            }
        }
//        mClusterManager.clearItems();
//        mClusterManager.addItems(listUsersNearBy);
        mClusterManager.cluster();

        Log.d(TAG, "Cluster item clicked: " + posSelectedClusterItem);
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(UserClusterItemVO item) {
        // Does nothing, but you could go into the user's profile page, for example.
        mClusterItemRenderer.getMarker(item).hideInfoWindow();
    }

    /**
     * Workarround to repaint markers
     *
     * @param item item to repaint
     */
    public void reloadMarker(UserClusterItemVO item, Marker marker, String count) {
        final IconGenerator mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
        final TextView txtClusterCount;
        final View viewClusterItem;
        final ImageView imgPin;

        viewClusterItem = inflater.inflate(R.layout.layout_marker_custom_user_maps_activity, null);
        mClusterIconGenerator.setContentView(viewClusterItem);
        mClusterIconGenerator.setBackground(null);
        txtClusterCount = (TextView) viewClusterItem.findViewById(R.id.amu_text);
        imgPin = (ImageView) viewClusterItem.findViewById(R.id.imageViewMarkerCustomUserMaps);

        txtClusterCount.setText(count);

        Log.d(TAG, "Cluster item isSelected: " + item.isSelected() + ", First Item Details Cluster Item ==> " + item.getFirstname() + " " + item.getLastname()
                + ", Location: " + item.getLocationString());
        viewClusterItem.setSelected(item.isSelected());

        if (item.getTastebudsmatchlevel() >= 70) {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_green);
        } else if (item.getTastebudsmatchlevel() < 70 && item.getTastebudsmatchlevel() >= 30) {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_blue);
        } else {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_red);
        }

        Bitmap icon = mClusterIconGenerator.makeIcon();
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));

    }

    /**
     * Workarround to repaint cluster markers
     *
     * @param cluster cluster marker to repaint
     */
    public void reloadMarker(Cluster<UserClusterItemVO> cluster, Marker marker, String count) {
        final IconGenerator mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
        final TextView txtClusterCount;
        final View viewClusterItem;
        final ImageView imgPin;

        viewClusterItem = inflater.inflate(R.layout.layout_marker_custom_user_maps_activity, null);
        mClusterIconGenerator.setContentView(viewClusterItem);
        mClusterIconGenerator.setBackground(null);
        txtClusterCount = (TextView) viewClusterItem.findViewById(R.id.amu_text);
        imgPin = (ImageView) viewClusterItem.findViewById(R.id.imageViewMarkerCustomUserMaps);

        txtClusterCount.setText(count);

        boolean isSelected = false;
        int sumPercentage = 0;

        for (UserClusterItemVO item : cluster.getItems()) {
            Log.d(TAG, " First Item Details Cluster ==> " + item.getFirstname() + " " + item.getLastname()
                    + ", Location: " + item.getLocationString());
            if (item.isSelected()) {
                isSelected = true;
                sumPercentage = sumPercentage + item.getTastebudsmatchlevel();
            }
        }

        Log.d(TAG, "Cluster isSelected: " + isSelected);
        viewClusterItem.setSelected(isSelected);
        double levelAverage = sumPercentage / cluster.getSize();

        if (levelAverage >= 70) {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_green);
        } else if (levelAverage < 70 && levelAverage >= 30) {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_blue);
        } else {
            imgPin.setImageResource(R.drawable.selector_marker_custom_user_red);
        }

        Bitmap icon = mClusterIconGenerator.makeIcon();
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    public void setPlaceDetails(final Place place) {
        if (place != null) {
            isCurrentLocationEnabled = false;
            stopLocationUpdates();

//            if (selectedLocationLatLong != null && selectedLocationLatLong != place.getLatLng()) {
            selectedLocationLatLong = place.getLatLng();
            placeName = place.getName().toString().trim();
            Log.d("NAME_PLACE_DETAILS", "Place: " + place.getAddress().toString().trim());
                /*txtChangeLocation.setText(place.getName().toString().trim());*/
            strQuerySearchLocation = place.getAddress().toString().trim();
            autoCompleteTextViewChangeLocation.setText(place.getAddress().toString().trim());
            autoCompleteTextViewChangeLocation.setSelection(place.getAddress().toString().trim().length());

            // TODO Save Location Lat Lng to profile
            if (currentLatitude != selectedLocationLatLong.latitude && currentLongitude != selectedLocationLatLong.longitude && !isCurrentLocationEnabled) {
                currentLatitude = selectedLocationLatLong.latitude;
                currentLongitude = selectedLocationLatLong.longitude;

                /*isLocationUpdatedOnce = true;*/
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, place.getAddress().toString().trim());
                SharedPreferenceUtil.save();

            /*// Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails();*/

                stopProgress();

                if (!isLocationReceivedOnce) {
                    isLocationReceivedOnce = true;
                }

                // Save user latitude & longitude in background using edit profile API.
                showProgress(getString(R.string.loading));
//                sendRequestSaveProfileLocationAPI(place.getName().toString().trim());
                sendRequestSaveProfileLocationAPI(place.getAddress().toString().trim());
                    /*setUpMapOnScreen(true);*/
            }

            /*if (mMap != null) {
                mMap.clear();
            }

            mMapView.getMapAsync(CreateChallengesFirstStepActivity.this);*/

            Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getId())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                Place myPlace = places.get(0);

                                // Setup Geocoder
                                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                List<Address> addresses;

                                // Attempt to Geocode from place lat & long
                                try {
                                    addresses = geocoder.getFromLocation(myPlace.getLatLng().latitude, myPlace.getLatLng().longitude, 1);

                                   /* if (addresses.size() > 0) {*/
                                    for (int j = 0; j < addresses.size(); j++) {
                                        Address addr = addresses.get(j);
                                        Log.d("LOCATION_ADDRESS", "Address: " + addresses.get(0).toString().trim());
                                        String addressLines = "";
                                        for (int i = 0; i <= addr.getMaxAddressLineIndex(); i++) {
                                            if (i == 0) {
                                                addressLines = addr.getAddressLine(i);
                                            } else {
                                                addressLines = addressLines + ", " + addr.getAddressLine(i);
                                            }
                                        }
//                                        Log.d("LOCATION_ADDRESS_LINES", "Address: " + addressLines);

                                        /*txtChangeLocation.setText(addressLines.trim());*/

                                        if (addr.getPostalCode() != null) {

                                        }

                                        if (addr.getLocality() != null) {

                                        }

                                        if (addr.getAdminArea() != null && addr.getLocality() != null) {

                                        }

                                        if (addr.getCountryName() != null) {

                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.i("PLACE NOT FOUND ", "Place not found");
                            }

                            places.release();
                        }
                    });
//            }
        } else {

        }
    }

    private void sendRequestSaveProfileLocationAPI(String location_string) {
//        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_location), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
        params.put(getString(R.string.api_param_key_devicetype), "android");
        fields = fields + getString(R.string.api_param_key_devicetype);
        String address = getPlaceAddress().trim();
        if (address.trim().isEmpty()) {
            address = location_string;
        }

        if (!address.trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_location_string), address.trim());
        } else {
            params.put(getString(R.string.api_param_key_location_string), "null");
        }

        if (SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").equalsIgnoreCase("")) {
            fields = fields + "," + getString(R.string.api_param_key_location) + "," + getString(R.string.api_param_key_location_string);
            params.put(getString(R.string.api_param_key_fields), fields);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_location) + "," + getString(R.string.api_param_key_location_string)
                    + "," + getString(R.string.api_param_key_devicetoken);
            params.put(getString(R.string.api_param_key_fields), fields);
            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, ""));
        }

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, HomeActivity.this);
    }

    public void GetTimeMeasureBetweenCoOrdinates(int posToLoad) {
        Log.d(TAG, "Matrix API ID POS TO CALL: " + posToLoad);
        if (posToLoad < listUsersNearBy.size()) {
            String destination = "";

            destination = listUsersNearBy.get(posToLoad).getmPosition().latitude + "," + listUsersNearBy.get(posToLoad).getmPosition().longitude;

            String origin = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");

            if (!destination.trim().isEmpty() && !destination.trim().equalsIgnoreCase(",") && !origin.trim().equalsIgnoreCase(",")) {
                if (NetworkUtil.isOnline(context)) {
                    params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
                    params.put(getString(R.string.api_param_key_origins), origin.trim());
                    params.put(getString(R.string.api_param_key_destinations), destination.trim());
                    params.put(getString(R.string.api_param_key_units), getString(R.string.api_param_value_units));
                    params.put(getString(R.string.google_api_param_key_departure_time),
                            String.valueOf(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true)));
                    params.put(getString(R.string.google_api_param_key_traffic_model), getString(R.string.api_param_value_best_guess));
                    params.put(getString(R.string.api_param_key_key), getString(R.string.google_api_key));

                    new HttpRequestSingleton(context, getString(R.string.google_distance_matrix_api_url), params,
                            Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API, HomeActivity.this);
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.internet_not_available));
                }
            } else {

            }
        }
    }

    private String getPlaceAddress() {
        String place = "";
        double latitude = 0.0, longitude = 0.0;
        latitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")) : 0.0;
        longitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")) : 0.0;

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (addresses != null) {
                Log.d(TAG, "Get place detailed address: " + TextUtils.join(",", addresses));

                if (addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    if (city != null && !city.trim().isEmpty() && !city.trim().equalsIgnoreCase("null")) {
                        place = city;
                    }
                    if (state != null && !state.trim().isEmpty() && !state.trim().equalsIgnoreCase("null")) {
                        if (place.trim().isEmpty()) {
                            place = state;
                        } else {
                            place = place + ", " + state;
                        }

                    }
                    if (country != null && !country.trim().isEmpty() && !country.trim().equalsIgnoreCase("null")) {
                        if (country.trim().isEmpty()) {
                            place = country;
                        } else {
                            place = place + ", " + country;
                        }
                    }
                }
            }
        }
        return place;
    }

    private void FacebookMutualFriendAPI(final int position) {
//        MutualfriendsList = new ArrayList<>();
        Log.d(TAG, "Facebook mutual friends: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim()
                + ", User Account Type: " + listUsersNearBy.get(position).getAccount_type().trim()
                + ", User Name: " + listUsersNearBy.get(position).getName().trim()
                + ", Social ID: " + listUsersNearBy.get(position).getSocialId().trim());

        if (listUsersNearBy.get(position).getAccount_type().trim().equalsIgnoreCase("Facebook")
                && !listUsersNearBy.get(position).getSocialId().trim().isEmpty()
                && SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().equalsIgnoreCase("Facebook")
                && LoginManager.getInstance() != null && AccessToken.getCurrentAccessToken() != null
                && !AccessToken.getCurrentAccessToken().isExpired()) {
//                && listUsersNearBy.get(position).getName().trim().equalsIgnoreCase("Vishal Shah")) {

            Bundle params = new Bundle();
//        params.putString("fields", "context.fields(all_mutual_friends.fields(picture.width(500).height(200)))");
/* make the API call */
//        params.putString("fields", "context.fields(all_mutual_friends.fields(picture.width(500).height(200),name))");
            params.putString("fields", "context.fields(mutual_friends)");

//        params.putString("fields", "context.fields(all_mutual_friends.limit(100))");
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
//                "/4567579139970839",
//                "/100001563959235",
                    "/" + listUsersNearBy.get(position).getSocialId(),
                    params,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Log.d("FACEBOOK_MUTUAL_FRIENDS_API", "Response: " + response);
                            JSONObject graphObject = response.getJSONObject();

                            if (graphObject != null) {
                                Log.d("GRAPH", "" + graphObject.toString());
                                try {
                                    final JSONObject context = graphObject.getJSONObject("context");
                                    final String contextId = !context.isNull(getString(R.string.api_response_param_key_id))
                                            ? context.getString(getString(R.string.api_response_param_key_id)).trim() : "";

                                    Log.d("CONTEXTID", "" + contextId);
                                    GraphRequest request = GraphRequest.newGraphPathRequest(
                                            AccessToken.getCurrentAccessToken(),
                                            "/" + contextId + "/all_mutual_friends/",
                                            new GraphRequest.Callback() {
                                                @Override
                                                public void onCompleted(GraphResponse response) {
                                                    Log.i("allMutualFriend", "" + response.toString());

                                                    JSONObject graphObject = response.getJSONObject();

                                                    if (graphObject != null) {

                                                        ArrayList<MutualFriendsParcelableVO> MutualfriendsList = new ArrayList<>();
                                                        try {
                                                            JSONArray data = !graphObject.isNull(getString(R.string.api_response_param_key_data))
                                                                    ? graphObject.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                                                            for (int i = 0; i < data.length(); i++) {
                                                                JSONObject user = data.getJSONObject(i);
                                                                MutualFriendsParcelableVO mutualFriend = new MutualFriendsParcelableVO();
                                                                String name = !user.isNull(getString(R.string.api_response_param_key_name))
                                                                        ? user.getString(getString(R.string.api_response_param_key_name)).trim() : "";

                                                                JSONObject picture = !user.isNull(getString(R.string.api_response_param_key_picture))
                                                                        ? user.getJSONObject(getString(R.string.api_response_param_key_picture)) : null;

                                                                JSONObject pictureData = !picture.isNull(getString(R.string.api_response_param_key_data))
                                                                        ? picture.getJSONObject(getString(R.string.api_response_param_key_data)) : null;

                                                                String url = !pictureData.isNull(getString(R.string.api_response_param_key_url))
                                                                        ? pictureData.getString(getString(R.string.api_response_param_key_url)).trim() : "";
                                                                mutualFriend.setFirst_name(name);
                                                                mutualFriend.setLast_name("");
                                                                mutualFriend.setProfilepic1(url.trim());
                                                                MutualfriendsList.add(mutualFriend);
                                                            }

                                                            if (position < listUsersNearBy.size() && MutualfriendsList.size() > 0) {
                                                                listUsersNearBy.get(position).setMutualFriends(MutualfriendsList.size());
                                                                listUsersNearBy.get(position).setMutualFriendList(MutualfriendsList);
                                                                for (int k = 0; k < listUsersNearByClicked.size(); k++) {
                                                                    if (listUsersNearByClicked.get(k).getId().trim().
                                                                            equalsIgnoreCase(listUsersNearBy.get(position).getId().trim())) {
//                                                    Log.d(TAG, "Distance match for user position: "+k);
                                                                        Log.d(TAG, "Matrix API ID: " + listUsersNearBy.get(position).getId() + " "
                                                                                + "\nUser Id: " + listUsersNearByClicked.get(k).getId());
                                                                        listUsersNearByClicked.get(k).setMutualFriends(MutualfriendsList.size());
                                                                        listUsersNearByClicked.get(k).setMutualFriendList(MutualfriendsList);
                                                                        if (adapter != null) {
                                                                            Log.d(TAG, "Matrix API ID Notify data item change for position: " + k);
                                                                            adapter.notifyItemChanged(k);
                                                                            adapter.notifyDataSetChanged();
                                                                        }
                                                                    }
                                                                }
                                                                if (adapter != null) {
                                                                    adapter.notifyItemChanged(position);
                                                                    adapter.notifyDataSetChanged();
                                                                }
                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }

                                                        if (posToGetMutualFriends < listUsersNearBy.size() - 1) {
                                                            posToGetMutualFriends++;
                                                            FacebookMutualFriendAPI(posToGetMutualFriends);
                                                        }
                                                    } else {
                                                        if (posToGetMutualFriends < listUsersNearBy.size() - 1) {
                                                            posToGetMutualFriends++;
                                                            FacebookMutualFriendAPI(posToGetMutualFriends);
                                                        }
                                                    }
                                                    // Insert your code here
                                                }
                                            });
                                    request.executeAsync();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    if (posToGetMutualFriends < listUsersNearBy.size() - 1) {
                                        posToGetMutualFriends++;
                                        FacebookMutualFriendAPI(posToGetMutualFriends);
                                    }
                                }
                            } else {
                                if (posToGetMutualFriends < listUsersNearBy.size() - 1) {
                                    posToGetMutualFriends++;
                                    FacebookMutualFriendAPI(posToGetMutualFriends);
                                }
                            }
            /* handle the result */
                        }
                    }
            ).executeAsync();
        } else {
            if (posToGetMutualFriends < listUsersNearBy.size() - 1) {
                posToGetMutualFriends++;
                FacebookMutualFriendAPI(posToGetMutualFriends);
            }
        }
    }

    public void createDialogMutualFriendsList(ArrayList<MutualFriendsParcelableVO> listMutualFriends) {
        dialogMutualFriends = new Dialog(context);
        dialogMutualFriends.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogMutualFriends.setContentView(R.layout.layout_mutual_friends_list_dialog);

        /*WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);*/

        dialogMutualFriends.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ListView listViewMutualFriends;
        MutualFriendsListAdapter adapter;

        listViewMutualFriends = (ListView) dialogMutualFriends.findViewById(R.id.listViewMutualFriendsDialog);

        if (listMutualFriends.size() > 0) {
            View viewHeader = inflater.inflate(R.layout.layout_mutual_friends_adapter_header, null);
            listViewMutualFriends.addHeaderView(viewHeader, null, false);
        }

        adapter = new MutualFriendsListAdapter(context, listMutualFriends);
        listViewMutualFriends.setAdapter(adapter);

        dialogMutualFriends.setCancelable(true);
        dialogMutualFriends.show();

       /* WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        int dialogWidth = lp.width;
        int dialogHeight = lp.height;*/

       /* int width = dialog.findViewById(R.id.linearLayoutMutualFriendsRoot).getWidth();
        int height = dialog.findViewById(R.id.linearLayoutMutualFriendsRoot).getHeight();

        Log.d(TAG, "Dialog height: " + height);

        final int MAX_HEIGHT = (int) (heightScreen * 0.50);

        if (height > MAX_HEIGHT) {
            dialog.getWindow().setLayout(width, MAX_HEIGHT);
        }*/
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (dialogMutualFriends != null && dialogMutualFriends.getWindow() != null && dialogMutualFriends.getWindow().getDecorView() != null) {
            int width = dialogMutualFriends.getWindow().getDecorView().getWidth();
            int height = dialogMutualFriends.getWindow().getDecorView().getHeight();

            Log.d(TAG, "Dialog height: " + height);

            if (height > 0) {
                final int MAX_HEIGHT = (int) (heightScreen * 0.50);

                if (height > MAX_HEIGHT) {
                    dialogMutualFriends.getWindow().setLayout(width, MAX_HEIGHT);
                }
            }
        }
    }

    private void ApiViewProfile() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_profile_api).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, HomeActivity.this);
    }

    public void fetchPlaceDetailsSelected(AutocompletePrediction item) {
        final String placeId = item.getPlaceId();
        final CharSequence primaryText = item.getPrimaryText(null);

        Log.d(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        /*Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
                Toast.LENGTH_SHORT).show();*/
        Log.d(TAG, "Called getPlaceById to get Place details for " + placeId);
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.d(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // TODO Fetch place details and update API & View
            selectedLocationLatLong = place.getLatLng();

            /*SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(place.getLatLng().latitude).trim().replace(",", "."));
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(place.getLatLng().longitude).trim().replace(",", "."));
            SharedPreferenceUtil.save();*/

            relativeSearchPopup.setVisibility(View.GONE);
            linearSearchLocation.setVisibility(View.GONE);
            linearSearchCancel.setVisibility(View.GONE);
            linearSearchTastebuds.setVisibility(View.GONE);
//            txtChangeLocation.setVisibility(View.VISIBLE);
            relativeChangeLocation.setVisibility(View.VISIBLE);

            if (menuFabListFilter.isOpened()) {
                menuFabListFilter.close(true);
            }

            /*// Save user latitude & longitude in background using edit profile API.
            showProgress(getString(R.string.loading));
            sendRequestSaveProfileLocationAPI();*/

            setPlaceDetails(place);
            // Hide softkeyboard here.
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            Log.d(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     *
     * @param constraint Autocomplete query string
     * @return Results from the autocomplete API or null if the query was not successful.
     * @see Places#GEO_DATA_API#getAutocomplete(CharSequence)
     * @see AutocompletePrediction#freeze()
     */
    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Starting autocomplete query for: " + constraint);

            setLatlngBounds(new LatLng(currentLatitude, currentLongitude));

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);

//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), BOUNDS_GREATER_SYDNEY, mPlaceFilter);
                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);
//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), null, null);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                /*handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Error contacting API: " + status.toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/

                Log.d(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.d(TAG, "Query completed. Received " + autocompletePredictions.getCount() + " predictions.");

            /*Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(HomeActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);*/

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        Log.d(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    public void setLatlngBounds(LatLng center) {
        double radiusDegrees = 0.10;
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        mBounds = LatLngBounds.builder().include(northEast).include(southWest).build();

    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }

    }

    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                //Do your stuff on GPS status change
                if (mGoogleApiClient == null) {
                    // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
                    buildGoogleApiClient();
                }

                if (!mGoogleApiClient.isConnected()) {
                    createLocationRequest();
                    buildLocationSettingsRequest();
                    checkLocationPermissionGrantedOrNot();

                    if (!mGoogleApiClient.isConnected())
                        mGoogleApiClient.connect();
                } else {
                    if (isCurrentLocationEnabled) {
                        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                        getString(R.string.loading);
                        setUpMapOnScreen(true);
                    } else {
                        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                        getString(R.string.loading);
                        setUpMapOnScreen(true);
                    }
                }
            }
        }
    };

    private void changePositionMyLocationButtomMaps() {
        /*// get reference to my location icon
        View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        if (locationButton != null) {
            // and next place it, for example, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);

            rlp.setMargins(0, 0, Constants.convertDpToPixels(16), (Constants.convertDpToPixels(154)));
            locationButton.setLayoutParams(rlp);
        }*/
    }

    private void getFullAddressForCurrentLocation(double currentLatitude, double currentLongitude, boolean isReloadScreen) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.d(TAG, "Address current location: " + addresses.get(0).toString().trim());

            String address = addresses.get(0).getAddressLine(0);
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() : "";
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String locality = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() : "";
            String thoroughfare = addresses.get(0).getThoroughfare() != null ? addresses.get(0).getThoroughfare() : "";
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                   /* // Save user latitude & longitude in background using edit profile API.
                    sendRequestEditProfileDetails();*/

            if (isCurrentLocationEnabled) {
                String location_string = (!thoroughfare.trim().isEmpty() ? thoroughfare : "")
                        + (!thoroughfare.trim().isEmpty() && !city.trim().isEmpty() ? ", " : "")
                        + (!city.trim().isEmpty() ? city : "");

                autoCompleteTextViewChangeLocation.setText("");
                autoCompleteTextViewChangeLocation.setHint(getString(R.string.current_location)
                        + (thoroughfare.trim().isEmpty() && city.trim().isEmpty() ? "" : " - ")
                        + location_string);

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, location_string.trim());
                SharedPreferenceUtil.save();

                if (isReloadScreen) {
                    sendRequestSaveProfileLocationAPI(location_string.trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            /*stopProgress();*/
            if (isReloadScreen) {
                sendRequestSaveProfileLocationAPI("");
            }
        } catch (Exception e) {
            e.printStackTrace();
            /*stopProgress();*/
            if (isReloadScreen) {
                sendRequestSaveProfileLocationAPI("");
            }
        }
    }

    private void ApiEditProfile() {
        String fields = "";
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
//        params.put(getString(R.string.api_param_key_first_name), profile.getFirst_name().toString().trim());
//        params.put(getString(R.string.api_param_key_last_name), profile.getLast_name().toString().trim());
//        params.put(getString(R.string.api_param_key_about), profile.getAbout_me().toString().trim());
//        params.put(getString(R.string.api_param_key_occupation), profile.getOccupation().toString().trim());
//        params.put(getString(R.string.api_param_key_ethnicity), profile.getEthnicity().toString().trim());
//        params.put(getString(R.string.api_param_key_relationship), profile.getRelationship().toString().trim());
        params.put(getString(R.string.api_response_param_key_showme), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME,
                getString(R.string.gender_all)).trim());
        params.put(getString(R.string.api_response_param_key_search_distance), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "100"));
        params.put(getString(R.string.api_response_param_key_search_max_age), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100"));
        params.put(getString(R.string.api_response_param_key_search_min_age), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16"));
        params.put(getString(R.string.api_response_param_key_distance_unit),
                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server)).trim());
//        params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
//        params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
//        params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
//        params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
//        params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_showme);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_showme);
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_search_distance);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_search_distance);
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_search_max_age);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_search_max_age);
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_search_min_age);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_search_min_age);
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_distance_unit);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_distance_unit);
        }



        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_first_name);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_first_name);
        }*/

        /*if (!profile.getFirst_name().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_first_name), profile.getFirst_name().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_first_name), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_last_name);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_last_name);
        }*/

        /*if (!profile.getLast_name().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_last_name), profile.getLast_name().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_last_name), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_dob);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_dob);
        }*/

        /*if (!profile.getDob().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_dob), profile.getDob().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_dob), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_about);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_about);
        }*/

        /*if (!profile.getAbout_me().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_about), profile.getAbout_me().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_about), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_occupation);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_occupation);
        }*/

        /*if (!profile.getOccupation().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_occupation), profile.getOccupation().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_occupation), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_relationship);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_relationship);
        }*/

        /*if (!profile.getRelationship().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_relationship), profile.getRelationship().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_relationship), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_ethnicity);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_ethnicity);
        }

        if (!profile.getEthnicity().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_ethnicity), profile.getEthnicity().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_ethnicity), "null");
        }*/

        /*if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic1);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic1);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic1), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic2);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic2);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic2), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic3);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic3);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic3), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic4);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic4);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic4), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic5);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic5);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic5), "null");
        }*/

        params.put(getString(R.string.api_param_key_fields), fields);
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params, Constants.ACTION_CODE_PROFILE, HomeActivity.this);
    }

    private CameraUpdate getZoomForDistance(LatLng originalPosition, double distance) {
        LatLng rightBottom = SphericalUtil.computeOffset(originalPosition, distance, 135);
        LatLng leftTop = SphericalUtil.computeOffset(originalPosition, distance, -45);
        LatLngBounds sBounds = new LatLngBounds(new LatLng(rightBottom.latitude, leftTop.longitude), new LatLng(leftTop.latitude, rightBottom.longitude));
        return CameraUpdateFactory.newLatLngBounds(sBounds, 0);
    }


    private void setUsersRecyclerViewScrolling() {
        if (rvUsers != null) {
            rvUsers.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    setDateValue();
                }
            }, 300);
            rvUsers.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    rvUsers.smoothScrollToPosition(adapter.getItemCount() - 1);
//                    setDateValue();
                }
            }, 5000);
        }
        ViewTreeObserver vtoCards = rvUsers.getViewTreeObserver();
        vtoCards.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {


            @Override
            public boolean onPreDraw() {
                rvUsers.getViewTreeObserver().removeOnPreDrawListener(this);
                finalWidthCard = rvUsers.getMeasuredWidth();
//                itemWidthCard = getResources().getDimension(R.dimen.item_dob_width);
//                itemWidthCard = widthScreen; // - Constants.convertDpToPixels(16)
                itemWidthCard = Constants.convertDpToPixels(240); // - Constants.convertDpToPixels(16)
//                paddingCard = (finalWidthCard - itemWidthCard) / 2;
                paddingCard = 0;
//                paddingCard = Constants.convertDpToPixels(16);
                firstItemWidthCard = paddingCard;
//                firstItemWidthCard = 0;
                allPixelsCard = 0;

//                final LinearLayoutManager dateLayoutManager = new LinearLayoutManager(getApplicationContext());
//                llmRecyclerView = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                llmRecyclerView = new LinearLayoutManager(getApplicationContext());
                llmRecyclerView.setOrientation(LinearLayoutManager.HORIZONTAL);
                rvUsers.setLayoutManager(llmRecyclerView);
                rvUsers.setHasFixedSize(false);
                /*rvUsers.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        synchronized (this) {
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                calculatePositionAndScrollCard(recyclerView);
                            }
                        }

                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        allPixelsCard += dx;
                    }
                });*/
                if (listUsersNearByClicked == null) {
                    listUsersNearByClicked = new ArrayList<>();
                }
//                genLabelerDate();
                /*dateAdapter = new DateAdapter(labelerDates, (int) firstItemWidthDate);
                recyclerViewDate.setAdapter(dateAdapter);
                dateAdapter.setSelecteditem(dateAdapter.getItemCount() - 1);*/
                return true;
            }
        });
    }

    private void calculatePositionAndScrollCard(RecyclerView recyclerView) {
        /*int expectedPositionDate = Math.round((allPixelsCard + paddingCard - firstItemWidthCard) / itemWidthCard);

        if (expectedPositionDate <= -1) {
            expectedPositionDate = 0;
        } else if (expectedPositionDate >= recyclerView.getAdapter().getItemCount()) {
            expectedPositionDate--;
        }
        scrollListToPositionDate(recyclerView, expectedPositionDate);*/

    }

    /* this if most important, if expectedPositionDate < 0 recyclerView will return to nearest item*/
    private void scrollListToPositionDate(RecyclerView recyclerView, int expectedPositionDate) {
        /*float targetScrollPosDate = expectedPositionDate * itemWidthCard + firstItemWidthCard - paddingCard;
        float missingPxDate = targetScrollPosDate - allPixelsCard;
        if (missingPxDate != 0) {
            recyclerView.smoothScrollBy((int) missingPxDate, 0);
        }*/
//        setDateValue();
    }

    public void showDialogFilterOptionsMap() {
        // Create custom dialog object
        final Dialog dialogFilter = new Dialog(this);
        dialogFilter.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout.LayoutParams dialogFilterParams = new LinearLayout.LayoutParams(Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);
        //set height(300) and width(match_parent) here, ie (width,height)

        LayoutInflater inflaterFilter = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dislogViewFilter = inflaterFilter.inflate(R.layout.layout_dialog_age_filter, null);

        Button btnSearchFilter = (Button) dislogViewFilter.findViewById(R.id.btnSearch);
        final Button btnFemaleFilter = (Button) dislogViewFilter.findViewById(R.id.btnFemale);
        final Button btnMaleFilter = (Button) dislogViewFilter.findViewById(R.id.btnMale);
        final Button btnLgbtFilter = (Button) dislogViewFilter.findViewById(R.id.btnLgbt);
        final TextView textViewGenderFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewGender);
        final TextView textViewAgeRangeFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewAgeRange);
        final TextView textViewDistanceFilter = (TextView) dislogViewFilter.findViewById(R.id.textViewDistance);
        final RangeBar rangebarAgeFilter = (RangeBar) dislogViewFilter.findViewById(R.id.rangebarAge);
        final SeekBar seekBarSearchDistanceMap = (SeekBar) dislogViewFilter.findViewById(R.id.seekBarSearchDistanceMap);
        final ImageView imageViewCloseFilter = (ImageView) dislogViewFilter.findViewById(R.id.imageViewClose);

        seekBarSearchDistanceMap.setProgress(Integer.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "")));
        rangebarAgeFilter.setRangePinsByValue(Float.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "")),
                Float.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "")));
        distanceSelected = Integer.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, ""));

        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("mi")) {
            textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));
            seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_MI);
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("km")) {
            textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));
            seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_KM);
        } else {
            textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));
            seekBarSearchDistanceMap.setMax(MAX_DISTANCE_RANGE_MI);
        }

        textViewAgeRangeFilter.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "")
                + "-" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "")); // + " Yrs"

        // TODO Retrive users gender prefrence and set button selected By default male is selected
        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").trim().toLowerCase()
                .equalsIgnoreCase(getString(R.string.gender_male_value))) {
            btnMaleFilter.setSelected(true);
            btnFemaleFilter.setSelected(false);
            btnLgbtFilter.setSelected(false);
            textViewGenderFilter.setText(getString(R.string.gender_male));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
                .equalsIgnoreCase(getString(R.string.gender_female_value))) {
            btnMaleFilter.setSelected(false);
            btnLgbtFilter.setSelected(false);
            btnFemaleFilter.setSelected(true);
            textViewGenderFilter.setText(getString(R.string.gender_female));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
                .equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
            btnMaleFilter.setSelected(false);
            btnFemaleFilter.setSelected(false);
            btnLgbtFilter.setSelected(true);
            textViewGenderFilter.setText(getString(R.string.gender_lgbt));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
                .equalsIgnoreCase(getString(R.string.gender_male_female_value))) {
            btnMaleFilter.setSelected(true);
            btnFemaleFilter.setSelected(true);
            btnLgbtFilter.setSelected(false);
            textViewGenderFilter.setText(getString(R.string.gender_male_female));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
                .equalsIgnoreCase(getString(R.string.gender_male_lgbt_value))) {
            btnMaleFilter.setSelected(true);
            btnFemaleFilter.setSelected(false);
            btnLgbtFilter.setSelected(true);
            textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").toLowerCase().trim()
                .equalsIgnoreCase(getString(R.string.gender_female_lgbt_value))) {
            btnMaleFilter.setSelected(false);
            btnFemaleFilter.setSelected(true);
            btnLgbtFilter.setSelected(true);
            textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
        } else {
            btnMaleFilter.setSelected(true);
            btnFemaleFilter.setSelected(true);
            btnLgbtFilter.setSelected(true);
            textViewGenderFilter.setText(getString(R.string.gender_all));
        }

        btnMaleFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfromButton = true;

                if (btnMaleFilter.isSelected()) {
                    if (btnFemaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
                        btnMaleFilter.setSelected(false);
                        textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnFemaleFilter.isSelected()) {
                            btnMaleFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_female));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
                            SharedPreferenceUtil.save();
                        } else if (btnLgbtFilter.isSelected()) {
                            btnMaleFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                } else {
                    btnMaleFilter.setSelected(true);
                    if (btnFemaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
                        textViewGenderFilter.setText(getString(R.string.gender_all));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnFemaleFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_male_female));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
                            SharedPreferenceUtil.save();
                        } else if (btnLgbtFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
                            SharedPreferenceUtil.save();
                        } else {
                            textViewGenderFilter.setText(getString(R.string.gender_male));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                }
            }
        });

        btnFemaleFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfromButton = true;

                if (btnFemaleFilter.isSelected()) {
                    if (btnMaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
                        btnFemaleFilter.setSelected(false);
                        textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnMaleFilter.isSelected()) {
                            btnFemaleFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_male));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
                            SharedPreferenceUtil.save();
                        } else if (btnLgbtFilter.isSelected()) {
                            btnFemaleFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                } else {
                    btnFemaleFilter.setSelected(true);
                    if (btnMaleFilter.isSelected() && btnLgbtFilter.isSelected()) {
                        textViewGenderFilter.setText(getString(R.string.gender_all));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnMaleFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_male_female));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
                            SharedPreferenceUtil.save();
                        } else if (btnLgbtFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
                            SharedPreferenceUtil.save();
                        } else {
                            textViewGenderFilter.setText(getString(R.string.gender_female));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                }
            }
        });

        btnLgbtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isfromButton = true;

                if (btnLgbtFilter.isSelected()) {
                    if (btnMaleFilter.isSelected() && btnFemaleFilter.isSelected()) {
                        btnLgbtFilter.setSelected(false);
                        textViewGenderFilter.setText(getString(R.string.gender_male_female));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_female_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnMaleFilter.isSelected()) {
                            btnLgbtFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_male));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_value));
                            SharedPreferenceUtil.save();
                        } else if (btnFemaleFilter.isSelected()) {
                            btnLgbtFilter.setSelected(false);
                            textViewGenderFilter.setText(getString(R.string.gender_female));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                } else {
                    btnLgbtFilter.setSelected(true);
                    if (btnMaleFilter.isSelected() && btnFemaleFilter.isSelected()) {
                        textViewGenderFilter.setText(getString(R.string.gender_all));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_all_value));
                        SharedPreferenceUtil.save();
                    } else {
                        if (btnMaleFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_male_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_male_lgbt_value));
                            SharedPreferenceUtil.save();
                        } else if (btnFemaleFilter.isSelected()) {
                            textViewGenderFilter.setText(getString(R.string.gender_female_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_female_lgbt_value));
                            SharedPreferenceUtil.save();
                        } else {
                            textViewGenderFilter.setText(getString(R.string.gender_lgbt));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, getString(R.string.gender_lgbt_value));
                            SharedPreferenceUtil.save();
                        }
                    }
                }
            }
        });

        age1 = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16");
        age2 = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100");

        rangebarAgeFilter.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
                age1 = leftPinValue;
                age2 = rightPinValue;

                try {
                    if (Integer.parseInt(age1.trim().replaceAll(",", ".")) < 16) {
                        age1 = "16";
                    }

                    if (Integer.parseInt(age1.trim().replaceAll(",", ".")) > 100) {
                        age1 = "100";
                    }

                    if (Integer.parseInt(age2.trim().replaceAll(",", ".")) > 100) {
                        age2 = "100";
                    }

                    if (Integer.parseInt(age2.trim().replaceAll(",", ".")) < 16) {
                        age2 = "16";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, String.valueOf(age1));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, String.valueOf(age2));
                SharedPreferenceUtil.save();

                textViewAgeRangeFilter.setText(age1 + " - " + age2); // + "Yrs"
            }
        });

        seekBarSearchDistanceMap.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) { // If the changes are from the User only then accept the changes
                    distanceSelected = (int) ((float) progress / 100 * (mMaxValue - mMinValue) + mMinValue);
                    distanceSelected *= 10;
                    distanceSelected = Math.round(distanceSelected);
                    distanceSelected /= 10;
                    Log.i("distanceSelected", "" + distanceSelected);
                }

                if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, "").trim().toLowerCase()
                        .contains(getResources().getString(R.string.distance_unit_km_value_server))) {
                    if (distanceSelected > 160) {
                        distanceSelected = 160;
                    }

                    textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));
                    if (!isfromButton)
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, String.valueOf(distanceSelected));
                    SharedPreferenceUtil.save();
                } else {
                    if (distanceSelected > 100) {
                        distanceSelected = 100;
                    }

                    textViewDistanceFilter.setText(Integer.toString(distanceSelected) + " " + getString(R.string.prefer_distance_mi));
                    if (!isfromButton)
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, String.valueOf(distanceSelected));
                    SharedPreferenceUtil.save();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btnSearchFilter.setSelected(true);
        btnSearchFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFilter.dismiss();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);
                linearSearchLocation.setVisibility(View.GONE);
                linearSearchCancel.setVisibility(View.GONE);
                linearSearchTastebuds.setVisibility(View.GONE);
//                        txtChangeLocation.setVisibility(View.VISIBLE);
                relativeChangeLocation.setVisibility(View.VISIBLE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }

                ApiEditProfile();
            }
        });

        imageViewCloseFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFilter.dismiss();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);
                linearSearchLocation.setVisibility(View.GONE);
                linearSearchCancel.setVisibility(View.GONE);
                linearSearchTastebuds.setVisibility(View.GONE);
//                        txtChangeLocation.setVisibility(View.VISIBLE);
                relativeChangeLocation.setVisibility(View.VISIBLE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });

        dialogFilter.setContentView(dislogViewFilter, dialogFilterParams);
        dialogFilter.show();
    }
}
