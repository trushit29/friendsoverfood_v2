package com.friendsoverfood.android.ListRestaurantForMessage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 27/02/17.
 */

//https://lh3.googleusercontent.com/proxy/Tekn01yxOmARrJl7GL4cbHL4fMtjo-FLqe16dqJXDknsj1GtXnamthHomzzwKWZOWgFINrHxVTF3Tbd8WaXCf33rbsYhz0IR3K8hRopuxBoWhBgDH7tV6nuCv_6-gUGV_doPwr5l8dv3V7DOI6YRVoFHVdgnjw=w408-h233
//https://lh4.googleusercontent.com/-cyXTjcaJbes/WEarsEZhmwI/AAAAAAAAF2s/piMUDL7TD2AN7pnV8LwC29q11mo1pSAWwCLIB/s408-k-no/
public class RestaurantListAdapterMessage extends RecyclerView.Adapter<RestaurantListAdapterMessage.ViewHolder> {
    private Context context;
    private ArrayList<RestaurantListVO> list;
    private Intent intent;
    private LayoutInflater mInflater;
    private ChatDetailsActivity activity;

    public RestaurantListAdapterMessage(Context context, ArrayList<RestaurantListVO> list) {
        this.context = context;
        this.list = list;
        mInflater = LayoutInflater.from(context);
        this.activity = (ChatDetailsActivity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_restaurant_list_adapter_chat, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        convertView.setTag(holder);
        holder.txtName.setText(list.get(position).getName().trim());
        holder.txtTimings.setText(list.get(position).getTimings().trim());
        holder.txtRating.setText(String.valueOf(list.get(position).getRatings()).trim() + " " + context.getResources().getString(R.string.rating));
//        holder.txtPriceRange.setText("₹₹₹");
        if (list.get(position).getOpen_now().equalsIgnoreCase("true"))
            holder.txtCuisine.setText("Open Now");
        else if (list.get(position).getOpen_now().equalsIgnoreCase("false"))
            holder.txtCuisine.setText("Closed Now");
        else {
            holder.txtCuisine.setText("Timings not Available");
        }

        holder.txtAddress.setText(list.get(position).getVicinity().trim());
        if (!list.get(position).getRatings().trim().isEmpty())
            holder.ratingBar.setRating(Float.valueOf(list.get(position).getRatings()));

        final ViewHolder finalHolder = holder;
        final int pos = position;

        if (!list.get(position).getPhotoReference().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getPhotoReference().trim()).resize(Constants.convertDpToPixels(92), Constants.convertDpToPixels(104))
                    .centerCrop().placeholder(R.drawable.ic_placeholder_restaurant).error(R.drawable.ic_placeholder_restaurant)
                    .into(holder.imgPic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getPhotoReference().trim());
                        }
                    });
        } else {
            holder.imgPic.setImageResource(R.drawable.ic_placeholder_restaurant);
        }

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close drawer menu if open
                if (activity.drawerMenuRoot.isDrawerOpen(Gravity.RIGHT)) {
                    activity.drawerMenuRoot.closeDrawer(Gravity.RIGHT);
                }
                activity.WriteMessageResto(list.get(pos));
                list.remove(pos);
                activity.adapterResto.notifyItemRangeRemoved(pos, list.size());
//
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txtRating, txtPriceRange, txtCuisine, txtAddress, txtTimings;
        //        public SwitchCompat switchNotifications;
        public LinearLayout linearRoot;
        public ImageView imgPic;
        public RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);

            txtName = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantName);
            txtPriceRange = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantPriceRange);
            txtRating = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantRatings);
            txtCuisine = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantCuisine);
            txtAddress = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantAddress);
            txtTimings = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantTimings);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListAdapterRestaurantsRatings);
            imgPic = (ImageView) view.findViewById(R.id.imageViewRestaurantListAdapterRestaurantPicture);
            linearRoot = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListAdapterRootChat);
            activity.setAllTypefaceMontserratRegular(view);
        }
    }
}

