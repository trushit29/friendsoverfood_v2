package com.friendsoverfood.android.RestaurantsList;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Trushit on 27/02/17.
 */

public class RestaurantListVO implements Serializable {
    public String name = "", url = "", cuisine = "", address = "", timings = "", description = "", websiteUrl = "", phoneNumber = "";
    public int priceRange = 1, posInList = 0;
    public String latitude = "";
    public String longitude = "";
    public String mSnippet = "";
    public String reference = "";
    public String vicinity = "";
    public String photoReference = "";
    public String timeToReach = "";
    public String ratings = "";
    public String open_now = "";
    public boolean isSelected = false;
    public String placeid = "";
    String formatted_address = "", formatted_phone_number = "", website = "";
    public ArrayList<RestaurantReviewVO> listReview = new ArrayList<>();
    public ArrayList<String> photos = new ArrayList<>();
    public ArrayList<String> cuisinesDetails = new ArrayList<>();

    public RestaurantListVO() {
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> photos) {
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(int priceRange) {
        this.priceRange = priceRange;
    }

    public int getPosInList() {
        return posInList;
    }

    public void setPosInList(int posInList) {
        this.posInList = posInList;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getmSnippet() {
        return mSnippet;
    }

    public void setmSnippet(String mSnippet) {
        this.mSnippet = mSnippet;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public String getTimeToReach() {
        return timeToReach;
    }

    public void setTimeToReach(String timeToReach) {
        this.timeToReach = timeToReach;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getOpen_now() {
        return open_now;
    }

    public void setOpen_now(String open_now) {
        this.open_now = open_now;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPlaceid() {
        return placeid;
    }

    public void setPlaceid(String placeid) {
        this.placeid = placeid;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getFormatted_phone_number() {
        return formatted_phone_number;
    }

    public void setFormatted_phone_number(String formatted_phone_number) {
        this.formatted_phone_number = formatted_phone_number;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public ArrayList<RestaurantReviewVO> getListReview() {
        return listReview;
    }

    public void setListReview(ArrayList<RestaurantReviewVO> listReview) {
        this.listReview = listReview;
    }

    public ArrayList<String> getCuisinesDetails() {
        return cuisinesDetails;
    }

    public void setCuisinesDetails(ArrayList<String> cuisinesDetails) {
        this.cuisinesDetails = cuisinesDetails;
    }
}
