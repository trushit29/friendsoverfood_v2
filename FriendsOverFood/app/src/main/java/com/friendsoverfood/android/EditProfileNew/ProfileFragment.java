package com.friendsoverfood.android.EditProfileNew;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

import java.util.Calendar;

/**
 * Created by ashish on 04/01/17.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_OBJECT = "object";
    public EditProfileNewActivity activity;

    //    public TextView txtSelectTastebuds;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.

        activity = (EditProfileNewActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_profile_layout, container, false);
        Bundle args = getArguments();
//        ((TextView) rootView.findViewById(android.R.id.text1)).setText(
//                Integer.toString(args.getInt(ARG_OBJECT)));

       /* activity.text_chip_layout = (ChipView) view.findViewById(R.id.text_chip_layout_edit);
        // Adapter
        activity.adapterLayout = new EditProfileChipViewAdapter(getContext());
        activity.txtViewNoTastebuds = (TextView) view.findViewById(R.id.txtViewNoTastebuds);*/

//        txtSelectTastebuds = (TextView) view.findViewById(R.id.textViewSelectTastebuds);
        activity.imgDummyProfile = (ImageView) view.findViewById(R.id.imageViewProfileFragmentDummy);
        activity.btnSelectTastebuds = (Button) view.findViewById(R.id.buttonProfileFragmentSelectTastebuds);
        activity.relativeRootProfileFragment = (RelativeLayout) view.findViewById(R.id.relativeLayoutProfileFragmentRoot);

//        activity.textViewEditTasteBuds = (LinearLayout) view.findViewById(R.id.textViewEditTasteBuds);
        activity.editTextOccupation = (EditText) view.findViewById(R.id.editTextOccupation);
        activity.editTextEducation = (EditText) view.findViewById(R.id.editTextEducation);
        activity.editTextEthnicity = (EditText) view.findViewById(R.id.editTextEthnicity);
        activity.editTextRelationShipStatus = (EditText) view.findViewById(R.id.editTextRelationShipStatus);
        activity.editTextAbout = (EditText) view.findViewById(R.id.editTextAbout);
        activity.textInputLayoutSignupActivityEthinicity = (TextInputLayout) view.findViewById(R.id.textInputLayoutSignupActivityEthinicity);
        activity.editTextLastName = (EditText) view.findViewById(R.id.editTextLastName);
        activity.editTextBirthday = (EditText) view.findViewById(R.id.editTextBirthday);
        activity.editTextGender = (EditText) view.findViewById(R.id.editTextGender);
        activity.linearLayoutSelectGender = (LinearLayout) view.findViewById(R.id.linearLayoutSelectGender);
        activity.btnMale = (Button) view.findViewById(R.id.btnMale);
        activity.btnFemale = (Button) view.findViewById(R.id.btnFemale);
        activity.btnLgbt = (Button) view.findViewById(R.id.btnLgbt);
        activity.editTextName = (EditText) view.findViewById(R.id.editTextName);
        activity.restoImageSlider = (SliderLayout) view.findViewById(R.id.slider);
        activity.relativeSlider = (RelativeLayout) view.findViewById(R.id.relativeSlider);
        activity.txtViewCounter = (TextView) view.findViewById(R.id.txtViewCounter);
        activity.spinner = (Spinner) view.findViewById(R.id.spinner);

        /*activity.imageViewEditProfileActivityUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage1);
        activity.imageViewEditProfileActivityUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage2);
        activity.imageViewEditProfileActivityUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage3);
        activity.imageViewEditProfileActivityUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage4);
//        activity.imageViewEditProfileActivityUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage5);
        activity.imageViewEditProfileActivityAddUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage1);
        activity.imageViewEditProfileActivityAddUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage2);
        activity.imageViewEditProfileActivityAddUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage3);
        activity.imageViewEditProfileActivityAddUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage4);
//        activity.imageViewEditProfileActivityAddUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage5);*/

//        activity.editTextAbout.setHint(getString(R.string.about) + " " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "").trim());

        // Set layout params for Images/Upload images
//        activity.sizeImageUpload = (activity.widthScreen/2) - Constants.convertDpToPixels(28) - Constants.convertDpToPixels(16);
       /* activity.sizeImageUpload = activity.imageViewEditProfileActivityUserImage1.getLayoutParams().width;

        RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(activity.sizeImageUpload, activity.sizeImageUpload);
        activity.imageViewEditProfileActivityUserImage1.setLayoutParams(paramsImage);
        activity.imageViewEditProfileActivityUserImage2.setLayoutParams(paramsImage);
        activity.imageViewEditProfileActivityUserImage3.setLayoutParams(paramsImage);
        activity.imageViewEditProfileActivityUserImage4.setLayoutParams(paramsImage);*/

        // Change height of slider to same as screen width
        activity.relativeSlider.getLayoutParams().height = activity.widthScreen;
        activity.restoImageSlider.getLayoutParams().height = activity.widthScreen;

       /* activity.text_chip_layout.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
                Log.d("clicked", "clicked");
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                TastebudChipVO tag = (TastebudChipVO) chip;
                tag.setType(tag.getType() == 0 ? 1 : 0);
                activity.text_chip_layout.refresh();
                for (int i = 0; i < activity.listUserTastebuds.size(); i++) {
                    if (activity.listUserTastebuds.get(i).getText().equalsIgnoreCase(tag.getText())) {
                        activity.listUserTastebuds.remove(i);
                        activity.initLayoutSuggestionsChips();
                    }
                }
            }
        });*/

        activity.isRevertUnsavedChanges = true;
        activity.setAllTypefaceMontserratRegular(view);
//        activity.setAllTypefaceMontserratRegularItalic(txtSelectTastebuds);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            default:
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showValueOfProfile();

        /*activity.imageViewEditProfileActivityAddUserImage1.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage2.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage3.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage4.setOnClickListener(activity);
//        activity.imageViewEditProfileActivityAddUserImage5.setOnClickListener(activity);

        activity.imageViewEditProfileActivityUserImage1.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage2.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage3.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage4.setOnClickListener(activity);*/
//        activity.imageViewEditProfileActivityUserImage5.setOnClickListener(activity);

        /*activity.text_chip_layout.setOnClickListener(activity);*/
//        activity.textViewEditTasteBuds.setOnClickListener(activity);
        activity.btnSelectTastebuds.setOnClickListener(activity);
        activity.editTextBirthday.setOnClickListener(activity);
        activity.editTextGender.setOnClickListener(activity);
        activity.btnMale.setOnClickListener(activity);
        activity.btnFemale.setOnClickListener(activity);
        activity.btnLgbt.setOnClickListener(activity);
        activity.editTextRelationShipStatus.setOnClickListener(activity);
        activity.hideKeyboard(activity);



        /*ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        String[] items = getResources().getStringArray(R.array.planets_array);
        activity.spinner.setAdapter(adapter);*/

        String[] items = getResources().getStringArray(R.array.planets_array);
        SpinnerCustomAdapter adapterSpinner = new SpinnerCustomAdapter(activity, items);
        activity.spinner.setAdapter(adapterSpinner);

        for (int i = 0; i < items.length; i++) {
            if (items[i].equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, ""))) {
                activity.spinner.setSelection(i);
            }
        }

        activity.date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.myCalendar.set(Calendar.YEAR, year);
                activity.myCalendar.set(Calendar.MONTH, monthOfYear + 1);
                activity.myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                activity.editTextBirthday.setText(String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth) + "/" + String.valueOf(year));
                activity.profile.setDob(activity.editTextBirthday.getText().toString());

            }

        };

        activity.editTextRelationShipStatus.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View arg0, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    activity.spinner.setVisibility(View.VISIBLE);
                }
            }
        });

        activity.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.profile.setRelationship(activity.spinner.getSelectedItem().toString());

                activity.editTextRelationShipStatus.setText(activity.spinner.getSelectedItem().toString()); //this is taking the first value of the spinner by default.

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        /*if(activity.isRevertUnsavedChanges) {
            activity.isRevertUnsavedChanges = false;
            activity.isUnsavedChanges = false;
        }else{
            activity.isUnsavedChanges = true;
        }*/
    }
}
