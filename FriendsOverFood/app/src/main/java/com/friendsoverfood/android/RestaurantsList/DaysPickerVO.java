package com.friendsoverfood.android.RestaurantsList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

/**
 * Created by Trushit on 08/03/17.
 */

public class DaysPickerVO implements Parcelable {
    public Calendar calendarDay = Calendar.getInstance();
    public String strDay = "";

    public DaysPickerVO() {
    }

    protected DaysPickerVO(Parcel in) {
        strDay = in.readString();
    }

    public static final Creator<DaysPickerVO> CREATOR = new Creator<DaysPickerVO>() {
        @Override
        public DaysPickerVO createFromParcel(Parcel in) {
            return new DaysPickerVO(in);
        }

        @Override
        public DaysPickerVO[] newArray(int size) {
            return new DaysPickerVO[size];
        }
    };

    public Calendar getCalendarDay() {
        return calendarDay;
    }

    public void setCalendarDay(Calendar calendarDay) {
        this.calendarDay = calendarDay;
    }

    public String getStrDay() {
        return strDay;
    }

    public void setStrDay(String strDay) {
        this.strDay = strDay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable((Calendar) calendarDay);
        dest.writeString(strDay);
    }
}
