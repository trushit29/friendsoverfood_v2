package com.friendsoverfood.android.util;

import java.util.Comparator;
import java.util.Map;

class HashMapComparatorStringIntegerDescending implements
		Comparator<Map<String, String>> {

	private final String key;

	public HashMapComparatorStringIntegerDescending(String key) {
		this.key = key;
	}

	public int compare(Map<String, String> first, Map<String, String> second) {
		int firstValue = Integer.parseInt(first.get(key));
		int secondValue = Integer.parseInt(second.get(key));
		return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
	}
}
