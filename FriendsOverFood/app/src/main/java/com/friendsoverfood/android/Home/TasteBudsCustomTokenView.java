package com.friendsoverfood.android.Home;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.R;
import com.tokenautocomplete.TokenCompleteTextView;

/**
 * Created by Trushit on 24/04/17.
 */

public class TasteBudsCustomTokenView extends TokenCompleteTextView<TasteBudsToken> {

    public TasteBudsCustomTokenView(Context context) {
        super(context);
    }

    public TasteBudsCustomTokenView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TasteBudsCustomTokenView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(TasteBudsToken token) {

        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) l.inflate(R.layout.layout_tastebuds_token_view, (ViewGroup) TasteBudsCustomTokenView.this.getParent(), false);
        TextView name = ((TextView) view.findViewById(R.id.name));
        name.setText(token.getName());
        if (token.isSelected()) {
            name.setSelected(true);
        }

        if (token.getType() != -1) {
            if (token.getType() == 2) {
                name.setTextColor(getResources().getColor(R.color.pin_tastebuds_match_green));
                name.setBackground(getResources().getDrawable(R.drawable.selector_tastebuds_token_white_bg_green_border));
            } else if (token.getType() == 2) {
                name.setTextColor(getResources().getColor(R.color.pin_tastebuds_match_blue));
                name.setBackground(getResources().getDrawable(R.drawable.selector_tastebuds_token_white_bg_blue_border));
            } else {
                name.setTextColor(getResources().getColor(R.color.pin_tastebuds_match_red));
                name.setBackground(getResources().getDrawable(R.drawable.selector_tastebuds_token_white_bg_red_border));
            }
        } else {
            if (token.isSelected()) {
                name.setTextColor(getResources().getColor(R.color.pin_tastebuds_match_red));
                name.setBackground(getResources().getDrawable(R.drawable.selector_tastebuds_token_white_bg_black_red_border_selected));
                name.setSelected(true);
            } else {
                name.setTextColor(getResources().getColor(R.color.pin_tastebuds_match_black));
                name.setBackground(getResources().getDrawable(R.drawable.selector_tastebuds_token_white_bg_black_red_border_selected));
                name.setSelected(false);
            }
        }

        setAllTypefaceMontserratLight(name);

        return view;
    }

    @Override
    protected TasteBudsToken defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        /*int index = completionText.indexOf('@');
        if (index == -1) {
            return new Person(completionText, completionText.replace(" ", "") + "@gmail.com");
        } else {*/
        return new TasteBudsToken(completionText, completionText, 0);
        /*}*/
    }

    public void setAllTypefaceMontserratLight(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratLight(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getContext().getResources().getAssets(), "Montserrat-Light.otf"));
            }
        }
    }
}
