package com.friendsoverfood.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.friendsoverfood.android.ChatNew.ChatListActivity;
import com.friendsoverfood.android.ContactUs.ContactUs;
import com.friendsoverfood.android.EditProfileNew.EditProfileNewActivity;
import com.friendsoverfood.android.FriendRequest.PendingRequestActivity;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.Welcome.WelcomeActivity;
import com.friendsoverfood.android.storage.DbConstants;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity implements OnClickListener, View.OnLongClickListener, HttpCallback {
    public static int heightScreen = 0, widthScreen = 0;
    protected Context mContext;
    public Handler handler = new Handler();
    LayoutInflater inflater;
    private ProgressDialog pd;
    public Intent intent;
    public HashMap<String, String> params = new HashMap<>();

    // base activity view elements
    public CoordinatorLayout coordinatorRoot;
    public FrameLayout frameContentRoot;
    public DrawerLayout drawerMenuRoot;
    public RelativeLayout relativeMenuRoot;
    public LinearLayout linearContentMain;
    private RelativeLayout relativeProgressRoot;
    private ProgressBar progressBarRoot;
    public Toolbar toolbarLayoutRoot; //toolBarBaseRoot
    public TextView txtTitleToolbarCentered, txtToolbarSkipLeft;
    public LinearLayout linearToolBarCustomLayout, linearFooterTabsBase;
    public ImageView imgFooterTabsFriends, imgFooterTabsChat, imgFooterTabsSearch, imgFooterTabsProfile, imgFooterTabsAbout;

    public boolean mIsReceiverRegistered = false;
    public MyBroadcastReceiver mReceiver;
    public BroadcastReceiver mNotificationReceiver;

    //    public View viewDrawerMenuHeader, viewDrawerMenuFooter;

    // Drawer menu elements
    private ActionBarDrawerToggle mDrawerToggle;
    private int widthDrawerMenu;
    public LinearLayout linearMenuHeader;
    public CircularImageView imgMenuUserProfilePic;
    public TextView txtMenuUserName, txtMenuUserProfession;
    public ListView listViewMenuItems;
    public DrawerMenuAdapter adapterDrawerMenu;
    public ArrayList<DrawerMenuVO> listDrawerMenu = new ArrayList<>();

    ProgressBar progressBarWebView;

    protected void onCreate(Bundle savedInstanceState, int containerType) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_base_activity);
        init();
    }

    private void init() {
        mContext = this;
        inflater = LayoutInflater.from(this);
        linearContentMain = (LinearLayout) findViewById(R.id.linearLayoutBaseActivityContentMain);
        coordinatorRoot = (CoordinatorLayout) findViewById(R.id.coordinatorLayoutBaseActivity);
        drawerMenuRoot = (DrawerLayout) findViewById(R.id.drawerLayoutBaseActivityRoot);
        relativeMenuRoot = (RelativeLayout) findViewById(R.id.relativeLayoutBaseActivityDrawerMenu);
        frameContentRoot = (FrameLayout) findViewById(R.id.frameLayoutBaseActivityContent);
        relativeProgressRoot = (RelativeLayout) findViewById(R.id.relativeLayoutBaseActivityFooterProgress);
        progressBarRoot = (ProgressBar) findViewById(R.id.progressBarBaseActivityFooter);
        linearFooterTabsBase = (LinearLayout) findViewById(R.id.linearLayoutBaseActivityFooterTabs);
        imgFooterTabsFriends = (ImageView) findViewById(R.id.imageViewBaseActivityFooterTabFriends);
        imgFooterTabsChat = (ImageView) findViewById(R.id.imageViewBaseActivityFooterTabChat);
        imgFooterTabsAbout = (ImageView) findViewById(R.id.imageViewBaseActivityFooterTabAbout);
        imgFooterTabsSearch = (ImageView) findViewById(R.id.imageViewBaseActivityFooterTabSearch);
        imgFooterTabsProfile = (ImageView) findViewById(R.id.imageViewBaseActivityFooterTabProfile);

        progressBarWebView = (ProgressBar) findViewById(R.id.progressBarWebView);

        // Drawer Menu Items
        linearMenuHeader = (LinearLayout) findViewById(R.id.linearLayoutDrawerMenuHeader);
        imgMenuUserProfilePic = (CircularImageView) findViewById(R.id.imageViewDrawerMenuUserProfilePic);
        txtMenuUserName = (TextView) findViewById(R.id.textViewDrawerMenuUserName);
        txtMenuUserProfession = (TextView) findViewById(R.id.textViewDrawerMenuUserProfession);
        listViewMenuItems = (ListView) findViewById(R.id.listViewDrawerMenuItems);

        setDrawerMenuFields();

        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        widthScreen = metrics.widthPixels;
        heightScreen = metrics.heightPixels;

        /*viewDrawerMenuHeader = inflater.inflate(R.layout.layout_drawer_menu_header_view, null);
        viewDrawerMenuFooter = inflater.inflate(R.layout.layout_drawer_menu_footer_view, null);*/

        // Set dynamic width of drawer menu
        widthDrawerMenu = (int) (widthScreen * 0.0);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) relativeMenuRoot.getLayoutParams();
        params.width = widthDrawerMenu;
        relativeMenuRoot.setLayoutParams(params);

        setUpToolbarInSupportActionBar();
        setVisibilityActionBarBackButton(true);

//        setDrawerMenu();
        setListener();
        setAllTypefaceMontserratRegular(linearContentMain);
    }

    private void setDrawerMenuFields() {
        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "") != null
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            Picasso.with(this)
                    .load(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""))
                    .resize(Constants.convertDpToPixels(58), Constants.convertDpToPixels(58)).centerCrop()
                    .placeholder(R.drawable.ic_user_profile_avatar).error(R.drawable.ic_user_profile_avatar)
                    .into(imgMenuUserProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d("Menu", "Success loading profile image: Drawer Menu " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
                        }

                        @Override
                        public void onError() {
                            Log.d("Menu", "Error loading profile image: Drawer Menu " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
                        }
                    });
            txtMenuUserName.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "") + " "
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, ""));
            txtMenuUserProfession.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, ""));
        }

    }

    private void setListener() {
        linearMenuHeader.setOnClickListener(this);
        imgFooterTabsProfile.setOnClickListener(this);
        imgFooterTabsFriends.setOnClickListener(this);
        imgFooterTabsChat.setOnClickListener(this);
        imgFooterTabsAbout.setOnClickListener(this);
        imgFooterTabsSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearLayoutDrawerMenuHeader:
               /* intent = new Intent(mContext, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());
                startActivity(intent);*/
                intent = new Intent(mContext, EditProfileNewActivity.class);
                startActivity(intent);
                break;

            case R.id.imageViewBaseActivityFooterTabAbout:
                if (drawerMenuRoot != null) {
                    drawerMenuRoot.closeDrawer(relativeMenuRoot);
                }

                if (Constants.getCurrentActiveTaskFromActivityStack(mContext, "com.friendsoverfood.android..ContactUs.ContactUs")) {

                } else {
                    intent = new Intent(mContext, ContactUs.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
                break;

            case R.id.imageViewBaseActivityFooterTabFriends:
                if (drawerMenuRoot != null) {
                    drawerMenuRoot.closeDrawer(relativeMenuRoot);
                }

                if (Constants.getCurrentActiveTaskFromActivityStack(mContext, "com.friendsoverfood.android.FriendRequest.PendingRequestActivity")) {

                } else {
                    intent = new Intent(mContext, PendingRequestActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
                break;

            case R.id.imageViewBaseActivityFooterTabChat:
                if (drawerMenuRoot != null) {
                    drawerMenuRoot.closeDrawer(relativeMenuRoot);
                }

                if (Constants.getCurrentActiveTaskFromActivityStack(mContext, "com.friendsoverfood.android.Chat.ChatHomeActivity")) {

                } else {
//                    intent = new Intent(mContext, ChatHomeActivity.class);
                    intent = new Intent(mContext, ChatListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
                break;

            case R.id.imageViewBaseActivityFooterTabProfile:
                if (drawerMenuRoot != null) {
                    drawerMenuRoot.closeDrawer(relativeMenuRoot);
                }

                if (Constants.getCurrentActiveTaskFromActivityStack(mContext, "com.friendsoverfood.android.UserSuggestions")) {

                } else {
                    /*intent = new Intent(mContext, EditProfileNewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();*/

                    intent = new Intent(mContext, SingleUserDetailActivity.class);
//                    intent = new Intent(mContext, DraggableStaggeredGridExampleActivity.class);
                    intent.putExtra("isFromTab", true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
                break;

            case R.id.imageViewBaseActivityFooterTabSearch:
                if (drawerMenuRoot != null) {
                    drawerMenuRoot.closeDrawer(relativeMenuRoot);
                }

                if (Constants.getCurrentActiveTaskFromActivityStack(mContext, "com.friendsoverfood.android.Home.HomeActivity")) {

                } else {
                    intent = new Intent(mContext, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
                break;

            default:
                break;
        }
    }

    public void logOutApi() {
        if (NetworkUtil.isOnline(getApplicationContext())) {
            /*showProgress(getString(R.string.loading));
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_log_out_action_value));
            params.put(getString(R.string.api_param_key_user_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_session_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

            new HttpRequestSingletonPost(getApplicationContext(), getString(R.string.api_base_url), params, Constants.API_LOG_OUT, BaseActivity.this);*/
        } else {
            resetValuesOnLogout();
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    /**
     * ***
     * To set Actionbar visibility
     * ********
     */
    protected void setVisibilityActionBar(Boolean isVisible) {
        if (isVisible) {
            getSupportActionBar().show();
        } else {
            getSupportActionBar().hide();
        }
    }

    /**
     * Set up toolbar in supprt action bar, this toolbar will behave same as default action bar.
     */
    private void setUpToolbarInSupportActionBar() {
        toolbarLayoutRoot = (Toolbar) findViewById(R.id.toolbarLayoutBaseActivityRoot);
        setSupportActionBar(toolbarLayoutRoot);

//        toolBarBaseRoot = (Toolbar) toolbarLayoutRoot.findViewById(R.id.toolBarBaseRoot);

        txtTitleToolbarCentered = (TextView) toolbarLayoutRoot.findViewById(R.id.textViewToolbarBaseActivityTitle);
        txtToolbarSkipLeft = (TextView) toolbarLayoutRoot.findViewById(R.id.textViewToolbarBaseActivitySkip);
        linearToolBarCustomLayout = (LinearLayout) toolbarLayoutRoot.findViewById(R.id.linearLayoutToolbarBaseActivityCustomLayout);
        linearToolBarCustomLayout.setVisibility(View.GONE);
    }

    /**
     * Set visibility of default back button in support action bar.
     * <br />
     * <font color="#FFFF00"><b>NOTE :</b> &nbsp; When back button is visible, make sure drawer menu is not added in support action bar.</font>
     *
     * @param isVisible
     */
    protected void setVisibilityActionBarBackButton(boolean isVisible) {
        if (isVisible) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(false);
        }
    }

    protected void setVisibilityActionBarCloseButton(int res) {
        getSupportActionBar().setHomeAsUpIndicator(res);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
    }

    /**
     * Set title in support action bar.
     *
     * @param strTitle
     */
    public void setTitleSupportActionBar(String strTitle) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(strTitle);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (txtTitleToolbarCentered != null) {
            txtTitleToolbarCentered.setText(Html.fromHtml(strTitle.trim()));
            setAllTypefaceMontserratRegular(txtTitleToolbarCentered);
        }
    }

    /**
     * Set title in support action bar.
     *
     * @param strTitle
     */
    public void setTitleSupportActionBarItalic(String strTitle) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(Html.fromHtml("<i>" + strTitle.trim() + "</i>"));
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (txtTitleToolbarCentered != null) {
            txtTitleToolbarCentered.setText(Html.fromHtml("<i>" + strTitle.trim() + "</i>"));
            setAllTypefaceMontserratRegular(txtTitleToolbarCentered);
        }
    }

    /**
     * Set title in support action bar.
     *
     * @param strTitle
     */
    protected void setTitleSupportActionBar(CharSequence strTitle) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(strTitle);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if (txtTitleToolbarCentered != null) {
            txtTitleToolbarCentered.setText(strTitle);
            setAllTypefaceMontserratRegular(txtTitleToolbarCentered);
        }
    }

    /**
     * Set visibility of footer tabs in base activity
     *
     * @param isVisible visibility - true or false
     */
    public void setVisibilityFooterTabsBase(boolean isVisible) {
        if (isVisible) {
            linearFooterTabsBase.setVisibility(View.VISIBLE);
        } else {
            linearFooterTabsBase.setVisibility(View.GONE);
        }
    }

    /**
     * Set selector for icons to display highlighted according to relative position sequence in screen. Sequence starts from 1.
     *
     * @param iconPosition Position of the icon in footer bar/layout.
     */
    public void setSelectorFooterTabsBase(int iconPosition) {
        switch (iconPosition) {
            case 1: // Profile
                imgFooterTabsFriends.setSelected(false);
                imgFooterTabsChat.setSelected(false);
                imgFooterTabsSearch.setSelected(false);
                imgFooterTabsProfile.setSelected(true);
                imgFooterTabsAbout.setSelected(false);
                break;

            case 2: // Chat
                imgFooterTabsFriends.setSelected(false);
                imgFooterTabsChat.setSelected(true);
                imgFooterTabsSearch.setSelected(false);
                imgFooterTabsProfile.setSelected(false);
                imgFooterTabsAbout.setSelected(false);
                break;

            case 3: // Search
                imgFooterTabsFriends.setSelected(false);
                imgFooterTabsChat.setSelected(false);
                imgFooterTabsSearch.setSelected(true);
                imgFooterTabsProfile.setSelected(false);
                imgFooterTabsAbout.setSelected(false);
                break;

            case 4: // Friends/Pending invitations
                imgFooterTabsFriends.setSelected(true);
                imgFooterTabsChat.setSelected(false);
                imgFooterTabsSearch.setSelected(false);
                imgFooterTabsProfile.setSelected(false);
                imgFooterTabsAbout.setSelected(false);
                break;

            case 5: // About
                imgFooterTabsFriends.setSelected(false);
                imgFooterTabsChat.setSelected(false);
                imgFooterTabsSearch.setSelected(false);
                imgFooterTabsProfile.setSelected(false);
                imgFooterTabsAbout.setSelected(true);
                break;

            default:
                imgFooterTabsFriends.setSelected(false);
                imgFooterTabsChat.setSelected(false);
                imgFooterTabsSearch.setSelected(false);
                imgFooterTabsProfile.setSelected(false);
                imgFooterTabsAbout.setSelected(false);
                break;
        }
    }

    /**
     * Set title in support action bar.
     *
     * @param idDrawable
     */
    protected void setLeftDrawableToTitleSupportActionBar(int idDrawable) {
        if (txtTitleToolbarCentered != null) {
            txtTitleToolbarCentered.setCompoundDrawablesWithIntrinsicBounds(idDrawable, 0, 0, 0);
        }
    }

    /**
     * Set drawer menu and <code>ActionBarDrawerToggle</code> icon in support action bar.
     */
    protected void setDrawerMenu() {
        /*DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) listViewMenuRoot.getLayoutParams();
        params.width = widthDrawerMenu;
        listViewMenuRoot.setLayoutParams(params);*/
        // Set dynamic width of drawer menu
        widthDrawerMenu = (int) (widthScreen * 0.80);

        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) relativeMenuRoot.getLayoutParams();
        params.width = widthDrawerMenu;
        relativeMenuRoot.setLayoutParams(params);

        // Set the list's click listener
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerMenuRoot, null, R.string.friends_over_food_title, R.string.friends_over_food_title) {

        };

        // Set the drawer toggle as the DrawerListener
        drawerMenuRoot.setDrawerListener(mDrawerToggle);

//        int drawableResourceId = this.getResources().getIdentifier("nameOfDrawable", "drawable", this.getPackageName());
        listDrawerMenu.clear();
        // Set List to menu items & set list view here.
        for (int i = 0; i < getResources().getStringArray(R.array.drawer_menu_title_list).length; i++) {
            DrawerMenuVO obj = new DrawerMenuVO();
            obj.setTitle(getResources().getStringArray(R.array.drawer_menu_title_list)[i].trim());
            obj.setIconId(getResources().getIdentifier(getResources().getStringArray(R.array.drawer_menu_icon_list)[i].trim(), "drawable", this.getPackageName()));
            if (i == 1) {
                obj.setCount(20);
            } else {
                obj.setCount(0);
            }
            listDrawerMenu.add(obj);
        }

        // Set adapter for drawer menu
        adapterDrawerMenu = new DrawerMenuAdapter(mContext, listDrawerMenu);
        listViewMenuItems.setAdapter(adapterDrawerMenu);

        imgMenuUserProfilePic.setImageResource(R.drawable.shape_red_primary_circle_small);
        setAllTypefaceMontserratRegular(relativeMenuRoot);
        setAllTypefaceMontserratLight(txtMenuUserProfession);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_root, menu);
        return super.onCreateOptionsMenu(menu);
    }

    protected LinearLayout getMiddleContent() {
        return linearContentMain;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button, so long as you specify a parent activity in
        // AndroidManifest.xml.
        if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

      /*  if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item)) {
            if (drawerMenuRoot.isDrawerOpen(Gravity.RIGHT)) {
                drawerMenuRoot.closeDrawer(Gravity.RIGHT);
            } else {
                drawerMensuRoot.openDrawer(Gravity.RIGHT);
            }
        }*/

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                /*NavUtils.navigateUpFromSameTask(this);*/
//                return true;
                return true;
//            case R.id.menu_item_toolbar_menu_hamburger:
//                if (drawerMenuRoot.isDrawerOpen(Gravity.LEFT)) {
//                    drawerMenuRoot.closeDrawer(Gravity.LEFT);
//                } else if (drawerMenuRoot.getDrawerLockMode(Gravity.LEFT) != DrawerLayout.LOCK_MODE_LOCKED_CLOSED) {
//
//                    if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, "") != null
//                            && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, "").equalsIgnoreCase("")) {
//                        Picasso.with(mContext)
//                                .load(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, ""))
//                                .resize(Constants.convertDpToPixels(96), Constants.convertDpToPixels(96))
//                                .into(imgDrawerMenuUserProfilePic, new Callback() {
//                                    @Override
//                                    public void onSuccess() {
//                                        Log.i("Success", "Image Load");
//                                    }
//
//                                    @Override
//                                    public void onError() {
//                                        Log.i("Error", "Image Load" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, ""));
//                                    }
//                                });
//                    }
//
//                    drawerMenuRoot.openDrawer(Gravity.RIGHT);
//                }
//                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Disable Navigation Drawer
     */
    public void lockDrawerMenu() {
        drawerMenuRoot.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void unlockDrawerMenu() {
        drawerMenuRoot.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    /**
     * ShowToast Message Notification
     *
     * @param msg Toast Title Message
     */
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * showProgress for show Dialog
     *
     * @param msg Title Message For Progress Dialog using String
     */
    public void showProgress(String msg) {
        try {
            if (pd == null) {
                pd = new ProgressDialog(this);
                pd.setCancelable(false);
            }
            pd.setMessage(msg);
            pd.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Cancel Progress Dialog
     */
    public void stopProgress() {
        try {
            if (pd != null) {
                pd.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isValidMailRegex(String strEmailAddress) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        p = Pattern.compile(EMAIL_STRING);
        m = p.matcher(strEmailAddress);
        check = m.matches();
        return check;
    }

    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void resetValuesOnLogout() {
        if (LoginManager.getInstance() != null) {
            LoginManager.getInstance().logOut();
        }

        // Logout Firebase user
        if (FirebaseAuth.getInstance() != null) {
            FirebaseAuth.getInstance().signOut();
        }

        // path to /data/data/yourapp/app_data/imageDir
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
        deleteRecursive(directory);

//        SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, false);
//        SharedPreferenceUtil.putValue(Constants.IS_USER_REGISTERED_FOR_KOPPI, false);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REGISTERED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CITY_STATE, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_ACTIVE, false);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROVIDER, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_BIRTH_DAY, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISPLAY_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UID, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.IS_SPOTIFY_AUTHENTICATED, false);
        DbConstants.removeAllDataFromLocal(getApplicationContext());

        callEditProfileApiLogout();
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public void resetValuesInLocalOnLogout() {
        if (LoginManager.getInstance() != null) {
            LoginManager.getInstance().logOut();
        }

        // Logout Firebase user
        if (FirebaseAuth.getInstance() != null) {
            FirebaseAuth.getInstance().signOut();
        }

        // path to /data/data/yourapp/app_data/imageDir
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
        deleteRecursive(directory);

//        SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, false);
//        SharedPreferenceUtil.putValue(Constants.IS_USER_REGISTERED_FOR_KOPPI, false);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REGISTERED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CITY_STATE, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_ACTIVE, false);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROVIDER, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_BIRTH_DAY, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISPLAY_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UID, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATED_AT, 0);
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, "");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, "");
//        SharedPreferenceUtil.putValue(Constants.IS_SPOTIFY_AUTHENTICATED, false);
        DbConstants.removeAllDataFromLocal(getApplicationContext());

        SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, false);
        SharedPreferenceUtil.save();

        SharedPreferenceUtil.clear();
        SharedPreferenceUtil.save();
        stopProgress();

        intent = new Intent(getApplicationContext(), WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        supportFinishAfterTransition();
    }

    public void deleteRecursive(File fileOrDirectory) {
        try {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles())
                    deleteRecursive(child);

            fileOrDirectory.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callEditProfileApiLogout() {

        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
        params.put(getString(R.string.api_param_key_devicetoken), "null");
//        params.put("")
        params.put(getString(R.string.api_param_key_fields), getString(R.string.api_param_key_devicetoken));
        new HttpRequestSingletonPost(getApplicationContext(), getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE_LOGOUT, BaseActivity.this);
    }

    private void callLogoutApi() {
//        action:logout
//        userid:1
//        sessionid:5909d2545cc0a0.41955129
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_response_param_action_logout).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
//        params.put(getString(R.string.api_param_key_devicetoken), "null");
//        params.put(getString(R.string.api_param_key_fields), getString(R.string.api_param_key_devicetoken));
        new HttpRequestSingletonPost(getApplicationContext(), getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_LOGOUT, BaseActivity.this);
    }

    public class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("BASEACTIVITY", "Local Broadcast Manager onReceive.");

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setDrawerMenuFields();

        // Clear all the notifications in notification bar
        /*NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();*/

        mNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

                // Clear all the notifications in notification bar
               /* NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();*/

                //Change here for update view when notification received
                if (intent.hasExtra("action")) {
                    // Table invite notification.

                }
            }
        };

        LocalBroadcastManager.getInstance(BaseActivity.this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.FILTER_NOTIFICATION_RECEIVED));
        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("refreshUI"));
            mIsReceiverRegistered = true;
        }
    }

    public void onPause() {
        try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(BaseActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void showAlertDialogPositiveButtonOnly(String strTitle, String strMessage) {
        new AlertDialog.Builder(mContext).setTitle(strTitle.trim()).setMessage(strMessage.trim())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
               /* .setIcon(android.R.drawable.ic_dialog_alert)*/
                .show();
    }

    public void showSnackBarMessageOnly(String strMessage) {
        Snackbar.make(coordinatorRoot, strMessage, Snackbar.LENGTH_LONG).show();
//                .setAction(R.string.snackbar_action_undo, clickListener)
    }

    /**
     * Set Visibility of progressbar in webview
     *
     * @param visibilityProgressBar
     */

    void setVisibilityProgressBar(Boolean visibilityProgressBar) {
        if (visibilityProgressBar) {
            progressBarWebView.setVisibility(View.VISIBLE);
        } else {
            progressBarWebView.setVisibility(View.GONE);
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            Log.d("DEVICE_MODEL_DETILS", "" + capitalize(model));
            return capitalize(model);
        } else {
            Log.d("DEVICE_MODEL_DETILS", "" + capitalize(manufacturer) + " " + model);
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public String getDeviceOS() {
        String strDeviceOSDetails = "";
        try {
            String strDeviceOSVersion = Build.VERSION.RELEASE;
            String strDeviceOSSDK = String.valueOf(Build.VERSION.SDK_INT);
            strDeviceOSDetails = strDeviceOSVersion + " (" + strDeviceOSSDK + ")";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("DEVICE_OS_VERSION_DETAILS", "" + strDeviceOSDetails);
            return strDeviceOSDetails;
        }
    }

    public String getDeviceCountry() {
        // Get Country selected from Device Locale
        String strDeviceCountryLocale = "";
        try {
            strDeviceCountryLocale = Locale.getDefault().getCountry();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.e("DEVICE_COUNTRY", "" + strDeviceCountryLocale);
            return strDeviceCountryLocale;
        }
    }

    public String getDeviceUniqueId() {
        String deviceId = "";
        try {
//            deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
            deviceId = FirebaseInstanceId.getInstance().getToken();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("DEVICE_UNIQUE_ID", "ID: " + deviceId);
            return deviceId;
        }
    }

    public String getAppVersion() {
        String strAppVersion = "-1";
        try {
            int versionCode = BuildConfig.VERSION_CODE;
            String versionName = BuildConfig.VERSION_NAME;
            strAppVersion = versionName + " (" + versionCode + ")";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("APP_VERSION_DETAILS", "" + strAppVersion);
            return strAppVersion;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null) {

            if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();
                /*stopProgress();*/
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST_UPDATE_ONLY && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();
                /*stopProgress();*/
            } else if (action == Constants.ACTION_CODE_PROFILE_LOGOUT && response != null) {
                // Parse response here
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        callLogoutApi();
                    }
                } catch (JSONException e) {
                    stopProgress();
                    e.printStackTrace();
                } catch (Exception e) {
                    stopProgress();
                    e.printStackTrace();
                }
            } else if (action == Constants.ACTION_CODE_LOGOUT && response != null) {
                // Parse response here
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        resetValuesInLocalOnLogout();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Handle Invalid session here. If so, user will be automatically logged out.
            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                if (errormessage.trim().equalsIgnoreCase(getString(R.string.invalid_session))) {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                    resetValuesInLocalOnLogout();
                }

                if (status) {

                } else {
                   /* stopProgress();*/
                }
            } catch (JSONException e) {
                /*e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));*/
            } catch (Exception e) {
                /*e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));*/
            }
        }
    }

    public void setAllTypefaceMontserratBlack(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratBlack(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Black.otf"));
            }
        }
    }

    public void setAllTypefaceMontserratBold(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratBold(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Bold.otf"));
            }
        }
    }

    public void setAllTypefaceMontserratHairline(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratHairline(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Hairline.otf"));
            }
        }
    }

    public void setAllTypefaceMontserratLight(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratLight(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Light.otf"));
            }
        }
    }

    public void setAllTypefaceMontserratRegular(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratRegular(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"));
            }
        }
    }

    public void setAllTypefaceMilkShake(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratRegular(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Milkshake.otf"));
            }
        }
    }

    public void setAllTypefaceMilkShakeItalic(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratRegular(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Milkshake.otf"), Typeface.ITALIC);
            }
        }
    }

    public void setAllTypefaceMontserratRegularItalic(View view) {
        if (view instanceof ViewGroup && ((ViewGroup) view).getChildCount() != 0) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setAllTypefaceMontserratRegularItalic(((ViewGroup) view).getChildAt(i));
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "Montserrat-Regular.otf"), Typeface.ITALIC);
            }
        }
    }

    public boolean checkIsAllAppPermissionsEnabled() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED &&
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public boolean checkIsLocationAppPermissionsEnabled() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public boolean checkIsCameraAppPermissionsEnabled() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public boolean checkIsStorageAppPermissionsEnabled() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public String appendUrl(String url) {
        if (url != null && !url.trim().isEmpty() && !url.contains("http")) {
            return getResources().getString(R.string.api_base_ur_image) + url;
//            return url;
        } else return url;
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void callFriendsListApi() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_FRIEND_LIST, BaseActivity.this);
    }

    public void callFriendsListApiUpdateOnly() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST_UPDATE_ONLY, BaseActivity.this);
    }

    public void setUpShareIntentInviteFriends(Context context, String messageId) {
        try {
            String shareText = Html.fromHtml(getString(R.string.invite_friends_message_prefix)) + " "
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "") + " "
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, "") + " "
                    + Html.fromHtml(getString(R.string.invite_friends_message_content))
                    + " friendsoverfoods://http://www.friendsoverfoods.com/invite.php?" + getString(R.string.api_response_param_key_refercode) + "="
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_REFER_CODE, "") + "&"
                    + getString(R.string.api_response_param_key_messageid) + "=" + messageId + " "
                    + getString(R.string.invite_friends_message_suffix);
            String chooserTitle = getString(R.string.invite_friends_using);

            /*ShareCompat.IntentBuilder intentBuilder = ShareCompat.IntentBuilder.from((Activity) context);
            intentBuilder.setChooserTitle(chooserTitle.trim()).setType("text/plain").setText(Html.fromHtml(shareText.trim())).startChooser();*/

            Intent shareIntent = ShareCompat.IntentBuilder.from((Activity) context)
                    .setType("text/plain")
                    .setText(shareText)
                    .getIntent();
            if (shareIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(shareIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            showSnackBarMessageOnly(e.getMessage());
        }
    }

}