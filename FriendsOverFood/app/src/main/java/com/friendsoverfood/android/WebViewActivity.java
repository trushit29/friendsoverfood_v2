package com.friendsoverfood.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Trushit on 02/03/17.
 */

public class WebViewActivity extends BaseActivity {

    private View view;
    private LayoutInflater inflater;
    private Context context;
    private Intent intent;
    protected ProgressDialog pd;
    protected Handler handler = new Handler();
    HTML5WebView mWebView;
    private String strURLToLoad = "";



   /* @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        context = this;
        inflater = LayoutInflater.from(this);

        showProgress(getString(R.string.loading));

        if (getIntent().getStringExtra("link") != null && getIntent().getStringExtra("link").trim().length() > 0) {
            Log.e("WEBVIEW", "Link to be load: " + getIntent().getStringExtra("link").trim());
            strURLToLoad = getIntent().getStringExtra("link").trim();
        }

        mWebView = new HTML5WebView(this);

        // Implementing navigation for vimeo videos
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("URL LOADED", "" + url);

                if (url.trim().contains("/login/") || url.trim().contains("/share/") || url.trim().contains("watch")) {
                    view.loadUrl(strURLToLoad);
                    return true;
                } else {
                    view.loadUrl(url);
                    return true;
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                setVisibilityProgressBar(false);
                progressBarWebView.setProgress(100);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                stopProgress();
                setVisibilityProgressBar(true);
                progressBarWebView.setProgress(0);
            }

        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBarWebView.setProgress(newProgress);
            }
        });

        mWebView.loadUrl(strURLToLoad);

        setContentView(mWebView.getLayout());

        setListener();

    }

    private void setListener() {

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mWebView != null) {
            mWebView.loadUrl("about:blank");
        }
        this.finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mWebView != null) {
            mWebView.saveState(outState);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mWebView != null) {
            mWebView.stopLoading();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("ON RESUME", "On Resume called.");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("On Pause", "Called");

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // super.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * showProgress for show Dialog
     *
     * @param msg Title Message For Progress Dialog using String
     */
    public void showProgress(String msg) {
        if (pd == null) {
            pd = new ProgressDialog(this);
            pd.setCancelable(true);
        }
        pd.setMessage(msg);
        pd.show();
    }

    /**
     * Cancel Progress Dialog
     */
    public void stopProgress() {
        if (pd != null) {
            pd.cancel();
        }
    }

}