package com.friendsoverfood.android.RestaurantsList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.R;
import com.google.android.gms.location.places.AutocompletePrediction;

import java.util.ArrayList;

/**
 * Created by Trushit on 24/05/17.
 */

public class SearchLocationListAdapter extends BaseAdapter {

    private LayoutInflater inflator;
    private Context context;
    private ArrayList<AutocompletePrediction> list;
    private Intent intent;
    private RestaurantsListActivity activity;
    private final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    private final CharacterStyle STYLE_NORMAL = new StyleSpan(Typeface.ITALIC);

    public SearchLocationListAdapter(Context context, ArrayList<AutocompletePrediction> list) {
        this.list = list;
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.activity = (RestaurantsListActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public AutocompletePrediction getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        convertView = null;

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.layout_google_search_location_adapter, null);

            holder.linearRoot = (LinearLayout) convertView.findViewById(R.id.linearLayoutGoogleSearchLocationAdapterRoot);
            holder.txtPlace = (TextView) convertView.findViewById(R.id.textViewGoogleSearchLocationAdapterPlace);

//            activity.setAllTypefaceMontserratRegular(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        holder.txtPlace.setText(list.get(position).get().trim());
        holder.txtPlace.setText(Html.fromHtml(list.get(position).getPrimaryText(STYLE_BOLD) + " " + list.get(position).getSecondaryText(STYLE_NORMAL)));

        final ViewHolder finalHolder = holder;
        final int pos = position;

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.fetchPlaceDetailsSelected(list.get(pos));
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public LinearLayout linearRoot;
        public TextView txtPlace;
    }
}
