package com.friendsoverfood.android.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class DateUtil {

	private static long aDay = (1000 * 60 * 60 * 24);
	private static long aMinute = (1000 * 60);
	private static final SimpleDateFormat timeFormat = new SimpleDateFormat("'at' hh:mm aa");
	public static final SimpleDateFormat fullDateTimeZome = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.Z");
	private static final SimpleDateFormat timeFormat2 = new SimpleDateFormat("MMMM dd 'at' hh:mm aa");

	private static SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	}

	public static String getTimeString(String dateStr1) {

		ParsePosition pos = new ParsePosition(0);
		SimpleDateFormat formatter = getDateFormat();
		long then = formatter.parse(dateStr1, pos).getTime();
		Date date = new Date(then);

		StringBuffer dateStr = new StringBuffer();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Calendar now = Calendar.getInstance();

		int days = daysBetween(calendar.getTime(), now.getTime());

		if (days == 0) {

			int minutes = hoursBetween(calendar.getTime(), now.getTime());

			if (minutes > 60) {
				int hours = minutes / 60;
				dateStr.append(hours).append(hours > 1 ? " hours" : " hour")
						.append(" ago ").append(timeFormat.format(date));
			} else {

				if (minutes <= 1) {
					dateStr.append("few seconds ago");
				} else {
					dateStr.append(minutes).append(" minutes ago");
				}
			}

		} else if (days == 1) {
			dateStr.append("Yesterday ").append(timeFormat.format(date));
		} else {
			dateStr.append(timeFormat2.format(date));
		}

		return dateStr.toString();
	}

	private static int hoursBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / aMinute);
	}

	private static int daysBetween(Date d1, Date d2) {
		return (int) ((d2.getTime() - d1.getTime()) / aDay);
	}

	public static String getTimeString(long time)
	{
		String timeString = "";
		int hours,minutes,seconds;
		long temptime;
		seconds = (int) (time % 60);
		temptime = time/60;
		if(temptime > 60)
		{
			minutes = (int) (temptime % 60);
			hours = (int) (temptime / 60);
			timeString = hours + "h:";
		}
		else
		{
			minutes = (int) (time / 60);
			hours = 0;
		}
		//bellow coupleof if-else for conver single digit into double digits.
		if(hours < 10)
		{
			timeString = "0"+hours+"h:";
		}else{
			timeString = hours+"h:";
		}
		if(minutes < 10)
		{
			timeString = timeString + "0" + minutes + "m:";
		}else{
			timeString = timeString + minutes + "m:";
		}
		if(seconds < 10)
		{
			timeString = timeString + "0" + seconds + "s";
		}else{
			timeString = timeString + seconds + "s";
		}
		return timeString;
		
	}

	
	public static String sqlToStringDate(String sqlDate) {
		String strDate = "";
		Date utilDate;

		SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Calendar calTempDate = Calendar.getInstance();
			utilDate = sqlDateFormat.parse(sqlDate);

			calTempDate.setTime(utilDate);

			strDate = new SimpleDateFormat("dd/MM/yyyy").format(calTempDate
					.getTime());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}
	
	public static String dateToSQLDate(Date dtUtil) {
		String sqlDate = "";

		SimpleDateFormat sqlDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Calendar calToday = Calendar.getInstance();
			calToday.setTime(dtUtil);

			sqlDate = sqlDateFormat.format(dtUtil);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sqlDate;
	}
}
