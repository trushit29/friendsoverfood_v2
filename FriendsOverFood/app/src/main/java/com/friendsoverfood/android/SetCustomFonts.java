package com.friendsoverfood.android;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.LongSparseArray;
import android.util.SparseArray;

import java.lang.reflect.Field;

/**
 * Created by Trushit on 25/09/15.
 * <p/>
 * Custom class for ovreride default fonts in application.
 * <p/>
 * Used from these answers: <br /><br />
 * http://stackoverflow.com/questions/2711858/is-it-possible-to-set-font-for-entire-application/16883281#16883281<br />
 * http://stackoverflow.com/questions/2973270/using-a-custom-typeface-in-android/16275257#16275257<br />
 * https://github.com/perchrh/FontOverrideExample/blob/master/src/main/java/org/digitalsprouts/examples/fontoverride/FontsOverrideApplication.java<br />
 */
class SetCustomFonts {

    private static String strFontsDefault = "SourceSansPro-Regular.otf", strFontsBold = "SourceSansPro-Bold.otf", strFontsLight = "SourceSansPro-Light.otf",
            strFontsItalic = "SourceSansPro-It.otf";

    public static void setCustomFont(Context context) throws NoSuchFieldException, IllegalAccessException {
//        final Typeface regular = Typeface.createFromAsset(context.getAssets(), "SourceSansPro-Regular.ttf");
//        replaceFont(staticTypefaceFieldName, regular);

        final Typeface bold = Typeface.createFromAsset(context.getResources().getAssets(), strFontsBold);
        final Typeface italic = Typeface.createFromAsset(context.getResources().getAssets(), strFontsItalic);
        final Typeface boldItalic = Typeface.createFromAsset(context.getResources().getAssets(), strFontsLight);
        final Typeface normal = Typeface.createFromAsset(context.getResources().getAssets(), strFontsDefault);

        Field defaultField = Typeface.class.getDeclaredField("DEFAULT");
        defaultField.setAccessible(true);
        defaultField.set(null, normal);

        Field defaultBoldField = Typeface.class.getDeclaredField("DEFAULT_BOLD");
        defaultBoldField.setAccessible(true);
        defaultBoldField.set(null, bold);

        Field sDefaults = Typeface.class.getDeclaredField("sDefaults");
        sDefaults.setAccessible(true);
        sDefaults.set(null, new Typeface[]{normal, bold, italic, boldItalic});

        final Typeface normal_sans = Typeface.createFromAsset(context.getResources().getAssets(), strFontsDefault);
        Field sansSerifDefaultField = Typeface.class.getDeclaredField("SANS_SERIF");
        sansSerifDefaultField.setAccessible(true);
        sansSerifDefaultField.set(null, normal_sans);

        final Typeface normal_serif = Typeface.createFromAsset(context.getResources().getAssets(), strFontsDefault);
        Field serifDefaultField = Typeface.class.getDeclaredField("SERIF");
        serifDefaultField.setAccessible(true);
        serifDefaultField.set(null, normal_serif);

        final Typeface normal_monospace = Typeface.createFromAsset(context.getResources().getAssets(), strFontsDefault);
        Field monospaceDefaultField = Typeface.class.getDeclaredField("MONOSPACE");
        monospaceDefaultField.setAccessible(true);
        monospaceDefaultField.set(null, normal_monospace);

        // The following code is only necessary if you are using the android:typeface attribute
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            try {
                setTypeFaceDefaults(normal, bold, italic, boldItalic);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void setTypeFaceDefaults(Typeface normal, Typeface bold, Typeface italic, Typeface boldItalic) throws NoSuchFieldException, IllegalAccessException {
        Field typeFacesField = Typeface.class.getDeclaredField("sTypefaceCache");
        typeFacesField.setAccessible(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            LongSparseArray<LongSparseArray<Typeface>> sTypefaceCacheLocal = new LongSparseArray<LongSparseArray<Typeface>>(3);
            typeFacesField.get(sTypefaceCacheLocal);

            LongSparseArray<Typeface> newValues = new LongSparseArray<Typeface>(4);
            newValues.put(Typeface.NORMAL, normal);
            newValues.put(Typeface.BOLD, bold);
            newValues.put(Typeface.ITALIC, italic);
            newValues.put(Typeface.BOLD_ITALIC, boldItalic);
            sTypefaceCacheLocal.put(0, newValues);
            sTypefaceCacheLocal.put(1, newValues);
            sTypefaceCacheLocal.put(2, newValues);
            sTypefaceCacheLocal.put(3, newValues);
            typeFacesField.set(null, sTypefaceCacheLocal);
        } else {
            SparseArray<SparseArray<Typeface>> sTypefaceCacheLocal = new SparseArray<SparseArray<Typeface>>(3);
            typeFacesField.get(sTypefaceCacheLocal);

            SparseArray<Typeface> newValues = new SparseArray<Typeface>(4);
            newValues.put(Typeface.NORMAL, normal);
            newValues.put(Typeface.BOLD, bold);
            newValues.put(Typeface.ITALIC, italic);
            newValues.put(Typeface.BOLD_ITALIC, boldItalic);
            sTypefaceCacheLocal.put(0, newValues);
            sTypefaceCacheLocal.put(1, newValues);
            sTypefaceCacheLocal.put(2, newValues);
            sTypefaceCacheLocal.put(3, newValues);
            typeFacesField.set(null, sTypefaceCacheLocal);
        }
    }
}
