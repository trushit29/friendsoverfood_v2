package com.friendsoverfood.android.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ValidationUtil {
	public static boolean isPhonevalid(String phoneNo) {

		boolean boolNum = false;
		if (phoneNo.matches("[0-9]+")) {
			boolNum = true;
		} else {
			boolNum = false;
		}

		return boolNum;
	}

	/**
	 * validate an email address
	 * 
	 * @param email
	 * @return @ throws Exception
	 */
	public static boolean isEmailvalid(String email) {

		boolean match = false;

		try {
			// Email format pattern
			Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
			Matcher m = p.matcher(email);
			match = m.matches();

		} catch (Exception e) {
		}

		return match;

	}
}