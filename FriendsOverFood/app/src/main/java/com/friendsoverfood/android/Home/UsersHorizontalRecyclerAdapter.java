package com.friendsoverfood.android.Home;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.CircularProgressBar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tokenautocomplete.TokenCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Trushit on 22/04/17.
 */

public class UsersHorizontalRecyclerAdapter extends RecyclerView.Adapter<UsersHorizontalRecyclerAdapter.ViewHolder> implements HttpCallback {
    private LayoutInflater mInflater;
    private ArrayList<UserClusterItemVO> list;
    private Context context;
    private HomeActivity activity;
    private Intent intent;
    private final String TAG = "USERS_SUGGESTIONS_ADAPTER";

    private static final int VIEW_TYPE_PADDING = 1;
    private static final int VIEW_TYPE_ITEM = 2;
    private int paddingWidthDate = 0;

    private int selectedItem = -1;

    public UsersHorizontalRecyclerAdapter(Context context, ArrayList<UserClusterItemVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (HomeActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public UserClusterItemVO getItem(int position) {
        return list.get(position);
    }

    public void removeItemAtPosition(int position) {
        if (position < list.size()) {
            list.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();
        }
    }

    @Override
    public UsersHorizontalRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            /*final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,
                    parent, false);
            return new DateViewHolder(view);*/
            return new ViewHolder(mInflater.inflate(R.layout.layout_users_horizontal_recycler_adapter, parent, false));
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_users_horizontal_recycler_adapter,
                    parent, false);

            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            layoutParams.width = paddingWidthDate;
            view.setLayoutParams(layoutParams);
            return new ViewHolder(view);
        }
//        return new ViewHolder(mInflater.inflate(R.layout.layout_users_horizontal_recycler_adapter, parent, false));
    }

    public void setSelecteditem(int selecteditem) {
        this.selectedItem = selecteditem;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        /*LabelerDate labelerDate = dateDataList.get(position);
        if (labelerDate.getType() == VIEW_TYPE_PADDING) {*/
        if (position == 0 || position == list.size() - 1) {
            return VIEW_TYPE_PADDING;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.txtNameAge.setText(list.get(position).getName().trim());
//        viewHolder.txtLocation.setText(list.get(position).getLocationString().trim());
        viewHolder.txtLocation.setText(list.get(position).getTimeToReach().trim());
        viewHolder.txtMutualFriendMore.setText("+20");
        viewHolder.txtPercentage.setText("" + context.getString(R.string.percentage_tastebuds_match));
        viewHolder.txtPercentageLevel.setText("" + list.get(position).getTastebudsmatchlevel() + "%");

        if (list.get(position).getTastebudsmatchlevel() >= 70) {
//            viewHolder.circularProgressBar.setProgressWithAnimation(65);
            viewHolder.circularProgressBar.setColor(context.getResources().getColor(R.color.pin_tastebuds_match_green));
            viewHolder.circularProgressBar.setProgressWithAnimation(list.get(position).getTastebudsmatchlevel());
//            viewHolder.txtPercentage.setTextColor(context.getResources().getColor(R.color.pin_tastebuds_match_green));
        } else if (list.get(position).getTastebudsmatchlevel() < 70 && list.get(position).getTastebudsmatchlevel() >= 30) {
//            viewHolder.txtPercentage.setTextColor(context.getResources().getColor(R.color.pin_tastebuds_match_blue));
            viewHolder.circularProgressBar.setColor(context.getResources().getColor(R.color.pin_tastebuds_match_blue));
            viewHolder.circularProgressBar.setProgressWithAnimation(list.get(position).getTastebudsmatchlevel());
        } else if (list.get(position).getTastebudsmatchlevel() > 0) {
//            viewHolder.txtPercentage.setTextColor(context.getResources().getColor(R.color.pin_tastebuds_match_red));
            viewHolder.circularProgressBar.setColor(context.getResources().getColor(R.color.pin_tastebuds_match_red));
            viewHolder.circularProgressBar.setProgressWithAnimation(list.get(position).getTastebudsmatchlevel());
        } else {
            viewHolder.circularProgressBar.setColor(context.getResources().getColor(R.color.pin_tastebuds_match_red));
            viewHolder.circularProgressBar.setProgressWithAnimation(list.get(position).getTastebudsmatchlevel());
//            viewHolder.txtPercentage.setText("No " + context.getString(R.string.percentage_tastebuds_match));
        }

        activity.setAllTypefaceMontserratRegular(viewHolder.cardViewRoot);

        // Clear already set tokens/tags of tastebuds
        viewHolder.tokenViewTastebuds.clearListSelection();
        viewHolder.tokenViewTastebuds.clear();

        /*for (int i = 0; i < list.get(position).getListTasteBuds().size(); i++) {
            String name = list.get(position).getListTasteBuds().get(i).trim();
            TasteBudsToken token = new TasteBudsToken(name, name, list.get(position).getTastebudsmatchlevel(), false);
            viewHolder.tokenViewTastebuds.addObject(token);
        }*/

        if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Friend")) {
            viewHolder.imgInvite.setImageResource(R.drawable.selector_ic_chat_medium_red_selected);
        } else if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Sent")) {
            viewHolder.imgInvite.setImageResource(R.drawable.ic_send_request);
        } else if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Pending")) {
//            viewHolder.imgInvite.setImageResource(R.drawable.selector_ic_invite_add_friend_red);
            viewHolder.imgInvite.setImageResource(R.drawable.ic_request_pending_red_small);
        } else {
            viewHolder.imgInvite.setImageResource(R.drawable.selector_ic_invite_add_friend_red);
//            viewHolder.imgDivider.setVisibility(View.GONE);
//            viewHolder.imgInvite.setVisibility(View.GONE);
        }

        if (position == list.size() - 1) {
//            android:layout_marginBottom="4dp"
//            android:layout_marginLeft="8dp"
//            android:layout_marginRight="8dp"
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(Constants.convertDpToPixels(224),
                    RecyclerView.LayoutParams.WRAP_CONTENT);
//            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(activity.widthScreen - Constants.convertDpToPixels(16),
//                    RecyclerView.LayoutParams.WRAP_CONTENT);
            params.setMargins(Constants.convertDpToPixels(8), 0, Constants.convertDpToPixels(96), Constants.convertDpToPixels(4));
            viewHolder.cardViewRoot.setLayoutParams(params);
        } else {
//            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(Constants.convertDpToPixels(224), RecyclerView.LayoutParams.WRAP_CONTENT);
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(Constants.convertDpToPixels(224),
                    RecyclerView.LayoutParams.WRAP_CONTENT);
//            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(activity.widthScreen - Constants.convertDpToPixels(16),
//                    RecyclerView.LayoutParams.WRAP_CONTENT);
            params.setMargins(Constants.convertDpToPixels(8), 0, Constants.convertDpToPixels(8), Constants.convertDpToPixels(4));
            viewHolder.cardViewRoot.setLayoutParams(params);
        }

        final int pos = position;
        final ViewHolder holder = viewHolder;

        Log.d("PROFILE_PIC_URL", "Pic: " + list.get(position).getProfilepic1().trim()
                + ", Full pic: " + activity.appendUrl(list.get(position).getProfilepic1().trim()).trim());

        if (!activity.appendUrl(list.get(position).getProfilepic1().trim()).trim().isEmpty()) {
            Picasso.with(context).load(activity.appendUrl(list.get(position).getProfilepic1().trim()).trim())
//                    .error(R.drawable.ic_user_profile_avatar).placeholder(R.drawable.ic_user_profile_avatar)
                    .error(list.get(pos).getSex().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_46
                            : R.drawable.ic_placeholder_user_avatar_female_46)
                    .placeholder(list.get(pos).getSex().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_46
                            : R.drawable.ic_placeholder_user_avatar_female_46)
                    .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48)).centerCrop()
                    .into(holder.imgProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            try {
                                Log.d("ERROR_LOADING_IMAGE", "User pic is not loaded: " + activity.appendUrl(list.get(pos).getProfilepic1().trim()).trim());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } else {
//            holder.imgProfilePic.setImageResource(R.drawable.shape_red_primary_circle_small);
            if (list.get(pos).getSex().trim().toLowerCase().equalsIgnoreCase("male")) {
                holder.imgProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_male_46);
            } else {
                holder.imgProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_male_46);
            }
           /* holder.imgProfilePic.setImageResource(R.drawable.ic_user_profile_avatar);*/
        }

        /*holder.imgMutualFriend1.setImageResource(R.drawable.shape_grey_circle_mutual_friends_small);
        holder.imgMutualFriend2.setImageResource(R.drawable.shape_grey_circle_mutual_friends_small);
        holder.imgMutualFriend3.setImageResource(R.drawable.shape_grey_circle_mutual_friends_small);*/
        // TODO Set Mutual friends details here
        if (list.get(position).getMutualFriends() > 0) {
            holder.relativeMutualFriends.setVisibility(View.VISIBLE);
            holder.txtMutualFriendsTitle.setText(context.getResources().getString(R.string.mutual_friends_title));

            if (list.get(position).getMutualFriends() > 3) {
                holder.txtMutualFriendMore.setVisibility(View.VISIBLE);
                holder.txtMutualFriendMore.setText("+ " + (list.get(position).getMutualFriends() - 3));
                holder.imgMutualFriend1.setVisibility(View.VISIBLE);
                holder.imgMutualFriend2.setVisibility(View.VISIBLE);
                holder.imgMutualFriend3.setVisibility(View.VISIBLE);
            } else {
                holder.txtMutualFriendMore.setText("");
                holder.txtMutualFriendMore.setVisibility(View.GONE);
                if (list.get(position).getMutualFriends() == 3) {
                    holder.imgMutualFriend1.setVisibility(View.VISIBLE);
                    holder.imgMutualFriend2.setVisibility(View.VISIBLE);
                    holder.imgMutualFriend3.setVisibility(View.VISIBLE);
                } else if (list.get(position).getMutualFriends() == 2) {
                    holder.imgMutualFriend1.setVisibility(View.VISIBLE);
                    holder.imgMutualFriend2.setVisibility(View.VISIBLE);
                    holder.imgMutualFriend3.setVisibility(View.GONE);
                } else if (list.get(position).getMutualFriends() == 1) {
                    holder.imgMutualFriend1.setVisibility(View.VISIBLE);
                    holder.imgMutualFriend2.setVisibility(View.GONE);
                    holder.imgMutualFriend3.setVisibility(View.GONE);
                } else {
                    holder.imgMutualFriend1.setVisibility(View.GONE);
                    holder.imgMutualFriend2.setVisibility(View.GONE);
                    holder.imgMutualFriend3.setVisibility(View.GONE);
                }
            }

            for (int i = 0; i < list.get(position).getMutualFriendList().size(); i++) {
                if (i == 0 && !activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim().isEmpty()) {
                    final int posList = i;
                    Picasso.with(context).load(activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim())
                            .resize(Constants.convertDpToPixels(30), Constants.convertDpToPixels(30)).centerCrop()
                            .error(R.drawable.shape_grey_circle_mutual_friends_small).placeholder(R.drawable.shape_grey_circle_mutual_friends_small)
                            .into(holder.imgMutualFriend1, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Log.d(TAG, "Error loading picture: "
                                            + activity.appendUrl(list.get(pos).getMutualFriendList().get(posList).getProfilepic1().trim()));
                                }
                            });
                }

                if (i == 1 && !activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim().isEmpty()) {
                    final int posList = i;
                    Picasso.with(context).load(activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim())
                            .resize(Constants.convertDpToPixels(30), Constants.convertDpToPixels(30)).centerCrop()
                            .error(R.drawable.shape_grey_circle_mutual_friends_small).placeholder(R.drawable.shape_grey_circle_mutual_friends_small)
                            .into(holder.imgMutualFriend2, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Log.d(TAG, "Error loading picture: "
                                            + activity.appendUrl(list.get(pos).getMutualFriendList().get(posList).getProfilepic1().trim()));
                                }
                            });
                }

                if (i == 2 && !activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim().isEmpty()) {
                    final int posList = i;
                    Picasso.with(context).load(activity.appendUrl(list.get(position).getMutualFriendList().get(i).getProfilepic1().trim()).trim())
                            .resize(Constants.convertDpToPixels(30), Constants.convertDpToPixels(30)).centerCrop()
                            .error(R.drawable.shape_grey_circle_mutual_friends_small).placeholder(R.drawable.shape_grey_circle_mutual_friends_small)
                            .into(holder.imgMutualFriend3, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Log.d(TAG, "Error loading picture: " +
                                            activity.appendUrl(list.get(pos).getMutualFriendList().get(posList).getProfilepic1().trim()));
                                }
                            });
                }
            }
        } else {
            holder.relativeMutualFriends.setVisibility(View.GONE);
            holder.txtMutualFriendsTitle.setText(context.getResources().getString(R.string.no_mutual_friends_found));
//            holder.txtMutualFriendsTitle.setText(context.getResources().getString(R.string.invite_friends));
        }

//        viewHolder.relativeMutualFriends.setOnClickListener(new View.OnClickListener() {
        viewHolder.linearLayoutHorizontalRecyclerAdapterMutualFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(pos).getMutualFriendList().size() > 0) {
                    activity.createDialogMutualFriendsList(list.get(pos).getMutualFriendList());
                } else {
                    // Request API to fetch message id for invite.
//                    sendRequestCreateFirebaseMessage(pos);
//                    activity.setUpShareIntentInviteFriends(context, asdasdasda);
                }
            }
        });

        viewHolder.imgInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Friend")
                        || list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Sent")
                        || list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Pending")) {
                    /*intent = new Intent(context, ChatListActivity.class);
                    context.startActivity(intent);*/

                    // Fetch all the match details from Shared Preferences here.
                    JSONObject jsonObjectResponse = null;
                    ArrayList<ChatListVO> matchList = new ArrayList<>();
                    try {
                        jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                        Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                        JSONObject settings = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_settings))
                                ? jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings)) : new JSONObject();
                        boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                                ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                        String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                                ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                        if (status) {
                            JSONArray data = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                    ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();

                            JSONObject dataInner = null;
                            for (int i = 0; i < data.length(); i++) {
                                ChatListVO item = new ChatListVO();
                                try {
                                    dataInner = data.getJSONObject(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String matchId = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                String friend1 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend1))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend1)).trim() : "";
                                String friend2 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend2))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend2)).trim() : "";
                                String statusFriend = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";

//                                if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Friend")) {
//                                } else if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Sent")) {
//                                } else if (list.get(position).getFriendstatus().trim().equalsIgnoreCase("Pending")) {
                                if (statusFriend.trim().equalsIgnoreCase("Friend")) {
                                    statusFriend = "1";
                                } else if (statusFriend.trim().equalsIgnoreCase("Sent")
                                        || statusFriend.trim().equalsIgnoreCase("Pending")) {
                                    statusFriend = "0";
                                }

                                item.setMatchid(matchId);
                                item.setFriend1(friend1);
                                item.setFriend2(friend2);
                                item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");

                                JSONArray details = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_details))
                                        ? dataInner.getJSONArray(context.getResources().getString(R.string.api_response_param_key_details)) : new JSONArray();
                                for (int j = 0; j < details.length(); j++) {

                                    JSONObject detailInner = details.getJSONObject(j);
                                    String userId = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                    String first_name = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_first_name))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_first_name)).trim() : "";
                                    String lastName = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_last_name))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_last_name)).trim() : "";
                                    String profilePic = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                    String gender = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_gender))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_gender)).trim() : "";
                                    item.setUserId(userId);
                                    item.setFirstName(first_name);
                                    item.setLastName(lastName);
                                    item.setProfilePic(profilePic);
                                    item.setGender(gender);
                                }
                                matchList.add(item);

                            }

                            // Get users VO object here to pass to chat screen using intent.
                            int posUser = -1;
                            for (int i = 0; i < matchList.size(); i++) {
                                Log.d(TAG, "PosSelected Id: " + list.get(pos).getId().trim() + ", Match List Id: " + matchList.get(i).getUserId().trim());
                                if (list.get(pos).getId().trim().equalsIgnoreCase(matchList.get(i).getUserId().trim())) {
                                    Log.d(TAG, "Name: " + matchList.get(i).getFirstName() + " " + matchList.get(i).getLastName()
                                            + ", Status: " + matchList.get(i).getStatus());
                                    posUser = i;
                                }
                            }

                            if (posUser != -1) {
                                final ChatListVO OtherUser = matchList.get(posUser);

                                if (OtherUser.getStatus().trim().equalsIgnoreCase("Friend")
                                        || list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Friend")) {
                                    OtherUser.setStatus("1");
                                } else if (OtherUser.getStatus().trim().equalsIgnoreCase("Sent")
                                        || OtherUser.getStatus().trim().equalsIgnoreCase("Pending")) {
                                    OtherUser.setStatus("0");
                                }

                                // Open Chat Screen from here.
                                activity.stopProgress();

                                Log.d(TAG, "Chat selected status: " + OtherUser.getStatus());

                                Intent intent = new Intent(context, ChatDetailsActivity.class);
                                intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
//                                intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
                                activity.startActivity(intent);
//                                activity.startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
                                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//                                activity.overridePendingTransition(0, 0);
                            } else {
                                activity.stopProgress();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        activity.stopProgress();
                    } catch (Exception e) {
                        e.printStackTrace();
                        activity.stopProgress();
                    }
               /* } else if (list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Sent")) {
                    activity.posSelected = pos;
                    activity.sendFriendRequestToUserSuggestion();

//                    sendFriendRequestToUserSuggestion();*/
                /*} else if (list.get(pos).getFriendstatus().trim().equalsIgnoreCase("Pending")) {
                    intent = new Intent(context, SingleUserDetailActivity.class);
                    intent.putExtra(Constants.INTENT_USER_ID, list.get(pos).getId());
//                View sharedViewImage = holder.imgProfilePic;
                    View sharedViewImage = holder.cardViewRoot;
                    String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedViewImage, transitionNameImage);
                        activity.startActivity(intent, transitionActivityOptions.toBundle());
                    } else {
                        activity.startActivity(intent);
                    }*/
                } else {
                    activity.posSelected = pos;
                    /*ActionSheet.createBuilder(context, activity.getSupportFragmentManager())
                            .setCancelButtonTitle("Cancel")
                            .setOtherButtonTitles("Now", "Later")
                            .setCancelableOnTouchOutside(true)
                            .setListener(activity).show();*/
                    //                Send friend request
                    /*activity.sendFriendRequestToUserSuggestion();*/
                    activity.callFriendsListApi();
                }
            }
        });

      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/

        viewHolder.cardViewRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Set this restaurant on map as cluster or cluster item.
                intent = new Intent(context, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, list.get(pos).getId());
//                View sharedViewImage = holder.imgProfilePic;
                View sharedViewImage = holder.cardViewRoot;
                String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedViewImage, transitionNameImage);
                    activity.startActivity(intent, transitionActivityOptions.toBundle());
                } else {
                    activity.startActivity(intent);
                }
            }
        });
    }

    public void sendRequestCreateFirebaseMessage(int pos) {
        // Prepare a firebase message here.
        String messageString = context.getResources().getString(R.string.hi_exclamation) + " " + context.getResources().getString(R.string.there)
//                + " " + list.get(pos).getFirstname() + " " + list.get(pos).getLastname()
                + "! " + context.getResources().getString(R.string.hey_please_accept_my_friend_request);
        Calendar c = Calendar.getInstance();
        /*MessageVO chat = new MessageVO();
        chat.setMsg(messageString);
        chat.setTimeStamp(Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true));
        chat.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());*/

        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        /*SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();*/

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(messageString);
        /*if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else*/
//        message.setRecieverId(list.get(pos).getId());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
//        message.setRecieverDP(list.get(pos).getProfilepic1().trim());
        message.setTimeStamp(timestampToSave);
        message.setContentType("text");

        // Convert Message to Firebase object here.
        String messageToFirebase = "";
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(message);// obj is your object

        try {
            JSONObject jsonObj = new JSONObject(toJson);
            JSONArray array = new JSONArray();
            array.put(jsonObj);
            messageToFirebase = array.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Convert JSON to POJO
//        Gson gson = new Gson();
//        YourPOJO myPOJO = gson.fromJson(jsonString, YourPOJO.class);
//        Firebase ref = new Firebase(YOUR_FIREBASE_REF);
//        ref.setValue(myPOJO);
        Log.d(TAG, "Message generated: " + messageToFirebase);

        activity.showProgress(context.getResources().getString(R.string.loading));
        HashMap<String, String> params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action),
                context.getResources().getString(R.string.api_response_param_action_createfirebasemessage));
        params.put(context.getResources().getString(R.string.api_param_key_userid),
                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid),
                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_message), messageToFirebase.trim());

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_CREATE_FIREBASE_MESSAGE,
                UsersHorizontalRecyclerAdapter.this);
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_CREATE_FIREBASE_MESSAGE && response != null) {
                Log.i(TAG, "Response received for CREATE_FIREBASE_MESSAGE: " + response);
                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string
                            .api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            String id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            String userid = !data.isNull(context.getResources().getString(R.string.api_response_param_key_userid))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_userid)).trim() : "";
                            String message = !data.isNull(context.getResources().getString(R.string.api_response_param_key_message))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_message)).trim() : "";
                            String createdat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_createdat))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_createdat)).trim() : "";
                            String updatedat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_updatedat))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_updatedat)).trim() : "";

                            // Fetch id as token in invite message.
                            if (!id.trim().isEmpty()) {
                                activity.stopProgress();
                                activity.setUpShareIntentInviteFriends(context, id.trim());
                            } else {
                                activity.stopProgress();
                                activity.showSnackBarMessageOnly(context.getResources().getString(R.string.error_in_generating_invite_token));
                            }
                        }
                    } else {
                        activity.stopProgress();
                        activity.showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtNameAge, txtLocation, txtMutualFriendMore, txtPercentage, txtPercentageLevel, txtMutualFriendsTitle;
        public CardView cardViewRoot;
        public CircularImageView imgProfilePic, imgMutualFriend1, imgMutualFriend2, imgMutualFriend3;
        public ImageButton imgInvite;
        public TasteBudsCustomTokenView tokenViewTastebuds;
        public ImageView imgDivider;
        public RelativeLayout relativeMutualFriends;
        public LinearLayout linearLayoutHorizontalRecyclerAdapterMutualFriends;
        public CircularProgressBar circularProgressBar;

        public ViewHolder(View view) {
            super(view);
            cardViewRoot = (CardView) view.findViewById(R.id.cardViewUsersHorizontalRecyclerAdapterRoot);
            imgProfilePic = (CircularImageView) view.findViewById(R.id.imageViewUsersHorizontalRecyclerAdapterProfilePicture);
            txtNameAge = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterNameAge);
            txtLocation = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterLocation);
            imgInvite = (ImageButton) view.findViewById(R.id.imageButtonUsersHorizontalRecyclerAdapterInvite);
            imgDivider = (ImageView) view.findViewById(R.id.imageViewUsersHorizontalRecyclerAdapterDivider);
            imgMutualFriend1 = (CircularImageView) view.findViewById(R.id.imageViewUsersHorizontalRecyclerAdapterMutualFriends1);
            imgMutualFriend2 = (CircularImageView) view.findViewById(R.id.imageViewUsersHorizontalRecyclerAdapterMutualFriends2);
            imgMutualFriend3 = (CircularImageView) view.findViewById(R.id.imageViewUsersHorizontalRecyclerAdapterMutualFriends3);
            txtMutualFriendMore = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterMutualFriendsMore);
            txtPercentage = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterPercentage);
            txtPercentageLevel = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterPercentageLevel);
            tokenViewTastebuds = (TasteBudsCustomTokenView) view.findViewById(R.id.tokenViewUsersHorizontalRecyclerAdapterTastebuds);
            relativeMutualFriends = (RelativeLayout) view.findViewById(R.id.relativeLayoutHorizontalRecyclerAdapterMutualFriends);
            linearLayoutHorizontalRecyclerAdapterMutualFriends = (LinearLayout) view.findViewById(R.id.linearLayoutHorizontalRecyclerAdapterMutualFriends);
            txtMutualFriendsTitle = (TextView) view.findViewById(R.id.textViewUsersHorizontalRecyclerAdapterMutualFriendsTitle);
            circularProgressBar = (CircularProgressBar) view.findViewById(R.id.circularProgressbar);

            tokenViewTastebuds.allowCollapse(false);
            tokenViewTastebuds.setAdapter(null);
            tokenViewTastebuds.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.None);
            //        tagViewInterests.setTokenListener(activity);
            activity.setAllTypefaceMontserratRegular(view);
        }
    }

}
