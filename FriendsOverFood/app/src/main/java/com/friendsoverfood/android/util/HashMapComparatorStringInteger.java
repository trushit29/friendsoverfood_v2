package com.friendsoverfood.android.util;

import java.util.Comparator;
import java.util.Map;

class HashMapComparatorStringInteger implements
		Comparator<Map<String, String>> {

	private final String key;

	public HashMapComparatorStringInteger(String key) {
		this.key = key;
	}

	public int compare(Map<String, String> first, Map<String, String> second) {
		long firstValue = Long.parseLong(first.get(key));
		long secondValue = Long.parseLong(second.get(key));
		return firstValue < secondValue ? -1 : firstValue > secondValue ? 1 : 0;
	}
}
