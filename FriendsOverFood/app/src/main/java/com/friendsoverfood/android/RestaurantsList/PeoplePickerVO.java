package com.friendsoverfood.android.RestaurantsList;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Trushit on 08/03/17.
 */

public class PeoplePickerVO implements Parcelable {

    public int peopleNumber = 1;
    public String peopleDisplay = "1 People";

    public PeoplePickerVO() {
    }

    protected PeoplePickerVO(Parcel in) {
        peopleNumber = in.readInt();
        peopleDisplay = in.readString();
    }

    public static final Creator<PeoplePickerVO> CREATOR = new Creator<PeoplePickerVO>() {
        @Override
        public PeoplePickerVO createFromParcel(Parcel in) {
            return new PeoplePickerVO(in);
        }

        @Override
        public PeoplePickerVO[] newArray(int size) {
            return new PeoplePickerVO[size];
        }
    };

    public int getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getPeopleDisplay() {
        return peopleDisplay;
    }

    public void setPeopleDisplay(String peopleDisplay) {
        this.peopleDisplay = peopleDisplay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(peopleNumber);
        dest.writeString(peopleDisplay);
    }
}
