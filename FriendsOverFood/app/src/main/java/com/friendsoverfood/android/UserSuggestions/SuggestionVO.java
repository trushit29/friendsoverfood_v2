package com.friendsoverfood.android.UserSuggestions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Trushit on 01/09/16.
 */
public class SuggestionVO implements Serializable {
    private String name = "";
    private String id = "";
    private String email = "";
    private String address = "";
    private String distance = "";
    private String firstname = "";
    private String lastname = "";
    private String sex = "";
    private String birthdate = "";
    private String profilepic = "";
    private String latitude = "";
    private String longitude = "";
    private String cuisines = "";
    private String occupation = "";
    private String testbuds = "";
    private String favrestaurants = "";
    private String profilepicurl = "";

    private String image1 = "";
    private String image2 = "";
    private String image3 = "";
    private String image4 = "";
    private String ethinicity = "";
    private String religion = "";
    private String education = "";
    private String employer = "";
    private String ilike = "";
    private String iPrefer = "";
    private String about = "";

    private String cuisinesdetails = "";
    private String socialId = "";

    private String relationshipStatus = "";
    public ArrayList<MutualFriendsVO> MutualFriendList = new ArrayList<>();

    private String testbudsdetails = "";
    private int mutualFriends = 0;
    private Date birthDate;
    private ArrayList<String> listTasteBuds = new ArrayList<>();

    private String username = "";
    private String BirthDateString = "";

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRelationshipStatus() {
        return relationshipStatus;
    }

    public void setRelationshipStatus(String relationshipStatus) {
        this.relationshipStatus = relationshipStatus;
    }

    public String getiPrefer() {
        return iPrefer;
    }

    public void setiPrefer(String iPrefer) {
        this.iPrefer = iPrefer;
    }

    public String getIlike() {
        return ilike;
    }

    public void setIlike(String ilike) {
        this.ilike = ilike;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEthinicity() {
        return ethinicity;
    }

    public void setEthinicity(String ethinicity) {
        this.ethinicity = ethinicity;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public ArrayList<MutualFriendsVO> getMutualFriendList() {
        return MutualFriendList;
    }

    public void setMutualFriendList(ArrayList<MutualFriendsVO> mutualFriendList) {
        MutualFriendList = mutualFriendList;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBirthDateString() {
        return BirthDateString;
    }

    public void setBirthDateString(String birthDateString) {
        BirthDateString = birthDateString;
    }

    public String getTestbudsdetails() {
        return testbudsdetails;
    }

    public void setTestbudsdetails(String testbudsdetails) {
        this.testbudsdetails = testbudsdetails;
    }

    public String getCuisinesdetails() {
        return cuisinesdetails;
    }

    public void setCuisinesdetails(String cuisinesdetails) {
        this.cuisinesdetails = cuisinesdetails;
    }

    public String getProfilepicurl() {
        return profilepicurl;
    }

    public void setProfilepicurl(String profilepicurl) {
        this.profilepicurl = profilepicurl;
    }

    public String getFavrestaurants() {
        return favrestaurants;
    }

    public void setFavrestaurants(String favrestaurants) {
        this.favrestaurants = favrestaurants;
    }

    public String getTestbuds() {
        return testbuds;
    }

    public void setTestbuds(String testbuds) {
        this.testbuds = testbuds;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getMutualFriends() {
        return mutualFriends;
    }

    public void setMutualFriends(int mutualFriends) {
        this.mutualFriends = mutualFriends;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public ArrayList<String> getListTasteBuds() {
        return listTasteBuds;
    }

    public void setListTasteBuds(ArrayList<String> listTasteBuds) {
        this.listTasteBuds = listTasteBuds;
    }
}
