package com.friendsoverfood.android.ChatNew;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ashish on 08/09/16.
 */
public class ChatListVO implements Serializable {

    String userId = "";
    String profilePic = "";
    String firstName = "";
    String lastName = "";
    String lastMsg = "";
    long time = 0;
    int unreadCount = 0;
    String friend1 = "";
    String friend2 = "";
    String matchid = "";
    String isSelected = "0";
    long timestampLastSeen = 0;
    String status= "";
    String gender= "";
    private String lastMsgSenderId = "", lastMsgReceiverId = "";
    private ArrayList<String> unreadMessageKeys = new ArrayList<>();

    public ChatListVO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getUserId() {
        return userId;
    }

    public String getMatchid() {
        return matchid;
    }

    public void setMatchid(String matchid) {
        this.matchid = matchid;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getFriend1() {
        return friend1;
    }

    public void setFriend1(String friend1) {
        this.friend1 = friend1;
    }

    public String getFriend2() {
        return friend2;
    }

    public void setFriend2(String friend2) {
        this.friend2 = friend2;
    }

    public long getTimestampLastSeen() {
        return timestampLastSeen;
    }

    public void setTimestampLastSeen(long timestampLastSeen) {
        this.timestampLastSeen = timestampLastSeen;
    }

    public ArrayList<String> getUnreadMessageKeys() {
        return unreadMessageKeys;
    }

    public void setUnreadMessageKeys(ArrayList<String> unreadMessageKeys) {
        this.unreadMessageKeys = unreadMessageKeys;
    }

    public String getLastMsgSenderId() {
        return lastMsgSenderId;
    }

    public void setLastMsgSenderId(String lastMsgSenderId) {
        this.lastMsgSenderId = lastMsgSenderId;
    }

    public String getLastMsgReceiverId() {
        return lastMsgReceiverId;
    }

    public void setLastMsgReceiverId(String lastMsgReceiverId) {
        this.lastMsgReceiverId = lastMsgReceiverId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    /*@Override
    public int compareTo(@NonNull Object o) {
        if (((ChatListVO) o).getTimestampLastSeen() > timestampLastSeen) {
            return 1;
        } else if (((ChatListVO) o).getTimestampLastSeen() == timestampLastSeen) {
            return 0;
        } else {
            return -1;
        }
       *//* return 0;*//*
    }*/
}
