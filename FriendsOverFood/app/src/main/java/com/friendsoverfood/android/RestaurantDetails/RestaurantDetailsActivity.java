package com.friendsoverfood.android.RestaurantDetails;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.friendsoverfood.android.ApplicationClass;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Home.TasteBudsCustomTokenView;
import com.friendsoverfood.android.Home.TasteBudsToken;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.DaysPickerVO;
import com.friendsoverfood.android.RestaurantsList.PeoplePickerVO;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.friendsoverfood.android.RestaurantsList.RestaurantReviewVO;
import com.friendsoverfood.android.RestaurantsList.RestaurantsListActivity;
import com.friendsoverfood.android.RestaurantsList.TimePickerVO;
import com.friendsoverfood.android.UserSuggestions.CustomSliderView;
import com.friendsoverfood.android.WebViewActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.NetworkUtil;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tokenautocomplete.TokenCompleteTextView;
import com.uber.sdk.android.core.UberButton;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestActivityBehavior;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import at.blogc.android.views.ExpandableTextView;

import static com.uber.sdk.android.core.utils.Preconditions.checkNotNull;
import static com.uber.sdk.android.core.utils.Preconditions.checkState;

/**
 * Created by Trushit on 01/03/17.
 */

public class RestaurantDetailsActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener, AdapterView.OnItemSelectedListener {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "RESTAURANTS_DETAILS_ACTIVITY";

    private RestaurantListVO restaurantDetails;
    private NestedScrollView scrollViewRestaurantDetails;
    private TextView txtRestaurantName, txtCuisine, txtTimeToReach, txtDescription, txtReviewUserName, txtReviewDatetime, txtReviewDescrition,
            txtReadAllReviews, txtReviewsTitle, txtCuisineTitle, txtWebsite, txtCall, txtDirection, textViewRestaurantDetailsActivityTime; // txtCuisineDetails
    private TasteBudsCustomTokenView tokenViewTastebuds;
    private RatingBar ratingBarReview;
    private CircularImageView imgReviewUserProfilePic;
    private Button btnContinue;
    private LinearLayout linearCuisineDetails, linearReviewDetails, linearFooterContactRestaurant, linearFooterPickers;
    private TextView txtViewCounterRestoDetails;
    private SliderLayout sliderRestoDetails;

    // Picker Variables
    public NumberPicker numberPickerDay, numberPickerTime, numberPickerPeople;
    public ArrayList<DaysPickerVO> listDaysPicker = new ArrayList<>();
    public ArrayList<TimePickerVO> listTimePicker = new ArrayList<>();
    public ArrayList<PeoplePickerVO> listPeoplePicker = new ArrayList<>();
    public LinearLayout linearViewDateTimePeoplePickers;
    public Calendar calDateSelected = Calendar.getInstance();
    public PeoplePickerVO peopleSelected = new PeoplePickerVO();
    public Button btnDoneDatePicker;
    public boolean isPickerSelected = false;
    public LinearLayout linearDatePicker, linearPeoplePicker;
    public TextView txtDatePicker;
    public ImageView imgDownArrowDatePicker, imgDownArrowPeoplePicker;
    public ImageButton imgBackground;
    public boolean isToExpandDatePicker = false;
    public ImageView buttonToggle;
    public ExpandableTextView expandableTextView;
    public RideRequestButton rideRequestButton;
    public RelativeLayout relativeUber;
    public FloatingActionMenu menuFabListFilter;
    public FloatingActionButton fabList;
    public FloatingActionButton fabFilter;
    public ImageView imgGradientFab;
    private static final int WIDGET_REQUEST_CODE = 1234;
    private static final String UBERX_PRODUCT_ID = "a1111c8c-c720-46c3-8534-2fcdd730040d";
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 4000;

    private TextView txtInviteFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(false);
//        setTitleSupportActionBar(getString(R.string.title_restaurants_list));
        setTitleSupportActionBar("");
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_restaurant_details_activity, getMiddleContent());

        menuFabListFilter = (FloatingActionMenu) view.findViewById(R.id.floatingActionMenuListFilterRestoDetail);
        fabList = (FloatingActionButton) view.findViewById(R.id.fabListRestoDetail);
        fabFilter = (FloatingActionButton) view.findViewById(R.id.fabFilterRestoDetail);
        imgGradientFab = (ImageView) view.findViewById(R.id.imageViewListActivityGradientRestoDetail);

        scrollViewRestaurantDetails = (NestedScrollView) view.findViewById(R.id.nestedScrollViewRestaurantDetailsActivity);
        sliderRestoDetails = (SliderLayout) view.findViewById(R.id.sliderRestoDetails);
        imgReviewUserProfilePic = (CircularImageView) view.findViewById(R.id.imageViewRestaurantDetailsActivityRestaurantReviewUserProfilePicture);
        txtRestaurantName = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantName);
        txtCuisine = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantCuisine);
        txtTimeToReach = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantTimeToReach);
        txtViewCounterRestoDetails = (TextView) view.findViewById(R.id.txtViewCounterRestoDetails);
//        txtDescription = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantDescription);
//        txtCuisineDetails = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantCuisineDetails);
        tokenViewTastebuds = (TasteBudsCustomTokenView) view.findViewById(R.id.tokenViewRestaurantDetailsActivityTastebuds);
        txtReviewUserName = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantReviewUserName);
        txtReviewDatetime = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantReviewDateTime);
        textViewRestaurantDetailsActivityTime = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityTime);
        txtReviewDescrition = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantReviewDescription);
        txtReadAllReviews = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantReviewsReadAll);
        txtReviewsTitle = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantReviewsTitle);
        txtCuisineTitle = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantCuisineDetailsTitle);
        txtWebsite = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantWebsite);
        txtCall = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantCall);
        txtDirection = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityRestaurantDirection);
        ratingBarReview = (RatingBar) view.findViewById(R.id.ratingBarRestaurantDetailsActivityRestaurantReview);
        linearCuisineDetails = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityCuisineDetails);
        linearReviewDetails = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityReviewsDetails);
        linearFooterContactRestaurant = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityFooterContactRestaurant);
        btnContinue = (Button) view.findViewById(R.id.buttonRestaurantDetailsActivityFooterContinue);
        linearFooterPickers = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityFooterPickers);
        linearViewDateTimePeoplePickers = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityFooterDateTimePeoplePickerView);
        imgBackground = (ImageButton) view.findViewById(R.id.imageViewRestaurantDetailsActivityFooterBackground);
        rideRequestButton = (RideRequestButton) view.findViewById(R.id.rideRequestButton);
        relativeUber = (RelativeLayout) view.findViewById(R.id.relativeLayoutUber);

        linearDatePicker = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityFooterDatePicker);
        linearPeoplePicker = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantDetailsActivityFooterPeoplePicker);
        txtDatePicker = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityFooterDatePicker);
        imgDownArrowDatePicker = (ImageView) view.findViewById(R.id.imageViewRestaurantDetailsActivityFooterDatePickerDownArrow);
        imgDownArrowPeoplePicker = (ImageView) view.findViewById(R.id.imageViewRestaurantDetailsActivityFooterPeopleDownArrow);
        imgDownArrowPeoplePicker = (ImageView) view.findViewById(R.id.imageViewRestaurantDetailsActivityFooterPeopleDownArrow);

        txtInviteFriends = (TextView) view.findViewById(R.id.textViewRestaurantDetailsActivityInviteFriends);

//        datePicker = (DatePicker) view.findViewById(R.id.datePickerRestaurantDetailsActivityFooter);
        numberPickerDay = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantDetailsActivityFooterDay);
        numberPickerTime = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantDetailsActivityFooterTime);
        numberPickerPeople = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantDetailsActivityFooterPeople);
        btnDoneDatePicker = (Button) view.findViewById(R.id.buttonRestaurantDetailsActivityFooterDatePickerDone);
        expandableTextView = (ExpandableTextView) this.findViewById(R.id.expandableTextView);
        buttonToggle = (ImageView) this.findViewById(R.id.button_toggle);
        expandableTextView.setAnimationDuration(1000L);

        // set interpolators for both expanding and collapsing animations
        expandableTextView.setInterpolator(new OvershootInterpolator());

        tokenViewTastebuds.allowCollapse(false);
        tokenViewTastebuds.setAdapter(null);
        tokenViewTastebuds.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.None);

// or set them separately
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());
        if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT) && getIntent().getSerializableExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT) != null) {
            /*restaurantDetails = (RestaurantListVO) getIntent().getSerializableExtra("restaurant");*/
            restaurantDetails = (RestaurantListVO) getIntent().getSerializableExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT);
            GetTimeMeasureBetweenCoOrdinates();
            GoogleRestaurentDetailApi();
        }
        createCustomAnimation();
        setUpRestaurantDetails(false);
        setListener();

        setAllTypefaceMontserratRegular(view);
        setAllTypefaceMontserratLight(txtCuisine);
        setAllTypefaceMontserratLight(numberPickerDay);
        setAllTypefaceMontserratLight(numberPickerTime);
        setAllTypefaceMontserratLight(numberPickerPeople);

        // Show/Hide invite friends button here.
        if (getIntent().hasExtra("isToShowInviteFriends") && getIntent().getBooleanExtra("isToShowInviteFriends", false)) {
            txtInviteFriends.setVisibility(View.VISIBLE);
        } else {
            txtInviteFriends.setVisibility(View.GONE);
        }

        RideParameters rideParams = new RideParameters.Builder()
                // Optional product_id from /v1/products endpoint (e.g. UberX). If not provided, most cost-efficient product will be used
//                .setProductId("a1111c8c-c720-46c3-8534-2fcdd730040d")
                .setPickupLocation(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")),
                        Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")),
                        SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, ""), "My Location")
                .setDropoffLocation(Double.valueOf(restaurantDetails.getLatitude()), Double.valueOf(restaurantDetails.getLongitude()), restaurantDetails.getName(), restaurantDetails.getAddress())
                .build();
        rideRequestButton.setRideParameters(rideParams);
    }

    private void createCustomAnimation() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (menuFabListFilter.isOpened()) {
                    imgGradientFab.setVisibility(View.VISIBLE);
                } else {
                    imgGradientFab.setVisibility(View.GONE);
                }

                menuFabListFilter.getMenuIconView().setImageResource(menuFabListFilter.isOpened()
                        ? R.drawable.ic_close_white : R.drawable.ic_logo_welcome_white_small);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        menuFabListFilter.setIconToggleAnimatorSet(set);
    }

    public void GoogleRestaurentDetailApi() {
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
//            params.put(getString(R.string.google_api_param_key_), getString(R.string.google_api_param_key_value));
//            params.put(getString(R.string.google_api_param_key_reference), restaurantDetails.getReference());
//            params.put(getString(R.string.google_api_param_key_place_id), restaurantDetails.getPlaceid());
            params.put(getString(R.string.google_api_param_key_), getString(R.string.google_api_param_key_value));
            params.put(getString(R.string.google_api_param_key_place_id), restaurantDetails.getPlaceid());
//            params.put(getString(R.string.google_api_param_key_reference), restaurantDetails.getReference());

//            if(!searchKeyWord.isEmpty())
//            {
//                params.put(getString(R.string.zomato_api_param_key_q), searchKeyWord);
//            }

            new HttpRequestSingleton(context, getString(R.string.google_api_restaurant_details_url), params,
                    Constants.ACTION_CODE_GOOGLE_DETAIL_API, RestaurantDetailsActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));

        }
    }

    private void setUpRestaurantDetails(boolean isToReset) {
        if (isToReset) {

        }

        if (restaurantDetails != null) {
            for (int i = 0; i < restaurantDetails.getPhotos().size(); i++) {

                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView
                        .image(appendUrl(restaurantDetails.getPhotos().get(i))).empty(R.drawable.ic_placeholder_restaurant).setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                sliderRestoDetails.addSlider(customSliderView);

            }

            sliderRestoDetails.setDuration(8000);
            sliderRestoDetails.addOnPageChangeListener(this);

        }

        txtRestaurantName.setText(restaurantDetails.getName().trim());
        txtCuisine.setText(restaurantDetails.getVicinity().trim());
        expandableTextView.setText(Html.fromHtml(restaurantDetails.getDescription().trim()));

        // Clear already set tokens/tags of tastebuds
        tokenViewTastebuds.clearListSelection();
        tokenViewTastebuds.clear();

        for (int i = 0; i < restaurantDetails.getCuisinesDetails().size(); i++) {
            String name = restaurantDetails.getCuisinesDetails().get(i).trim();
            if (i % 2 == 0) {
                TasteBudsToken token = new TasteBudsToken(name, name, -1, true);
                tokenViewTastebuds.addObject(token);
            } else {
                TasteBudsToken token = new TasteBudsToken(name, name, -1, false);
                tokenViewTastebuds.addObject(token);
            }
        }

            /*String cuisinesDetails = restaurantDetails.getCuisine().trim() + " - ";

            for (int i = 0; i < restaurantDetails.getCuisinesDetails().size(); i++) {
                if (i == 0) {
                    cuisinesDetails = cuisinesDetails + restaurantDetails.getCuisinesDetails().get(i).trim();
                } else {
                    cuisinesDetails = cuisinesDetails + ", " + restaurantDetails.getCuisinesDetails().get(i).trim();
                }
            }*/
//            txtCuisineDetails.setText(cuisinesDetails.trim());

        if (restaurantDetails.getListReview().size() > 0) {
            linearReviewDetails.setVisibility(View.VISIBLE);

            if (restaurantDetails.getListReview().size() > 1) {
                txtReadAllReviews.setVisibility(View.VISIBLE);
            } else {
                txtReadAllReviews.setVisibility(View.GONE);
            }

            final RestaurantReviewVO review = restaurantDetails.getListReview().get(0);
            if (!review.getProfilePicUrl().trim().isEmpty()) {
                Picasso.with(context).load(review.getProfilePicUrl().trim()).resize(Constants.convertDpToPixels(36), Constants.convertDpToPixels(36)).centerCrop()
                        .placeholder(R.drawable.shape_red_primary_circle_small).error(R.drawable.shape_red_primary_circle_small)
                        .into(imgReviewUserProfilePic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.d(TAG, "Error loading reviewed user profile pic: " + review.getProfilePicUrl().trim());
                            }
                        });
            } else {
                imgReviewUserProfilePic.setImageResource(R.drawable.shape_red_primary_circle_small);
            }

            txtReviewUserName.setText(review.getUsername().trim());
            txtReviewDatetime.setText(review.getDatetime().trim());
            ratingBarReview.setRating((float) review.getReviewRating());
            txtReviewDescrition.setText(Html.fromHtml(review.getDescription().trim()));
        } else {
            linearReviewDetails.setVisibility(View.GONE);
        }

        if (restaurantDetails.getWebsite() != null && !restaurantDetails.getWebsite().trim().isEmpty()
                && !restaurantDetails.getWebsite().trim().equalsIgnoreCase("null")) {
            txtWebsite.setSelected(true);
            txtWebsite.setClickable(true);
            txtWebsite.setFocusable(true);
            txtWebsite.setFocusableInTouchMode(true);
            txtWebsite.setText(restaurantDetails.getWebsite());
        } else {
            txtWebsite.setSelected(false);
            txtWebsite.setClickable(false);
            txtWebsite.setFocusable(false);
            txtWebsite.setFocusableInTouchMode(false);
        }

        if (restaurantDetails.getFormatted_phone_number() != null && !restaurantDetails.getFormatted_phone_number().trim().isEmpty()
                && !restaurantDetails.getFormatted_phone_number().trim().equalsIgnoreCase("null")) {
            txtCall.setSelected(true);
            txtCall.setClickable(true);
            txtCall.setText(restaurantDetails.getFormatted_phone_number());
//                txtCall.setFocusable(true);
//                txtCall.setFocusableInTouchMode(true);
        } else {
            txtCall.setSelected(false);
            txtCall.setClickable(false);
//                txtCall.setFocusable(false);
//                txtCall.setFocusableInTouchMode(false);
        }

        if (restaurantDetails.getLatitude() != null && restaurantDetails.getLongitude() != null) {
            txtDirection.setSelected(true);
            txtDirection.setClickable(true);
            txtDirection.setFocusable(true);
            txtDirection.setFocusableInTouchMode(true);
            txtDirection.setText(restaurantDetails.getFormatted_address());
                /*rideRequestButton.setVisibility(View.VISIBLE);*/
            rideRequestButton.setVisibility(View.VISIBLE);
        } else {
            txtDirection.setSelected(false);
            txtDirection.setClickable(false);
            rideRequestButton.setVisibility(View.VISIBLE);
//                txtDirection.setFocusable(false);
//                txtDirection.setFocusableInTouchMode(false);
        }
    }

    private void setListener() {
        txtReadAllReviews.setOnClickListener(this);
        txtWebsite.setOnClickListener(this);
        txtCall.setOnClickListener(this);
        txtDirection.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
        linearDatePicker.setOnClickListener(this);
        linearPeoplePicker.setOnClickListener(this);
        btnDoneDatePicker.setOnClickListener(this);
        imgBackground.setOnClickListener(this);
        buttonToggle.setOnClickListener(this);
        relativeUber.setOnClickListener(this);
        fabFilter.setOnClickListener(this);
        fabList.setOnClickListener(this);
        imgGradientFab.setOnClickListener(this);
        txtInviteFriends.setOnClickListener(this);
    }

    public void setPickerForDay() {
        int posSelectedDay = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Prepare days list here. Days list will be only for a week.
        listDaysPicker.clear();
        numberPickerDay.setDisplayedValues(null);
        for (int i = 0; i < 7; i++) {
            Calendar calDay = Calendar.getInstance();
            calDay.add(Calendar.DAY_OF_YEAR, i);
            String strDay = new SimpleDateFormat("EEEE").format(calDay.getTime());
            Log.d(TAG, "Day to add: " + strDay + ", Date: " + calDay.getTime());
            DaysPickerVO obj = new DaysPickerVO();
            obj.setCalendarDay(calDay);
            if (i == 0) {
                obj.setStrDay(getString(R.string.today));
            } else if (i == 1) {
                obj.setStrDay(getString(R.string.tomorrow));
            } else {
                obj.setStrDay(strDay.trim());
            }
            listDaysPicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calDay.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calDay.get(Calendar.DAY_OF_YEAR)) {
                    posSelectedDay = i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listDaysPicker.size()];
        for (int i = 0; i < listDaysPicker.size(); i++) {
            strDays[i] = listDaysPicker.get(i).getStrDay().trim();
        }

        numberPickerDay.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerDay.setMaxValue(strDays.length - 1);
            numberPickerDay.setWrapSelectorWheel(false);
            numberPickerDay.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerDay);

            // TODO Set time picker here as 1st position is selected by default for picker.
            if (!isFromIntent) {
                calDateSelected.setTime(listDaysPicker.get(0).getCalendarDay().getTime());
            }
            setPickerForTime();
            updateDateTitleSelectedFromPicker();

            numberPickerDay.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    // TODO Set time picker here depending on date selected.
                    Log.d(TAG, "Day selected at pos " + newVal + " is: " + listDaysPicker.get(newVal).getCalendarDay().getTime());
                    calDateSelected.setTime(listDaysPicker.get(newVal).getCalendarDay().getTime());
                    updateDateTitleSelectedFromPicker();
                    setPickerForTime();
                }
            });

            numberPickerDay.setValue(posSelectedDay);
        }
    }

    public void setPickerForTime() {
        Calendar calToday = Calendar.getInstance();
        int posSelectedTime = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar calStartTime = Calendar.getInstance();
        calStartTime.setTime(calDateSelected.getTime());
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR) && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) {
            int currentMinute = calStartTime.get(Calendar.MINUTE);
            int diffToAdd = 30 - (currentMinute % 30);

            calStartTime.add(Calendar.MINUTE, diffToAdd);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        } else {
            calStartTime.set(Calendar.HOUR_OF_DAY, 0);
            calStartTime.set(Calendar.MINUTE, 0);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        }

        Calendar calEndTime = Calendar.getInstance();
        calEndTime.setTime(calDateSelected.getTime());
        calEndTime.set(Calendar.HOUR_OF_DAY, 23);
        calEndTime.set(Calendar.MINUTE, 59);
        calEndTime.set(Calendar.SECOND, 59);
        calEndTime.set(Calendar.MILLISECOND, 999);

        Log.d(TAG, "Start time: " + calStartTime.getTime() + ", End time: " + calEndTime.getTime());

        // Clear list.
        listTimePicker.clear();
        numberPickerTime.setDisplayedValues(null);
        while (calStartTime.getTimeInMillis() <= calEndTime.getTimeInMillis()) {
            String strTime = new SimpleDateFormat("hh:mm aa").format(calStartTime.getTime());
            Log.d(TAG, "Time to add: " + strTime + ", Date: " + calStartTime.getTime());
            TimePickerVO obj = new TimePickerVO();
            obj.setCalendarTime(calStartTime);
            obj.setStrTime(strTime.trim());
            listTimePicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calStartTime.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calStartTime.get(Calendar.DAY_OF_YEAR)
                        && calDateSelected.get(Calendar.HOUR_OF_DAY) == calStartTime.get(Calendar.HOUR_OF_DAY)
                        && calDateSelected.get(Calendar.MINUTE) == calStartTime.get(Calendar.MINUTE)) {
                    posSelectedTime = listTimePicker.size() - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                calStartTime.add(Calendar.MINUTE, 30);
            }
        }

        String[] strDays = new String[listTimePicker.size()];
        for (int i = 0; i < listTimePicker.size(); i++) {
            strDays[i] = listTimePicker.get(i).getStrTime().trim();
        }

        numberPickerTime.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerTime.setMaxValue(strDays.length - 1);
            numberPickerTime.setWrapSelectorWheel(false);
            numberPickerTime.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerTime);
            numberPickerTime.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    calDateSelected.setTime(listTimePicker.get(newVal).getCalendarTime().getTime());
                }
            });
            numberPickerTime.setValue(posSelectedTime);
        }
    }

    public void setPickerForPeople() {
        int posSelectedPeople = 0;
        int peopleFromIntent = 0;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_PEOPLE)) {
                peopleFromIntent = getIntent().getIntExtra(Constants.KEY_INTENT_EXTRA_PEOPLE, 0);
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Clear list.
        listPeoplePicker.clear();
        numberPickerPeople.setDisplayedValues(null);
        for (int i = 0; i < 9; i++) {
            PeoplePickerVO obj = new PeoplePickerVO();
            obj.setPeopleDisplay(String.valueOf(i + 1) + " " + getString(R.string.people));
            obj.setPeopleNumber((i + 1));
            listPeoplePicker.add(obj);

            try {
                if (peopleFromIntent == (i + 1)) {
                    posSelectedPeople = (i + 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listPeoplePicker.size()];
        for (int i = 0; i < listPeoplePicker.size(); i++) {
            strDays[i] = listPeoplePicker.get(i).getPeopleDisplay().trim();
        }

        numberPickerPeople.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerTime.setMaxValue(strDays.length - 1);
            numberPickerPeople.setMaxValue(strDays.length - 1);
            numberPickerPeople.setWrapSelectorWheel(false);
            numberPickerPeople.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerPeople);
            numberPickerPeople.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    peopleSelected = listPeoplePicker.get(newVal);
                }
            });
            numberPickerPeople.setValue(posSelectedPeople);
        }
    }

    public void updateDateTitleSelectedFromPicker() {
        // TODO Set Selected date in action bar
        Calendar calToday = Calendar.getInstance();
        int selectedDayOfYearCalendar = calDateSelected.get(Calendar.DAY_OF_YEAR);
        int tomorrow = calToday.get(Calendar.DAY_OF_YEAR) + 1;
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) { // Selected date is of today
            txtDatePicker.setText(getString(R.string.today));
        } else if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && tomorrow == selectedDayOfYearCalendar) {
            txtDatePicker.setText(getString(R.string.tomorrow));
        } else {
            String dayOfMonth = String.valueOf(calDateSelected.get(Calendar.DAY_OF_MONTH));
            int day = calDateSelected.get(Calendar.DAY_OF_MONTH);
            String charAtLastPosition = dayOfMonth.substring(dayOfMonth.length() - 1);
            if (Integer.parseInt(charAtLastPosition) == 1 && day != 11) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>st</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 2 && day != 12) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>nd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 3 && day != 13) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>rd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>th</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            }
        }
    }

    public void expandView(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapseView(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void colorizeDatePicker(NumberPicker datePicker) {
        Resources system = Resources.getSystem();
        int dayId = system.getIdentifier("day", "id", "android");
        int monthId = system.getIdentifier("month", "id", "android");
        int yearId = system.getIdentifier("year", "id", "android");

        NumberPicker dayPicker = (NumberPicker) datePicker.findViewById(dayId);
        NumberPicker monthPicker = (NumberPicker) datePicker.findViewById(monthId);
        NumberPicker yearPicker = (NumberPicker) datePicker.findViewById(yearId);

        setDividerColorNumberPicker(dayPicker);
        setDividerColorNumberPicker(monthPicker);
        setDividerColorNumberPicker(yearPicker);
    }

    private void setDividerColorNumberPicker(NumberPicker picker) {
        if (picker == null)
            return;

        final int count = picker.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                Field dividerField = picker.getClass().getDeclaredField("mSelectionDivider");
                dividerField.setAccessible(true);
                ColorDrawable colorDrawable = new ColorDrawable(picker.getResources().getColor(R.color.colorPrimary));
                dividerField.set(picker, colorDrawable);
                picker.invalidate();
            } catch (Exception e) {
                Log.w("setDividerColor", e);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        setDividerColorNumberPicker(numberPickerDay);
//        setDividerColorNumberPicker(numberPickerTime);
//        setDividerColorNumberPicker(numberPickerPeople);
//        setPickerForPeople();
//        setPickerForDay();

        if (isPickerSelected) {
            imgBackground.setVisibility(View.GONE);
            linearViewDateTimePeoplePickers.setVisibility(View.GONE);
            linearFooterPickers.setVisibility(View.GONE);
            btnContinue.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + restaurantDetails.getFormatted_phone_number().trim()));
                    startActivity(intent);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.textViewRestaurantDetailsActivityRestaurantReviewsReadAll:
                intent = new Intent(context, ReviewsActivity.class);
                intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, restaurantDetails);
                startActivity(intent);
                break;

            case R.id.textViewRestaurantDetailsActivityRestaurantWebsite:
                if (txtWebsite.isSelected() && !restaurantDetails.getWebsite().trim().isEmpty()) {
                    intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra("link", restaurantDetails.getWebsite().trim());
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
                break;

            case R.id.textViewRestaurantDetailsActivityRestaurantCall:
                if (restaurantDetails != null && !restaurantDetails.getFormatted_phone_number().trim().isEmpty()) {

                    if (ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                android.Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
//

                        } else {

                            // No explanation needed, we can request the permission.

                            requestPermissions(
                                    new String[]{android.Manifest.permission.CALL_PHONE},
                                    MY_PERMISSIONS_REQUEST_CALL_PHONE);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }

                    } else {
                        intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + restaurantDetails.getFormatted_phone_number().trim()));
                        startActivity(intent);
                    }
                }
                break;

            case R.id.textViewRestaurantDetailsActivityRestaurantDirection:
                if (txtDirection.isSelected()) {
                    /*String currentLat = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0");
                    String currentLong = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + restaurantDetails.getLatitude() + ","
                            + restaurantDetails.getLongitude() + "");
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);*/

                    String currentLat = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0");
                    String currentLong = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");

                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr="
                                    + currentLat + ","
                                    + currentLong
                                    + "&daddr=" + restaurantDetails.getLatitude() + "," + restaurantDetails.getLongitude()));
                    startActivity(intent);
//                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
                break;

            case R.id.buttonRestaurantDetailsActivityFooterContinue:
                createDialogPostEvents();
                break;

            case R.id.linearLayoutRestaurantDetailsActivityFooterDatePicker:
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.VISIBLE);*/
                    imgBackground.setVisibility(View.GONE);
                    collapseView(linearViewDateTimePeoplePickers);
                } else {
                    // TODO Open datepicker view
                    isToExpandDatePicker = true;
                    if (imgDownArrowDatePicker.getRotation() != -180) {
                        imgDownArrowDatePicker.animate().rotationBy(-180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != -180) {
                        imgDownArrowPeoplePicker.animate().rotationBy(-180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.GONE);*/
                    imgBackground.setVisibility(View.GONE);
                    expandView(linearViewDateTimePeoplePickers);
                }
                break;

            case R.id.buttonRestaurantDetailsActivityFooterDatePickerDone:
                isPickerSelected = true;
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.VISIBLE);*/
                    collapseView(linearViewDateTimePeoplePickers);
                    imgBackground.setVisibility(View.GONE);
                    linearFooterPickers.setVisibility(View.GONE);
                    btnContinue.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.linearLayoutRestaurantDetailsActivityFooterPeoplePicker:
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    imgBackground.setVisibility(View.GONE);
                    collapseView(linearViewDateTimePeoplePickers);
                } else {
                    // TODO Open datepicker view
                    isToExpandDatePicker = true;
                    if (imgDownArrowDatePicker.getRotation() != -180) {
                        imgDownArrowDatePicker.animate().rotationBy(-180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != -180) {
                        imgDownArrowPeoplePicker.animate().rotationBy(-180f).start();
                    }
                    imgBackground.setVisibility(View.VISIBLE);
                    expandView(linearViewDateTimePeoplePickers);
                }
                break;

            case R.id.imageViewRestaurantDetailsActivityFooterBackground:

                break;
            case R.id.button_toggle:
                if (expandableTextView.isExpanded()) {
                    if (buttonToggle.getRotation() != 0) {
                        buttonToggle.animate().rotationBy(180f).start();
                    }

                    expandableTextView.collapse();
                } else {
                    if (buttonToggle.getRotation() != -180) {
                        buttonToggle.animate().rotationBy(-180f).start();
                    }
                    expandableTextView.expand();
                }

                break;

            case R.id.relativeLayoutUber:
                createDialogGetUberRide();
                break;

            case R.id.imageViewListActivityGradientRestoDetail:
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                } else {
                    menuFabListFilter.open(true);
                }
                break;

            case R.id.fabListRestoDetail:
                menuFabListFilter.toggle(true);

                intent = new Intent(context, RestaurantsListActivity.class);
                startActivity(intent);
                break;

            case R.id.fabFilterRestoDetail:
                intent = new Intent(context, HomeActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.textViewRestaurantDetailsActivityInviteFriends:
                // Create request for invite friends.
                sendRequestCreateFirebaseMessage();
                break;

            default:
                break;

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API) {
                stopProgress();
                Log.d(TAG, "DISTANCE MATRIX API RESPONSE: " + response);

                try {
                    // Parse API response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";

                    if (status.trim().equalsIgnoreCase("OK")) {
                        // Success. Update result for time travel in list here.
                        JSONArray rows = !jsonObjectResponse.isNull("rows") ? jsonObjectResponse.getJSONArray("rows") : new JSONArray();
                        for (int i = 0; i < rows.length(); i++) {
                            JSONObject row = rows.getJSONObject(i);
                            JSONArray elements = !row.isNull("elements") ? row.getJSONArray("elements") : new JSONArray();
                            for (int j = 0; j < elements.length(); j++) {
                                JSONObject element = elements.getJSONObject(j);
                                String statusElement = !element.isNull("status") ? element.getString("status").trim() : "";
                                if (statusElement.trim().equalsIgnoreCase("OK")) {
                                    JSONObject duration = !element.isNull("duration") ? element.getJSONObject("duration") : new JSONObject();
                                    String text = !duration.isNull("text") ? duration.getString("text").trim() : "";
                                    if (!text.trim().isEmpty()) {
                                        txtTimeToReach.setText(text);

                                    } else {
                                        txtTimeToReach.setText("");

                                    }
                                } else {
                                    txtTimeToReach.setText("");

                                }
                            }
                        }
                    } else {

                        txtTimeToReach.setText("");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } finally {

                    int posNew = -1;

                }
            } else if (action == Constants.ACTION_CODE_GOOGLE_DETAIL_API) {
                stopProgress();
                Log.d(TAG, "Google Place Details API Response: " + response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);

                    boolean status = !jsonObjectResponse.isNull("status") && jsonObjectResponse.getString("status").trim().equalsIgnoreCase("OK") ? true : false;
                    if (status) {
                        JSONObject result = !jsonObjectResponse.isNull("result") ? jsonObjectResponse.getJSONObject("result") : new JSONObject();
                        String formatted_address = !result.isNull("formatted_address") ? result.getString("formatted_address").trim() : "";
                        String phone_number = !result.isNull("international_phone_number") ? result.getString("international_phone_number").trim() : "";
                        String website = !result.isNull("website") ? result.getString("website").trim() : "";

                        if (phone_number.trim().isEmpty()) {
                            phone_number = !result.isNull("formatted_phone_number") ? result.getString("formatted_phone_number").trim() : "";
                        }
                        restaurantDetails.setFormatted_address(formatted_address);
                        restaurantDetails.setFormatted_phone_number(phone_number);
                        restaurantDetails.setWebsite(website);

                        JSONObject opening_hours = !result.isNull(getString(R.string.google_response_opening_hours))
                                ? result.getJSONObject(getString(R.string.google_response_opening_hours)) : new JSONObject();
                        String open_now = !opening_hours.isNull(getString(R.string.google_response_open_now)) ? opening_hours.getString(getString(R.string.google_response_open_now)).trim() : "";
                        if (open_now.equalsIgnoreCase("true")) {

                            ArrayList<String> timings = new ArrayList<>();
                            JSONArray weekday_text = !opening_hours.isNull("weekday_text") ? opening_hours.getJSONArray("weekday_text") : new JSONArray();
                            for (int i = 0; i < weekday_text.length(); i++) {
                                timings.add(weekday_text.getString(i));
                            }
                            Calendar c = Calendar.getInstance();
                            int a = c.get(Calendar.DAY_OF_WEEK);
                            textViewRestaurantDetailsActivityTime.setText(timings.get(a - 1));
                        } else {
                            textViewRestaurantDetailsActivityTime.setText("CLOSED");
                        }

                        // Reviews here
                        JSONArray reviews = !result.isNull("reviews") ? result.getJSONArray("reviews") : new JSONArray();
                        for (int i = 0; i < reviews.length(); i++) {
                            JSONObject review = reviews.getJSONObject(i);
                            String rating = "N.A.";
                            /*JSONArray aspects = !review.isNull("aspects") ? review.getJSONArray("aspects") : new JSONArray();
                            for(int j = 0;j<aspects.length();j++){
                                JSONObject aspect = aspects.getJSONObject(j);
                                rating = !aspect.isNull("rating") ? aspect.getString("rating").trim() : "N.A.";
                            }*/
                            String name = !review.isNull("author_name") ? review.getString("author_name").trim() : "";
                            String profile_photo_url = !review.isNull("profile_photo_url") ? review.getString("profile_photo_url") : "";
                            rating = !review.isNull("rating") ? String.valueOf(review.getDouble("rating")) : "";
                            String text = !review.isNull("text") ? review.getString("text").trim() : "";

                            RestaurantReviewVO obj = new RestaurantReviewVO();
                            obj.setReviewRating(Double.valueOf(rating));
                            obj.setUsername(name);
                            obj.setProfilePicUrl(profile_photo_url);
                            obj.setDescription(text);
                            restaurantDetails.getListReview().add(obj);
                        }

                        JSONArray photos = !result.isNull("photos") ? result.getJSONArray("photos") : new JSONArray();

                        ArrayList<String> photosList = new ArrayList<>();
                        for (int i = 0; i < photos.length(); i++) {

                            JSONObject photosObj = photos.getJSONObject(i);

                            String photo_reference = !photosObj.isNull("photo_reference") ? photosObj.getString("photo_reference").trim() : "";

                            photosList.add("https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + widthScreen + "&photoreference="
                                    + photo_reference + "&key=AIzaSyCwErSCCBZN2fL0y9znT3wCY7qrhBxd0JI");

                        }
                        restaurantDetails.setPhotos(photosList);

                        setUpRestaurantDetails(false);

                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(getString(R.string.some_error_occured));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(e.getMessage());
                }
            } else if (action == Constants.ACTION_CODE_API_CREATE_FIREBASE_MESSAGE && response != null) {
                Log.i(TAG, "Response received for CREATE_FIREBASE_MESSAGE: " + response);
                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string
                            .api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            String id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            String userid = !data.isNull(context.getResources().getString(R.string.api_response_param_key_userid))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_userid)).trim() : "";
                            String message = !data.isNull(context.getResources().getString(R.string.api_response_param_key_message))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_message)).trim() : "";
                            String createdat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_createdat))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_createdat)).trim() : "";
                            String updatedat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_updatedat))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_updatedat)).trim() : "";

                            // Fetch id as token in invite message.
                            if (!id.trim().isEmpty()) {
                                stopProgress();
                                setUpShareIntentInviteFriends(context, id.trim());
                            } else {
                                stopProgress();
                                showSnackBarMessageOnly(context.getResources().getString(R.string.error_in_generating_invite_token));
                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            }
        }
    }

    public void createDialogPostEvents() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_restaurant_card_dialog);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout linearRoot;
        ImageView imgRestaurantPic;
        TextView txtShare, txtCalendar, txtModify, txtCancel, txtRestaurantName, txtRestaurantTableDetails, txtRestaurantAddress;

        linearRoot = (LinearLayout) dialog.findViewById(R.id.linearLayoutRestaurantCardPopupRoot);
        imgRestaurantPic = (ImageView) dialog.findViewById(R.id.imageViewRestaurantCardPopupRestaurantPic);
        txtShare = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupShare);
        txtCalendar = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupCalendar);
        txtModify = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupModify);
        txtCancel = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupCancel);
        txtRestaurantName = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupRestaurantName);
        txtRestaurantTableDetails = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupRestaurantTableDetails);
        txtRestaurantAddress = (TextView) dialog.findViewById(R.id.textViewRestaurantCardPopupRestaurantAddress);

        setAllTypefaceMontserratRegular(linearRoot);
        setAllTypefaceMontserratLight(txtShare);
        setAllTypefaceMontserratLight(txtCalendar);
        setAllTypefaceMontserratLight(txtCancel);
        setAllTypefaceMontserratLight(txtModify);

        if (restaurantDetails != null) {
//            Transformation transformation = new RoundedCornersTransformation(radius, margin);
//            final int radius = 5;
//            final int margin = 5;
//            final Transformation transformation = new RoundedCornersTransformation(radius, margin);
//            Picasso.with(activity).load(url).transform(transformation).into(imageView);
//            if (!restaurantDetails.getUrl().trim().isEmpty())
            if (!restaurantDetails.getWebsite().trim().isEmpty())
                Picasso.with(context).load(restaurantDetails.getWebsite().trim())
                        .resize(Constants.convertDpToPixels(332), Constants.convertDpToPixels(152))
                        .into(imgRestaurantPic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.d(TAG, "Error loading restaurant pic for Card: " + restaurantDetails.getWebsite().trim());
                            }
                        });

            txtRestaurantName.setText(restaurantDetails.getName().trim());
            txtRestaurantAddress.setText(restaurantDetails.getAddress().trim());

            // Set Selected Table details here.
            String strTableDetails = getString(R.string.table_for) + " " + peopleSelected.getPeopleDisplay() + "<br />"
                    + "<font color=\"" + getResources().getColor(R.color.colorPrimary) + "\">" + new SimpleDateFormat("EEEE, MMMM dd, yyyy").format(calDateSelected.getTime())
                    + " " + getString(R.string.at) + " " + new SimpleDateFormat("hh:mm aa").format(calDateSelected.getTime()) + "</font>";
            txtRestaurantTableDetails.setText(Html.fromHtml(strTableDetails));
        }

        txtShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                // Share details with intent here.
            }
        });

        txtCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                // Add current restaurant booking to Google Calendar here.
            }
        });

        txtModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                // Edit Table Booking Date Time & People here.
            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                /*// Cancel popup here.
                intent = new Intent(context, SingleUserSuggestionActivity.class);
                startActivity(intent);*/
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    public void createDialogGetUberRide() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_request_uber_ride);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        validateConfiguration(ApplicationClass.config);
        ServerTokenSession session = new ServerTokenSession(ApplicationClass.config);

        RideParameters rideParametersForProduct = new RideParameters.Builder()
                .setProductId(UBERX_PRODUCT_ID)
                // .setProductId(UBERX_PRODUCT_ID) // Optional product_id from /v1/products endpoint (e.g. UberX). If not provided, most cost-efficient product will be
                // used
                .setPickupLocation(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")),
                        Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")), "", "")
                .setDropoffLocation(Double.valueOf(restaurantDetails.getLatitude()), Double.valueOf(restaurantDetails.getLongitude()), restaurantDetails.getName(), restaurantDetails.getAddress())
                .build();

        // This button demonstrates launching the RideRequestActivity (customized button behavior).
        // You can optionally setRideParameters for pre-filled pickup and dropoff locations.
        RideRequestButton uberButtonWhite = (RideRequestButton) dialog.findViewById(R.id.uber_button_white);
        TextView txtRide = (TextView) dialog.findViewById(R.id.textViewUberGetRideDialogTitle);

        UberButton button = (UberButton) uberButtonWhite.getChildAt(0);
        button.setText(getString(R.string.ride_with_uber));

        setAllTypefaceMontserratLight(txtRide);
        txtRide.setText(getString(R.string.get_a_ride_to_restaurant) + " " + restaurantDetails.getName().trim());

        RideRequestActivityBehavior rideRequestActivityBehavior = new RideRequestActivityBehavior(this, WIDGET_REQUEST_CODE, ApplicationClass.config);
        uberButtonWhite.setRequestBehavior(rideRequestActivityBehavior);
        uberButtonWhite.setRideParameters(rideParametersForProduct);
        uberButtonWhite.setSession(session);
        uberButtonWhite.loadRideInformation();

        dialog.setCancelable(true);
        dialog.show();
    }

    /**
     * Validates the local variables needed by the Uber SDK used in the sample project
     *
     * @param configuration
     */
    private void validateConfiguration(SessionConfiguration configuration) {
        String nullError = "%s must not be null";
        String sampleError = "Please update your %s in the gradle.properties of the project before " +
                "using the Uber SDK Sample app. For a more secure storage location, " +
                "please investigate storing in your user home gradle.properties ";

        checkNotNull(configuration, String.format(nullError, "SessionConfiguration"));
        checkNotNull(configuration.getClientId(), String.format(nullError, "Client ID"));
        checkNotNull(configuration.getRedirectUri(), String.format(nullError, "Redirect URI"));
        checkNotNull(configuration.getServerToken(), String.format(nullError, "Server Token"));
        checkState(!configuration.getClientId().equals("insert_your_client_id_here"),
                String.format(sampleError, "Client ID"));
        checkState(!configuration.getRedirectUri().equals("insert_your_redirect_uri_here"),
                String.format(sampleError, "Redirect URI"));
        checkState(!configuration.getRedirectUri().equals("insert_your_server_token_here"),
                String.format(sampleError, "Server Token"));
    }

    public void GetTimeMeasureBetweenCoOrdinates() {
        String destination = "";

        destination = restaurantDetails.getLatitude() + "," + restaurantDetails.getLongitude();

        String origin = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");

        if (!destination.trim().isEmpty() && !destination.trim().equalsIgnoreCase(",") && !origin.trim().equalsIgnoreCase(",")) {
            if (NetworkUtil.isOnline(context)) {
                params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
                params.put(getString(R.string.api_param_key_origins), origin.trim());
                params.put(getString(R.string.api_param_key_destinations), destination.trim());
                params.put(getString(R.string.api_param_key_units), getString(R.string.api_param_value_units));
                params.put(getString(R.string.google_api_param_key_departure_time),
                        String.valueOf(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true)));
                params.put(getString(R.string.google_api_param_key_traffic_model), getString(R.string.api_param_value_best_guess));
                params.put(getString(R.string.api_param_key_key), getString(R.string.google_api_key));

                new HttpRequestSingleton(context, getString(R.string.google_distance_matrix_api_url), params,
                        Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API, RestaurantDetailsActivity.this);
            } else {
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.internet_not_available));

            }
        } else {

        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        txtViewCounterRestoDetails.setText(position + 1 + "/" + restaurantDetails.getPhotos().size());

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void sendRequestCreateFirebaseMessage() {
        // Prepare a firebase message here.
        String messageString = getString(R.string.hi_exclamation) + " " + getString(R.string.there)
//                + " " + list.get(pos).getFirstname() + " " + list.get(pos).getLastname()
                + "! " + getString(R.string.lets_go_and_grab_a_bite_at) + " " + restaurantDetails.getName().trim() + "?";
        Calendar c = Calendar.getInstance();
        /*MessageVO chat = new MessageVO();
        chat.setMsg(messageString);
        chat.setTimeStamp(Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true));
        chat.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());*/

        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        /*SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();*/

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(messageString);
        /*if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else*/
//        message.setRecieverId(list.get(pos).getId());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
//        message.setRecieverDP(list.get(pos).getProfilepic1().trim());
        message.setTimeStamp(timestampToSave);
        message.setContentType("text");
//        message.setRestoVo(restaurantDetails);

        MessageVO messageRestaurant = new MessageVO();
        messageRestaurant.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        messageRestaurant.setMsg(getString(R.string.sent_you_a_restaurant));
        /*if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else*/
//        message.setRecieverId(list.get(pos).getId());

        messageRestaurant.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
//        message.setRecieverDP(list.get(pos).getProfilepic1().trim());
        messageRestaurant.setTimeStamp(timestampToSave);
        messageRestaurant.setContentType("restaurant");
        messageRestaurant.setRestoVo(restaurantDetails);


        // Convert Message to Firebase object here.
        String messageToFirebase = "";
        Gson gson = new GsonBuilder().create();
        String toJson = gson.toJson(message);// obj is your object
        String toJsonRestaurant = gson.toJson(messageRestaurant);

        try {
            JSONObject jsonObj = new JSONObject(toJson);
            JSONObject jsonObjRestaurant = new JSONObject(toJsonRestaurant);
            JSONArray array = new JSONArray();
            array.put(jsonObj);
            array.put(jsonObjRestaurant);
            messageToFirebase = array.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Convert JSON to POJO
//        Gson gson = new Gson();
//        YourPOJO myPOJO = gson.fromJson(jsonString, YourPOJO.class);
//        Firebase ref = new Firebase(YOUR_FIREBASE_REF);
//        ref.setValue(myPOJO);
        Log.d(TAG, "Message generated: " + messageToFirebase);

        showProgress(getString(R.string.loading));
        HashMap<String, String> params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_createfirebasemessage));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_message), messageToFirebase.trim());

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_CREATE_FIREBASE_MESSAGE,
                RestaurantDetailsActivity.this);
    }
}

