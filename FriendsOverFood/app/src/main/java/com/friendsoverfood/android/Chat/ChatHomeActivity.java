package com.friendsoverfood.android.Chat;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatListAdapter;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatHomeActivity extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private RecyclerView recyclerViewChatHome;
    private LinearLayoutManager layoutManager;
    private ChatListAdapter adapterChatHome;
    private ArrayList<ChatListVO> itemList = new ArrayList<>();
//    private ListView listViewChatHome;
//    private AdapterChatHomeListAdapter adapterChatList;

    private final String TAG = "ChatHomeActivity";

    // Alarm receiver initiate object
    public AlarmManager alarms;

    // Firebase instances to load latest message from chat
    public DatabaseReference mDatabase;
    public DatabaseReference mDatabaseMessages;

    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListener();
/*        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        setTitleSupportActionBar(getString(R.string.converations));
        callFriendsListApi();

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(2); // Set search icon selected

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_chat_home_activity, getMiddleContent());
        recyclerViewChatHome = (RecyclerView) findViewById(R.id.recyclerViewChatHome);
//        imgViewSendRequest = (ImageView) findViewById(R.id.imgViewSendRequest);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewChatHome.setLayoutManager(layoutManager);
        recyclerViewChatHome.setHasFixedSize(false);

//        listViewChatHome = (ListView) view.findViewById(R.id.listViewChatHome);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("CHAT_HOME_ACTIVITY", "onAuthStateChanged:signed_in:" + user.getUid() + ", Name: " + user.getDisplayName());
                } else {
                    // User is signed out
                    Log.d("CHAT_HOME_ACTIVITY", "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    private void setListener() {
//        imgViewSendRequest.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
//            case R.id.imgViewSendRequest:
//                ActionSheet.createBuilder(this, getSupportFragmentManager())
//                        .setCancelButtonTitle("Cancel")
//                        .setOtherButtonTitles("Now", "Later")
//                        .setCancelableOnTouchOutside(true)
//                        .setListener(this).show();
//                break;
            default:
                break;

        }
    }

    public void callFriendsListApi() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, ChatHomeActivity.this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
            Log.i("Response", "ACTION_CODE_API_FRIEND_LIST: " + response);
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
            SharedPreferenceUtil.save();
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        ChatListVO item = new ChatListVO();
                        try {
                            dataInner = data.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                        String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                        String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                        String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                        item.setMatchid(matchId);
                        item.setFriend1(friend1);
                        item.setFriend2(friend2);
                        item.setStatus(statusFriend);

                        JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                        for (int j = 0; j < details.length(); j++) {

                            JSONObject detailInner = details.getJSONObject(j);
                            String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                            String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                            String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                            String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                            item.setUserId(userId);
                            item.setFirstName(first_name);
                            item.setLastName(lastName);
                            item.setProfilePic(profilePic);
                            item.setGender(gender);

                        }
                        itemList.add(item);
                    }

                    if (adapterChatHome == null) {
                        adapterChatHome = new com.friendsoverfood.android.ChatNew.ChatListAdapter(context, itemList);
                        recyclerViewChatHome.setAdapter(adapterChatHome);
                    } else {
                        adapterChatHome.notifyDataSetChanged();
                    }

                    /*if (adapterChatList == null) {
                        adapterChatList = new AdapterChatHomeListAdapter(context, itemList);
                        listViewChatHome.setAdapter(adapterChatList);
                    } else {
                        adapterChatList.notifyDataSetChanged();
                    }*/
                } else {
                    showSnackBarMessageOnly(errormessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    /*public void sortListNotifyChanges() {
        int sizeBefore = itemList.size();
        Collections.sort(itemList, new ComparatorLongDescending());
        if (adapterChatHome != null) {
            adapterChatHome.notifyItemRangeChanged(0, sizeBefore);
            adapterChatHome.notifyDataSetChanged();
        } else {
            adapterChatHome = new ChatAdapter(context, itemList);
        }
    }

    public class ComparatorLongDescending implements Comparator<ChatListVO> {

        public int compare(ChatListVO first, ChatListVO second) {
            *//*if (!first.getTime().trim().isEmpty() && !second.getTime().trim().isEmpty()) {
                long firstValue = Long.parseLong(first.getTime());
                long secondValue = Long.parseLong(second.getTime());
                return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
            } else {
                return 0;
            }*//*
            Log.d("SORT_CHAT_LIST", "First: "+first.getFirstName()+" "+first.lastName+", Timestamp: "+first.getTimestampLastSeen()+", Second: "+
                    second.getFirstName()+" "+second.lastName+", Timestamp: "+second.getTimestampLastSeen());

            long firstValue = first.getTimestampLastSeen();
            long secondValue = second.getTimestampLastSeen();
            return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
            *//*return 0;*//*

        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ACTION_CODE_CHAT_USERS_LIST) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(Constants.INTENT_CHAT_LIST) && data.getSerializableExtra(Constants.INTENT_CHAT_LIST) != null) {
                    int sizeBefore = itemList != null ? itemList.size() : 0;
                    itemList.clear();
                    if (adapterChatHome != null) {
                        adapterChatHome.notifyItemRangeChanged(0, sizeBefore);
                        adapterChatHome.notifyDataSetChanged();
                    }

                    ArrayList<ChatListVO> listUsers = (ArrayList<ChatListVO>) data.getSerializableExtra(Constants.INTENT_CHAT_LIST);
                    itemList.addAll(listUsers);

                    if (adapterChatHome == null) {
                        adapterChatHome = new ChatListAdapter(context, itemList);
                        recyclerViewChatHome.setAdapter(adapterChatHome);
                    } else {
                        adapterChatHome.notifyItemRangeInserted(0, itemList.size());
                        adapterChatHome.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        intent = new Intent(mContext, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        supportFinishAfterTransition();
    }
}
