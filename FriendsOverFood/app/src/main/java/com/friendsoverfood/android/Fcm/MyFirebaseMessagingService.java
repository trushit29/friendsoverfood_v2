

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.friendsoverfood.android.Fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.Welcome.WelcomeActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements HttpCallback {

    private static final String TAG = "MyFirebaseMsgService";
    public Intent intent;
    public String strImageURL = "";

    /* Action for friend request action:
        1 ==> Friend request accepted,
        2 ==> Friend request received
     */
    public int action = -1;
    public UserProfileAPI userProfileVo;


   /* private int skipChatList = 0;
    private int skipChatHistory = 0;*/

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled here in onMessageReceived whether the app is in the
        // foreground or background. Data messages are the type traditionally used with GCM. Notification messages are only received here in onMessageReceived when the
        // app is in the foreground. When the app is in the background an automatically generated notification is displayed. When the user taps on the notification they
        // are returned to the app. Messages containing both notification and data payloads are treated as notification messages. The Firebase console always sends
        // notification messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData() != null) {
            Log.d(TAG, "Notification Data Body: " + remoteMessage.toString() + ", Data: " + remoteMessage.getData().toString());

            callFriendsListApi();

            String body = remoteMessage.getData().get("body") != null ? remoteMessage.getData().get("body").toString().trim() : "";
            String title = remoteMessage.getData().get("title") != null ? remoteMessage.getData().get("title").toString().trim() : "";
            String sender_name = remoteMessage.getData().get("sender_name") != null ? remoteMessage.getData().get("sender_name").toString().trim() : "";
            String message = remoteMessage.getData().get("message") != null ? remoteMessage.getData().get("message").toString().trim() : "";
            String senderId = remoteMessage.getData().get("senderId") != null ? remoteMessage.getData().get("senderId").toString().trim() : "";
            String chatId = remoteMessage.getData().get("chatId") != null ? remoteMessage.getData().get("chatId").toString().trim() : "";
            String messageId = remoteMessage.getData().get("messageId") != null ? remoteMessage.getData().get("messageId").toString().trim() : "";
            String mediaUrl = remoteMessage.getData().get("mediaUrl") != null ? remoteMessage.getData().get("mediaUrl").toString().trim() : "";
            Log.i(TAG, "Body: " + body + ", Title: " + title);
//            sendNotification(text, text, type);

            // TODO Display Friend request notification here.
            if (body.trim().equalsIgnoreCase(getString(R.string.notification_title_new_friend_request))) {
                try {
                    JSONArray friend = remoteMessage.getData().get("friend") != null ? new JSONArray(remoteMessage.getData().get("friend").toString().trim())
                            : new JSONArray();

                    for (int i = 0; i < friend.length(); i++) {
                        JSONObject objFriend = friend.getJSONObject(i);
                        userProfileVo = new UserProfileAPI(MyFirebaseMessagingService.this, objFriend);

                        String notificationBody = userProfileVo.getFirst_name().trim() + " " + userProfileVo.getLast_name().trim() + " "
                                + getString(R.string.notification_body_new_friend_request);

                        if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true)) {
                            sendNotification(body, notificationBody);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            } else if (body.trim().equalsIgnoreCase(getString(R.string.notification_title_friend_request_accepted))) {
                try {
                    JSONArray friend = remoteMessage.getData().get("friend") != null ? new JSONArray(remoteMessage.getData().get("friend").toString().trim())
                            : new JSONArray();

                    for (int i = 0; i < friend.length(); i++) {
                        JSONObject objFriend = friend.getJSONObject(i);
                        userProfileVo = new UserProfileAPI(MyFirebaseMessagingService.this, objFriend);

                        String notificationBody = userProfileVo.getFirst_name().trim() + " " + userProfileVo.getLast_name().trim() + " "
                                + getString(R.string.notification_body_friend_request_accepted);

                        if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true)) {
                            sendNotification(body, notificationBody);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                }
            }
            /*else {
                // Chat messages or any other type of notifications here.
                userProfileVo = new UserProfileAPI();
                userProfileVo.setFirst_name(sender_name.trim());
                userProfileVo.setProfilepic1(mediaUrl.trim());
                userProfileVo.setId(senderId.trim());
                String notificationBody = sender_name + ": " + message;

                if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true)) {
                    sendNotification(body, notificationBody);
                }
            }*/
        } else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notification Message Body: " + remoteMessage.toString() + ", Data: " + remoteMessage.getNotification().getBody().toString());
            intent = new Intent(MyFirebaseMessagingService.this, WelcomeActivity.class);
           /* handleMessage(remoteMessage.getNotification().getTitle().trim(), remoteMessage.getNotification().getBody().trim(), "", "");*/
        }

        // Update user object and sObject in background here.

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notificationTitle Title to display in notification
     * @param notificationBody  Body message to display in notification
     */
    private void sendNotification(String notificationTitle, String notificationBody) {
        Bitmap bitmapUser = null;
        List<String> listTemp = new ArrayList<>(Arrays.asList(SharedPreferenceUtil.getString(Constants.NOTIFICATION_MESSAGES, "").trim().split(",")));
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < listTemp.size(); i++) {
            if (!listTemp.get(i).trim().isEmpty()) {
                list.add(listTemp.get(i).trim());
            }
        }

        if (!notificationBody.trim().isEmpty()) {
            list.add(notificationBody.trim());
        } else {
            list.add(notificationTitle.trim());
        }

        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, android.text.TextUtils.join(",", list));
        SharedPreferenceUtil.save();

        Intent intent = null;
        if (notificationTitle.trim().equalsIgnoreCase(getString(R.string.notification_title_new_friend_request))) {

            if (userProfileVo != null) {
//                otherUserID = getIntent().getStringExtra(Constants.INTENT_USER_ID);
                Log.d(TAG, "User id in intent: " + userProfileVo.getId().trim());
//                intent = new Intent(MyFirebaseMessagingService.this, SingleUserDetailActivity.class);
                intent = new Intent(MyFirebaseMessagingService.this, ChatDetailsActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, userProfileVo.getId().trim());
                intent.putExtra(Constants.IS_FROM_NOTIFICATION, true);
            } else {
                intent = new Intent(this, WelcomeActivity.class);
            }
        } else if (notificationTitle.trim().equalsIgnoreCase(getString(R.string.notification_title_friend_request_accepted))) {
            intent = new Intent(MyFirebaseMessagingService.this, SingleUserDetailActivity.class);
            if (userProfileVo != null) {
//                otherUserID = getIntent().getStringExtra(Constants.INTENT_USER_ID);
                Log.d(TAG, "User id in intent: " + userProfileVo.getId().trim());
                intent = new Intent(MyFirebaseMessagingService.this, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, userProfileVo.getId().trim());
                intent.putExtra(Constants.INTENT_USER_DETAILS, userProfileVo);
                intent.putExtra(Constants.IS_FROM_NOTIFICATION, true);
            } else {
                intent = new Intent(this, WelcomeActivity.class);
            }
        } else {
            intent = new Intent(this, WelcomeActivity.class);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setTicker(!notificationBody.trim().isEmpty() ? notificationBody.trim() : getString(R.string.you_have_a_new_notification))
                .setSmallIcon(R.mipmap.ic_notification_big) // Icon to display in notification
                .setWhen(System.currentTimeMillis())
                .setContentTitle(getString(R.string.app_name))
                .setContentText(!notificationBody.trim().isEmpty() ? notificationBody.trim() : getString(R.string.you_have_a_new_notification))
                .setAutoCancel(true)
                .setNumber(list.size())
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setColor(getResources().getColor(R.color.colorPrimaryDark));

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(notificationBuilder).setBigContentTitle(getString(R.string.app_name));
        if (list.size() == 1) {
            if (userProfileVo != null && !userProfileVo.getProfilepic1().trim().isEmpty()) {
                String strBitmapURL = userProfileVo.getProfilepic1().trim();
            /*if (!strBitmapURL.startsWith("http")) {
                strBitmapURL = getString(R.string.api_base_url_upload_images_retrofit) + strBitmapURL;
            }*/
                try {
                    bitmapUser = BitmapFactory.decodeStream((InputStream) new URL(strBitmapURL).getContent());
//                    notificationBuilder.setContentTitle(getString(R.string.you_have_a_new_notification));
                    notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary)).setLargeIcon(bitmapUser);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            notificationBuilder.setContentTitle(getString(R.string.you_have_a_new_notification));
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notification));
        } else {
            notificationBuilder.setContentTitle(getString(R.string.you_have_new_notifications));
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));
        }

        for (int i = 0; i < list.size(); i++) {
            inboxStyle.addLine(list.get(i));
        }

        Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
        notificationManager.notify(0, notification);

        sendLocalBroadcastManager(notificationTitle.trim());
    }

    private void sendLocalBroadcastManager(String title) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_NOTIFICATION_RECEIVED);
        i.putExtra("action", action);
        i.putExtra("message", title.trim());

        // Update current activity using Local Broadcast manager if needed.
        if (title.trim().equalsIgnoreCase(getString(R.string.notification_title_new_friend_request))) {
            if (userProfileVo != null) {
                action = 2;
                i.putExtra("action", String.valueOf(action));
                i.putExtra("user", userProfileVo);
            }
        } else if (title.trim().equalsIgnoreCase(getString(R.string.notification_title_friend_request_accepted))) {
            if (userProfileVo != null) {
                action = 1;
                i.putExtra("action", String.valueOf(action));
                i.putExtra("user", userProfileVo);
            }
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(i);
    }

    public void callFriendsListApi() {
        if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false)) {
//        showProgress(getString(R.string.loading));
            HashMap<String, String> params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

            new HttpRequestSingletonPost(getApplicationContext(), getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_FRIEND_LIST, MyFirebaseMessagingService.this);
        }
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
//                Log.i("Response", "" + response);
                Log.d(TAG, "RESPONSE FRIENDS LIST: " + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();
                /*stopProgress();*/
            }
        } else {
            Log.d(TAG, "Null response received for action: " + response);
        }
    }


    /*@Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }*/
}