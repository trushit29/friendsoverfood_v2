package com.friendsoverfood.android.UserSuggestions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.friendsoverfood.android.R;

/**
 * Created by ashish on 26/08/16.
 */
public class CustomSliderView extends BaseSliderView {
    public CustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.layout_custom_slider_layout, null);
        ImageView target = (ImageView) v.findViewById(R.id.daimajia_slider_image);
//        TextView description = (TextView) v.findViewById(R.id.description);
//        description.setText(getDescription());
        bindEventAndShow(v, target);
        return v;
    }
}