package com.friendsoverfood.android.SignIn;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.R;

/**
 * Created by Trushit on 08/03/17.
 */

public class ForgotPasswordActivity extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "FORGOT_PASSWORD_ACTIVITY";

    private TextInputLayout tilEmail;
    private AppCompatEditText edtEmail;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        setTitleSupportActionBar();
        setTitleSupportActionBar(Html.fromHtml("<font color='#C22945'>" + getString(R.string.forgot_password) + "</font>"));
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        /*setDrawerMenu();*/

        /*setDrawerMenu();
        lockDrawerMenu();*/

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_forgot_password_activity, getMiddleContent());

        btnSubmit = (Button) view.findViewById(R.id.buttonForgotPasswordActivitySubmit);
        tilEmail = (TextInputLayout) view.findViewById(R.id.textInputLayoutForgotPasswordActivityEmail);
        edtEmail = (AppCompatEditText) view.findViewById(R.id.editTextForgotPasswordActivityEmail);

//        setAllTypefaceDINMedium(btnRegister);
//        setAllTypefaceDINMedium(btnSignIn);
        initLayout();
        setListener();
        setAllTypefaceMontserratRegular(view);
    }

    private void initLayout() {
        edtEmail.setText("");
        edtEmail.requestFocus();

        if (tilEmail.isErrorEnabled() && tilEmail.getError() != null) {
            tilEmail.setError(null);
        }
    }

    private void setListener() {
        btnSubmit.setOnClickListener(this);
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtEmail.getText().toString().trim().length() > 0) {
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                }
            }
        });
    }

    private boolean validateSubmit() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if (edtEmail.getText().toString().trim().isEmpty()) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.email_is_required));
            return false;
        } else if (!isValidEmail(edtEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.please_enter_valid_email));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO Set Transparent toolbar color for background
        toolbarLayoutRoot.setBackgroundResource(R.color.transparent);

        // TODO Set Background to Coordinator layout of base activity root, so that transparent toolbar works
        coordinatorRoot.setBackground(getResources().getDrawable(R.drawable.ic_bg_app_white_small_icons));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.buttonForgotPasswordActivitySubmit:
                if (validateSubmit()) {
                    onBackPressed();
                }
                break;

            default:
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
