package com.friendsoverfood.android.util;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.ScrollView;

class MyCustomAnimation extends Animation {

	public final static int COLLAPSE = 1;
	private final static int EXPAND = 0;

	private View mView;
	private int mEndHeight;
	private int mType;
	private LinearLayout.LayoutParams mLayoutParams;

	public MyCustomAnimation(View view, int duration, int type) {

		setDuration(duration);
		mView = view;
		mEndHeight = mView.getHeight();
		mLayoutParams = ((LinearLayout.LayoutParams) view.getLayoutParams());
		mType = type;
		if (mType == EXPAND) {
			mLayoutParams.height = 0;
		} else {
			mLayoutParams.height = LayoutParams.WRAP_CONTENT;
		}
		view.setVisibility(View.VISIBLE);
	}

	public int getHeight() {
		return mView.getHeight();
	}

	public void setHeight(int height) {
		mEndHeight = height;
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {

		super.applyTransformation(interpolatedTime, t);
		if (interpolatedTime < 1.0f) {
			if (mType == EXPAND) {
				mLayoutParams.height = (int) (mEndHeight * interpolatedTime);
			} else {
				mLayoutParams.height = (int) (mEndHeight * (1 - interpolatedTime));
			}
			mView.requestLayout();
		} else {
			if (mType == EXPAND) {
				mLayoutParams.height = LayoutParams.WRAP_CONTENT;
				mView.requestLayout();
			} else {
				mView.setVisibility(View.GONE);
			}
		}
	}

	// public static void expand(final View v, final ImageView i) {
	// public static void expand(final View v,
	// final ParallaxScrollView scrollViewParent) {
	public static void expand(final View v, final ScrollView scrollViewParent) {
		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int targetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT
						: (int) (targetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (targetHeight / v.getContext().getResources()
				.getDisplayMetrics().density));
		v.startAnimation(a);

		a.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// i.setImageDrawable(v.getContext().getResources()
				// .getDrawable(R.drawable.icon_down_arrow_menu_red));
				scrollViewParent.smoothScrollBy(0, v.getMeasuredHeight());
			}
		});
	}

	// public static void collapse(final View v, final ImageView i) {
	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight
							- (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (initialHeight / v.getContext().getResources()
				.getDisplayMetrics().density));
		v.startAnimation(a);

		a.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// i.setImageDrawable(v.getContext().getResources()
				// .getDrawable(R.drawable.icon_right_arrow_menu_black));
			}
		});
	}
}