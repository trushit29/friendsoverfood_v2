package com.friendsoverfood.android.ChatNew;

import android.content.Context;
import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Trushit on 22/03/17.
 */

public class ChatUsersListAdapter extends RecyclerView.Adapter<ChatUsersListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ArrayList<ChatListVO> list;
    private Context context;
    private ChatDetailsActivity activity;
    private Intent intent;
    private final String TAG = "ADAPTER_CHAT_DETAILS_USERS";

    public ChatUsersListAdapter(Context context, ArrayList<ChatListVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (ChatDetailsActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_chat_users_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.txtUserName.setText(list.get(position).getFirstName().trim());

        if (list.get(position).getIsSelected().trim().equalsIgnoreCase("1")) {
//            activity.setTitleSupportActionBar(list.get(position).getFirstName());
            activity.textViewToolbarBaseActivityTitle.setText(list.get(position).getFirstName());

            if (list.get(position).getStatus().equalsIgnoreCase("1")) {
                activity.isAlreadyFriend = true;
            } else {
                activity.isAlreadyFriend = false;
            }

            if (list.get(position).getFriend2().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) && !activity.isAlreadyFriend) {
                activity.isReceiver = true;
                activity.linearLayoutAcceptReject.setVisibility(View.VISIBLE);
                activity.textacceptReject.setText(context.getResources().getString(R.string.accept_friend_request_to_start_conversation));
                activity.linearLayoutListResto.setVisibility(View.GONE);
                activity.relativeChatFooter.setVisibility(View.GONE);
                activity.selectedFriendId = list.get(position).getFriend1();
            } else {
                activity.isReceiver = false;
                activity.linearLayoutAcceptReject.setVisibility(View.GONE);
//                activity.linearLayoutListResto.setVisibility(View.VISIBLE);
                activity.relativeChatFooter.setVisibility(View.VISIBLE);
            }

            list.get(position).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));

            viewHolder.linearRoot.setSelected(true);
            setGreyScale(viewHolder.linearRoot, false);
            activity.setAllTypefaceMontserratRegular(viewHolder.txtUserName);
        } else {
            viewHolder.linearRoot.setSelected(false);
            setGreyScale(viewHolder.linearRoot, true);
            activity.setAllTypefaceMontserratLight(viewHolder.txtUserName);
        }

        if (list.get(position).getUnreadCount() > 0 && !list.get(position).getIsSelected().trim().equalsIgnoreCase("1")) {
            viewHolder.textViewChatHomeAdapterMessagesCount.setVisibility(View.VISIBLE);
            viewHolder.textViewChatHomeAdapterMessagesCount.setText(String.valueOf(list.get(position).getUnreadCount()));
        } else {
            viewHolder.textViewChatHomeAdapterMessagesCount.setVisibility(View.GONE);
        }

        final int pos = position;
        final ViewHolder holder = viewHolder;

        if (!list.get(position).getProfilePic().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getProfilePic().trim())
                    .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48)).centerCrop()
                    .placeholder(R.drawable.ic_user_profile_avatar).error(R.drawable.ic_user_profile_avatar)
                    .into(holder.imgUserProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getProfilePic().trim());
                        }
                    });
        } else {
            viewHolder.imgUserProfilePic.setImageResource(R.drawable.ic_user_profile_avatar);
        }

        /*if (list.get(position).getLastMsg().trim().isEmpty()) {*/
        if (holder.listenerStatus == null) {
            holder.listenerStatus = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, "Timestamp snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                    try {
                        StatusVO status = dataSnapshot.getValue(StatusVO.class);
                        if (status.getSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                            long lastSeenCurrentUser = Long.valueOf(status.getLastSeen());
                            Log.d(TAG, "Current user last seen: " + lastSeenCurrentUser);

                            list.get(pos).setTimestampLastSeen(lastSeenCurrentUser);
                            getLastMessageFromChat(list.get(pos).getMatchid().trim());
                            getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    Log.d(TAG, "Timestamp snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                    try {
                        StatusVO status = dataSnapshot.getValue(StatusVO.class);
                        if (status.getSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                            long lastSeenCurrentUser = Long.valueOf(status.getLastSeen());
                            Log.d(TAG, "Current user last seen: " + lastSeenCurrentUser);

                            list.get(pos).setTimestampLastSeen(lastSeenCurrentUser);
                            getLastMessageFromChat(list.get(pos).getMatchid().trim());
                            getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                        } else {
                            long lastSeenCurrentUser = list.get(pos).getTimestampLastSeen();
                            getLastMessageFromChat(list.get(pos).getMatchid().trim());
                            getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            holder.mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");
            holder.mDatabaseStatus.child(list.get(position).getMatchid().trim()).addChildEventListener(holder.listenerStatus);
        }
        /*}*/
//            .child(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))



      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/

        viewHolder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.get(pos).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
                list.get(pos).setUnreadCount(0);

                activity.chatSelected = list.get(pos);
                activity.updateChatList(activity.chatSelected.getMatchid());
                list.get(activity.posSelectedUsersList).setIsSelected("0");
                notifyItemChanged(activity.posSelectedUsersList);

                activity.posSelectedUsersList = pos;
                list.get(pos).setIsSelected("1");
                activity.name = list.get(pos).getFirstName();
                if (list.get(pos).getStatus().equalsIgnoreCase("1")) {
                    activity.isAlreadyFriend = true;
                } else {
                    activity.isAlreadyFriend = false;
                }

                if (list.get(pos).getFriend2().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) && !activity.isAlreadyFriend) {
                    activity.isReceiver = true;
                    activity.linearLayoutAcceptReject.setVisibility(View.VISIBLE);
                    activity.textacceptReject.setText(context.getResources().getString(R.string.accept_friend_request_to_start_conversation));
                    activity.linearLayoutListResto.setVisibility(View.GONE);
                    activity.relativeChatFooter.setVisibility(View.GONE);
                    activity.selectedFriendId = list.get(pos).getFriend1();
                } else {
                    activity.isReceiver = false;
                    activity.linearLayoutAcceptReject.setVisibility(View.GONE);
//                    activity.linearLayoutListResto.setVisibility(View.VISIBLE);
                    activity.relativeChatFooter.setVisibility(View.VISIBLE);
                }

                activity.textViewToolbarBaseActivityTitle.setText(activity.name);
                notifyItemChanged(pos);
                notifyDataSetChanged();

                // Reset chat list here.
                int sizeBefore = activity.listChats.size();
                activity.listChats.clear();
                if (activity.adapterChats != null) {
                    activity.adapterChats.notifyItemRangeRemoved(0, sizeBefore);
                    activity.adapterChats.notifyDataSetChanged();
                }
                activity.setAdapterChatsList();
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUserName, textViewChatHomeAdapterMessagesCount;
        public LinearLayout linearRoot;
        public CircularImageView imgUserProfilePic;
        public ChildEventListener listenerStatus;
        public DatabaseReference mDatabaseStatus;

        public ViewHolder(View view) {
            super(view);
            txtUserName = (TextView) view.findViewById(R.id.textViewChatUsersListAdapterUserName);
            textViewChatHomeAdapterMessagesCount = (TextView) view.findViewById(R.id.textViewChatHomeAdapterMessagesCount);
            imgUserProfilePic = (CircularImageView) view.findViewById(R.id.imageViewChatUsersListAdapterUserProfilePic);
            linearRoot = (LinearLayout) view.findViewById(R.id.linearLayoutChatUsersListAdapterRoot);
            activity.setAllTypefaceMontserratRegular(view);
            activity.setAllTypefaceMontserratLight(txtUserName);
        }
    }

    public void setGreyScale(View v, boolean greyscale) {
        if (greyscale) {
            // Create a paint object with 0 saturation (black and white)
            ColorMatrix cm = new ColorMatrix();
            cm.setSaturation(0);
            Paint greyscalePaint = new Paint();
            greyscalePaint.setColorFilter(new ColorMatrixColorFilter(cm));
            // Create a hardware layer with the greyscale paint
            v.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
        } else {
            // Remove the hardware layer
            v.setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }

    public void getLastMessageFromChat(final String matchId) {
        Query lastMessageQuery = activity.mDatabaseMessages.child(matchId.trim()).orderByChild("timeStamp").limitToLast(1);
//        .limitToLast(1)
//            Query lastMessageQuery = activity.mDatabaseMessages.child("a3832fdd931de9b778f0bcd2389bca81").limitToLast(1);
        lastMessageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Last message added is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            list.get(i).setLastMsg(msg.getMsg().trim());
//                            list.get(i).setTime(getDateCurrentTimeZone(msg.getTimeStamp()));
                            list.get(i).setTime(msg.getTimeStamp());
                            notifyItemChanged(i);
                            notifyDataSetChanged();
                        }
                    }

                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                        // Update adapter & list by applying sort here.
                        /*activity.sortListNotifyChanges();*/
                        int sizeBefore = list.size();
                        Collections.sort(list, new ComparatorLongDescending());
                        /*Collections.sort(list);*/
                        notifyItemRangeChanged(0, sizeBefore);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Last Message changed is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            list.get(i).setLastMsg(msg.getMsg().trim());
                            /*list.get(i).setTime(getDateCurrentTimeZone(msg.getTimeStamp()));*/
                            list.get(i).setTime(msg.getTimeStamp());
                            notifyItemChanged(i);
                            notifyDataSetChanged();
                        }
                    }

                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                        // Update adapter & list by applying sort here.
                        /*activity.sortListNotifyChanges();*/
                        int sizeBefore = list.size();
                       /* Collections.sort(list);*/
                        Collections.sort(list, new ComparatorLongDescending());
                        notifyItemRangeChanged(0, sizeBefore);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        });
    }

    public void getUnreadMessagesFromLastSeen(final long lastSeenTimeStamp, final String matchId) {
        Query lastMessageQuery = activity.mDatabaseMessages.child(matchId.trim()).orderByChild("timeStamp").startAt(lastSeenTimeStamp);
        lastMessageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Update count added is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    // increase count when message received for chat
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            if (!list.get(i).getUnreadMessageKeys().contains(dataSnapshot.getKey())) {
                                list.get(i).getUnreadMessageKeys().add(dataSnapshot.getKey());
                                if (!list.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                                    list.get(i).setUnreadCount(list.get(i).getUnreadCount() + 1);
                                } else {
                                    list.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
                                }
                                notifyItemChanged(i);
                                notifyDataSetChanged();

                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    // Update adapter & list by applying sort here.
                                    int sizeBefore = list.size();
                                    Collections.sort(list, new ComparatorLongDescending());
                                    notifyItemRangeChanged(0, sizeBefore);
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Update count changes is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    // increase count when message received for chat
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            if (!list.get(i).getUnreadMessageKeys().contains(dataSnapshot.getKey())) {
                                list.get(i).getUnreadMessageKeys().add(dataSnapshot.getKey());
                                if (!list.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                                    list.get(i).setUnreadCount(list.get(i).getUnreadCount() + 1);
                                } else {
                                    list.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
                                }
                                notifyItemChanged(i);
                                notifyDataSetChanged();

                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    // Update adapter & list by applying sort here.
                                    int sizeBefore = list.size();
                                    Collections.sort(list, new ComparatorLongDescending());
                                    notifyItemRangeChanged(0, sizeBefore);
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        });
    }

    public class ComparatorLongDescending implements Comparator<ChatListVO> {

        public int compare(ChatListVO first, ChatListVO second) {
//            Log.d("SORT_CHAT_LIST", "First: " + first.getFirstName() + " " + first.lastName + ", Timestamp: " + first.getTimestampLastSeen() + ", Second: " +
//                    second.getFirstName() + " " + second.lastName + ", Timestamp: " + second.getTimestampLastSeen());

//            long firstValue = first.getTimestampLastSeen();
            long firstValue = first.getTime();
            long secondValue = second.getTime();
            return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
        }
    }
}