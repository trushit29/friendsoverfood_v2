package com.friendsoverfood.android.ChatNew;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantDetails.RestaurantDetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 07/07/17.
 */

public class ChatRequestRestaurantsListAdapter extends RecyclerView.Adapter<ChatRequestRestaurantsListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MessageVO> list;
    private Intent intent;
    private LayoutInflater mInflater;
    private ChatDetailsActivity activity;

    public ChatRequestRestaurantsListAdapter(Context context, ArrayList<MessageVO> list) {
        this.context = context;
        this.list = list;
        mInflater = LayoutInflater.from(context);
        this.activity = (ChatDetailsActivity) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_chat_request_restaurant_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        convertView.setTag(holder);
        holder.txtName.setText(list.get(position).getRestoVo().getName().trim());
        holder.txtRating.setText(String.valueOf(list.get(position).getRestoVo().getRatings()).trim() + " " + context.getResources().getString(R.string.rating));
//        holder.txtPriceRange.setText("₹₹₹");

        if (!list.get(position).getRestoVo().getRatings().trim().isEmpty()) {
            holder.txtRating.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(Float.valueOf(list.get(position).getRestoVo().getRatings()));
        }else{
            holder.ratingBar.setVisibility(View.GONE);
            holder.txtRating.setVisibility(View.GONE);
        }

        final ViewHolder finalHolder = holder;
        final int pos = position;

        if (!list.get(position).getRestoVo().getPhotoReference().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getRestoVo().getPhotoReference().trim())
                    .resize(Constants.convertDpToPixels(108), Constants.convertDpToPixels(92))
                    .centerCrop().placeholder(R.drawable.ic_placeholder_restaurant).error(R.drawable.ic_placeholder_restaurant)
                    .into(holder.imgPic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getRestoVo().getPhotoReference().trim());
                        }
                    });
        } else {
            holder.imgPic.setImageResource(R.drawable.ic_placeholder_restaurant);
        }

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, RestaurantDetailsActivity.class);
                intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos).getRestoVo());
                intent.putExtra("isToShowInviteFriends", false);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txtRating;
        //        public SwitchCompat switchNotifications;
        public LinearLayout linearRoot;
        public ImageView imgPic;
        public RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);

            txtName = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantName);
            txtRating = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantRatings);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListAdapterRestaurantsRatings);
            imgPic = (ImageView) view.findViewById(R.id.imageViewRestaurantListAdapterRestaurantPicture);
            linearRoot = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListAdapterRootChat);
            activity.setAllTypefaceMontserratRegular(view);
        }
    }
}