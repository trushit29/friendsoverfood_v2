package com.friendsoverfood.android.ChatNew;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Trushit on 20/06/17.
 */

public class ChatListActivity extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private RecyclerView recyclerViewChatHome;
    private LinearLayoutManager layoutManager;
    private ChatListAdapter adapterChatHome;
    private ArrayList<ChatListVO> itemList = new ArrayList<>();
//    private ListView listViewChatHome;
//    private AdapterChatHomeListAdapter adapterChatList;

    private final String TAG = "CHAT_LIST_ACTIVITY";

    // Alarm receiver initiate object
    public AlarmManager alarms;

    // Firebase instances to load latest message from chat
    public DatabaseReference mDatabase;
    public DatabaseReference mDatabaseMessages;

    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;

    // Objects for Local Broadcast manager & Push notifications handling in the app.
    public boolean mIsReceiverRegistered = false, mIsReceiverRegisteredRequestLocal = false;
    public MyBroadcastReceiver mReceiver, mReceiverRequestLocal;
    public BroadcastReceiver mNotificationReceiver, mNotificationReceiverRequestLocal;

    public boolean isReloadChatListScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListener();
/*        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/
        setTitleSupportActionBar(getString(R.string.converations));
        callFriendsListApi();

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(2); // Set search icon selected
        setVisibilityActionBarBackButton(false);
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_chat_list_activity, getMiddleContent());
        recyclerViewChatHome = (RecyclerView) findViewById(R.id.recyclerViewChatListActivity);
//        imgViewSendRequest = (ImageView) findViewById(R.id.imgViewSendRequest);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerViewChatHome.setLayoutManager(layoutManager);
        recyclerViewChatHome.setHasFixedSize(false);

//        listViewChatHome = (ListView) view.findViewById(R.id.listViewChatHome);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid() + ", Name: " + user.getDisplayName());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuthListener == null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    private void setListener() {
//        imgViewSendRequest.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
//            case R.id.imgViewSendRequest:
//                ActionSheet.createBuilder(this, getSupportFragmentManager())
//                        .setCancelButtonTitle("Cancel")
//                        .setOtherButtonTitles("Now", "Later")
//                        .setCancelableOnTouchOutside(true)
//                        .setListener(this).show();
//                break;
            default:
                break;

        }
    }

    public void callFriendsListApi() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
            Log.i(TAG, "RESPONSE FRIEND LIST: " + response);
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
            SharedPreferenceUtil.save();
            /*stopProgress();*/

            int sizeBefore = itemList.size();
            itemList.clear();
            if (adapterChatHome != null) {
                adapterChatHome.notifyItemRangeRemoved(0, sizeBefore);
                adapterChatHome.notifyDataSetChanged();
            }
            processFriendsListFromLocal(response);

        }

    }

    public void processFriendsListFromLocal(String response) {
        JSONObject jsonObjectResponse = null;
        try {
            jsonObjectResponse = new JSONObject(response);
            JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
            boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                    ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
            String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                    ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
            if (status) {
                JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                        ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                int sizeBefore = itemList.size();
                JSONObject dataInner = null;
                for (int i = 0; i < data.length(); i++) {
                    ChatListVO item = new ChatListVO();
                    try {
                        dataInner = data.getJSONObject(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                            ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                    String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                            ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                    String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                            ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                    String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                            ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                    item.setMatchid(matchId);
                    item.setFriend1(friend1);
                    item.setFriend2(friend2);
                    item.setStatus(statusFriend);

                    JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                            ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                    for (int j = 0; j < details.length(); j++) {

                        JSONObject detailInner = details.getJSONObject(j);
                        String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                        String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                        String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                        String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                        String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                        item.setUserId(userId);
                        item.setFirstName(first_name);
                        item.setLastName(lastName);
                        item.setProfilePic(profilePic);
                        item.setGender(gender);

                    }
                    itemList.add(item);
                }

                stopProgress();
                if (adapterChatHome == null) {
                    adapterChatHome = new ChatListAdapter(context, itemList);
                    recyclerViewChatHome.setAdapter(adapterChatHome);
                } else {
                    adapterChatHome.notifyItemRangeInserted(sizeBefore, itemList.size());
                    adapterChatHome.notifyDataSetChanged();
                }

                    /*if (adapterChatList == null) {
                        adapterChatList = new AdapterChatHomeListAdapter(context, itemList);
                        listViewChatHome.setAdapter(adapterChatList);
                    } else {
                        adapterChatList.notifyDataSetChanged();
                    }*/
            } else {
                showSnackBarMessageOnly(errormessage);
                stopProgress();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            stopProgress();
            showSnackBarMessageOnly(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            stopProgress();
            showSnackBarMessageOnly(e.getMessage());
        }
    }

    /*public void sortListNotifyChanges() {
        int sizeBefore = itemList.size();
        Collections.sort(itemList, new ComparatorLongDescending());
        if (adapterChatHome != null) {
            adapterChatHome.notifyItemRangeChanged(0, sizeBefore);
            adapterChatHome.notifyDataSetChanged();
        } else {
            adapterChatHome = new ChatAdapter(context, itemList);
        }
    }

    public class ComparatorLongDescending implements Comparator<ChatListVO> {

        public int compare(ChatListVO first, ChatListVO second) {
            *//*if (!first.getTime().trim().isEmpty() && !second.getTime().trim().isEmpty()) {
                long firstValue = Long.parseLong(first.getTime());
                long secondValue = Long.parseLong(second.getTime());
                return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
            } else {
                return 0;
            }*//*
            Log.d("SORT_CHAT_LIST", "First: "+first.getFirstName()+" "+first.lastName+", Timestamp: "+first.getTimestampLastSeen()+", Second: "+
                    second.getFirstName()+" "+second.lastName+", Timestamp: "+second.getTimestampLastSeen());

            long firstValue = first.getTimestampLastSeen();
            long secondValue = second.getTimestampLastSeen();
            return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
            *//*return 0;*//*

        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ACTION_CODE_CHAT_USERS_LIST) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(Constants.INTENT_CHAT_LIST) && data.getSerializableExtra(Constants.INTENT_CHAT_LIST) != null) {
                    int sizeBefore = itemList != null ? itemList.size() : 0;
                    itemList.clear();
                    if (adapterChatHome != null) {
                        adapterChatHome.notifyItemRangeChanged(0, sizeBefore);
                        adapterChatHome.notifyDataSetChanged();
                    }

                    ArrayList<ChatListVO> listUsers = (ArrayList<ChatListVO>) data.getSerializableExtra(Constants.INTENT_CHAT_LIST);
                    itemList.addAll(listUsers);

                    if (adapterChatHome == null) {
                        adapterChatHome = new ChatListAdapter(context, itemList);
                        recyclerViewChatHome.setAdapter(adapterChatHome);
                    } else {
                        adapterChatHome.notifyItemRangeInserted(0, itemList.size());
                        adapterChatHome.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        intent = new Intent(mContext, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        supportFinishAfterTransition();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (isReloadChatListScreen) {
            isReloadChatListScreen = false;
            int sizeBefore = itemList.size();
            itemList.clear();
            if (adapterChatHome != null) {
                adapterChatHome.notifyItemRangeRemoved(0, sizeBefore);
                adapterChatHome.notifyDataSetChanged();
            }
            processFriendsListFromLocal(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, ""));
        }

        if (!mIsReceiverRegistered) {
            mNotificationReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    Log.d(TAG, "Notification register onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

                    if (intent.hasExtra("action") && !intent.getStringExtra("action").trim().isEmpty()) {
                        if (intent.getStringExtra("action").trim().equalsIgnoreCase("1")) { // Friend request accepted. Notify data set changed here.
                            if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                                UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action accepted. Friend ID: " + user.getId().trim());

                                int sizeBefore = itemList.size();
                                itemList.clear();
                                if (adapterChatHome != null) {
                                    adapterChatHome.notifyItemRangeRemoved(0, sizeBefore);
                                    adapterChatHome.notifyDataSetChanged();
                                    callFriendsListApi();
                                }
                            }
                        } else if (intent.getStringExtra("action").trim().equalsIgnoreCase("2")) { // Friend request received. Notify data set changed here.
                            if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                                UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                                int sizeBefore = itemList.size();
                                itemList.clear();
                                if (adapterChatHome != null) {
                                    adapterChatHome.notifyItemRangeRemoved(0, sizeBefore);
                                    adapterChatHome.notifyDataSetChanged();
                                }
                                callFriendsListApi();
                            }
                        }
                    }
                }
            };

            LocalBroadcastManager.getInstance(ChatListActivity.this)
                    .registerReceiver(mNotificationReceiver, new IntentFilter(Constants.FILTER_NOTIFICATION_RECEIVED));

            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("refreshUI"));
            mIsReceiverRegistered = true;
        }

        if (!mIsReceiverRegisteredRequestLocal) {
            mNotificationReceiverRequestLocal = new BroadcastReceiver() {
                @Override
                public void onReceive(final Context context, final Intent intent) {
                    Log.d(TAG, "Broadcast Receiver Local Request Action onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

                    if (intent.hasExtra("action") && !intent.getStringExtra("action").trim().isEmpty()) {
                        if (intent.getStringExtra("action").trim().equalsIgnoreCase("1")) { // Friend request accepted. Notify data set changed here.
                            if (intent.hasExtra("otherUserId") && intent.getStringExtra("otherUserId") != null) {
                                String otherUserId = intent.getStringExtra("otherUserId");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action accepted. Friend ID: " + otherUserId.trim());

                                for (int i = 0; i < itemList.size(); i++) {
                                    if (itemList.get(i).getFriend1().trim()
                                            .equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                        if (itemList.get(i).getFriend2().trim().equalsIgnoreCase(otherUserId.trim())) {
                                            Log.d(TAG, "Broadcast, friend id status to 1: " + itemList.get(i).getFriend2() + ", POS: " + i);
                                            itemList.get(i).setStatus("1");
                                            if (adapterChatHome != null) {
                                                adapterChatHome.notifyItemChanged(i);
                                                adapterChatHome.notifyDataSetChanged();
                                            }

                                            isReloadChatListScreen = true;
                                        }
                                    } else {
                                        if (itemList.get(i).getFriend1().trim().equalsIgnoreCase(otherUserId.trim())) {
                                            Log.d(TAG, "Broadcast, friend id status to 1: " + itemList.get(i).getFriend1() + ", POS: " + i);
                                            itemList.get(i).setStatus("1");
                                            if (adapterChatHome != null) {
                                                adapterChatHome.notifyItemChanged(i);
                                                adapterChatHome.notifyDataSetChanged();
                                            }

                                            isReloadChatListScreen = true;
                                        }
                                    }
                                }
                            /*itemList.clear();
                            showProgress(getString(R.string.loading));
                            processFriendsListFromLocal(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, ""));*/

                            }
                        } else if (intent.getStringExtra("action").trim().equalsIgnoreCase("2")) { // Friend request rejected. Notify data set changed
                            // here.
                            if (intent.hasExtra("otherUserId") && intent.getSerializableExtra("otherUserId") != null) {
                                String otherUserId = intent.getStringExtra("otherUserId");

                                // Iterate through user suggestion array and update user card if applicable.
                                Log.d(TAG, "Friend request action rejected. Friend ID: " + otherUserId.trim());

                            /*itemList.clear();
                            showProgress(getString(R.string.loading));
                            processFriendsListFromLocal(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, ""));*/
                                for (int i = 0; i < itemList.size(); i++) {
                                    if (itemList.get(i).getFriend1().trim()
                                            .equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                        if (itemList.get(i).getFriend2().trim().equalsIgnoreCase(otherUserId.trim())) {
                                            Log.d(TAG, "Broadcast, friend id to remove: " + itemList.get(i).getFriend2());
                                            itemList.remove(i);
                                            if (adapterChatHome != null) {
                                                adapterChatHome.notifyItemRemoved(i);
                                                adapterChatHome.notifyDataSetChanged();
                                            }

                                            isReloadChatListScreen = true;
                                        }
                                    } else {
                                        if (itemList.get(i).getFriend1().trim().equalsIgnoreCase(otherUserId.trim())) {
                                            Log.d(TAG, "Broadcast, friend id to remove: " + itemList.get(i).getFriend1());
                                            itemList.remove(i);
                                            if (adapterChatHome != null) {
                                                adapterChatHome.notifyItemRemoved(i);
                                                adapterChatHome.notifyDataSetChanged();
                                            }

                                            isReloadChatListScreen = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };


            LocalBroadcastManager.getInstance(ChatListActivity.this)
                    .registerReceiver(mNotificationReceiverRequestLocal, new IntentFilter(Constants.FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED));

            if (mReceiverRequestLocal == null)
                mReceiverRequestLocal = new MyBroadcastReceiver();
            registerReceiver(mReceiverRequestLocal, new IntentFilter("refreshList"));
            mIsReceiverRegisteredRequestLocal = true;
        }
    }

    @Override
    protected void onDestroy() {
        try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(ChatListActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }

            if (mNotificationReceiverRequestLocal != null)
                LocalBroadcastManager.getInstance(ChatListActivity.this).unregisterReceiver(mNotificationReceiverRequestLocal);

            if (mIsReceiverRegisteredRequestLocal) {
                unregisterReceiver(mReceiverRequestLocal);
                mReceiverRequestLocal = null;
                mIsReceiverRegisteredRequestLocal = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onDestroy();
        }
    }
}