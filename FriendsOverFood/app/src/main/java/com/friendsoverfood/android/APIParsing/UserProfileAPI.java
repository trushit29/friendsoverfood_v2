package com.friendsoverfood.android.APIParsing;

import android.content.Context;

import com.friendsoverfood.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Trushit on 04/05/17.
 * <p>
 * This class will be used for user profile related APIs as well as Sign In, Sign Up & Social Sign In APIs.
 */

public class UserProfileAPI implements Serializable {


    public String id = "", sessionid = "", email = "", password = "", gender = "", first_name = "", last_name = "", mobile = "", dob = "", occupation = "",
            relationship = "", about_me = "", ethnicity = "", location = "", location_string = "", account_type = "", access_token = "", social_id = "", showme = "",
            search_distance = "100", search_min_age = "16", search_max_age = "100", distance_unit = "mi", profilepic1 = "", profilepic2 = "",
            profilepic3 = "",
            profilepic4 = "", profilepic5 = "", devicetoken = "", testbuds = "", friendstatus = "", reference = "", refercode = "", profilepic6 = "",
            profilepic7 = "", profilepic8 = "", profilepic9 = "", education = "";

    public long createdat = 0, updatedat = 0;

    public boolean is_receive_messages_notifications = false, is_receive_invitation_notifications = false, isregistered = true,
            is_show_location = true;

    public UserProfileAPI() {
    }

    public UserProfileAPI(Context context, JSONObject data) {
        try {
            id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
            sessionid = !data.isNull(context.getResources().getString(R.string.api_response_param_key_sessionid))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_sessionid)).trim() : "";
            email = !data.isNull(context.getResources().getString(R.string.api_response_param_key_email))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_email)).trim() : "";
            password = !data.isNull(context.getResources().getString(R.string.api_response_param_key_password))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_password)).trim() : "";
            gender = !data.isNull(context.getResources().getString(R.string.api_response_param_key_gender))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_gender)).trim() : "";
            first_name = !data.isNull(context.getResources().getString(R.string.api_response_param_key_first_name))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_first_name)).trim() : "";
            last_name = !data.isNull(context.getResources().getString(R.string.api_response_param_key_last_name))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_last_name)).trim() : "";
            mobile = !data.isNull(context.getResources().getString(R.string.api_response_param_key_mobile))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_mobile)).trim() : "";
            dob = !data.isNull(context.getResources().getString(R.string.api_response_param_key_dob))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_dob)).trim() : "";
            occupation = !data.isNull(context.getResources().getString(R.string.api_response_param_key_occupation))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_occupation)).trim() : "";
            education = !data.isNull(context.getResources().getString(R.string.api_response_param_key_education))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_education)).trim() : "";
            relationship = !data.isNull(context.getResources().getString(R.string.api_response_param_key_relationship))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_relationship)).trim() : "";
            about_me = !data.isNull(context.getResources().getString(R.string.api_response_param_key_about_me))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_about_me)).trim() : "";
            ethnicity = !data.isNull(context.getResources().getString(R.string.api_response_param_key_ethnicity))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_ethnicity)).trim() : "";
            location = !data.isNull(context.getResources().getString(R.string.api_response_param_key_location))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_location)).trim() : "";
            location_string = !data.isNull(context.getResources().getString(R.string.api_response_param_key_location_string))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_location_string)).trim() : "";
            account_type = !data.isNull(context.getResources().getString(R.string.api_response_param_key_account_type))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_account_type)).trim() : "";
            access_token = !data.isNull(context.getResources().getString(R.string.api_response_param_key_access_token))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_access_token)).trim() : "";
            social_id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_social_id))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_social_id)).trim() : "";
            showme = !data.isNull(context.getResources().getString(R.string.api_response_param_key_showme))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_showme)).trim() : "";
            search_distance = !data.isNull(context.getResources().getString(R.string.api_response_param_key_search_distance))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_search_distance)).trim() : "";

            if (!data.isNull(context.getResources().getString(R.string.api_response_param_key_search_min_age))) {
                if (!data.getString(context.getResources().getString(R.string.api_response_param_key_search_min_age)).trim().isEmpty()) {
                    try {
                        String minAge = data.getString(context.getResources().getString(R.string.api_response_param_key_search_min_age)).trim();
                        if (Integer.parseInt(minAge.trim()) < 16) {
                            minAge = "16";
                        } else if (Integer.parseInt(minAge.trim()) > 100) {
                            minAge = "100";
                        }
                        search_min_age = minAge;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    search_min_age = "";
                }
            } else {
                search_min_age = "";
            }
            /*search_min_age = !data.isNull(context.getResources().getString(R.string.api_response_param_key_search_min_age))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_search_min_age)).trim()
                    : "";*/

            if (!data.isNull(context.getResources().getString(R.string.api_response_param_key_search_max_age))) {
                if (!data.getString(context.getResources().getString(R.string.api_response_param_key_search_max_age)).trim().isEmpty()) {
                    try {
                        String minAge = data.getString(context.getResources().getString(R.string.api_response_param_key_search_max_age)).trim();
                        if (Integer.parseInt(minAge.trim()) < 16) {
                            minAge = "16";
                        } else if (Integer.parseInt(minAge.trim()) > 100) {
                            minAge = "100";
                        }
                        search_max_age = minAge;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    search_max_age = "";
                }
            } else {
                search_max_age = "";
            }
            /*search_max_age = !data.isNull(context.getResources().getString(R.string.api_response_param_key_search_max_age))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_search_max_age)).trim() : "";*/
            is_receive_messages_notifications = !data.isNull(context.getResources().getString(R.string.api_response_param_key_is_receive_messages_notifications))
                    && data.getString(context.getResources().getString(R.string.api_response_param_key_is_receive_messages_notifications)).trim().equalsIgnoreCase("1")
                    ? true : false;
            is_receive_invitation_notifications = !data.isNull(context.getResources().getString(R.string.api_response_param_key_is_receive_invitation_notifications))
                    && data.getString(context.getResources().getString(R.string.api_response_param_key_is_receive_invitation_notifications)).trim().equalsIgnoreCase("1")
                    ? true : false;
            is_show_location = !data.isNull(context.getResources().getString(R.string.api_response_param_key_is_show_location))
                    && data.getString(context.getResources().getString(R.string.api_response_param_key_is_show_location)).trim().equalsIgnoreCase("1")
                    ? true : false;
            distance_unit = !data.isNull(context.getResources().getString(R.string.api_response_param_key_distance_unit))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_distance_unit)).trim() : "";
            profilepic1 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
            profilepic2 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic2))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic2)).trim() : "";
            profilepic3 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic3))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic3)).trim() : "";
            profilepic4 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic4))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic4)).trim() : "";
            profilepic5 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic5))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic5)).trim() : "";
            profilepic6 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic6))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic6)).trim() : "";
            profilepic7 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic7))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic7)).trim() : "";
            profilepic8 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic8))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic8)).trim() : "";
            profilepic9 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic9))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic9)).trim() : "";
            refercode = !data.isNull(context.getResources().getString(R.string.api_response_param_key_refercode))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_refercode)).trim() : "";
            reference = !data.isNull(context.getResources().getString(R.string.api_response_param_key_reference))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_reference)).trim() : "";
            devicetoken = !data.isNull(context.getResources().getString(R.string.api_response_param_key_devicetoken))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_devicetoken)).trim() : "";
            testbuds = !data.isNull(context.getResources().getString(R.string.api_response_param_key_testbuds))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_testbuds)).trim() : "";
            createdat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_createdat))
                    ? Long.parseLong(data.getString(context.getResources().getString(R.string.api_response_param_key_createdat)).trim()) : 0;
            updatedat = !data.isNull(context.getResources().getString(R.string.api_response_param_key_updatedat))
                    ? Long.parseLong(data.getString(context.getResources().getString(R.string.api_response_param_key_updatedat)).trim()) : 0;
            isregistered = !data.isNull(context.getResources().getString(R.string.api_response_param_key_isregistered))
                    ? data.getBoolean(context.getResources().getString(R.string.api_response_param_key_isregistered)) : false;
            friendstatus = !data.isNull(context.getResources().getString(R.string.api_response_param_key_friendstatus))
                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_friendstatus)).trim() : "";
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getShowme() {
        return showme;
    }

    public void setShowme(String showme) {
        this.showme = showme;
    }

    public String getSearch_distance() {
        return search_distance;
    }

    public void setSearch_distance(String search_distance) {
        this.search_distance = search_distance;
    }

    public String getSearch_min_age() {
        return search_min_age;
    }

    public void setSearch_min_age(String search_min_age) {
        this.search_min_age = search_min_age;
    }

    public String getSearch_max_age() {
        return search_max_age;
    }

    public void setSearch_max_age(String search_max_age) {
        this.search_max_age = search_max_age;
    }

    public String getDistance_unit() {
        return distance_unit;
    }

    public void setDistance_unit(String distance_unit) {
        this.distance_unit = distance_unit;
    }

    public String getProfilepic1() {
        return profilepic1;
    }

    public void setProfilepic1(String profilepic1) {
        this.profilepic1 = profilepic1;
    }

    public String getProfilepic2() {
        return profilepic2;
    }

    public void setProfilepic2(String profilepic2) {
        this.profilepic2 = profilepic2;
    }

    public String getProfilepic3() {
        return profilepic3;
    }

    public void setProfilepic3(String profilepic3) {
        this.profilepic3 = profilepic3;
    }

    public String getProfilepic4() {
        return profilepic4;
    }

    public void setProfilepic4(String profilepic4) {
        this.profilepic4 = profilepic4;
    }

    public String getProfilepic5() {
        return profilepic5;
    }

    public void setProfilepic5(String profilepic5) {
        this.profilepic5 = profilepic5;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRefercode() {
        return refercode;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public long getCreatedat() {
        return createdat;
    }

    public void setCreatedat(long createdat) {
        this.createdat = createdat;
    }

    public long getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(long updatedat) {
        this.updatedat = updatedat;
    }

    public boolean is_receive_messages_notifications() {
        return is_receive_messages_notifications;
    }

    public void setIs_receive_messages_notifications(boolean is_receive_messages_notifications) {
        this.is_receive_messages_notifications = is_receive_messages_notifications;
    }

    public boolean is_receive_invitation_notifications() {
        return is_receive_invitation_notifications;
    }

    public String getFriendstatus() {
        return friendstatus;
    }

    public void setFriendstatus(String friendstatus) {
        this.friendstatus = friendstatus;
    }

    public void setIs_receive_invitation_notifications(boolean is_receive_invitation_notifications) {
        this.is_receive_invitation_notifications = is_receive_invitation_notifications;
    }

    public boolean isregistered() {
        return isregistered;
    }

    public void setIsregistered(boolean isregistered) {
        this.isregistered = isregistered;
    }

    public String getTestbuds() {
        return testbuds;
    }

    public void setTestbuds(String testbuds) {
        this.testbuds = testbuds;
    }

    public String getLocation_string() {
        return location_string;
    }

    public void setLocation_string(String location_string) {
        this.location_string = location_string;
    }


    public String getProfilepic6() {
        return profilepic6;
    }

    public void setProfilepic6(String profilepic6) {
        this.profilepic6 = profilepic6;
    }

    public String getProfilepic7() {
        return profilepic7;
    }

    public void setProfilepic7(String profilepic7) {
        this.profilepic7 = profilepic7;
    }

    public String getProfilepic8() {
        return profilepic8;
    }

    public void setProfilepic8(String profilepic8) {
        this.profilepic8 = profilepic8;
    }

    public String getProfilepic9() {
        return profilepic9;
    }

    public void setProfilepic9(String profilepic9) {
        this.profilepic9 = profilepic9;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public boolean is_show_location() {
        return is_show_location;
    }

    public void setIs_show_location(boolean is_show_location) {
        this.is_show_location = is_show_location;
    }
}
