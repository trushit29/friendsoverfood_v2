package com.friendsoverfood.android.Http;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.friendsoverfood.android.ApplicationClass;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Trushit on 24/11/15.
 */
@SuppressWarnings("ALL")
public class HttpRequestSingletonPut {
    private static Context mContext;

    private String urlstring;
    private int action;
    private HttpCallback cb;
    private HashMap<String, String> params = null;

    public HttpRequestSingletonPut(Context context, String url, HashMap<String, String> params, int action, HttpCallback cb) {
        this.mContext = context;

        this.urlstring = url;
        this.cb = cb;
        this.action = action;
        this.params = params;

        StringBuilder builder = new StringBuilder(urlstring);

       /* if (params != null) {
            builder.append("&");
            Set<String> set = params.keySet();

            for (Iterator<String> iterator = set.iterator(); iterator.hasNext(); ) {
                String paramName = iterator.next();
                if (params.get(paramName) != null && paramName != null) {
                    builder.append(paramName.trim()).append("=").append(URLEncoder.encode(params.get(paramName).trim())).append("&");
                }
            }
        }*/

        // Formulate the request and handle the response.
        StringRequest strRequest = new StringRequest(Request.Method.POST, builder.toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                HttpRequestSingletonPut.this.cb.onResponse(response, HttpRequestSingletonPut.this.action);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Handle error
                HttpRequestSingletonPut.this.cb.onResponse(null, HttpRequestSingletonPut.this.action);
                Toast.makeText(HttpRequestSingletonPut.this.mContext, VolleyErrorHelper.getMessage(error, HttpRequestSingletonPut.this.mContext), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params;
                if (HttpRequestSingletonPut.this.params != null) {
                    params = HttpRequestSingletonPut.this.params;
                } else {
                    params = new HashMap<>();
                }

                Set<String> set = params.keySet();
                for (Iterator<String> iterator = set.iterator(); iterator.hasNext(); ) {
                    String paramName = iterator.next();
                    if (params.get(paramName) != null && paramName != null) {
//                        builder.append(paramName.trim()).append("=").append(URLEncoder.encode(params.get(paramName).trim())).append("&");
                        Log.d("" + paramName.trim(), "" + params.get(paramName).trim());
                    }
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false)
                        && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim().isEmpty()) {
                    Log.d("REQUEST_HEADER", "SESSION ID: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put(Constants.API_REQUEST_HEADER_KEY_COOKIE, "sid=" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
//                    params.put("Content-Type", "application/json");
                }
                params.put(Constants.API_REQUEST_HEADER_KEY_RESPONSETYPE, Constants.API_REQUEST_HEADER_VALUE_RESPONSETYPE);
                return params;
            }
        };

        // Add timeout policy to request
        strRequest.setRetryPolicy(new DefaultRetryPolicy(120 * 1000, 1, 1.0f));

        // Print URL string in log trace
        Log.i("REQUEST_URL_POST", "" + strRequest.getUrl());

        // Add the request to the RequestQueue.
       /* mRequestQueue.add(strRequest);*/
        /*RequestSingletonInstance.getInstance(mContext.getApplicationContext()).addToRequestQueue(strRequest);*/
        // add the request object to the queue to be executed
        ApplicationClass.getInstance().addToRequestQueue(strRequest);
    }
}