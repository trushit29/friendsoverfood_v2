package com.friendsoverfood.android.Http;


/**
 * HttpCallBack Interface for Api
 */
@SuppressWarnings("ALL")
public interface HttpCallback {
   
	/**
	 * Callback method for http response. called when an http response is received.
	 * @param response
	 * @param action
	 */
    public void onResponse(String response, int action);
    
}