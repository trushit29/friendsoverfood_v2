package com.friendsoverfood.android.FriendRequest;

import java.io.Serializable;

/**
 * Created by ashish on 24/04/17.
 */

public class RequestVO implements Serializable {
   public String first_name = "", last_name = "", id = "", gender = "", social_id = "", profilepic1 = "", testbuds = "", createdat = "", updatedat = "", dob = "",
           location_string = "", location = "", status = "";

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getProfilepic1() {
        return profilepic1;
    }

    public void setProfilepic1(String profilepic1) {
        this.profilepic1 = profilepic1;
    }

    public String getTestbuds() {
        return testbuds;
    }

    public void setTestbuds(String testbuds) {
        this.testbuds = testbuds;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLocation_string() {
        return location_string;
    }

    public void setLocation_string(String location_string) {
        this.location_string = location_string;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
