package com.friendsoverfood.android.Fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Trushit on 18/05/16.
 */
public class AlarmService extends Service implements HttpCallback {

    public final String TAG = "BACKGROUND_ALARM_SERVICE";

    public BroadcastReceiver mRegistrationBroadcastReceiver;
    public boolean isReceiverRegistered = false;
    // Firebase instances to load latest message from chat
    public DatabaseReference mDatabase;
    public DatabaseReference mDatabaseMessages;
    public DatabaseReference mDatabaseMessagesMatchid;
    public FirebaseAuth mAuth;
    public ArrayList<ChatListVO> matchList = new ArrayList<>();
    public FirebaseAuth.AuthStateListener mAuthListener;
    public Target loadtarget;
    public final Bitmap bitmapImage = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service onCreate() called.");

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d(TAG, "Service onStartCommand() called. Connecting to GCM for push notifications.");

        if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false)) {
            // Do your work here for Logged In user
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");

            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid() + ", Name: " + user.getDisplayName());
                    } else {
                        // User is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                    }
                }
            };

            // Fetch all the match details from Shared Preferences here.
            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                        ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                JSONObject settings = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_settings))
                        ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    matchList = new ArrayList<>();
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        ChatListVO item = new ChatListVO();
                        try {
                            dataInner = data.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                        String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                        String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                        String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                        item.setMatchid(matchId);
                        item.setFriend1(friend1);
                        item.setFriend2(friend2);

                        JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                        for (int j = 0; j < details.length(); j++) {

                            JSONObject detailInner = details.getJSONObject(j);
                            String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                            String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                            String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                            String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                            item.setUserId(userId);
                            item.setFirstName(first_name);
                            item.setLastName(lastName);
                            item.setProfilePic(profilePic);
                            item.setGender(gender);
                            item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                        }
                        matchList.add(item);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                for (int i = 0; i < matchList.size(); i++) {

                    final int pos = i;

//                    .limitToLast(1)
                    // Convert current time to UTC or GMT
                    Calendar c = Calendar.getInstance();

                    /*TimeZone z = c.getTimeZone();
                    int offset = z.getRawOffset();
                    if (z.inDaylightTime(new Date())) {
                        offset = offset + z.getDSTSavings();
                    }
                    int offsetHrs = offset / 1000 / 60 / 60;
                    int offsetMins = offset / 1000 / 60 % 60;

                    System.out.println("offset: " + offsetHrs);
                    System.out.println("offset: " + offsetMins);

                    c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
                    c.add(Calendar.MINUTE, (-offsetMins));*/

                    // Use current UTC timestamp to query latest messages from firebase
                    mDatabaseMessagesMatchid = mDatabaseMessages.child(matchList.get(i).getMatchid());
                    mDatabaseMessagesMatchid.keepSynced(true);
                    Query latestMessagesQuery = mDatabaseMessages.child(matchList.get(i).getMatchid().trim()).orderByChild("timeStamp")
                            .startAt(SharedPreferenceUtil.getLong(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED,
                                    Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true)));
                    latestMessagesQuery.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            TimeZone timeZone = TimeZone.getTimeZone("UTC");
                            Calendar calendar = Calendar.getInstance(timeZone);
                            Log.d(TAG, "Latest Message is: " + dataSnapshot.toString() + ", Current timestamp: " + calendar.getTimeInMillis());
                            try {
                                MessageVO msg = dataSnapshot.getValue(MessageVO.class);
                                msg.setKey(dataSnapshot.getKey().trim());
                                Calendar c = Calendar.getInstance();
                                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
                                if (msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) &&
                                        msg.getDelivered().toString().equalsIgnoreCase("") && msg.getDelivered().toString().isEmpty()) {
                                    if (msg.getDelivered().equalsIgnoreCase(""))
                                        mDatabaseMessages.child(matchList.get(pos).getMatchid()).child
                                                (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                                }

                                // Convert current time to UTC or GMT
                                /*TimeZone z = c.getTimeZone();
                                int offset = z.getRawOffset();
                                if (z.inDaylightTime(new Date())) {
                                    offset = offset + z.getDSTSavings();
                                }
                                int offsetHrs = offset / 1000 / 60 / 60;
                                int offsetMins = offset / 1000 / 60 % 60;

//                                System.out.println("offset: " + offsetHrs);
//                                System.out.println("offset: " + offsetMins);
                                Log.d(TAG, "Datetime: " + c.getTime() + ", Offset Hours: " + offsetHrs + ", Mins: " + offsetMins);

                                c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
                                c.add(Calendar.MINUTE, (-offsetMins));
                                Log.d(TAG, "Datetime after offset: " + c.getTime());*/

                                // Current timestamp compare
                                Log.d(TAG, "Message timestamp: " + msg.getTimeStamp()
                                        + ", Current timestamp: " + Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true) + ", Saved Timestamp: "
                                        + SharedPreferenceUtil.getLong(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, 0) + ", Key: " + msg.getKey());
                                if (msg.getTimeStamp() > SharedPreferenceUtil.getLong(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, 0)) {
                                    SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, msg.getTimeStamp());
                                    SharedPreferenceUtil.save();

                                    // Send notification for message received.

                                    final ChatListVO OtherUsers = matchList.get(pos);

//                                    final String message = OtherUsers.getFirstName() + " " + OtherUsers.getLastName() + ": " + msg.getMsg().trim();
                                    final String message = OtherUsers.getFirstName() + ": " + msg.getMsg().trim();

                                    // Send notification here.
                                    if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim().equalsIgnoreCase(msg.getRecieverId().trim())) {

                                        if (Constants.CheckOtherUSerOnline.equalsIgnoreCase(msg.getSenderId())) {
                                            MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.audiofile);
                                            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                            mPlayer.start();
                                        } else {
                                            if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)) {
                                                sendNotification(message, message, "message", OtherUsers);
                                            }
                                        }

                                        if (msg.getDelivered().equalsIgnoreCase(""))
                                            mDatabaseMessages.child(matchList.get(pos).getMatchid().trim()).child(msg.getKey()).child("delivered")
                                                    .setValue(String.valueOf(timestampToSave));
                                    }
                                }
//                                else
                                // Send notification here.
//                                    if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim().equalsIgnoreCase(msg.getRecieverId().trim())) {
//
//                                        ChatListVO OtherUsers = matchList.get(pos);
//
//                                        String message = OtherUsers.getFirstName() + " " + OtherUsers.getLastName() + ": " + msg.getMsg().trim();
//                                        if( Constants.CheckOtherUSerOnline.equalsIgnoreCase(msg.getSenderId()))
//                                        {
//                                            MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.audiofile);
//                                            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                                            mPlayer.start();
//                                        }
//                                        else
//                                            sendNotification(message, getString(R.string.you_have_a_new_message), "message",OtherUsers);
//                                        mDatabaseMessages.child(matchList.get(pos).getMatchid().trim()).child(msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
//
//                                    }
                                /*if(msg.getTimeStamp() >= Constants)*/

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            /*Log.d(TAG, "Latest Message changed at pos " + pos + " is: " + dataSnapshot.toString());
                            try {
                                MessageVO msg = dataSnapshot.getValue(MessageVO.class);
                                list.get(pos).setLastMsg(msg.getMsg().trim());
                                holder.txtLastMessage.setText(msg.getMsg().trim());
                                holder.txtTime.setText(new SimpleDateFormat("hh:mm aa").format(
                                        new Date(Constants.convertTimestampTo10DigitsOnly(Long.parseLong(msg.getTimeStamp()), false))));

                                // Update adapter & list by applying sort here.
                                activity.sortListNotifyChanges();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            databaseError.toException().printStackTrace();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Registering BroadcastReceiver
        registerReceiver();

//        return super.onStartCommand(intent, flags, startId);
        // Return START_STICKY to run service in background all the time.
        return START_STICKY;
    }
    // Registering BroadcastReceiver

//        return super.onStartCommand(intent, flags, startId);
    // Return START_STICKY to run service in background all the time.

    @Override
    public boolean onUnbind(Intent intent) {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        return super.onUnbind(intent);
    }

    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        return super.bindService(service, conn, flags);
    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
//            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Constants.REGISTRATION_COMPLETE));
//            isReceiverRegistered = true;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
//    private boolean checkPlayServices() {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
//        if (resultCode != ConnectionResult.SUCCESS) {
//            if (apiAvailability.isUserResolvableError(resultCode)) {
//                /*apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
//                        .show();*/
//            } else {
//                Log.i("BASE_ACTIVITY", "This device is not supported.");
//            }
//            return false;
//        }
//        return true;
//    }
    @Override
    public void onResponse(String response, int action) {

    }

    private void sendNotification(String text, String messageBody, String type, ChatListVO OtherUser) {
        List<String> listTemp = new ArrayList<>(Arrays.asList(SharedPreferenceUtil.getString(Constants.NOTIFICATION_MESSAGES, "").trim().split(",")));
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < listTemp.size(); i++) {
            if (!listTemp.get(i).trim().isEmpty()) {
                list.add(listTemp.get(i).trim());
            }
        }

        if (!text.trim().isEmpty()) {
            list.add(text.trim());
        } else {
            list.add(text.trim());
        }

        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, android.text.TextUtils.join(",", list));
        SharedPreferenceUtil.save();

//            Intent intent = new Intent(this, ListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intent = new Intent(AlarmService.this, ChatDetailsActivity.class);
        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
        intent.putExtra("isFromNotification", true);
//        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser); 
//        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList); 
//        intent.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());

        PendingIntent contentIntent = null;
        contentIntent = PendingIntent.getActivity(getApplicationContext(), (int) (Math.random() * 100), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setTicker(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setSmallIcon(R.mipmap.ic_notification_big)
                .setWhen(System.currentTimeMillis())
//                .setContentTitle(OtherUser.getFirstName() + " " + OtherUser.getLastName())
                .setContentTitle(OtherUser.getFirstName())
                .setContentText(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setAutoCancel(true)
                .setNumber(list.size())
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(contentIntent)
                .setColor(getResources().getColor(R.color.colorPrimaryDark));

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(notificationBuilder).setBigContentTitle(getString(R.string.app_name));
        if (list.size() == 1) {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notification));
            pictureStyleNotification(inboxStyle, text, type, notificationBuilder, OtherUser);

        } else {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

            Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notification.defaults |= Notification.DEFAULT_LIGHTS;
            notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
            notificationManager.notify(0, notification);

            // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
            sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/
        }

        for (int i = 0; i < list.size(); i++) {
            inboxStyle.addLine(list.get(i));
        }
        inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

        Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
        notificationManager.notify(0, notification);

        // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
        sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

    }

    private void sendLocalBroadcastManager(String body, String type) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_NOTIFICATION_RECEIVED);
        i.putExtra("type", type);
        i.putExtra("message", body.trim());
      /*  if (action == ACTION_TABLE_INVITE || action == ACTION_ACCEPTED_INVITE || action == ACTION_DINING_STARTED) {
            i.putExtra("restaurant", restaurantDetails);
            i.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());
        }*/
        LocalBroadcastManager.getInstance(AlarmService.this).sendBroadcast(i);
    }

    public void pictureStyleNotification(final NotificationCompat.InboxStyle inboxStyle, final String text, final String type,
                                         final NotificationCompat.Builder notificationBuilder, final ChatListVO OtherUser) {
        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                notificationBuilder.setLargeIcon(bitmap);

                Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.defaults |= Notification.DEFAULT_LIGHTS;
                notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
                notificationManager.notify(0, notification);

                // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
                sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };
        Picasso.with(getApplicationContext())
                .load(OtherUser.getProfilePic())
                .into(loadtarget);
    }
}
