package com.friendsoverfood.android.ChatNew;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.FacebookPhotos.FacebookPhotoVO;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.ListRestaurantForMessage.RestaurantListAdapterMessage;
import com.friendsoverfood.android.ListRestaurantForMessage.RestaurantListForSendRequestActivity;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Trushit on 22/03/17.
 */

public class ChatDetailsActivity extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "CHAT_DETAILS_ACTIVITY";

    public LinearLayout linearChatFriends, linearChatRequest, linearChatRequestAcceptReject;
    public TextView txtChatRequestMessage, txtChatRequestAcceptRejectPlaceholder, txtChatRequestAccept, txtChatRequestReject;
    public RecyclerView rvUsers, rvChats, recyclerViewListResto, rvChatRequestRestaurants;
    public LinearLayoutManager llmRecyclerViewUsers, llmRecyclerViewChat, llmRecyclerViewResto, llmRecyclerViewChatRequestRestaurant;
    public LinearLayout linearLayoutListResto;
    public ChatUsersListAdapter adapter;
    public ChatAdapter adapterChats;
    public RestaurantListAdapterMessage adapterResto;
    public ArrayList<ChatListVO> listUsers = new ArrayList<>();
    public ChatListVO chatSelected = new ChatListVO();
    private ChildEventListener listenerChat;
    private ChildEventListener listenerStatus;
    private DatabaseReference mDatabase;
    public DatabaseReference mDatabaseMessages;
    public DatabaseReference mDatabaseStatus;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public ArrayList<MessageVO> listChats = new ArrayList<>();
    public int posToScrollUsersList = 0, posSelectedUsersList = 0, posToScrollChatsList = 0;

    public boolean isToExpandChatList = true;
    public ImageView imgUpArrow, imgUploadAttachment, imageViewRestoList, imgrejectChat, imgacceptChat, imgSend;
    public TextView txtSend, txtViewShownIntrest, textacceptReject;
    public LinearLayout linearLayoutAcceptReject;
    public EditText edtMessage;
    public String name = "";
    public RelativeLayout relativeShownIntrest, relativeChatFooter;
    public TextView textViewToolbarBaseLastseen, textViewToolbarBaseActivityTitle;
    public CircularImageView imageViewToolbarChatActivityUserProfilePic;
    public View viewCustomActionBar;
    public String selectedFriendId = "";
    public boolean isTextMsgReached = false;
    public boolean isRestoLimitReached = false;
    public boolean isFriend = false;
    public boolean isReceiver = false;
    public boolean isAlreadyFriend = false;
    private int RestoCount = 0;

    private boolean isEndOfResult = false;
    private String searchKeyWord = "";
    public String strPageTokenRestaurantList = "";
    private ArrayList<RestaurantListVO> listRestaurant = new ArrayList<>();

    public MessageVO msg;
    private CountDownTimer countDownTimer;
//    public int heightForExpandAnimation = 0;

    // Objects for Local Broadcast manager & Push notifications handling in the app.
    public boolean mIsReceiverRegistered = false;
    public MyBroadcastReceiver mReceiver;
    public BroadcastReceiver mNotificationReceiver;

    public boolean isOnInit = true;

    private Intent intentDataOnActivityResult = null;
    private final int ACTION_FRIENDS_LIST_API = 701;
    public String otherUserID = "";
    public UserProfileAPI userProfileVo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        setTitleSupportActionBar(getString(R.string.title_restaurants_list));
//        setTitleSupportActionBar(name);

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_chat_details_activity, getMiddleContent());

        linearChatFriends = (LinearLayout) view.findViewById(R.id.linearLayoutChatDetailsActivityChatFriends);
        linearChatRequest = (LinearLayout) view.findViewById(R.id.linearLayoutChatDetailsActivityChatRequest);
        linearChatRequestAcceptReject = (LinearLayout) view.findViewById(R.id.linearLayoutChatDetailsActivityChatRequestAcceptReject);
        txtChatRequestMessage = (TextView) view.findViewById(R.id.textViewChatDetailsActivityChatRequestMessage);
        rvChatRequestRestaurants = (RecyclerView) view.findViewById(R.id.recyclerViewChatDetailsActivityChatRequestRestaurants);
        txtChatRequestAcceptRejectPlaceholder = (TextView) view.findViewById(R.id.textViewChatDetailsActivityChatRequestAcceptRejectPlaceholder);
        txtChatRequestAccept = (TextView) view.findViewById(R.id.textViewChatDetailsActivityChatRequestAccept);
        txtChatRequestReject = (TextView) view.findViewById(R.id.textViewChatDetailsActivityChatRequestReject);
        imgUpArrow = (ImageView) view.findViewById(R.id.imageViewChatDetailsActivityUpArrow);
        imageViewRestoList = (ImageView) view.findViewById(R.id.imageViewRestoList);
        imgUploadAttachment = (ImageView) view.findViewById(R.id.imageViewChatDetailsActivityUploadAttachment);
        imgacceptChat = (ImageView) view.findViewById(R.id.imgacceptChat);
        imgrejectChat = (ImageView) view.findViewById(R.id.imgrejectChat);
        txtSend = (TextView) view.findViewById(R.id.textViewChatDetailsActivitySendMessage);
        imgSend = (ImageView) view.findViewById(R.id.imageViewChatDetailsActivitySendMessage);
        txtViewShownIntrest = (TextView) view.findViewById(R.id.txtViewShownIntrest);
        textacceptReject = (TextView) view.findViewById(R.id.textacceptReject);
        relativeChatFooter = (RelativeLayout) view.findViewById(R.id.relativeChatFooter);
        relativeShownIntrest = (RelativeLayout) view.findViewById(R.id.relativeShownIntrest);
        edtMessage = (EditText) view.findViewById(R.id.editTextChatDetailsActivityMessage);
        rvUsers = (RecyclerView) view.findViewById(R.id.recyclerViewChatDetailsActivityUsersList);
        rvChats = (RecyclerView) view.findViewById(R.id.recyclerViewChatDetailsActivityChatList);
        recyclerViewListResto = (RecyclerView) view.findViewById(R.id.recyclerViewListResto);
        linearLayoutListResto = (LinearLayout) view.findViewById(R.id.linearLayoutListResto);
        linearLayoutAcceptReject = (LinearLayout) view.findViewById(R.id.linearLayoutAcceptReject);

        llmRecyclerViewUsers = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        llmRecyclerViewResto = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        llmRecyclerViewChatRequestRestaurant = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        llmRecyclerViewChat = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        llmRecyclerViewChat.setReverseLayout(true);
        rvUsers.setLayoutManager(llmRecyclerViewUsers);
        rvUsers.setHasFixedSize(false);
        recyclerViewListResto.setLayoutManager(llmRecyclerViewResto);
        recyclerViewListResto.setHasFixedSize(false);

        rvChatRequestRestaurants.setLayoutManager(llmRecyclerViewChatRequestRestaurant);
        rvChatRequestRestaurants.setHasFixedSize(false);

        rvChats.setLayoutManager(llmRecyclerViewChat);
        rvChats.setHasFixedSize(false);

        isOnInit = true;

        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_chat_detail_activity, linearToolBarCustomLayout);
        linearToolBarCustomLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setCustomView(viewCustomActionBar);
//        Toolbar parent =(Toolbar) viewCustomActionBar.getParent();
        toolbarLayoutRoot.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        toolbarLayoutRoot.setContentInsetsAbsolute(0, 0);
        toolbarLayoutRoot.setContentInsetsRelative(0, 0);
        toolbarLayoutRoot.setContentInsetStartWithNavigation(0);
        imageViewToolbarChatActivityUserProfilePic = (CircularImageView) toolbarLayoutRoot.findViewById(R.id.imageViewToolbarChatActivityUserProfilePic);
        textViewToolbarBaseLastseen = (TextView) toolbarLayoutRoot.findViewById(R.id.textViewToolbarBaseLastseen);
        textViewToolbarBaseActivityTitle = (TextView) toolbarLayoutRoot.findViewById(R.id.textViewToolbarChatActivityTitle);

        toolbarLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(context, SingleUserDetailActivity.class);
                intent.putExtra("fromChatScreen", true);
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
                    intent.putExtra(Constants.INTENT_USER_ID, chatSelected.getFriend2());
                else {
                    intent.putExtra(Constants.INTENT_USER_ID, chatSelected.getFriend1());
                }
                /*startActivity(intent);*/
                startActivityForResult(intent, Constants.REQEUST_CODE_FROM_CHAT);

            }
        });

        // Clear all saved notification messages
        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, "");
        SharedPreferenceUtil.save();

        // Clear all the notifications in notification bar
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();

        if (getIntent().hasExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST)
                && getIntent().getBooleanExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false)) {
            // TODO open restaurant list screen from here.
            intent = new Intent(context, RestaurantListForSendRequestActivity.class);
            if (getIntent().hasExtra(Constants.INTENT_CHAT_SELECTED))
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED));
            if (getIntent().hasExtra(Constants.INTENT_CHAT_LIST))
                intent.putExtra(Constants.INTENT_CHAT_LIST, getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST));
            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
//                intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
            startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
        mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");

//        Constants.CheckOtherUSerOnline =chatVo.getId();

        if (getIntent().hasExtra(Constants.INTENT_CHAT_LIST)) {
            listUsers = (ArrayList<ChatListVO>) getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST);
            if (getIntent().hasExtra(Constants.INTENT_CHAT_SELECTED)) {
                chatSelected = (ChatListVO) getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED);
                initLayout();
            }
        } else {
            // If chat details or chat object not found in intent, get user id from chat and call for friends list and fetch the chat object from
            // there.
            if (getIntent().hasExtra(Constants.IS_FROM_NOTIFICATION) && getIntent().getBooleanExtra(Constants.IS_FROM_NOTIFICATION, false)) {
//                listUsers
//                chatSelected
                if (getIntent().hasExtra(Constants.INTENT_USER_ID) && getIntent().getStringExtra(Constants.INTENT_USER_ID) != null
                        && !getIntent().getStringExtra(Constants.INTENT_USER_ID).trim().isEmpty()) {
                    showProgress(getString(R.string.loading));

                    otherUserID = getIntent().getStringExtra(Constants.INTENT_USER_ID).trim();

                    if (getIntent().hasExtra(Constants.INTENT_USER_DETAILS)
                            && getIntent().getSerializableExtra(Constants.INTENT_USER_DETAILS) != null) {
                        userProfileVo = (UserProfileAPI) getIntent().getSerializableExtra(Constants.INTENT_USER_DETAILS);
                    }
                    callFriendsListApiUpdateChat();
                }
            }
        }

        setListener();
        /*setAllTypefaceMontserratRegular(view);
        setAllTypefaceMontserratLight(txtCuisine);
        setAllTypefaceMontserratLight(numberPickerDay);
        setAllTypefaceMontserratLight(numberPickerTime);
        setAllTypefaceMontserratLight(numberPickerPeople);*/

    }

    private void initLayout() {
        if (chatSelected != null) {
            if (!chatSelected.getProfilePic().trim().isEmpty()) {
                Picasso.with(context).load(chatSelected.getProfilePic().trim())
                        .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48)).centerCrop()
//                            .placeholder(R.drawable.ic_user_profile_avatar).error(R.drawable.ic_user_profile_avatar)
                        .placeholder(chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                                : R.drawable.ic_placeholder_user_avatar_female_54)
                        .error(chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                                : R.drawable.ic_placeholder_user_avatar_female_54)
                        .into(imageViewToolbarChatActivityUserProfilePic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.d(TAG, "Error loading profile pic of user: " + chatSelected.getProfilePic().trim());
                            }
                        });
            } else {
                    /*imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_user_profile_avatar);*/
                if (chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_male_54);
                } else {
                    imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_female_54);
                }

            }

            name = chatSelected.getFirstName() + " " + chatSelected.getLastName();
            textViewToolbarBaseActivityTitle.setText(name);
            updateChatList(chatSelected.getMatchid());
            for (int i = 0; i < listUsers.size(); i++) {
                if (listUsers.get(i).getMatchid().equalsIgnoreCase(chatSelected.getMatchid())) {
                    listUsers.get(i).setIsSelected("1");
                } else {
                    listUsers.get(i).setIsSelected("0");
                }
            }

            // set selected friend id here.
            if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                // User is sender in the chat
                selectedFriendId = chatSelected.getFriend2().trim();
            } else {
                // User is receiver in the chat
                selectedFriendId = chatSelected.getFriend1().trim();
            }

            Log.d(TAG, "Chat selected status in details: " + chatSelected.getStatus());

            // Check if both users are friend or not. If not, check current user is sender or receiver in the chat.
            if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                isAlreadyFriend = true;
                linearChatRequest.setVisibility(View.GONE);
                linearChatFriends.setVisibility(View.VISIBLE);
            } else {
                isAlreadyFriend = false;

                if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                    // User is sender in the chat
                    isReceiver = false;
                } else {
                    // User is receiver in the chat
                    isReceiver = true;
                }

                    /*linearChatRequest.setVisibility(View.VISIBLE);
                    linearChatFriends.setVisibility(View.GONE);*/
            }

            setAdapterUsersList();
            setAdapterChatsList();
            setFirebaseChatListeners();
        }
    }

    private void setListener() {
        imgUpArrow.setOnClickListener(this);
        imgUploadAttachment.setOnClickListener(this);
        txtSend.setOnClickListener(this);
        imgSend.setOnClickListener(this);
        imageViewRestoList.setOnClickListener(this);
        imgrejectChat.setOnClickListener(this);
        imgacceptChat.setOnClickListener(this);
        textViewToolbarBaseActivityTitle.setOnClickListener(this);
        txtChatRequestAccept.setOnClickListener(this);
        txtChatRequestReject.setOnClickListener(this);

        edtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                    countDownTimer = null;
                }

                countDownTimer = new CountDownTimer(2500, 2500) {

                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {

                        Log.i("Typing", "false");
                        WriteNewUserStatus(getString(R.string.status_online));
                    }
                }.start();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    Log.i("Typing", "true");

                    WriteNewUserStatus("typing...");
                }
            }
        });
    }

    private void setFirebaseChatListeners() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
        mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Log.d(TAG, "onAuthStateChanged:UserNotNull");

                    listenerChat = new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            /*for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                            // TODO: handle the chat message here.
                            Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                            try {
                                msg = dataSnapshot.getValue(MessageVO.class);
//                                Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    msg.setKey(dataSnapshot.getKey().trim());
                                    if (msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
                                            && msg.getDelivered().toString().trim().equalsIgnoreCase("") && msg.getDelivered().trim().isEmpty()) {
                                        Calendar c = Calendar.getInstance();
                                        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
                                        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child
                                                (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                                    }

                                    /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").equalsIgnoreCase(msg.getSenderId())) {*/
                                    if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                        String currentSenderId = !chatSelected.getFriend1().trim().equalsIgnoreCase(
                                                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                                ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                                        if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                            int updatePosition = -1;
                                            for (int i = 0; i < listChats.size(); i++) {
                                                if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                    updatePosition = i;
                                                }
                                            }

                                            if (updatePosition != -1) {
                                                listChats.set(updatePosition, msg);
                                            } else {
                                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                    listChats.add(0, msg);
                                                }
                                            }
                                        }
                                    } else {
                                        int updatePosition = -1;
                                        for (int i = 0; i < listChats.size(); i++) {
                                            if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                updatePosition = i;
                                            }
                                        }

                                        if (updatePosition != -1) {
                                            listChats.set(updatePosition, msg);
                                        } else {
                                            if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                listChats.add(0, msg);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            /*}*/

                            setAdapterChatsList();
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                           /* for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                            // TODO: handle the chat message here.
                            Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                            try {

                                msg = dataSnapshot.getValue(MessageVO.class);
                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    if (msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
                                            && msg.getDelivered().toString().trim().equalsIgnoreCase("") && msg.getDelivered().trim().isEmpty()) {
                                        Calendar c = Calendar.getInstance();
                                        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                        if (msg.getDelivered().equalsIgnoreCase(""))
                                            mDatabaseMessages.child(chatSelected.getMatchid().trim()).child
                                                    (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                                    }
                                }
                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
//                                    Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                                    msg.setKey(dataSnapshot.getKey().trim());

                                    if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                        String currentSenderId =
                                                !chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                                        ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                                        if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                            int updatePosition = -1;
                                            for (int i = 0; i < listChats.size(); i++) {
                                                if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                    updatePosition = i;
                                                }
                                            }

                                            if (updatePosition != -1) {
                                                listChats.set(updatePosition, msg);
                                            } else {
                                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                    listChats.add(0, msg);
                                                }
                                            }
                                        }
                                    } else {
                                        int updatePosition = -1;
                                        for (int i = 0; i < listChats.size(); i++) {
                                            if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                updatePosition = i;
                                            }
                                        }

                                        if (updatePosition != -1) {
                                            listChats.set(updatePosition, msg);
                                        } else {
                                            if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                listChats.add(0, msg);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            /*}*/

                            setAdapterChatsList();
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };

                    listenerStatus = new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            Log.d(TAG, "Status Listener ==> Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim()
                                    + ", Current User Id: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                            try {
                                StatusVO status = dataSnapshot.getValue(StatusVO.class);

                                if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {
                                    Log.d(TAG, "Status of other user: " + status.getStatus());
                                    if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                                        if (!status.getLastSeen().trim().isEmpty()) {
                                            if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
//                                            textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " " + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                                textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                                        + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                            } else {
                                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                            }
                                        } else {
                                            textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                        }

//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                    } else {
                                        if (status.getStatus().trim().isEmpty()) {
                                            textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                        } else {
                                            if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                            } else {
                                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                            }
                                        }

//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString().trim());
//                                        SharedPreferenceUtil.save();
                                    }
                                } else {
                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                }

                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            /*}*/

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            Log.d(TAG, "Status Listener ==> Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim()
                                    + ", Current User Id: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));

                            try {

                                StatusVO status = dataSnapshot.getValue(StatusVO.class);

                                if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {

                                    if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                                        if (!status.getLastSeen().trim().isEmpty()) {
                                            if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                                        + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                            } else {
                                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                            }
                                        } else {
                                            textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                        }
//                                        textViewToolbarBaseLastseen.setText("last seen " + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                    } else {
                                        if (status.getStatus().trim().isEmpty()) {
                                            textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                        } else {
                                            if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                            } else {
                                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                            }
                                        }
//                                        textViewToolbarBaseLastseen.setText(status.getStatus());
//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                    }
                                }
                                Log.i(status.getStatus(), "status");
                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };
                    Log.i(TAG, "Match Id: " + chatSelected.getMatchid() + ", Friend1: " + chatSelected.getFriend1() + ", Friend2: " + chatSelected.getFriend2());
                    mDatabaseMessages.child(chatSelected.getMatchid().trim()).addChildEventListener(listenerChat);

                    if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                        mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend2()).addChildEventListener(listenerStatus);
                    } else {
                        mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend1()).addChildEventListener(listenerStatus);
                    }

                    /*// Binding chat history value events listener to load all the chat messages for current chat
                    listenerChatListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {
                                // TODO: handle the chat message here.
                                Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + chatSnapshot.toString() + ", Key: " + chatSnapshot.getKey().trim());
                                try {
                                    MessageVO msg = chatSnapshot.getValue(MessageVO.class);
                                    msg.setKey(chatSnapshot.getKey().trim());
                                    *//*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").equalsIgnoreCase(msg.getSenderId())) {*//*
                                    int updatePosition = -1;
                                    for (int i = 0; i < listMessages.size(); i++) {
                                        if (listMessages.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                            updatePosition = i;
                                        }
                                    }

                                    if (updatePosition != -1) {
                                        listMessages.set(updatePosition, msg);
                                    } else {
                                        listMessages.add(0, msg);
                                    }
                                    *//*}*//*
                                    *//*adapter.notifyDataSetChanged();*//*
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if (adapter == null) {
                                adapter = new AdapterChatDetail(context, listMessages);
                                rvChatting.setAdapter(adapter);
                            } else {
                                adapter.notifyDataSetChanged();
                            }
                            rvChatting.scrollToPosition(0);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    };

                    mDatabaseMessages.child("a3832fdd931de9b778f0bcd2389bca81").addValueEventListener(listenerChatListener);*/
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        if (mAuthListener == null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    public void setAdapterUsersList() {

        // Set Recycler view adapter here.
        if (adapter != null) {
            adapter.notifyItemRangeInserted(0, listUsers.size());
            adapter.notifyDataSetChanged();
        } else {
            adapter = new ChatUsersListAdapter(context, listUsers);
            rvUsers.setAdapter(adapter);
        }
        setScrollListenerFriendsList();
    }

    public void setAdapterChatsList() {
        if (!isAlreadyFriend) {
            isTextMsgReached = false;
            isRestoLimitReached = false;
            RestoCount = 0;

            String strMessageRequest = "";
            ArrayList<MessageVO> listRestaurant = new ArrayList<>();
            /*if (listChats.size() <= 6) {*/
            for (int i = 0; i < listChats.size(); i++) {
                if (listChats.get(i).getContentType().equalsIgnoreCase("text")) {
                    isTextMsgReached = true;
                }

                if (listChats.get(i).getContentType().equalsIgnoreCase("restaurant")) {
                    RestoCount++;
                    Log.i("RESTOCOUNT", "" + RestoCount);
                    if (RestoCount == 5) {
                        isRestoLimitReached = true;
                    } else {
                        isRestoLimitReached = false;
                    }
                }

                if (!listChats.get(i).getMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_a_restaurant))
                        && !listChats.get(i).getMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_an_image))) {
                    strMessageRequest = listChats.get(i).getMsg().trim();
                } else if (listChats.get(i).getMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_a_restaurant))) {
                    listRestaurant.add(listChats.get(i));
                }
            }

            if (isReceiver) {
                    /*linearLayoutListResto.setVisibility(View.GONE);
                    linearLayoutAcceptReject.setVisibility(View.VISIBLE);*/
                if (isTextMsgReached || isRestoLimitReached) {
                    // Display request message & restaurants here.
                    txtChatRequestMessage.setText(strMessageRequest.trim());

                    // Set list of restaurants adapter
                    ChatRequestRestaurantsListAdapter adapter = new ChatRequestRestaurantsListAdapter(context, listRestaurant);
                    rvChatRequestRestaurants.setAdapter(adapter);

                    linearChatRequestAcceptReject.setVisibility(View.VISIBLE);
                    txtChatRequestAcceptRejectPlaceholder.setText(getString(R.string.to_continue_chat_accept_friend_request));

                    linearChatRequest.setVisibility(View.VISIBLE);
                    linearChatFriends.setVisibility(View.GONE);
                } else {
                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                        relativeChatFooter.setVisibility(View.VISIBLE);
                        linearLayoutListResto.setVisibility(View.GONE);
                        relativeShownIntrest.setVisibility(View.GONE);
                        imgUploadAttachment.setVisibility(View.VISIBLE);
                        linearLayoutAcceptReject.setVisibility(View.GONE);
                    } else {
                        imgUploadAttachment.setVisibility(View.GONE);
                        relativeChatFooter.setVisibility(View.GONE);
                        linearLayoutListResto.setVisibility(View.GONE);
                        relativeShownIntrest.setVisibility(View.GONE);
                        linearLayoutAcceptReject.setVisibility(View.VISIBLE);
                        textacceptReject.setText(getString(R.string.accept_friend_request_to_start_conversation));
                    }

                    linearChatRequest.setVisibility(View.GONE);
                    linearChatFriends.setVisibility(View.VISIBLE);
                }
            } else {

                if (isTextMsgReached || isRestoLimitReached) {
                    // Display request message & restaurants here.
                    txtChatRequestMessage.setText(strMessageRequest.trim());

                    // Set list of restaurants adapter
                    ChatRequestRestaurantsListAdapter adapter = new ChatRequestRestaurantsListAdapter(context, listRestaurant);
                    rvChatRequestRestaurants.setAdapter(adapter);

                    linearChatRequestAcceptReject.setVisibility(View.GONE);
                    txtChatRequestAcceptRejectPlaceholder.setText(chatSelected.getFirstName() + " " + chatSelected.getLastName() + " "
                            + getResources().getString(R.string.cant_send_msg));

                    linearChatRequest.setVisibility(View.VISIBLE);
                    linearChatFriends.setVisibility(View.GONE);
                } else {
                    linearLayoutAcceptReject.setVisibility(View.GONE);

                    linearChatRequest.setVisibility(View.GONE);
                    linearChatFriends.setVisibility(View.VISIBLE);

                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
//                    txtSend.setVisibility(View.VISIBLE);
                        imgSend.setVisibility(View.VISIBLE);

                        linearLayoutListResto.setVisibility(View.GONE);
                        relativeShownIntrest.setVisibility(View.GONE);
                        relativeChatFooter.setVisibility(View.VISIBLE);
                        imageViewRestoList.setVisibility(View.VISIBLE);
                    } else if (isTextMsgReached && isRestoLimitReached) {
//                    txtSend.setVisibility(View.GONE);
                        imgSend.setVisibility(View.GONE);
                        imageViewRestoList.setVisibility(View.GONE);
                        imgUploadAttachment.setVisibility(View.GONE);
                        relativeChatFooter.setVisibility(View.GONE);
                        relativeShownIntrest.setVisibility(View.VISIBLE);
                        linearLayoutListResto.setVisibility(View.GONE);
                        txtViewShownIntrest.setText(chatSelected.getFirstName() + " " + chatSelected.getLastName() + " " + getResources().getString(R.string.cant_send_msg));
                    } else {
                        relativeChatFooter.setVisibility(View.VISIBLE);
                        linearLayoutListResto.setVisibility(View.GONE);
                        imgUploadAttachment.setVisibility(View.GONE);
                        relativeShownIntrest.setVisibility(View.GONE);
                        if (isTextMsgReached) {
//                        txtSend.setVisibility(View.GONE);
                            imgSend.setVisibility(View.GONE);
                        } else {
//                        txtSend.setVisibility(View.VISIBLE);
                            imgSend.setVisibility(View.VISIBLE);
                            imageViewRestoList.setVisibility(View.GONE);
                        }
                        if (isRestoLimitReached) {
                            imageViewRestoList.setVisibility(View.GONE);

                        } else {
                            imageViewRestoList.setVisibility(View.GONE);
                        }
                    }

                    if (adapterChats == null) {
                        adapterChats = new ChatAdapter(context, listChats);
                        rvChats.setAdapter(adapterChats);
                    } else {
                        adapterChats.notifyDataSetChanged();
                    }

                    if (isOnInit) {
                        if (rvChats != null) {
                            rvChats.smoothScrollToPosition(0);
                            rvChats.scrollToPosition(0);
                        }
                    }
                }
            }
        } else {
            linearLayoutAcceptReject.setVisibility(View.GONE);

            linearChatRequest.setVisibility(View.GONE);
            linearChatFriends.setVisibility(View.VISIBLE);

            if (adapterChats == null) {
                adapterChats = new ChatAdapter(context, listChats);
                rvChats.setAdapter(adapterChats);
                rvChats.scrollToPosition(0);
            } else {
                adapterChats.notifyDataSetChanged();
            }

            if (isReceiver) {
                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                    relativeChatFooter.setVisibility(View.GONE);
                    linearLayoutListResto.setVisibility(View.GONE);
                    relativeShownIntrest.setVisibility(View.GONE);
                    linearLayoutAcceptReject.setVisibility(View.VISIBLE);
                    imgUploadAttachment.setVisibility(View.GONE);
                    textacceptReject.setText(getString(R.string.accept_friend_request_to_start_conversation));
                } else {
                    relativeChatFooter.setVisibility(View.VISIBLE);
                    imgUploadAttachment.setVisibility(View.VISIBLE);
                    linearLayoutListResto.setVisibility(View.GONE);
                    relativeShownIntrest.setVisibility(View.GONE);
                    linearLayoutAcceptReject.setVisibility(View.GONE);
                }
            } else {
                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
//                    txtSend.setVisibility(View.VISIBLE);
                    imgSend.setVisibility(View.VISIBLE);
                    linearLayoutAcceptReject.setVisibility(View.GONE);
                    linearLayoutListResto.setVisibility(View.GONE);
                    relativeShownIntrest.setVisibility(View.GONE);
                    imgUploadAttachment.setVisibility(View.VISIBLE);
                    relativeChatFooter.setVisibility(View.VISIBLE);
                    imageViewRestoList.setVisibility(View.VISIBLE);
                } else if (isTextMsgReached && isRestoLimitReached) {
//                    txtSend.setVisibility(View.GONE);
                    imgSend.setVisibility(View.GONE);
                    imageViewRestoList.setVisibility(View.GONE);

                    relativeChatFooter.setVisibility(View.GONE);
                    imgUploadAttachment.setVisibility(View.GONE);
                    linearLayoutAcceptReject.setVisibility(View.GONE);
                    relativeShownIntrest.setVisibility(View.VISIBLE);
                    linearLayoutListResto.setVisibility(View.GONE);
                    txtViewShownIntrest.setText(chatSelected.getFirstName() + " " + chatSelected.getLastName() + " " + getResources().getString(R.string.cant_send_msg));
                    //TODO remove edit text and put accept reject
                } else {
                    relativeChatFooter.setVisibility(View.VISIBLE);
                    relativeShownIntrest.setVisibility(View.GONE);
                    imgUploadAttachment.setVisibility(View.GONE);
                    if (isTextMsgReached) {
//                        txtSend.setVisibility(View.GONE);
                        imgSend.setVisibility(View.GONE);
                    } else {
//                        txtSend.setVisibility(View.VISIBLE);
                        imgSend.setVisibility(View.VISIBLE);
                    }
                    if (isRestoLimitReached) {
                        imageViewRestoList.setVisibility(View.GONE);

                    } else {
                        imageViewRestoList.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        /*edtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAlreadyFriend) {
                    if (!isTextMsgReached && !isRestoLimitReached) {
                        if (isReceiver) {
                            Log.d(TAG, "Send a new message here along.");
                        }
                    }
                }
            }
        });*/

        edtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    linearLayoutListResto.setVisibility(View.GONE);

                if (hasFocus) {
                    if (!isAlreadyFriend) {
                        if (!isTextMsgReached && !isRestoLimitReached) {
                            if (!isReceiver) {
                                Log.d(TAG, "Send a new message here along.");
                            /*// Hide keyboard here.
                            InputMethodManager inputMethodManger = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                            inputMethodManger.hideSoftInputFromWindow(edtMessage.getWindowToken(), 0);*/

                                edtMessage.clearFocus();

                                /*showDialogSendMessage();*/

                                // Todo Forward user to restaurant list screen (specific screen for sending a request)
                                intent = new Intent(context, RestaurantListForSendRequestActivity.class);
                                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                                intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
//                                intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
                                startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
                            }
                        }
                    }
                }
            }
        });

        /*edtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    }

    public void setScrollListenerFriendsList() {
        // Scroll listener to call paging at the end of scroll
        rvUsers.addOnScrollListener(new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerViewUsers) {
            @Override
            public void onLoadMore(int lastItemScrolled) {
                Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);

            }

            @Override
            public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
                Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount);
                if (firstVisibleItem != -1) {
                    posToScrollUsersList = firstVisibleItem;
                }

               /* if (visibleItemCount > 1) {
                    Log.d("SCROLL_MANUAL", "Scrolling to position: " + posToScrollRestaurantList);
                    llmRecyclerView.smoothScrollToPosition(rvRestaurants, null, posToScrollRestaurantList);
                    llmRecyclerView.scrollToPosition(posToScrollRestaurantList);
                    *//*rvRestaurants.smoothScrollToPosition(posToScrollRestaurantList);*//*
                }*/
            }

            @Override
            public void onLoadMoreTopItems() {
            }
        });
    }

    public void setScrollListenerChatsList() {
        // Scroll listener to call paging at the end of scroll
        rvChats.addOnScrollListener(new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerViewUsers) {
            @Override
            public void onLoadMore(int lastItemScrolled) {
                Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);

            }

            @Override
            public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
                Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount);
                if (firstVisibleItem != -1) {
                    posToScrollChatsList = firstVisibleItem;
                }

               /* if (visibleItemCount > 1) {
                    Log.d("SCROLL_MANUAL", "Scrolling to position: " + posToScrollRestaurantList);
                    llmRecyclerView.smoothScrollToPosition(rvRestaurants, null, posToScrollRestaurantList);
                    llmRecyclerView.scrollToPosition(posToScrollRestaurantList);
                    *//*rvRestaurants.smoothScrollToPosition(posToScrollRestaurantList);*//*
                }*/
            }

            @Override
            public void onLoadMoreTopItems() {
            }
        });
    }

    public void expandView(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        final int targetHeight = heightForExpandAnimation == 0 ? v.getMeasuredHeight() : heightForExpandAnimation;
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapseView(final View v) {
        final int initialHeight = v.getMeasuredHeight();
//        heightForExpandAnimation = initialHeight;
//        Log.d("initialHeight", "initialHeight: "+initialHeight);

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("FromChatList") && getIntent().getBooleanExtra("FromChatList", false)) {
            for (int i = 0; i < listUsers.size(); i++) {
                if (listUsers.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                    listUsers.get(i).setIsSelected("0");
                }
            }

            Intent intent = new Intent();
        /*String text = "Result to be returned...."*/
            intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
            intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
            setResult(RESULT_OK, intent);
            supportFinishAfterTransition();
        } else if (getIntent().hasExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST)
                && getIntent().getBooleanExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false)) {
            if (intentDataOnActivityResult != null) {
                if (chatSelected != null)
                    intentDataOnActivityResult.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intentDataOnActivityResult.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
                setResult(RESULT_OK, intentDataOnActivityResult);
                supportFinishAfterTransition();
                overridePendingTransition(0, 0);
            } else {
                Intent intent = new Intent();
                if (chatSelected != null)
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
                setResult(RESULT_CANCELED, intent);
                supportFinishAfterTransition();
            }
            /*super.onBackPressed();*/
        } else if ((getIntent().hasExtra("isFromNotification") && getIntent().getBooleanExtra("isFromNotification", false))
                || (getIntent().hasExtra(Constants.IS_FROM_NOTIFICATION) && getIntent().getBooleanExtra(Constants.IS_FROM_NOTIFICATION, false))) {
            intent = new Intent(mContext, ChatListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(0, 0);
            supportFinishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.imageViewChatDetailsActivityUpArrow:
                if (isToExpandChatList) {
                    // TODO Close datepicker view
                    isToExpandChatList = false;
                    if (imgUpArrow.getRotation() != 180f) {
                        imgUpArrow.animate().rotationBy(180f).start();
                    }
                    /*if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }*/
                    collapseView(rvUsers);
                } else {
                    // TODO Open datepicker view
                    isToExpandChatList = true;
                    if (imgUpArrow.getRotation() != -180) {
                        imgUpArrow.animate().rotationBy(-180f).start();
                    }
                    /*if (imgDownArrowPeoplePicker.getRotation() != -180) {
                        imgDownArrowPeoplePicker.animate().rotationBy(-180f).start();
                    }*/
                    /*linearViewDatePicker.setVisibility(View.GONE);*/
                    expandView(rvUsers);
                }
                break;

            case R.id.imageViewChatDetailsActivityUploadAttachment:

                if (isStoragePermissionGranted()) {
                    openUploadImageFromDeviceIntent();
                } else {
                    checkReadWritePermissionGrantedOrNot();
                }

                break;

//            case R.id.textViewChatDetailsActivitySendMessage:
            case R.id.imageViewChatDetailsActivitySendMessage:
                if (edtMessage.getText().toString().equalsIgnoreCase("")) {
                    /*Toast.makeText(activity, "You Can't Send empty message!", Toast.LENGTH_LONG).show();*/
                    showToast(getString(R.string.message_cannot_be_empty));
                } else {
                    /*HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Image", "http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image1.jpg");
                    hashMap.put("Text", editTextMessagesTab.getText().toString());
                    hashMap.put("Name", "Name(User)");
                    arrayList.add(hashMap);*/
                   /* long timestamp = Constants.convertTimestampTo10DigitsOnly(System.currentTimeMillis(), true);*/
                    // Convert current time to UTC or GMT timestamp.
                    Calendar c = Calendar.getInstance();

                    MessageVO chat = new MessageVO();
                    chat.setMsg(edtMessage.getText().toString().trim());
                    chat.setTimeStamp(Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true));

                    chat.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());
                   /* listMessages.add(0, chat);
                    adapter.notifyItemInserted(0);
                    adapter.notifyDataSetChanged();*/

                    WriteMessageText(edtMessage.getText().toString().trim());
                    edtMessage.setText("");
                }
                break;

            case R.id.textViewToolbarBaseActivityTitle:
                intent = new Intent(context, SingleUserDetailActivity.class);
                intent.putExtra("fromChatScreen", true);
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
                    intent.putExtra(Constants.INTENT_USER_ID, chatSelected.getFriend2());
                else {
                    intent.putExtra(Constants.INTENT_USER_ID, chatSelected.getFriend1());
                }
                startActivityForResult(intent, Constants.REQEUST_CODE_FROM_CHAT);

                break;
            case R.id.imageViewRestoList:

                /*if (linearLayoutListResto.getVisibility() == View.VISIBLE) {
                    linearLayoutListResto.setVisibility(View.GONE);
                } else {
                    edtMessage.clearFocus();
                    hideKeyboard(this);
                    linearLayoutListResto.setVisibility(View.VISIBLE);
                    GoogleRestaurentListApi();
                }*/

                // TODO open restaurant list screen from here.
                if (getIntent().hasExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST)
                        && getIntent().getBooleanExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false)) {
                    intent = new Intent(context, RestaurantListForSendRequestActivity.class);
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                    intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                    intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
//                intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
                    startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
                } else {
                    intent = new Intent(context, RestaurantListForSendRequestActivity.class);
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                    intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//                intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
                    startActivity(intent);
                }

                break;

            case R.id.imgacceptChat:
                callAcceptFriendRequest();
                break;

            case R.id.imgrejectChat:
                callRejectFriendRequest();
                break;

            case R.id.textViewChatDetailsActivityChatRequestAccept:
                callAcceptFriendRequest();
                break;

            case R.id.textViewChatDetailsActivityChatRequestReject:
                callRejectFriendRequest();
                break;

            default:
                break;

        }
    }

    private void callRejectFriendRequest() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_deletefriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), selectedFriendId);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST, ChatDetailsActivity.this);
    }

    private void callAcceptFriendRequest() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action),
                context.getResources().getString(R.string.api_response_param_action_acceptfriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), selectedFriendId);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST, ChatDetailsActivity.this);
    }

    public void callFriendsListApiUpdateChat() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                ACTION_FRIENDS_LIST_API, ChatDetailsActivity.this);
    }

    public void GoogleRestaurentListApi() {
        if (NetworkUtil.isOnline(context)) {
            if (!isEndOfResult) {
                double radiusInMeters = Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                        .trim().replaceAll(",", ".").trim()) * 1000;

                showProgress(getString(R.string.loading));
                params = new HashMap<>();
                params.put(getString(R.string.google_api_param_key_), getString(R.string.google_api_param_key_value));
                params.put(getString(R.string.google_api_param_key_location), "" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")
                        + "," + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
                params.put(getString(R.string.google_api_param_key_radius), String.valueOf(radiusInMeters));
//            params.put(getString(R.string.google_api_param_key_rankby), "distance");
//            params.put(getString(R.string.google_api_param_key_keyword), "restaurant");
                params.put(getString(R.string.google_api_param_key_type), "restaurant");
                if (!strPageTokenRestaurantList.trim().isEmpty()) {
                    params.put(getString(R.string.google_api_param_key_pagetoken), strPageTokenRestaurantList.trim());
                }

                if (!searchKeyWord.isEmpty()) {
                    params.put(getString(R.string.google_api_param_key_keyword), searchKeyWord.trim());
                }

                new HttpRequestSingleton(context, getString(R.string.google_api_list_url), params,
                        Constants.ACTION_CODE_GOOGLE_LIST_API, this);
            }
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {

            if (action == Constants.ACTION_CODE_GOOGLE_LIST_API) {
                Log.d(TAG, "ACTION_CODE_GOOGLE_LIST_API: " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";
                    String next_page_token = !jsonObjectResponse.isNull("next_page_token") ? jsonObjectResponse.getString("next_page_token").trim() : "";
                    strPageTokenRestaurantList = next_page_token;

                    if (status.trim().equalsIgnoreCase("OK")) {
                        JSONArray Results = !jsonObjectResponse.isNull("results") ? jsonObjectResponse.getJSONArray("results") : new JSONArray();

                        int size = listRestaurant.size();
                        for (int i = 0; i < Results.length(); i++) {
                            RestaurantListVO restovo = new RestaurantListVO();
                            JSONObject data = Results.getJSONObject(i);
                            JSONObject geomatry = !data.isNull(getString(R.string.google_response_param_key_geometry))
                                    ? data.getJSONObject(getString(R.string.google_response_param_key_geometry)) : new JSONObject();
                            JSONObject location = !geomatry.isNull(getString(R.string.google_response_param_key_location))
                                    ? geomatry.getJSONObject(getString(R.string.google_response_param_key_location)) : new JSONObject();

                            restovo.setLatitude(!location.isNull(getString(R.string.google_response_param_key_lat))
                                    ? location.getString(getString(R.string.google_response_param_key_lat)).trim() : "");
                            restovo.setLongitude(!location.isNull(getString(R.string.google_response_param_key_lng))
                                    ? location.getString(getString(R.string.google_response_param_key_lng)).trim() : "");

                            Log.d(TAG, "Location: " + location.toString() + ", Lat: " + location.getString(getString(R.string.google_response_param_key_lat))
                                    + ", Long: " + location.getString(getString(R.string.google_response_param_key_lng)));

                            restovo.setPlaceid(!data.isNull("place_id") ? data.getString("place_id").trim() : "");

                            restovo.setName(!data.isNull(getString(R.string.google_response_param_key_name))
                                    ? data.getString(getString(R.string.google_response_param_key_name)).trim() : "");
                            JSONObject opening_hours = !data.isNull(getString(R.string.google_response_opening_hours))
                                    ? data.getJSONObject(getString(R.string.google_response_opening_hours)) : new JSONObject();
                            restovo.setOpen_now(!opening_hours.isNull(getString(R.string.google_response_open_now))
                                    ? opening_hours.getString(getString(R.string.google_response_open_now)).trim() : "");
                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");

                            JSONArray photos = !data.isNull("photos") ? data.getJSONArray("photos") : new JSONArray();

                            for (int j = 0; j < photos.length(); j++) {
                                JSONObject photosObj = photos.getJSONObject(j);

                                String photo_reference = !photosObj.isNull(getString(R.string.google_response_param_key_photo_reference))
                                        ? photosObj.getString(getString(R.string.google_response_param_key_photo_reference)).trim() : "";
//                                Log.i("photo_reference", "photo_reference" + photo_reference);
                                restovo.setPhotoReference("https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + widthScreen + "&photoreference="
                                        + photo_reference + "&key=AIzaSyCwErSCCBZN2fL0y9znT3wCY7qrhBxd0JI");
                            }

                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");
                            restovo.setRatings(!data.isNull(getString(R.string.google_response_param_key_ratings))
                                    ? data.getString(getString(R.string.google_response_param_key_ratings)).trim() : "");
                            JSONArray types = !data.isNull("types") ? data.getJSONArray("types") : new JSONArray();

                            restovo.setReference(!data.isNull(getString(R.string.google_response_param_key_reference))
                                    ? data.getString(getString(R.string.google_response_param_key_reference)).trim() : "");

                            Location locationA = new Location("point A");

                            locationA.setLatitude(Double.valueOf(restovo.getLatitude()));
                            locationA.setLongitude(Double.valueOf(restovo.getLongitude()));

                            Location locationB = new Location("point B");

                            locationB.setLatitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")));
                            locationB.setLongitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")));

                            float distance = locationA.distanceTo(locationB);
                            Log.i("distnce", "" + distance);

                            // Set distance in URL parameter
                            restovo.setUrl(String.valueOf(distance).trim().replaceAll(",", "."));

                            boolean isRestoVoExists = false;

                            if (distance > Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MIN_DISTANCE, "1")) * 1000
                                    && Float.valueOf(restovo.getRatings().isEmpty() ? "0" : restovo.getRatings()) >=
                                    Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_PREFER_RATING, "0"))) {
                                for (int j = 0; j < listRestaurant.size(); j++) {
                                    if (listRestaurant.get(j).getPlaceid().trim().equalsIgnoreCase(restovo.getPlaceid().trim())) {
                                        isRestoVoExists = true;
                                    }
                                }

                                if (!isRestoVoExists) {
                                    if (types.toString().contains("lodging") || types.toString().contains("spa")) {

                                    } else {
                                        listRestaurant.add(restovo);
                                    }
                                }
                            }

                            if (Results.length() > 0 && !strPageTokenRestaurantList.trim().isEmpty()) {
                                isEndOfResult = false;
                            } else {
                                isEndOfResult = true;
                            }

//                        int posNew = -1;
//                        for (int i = 0; i < listRestaurant.size(); i++) {
//                            if (listRestaurant.get(i).getTimeToReach().trim().isEmpty()) {
//                                posNew = i;
//                                break;
//                            }
//                        }
//                        if (posNew != posRestaurantList && posNew != -1) {
//                            posRestaurantList = posNew;
//                            GetTimeMeasureBetweenCoOrdinates(posRestaurantList);
//                        }
                        }

                        if (adapterResto != null) {
                            adapterResto.notifyItemRangeInserted(size, listRestaurant.size());
                        }

                        setAdapterRestaurantListGoogle();
                        stopProgress();
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(getString(R.string.some_error_occured));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(e.getMessage());
                }

            } else if (action == Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    linearLayoutAcceptReject.setVisibility(View.GONE);
                    relativeChatFooter.setVisibility(View.VISIBLE);

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {

                        }

                        isAlreadyFriend = true;
                        stopProgress();

                        linearChatRequest.setVisibility(View.GONE);
                        linearChatFriends.setVisibility(View.VISIBLE);

                        callFriendsListApi();
                        /*showSnackBarMessageOnly(context.getString(R.string.you_are_now_friends));*/

                        setAdapterChatsList();
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {

                        }

                        stopProgress();
//                        showSnackBarMessageOnly(context.getString(R.string.friend_request_rejected));

                       /* sendLocalBroadcastManagerFriendRequestAction(2);*/

                        isAlreadyFriend = false;
                        callFriendsListApi();
//                        intent = new Intent(context, ChatHomeActivity.class);
                        /*intent = new Intent(context, ChatListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                        /*supportFinishAfterTransition();*/
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == ACTION_FRIENDS_LIST_API) {
                Log.d(TAG, "RESPONSE_FRIEND_LIST_API_ACCEPT_FRIEND_REQUEST: " + response);

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                // Fetch all the match details from Shared Preferences here.
                JSONObject jsonObjectResponse = null;
                listUsers = new ArrayList<>();
                try {
                    jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                            ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                    Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                    JSONObject settings = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_settings))
                            ? jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings)) : new JSONObject();
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        JSONArray data = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();

                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            ChatListVO item = new ChatListVO();
                            try {
                                dataInner = data.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String matchId = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            String friend1 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend1))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend1)).trim() : "";
                            String friend2 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend2))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend2)).trim() : "";
                            String statusFriend = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";
                            item.setMatchid(matchId);
                            item.setFriend1(friend1);
                            item.setFriend2(friend2);

                            JSONArray details = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_details))
                                    ? dataInner.getJSONArray(context.getResources().getString(R.string.api_response_param_key_details)) : new JSONArray();
                            for (int j = 0; j < details.length(); j++) {

                                JSONObject detailInner = details.getJSONObject(j);
                                String userId = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                String first_name = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_first_name))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_first_name)).trim() : "";
                                String lastName = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_last_name))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_last_name)).trim() : "";
                                String profilePic = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                String gender = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_gender))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_gender)).trim() : "";
                                item.setUserId(userId);
                                item.setFirstName(first_name);
                                item.setLastName(lastName);
                                item.setProfilePic(profilePic);
                                item.setGender(gender);
                                item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                            }
                            listUsers.add(item);

                        }

                        // Get users VO object here to pass to chat screen using intent.
                        int posUser = -1;
                        for (int i = 0; i < listUsers.size(); i++) {
                            Log.d(TAG, "PosSelected Id: " + otherUserID.trim() + ", Match List Id: " + listUsers.get(i).getUserId().trim());
                            if (otherUserID.trim().equalsIgnoreCase(listUsers.get(i).getUserId().trim())) {
                                posUser = i;
                                chatSelected = listUsers.get(i);
                            }
                        }

                        if (posUser != -1) {
                            stopProgress();
                            initLayout();
                        } else {
                            stopProgress();
                            initLayout();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));

                    initLayout();
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));

                    initLayout();
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST) {
                Log.d(TAG, "RESPONSE_FRIEND_LIST_API: " + response);

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                if (isAlreadyFriend) {
                    sendLocalBroadcastManagerFriendRequestAction(1);
                } else {
                    sendLocalBroadcastManagerFriendRequestAction(2);
                    /*intent = new Intent(context, ChatListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);*/
                    supportFinishAfterTransition();
                }
            } else if (action == Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION) {
                Log.d(TAG, "RESPONSE_SEND_NOTIFICATION_API: " + response);

            }
        }
    }

    public void callFriendsListApi() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, ChatDetailsActivity.this);
    }

    public void WriteNewUserStatus(String Status) {
        StatusVO status = new StatusVO();

        Calendar c = Calendar.getInstance();
//
//        TimeZone z = c.getTimeZone();
//        int offset = z.getRawOffset();
//        if (z.inDaylightTime(new Date())) {
//            offset = offset + z.getDSTSavings();
//        }
//        int offsetHrs = offset / 1000 / 60 / 60;
//        int offsetMins = offset / 1000 / 60 % 60;
//
////                                System.out.println("offset: " + offsetHrs);
////                                System.out.println("offset: " + offsetMins);
//        Log.d(TAG, "Datetime: " + c.getTime() + ", Offset Hours: " + offsetHrs + ", Mins: " + offsetMins + ", Current Timestamp: " + c.getTimeInMillis());
//
//        c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
//        c.add(Calendar.MINUTE, (-offsetMins));
//        Log.d(TAG, "Datetime after offset: " + c.getTime() + ", Timestamp: " + c.getTimeInMillis());

        long timestamp = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
        status.setLastSeen(String.valueOf(timestamp));
        if (chatSelected.getFriend2().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
            status.setRecieverId(chatSelected.getFriend1().trim());
        } else {
            status.setRecieverId(chatSelected.getFriend2().trim());
        }
        status.setStatus(Status);
        status.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        mDatabaseStatus.child(chatSelected.getMatchid().trim()).child(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")).setValue(status);

    }

    public void WriteNewUserLastSeen(String time) {
        StatusVO status = new StatusVO();

        status.setLastSeen(time);
        if (chatSelected.getFriend2().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
            status.setRecieverId(chatSelected.getFriend1().trim());
        } else {
            status.setRecieverId(chatSelected.getFriend2().trim());
        }
        status.setStatus(getString(R.string.status_offline));
        status.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        mDatabaseStatus.child(chatSelected.getMatchid().trim()).child(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")).setValue(status);

    }

    private void WriteMessageText(String msg) {
        Calendar c = Calendar.getInstance();
        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(msg);
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("text");
//        message.setRestoVO(null);

        String key = mDatabase.child("posts").push().getKey();
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child(key).setValue(message);

        // Add key here
        message.setKey(key);

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);

        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                listUsers.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
            }
        }

        setAdapterChatsList();

        // Send chat message notification here.
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_sendnotification));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_chatid), chatSelected.getMatchid().trim());
            params.put(getString(R.string.api_param_key_messageid), key);
            params.put(getString(R.string.api_param_key_message), msg);
            params.put(getString(R.string.api_param_key_receiverid), message.getRecieverId().trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION, ChatDetailsActivity.this);
        }
    }

    public void WriteMessageResto(RestaurantListVO RestoListVO) {
        /*TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        long timestamp = Constants.convertTimestampTo10DigitsOnly(calendar.getTimeInMillis(), true);*/
        // Convert current time to UTC or GMT
        Calendar c = Calendar.getInstance();

//        TimeZone z = c.getTimeZone();
//        int offset = z.getRawOffset();
//        if (z.inDaylightTime(new Date())) {
//            offset = offset + z.getDSTSavings();
//        }
//        int offsetHrs = offset / 1000 / 60 / 60;
//        int offsetMins = offset / 1000 / 60 % 60;
//
////                                System.out.println("offset: " + offsetHrs);
////                                System.out.println("offset: " + offsetMins);
//        Log.d(TAG, "Datetime: " + c.getTime() + ", Offset Hours: " + offsetHrs + ", Mins: " + offsetMins + ", Current Timestamp: " + c.getTimeInMillis());
//
//        c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
//        c.add(Calendar.MINUTE, (-offsetMins));
//        Log.d(TAG, "Datetime after offset: " + c.getTime() + ", Timestamp: " + c.getTimeInMillis());

        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(getString(R.string.sent_you_a_restaurant));
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("restaurant");
        message.setRestoVo(RestoListVO);

        String key = mDatabase.child("posts").push().getKey();
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child(key).setValue(message);

        // Add key here
        message.setKey(key);

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);

        for (int i = 0; i < listUsers.size(); i++) {
            if (listUsers.get(i).getIsSelected().trim().equalsIgnoreCase("1")) {
                listUsers.get(i).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true));
            }
        }

        setAdapterChatsList();

        // Send chat message notification here.
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_sendnotification));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_chatid), chatSelected.getMatchid().trim());
            params.put(getString(R.string.api_param_key_messageid), key);
            params.put(getString(R.string.api_param_key_message), message.getMsg());
            params.put(getString(R.string.api_param_key_receiverid), message.getRecieverId().trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION, ChatDetailsActivity.this);
        }
    }

    private void WriteMessageImage(String url) {
        /*TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        long timestamp = Constants.convertTimestampTo10DigitsOnly(calendar.getTimeInMillis(), true);*/
        // Convert current time to UTC or GMT
        Calendar c = Calendar.getInstance();

//        TimeZone z = c.getTimeZone();
//        int offset = z.getRawOffset();
//        if (z.inDaylightTime(new Date())) {
//            offset = offset + z.getDSTSavings();
//        }
//        int offsetHrs = offset / 1000 / 60 / 60;
//        int offsetMins = offset / 1000 / 60 % 60;
//
////                                System.out.println("offset: " + offsetHrs);
////                                System.out.println("offset: " + offsetMins);
//        Log.d(TAG, "Datetime: " + c.getTime() + ", Offset Hours: " + offsetHrs + ", Mins: " + offsetMins + ", Current Timestamp: " + c.getTimeInMillis());
//
//        c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
//        c.add(Calendar.MINUTE, (-offsetMins));
//        Log.d(TAG, "Datetime after offset: " + c.getTime() + ", Timestamp: " + c.getTimeInMillis());

        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(getString(R.string.sent_you_an_image));
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("image");
        message.setImgUrl(url);
//        message.setRestoVO(null);

        String key = mDatabase.child("posts").push().getKey();
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
//        mDatabaseMessages.child(chatVo.getMatchId()).child(key).setValue(message);
        mDatabaseMessages.child(chatSelected.getMatchid().trim()).child(key).setValue(message);

        // Add key here
        message.setKey(key);

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);
        setAdapterChatsList();

        // Send chat message notification here.
        if (NetworkUtil.isOnline(context)) {
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_sendnotification));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_chatid), chatSelected.getMatchid().trim());
            params.put(getString(R.string.api_param_key_messageid), key);
            params.put(getString(R.string.api_param_key_message), message.getMsg());
            params.put(getString(R.string.api_param_key_receiverid), message.getRecieverId().trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_CHAT_SEND_NOTIFICATION, ChatDetailsActivity.this);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStop() {
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        Calendar c = Calendar.getInstance();
        long timestamp = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
        WriteNewUserLastSeen(String.valueOf(timestamp));
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        Constants.CheckOtherUSerOnline = "-1";

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

        Calendar c = Calendar.getInstance();
        long timestamp = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
        WriteNewUserLastSeen(String.valueOf(timestamp));

        try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(ChatDetailsActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onDestroy();
        }
    }

    @Override
    public void onPause() {

        isOnInit = false;
        Calendar c = Calendar.getInstance();
//
//        TimeZone z = c.getTimeZone();
//        int offset = z.getRawOffset();
//        if (z.inDaylightTime(new Date())) {
//            offset = offset + z.getDSTSavings();
//        }
//        int offsetHrs = offset / 1000 / 60 / 60;
//        int offsetMins = offset / 1000 / 60 % 60;
//
////                                System.out.println("offset: " + offsetHrs);
////                                System.out.println("offset: " + offsetMins);
//        Log.d(TAG, "Datetime: " + c.getTime() + ", Offset Hours: " + offsetHrs + ", Mins: " + offsetMins + ", Current Timestamp: " + c.getTimeInMillis());
//
//        c.add(Calendar.HOUR_OF_DAY, (-offsetHrs));
//        c.add(Calendar.MINUTE, (-offsetMins));
//        Log.d(TAG, "Datetime after offset: " + c.getTime() + ", Timestamp: " + c.getTimeInMillis());

        long timestamp = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
        WriteNewUserLastSeen(String.valueOf(timestamp));
        Constants.CheckOtherUSerOnline = "-1";

        /*try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(ChatDetailsActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {*/
        super.onPause();
       /* }*/
    }


    @Override
    public void onResume() {
        isOnInit = false;
        StatusVO status = new StatusVO();
        long timestamp = Constants.convertTimestampTo10DigitsOnly(System.currentTimeMillis(), true);
        status.setLastSeen(String.valueOf(timestamp));
        if (chatSelected.getFriend2().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
            status.setRecieverId(chatSelected.getFriend1().trim());
        } else {
            status.setRecieverId(chatSelected.getFriend2().trim());
        }
        status.setStatus(getString(R.string.status_online));
        status.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        mDatabaseStatus.child(chatSelected.getMatchid().trim()).child(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")).setValue(status);
        Constants.CheckOtherUSerOnline = chatSelected.getFriend1();

        mNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                Log.d(TAG, "Notification register onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

               /* SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, "");
                SharedPreferenceUtil.save();

                // Clear all the notifications in notification bar
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();*/

                if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                    UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                    if (!selectedFriendId.trim().isEmpty()) {
                        if (user.getId().trim().equalsIgnoreCase(selectedFriendId.trim())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    isAlreadyFriend = true;

                                    linearChatRequest.setVisibility(View.GONE);
                                    linearChatFriends.setVisibility(View.VISIBLE);

                                    chatSelected.setStatus("1");
                                    setAdapterChatsList();
                                }
                            });
                        }
                    } else {
                        if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                            if (user.getId().trim().equalsIgnoreCase(chatSelected.getFriend2().trim())) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        isAlreadyFriend = true;

                                        linearChatRequest.setVisibility(View.GONE);
                                        linearChatFriends.setVisibility(View.VISIBLE);

                                        chatSelected.setStatus("1");
                                        setAdapterChatsList();
                                    }
                                });
                            }
                        } else {
                            if (user.getId().trim().equalsIgnoreCase(chatSelected.getFriend1().trim())) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        isAlreadyFriend = true;

                                        linearChatRequest.setVisibility(View.GONE);
                                        linearChatFriends.setVisibility(View.VISIBLE);

                                        chatSelected.setStatus("1");
                                        setAdapterChatsList();
                                    }
                                });
                            }
                        }
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(ChatDetailsActivity.this).registerReceiver(mNotificationReceiver, new IntentFilter(Constants.FILTER_NOTIFICATION_RECEIVED));
        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("refreshUI"));
            mIsReceiverRegistered = true;
        }
        super.onResume();
    }

    public String getDateCurrentTimeZone(long timestamp) {
        String lastseen = "";
        try {
            /*Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("EEE HH:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);*/

            long timestampConverted = Constants.convertTimestampTo10DigitsOnly(timestamp, false);

            // Check if timezone is in one hour, show relative time span
            Calendar c = Calendar.getInstance();
            Calendar calStartToday = Calendar.getInstance();
            calStartToday.set(Calendar.HOUR_OF_DAY, 0);
            calStartToday.set(Calendar.MINUTE, 0);
            calStartToday.set(Calendar.SECOND, 0);
            calStartToday.set(Calendar.MILLISECOND, 0);

            if (c.getTimeInMillis() - timestampConverted <= (1000 * 60 * 60)) {
                lastseen = DateUtils.getRelativeTimeSpanString(timestampConverted, new Date().getTime(),
                        DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            } else {
                if (timestampConverted >= calStartToday.getTimeInMillis()) {
                    lastseen = new SimpleDateFormat("hh:mm aa").format(new Date(timestampConverted));
                } else {
                    lastseen = new SimpleDateFormat("EEE hh:mm aa").format(new Date(timestampConverted));
                }
//            lastseen = new SimpleDateFormat()
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastseen;
    }

    public void updateChatList(final String matchId) {

        listenerChat = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            /*for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                // TODO: handle the chat message here.
                Log.d("CHAT_DETAILS_ACTIVITY", "Child Added Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                try {

                    Gson gson = new Gson();
                    String s1 = gson.toJson(dataSnapshot.getValue());
                    Log.d(TAG, "Message snapshot converted to JSON: " + s1.toString().trim());

                    msg = dataSnapshot.getValue(MessageVO.class);
//                    Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                        msg.setKey(dataSnapshot.getKey().trim());
//                                if(msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID,"")) && msg.getDelivered().toString().equalsIgnoreCase("") && msg.getDelivered().trim().isEmpty())
//                                {
//                                    Calendar c = Calendar.getInstance();
//                                    long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
//                                    mDatabaseMessages.child(chatVo.getMatchId().trim()).child
//                                            (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
//                                }

                                    /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").equalsIgnoreCase(msg.getSenderId())) {*/
                        if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                            String currentSenderId =
                                    !chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                            ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                            if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                int updatePosition = -1;
                                for (int i = 0; i < listChats.size(); i++) {
                                    if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                        updatePosition = i;
                                    }
                                }

                                if (updatePosition != -1) {
                                    listChats.set(updatePosition, msg);
                                } else {
                                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                        listChats.add(0, msg);
                                    }
                                }
                            }
                        } else {
                            int updatePosition = -1;
                            for (int i = 0; i < listChats.size(); i++) {
                                if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                    updatePosition = i;
                                }
                            }

                            if (updatePosition != -1) {
                                listChats.set(updatePosition, msg);
                            } else {
                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    listChats.add(0, msg);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                            /*}*/

                setAdapterChatsList();

                if (rvChats != null) {
                    rvChats.smoothScrollToPosition(0);
                    rvChats.scrollToPosition(0);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                           /* for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                // TODO: handle the chat message here.
                Log.d("CHAT_DETAILS_ACTIVITY", "Child Changed Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                try {
                    Calendar c = Calendar.getInstance();
                    long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
//                    if(msg.getDelivered().equalsIgnoreCase(""))
//                    mDatabaseMessages.child(chatSelected.getMatchid().trim()).child
//                            (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                    msg = dataSnapshot.getValue(MessageVO.class);

                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
//                        Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                        msg.setKey(dataSnapshot.getKey().trim());

                        if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                            String currentSenderId =
                                    !chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                            ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                            if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                int updatePosition = -1;
                                for (int i = 0; i < listChats.size(); i++) {
                                    if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                        updatePosition = i;
                                    }
                                }

                                if (updatePosition != -1) {
                                    listChats.set(updatePosition, msg);
                                } else {
                                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                        listChats.add(0, msg);
                                    }
                                }
                            }
                        } else {
                            int updatePosition = -1;
                            for (int i = 0; i < listChats.size(); i++) {
                                if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                    updatePosition = i;
                                }
                            }

                            if (updatePosition != -1) {
                                listChats.set(updatePosition, msg);
                            } else {
                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    listChats.add(0, msg);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                            /*}*/

                setAdapterChatsList();

                if (rvChats != null) {
                    rvChats.smoothScrollToPosition(0);
                    rvChats.scrollToPosition(0);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        listenerStatus = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                try {
                    StatusVO status = dataSnapshot.getValue(StatusVO.class);

                    if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {

                        if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                            if (!status.getLastSeen().trim().isEmpty()) {
                                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                    textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                    textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                            + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                } else {
                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                }
                            } else {
                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                            }
//                            textViewToolbarBaseLastseen.setText("last seen " + new SimpleDateFormat("hh:mm aa").format(new Date(Long.valueOf(status.getLastSeen()))));

//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                            SharedPreferenceUtil.save();
                        } else {
                            if (status.getStatus().trim().isEmpty()) {
                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                            } else {
                                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                    textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                    textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                } else {
                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                }
                            }
//                            textViewToolbarBaseLastseen.setText(status.getStatus());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                            SharedPreferenceUtil.save();
                        }
                    }
                    Log.i(status.getStatus(), "status");
                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                            /*}*/

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());

                try {

                    StatusVO status = dataSnapshot.getValue(StatusVO.class);

                    if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {

                        if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                            if (!status.getLastSeen().trim().isEmpty()) {
                                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                    textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                    textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                            + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                } else {
                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                }
                            } else {
                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                            }
//                            textViewToolbarBaseLastseen.setText("last seen " + new SimpleDateFormat("hh:mm aa").format(new Date(Long.valueOf(status.getLastSeen()))));
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                            SharedPreferenceUtil.save();
                        } else {
                            if (status.getStatus().trim().isEmpty()) {
                                textViewToolbarBaseLastseen.setVisibility(View.GONE);
                            } else {
                                if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                    textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                    textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                } else {
                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                }
                            }
//                            textViewToolbarBaseLastseen.setText(status.getStatus());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                            SharedPreferenceUtil.save();
                        }
                    }
                    Log.i(status.getStatus(), "status");
                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        Log.i(TAG, "MatchId" + matchId + ", Friend1: " + chatSelected.getFriend1() + ", Friend2: " + chatSelected.getFriend2());
        mDatabaseMessages.child(matchId).addChildEventListener(listenerChat);
//        mDatabaseStatus.child(matchId).addChildEventListener(listenerStatus);
        if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend2()).addChildEventListener(listenerStatus);
        } else {
            mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend1()).addChildEventListener(listenerStatus);
        }

    }

    private void openUploadImageFromDeviceIntent() {
//        http://stackoverflow.com/questions/4455558/allow-user-to-select-camera-or-gallery-for-image
        /*// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        root.mkdirs();

        // Generate File name here
        String strDate = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)).format(new Date());

        final String fname = "IMG_" + strDate + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);*/

        String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        Log.d("CAMERA_PICTURE_FILE_NAME", "" + fileName);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        Constants.outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Constants.outputFileUri);
            cameraIntents.add(intent);
        }

//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        if (accessToken != null) {
//            Intent intent = new Intent(context, FacebookAlbumsListActivity.class);
//            Log.i("FAcebookAccesstoken", "Found");
////        intent.setPackage("Facebook");
////        intent.setComponent(new ComponentName("FACEBOOK", "FACEBOOK"));
//            cameraIntents.add(intent);
//        } else {
//            Log.i("FAcebookAccesstoken", "NotFound");
//        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.upload_image_using));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        // Facebook Image Intent

        startActivityForResult(chooserIntent, Constants.REQUEST_CODE_SELECT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SELECT_PICTURE) {
                FacebookPhotoVO photoFb = null;
                boolean isCamera;
                boolean isFacebook = false;
//                if (data == null) {
                if (data != null && data.hasExtra(Constants.INTENT_PARAM_FB_ALBUM)) {
                    isCamera = false;
                    isFacebook = true;

                    photoFb = (FacebookPhotoVO) data.getSerializableExtra(Constants.INTENT_PARAM_FB_ALBUM);
                } else {
                    isFacebook = false;
                    if (data == null || data.getData() == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }
                }

                Uri selectedImageUri;
                if (isFacebook) {
                    // Update selected image from facebook to local profile object.
                    // Get image url here
                    String imageUrlFb = "";
                    for (int i = 0; i < photoFb.getImages().size(); i++) {
                        if (imageUrlFb.trim().isEmpty()) {
                            imageUrlFb = photoFb.getImages().get(i).getSource().trim();
                            break;
                        } else {
                            break;
                        }
                    }

                    if (imageUrlFb.trim().isEmpty()) {
                        imageUrlFb = photoFb.getPicture().trim();
                    }

                    Log.d("IMAGE_URL_FROM_FACEBOOK", "" + imageUrlFb);

                } else if (isCamera) {
                    selectedImageUri = Constants.outputFileUri;
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Camera result.");
                    String[] projection = {MediaStore.Images.Media.DATA};
                   /* Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null);
                    int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    String picturePath = cursor.getString(column_index_data);*/
                    Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
                    String timestamp = chatSelected.getMatchid() + Calendar.getInstance().getTimeInMillis();
                    addDummyChatNode(timestamp);
                    uploadPicToFirebase(picturePath, timestamp);
//                    decodeFile(picturePath, 1);

                } else {
                    selectedImageUri = data == null ? null : data.getData();
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Gallery result.");
                    try {
                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
                        String timestamp = chatSelected.getMatchid() + Calendar.getInstance().getTimeInMillis();
                        addDummyChatNode(timestamp);
                        uploadPicToFirebase(picturePath, timestamp);
//                        decodeFile(picturePath, 1);

                         /* Bitmap bitmap = BitmapFactory.decodeFile(selectedImageUri.getPath());*/

//                        showProgress(getString(R.string.loading));
//                        ImageUploadApi(picturePath);
//                        callOtherImageUploadAPI(picturePath,Constants.flagImageUpload);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == Constants.ACTION_CODE_SEND_NEW_REQUEST) {
                // Save result intent to object for onActivityResult to corresponding activity.
                intentDataOnActivityResult = data;

                // Update Chat screen as sent request is successful.
                mAuth = FirebaseAuth.getInstance();
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
                mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");

//        Constants.CheckOtherUSerOnline =chatVo.getId();

                if (data.hasExtra(Constants.INTENT_CHAT_LIST)) {
                    listUsers = (ArrayList<ChatListVO>) data.getSerializableExtra(Constants.INTENT_CHAT_LIST);
                    if (data.hasExtra(Constants.INTENT_CHAT_SELECTED)) {
                        chatSelected = (ChatListVO) data.getSerializableExtra(Constants.INTENT_CHAT_SELECTED);

                        if (!chatSelected.getProfilePic().trim().isEmpty()) {
                            Picasso.with(context).load(chatSelected.getProfilePic().trim()).resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48)).centerCrop()
//                            .placeholder(R.drawable.ic_user_profile_avatar).error(R.drawable.ic_user_profile_avatar)
                                    .placeholder(chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                                            : R.drawable.ic_placeholder_user_avatar_female_54)
                                    .error(chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                                            : R.drawable.ic_placeholder_user_avatar_female_54)
                                    .into(imageViewToolbarChatActivityUserProfilePic, new Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d(TAG, "Error loading profile pic of user: " + chatSelected.getProfilePic().trim());
                                        }
                                    });
                        } else {
                    /*imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_user_profile_avatar);*/
                            if (chatSelected.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                                imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_male_54);
                            } else {
                                imageViewToolbarChatActivityUserProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_female_54);
                            }

                        }

                        name = chatSelected.getFirstName() + " " + chatSelected.getLastName();
                        textViewToolbarBaseActivityTitle.setText(name);
                        updateChatList(chatSelected.getMatchid());
                        for (int i = 0; i < listUsers.size(); i++) {
                            if (listUsers.get(i).getMatchid().equalsIgnoreCase(chatSelected.getMatchid())) {
                                listUsers.get(i).setIsSelected("1");
                            } else {
                                listUsers.get(i).setIsSelected("0");
                            }
                        }

                        // set selected friend id here.
                        if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                            // User is sender in the chat
                            selectedFriendId = chatSelected.getFriend2().trim();
                        } else {
                            // User is receiver in the chat
                            selectedFriendId = chatSelected.getFriend1().trim();
                        }

                        // Check if both users are friend or not. If not, check current user is sender or receiver in the chat.
                        if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                            isAlreadyFriend = true;

                            linearChatRequest.setVisibility(View.GONE);
                            linearChatFriends.setVisibility(View.VISIBLE);
                        } else {
                            isAlreadyFriend = false;

                            if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                // User is sender in the chat
                                isReceiver = false;
                            } else {
                                // User is receiver in the chat
                                isReceiver = true;
                            }

                            /*linearChatRequest.setVisibility(View.VISIBLE);
                            linearChatFriends.setVisibility(View.GONE);*/
                        }
                    }
                } else {

                }

                setAdapterUsersList();
                setAdapterChatsList();

                mAuth = FirebaseAuth.getInstance();
                mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
                mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");

                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        if (user != null) {
                            Log.d(TAG, "onAuthStateChanged:UserNotNull");

                            listenerChat = new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            /*for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                                    // TODO: handle the chat message here.
                                    Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                                    try {
                                        msg = dataSnapshot.getValue(MessageVO.class);
//                                Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                                        if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                            msg.setKey(dataSnapshot.getKey().trim());
                                            if (msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
                                                    && msg.getDelivered().toString().trim().equalsIgnoreCase("") && msg.getDelivered().trim().isEmpty()) {
                                                Calendar c = Calendar.getInstance();
                                                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
                                                mDatabaseMessages.child(chatSelected.getMatchid().trim()).child
                                                        (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                                            }

                                    /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").equalsIgnoreCase(msg.getSenderId())) {*/
                                            if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                                String currentSenderId =
                                                        !chatSelected.getFriend1().trim().equalsIgnoreCase(
                                                                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                                                ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                                                if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                                    int updatePosition = -1;
                                                    for (int i = 0; i < listChats.size(); i++) {
                                                        if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                            updatePosition = i;
                                                        }
                                                    }

                                                    if (updatePosition != -1) {
                                                        listChats.set(updatePosition, msg);
                                                    } else {
                                                        if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                            listChats.add(0, msg);
                                                        }
                                                    }
                                                }
                                            } else {
                                                int updatePosition = -1;
                                                for (int i = 0; i < listChats.size(); i++) {
                                                    if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                        updatePosition = i;
                                                    }
                                                }

                                                if (updatePosition != -1) {
                                                    listChats.set(updatePosition, msg);
                                                } else {
                                                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                        listChats.add(0, msg);
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                            /*}*/

                                    setAdapterChatsList();
                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                           /* for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {*/
                                    // TODO: handle the chat message here.
                                    Log.d("CHAT_DETAILS_ACTIVITY", "Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                                    try {

                                        msg = dataSnapshot.getValue(MessageVO.class);
                                        if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                            if (msg.getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
                                                    && msg.getDelivered().toString().trim().equalsIgnoreCase("") && msg.getDelivered().trim().isEmpty()) {
                                                Calendar c = Calendar.getInstance();
                                                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                                if (msg.getDelivered().equalsIgnoreCase(""))
                                                    mDatabaseMessages.child(chatSelected.getMatchid().trim()).child
                                                            (msg.getKey()).child("delivered").setValue(String.valueOf(timestampToSave));
                                            }
                                        }
                                        if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
//                                    Log.d(TAG, "Message Time: "+new Date(msg.getTimeStamp()));
                                            msg.setKey(dataSnapshot.getKey().trim());

                                            if (msg.getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                                                String currentSenderId =
                                                        !chatSelected.getFriend1().trim().equalsIgnoreCase(
                                                                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                                                ? chatSelected.getFriend1().trim() : chatSelected.getFriend2().trim();
                                                if (msg.getSenderId().trim().equalsIgnoreCase(currentSenderId)) {
                                                    int updatePosition = -1;
                                                    for (int i = 0; i < listChats.size(); i++) {
                                                        if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                            updatePosition = i;
                                                        }
                                                    }

                                                    if (updatePosition != -1) {
                                                        listChats.set(updatePosition, msg);
                                                    } else {
                                                        if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                            listChats.add(0, msg);
                                                        }
                                                    }
                                                }
                                            } else {
                                                int updatePosition = -1;
                                                for (int i = 0; i < listChats.size(); i++) {
                                                    if (listChats.get(i).getKey().trim().equalsIgnoreCase(msg.getKey().trim())) {
                                                        updatePosition = i;
                                                    }
                                                }

                                                if (updatePosition != -1) {
                                                    listChats.set(updatePosition, msg);
                                                } else {
                                                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                                        listChats.add(0, msg);
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                            /*}*/

                                    setAdapterChatsList();
                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            };

                            listenerStatus = new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    Log.d(TAG, "Status Listener ==> Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim()
                                            + ", Current User Id: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                                    try {
                                        StatusVO status = dataSnapshot.getValue(StatusVO.class);

                                        if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {
                                            Log.d(TAG, "Status of other user: " + status.getStatus());
                                            if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                                                if (!status.getLastSeen().trim().isEmpty()) {
                                                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                        textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
//                                            textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " " + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                                        textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                                                + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                                    } else {
                                                        textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                    }
                                                } else {
                                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                }

//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                            } else {
                                                if (status.getStatus().trim().isEmpty()) {
                                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                } else {
                                                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                        textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                        textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                                    } else {
                                                        textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                    }
                                                }

//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString().trim());
//                                        SharedPreferenceUtil.save();
                                            }
                                        } else {
                                            textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                        }

                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                            /*}*/

                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                    Log.d(TAG, "Status Listener ==> Message snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim()
                                            + ", Current User Id: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));

                                    try {

                                        StatusVO status = dataSnapshot.getValue(StatusVO.class);

                                        if (status.getSenderId() != SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")) {

                                            if (status.getStatus().equalsIgnoreCase(getString(R.string.status_offline))) {
                                                if (!status.getLastSeen().trim().isEmpty()) {
                                                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                        textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                        textViewToolbarBaseLastseen.setText(getString(R.string.last_seen) + " "
                                                                + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
                                                    } else {
                                                        textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                    }
                                                } else {
                                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                }
//                                        textViewToolbarBaseLastseen.setText("last seen " + getDateCurrentTimeZone(Long.valueOf(status.getLastSeen())));
//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                            } else {
                                                if (status.getStatus().trim().isEmpty()) {
                                                    textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                } else {
                                                    if (chatSelected != null && chatSelected.getStatus().trim().equalsIgnoreCase("1")) {
                                                        textViewToolbarBaseLastseen.setVisibility(View.VISIBLE);
                                                        textViewToolbarBaseLastseen.setText(status.getStatus().trim());
                                                    } else {
                                                        textViewToolbarBaseLastseen.setVisibility(View.GONE);
                                                    }
                                                }
//                                        textViewToolbarBaseLastseen.setText(status.getStatus());
//                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_STAUS, textViewToolbarBaseLastseen.getText().toString());
//                                        SharedPreferenceUtil.save();
                                            }
                                        }
                                        Log.i(status.getStatus(), "status");
                                    /*}*/
                                    /*adapter.notifyDataSetChanged();*/
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            };
                            Log.i(TAG, "Match Id: " + chatSelected.getMatchid() + ", Friend1: " + chatSelected.getFriend1() + ", Friend2: " + chatSelected.getFriend2());
                            mDatabaseMessages.child(chatSelected.getMatchid().trim()).addChildEventListener(listenerChat);

                            if (chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend2()).addChildEventListener(listenerStatus);
                            } else {
                                mDatabaseStatus.child(chatSelected.getMatchid().trim()).orderByKey().equalTo(chatSelected.getFriend1()).addChildEventListener(listenerStatus);
                            }
                        } else {
                            // User is signed out
                            Log.d(TAG, "onAuthStateChanged:signed_out");
                        }
                    }
                };

                if (mAuthListener == null) {
                    mAuth.addAuthStateListener(mAuthListener);
                }
            } else if (requestCode == 1) {
                RestaurantListVO result = (RestaurantListVO) data.getSerializableExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT);
                WriteMessageResto(result);
            } else if (requestCode == Constants.REQEUST_CODE_FROM_CHAT) {
                Log.d(TAG, "Friend request is accepted or rejected from details screen. Perform appropriate action here.");
                if (data.hasExtra("action") && data.getIntExtra("action", 0) == 1) {
                    Log.d(TAG, "Friend request is accepted.");

                    linearLayoutAcceptReject.setVisibility(View.GONE);
                    relativeChatFooter.setVisibility(View.VISIBLE);
                    linearChatRequest.setVisibility(View.GONE);
                    linearChatFriends.setVisibility(View.VISIBLE);

                    isAlreadyFriend = true;

                    setAdapterChatsList();
                    stopProgress();
                } else if (data.hasExtra("action") && data.getIntExtra("action", 0) == 2) {
                    Log.d(TAG, "Friend request is rejected.");
                    stopProgress();
                    isAlreadyFriend = false;
//                    intent = new Intent(context, ChatHomeActivity.class);
                   /* intent = new Intent(context, ChatListActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);*/
                    supportFinishAfterTransition();
                } else {
                    Log.d(TAG, "Unidentified action found for friend request from details screen.");
                }
            }
        } else if (resultCode == RESULT_CANCELED) {
            if (requestCode == Constants.ACTION_CODE_SEND_NEW_REQUEST) {
//                Log.d(TAG, "On back press result cancel.");
                intentDataOnActivityResult = null;
                /*onBackPressed();*/
                Intent intent = new Intent();
                if (chatSelected != null)
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
                setResult(RESULT_CANCELED, intent);
                supportFinishAfterTransition();
                overridePendingTransition(0, 0);
            } else if (requestCode == Constants.REQEUST_CODE_FROM_CHAT) {
                Log.d(TAG, "No action taken for friend request on details screen. Nothing happens.");
            }
        }
//        else if (resultCode == RESULT_CANCELED) {
//            // user cancelled Image capture
//            showSnackBarMessageOnly(getString(R.string.image_upload_cancelled));
//        }
    }

    private void uploadPicToFirebase(String path, String timeStamp) {
        StorageReference mStorageRef;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        Uri file = Uri.fromFile(new File(path));
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        StorageReference riversRef = mStorageRef.child("images/" + timeStamp);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = riversRef.putBytes(data);
        // Create file metadata including the content type
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setCustomMetadata(String.valueOf(bitmap.getHeight()), String.valueOf(bitmap.getWidth()))
                .build();
        // Update metadata properties
        mStorageRef.updateMetadata(metadata)
                .addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(StorageMetadata storageMetadata) {
                        // Updated metadata is in storageMetadata
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                    }
                });
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.d("downloadUrl", "downloadUrl" + downloadUrl);
                for (int i = 0; i < listChats.size(); i++) {
                    if (downloadUrl.toString().trim().contains(listChats.get(i).getImgUrl().trim())
                            && listChats.get(i).getContentType().trim().equalsIgnoreCase("dummy")) {
                        removeDummyView(i);
                    }

                }

                WriteMessageImage(downloadUrl.toString());
                Log.d("downloadUrl", "" + downloadUrl);
            }
        });
    }

    public void sortListNotifyChanges() {
        Collections.sort(listUsers, new ChatDetailsActivity.ComparatorLongDescending());
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        } else {
            adapter = new ChatUsersListAdapter(context, listUsers);
        }
    }

    public class ComparatorLongDescending implements Comparator<ChatListVO> {

        public int compare(ChatListVO first, ChatListVO second) {
//            Log.d("SORT_CHAT_LIST", "First: " + first.getFirstName() + " " + first.lastName + ", Timestamp: " + first.getTimestampLastSeen() + ", Second: " +
//                    second.getFirstName() + " " + second.lastName + ", Timestamp: " + second.getTimestampLastSeen());

//            long firstValue = first.getTimestampLastSeen();
            long firstValue = first.getTime();
            long secondValue = second.getTime();
            return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
        }
    }

    private void addDummyChatNode(String timestamp) {
        Calendar c = Calendar.getInstance();
        long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

        SharedPreferenceUtil.putValue(Constants.TIMESTAMP_LATEST_MESSAGE_RECEIVED, timestampToSave);
        SharedPreferenceUtil.save();

        MessageVO message = new MessageVO();
        message.setSenderId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        message.setMsg(getString(R.string.sent_you_an_image));
        if (chatSelected.getFriend1().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "")))
            message.setRecieverId(chatSelected.getFriend2());
        else
            message.setRecieverId(chatSelected.getFriend1());

        message.setSenderDp((SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")));
        message.setRecieverDP(chatSelected.getProfilePic());
        message.setTimeStamp(timestampToSave);
        message.setContentType("dummy");
        message.setImgUrl(timestamp);
//        message.setRestoVO(null);

        // Add key here

//        mDatabaseMessages.child(chatVo.getMatchId().trim()).child(key).child("key").setValue(key);
        listChats.add(0, message);
        setAdapterChatsList();

    }

    public void removeDummyView(int i) {
        if (listChats.size() > i) {
            listChats.remove(i);
            setAdapterChatsList();
        }
    }

    public void setAdapterRestaurantListGoogle() {
        if (adapterResto == null) {
            adapterResto = new RestaurantListAdapterMessage(context, listRestaurant);
            recyclerViewListResto.setAdapter(adapterResto);
        } else {
            adapterResto.notifyDataSetChanged();
        }
    }

    private boolean isStoragePermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;

            }
        } else {
            return true;
        }
    }

    private void checkReadWritePermissionGrantedOrNot() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        !shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                        !shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    showDialogAlertRequestPermissionReadStorage(getString(R.string.request_permission),
                            getString(R.string.storage_permission_is_required));
                    /*requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);*/
                    return;
                }

                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS);
            } else {
            }
        } else {
        }
    }

    protected void showDialogAlertRequestPermissionReadStorage(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS:
//                isPermissionDialogDisplayed = false;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    showSnackBarMessageOnly(getString(R.string.you_have_successfully_granted_required_permissions));
//                    Log.d(TAG, "Permission granted pos: " + 0);
                    openUploadImageFromDeviceIntent();
                } else {
                    // Permission Denied
                    showSnackBarMessageOnly(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showDialogSendMessage() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(
                Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);//set height(300) and width(match_parent) here, ie (width,height)

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dislogView = inflater.inflate(R.layout.layout_custom_dialog_send_message_request, null);

        TextView btnNext = (TextView) dislogView.findViewById(R.id.btnNext);
        final EditText edtTypeMessage = (EditText) dislogView.findViewById(R.id.editTextCustomDialogSendMessageRequest);
        final TextView txtCounter = (TextView) dislogView.findViewById(R.id.textViewCustomDialogSendMessageRequestCounts);
        ImageView imageViewClose = (ImageView) dislogView.findViewById(R.id.imageViewClose);

        edtTypeMessage.setSelection(edtTypeMessage.getText().toString().trim().length());
        txtCounter.setText(String.valueOf(edtTypeMessage.getText().toString().length()) + " " + getString(R.string.of_160_characters));

        edtTypeMessage.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //This sets a textview to the current length
                if (edtTypeMessage.getText().toString().trim().length() > 0) {
                    edtTypeMessage.setError(null);
                }

                txtCounter.setText(String.valueOf(edtTypeMessage.getText().toString().length()) + " " + getString(R.string.of_160_characters));
            }

            public void afterTextChanged(Editable s) {
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtTypeMessage.getText().toString().trim().isEmpty()) {
                    dialog.dismiss();

                    // Todo Forward user to restaurant list screen (specific screen for sending a request)
                    intent = new Intent(context, RestaurantListForSendRequestActivity.class);
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, chatSelected);
                    intent.putExtra(Constants.INTENT_CHAT_LIST, listUsers);
                    intent.putExtra(Constants.INTENT_CHAT_MESSAGE, edtTypeMessage.getText().toString().trim());
                    startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
                } else {
                    edtTypeMessage.setError(getString(R.string.please_type_a_message_first));
                }
            }
        });

        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(dislogView, dialogParams);
        dialog.show();
    }

    /**
     * Send local broadcast manager to other running activities to update the status of accepted or rejected friend request in the view, if the
     * match or friend is valid.
     *
     * @param action Action taken on friend request.
     *               1 ==> Friend request accepted.
     *               2 ==> Friend request rejected.
     */
    private void sendLocalBroadcastManagerFriendRequestAction(int action) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED);

        // Update current activity using Local Broadcast manager if needed.
        if (action == 1) { // Friend request accepted.

            i.putExtra("action", String.valueOf(action));
            i.putExtra("user", chatSelected);
            i.putExtra("otherUserId",
                    chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                            ? chatSelected.getFriend2().trim() : chatSelected.getFriend1().trim());
        } else if (action == 2) { // Friend request rejected.
            i.putExtra("action", String.valueOf(action));
            i.putExtra("user", chatSelected);
            i.putExtra("otherUserId",
                    chatSelected.getFriend1().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                            ? chatSelected.getFriend2().trim() : chatSelected.getFriend1().trim());
        }

        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }
}

