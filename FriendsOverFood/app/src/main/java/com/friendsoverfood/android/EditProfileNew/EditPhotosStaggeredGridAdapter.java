package com.friendsoverfood.android.EditProfileNew;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.DemoStaggeredGrid.AbstractDataProvider;
import com.friendsoverfood.android.DemoStaggeredGrid.DrawableUtils;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

/**
 * Created by Trushit on 13/07/17.
 */

public class EditPhotosStaggeredGridAdapter extends RecyclerView.Adapter<EditPhotosStaggeredGridAdapter.BaseViewHolder>
        implements DraggableItemAdapter<EditPhotosStaggeredGridAdapter.BaseViewHolder> {
    private static final String TAG = "MyDraggableItemAdapter";
    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_NORMAL_ITEM_OFFSET = 1;

    private static final boolean USE_DUMMY_HEADER = true;
    private static final boolean RANDOMIZE_ITEM_SIZE = false;

    public EditProfileNewActivity activity;
    public Context context;
    //    public ArrayList<HashMap<String, String>> listProfilePictures;
    public ArrayList<UserProfilePictureVO> listProfilePictures;

    // NOTE: Make accessible with short name
    private interface Draggable extends DraggableItemConstants {
    }

    private AbstractDataProvider mProvider;

    public static class BaseViewHolder extends AbstractDraggableItemViewHolder {
        public BaseViewHolder(View v) {
            super(v);
        }
    }

    public static class HeaderItemViewHolder extends BaseViewHolder {
        public HeaderItemViewHolder(View v) {
            super(v);
        }
    }

    public static class NormalItemViewHolder extends BaseViewHolder {
        public FrameLayout mContainer, frameRoot;
        public View mDragHandle;
        public ImageView imgUserProfile;
        public ImageView imgUserProfileAdd, imgUserProfileRemove;
//        public TextView mTextView;

        public NormalItemViewHolder(View v) {
            super(v);
            mContainer = (FrameLayout) v.findViewById(R.id.container);
            frameRoot = (FrameLayout) v.findViewById(R.id.frameLayoutRoot);
            mDragHandle = v.findViewById(R.id.drag_handle);
//            mTextView = (TextView) v.findViewById(android.R.id.text1);
            imgUserProfile = (ImageView) v.findViewById(R.id.imageViewEditProfileActivityUserImage);
            imgUserProfileAdd = (ImageView) v.findViewById(R.id.imageViewEditProfileActivityAddUserImage);
            imgUserProfileRemove = (ImageView) v.findViewById(R.id.imageViewEditProfileActivityRemoveUserImage);
        }
    }

    public EditPhotosStaggeredGridAdapter(Context context, ArrayList<UserProfilePictureVO> listProfilePictures, AbstractDataProvider dataProvider) {
        this.activity = (EditProfileNewActivity) context;
        this.context = context;
        this.listProfilePictures = listProfilePictures;
        mProvider = dataProvider;

        // DraggableItemAdapter requires stable ID, and also
        // have to implement the getItemId() method appropriately.
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        if (isHeader(position)) {
            return RecyclerView.NO_ID;
        } else {
            return mProvider.getItem(toNormalItemPosition(position)).getId();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return ITEM_VIEW_TYPE_HEADER;
        } else {
            return ITEM_VIEW_TYPE_NORMAL_ITEM_OFFSET + mProvider.getItem(toNormalItemPosition(position)).getViewType();
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ITEM_VIEW_TYPE_HEADER: {
                // NOTE:
                // This dummy header item is required to workaround the
                // weired animation when occurs on moving the item 0
                //
                // Related issue
                //   Issue 99047:	Inconsistent behavior produced by mAdapter.notifyItemMoved(indexA,indexB);
                //   https://code.google.com/p/android/issues/detail?id=99047&q=notifyItemMoved&colspec=ID%20Status%20Priority%20Owner%20Summary%20Stars%20Reporter%20Opened&
                final View v = inflater.inflate(R.layout.dummy_header_item, parent, false);
                return new HeaderItemViewHolder(v);
            }
            default: {
//                final View v = inflater.inflate(R.layout.list_grid_item, parent, false);
                final View v = inflater.inflate(R.layout.layout_adapter_edit_photos, parent, false);
                return new NormalItemViewHolder(v);
            }
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (isHeader(position)) {
            onBindHeaderViewHolder((HeaderItemViewHolder) holder, position);
        } else {
            onBindNormalItemViewHolder((NormalItemViewHolder) holder, toNormalItemPosition(position));
        }
    }

    private void onBindHeaderViewHolder(HeaderItemViewHolder holder, int position) {
        StaggeredGridLayoutManager.LayoutParams lp = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
        lp.setFullSpan(true);
    }

    private void onBindNormalItemViewHolder(NormalItemViewHolder holder, int position) {

        final AbstractDataProvider.Data item = mProvider.getItem(position);

        int sizeImage = 0;
        if (position == 0) {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
    /*
     * to make View to occupy full width of the parent
     */
            layoutParams.setFullSpan(true);

            holder.imgUserProfileAdd.setImageResource(R.drawable.ic_add_photo_big);
            holder.imgUserProfileRemove.setImageResource(R.drawable.ic_close_grey_circle_big);
            sizeImage = activity.widthScreen - Constants.convertDpToPixels(16);

//            Log.d(TAG, "Width of element: " + layoutParams.width);
        } else {
            StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
    /*
     * to make View to occupy full width of the parent
     */
            layoutParams.setFullSpan(false);

            holder.imgUserProfileAdd.setImageResource(R.drawable.ic_add_photo_small);
            holder.imgUserProfileRemove.setImageResource(R.drawable.ic_close_grey_circle_small);
            sizeImage = (activity.widthScreen - Constants.convertDpToPixels(32)) / 4;

//            Log.d(TAG, "Width of element: " + layoutParams.width);
        }

        // set text
       /* holder.mTextView.setText(item.getText());*/
        final NormalItemViewHolder holderFinal = holder;
        final int pos = position;
        if (listProfilePictures.get(position).getImageUrl().trim().isEmpty()) {
            holder.imgUserProfileAdd.setVisibility(View.VISIBLE);
            holder.imgUserProfileRemove.setVisibility(View.GONE);
            holder.imgUserProfile.setImageResource(0);
        } else {
            holder.imgUserProfileAdd.setVisibility(View.GONE);
            holder.imgUserProfileRemove.setVisibility(View.GONE);

            /*if(pos == 0){
                try{
                    ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                    // path to /data/data/yourapp/app_data/imageDir
                    File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                    // Create imageDir
                    File mypath = new File(directory, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1 + ".png");

                    if(mypath != null && mypath.exists()) {
                        Log.d(TAG, "File loaded from local data store: "+mypath.getAbsolutePath().trim());
                        Picasso.with(context).load(mypath).resize(sizeImage, sizeImage).centerCrop()
                                .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                                .into(holder.imgUserProfile, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        holderFinal.imgUserProfileAdd.setVisibility(View.GONE);
                                        holderFinal.imgUserProfileRemove.setVisibility(View.VISIBLE);
                                        Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).get("url").trim());
                                    }

                                    @Override
                                    public void onError() {
                                        holderFinal.imgUserProfileAdd.setVisibility(View.VISIBLE);
                                        Log.d(TAG, "Error loading restaurant pic for Card: " + listProfilePictures.get(pos).get("url").trim());
                                    }
                                });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else {*/
            if (listProfilePictures.get(position).getImageBitmap() != null) {
                Log.d(TAG, "File path from local storage: " + listProfilePictures.get(position).getImagePath() + ", Position: " + position);

                /*Uri uriFileImage = Uri.parse(listProfilePictures.get(position).get("path").trim());*/
//                Picasso.with(context).load(new File(listProfilePictures.get(position).get("path").trim())).resize(sizeImage, sizeImage).centerCrop()
                holderFinal.imgUserProfileAdd.setVisibility(View.GONE);
//                holderFinal.imgUserProfileRemove.setVisibility(View.VISIBLE);
                holderFinal.imgUserProfileRemove.setVisibility(View.GONE);
                holder.imgUserProfile.setImageBitmap(listProfilePictures.get(position).getImageBitmap());
                /*Picasso.with(context).load(new File(listProfilePictures.get(position).get("path").trim())).resize(sizeImage, sizeImage).centerCrop()
                        .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                        .into(holder.imgUserProfile, new Callback() {
                            @Override
                            public void onSuccess() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.GONE);
                                holderFinal.imgUserProfileRemove.setVisibility(View.VISIBLE);
                                Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).get("path").trim());
                            }

                            @Override
                            public void onError() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.VISIBLE);
                                Log.d(TAG, "Error loading restaurant pic for Card: " + listProfilePictures.get(pos).get("path").trim());
                            }
                        });*/
            } else if (!listProfilePictures.get(position).getImagePath().trim().isEmpty()) {
                Log.d(TAG, "File path from local storage: " + listProfilePictures.get(position).getImagePath() + ", Position: " + position);

                Picasso.with(context).load(new File(listProfilePictures.get(position).getImagePath().trim())).resize(sizeImage, sizeImage).centerCrop()
                        .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                        .into(holder.imgUserProfile, new Callback() {
                            @Override
                            public void onSuccess() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.GONE);
//                                holderFinal.imgUserProfileRemove.setVisibility(View.VISIBLE);
                                holderFinal.imgUserProfileRemove.setVisibility(View.GONE);
                                Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).getImagePath().trim());
                            }

                            @Override
                            public void onError() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.VISIBLE);
                                Log.d(TAG, "Error loading restaurant pic for Card: " + listProfilePictures.get(pos).getImagePath().trim());
                            }
                        });
            } else {
                Picasso.with(context).load(listProfilePictures.get(position).getImageUrl().trim()).resize(sizeImage, sizeImage).centerCrop()
                        .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                        .into(holder.imgUserProfile, new Callback() {
                            @Override
                            public void onSuccess() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.GONE);
//                                holderFinal.imgUserProfileRemove.setVisibility(View.VISIBLE);
                                holderFinal.imgUserProfileRemove.setVisibility(View.GONE);
                                Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).getImageUrl().trim());
                            }

                            @Override
                            public void onError() {
                                holderFinal.imgUserProfileAdd.setVisibility(View.VISIBLE);
                                Log.d(TAG, "Error loading restaurant pic for Card: " + listProfilePictures.get(pos).getImageUrl().trim());
                            }
                        });
            }
            /*}*/
        }

        // set item view height
        Context context = holder.itemView.getContext();
        int itemHeight = calcItemHeight(context, item, activity, position);
        ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if (lp.height != itemHeight) {
            lp.height = itemHeight;
            holder.itemView.setLayoutParams(lp);
        }

        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();

        if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
//                bgResId = R.drawable.bg_item_dragging_active_state;
                bgResId = R.color.transparent;

                // need to clear drawable state here to get correct appearance of the dragging item.
                DrawableUtils.clearState(holder.mContainer.getForeground());
            } else if ((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) {
//                bgResId = R.drawable.bg_item_dragging_state;
                bgResId = R.color.transparent;
            } else {
//                bgResId = R.drawable.bg_item_normal_state;
                bgResId = R.color.transparent;
            }

            holder.mContainer.setBackgroundResource(bgResId);
        }


        holder.imgUserProfileAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.imgForUploadSelected = holderFinal.imgUserProfile; // imageViewEditProfileActivityUserImage2
                activity.imgAddButtonForUploadSelected = holderFinal.imgUserProfileAdd; // imageViewEditProfileActivityAddUserImage2
                activity.imgRemoveButtonForUploadSelected = holderFinal.imgUserProfileRemove;
                Constants.flagImageUpload = pos + 1;
                activity.isFromPlus = true;

                activity.isRemoveToDisplay = false;
                /*activity.openUploadImageFromDeviceIntent();*/
                activity.dialogForUploadOption(false);
            }
        });

        holder.imgUserProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.imgForUploadSelected = holderFinal.imgUserProfile; // imageViewEditProfileActivityUserImage2
                activity.imgAddButtonForUploadSelected = holderFinal.imgUserProfileAdd; // imageViewEditProfileActivityAddUserImage2
                activity.imgRemoveButtonForUploadSelected = holderFinal.imgUserProfileRemove;
                Constants.flagImageUpload = pos + 1;
                activity.isFromPlus = true;
                /*activity.openUploadImageFromDeviceIntent();*/
                activity.isRemoveToDisplay = true;
                activity.dialogForUploadOption(true);
            }
        });

        holder.imgUserProfileRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.imgForUploadSelected = holderFinal.imgUserProfile; // imageViewEditProfileActivityUserImage2
                activity.imgAddButtonForUploadSelected = holderFinal.imgUserProfileAdd; // imageViewEditProfileActivityAddUserImage2
                activity.imgRemoveButtonForUploadSelected = holderFinal.imgUserProfileRemove;
                Constants.flagImageUpload = pos + 1;
                activity.isFromPlus = true;
                activity.openConfirmDeleteDialog();
            }
        });
    }

    @Override
    public int getItemCount() {
        int headerCount = getHeaderItemCount();
        int count = mProvider.getCount();
        return headerCount + count;
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        Log.d(TAG, "onMoveItem(fromPosition = " + fromPosition + ", toPosition = " + toPosition + ")");

        if (fromPosition == toPosition) {
            return;
        }

        fromPosition = toNormalItemPosition(fromPosition);
        toPosition = toNormalItemPosition(toPosition);

        mProvider.moveItem(fromPosition, toPosition);

//        int from = fromPosition;
//        int to = toPosition;

        // Swap bitmap here.
        if (toPosition > fromPosition) {
            // Rename all the file names between these positions in loop below.
            String fileNameTempTo = listProfilePictures.get(toPosition).getImageName().trim();
            String fileNameTempFrom = listProfilePictures.get(fromPosition).getImageName().trim();
//            String fileNameTempNext = "";
            for (int i = fromPosition; i < toPosition; i++) {
                if (i == fromPosition) {
                    /*try {
                        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                        // path to /data/data/yourapp/app_data/imageDir
                        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                        // Create imageDir
                        File fileFrom = new File(directory, listProfilePictures.get(i).get("name").trim() + ".png");
                        File fileTo = new File(directory, fileNameTempTo + ".png");

                        if (fileFrom.exists()) {
                            boolean isRenameDone = fileFrom.renameTo(fileTo);
                            Log.d(TAG, "File rename from " + fileFrom.getAbsolutePath() + " to " + fileTo.getAbsolutePath() + " done: " + isRenameDone);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                } else {
                    try {
                        ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                        // path to /data/data/yourapp/app_data/imageDir
                        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                        // Create imageDir
                        File fileFrom = new File(directory, listProfilePictures.get(i + 1).getImageName().trim() + ".png");
                        File fileTo = new File(directory, listProfilePictures.get(i).getImageName().trim() + ".png");

                        if (fileFrom.exists()) {
                            boolean isRenameDone = fileFrom.renameTo(fileTo);
                            Log.d(TAG, "File rename from " + fileFrom.getAbsolutePath() + " to " + fileTo.getAbsolutePath() + " done: " + isRenameDone);
                        } else {
                            /*if(fileTo.exists()){
                                fileTo.delete();
                            }*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                // Create imageDir
                File fileFrom = new File(directory, fileNameTempFrom + ".png");
                File fileTo = new File(directory, fileNameTempTo + ".png");

                if (fileFrom.exists()) {
                    boolean isRenameDone = fileFrom.renameTo(fileTo);
                    Log.d(TAG, "File rename from " + fileFrom.getAbsolutePath() + " to " + fileTo.getAbsolutePath() + " done: " + isRenameDone);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (toPosition < fromPosition) {
            // Rename all the file names between these positions in loop below.
            String fileNameTempTo = listProfilePictures.get(toPosition).getImageName().trim();
            String fileNameTempFrom = listProfilePictures.get(fromPosition).getImageName().trim();
            Bitmap bitmapFrom = activity.loadImageBitmapFromInternalAppStorage(listProfilePictures.get(fromPosition).getImageName().trim());
            for (int i = fromPosition; i > toPosition; i--) {
                try {
                    ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                    // path to /data/data/yourapp/app_data/imageDir
                    File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                    // Create imageDir
                    File fileFrom = new File(directory, listProfilePictures.get(i - 1).getImageName().trim() + ".png");
                    File fileTo = new File(directory, listProfilePictures.get(i).getImageName().trim() + ".png");

                    if (fileFrom.exists()) {
                        boolean isRenameDone = fileFrom.renameTo(fileTo);
                        Log.d(TAG, "File rename from " + fileFrom.getAbsolutePath() + " to " + fileTo.getAbsolutePath() + " done: " + isRenameDone);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                /*ContextWrapper cw = new ContextWrapper(activity.getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                // Create imageDir
                File fileFrom = new File(directory, fileNameTempFrom + ".png");
                File fileTo = new File(directory, fileNameTempTo + ".png");

                if (fileFrom.exists()) {
                    boolean isRenameDone = fileFrom.renameTo(fileTo);
                    Log.d(TAG, "File rename from " + fileFrom.getAbsolutePath() + " to " + fileTo.getAbsolutePath() + " done: " + isRenameDone);
                }*/
                Log.d(TAG, "File rename saving bitmap of file: " + fileNameTempFrom + " to: " + fileNameTempTo);


                activity.saveImageToInternalAppStorage(bitmapFrom, fileNameTempTo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (toPosition < fromPosition) {
//            listProfilePictures.add(toPosition, listProfilePictures.get(fromPosition));
//            notifyItemInserted(toPosition);
//            listProfilePictures.remove(fromPosition+1);
//            notifyItemRemoved(fromPosition+1);
           /* HashMap<String, String> data = new HashMap<>();
            data.put("url", listProfilePictures.get(fromPosition).get("url").trim());
            data.put("path", listProfilePictures.get(toPosition).get("path").trim());
            data.put("name", listProfilePictures.get(toPosition).get("name").trim());
            listProfilePictures.add(toPosition, data);
//            listProfilePictures.add(toPosition, listProfilePictures.get(fromPosition));
            notifyItemInserted(toPosition);
            listProfilePictures.remove(fromPosition + 1);
            notifyItemRemoved(fromPosition+1);*/

            UserProfilePictureVO dataTempFrom = listProfilePictures.get(fromPosition);
            UserProfilePictureVO dataTempTo = listProfilePictures.get(toPosition);

            for (int i = toPosition + 1; i <= fromPosition; i++) {
                UserProfilePictureVO dataInner = new UserProfilePictureVO();
                dataInner.setImageUrl(listProfilePictures.get(i).getImageUrl().trim());
                dataInner.setImageBitmap(listProfilePictures.get(i).getImageBitmap());
                if (i == 1) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1).trim());
                } else if (i == 2) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2).trim());
                } else if (i == 3) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3).trim());

                } else if (i == 4) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4).trim());

                } else if (i == 5) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5).trim());

                }else if (i == 6) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6).trim());

                }else if (i == 7) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7).trim());

                }else if (i == 8) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8).trim());

                }else if (i == 9) {
                    dataInner.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim());
                    dataInner.setImagePath(activity.getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9).trim());

                }
                listProfilePictures.set(i, dataInner);
            }

            // Set from position object to to position
            dataTempFrom.setImagePath(dataTempTo.getImagePath().trim());
            dataTempFrom.setImageName(dataTempTo.getImageName().trim());
            listProfilePictures.add(toPosition, dataTempFrom);
            notifyItemInserted(toPosition);
            listProfilePictures.remove(fromPosition + 1);
            notifyItemRemoved(fromPosition+1);

            /*HashMap<String, String> dataTempTo = listProfilePictures.get(toPosition);
            HashMap<String, String> dataTempFrom = listProfilePictures.get(fromPosition);
            for (int i = fromPosition; i > toPosition; i--) {
                try {
                    // Create imageDir
                    HashMap<String, String> dataFrom = new HashMap<>();
                    dataFrom.put("url", listProfilePictures.get(i - 1).get("url"));
                    dataFrom.put("name", listProfilePictures.get(i).get("name"));
                    dataFrom.put("path", listProfilePictures.get(i).get("path"));
                    listProfilePictures.set(i, dataFrom);
//                    HashMap<String, String> dataTo = listProfilePictures.get(i);

//                    listProfilePictures.add(i, dataFrom);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Move from item from temp to to position
            HashMap<String, String> dataLast = new HashMap<>();
            dataLast.put("name", dataTempTo.get("name").trim());
            dataLast.put("path", dataTempTo.get("path").trim());
            dataLast.put("url", dataTempFrom.get("url").trim());
            listProfilePictures.set(toPosition, dataLast);*/
        } else {
            listProfilePictures.add(toPosition + 1, listProfilePictures.get(fromPosition));
            notifyItemInserted(toPosition + 1);
            listProfilePictures.remove(fromPosition);
            notifyItemRemoved(fromPosition);
            /*HashMap<String, String> dataTempTo = listProfilePictures.get(toPosition);
            HashMap<String, String> dataTempFrom = listProfilePictures.get(fromPosition);
            listProfilePictures.set(toPosition + 1, dataTempFrom);
            listProfilePictures.remove(fromPosition);*/

//            listProfilePictures.set(toPosition+1, dataTempTo);
        }

//        notifyItemMoved(fromPosition, toPosition);
        notifyDataSetChanged();

        // Delete files from local if not applicable or not exists.
        /*for (int i = 0; i < listProfilePictures.size(); i++) {

        }*/

//        activity.showProgress(context.getResources().getString(R.string.loading));
        for (int i = 0; i < listProfilePictures.size(); i++) {
            if (i == 0) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, listProfilePictures.get(i).getImageUrl().trim());
            } else if (i == 1) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, listProfilePictures.get(i).getImageUrl().trim());
            } else if (i == 2) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, listProfilePictures.get(i).getImageUrl().trim());
            } else if (i == 3) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, listProfilePictures.get(i).getImageUrl().trim());
            } else if (i == 4) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, listProfilePictures.get(i).getImageUrl().trim());
            } else if (i == 5) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, listProfilePictures.get(i).getImageUrl().trim());
            }else if (i == 6) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, listProfilePictures.get(i).getImageUrl().trim());
            }else if (i == 7) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, listProfilePictures.get(i).getImageUrl().trim());
            }else if (i == 9) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, listProfilePictures.get(i).getImageUrl().trim());
            }
        }
        SharedPreferenceUtil.save();

        activity.currentItemInViewPager = 1;
        activity.ApiEditProfileAddRemoveProfilePicture();
    }

    @Override
    public boolean onCheckCanStartDrag(BaseViewHolder holder, int position, int x, int y) {
        if (isHeader(position)) {
            return false;
        } else {
            /*if (listProfilePictures.get(position).get("url").trim().isEmpty()) {
                return false;
            }else{*/
            return false;
            /*}*/
        }
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(BaseViewHolder holder, int position) {
        int headerCount = getHeaderItemCount();
        return new ItemDraggableRange(headerCount, getItemCount() - 1);
    }

    @Override
    public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
        return true;
    }

    static int getHeaderItemCount() {
        return (USE_DUMMY_HEADER) ? 1 : 0;
    }

    static boolean isHeader(int position) {
        return (position < getHeaderItemCount());
    }

    static int toNormalItemPosition(int position) {
        return position - getHeaderItemCount();
    }

    static int calcItemHeight(Context context, AbstractDataProvider.Data item, EditProfileNewActivity activity, int position) {
        float density = context.getResources().getDisplayMetrics().density;
        if (RANDOMIZE_ITEM_SIZE) {
            int s = (int) item.getId();
            s = swapBit(s, 0, 8);
            s = swapBit(s, 1, 5);
            s = swapBit(s, 3, 2);
            return (int) ((8 + (s % 13)) * 10 * density);
        } else {
            /*return (int) (100 * density);*/
            if (position == 0) {
                return activity.widthScreen - Constants.convertDpToPixels(16);
            } else {
                return (activity.widthScreen - Constants.convertDpToPixels(32)) / 4;
            }
           /*int sizeImage = activity.widthScreen - Constants.convertDpToPixels(16);

            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(sizeImage, sizeImage);
            holder.frameRoot.setLayoutParams(lp);*/
        }
    }

    static int swapBit(int x, int pos1, int pos2) {
        int m1 = 1 << pos1;
        int m2 = 1 << pos2;
        int y = x & ~(m1 | m2);

        if ((x & m1) != 0) {
            y |= m2;
        }
        if ((x & m2) != 0) {
            y |= m1;
        }

        return y;
    }
}
