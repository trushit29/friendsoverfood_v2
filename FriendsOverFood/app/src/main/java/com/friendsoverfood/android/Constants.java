package com.friendsoverfood.android;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@SuppressWarnings("ALL")
public class Constants {

    public final static String API_REQUEST_HEADER_KEY_COOKIE = "Cookie", API_REQUEST_HEADER_KEY_RESPONSETYPE = "responsetype",
            API_REQUEST_HEADER_VALUE_RESPONSETYPE = "api";
    // Request codes for app, start from 1001
    public static final String
            IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN",
            LOGGED_IN_USER_CURRENT_LATITUDE = "LOGGED_IN_USER_CURRENT_LATITUDE",
            LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE = "LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE",
            LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE = "LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE",
            LOGGED_IN_USER_CURRENT_LONGITUDE = "LOGGED_IN_USER_CURRENT_LONGITUDE",
            LOGGED_IN_USER_PREFERED_UNIT = "LOGGED_IN_USER_PREFERED_UNIT",
            LOGGED_IN_USER_SEARCH_DISTANCE = "LOGGED_IN_USER_SEARCH_DISTANCE",
            LOGGED_IN_USER_USER_ID = "LOGGED_IN_USER_USER_ID",
            LOGGED_IN_USER_INTRESTED_IN = "LOGGED_IN_USER_INTRESTED_IN",
            LOGGED_IN_USER_ACCESS_TOKEN = "LOGGED_IN_USER_ACCESS_TOKEN",
            LOGGED_IN_USER_FIRST_NAME = "LOGGED_IN_USER_FIRST_NAME",
            LOGGED_IN_USER_LAST_NAME = "LOGGED_IN_USER_LAST_NAME",
            LOGGED_IN_USER_IS_ACTIVE = "LOGGED_IN_USER_IS_ACTIVE",
            LOGGED_IN_USER_SESSION_ID = "LOGGED_IN_USER_SESSION_ID",
            LOGGED_IN_USER_NAME = "LOGGED_IN_USER_NAME",
            LOGGED_IN_USER_EMAIL = "LOGGED_IN_USER_EMAIL",
            LOGGED_IN_USER_GENDER = "LOGGED_IN_USER_GENDER",
            LOGGED_IN_USER_DOB = "LOGGED_IN_USER_DOB",
            LOGGED_IN_USER_DEVICETYPE = "LOGGED_IN_USER_DEVICETYPE",
            LOGGED_IN_USER_DEVICETOKEN = "LOGGED_IN_USER_DEVICETOKEN",
            LOGGED_IN_USER_CREATEDAT = "LOGGED_IN_USER_CREATEDAT",
            LOGGED_IN_USER_UPDATEDAT = "LOGGED_IN_USER_UPDATEDAT",
            LOGGED_IN_USER_ACCOUNT_TYPE = "LOGGED_IN_USER_ACCOUNT_TYPE",
            LOGGED_IN_USER_OCCUPATION = "LOGGED_IN_USER_OCCUPATION",
            LOGGED_IN_USER_EMPLOYER = "LOGGED_IN_USER_EMPLOYER",
            LOGGED_IN_USER_ETHNICITY = "LOGGED_IN_USER_ETHNICITY",
            LOGGED_IN_USER_EDUCATION = "LOGGED_IN_USER_EDUCATION",
            LOGGED_IN_USER_LIKE = "LOGGED_IN_USER_LIKE",
            LOGGED_IN_USER_PREFER = "LOGGED_IN_USER_PREFER",
    //            LOGGED_IN_USER_RELIGION = "LOGGED_IN_USER_RELIGION",
    //            LOGGED_IN_USER_ABOUT = "LOGGED_IN_USER_ABOUT",
    LOGGED_IN_USER_SOCIAL_ID = "LOGGED_IN_USER_SOCIAL_ID",
            LOGGED_IN_USER_RELATIONSHIP = "LOGGED_IN_USER_RELATIONSHIP",
            LOGGED_IN_USER_MOBILE = "LOGGED_IN_USER_MOBILE",
            LOGGED_IN_USER_ABOUT_ME = "LOGGED_IN_USER_ABOUT_ME",
            LOGGED_IN_USER_SHOWME = "LOGGED_IN_USER_SHOWME",
            LOGGED_IN_USER_SEARCH_MIN_AGE = "LOGGED_IN_USER_SEARCH_MIN_AGE",
            LOGGED_IN_USER_SEARCH_MAX_AGE = "LOGGED_IN_USER_SEARCH_MAX_AGE",
            LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS = "LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS",
            LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS = "LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS",
            LOGGED_IN_USER_IS_SHOW_LOCATION = "LOGGED_IN_USER_IS_SHOW_LOCATION",
            LOGGED_IN_USER_DISTANCE_UNIT = "LOGGED_IN_USER_DISTANCE_UNIT",
            LOGGED_IN_USER_PROFILEPIC_SOCIAL = "LOGGED_IN_USER_PROFILEPIC_SOCIAL",
            LOGGED_IN_USER_PROFILEPIC1 = "LOGGED_IN_USER_PROFILEPIC1",
            LOGGED_IN_USER_PROFILEPIC2 = "LOGGED_IN_USER_PROFILEPIC2",
            LOGGED_IN_USER_PROFILEPIC3 = "LOGGED_IN_USER_PROFILEPIC3",
            LOGGED_IN_USER_PROFILEPIC4 = "LOGGED_IN_USER_PROFILEPIC4",
            LOGGED_IN_USER_PROFILEPIC5 = "LOGGED_IN_USER_PROFILEPIC5", LOGGED_IN_USER_TASTEBUDS = "LOGGED_IN_USER_TASTEBUDS",
            LOGGED_IN_USER_ISREGISTERED = "LOGGED_IN_USER_ISREGISTERED",
            USER_RESTO_FILTER_PREFER_RATING = "USER_RESTO_FILTER_PREFER_RATING",
            USER_RESTO_FILTER_MAX_DISTANCE_KM = "USER_RESTO_FILTER_MAX_DISTANCE_KM",
            USER_RESTO_FILTER_MIN_DISTANCE = "USER_RESTO_FILTER_MIN_DISTANCE",
            LOGGED_IN_USER_LOCATION_STRING = "LOGGED_IN_USER_LOCATION_STRING",
            LOGGED_IN_USER_CHANGED_LOCATION_STRING = "LOGGED_IN_USER_CHANGED_LOCATION_STRING",
            LOGGED_IN_USER_FRIEND_LIST = "LOGGED_IN_USER_FRIEND_LIST",
            LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY = "LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY",
            LOGGED_IN_USER_REFER_CODE = "LOGGED_IN_USER_REFER_CODE", LOGGED_IN_USER_REFERENCE = "LOGGED_IN_USER_REFERENCE",
            LOGGED_IN_USER_PROFILEPIC6 = "LOGGED_IN_USER_PROFILEPIC6", LOGGED_IN_USER_PROFILEPIC7 = "LOGGED_IN_USER_PROFILEPIC7",
            LOGGED_IN_USER_PROFILEPIC8 = "LOGGED_IN_USER_PROFILEPIC8", LOGGED_IN_USER_PROFILEPIC9 = "LOGGED_IN_USER_PROFILEPIC9";
//    LOGGED_IN_USER_STAUS = "LOGGED_IN_USER_STAUS",

    public static final long TIMER_ALARM_SERVICE = 1000 * 60 * 60 * 4; // 4 hours
    public static final int NOTIFICATION_ID = 121;
    public final static String TIMESTAMP_LATEST_MESSAGE_RECEIVED = "TIMESTAMP_LATEST_MESSAGE_RECEIVED";
    public static String NOTIFICATION_MESSAGES = "notificationMessages";

    public static int flagImageUpload = 1;
    public static Uri outputFileUri;

    //            LOGGED_IN_USER_ = "LOGGED_IN_USER_";
    public static final String GCM_PUSH_COUNT = "GCM_PUSH_COUNT";

    public static final String KEY_INTENT_EXTRA_RESTAURANT = "KEY_INTENT_EXTRA_RESTAURANT", KEY_INTENT_EXTRA_DATETIME = "KEY_INTENT_EXTRA_DATETIME",
            KEY_INTENT_EXTRA_PEOPLE = "KEY_INTENT_EXTRA_PEOPLE", INTENT_PARAM_FB_ALBUM = "Album", INTENT_USER_ID = "INTENT_USER_ID",
            INTENT_CHAT_SELECTED = "INTENT_CHAT_SELECTED", INTENT_CHAT_LIST = "INTENT_CHAT_LIST", INTENT_USER_LOGGED_IN = "INTENT_USER_LOGGED_IN",
            INTENT_CHAT_MESSAGE = "INTENT_CHAT_MESSAGE", INTENT_CHAT_SEND_NEW_REQUEST = "INTENT_CHAT_SEND_NEW_REQUEST",
            INTENT_USER_DETAILS = "INTENT_USER_DETAILS";

    public final static String FILTER_NOTIFICATION_RECEIVED = "com.friendsoverfood.android.FILTER_NOTIFICATION_RECEIVED",
            FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED = "com.friendsoverfood.android.FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED";

    // Permission model request codes
    public static final int REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS = 601, REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION = 602,
            REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS = 603, REQUEST_CODE_ASK_FOR_CAMERA_PERMISSIONS = 604;

    //ALL API ACTION CODE
    public static final int ACTION_CODE_API_FACEBOOK_SIGN_IN = 201,
            ACTION_CODE_API_GOOGLE_SIGN_IN = 202,
            ACTION_CODE_API_LOGIN_DB = 203,
            ACTION_CODE_API_USERS_ME = 204,
            ACTION_CODE_API_CUISINS = 205,
            ACTION_CODE_API_SUGESSION = 206,
            ACTION_CODE_API_LIKE = 207,
            ACTION_CODE_UPDATE_DEVICE_TOKEN = 208,
            ACTION_CODE_API_USERS = 210,
            ACTION_CODE_API_SETTINGS = 211,
            ACTION_CODE_EDIT_PROFILE = 212,
            ACTION_CODE_API_OTHER_USER_DETAIL = 213, ACTION_CODE_PROFILE = 214, ACTION_CODE_EDIT_PROFILE_ADD_REMOVE_PICTURE = 215,
            ACTION_CODE_API_CREATE_FIREBASE_MESSAGE = 216, ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN = 217,
            ACTION_CODE_API_ADDREFERENCE = 218,
            ACTION_CODE_API_EDIT_PROFILE_SIGN_IN = 250, ACTION_CODE_API_SIGN_UP = 251,
            ACTION_CODE_API_GET_ALL_TASTEBUDS = 252,
            ACTION_CODE_API_GET_USER_TASTEBUDS = 253, ACTION_CODE_API_EDIT_PROFILE_TASTEBUDS = 254, ACTION_CODE_API_USER_SUGGESTIONS = 255,
            ACTION_CODE_API_SEND_FRIEND_REQUEST = 256, ACTION_CODE_API_ACCEPT_FRIEND_REQUEST = 257, ACTION_CODE_API_REJECT_FRIEND_REQUEST = 258,
            ACTION_CODE_API_FRIEND_LIST = 259, ACTION_CODE_API_USER_PENDING_FRIEND_REQUESTS = 260, ACTION_CODE_API_FRIEND_LIST_UPDATE_ONLY = 261,
            ACTION_CODE_LOGOUT = 262, ACTION_CODE_PROFILE_LOGOUT = 263, ACTION_CODE_API_CHAT_SEND_NOTIFICATION = 264;
    public static String CheckOtherUSerOnline = "-1";

    public static final int REQUEST_CODE_PERMISSION_ALL = 300,
            REQUEST_CODE_PERMISSION_LOCATION = 301,
            REQUEST_CODE_SELECT_PICTURE = 302,
            REQUEST_CAMERA = 303,
            REQUEST_READ_EXTERNAL_STORAGE = 304,
            REQUEST_WRITE_EXTERNAL_STORAGE = 305;
    public static final int REQUEST_CODE_PROFILE = 400;

    public static final int ACTION_CODE_ZOMATO_API_SEARCH = 401,
            ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API = 402,
            ACTION_CODE_REVIEWES_API = 403,
            ACTION_CODE_GOOGLE_LIST_API = 404,
            ACTION_CODE_GOOGLE_DETAIL_API = 405;

    public static final String LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1 = "profilepic1", LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2 = "profilepic2",
            LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3 = "profilepic3", LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4 = "profilepic4",
            LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5 = "profilepic5", LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6 = "profilepic6",
            LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7 = "profilepic7", LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8 = "profilepic8",
            LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9 = "profilepic9";

    // Action codes for startActivityForResult, start from 500.
    public static final int ACTION_CODE_CHAT_USERS_LIST = 500, ACTION_CODE_SEND_NEW_REQUEST = 501, REQEUST_CODE_FROM_CHAT = 502;
    public static String INTENT_VIEW_PROFILE = "viewprofile", IS_FROM_NOTIFICATION = "IS_FROM_NOTIFICATION", NOTIFICATION_TYPE = "NOTIFICATION_TYPE",
            INTENT_MATCH_USER_VO = "INTENT_MATCH_USER_VO", INTENT_MATCH_CHAT_VO = "INTENT_MATCH_CHAT_VO", INTENT_REMOVE_IMAGE = "INTENT_REMOVE_IMAGE";

    @SuppressLint("SimpleDateFormat")
    public static String getSQLDateStringUtcTimezone() {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());
        // Log.i("FORMATED UTC DATETIME", "" + utcTime);
        return utcTime;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSystemDateStringFromUTCDateString(String strDate) {
        String strSystemDateTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date dtUtc = sdf.parse(strDate);
            sdf.setTimeZone(TimeZone.getDefault());
            strSystemDateTime = sdf.format(dtUtc);
            Log.d("DATETIME SYSTEM TIMEZONE", "" + strSystemDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strSystemDateTime;
    }

    @SuppressLint({"SimpleDateFormat", "DefaultLocale"})
    public static String getDateTimeFormattedHHmmWithDateFromTimestamp(String strSqlDateTime) {
        String strDateTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date dtUtc = sdf.parse(strSqlDateTime);
            sdf.setTimeZone(TimeZone.getDefault());
            strDateTime = sdf.format(dtUtc);

            Date dtTimeZone = sdf.parse(strDateTime);

            if (DateUtils.isToday(dtTimeZone.getTime())) {
                strDateTime = new SimpleDateFormat("hh:mm aa").format(dtTimeZone.getTime()).toUpperCase();
            } else {
                strDateTime = new SimpleDateFormat("hh:mm aa").format(dtTimeZone.getTime()).toUpperCase() + " on " + new SimpleDateFormat("MMM dd, yyyy").format(dtTimeZone.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDateTime;
    }

    public static long convertTimestampTo10DigitsOnly(long timestamp, boolean isToSeconds) {
        long converted = -1;
        try {
            /*int length = (int) (Math.log10(timestamp) + 1);
//            System.out.println("Length: " + length);
            if (length > 10) {
//                System.out.println("Timestamp: " + timestamp + ", POW: " + (Math.pow(10, (length - 10))));
                converted = timestamp / (long) (Math.pow(10, (length - 10)));
//                System.out.println("DATE: " + timestamp);
            } else if (length < 10) {
//                System.out.println("Timestamp: " + timestamp + ", POW: " + (Math.pow(10, (length - 10))))
                converted = timestamp * (long) (Math.pow(10, (10 - length)));
//                System.out.println("timestamp: " + timestamp);
            }*/
           /* long unix = Instant.ofEpochMilli(timestamp).getEpochSecond();
            System.out.println("UNIX: "+unix);*/

            if (isToSeconds) {
                converted = TimeUnit.MILLISECONDS.toSeconds(timestamp);
            } else {
                converted = TimeUnit.SECONDS.toMillis(timestamp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (converted == -1) {
                converted = timestamp;
            }
            Log.d("TIMESTAMP", "Timestamp converted from original timestamp: " + timestamp + " is: " + converted);
            return converted;
        }
    }

    public static String getCurrentTimezone() {
//		Calendar calendar = Calendar.getInstance();
//		String strTimezone = new SimpleDateFormat("ZZZZ").format(calendar.getTime());
//		Log.i("Time", "" + calendar.getTime());
//		Log.i("ZZZZ", "" + strTimezone.substring(0, 3).toLowerCase());
//		String time = new SimpleDateFormat("ZZ").format(calendar.getTime()).replaceAll(Pattern.quote("+"), "p").replaceAll(Pattern.quote("-"), "m");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("Z");
        String strTimezone = sdf.format(calendar.getTime()).trim().replaceAll(Pattern.quote("+"), "p").replaceAll(Pattern.quote("-"), "m");
//        Log.i("Time", "" + calendar.getTime());
        Log.i("Z", "" + strTimezone.trim());
        if (!strTimezone.startsWith("p") && !strTimezone.startsWith("m")) {
            if (strTimezone.trim().length() != 5) {
                strTimezone = "p0000";
            } else if (strTimezone.trim().length() == 4) {
                strTimezone = "p" + strTimezone.trim();
            } else {
                strTimezone = "p0000";
            }
        }

        String datetime = "gmt" + strTimezone.trim();
        Log.d("TIMEZONE STRING API ", "" + datetime);
        return datetime;

    }

    public static String getIntegerValue(String id) {
        DecimalFormat df = new DecimalFormat("#");
        String formatted = df.format(Double.parseDouble(id));
        System.out.println(formatted);
        Log.d("DECIMAL_VALUE", "" + formatted);

        return formatted;
    }

    public static Calendar stringToCalendarDate(String strEndDate) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(strEndDate));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cal;
    }

    public static int convertDpToPixels(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int convertPixelsToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
    /*public static boolean getCurrentActiveTaskFromActivityStack(Context context, String strCurrentActivityFullName) {
        String strCurrentActiveTask = "";
        boolean isCurrentActivityActive = false;
        ActivityManager mngr = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        strCurrentActiveTask = taskList.get(0).topActivity.getClassName().trim();
        Log.e("TOP ACTIVITY IN STACK", "" + strCurrentActiveTask);
        if (strCurrentActiveTask.trim().equalsIgnoreCase(strCurrentActivityFullName.trim())) {
            isCurrentActivityActive = true;
        }
        return isCurrentActivityActive;
    }*/

    public static String convertAgeFromDateOfBirth(String BirthDate) {
        if (BirthDate.isEmpty()) {
            return "";
        }

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        try {
            date = format.parse(BirthDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int year = date.getYear() + 1900;
        int month = date.getMonth();
        int day = date.getDay();
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public static boolean getCurrentActiveTaskFromActivityStack(Context context, String strCurrentActivityFullName) {
        String strCurrentActiveTask = "";
        boolean isCurrentActivityActive = false;
        ActivityManager mngr = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        strCurrentActiveTask = taskList.get(0).topActivity.getClassName().trim();
        Log.d("TOP_ACTIVITY_IN_STACK", "" + strCurrentActiveTask);
        if (strCurrentActiveTask.trim().equalsIgnoreCase(strCurrentActivityFullName.trim())) {
            isCurrentActivityActive = true;
        }
        return isCurrentActivityActive;
    }
}
