package com.friendsoverfood.android.RestaurantsList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

/**
 * Created by Trushit on 08/03/17.
 */

public class TimePickerVO implements Parcelable {

    public Calendar calendarTime = Calendar.getInstance();
    public String strTime = "";

    public TimePickerVO() {
    }

    protected TimePickerVO(Parcel in) {
        strTime = in.readString();
    }

    public static final Creator<TimePickerVO> CREATOR = new Creator<TimePickerVO>() {
        @Override
        public TimePickerVO createFromParcel(Parcel in) {
            return new TimePickerVO(in);
        }

        @Override
        public TimePickerVO[] newArray(int size) {
            return new TimePickerVO[size];
        }
    };

    public Calendar getCalendarTime() {
        return calendarTime;
    }

    public void setCalendarTime(Calendar calendarTime) {
        this.calendarTime.setTime(calendarTime.getTime());
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable((Calendar) calendarTime);
        dest.writeString(strTime);
    }
}
