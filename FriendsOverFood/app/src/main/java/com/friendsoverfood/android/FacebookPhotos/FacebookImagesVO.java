package com.friendsoverfood.android.FacebookPhotos;

import java.io.Serializable;

/**
 * Created by Trushit on 05/07/16.
 */
public class FacebookImagesVO implements Serializable {
    private long height = 0, width = 0;
    private String source = "";

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
