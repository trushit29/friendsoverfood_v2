package com.friendsoverfood.android.storage;

@SuppressWarnings("serial")
public class DbException extends Exception {

	public DbException(String message) {
		super(message);
	}
}
