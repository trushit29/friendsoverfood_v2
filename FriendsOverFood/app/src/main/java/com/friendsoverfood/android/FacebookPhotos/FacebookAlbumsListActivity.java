package com.friendsoverfood.android.FacebookPhotos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by Trushit on 04/07/16.
 */
public class FacebookAlbumsListActivity extends BaseActivity {

    private Context context;
    private View view;

    private final String TAG = "FACEBOOK_ALBUMS_LIST_ACTIVITY";
    private ListView listViewAlbums;
    private ArrayList<FacebookAlbumVO> listAlbums = new ArrayList<>();
    private FacebookAlbumsListAdapter adapter;
    private int posToScroll = 0;
    private LayoutInflater inflater;
    public TextView txtNoAlbums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListener();

        setVisibilityActionBar(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setTitleSupportActionBar(getString(R.string.upload_from_facebook));
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_facebook_albums_list_activity, getMiddleContent());

        listViewAlbums = (ListView) view.findViewById(R.id.listViewFacebookAlbumsListActivity);
        txtNoAlbums = (TextView) view.findViewById(R.id.textViewFacebookAlbumsActivity);

        // Call Graph API for albums here.
        callAlbumsAPI("");

    }

    private void callAlbumsAPI(String after) {
        showProgress(getString(R.string.loading));
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/me/albums", new GraphRequest.Callback() {
                @Override
                public void onCompleted(final GraphResponse response) {
                    Log.d("ALBUMS_RESPONSE", "" + response.toString().trim());
                    // Insert your code here
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                /*stopProgress();*/
                                JSONObject jsonObjResponse = response.getJSONObject();

                                JSONArray data = !jsonObjResponse.isNull("data") ? jsonObjResponse.getJSONArray("data") : new JSONArray();
                                JSONObject paging = !jsonObjResponse.isNull("paging") ? jsonObjResponse.getJSONObject("paging") : null;
                                for (int i = 0; i < data.length(); i++) {
                                    FacebookAlbumVO album = new FacebookAlbumVO();
                                    JSONObject objData = data.getJSONObject(i);
                                    album.setId(!objData.isNull("id") ? objData.getString("id").trim() : "");
                                    album.setName(!objData.isNull("name") ? objData.getString("name").trim() : "");
                                    album.setCount(!objData.isNull("count") ? objData.getLong("count") : 0);
                                    album.setPhoto_count(!objData.isNull("photo_count") ? objData.getLong("photo_count") : 0);

                                    JSONObject picture = !objData.isNull("picture") ? objData.getJSONObject("picture") : null;
                                    if (picture != null) {
                                        JSONObject dataPicture = !picture.isNull("data") ? picture.getJSONObject("data") : null;
                                        if (dataPicture != null) {
                                            album.setUrl(!dataPicture.isNull("url") ? dataPicture.getString("url").trim() : "");
                                        }
                                    }

                                    listAlbums.add(album);
                                }

                                // Set adapter here
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                    listViewAlbums.smoothScrollToPosition(posToScroll);

                                    if(listAlbums.size() == 0){
                                        txtNoAlbums.setVisibility(View.VISIBLE);
                                    }else{
                                        txtNoAlbums.setVisibility(View.GONE);
                                    }
                                } else {
                                    adapter = new FacebookAlbumsListAdapter(context, listAlbums);
                                    listViewAlbums.setAdapter(adapter);
                                    listViewAlbums.smoothScrollToPosition(posToScroll);

                                    if(listAlbums.size() == 0){
                                        txtNoAlbums.setVisibility(View.VISIBLE);
                                    }else{
                                        txtNoAlbums.setVisibility(View.GONE);
                                    }
                                }

                                if (paging != null) {
                                    String next = !paging.isNull("next") ? paging.getString("next").trim() : "";
                                    if (!next.trim().isEmpty()) {
                                        Uri uri = Uri.parse(next.trim());
                                        String after = uri.getQueryParameter("after").trim();
                                        if (after != null && !after.trim().isEmpty()) {
                                            callAlbumsAPI(after.trim());
                                        } else {
                                            stopProgress();
                                        }
                                    } else {
                                        stopProgress();
                                    }
                                } else {
                                    stopProgress();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
//                                onBackPressed();
                            } catch (Exception e) {
                                e.printStackTrace();
//                                onBackPressed();
                            }
                        }
                    });
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "photo_count,count,picture,name");
            if (!after.trim().isEmpty())
                parameters.putString("after", after.trim());
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setListener() {
        listViewAlbums.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                posToScroll = firstVisibleItem;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
      /*  MenuItem itemSave = menu.findItem(R.id.menu_item_toolbar_save);
        itemSave.setVisible(true);*/
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will automatically handle clicks on the Home/Up button, so long as you specify a parent activity in
        // AndroidManifest.xml.
        switch (item.getItemId()) {
          /*  case R.id.menu_item_toolbar_save:

                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
       /* objProfile.setAboutme(edtAboutUser.getText().toString().trim());
        objProfile.setGender(rbFemale.isChecked() ? getString(R.string.female_gender).trim() : getString(R.string.male_gender).trim());

        ProfileDAO.addOrUpdateUserToDb(context, objProfile);
        sendRequestEditProfile(true);*/
        intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        super.onBackPressed();
    }

   /* private void sendRequestGetProfile(boolean isShowRequest) {
        if (NetworkUtil.isOnline(context)) {
            if (isShowRequest)
                showProgress(getString(R.string.loading));
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_param_value_action_getprofile));
            params.put(getString(R.string.api_param_key_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ID, "").trim());
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSIONID, "").trim());
            params.put(getString(R.string.api_param_key_fields), "*");

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_GET_PROFILE, this);
        } else {
            showSnackBar(getString(R.string.internet_not_available));
        }
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null && action == Constants.ACTION_CODE_API_GET_PROFILE) {
            Log.i(TAG, "GET PROFILE RESPONSE: " + response.trim());
            try {
                final JSONObject jsonObjectResponse = new JSONObject(response);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject settings = jsonObjectResponse != null && !jsonObjectResponse.isNull(getString(R.string.key_response_param_settings))
                                    ? jsonObjectResponse.getJSONObject(getString(R.string.key_response_param_settings)) : null;

                            if (settings != null) {
                                boolean success = !settings.isNull(getString(R.string.key_response_param_success))
                                        ? settings.getBoolean(getString(R.string.key_response_param_success)) : false;

                                if (success) {
                                    JSONArray jsonArrayData = !jsonObjectResponse.isNull(getString(R.string.key_response_param_data))
                                            ? jsonObjectResponse.getJSONArray(getString(R.string.key_response_param_data)) : null;

                                } else {
                                    stopProgress();
                                    int errorCode = !settings.isNull(getString(R.string.key_response_param_errorcode))
                                            ? settings.getInt(getString(R.string.key_response_param_errorcode)) : 0;
                                    String errorMessage = !settings.isNull(getString(R.string.key_response_param_errormessage))
                                            ? settings.getString(getString(R.string.key_response_param_errormessage)) : "";
                                    Log.d("SIGN_UP_ERROR", "Error code: " + errorCode + ", Message: " + errorMessage);
                                    showSnackBar(errorMessage.trim());
                                }
                            } else {
                                stopProgress();
                                showSnackBar(getString(R.string.some_error_occured));
                            }
                        } catch (JSONException e) {
                            stopProgress();
                            e.printStackTrace();
                            showSnackBar(getString(R.string.some_error_occured));
                        }
                    }
                });
            } catch (JSONException e) {
                stopProgress();
                e.printStackTrace();
                showSnackBar(getString(R.string.some_error_occured));
            } catch (Exception e) {
                stopProgress();
                e.printStackTrace();
                showSnackBar(getString(R.string.some_error_occured));
            }
        } else if (response == null) {
            stopProgress();
            Log.d(TAG, "Null response received for action: " + action);
//            showSnackBar(getString(R.string.some_error_occured));
        }
        super.onResponse(response, action);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SELECT_PICTURE) {
               /* Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.INTENT_PARAM_FB_ALBUM, list.get(pos));
                Intent intent = new Intent();
                intent.putExtras(bundle);*/
                setResult(Activity.RESULT_OK, data);
                supportFinishAfterTransition();
            }
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled Image capture
//            showSnackBar(getString(R.string.image_upload_cancelled));
        }
    }

}