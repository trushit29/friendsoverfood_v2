package com.friendsoverfood.android.Home;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Trushit on 22/04/17.
 */

public class MutualFriendsParcelableVO implements Parcelable {

    public String id = "", first_name = "", last_name = "", social_id = "", profilepic1 = "", profilepic2 = "", profilepic3 = "", profilepic4 = "",
            profilepic5 = "", profilepic6 = "", profilepic7 = "", profilepic8 = "", profilepic9 = "";

    public MutualFriendsParcelableVO() {
    }

    protected MutualFriendsParcelableVO(Parcel in) {
        id = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        social_id = in.readString();
        profilepic1 = in.readString();
        profilepic2 = in.readString();
        profilepic3 = in.readString();
        profilepic4 = in.readString();
        profilepic5 = in.readString();
        profilepic6 = in.readString();
        profilepic7 = in.readString();
        profilepic8 = in.readString();
        profilepic9 = in.readString();
    }

    public static final Creator<MutualFriendsParcelableVO> CREATOR = new Creator<MutualFriendsParcelableVO>() {
        @Override
        public MutualFriendsParcelableVO createFromParcel(Parcel in) {
            return new MutualFriendsParcelableVO(in);
        }

        @Override
        public MutualFriendsParcelableVO[] newArray(int size) {
            return new MutualFriendsParcelableVO[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getProfilepic1() {
        return profilepic1;
    }

    public void setProfilepic1(String profilepic1) {
        this.profilepic1 = profilepic1;
    }

    public String getProfilepic2() {
        return profilepic2;
    }

    public void setProfilepic2(String profilepic2) {
        this.profilepic2 = profilepic2;
    }

    public String getProfilepic3() {
        return profilepic3;
    }

    public void setProfilepic3(String profilepic3) {
        this.profilepic3 = profilepic3;
    }

    public String getProfilepic4() {
        return profilepic4;
    }

    public void setProfilepic4(String profilepic4) {
        this.profilepic4 = profilepic4;
    }

    public String getProfilepic5() {
        return profilepic5;
    }

    public void setProfilepic5(String profilepic5) {
        this.profilepic5 = profilepic5;
    }

    public String getProfilepic6() {
        return profilepic6;
    }

    public void setProfilepic6(String profilepic6) {
        this.profilepic6 = profilepic6;
    }

    public String getProfilepic7() {
        return profilepic7;
    }

    public void setProfilepic7(String profilepic7) {
        this.profilepic7 = profilepic7;
    }

    public String getProfilepic8() {
        return profilepic8;
    }

    public void setProfilepic8(String profilepic8) {
        this.profilepic8 = profilepic8;
    }

    public String getProfilepic9() {
        return profilepic9;
    }

    public void setProfilepic9(String profilepic9) {
        this.profilepic9 = profilepic9;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(social_id);
        dest.writeString(profilepic1);
        dest.writeString(profilepic2);
        dest.writeString(profilepic3);
        dest.writeString(profilepic4);
        dest.writeString(profilepic5);
        dest.writeString(profilepic6);
        dest.writeString(profilepic7);
        dest.writeString(profilepic8);
        dest.writeString(profilepic9);
    }
}
