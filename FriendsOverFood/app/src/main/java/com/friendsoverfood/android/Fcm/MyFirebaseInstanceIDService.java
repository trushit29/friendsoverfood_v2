

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.friendsoverfood.android.Fcm;

import android.util.Log;

import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements HttpCallback {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        saveDeviceTokenForFCM(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        // Add custom implementation, as needed.
        if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false) && !token.trim().isEmpty()) {
            if (NetworkUtil.isOnline(MyFirebaseInstanceIDService.this)) {
                sendRequestSaveSocialProfileAPI();
            }
        }
        // Save token to _User class
    }

    private void saveDeviceTokenForFCM(String token) {
        SharedPreferencesTokens.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, token);
        SharedPreferencesTokens.save();
    }

    private void sendRequestSaveSocialProfileAPI() {
//        showProgress(getString(R.string.loading));
        String fields = "";
        HashMap<String, String> params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_devicetype), "android");
        fields = fields + getString(R.string.api_param_key_devicetype);

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_gender);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_gender);
            }
            params.put(getString(R.string.api_param_key_gender), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_dob);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_dob);
            }
            params.put(getString(R.string.api_param_key_dob), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_occupation);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_occupation);
            }
            params.put(getString(R.string.api_param_key_occupation), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_relationship);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_relationship);
            }
            params.put(getString(R.string.api_param_key_relationship), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_about_me);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_about_me);
            }
            params.put(getString(R.string.api_param_key_about_me), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_ethnicity);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_ethnicity);
            }
            params.put(getString(R.string.api_param_key_ethnicity), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_access_token);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_access_token);
            }
            params.put(getString(R.string.api_param_key_access_token), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_social_id);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_social_id);
            }
            params.put(getString(R.string.api_param_key_social_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic1);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic1);
            }
            params.put(getString(R.string.api_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic2);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic2);
            }
            params.put(getString(R.string.api_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic3);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic3);
            }
            params.put(getString(R.string.api_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic4);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic4);
            }
            params.put(getString(R.string.api_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic5);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic5);
            }
            params.put(getString(R.string.api_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        }

        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "");
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }*/

        if (!SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_devicetoken);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_devicetoken);
            }
            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim());
        }
        params.put(getString(R.string.api_param_key_fields), fields.trim());
        new HttpRequestSingletonPost(MyFirebaseInstanceIDService.this, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, MyFirebaseInstanceIDService.this);
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d("RESPONSE_EDIT_PROFILE_SOCIAL_SIGN_IN", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(MyFirebaseInstanceIDService.this, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();
                        }
                    } else {
//                        stopProgress();
//                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    stopProgress();
//                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
//                    stopProgress();
//                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }
}