package com.friendsoverfood.android.SignIn;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.EditProfileNew.EditProfileNewActivity;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.plumillonforge.android.chipview.OnChipClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Trushit on 26/02/17.
 */

public class TasteBudsSuggestionsActivity extends BaseActivity implements OnChipClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "TASTEBUD_SUGGESTIONS_ACTIVITY";

    private List<TastebudChipVO> listTastebudsSuggestion = new ArrayList<>();
    private List<TastebudChipVO> listTastebudsSuggestionSeach = new ArrayList<>();

    private Button btnContinue;
    private ChipView mTextChipLayout;
    private ChipViewAdapter adapterLayout;
    private TextView txtViewNoTastebuds;

    // Search view & search related variables
    public SearchView searchViewSuggestions;
    public SearchManager searchManager;
    // Timer for search
    public CountDownTimer timerSearchSuggestions;
    //    public boolean isTimerRunning = false;
    public String strQuerySearch = "";

    // Variables for Google API Client & Location services.
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    public boolean isLocationReceivedOnce = false;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    public LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Constant used in the location settings dialog.
     */
    public final int REQUEST_CHECK_SETTINGS = 0x1;

    public double currentLatitude = 0.0, currentLongitude = 0.0;

    private boolean isPermissionDialogDisplayed = false;

    private EditText searchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        setTitleSupportActionBarItalic(getString(R.string.title_tastebuds_profile));
        setTitleSupportActionBarItalic((!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "").trim().isEmpty()) ?
                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "") + getString(R.string.tastebuds_profile_title_suffix)
                : getString(R.string.your_tastebuds_profile));
//        setTitleSupportActionBar(getString(R.string.title_tastebuds_profile));

        // Increase the height for title in header for this activity only.
        txtTitleToolbarCentered.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20.0f);

        if (getIntent().hasExtra("isFromEditProfile") && getIntent().getBooleanExtra("isFromEditProfile", false)) {
            // Display Back button in support action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        } else if (getIntent().hasExtra("isFromRegister") && getIntent().getBooleanExtra("isFromRegister", false)) {
            // Display Back button in support action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        } else {
            // Hide Back button in support action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        /*getCurrentDeviceLocationUsingGoogleApi();*/
        /*setDrawerMenu();*/

        /*setDrawerMenu();
        lockDrawerMenu();*/
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_tastebuds_suggestions_activity, getMiddleContent());

        btnContinue = (Button) view.findViewById(R.id.buttonTastebudsSuggestionsActivityContinue);
        mTextChipLayout = (ChipView) view.findViewById(R.id.text_chip_layout);
        // Adapter
        adapterLayout = new TastebudsChipViewAdapter(this);

        txtViewNoTastebuds = (TextView) view.findViewById(R.id.txtViewNoTastebuds);

        searchViewSuggestions = (SearchView) view.findViewById(R.id.searchViewTastebudsSuggestionsActivity);
        searchViewSuggestions.setVisibility(View.VISIBLE);
        searchViewSuggestions.requestFocusFromTouch();
        searchViewSuggestions.setActivated(true);
        searchViewSuggestions.onActionViewExpanded();
        searchViewSuggestions.setIconifiedByDefault(false);
//        searchViewSuggestions.setFocusable(false);
        searchViewSuggestions.setIconified(false);
        searchViewSuggestions.clearFocus();
        searchViewSuggestions.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search) + "</font>"));

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        Intent intent = getIntent();

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    TastebudsSuggestionProvider.AUTHORITY, TastebudsSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
        }

        // TODO Change text color of search view
        searchEditText = (EditText) searchViewSuggestions.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHint(getString(R.string.what_interests_you));
        searchEditText.setTextSize(14);
        searchEditText.setTextColor(getResources().getColor(R.color.white_selector));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white_selector));

        // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchEditText, R.drawable.shape_edittext_cursor_green);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Change search magnification icons here.
        ImageView magImage = (ImageView) searchViewSuggestions.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        magImage.setImageResource(R.drawable.ic_search_edittext_white);

        // TODO Set bottom line to search view for all OS versions.
        searchViewSuggestions.findViewById(android.support.v7.appcompat.R.id.search_src_text).setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);

        searchViewSuggestions.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        /*for (int i = 0; i < 5; i++) {
            listTastebudsSuggestion.add(new TastebudChipVO("American", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Chinese", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Thai", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Continental", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Indonesian", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("French", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Indian", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Italian", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Vietnamese", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Singapore", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Spanish", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Korean", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Labanese", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Mexican", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Cuban", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Greek", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Continental", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Indonesian", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Mexican", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Cuban", 0));
            listTastebudsSuggestion.add(new TastebudChipVO("Greek", 0));
        }*/

        // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();

        setListener();

        setAllTypefaceMontserratRegular(searchEditText);
        setAllTypefaceMontserratRegular(view);
        setAllTypefaceMontserratBold(btnContinue);
    }

    private void initLayoutAfterPermissionGranted() {
        getCurrentDeviceLocationUsingGoogleApi();

//        setAllTypefaceDINMedium(btnRegister);
//        setAllTypefaceDINMedium(btnSignIn);
    }

    private void getTastebudsAll() {
        sendRequestGetTastebuds();
    }

    private void initLayoutSuggestionsChips() {

        // Custom layout and background colors
        mTextChipLayout.setAdapter(adapterLayout);
        mTextChipLayout.setChipLayoutRes(R.layout.chip_close);
        mTextChipLayout.setChipBackgroundColor(getResources().getColor(R.color.red_bg_chip));
        mTextChipLayout.setChipBackgroundColorSelected(getResources().getColor(R.color.white_bg_chip));
        if (strQuerySearch.trim().isEmpty()) {
            ArrayList<Chip> listChips = new ArrayList<>();
            for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
                listChips.add(new TastebudChipVO(listTastebudsSuggestion.get(i).getName().trim(), listTastebudsSuggestion.get(i).getType()));
            }

            if (listChips.size() > 0) {
                txtViewNoTastebuds.setVisibility(View.GONE);
                mTextChipLayout.setVisibility(View.VISIBLE);
                mTextChipLayout.setChipList(listChips);
            } else {
                txtViewNoTastebuds.setVisibility(View.VISIBLE);
                mTextChipLayout.setVisibility(View.GONE);
            }
        } else {
            ArrayList<Chip> listChipsSearch = new ArrayList<>();
            for (int i = 0; i < listTastebudsSuggestionSeach.size(); i++) {
                listChipsSearch.add(new TastebudChipVO(listTastebudsSuggestionSeach.get(i).getName().trim(), listTastebudsSuggestion.get(i).getType()));
            }

            if (listChipsSearch.size() > 0) {
                txtViewNoTastebuds.setVisibility(View.GONE);
                mTextChipLayout.setVisibility(View.VISIBLE);
                mTextChipLayout.setChipList(listChipsSearch);
            } else {
                txtViewNoTastebuds.setVisibility(View.VISIBLE);
                mTextChipLayout.setVisibility(View.GONE);
            }
        }
    }

    private void getCurrentDeviceLocationUsingGoogleApi() {

        checkLocationPermissionGrantedOrNot();
    }

    private void setListener() {
        btnContinue.setOnClickListener(this);
        mTextChipLayout.setOnChipClickListener(this);
        searchViewSuggestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        searchViewSuggestions.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
//                Log.i("SEARCH_QUERY_STRING", "" + newText);
                if (timerSearchSuggestions != null) {
                    timerSearchSuggestions.cancel();
                }

                timerSearchSuggestions = new CountDownTimer(1000, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (!newText.trim().isEmpty()) {
                            if (!newText.trim().equalsIgnoreCase(strQuerySearch.trim())) {
                                strQuerySearch = newText.trim();
                            }

                            Log.d(TAG, "Search timer finished. Text: " + newText.trim() + ", SearchQuery: " + strQuerySearch.trim());

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                            // TODO Add all the matching query items to search list from original list.
                            listTastebudsSuggestionSeach.clear();
                            for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
                                if (listTastebudsSuggestion.get(i).getText().trim().toLowerCase().contains(strQuerySearch.trim().toLowerCase())) {
//                                    Log.d(TAG, "Matched pos: " + i + ", Text: " + listTastebudsSuggestion.get(i).getText().trim());
                                    TastebudChipVO tag = (TastebudChipVO) listTastebudsSuggestion.get(i);
                                    listTastebudsSuggestionSeach.add(tag);
                                }
                            }

                            initLayoutSuggestionsChips();

                            // Begin search here.
                            /*if (listViewSearch != null) {
                                listViewSearch.setVisibility(View.GONE);
                            }

                            if (txtNoResults != null) {
                                txtNoResults.setVisibility(View.GONE);
                            }

                            if (progressBarLoading != null) {
                                progressBarLoading.setVisibility(View.VISIBLE);
                            }*/
                        } else {
                            strQuerySearch = "";
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            initLayoutSuggestionsChips();
                        }
                    }
                };
                timerSearchSuggestions.start();
                return true;
            }
        });

        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    searchEditText.setHint("");

                    Log.d(TAG, "Focus change. On focus.");
                } else {
                    searchEditText.setHint("");
                    Log.d(TAG, "Focus change. Focus lost.");
                }
            }
        });
    }

    @Override
    public void onChipClick(Chip chip) {
        TastebudChipVO tag = (TastebudChipVO) chip;
        tag.setType(tag.getType() == 0 ? 1 : 0);
        mTextChipLayout.refresh();

        Log.d(TAG, "Chip clicked Name: " + chip.getText() + ", Tag name Id: " + tag.getName() + ", Tag text: " + tag.getText().trim());

        for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
            if (chip.getText().toString().trim().equalsIgnoreCase(listTastebudsSuggestion.get(i).getText().toString().trim())) {
                listTastebudsSuggestion.get(i).setType(listTastebudsSuggestion.get(i).getType() == 0 ? 1 : 0);
            }
        }


        /*for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
            if (listTastebudsSuggestion.get(i).getText().equalsIgnoreCase(tag.getText())) {
                if (tag.getType() == 1)
                    listTastebudsSuggestion.set(i, new TastebudChipVO(tag.getText(), 0));
                else
                    listTastebudsSuggestion.set(i, new TastebudChipVO(tag.getText(), 1));

            }

        }
        mTextChipLayout.setAdapter(adapterLayout);
        Log.i("Chip", "chip" + tag.getText());*/
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO Set Transparent toolbar color for background
        toolbarLayoutRoot.setBackgroundResource(R.color.transparent);

        // TODO Set Background to Coordinator layout of base activity root, so that transparent toolbar works
        coordinatorRoot.setBackground(getResources().getDrawable(R.drawable.ic_bg_app_red_small_icons));

        isLocationReceivedOnce = false;
        isPermissionDialogDisplayed = false;
        getCurrentDeviceLocationUsingGoogleApi();
//        if (!checkIsAllAppPermissionsEnabled()) {
//            showAllAppPermissionsEnabled();
//        } else {
//
//            /*initLayoutSuggestionsChips();*/
//        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.buttonTastebudsSuggestionsActivityContinue:
//                intent = new Intent(context, RestaurantsMapsActivity.class);
                String tastebudsSelected = "";

                for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
                    if (listTastebudsSuggestion.get(i).getType() == 1) {
                        if (tastebudsSelected.trim().isEmpty()) {
                            tastebudsSelected = "" + listTastebudsSuggestion.get(i).getName().trim();
                        } else {
                            tastebudsSelected = tastebudsSelected + "," + listTastebudsSuggestion.get(i).getName().trim();
                        }
                    }
                }

                if (tastebudsSelected.trim().isEmpty()) {
                    showAlertDialogTastebudsMessage(getString(R.string.dont_you_have_any_tastebuds),
                            getString(R.string.please_select_few_tastebuds_to_get_better_matches));
                } else {
                    sendRequestSaveProfileTastebudsAPI(tastebudsSelected);
                }
                /*intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();*/
                break;

            default:
                break;

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItemNotification = menu.findItem(R.id.menu_item_toolbar_skip);
        menuItemNotification.setVisible(false);
        View viewSkip = (View) menuItemNotification.getActionView();
        TextView txtSkip = (TextView) viewSkip.findViewById(R.id.textViewCustomMenuItemSkip);

        viewSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                intent = new Intent(context, RestaurantsMapsActivity.class);
                intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.d(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d(TAG, "RESPONSE EDIT PROFILE SIGN IN: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            /*// TODO Forward user to TasteBuds profile screen.
                            intent = new Intent(context, TasteBudsSuggestionsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            supportFinishAfterTransition();*/
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_GET_ALL_TASTEBUDS) {
                Log.d("RESPONSE_TASTEBUDS_ALL", "Response: " + response.toString().trim());

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response.toString().trim());
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        listTastebudsSuggestion = new ArrayList<>();
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            TastebudChipVO chip = new TastebudChipVO();
                            chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
                            chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
                            chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
                            chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
                            chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
                            chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                            chip.setType(0);
                            listTastebudsSuggestion.add(chip);
                            /*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*/
                        }

                        // TODO Get All tastebuds for current logged in user
//                        sendRequestGetTastebudsForUser();

                        // Fetch all tastebuds from local.
                        List<String> listItemsTastebuds = new ArrayList<String>(Arrays.asList(
                                SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_TASTEBUDS, "").split("\\s*,\\s*")));
                        Log.d(TAG, "All tastebuds user: " + listItemsTastebuds.toString());

                        // Set tastebuds selected, if present in user's list
                        for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
                            boolean isExists = false;
                            for (int j = 0; j < listItemsTastebuds.size(); j++) {
//                                if (listTastebudsSuggestionsUser.get(j).getZomato_id().trim().equalsIgnoreCase(listTastebudsSuggestion.get(i).getZomato_id().trim())) {
                                if (listItemsTastebuds.get(j).trim().equalsIgnoreCase(listTastebudsSuggestion.get(i).getName().trim())) {
                                    isExists = true;
                                }
                            }

                            // Tastebud is selected by user.
                            if (isExists) {
                                listTastebudsSuggestion.get(i).setType(1);
                            } else {
                                listTastebudsSuggestion.get(i).setType(0);
                            }
                        }

                        // Add all user tastebuds which are not present in tastebuds list for location
                        for (int i = 0; i < listItemsTastebuds.size(); i++) {
                            boolean isExists = false;
                            for (int j = 0; j < listTastebudsSuggestion.size(); j++) {
//                                if (listTastebudsSuggestionsUser.get(i).getZomato_id().trim().equalsIgnoreCase(listTastebudsSuggestion.get(j).getZomato_id().trim())) {
                                if (listItemsTastebuds.get(i).trim().equalsIgnoreCase(listTastebudsSuggestion.get(j).getName().trim())) {
                                    isExists = true;
                                }
                            }

                            // TODO By trushit, restrict user customized tastebuds here, not to display in the list. The only tastebuds will be
                            // displayed are the one from the list only. - 18 Sept 2017
                            /*if (!isExists) {
//                                listItemsTastebuds.get(i).setType(1);

                                TastebudChipVO chip = new TastebudChipVO();
                                chip.setId("-1");
                                chip.setName(listItemsTastebuds.get(i).trim());
                                chip.setType(1);
                                listTastebudsSuggestion.add(chip);
//                                listTastebudsSuggestion.add(listTastebudsSuggestionsUser.get(i));
                            }*/
                        }

                        // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
                        initLayoutSuggestionsChips();
                        stopProgress();

                        stopProgress();
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_GET_USER_TASTEBUDS) {
                Log.d("RESPONSE_TASTEBUDS_USER", "Response: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        ArrayList<TastebudChipVO> listTastebudsSuggestionsUser = new ArrayList<>();
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            TastebudChipVO chip = new TastebudChipVO();
                            chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
                            chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
                            chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
                            chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
                            chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
                            chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                            listTastebudsSuggestionsUser.add(chip);
                            /*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*/
                        }

                        // Set tastebuds selected, if present in user's list
                        for (int i = 0; i < listTastebudsSuggestion.size(); i++) {
                            boolean isExists = false;
                            for (int j = 0; j < listTastebudsSuggestionsUser.size(); j++) {
//                                if (listTastebudsSuggestionsUser.get(j).getZomato_id().trim().equalsIgnoreCase(listTastebudsSuggestion.get(i).getZomato_id().trim())) {
                                if (listTastebudsSuggestionsUser.get(j).getName().trim()
                                        .equalsIgnoreCase(listTastebudsSuggestion.get(i).getName().trim())) {
                                    isExists = true;
                                }
                            }

                            // Tastebud is selected by user.
                            if (isExists) {
                                listTastebudsSuggestion.get(i).setType(1);
                            } else {
                                listTastebudsSuggestion.get(i).setType(0);
                            }
                        }

                        // Add all user tastebuds which are not present in tastebuds list for location
                        for (int i = 0; i < listTastebudsSuggestionsUser.size(); i++) {
                            boolean isExists = false;
                            for (int j = 0; j < listTastebudsSuggestion.size(); j++) {
//                                if (listTastebudsSuggestionsUser.get(i).getZomato_id().trim().equalsIgnoreCase(listTastebudsSuggestion.get(j).getZomato_id().trim())) {
                                if (listTastebudsSuggestionsUser.get(i).getName().trim()
                                        .equalsIgnoreCase(listTastebudsSuggestion.get(j).getName().trim())) {
                                    isExists = true;
                                }
                            }

                            if (!isExists) {
                                listTastebudsSuggestionsUser.get(i).setType(1);
                                listTastebudsSuggestion.add(listTastebudsSuggestionsUser.get(i));
                            }
                        }

                        // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
                        initLayoutSuggestionsChips();
                        stopProgress();
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_TASTEBUDS) {
                Log.d("RESPONSE_API_EDIT_PROFILE_TASTEBUDS", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            stopProgress();
                            if (getIntent().hasExtra("isFromEditProfile") && getIntent().getBooleanExtra("isFromEditProfile", false)) {
                                onBackPressed();
                               /* // TODO Forward user to Home screen once tastebuds are saved.
                                intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();*/
                            } else if (getIntent().hasExtra("isFromRegister") && getIntent().getBooleanExtra("isFromRegister", false)) {
                                /*intent = new Intent(context, EditProfileNewActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                                startActivity(intent);
                                supportFinishAfterTransition();*/
                                // TODO Forward user to Home screen once tastebuds are saved.
                                intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } else {
                                // TODO Forward user to Home screen once tastebuds are saved.
                                intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS:
//                isPermissionDialogDisplayed = false;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    showSnackBarMessageOnly(getString(R.string.you_have_successfully_granted_required_permissions));
                    initLayoutAfterPermissionGranted();
//                    Log.d(TAG, "Permission granted pos: " + 0);
                } else {
                    // Permission Denied
                    showSnackBarMessageOnly(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
                    initLayoutAfterPermissionGranted();
                }
                break;

            case Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the  camera related task you need to do.
                   /* showSnackBar(getString(R.string.you_have_successfully_granted_required_permissions));*/
                    checkLocationSettings();
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    /*supportFinishAfterTransition();*/
                    Toast.makeText(context, getString(R.string.you_have_not_granted_location_permission), Toast.LENGTH_SHORT).show();
                    /*initLayoutSuggestionsChips();*/
                    /*getTastebudsAll();*/
                }
                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showAllAppPermissionsEnabled() {
        if (!isPermissionDialogDisplayed) {
            isPermissionDialogDisplayed = true;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialogAlertRequestAllPermissions(getString(R.string.request_permission), getString(R.string.you_need_to_grant_required_permissions));
                        return;
                    }
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                                    , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
                } else {
                    // All permissions are granted. Forward user to home screen of the app.
                    initLayoutAfterPermissionGranted();
                }
            } else {
                initLayoutAfterPermissionGranted();
            }
        }
    }

    protected void showDialogAlertRequestAllPermissions(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                                    , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
                }
            }
        });
                /*.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Snackbar.make(view, getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly), Snackbar.LENGTH_LONG).show();
//                showSn(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
            }
        });*/
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Sets up the location request. Android has two location request settings: {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update interval (5 seconds), the Fused Location Provider API returns location updates
     * that are accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(5 * 5000); // 5 second, in milliseconds
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        if (!isLocationReceivedOnce) {
            Log.d(TAG, "Checking location settings...");
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);
            result.setResultCallback(TasteBudsSuggestionsActivity.this);
        }
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private boolean isLocationPermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;

            }
        } else {
            return true;
        }
    }

    private void checkLocationPermissionGrantedOrNot() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) ||
                        !shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    showDialogAlertRequestPermissionLocation(getString(R.string.request_permission),
                            getString(R.string.location_permission_is_required));
                    /*requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);*/
                    return;
                }

                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
            } else {
                checkLocationSettings();
            }
        } else {
            checkLocationSettings();
        }
    }

    protected void showDialogAlertRequestPermissionLocation(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Log.d(TAG, "Google API Client connected");
            if (isLocationPermissionGranted()) {
                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location == null) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TasteBudsSuggestionsActivity.this);
                } else {
                    //If everything went fine lets get latitude and longitude
                    Log.d(TAG, "Location using last location. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude());

                    if (!isLocationReceivedOnce) {
                        isLocationReceivedOnce = true;
                    }

                    if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude()) {
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();
                        String lat = String.valueOf(currentLatitude);
                        String lng = String.valueOf(currentLongitude);

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, lat.trim().replaceAll(",", "."));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, lng.trim().replaceAll(",", "."));
                        SharedPreferenceUtil.save();

                        // TODO Update user location by calling edit profile
                        sendRequestSaveProfileLocationAPI();

                        getTastebudsAll();
                    }


                   /* // Save user latitude & longitude in background using edit profile API.
                    sendRequestEditProfileDetails();*/

                    stopProgress();


                    /*initLayoutSuggestionsChips();*/

                }
            } else {
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            stopProgress();
        } catch (Exception e) {
            e.printStackTrace();
            stopProgress();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Suspended");

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location updated. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude());
        stopProgress();

        if (!isLocationReceivedOnce) {
            isLocationReceivedOnce = true;

            if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude()) {
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.save();

                // TODO Update user location by calling edit profile
                sendRequestSaveProfileLocationAPI();


            /*// Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails();*/

                stopProgress();


            /*initLayoutSuggestionsChips();*/
                getTastebudsAll();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Failed");
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(TasteBudsSuggestionsActivity.this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
                stopProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
            stopProgress();
        }
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        Log.d(TAG, "Result received: " + locationSettingsResult.getStatus());
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                    status.startResolutionForResult(TasteBudsSuggestionsActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    Log.d(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                }
            });
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        if (isLocationPermissionGranted()) {
            if (mGoogleApiClient != null && mLocationRequest != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, TasteBudsSuggestionsActivity.this)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                Log.d(TAG, "Location status: " + status.toString().trim());
                               /* if (!isLocationReceivedOnce) {*/
                                    /*initLayoutSuggestionsChips();*/
                                if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0") &&
                                        !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
                                    getTastebudsAll();
                                }
                                /*}*/
                            }
                        });
            }
        }
    }

    private void sendRequestSaveProfileLocationAPI() {
        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            showProgress(getString(R.string.loading));
            String fields = "";
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_location), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
            params.put(getString(R.string.api_param_key_devicetype), "android");
            fields = fields + getString(R.string.api_param_key_devicetype);
            String location_string = "";
            location_string = getPlaceAddress();
            if (!location_string.trim().isEmpty()) {
                params.put(getString(R.string.api_param_key_location_string), location_string.trim());
            }
            fields = fields + "," + getString(R.string.api_param_key_location) + (!location_string.trim().isEmpty() ? ","
                    + getString(R.string.api_param_key_location_string) : "");
            params.put(getString(R.string.api_param_key_fields), fields.trim());

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, TasteBudsSuggestionsActivity.this);
        }
    }

    private void sendRequestGetTastebuds() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_testbuds));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            params.put(getString(R.string.api_param_key_latlng), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().replaceAll(",", ".")
                    + "," + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().replaceAll(",", "."));
        } else {
            params.put(getString(R.string.api_param_key_latlng), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                    + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
        }

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_GET_ALL_TASTEBUDS, TasteBudsSuggestionsActivity.this);
    }

    private void sendRequestGetTastebudsForUser() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_mytestbuds));
//        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_mytestbuds));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_GET_USER_TASTEBUDS, TasteBudsSuggestionsActivity.this);
    }

    private void sendRequestSaveProfileTastebudsAPI(String strTastebudsCommaSeparated) {
        if (!strTastebudsCommaSeparated.trim().isEmpty()) {
            showProgress(getString(R.string.loading));
            String fields = "";
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_devicetype), "android");
            fields = fields + getString(R.string.api_param_key_devicetype);
            params.put(getString(R.string.api_param_key_testbuds), strTastebudsCommaSeparated);
            fields = fields + "," + getString(R.string.api_param_key_testbuds);
            params.put(getString(R.string.api_param_key_fields), fields);

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_EDIT_PROFILE_TASTEBUDS, TasteBudsSuggestionsActivity.this);
        } else {
            // TODO Forward user to Home screen once tastebuds are saved.
            if (getIntent().hasExtra("isFromRegister") && getIntent().getBooleanExtra("isFromRegister", false)) {
                intent = new Intent(context, EditProfileNewActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                startActivity(intent);
                supportFinishAfterTransition();
            } else if (getIntent().hasExtra("isFromEditProfile") && getIntent().getBooleanExtra("isFromEditProfile", false)) {
                onBackPressed();
            } else {
                intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            }
        }
    }

    private String getPlaceAddress() {
        String place = "";
        double latitude = 0.0, longitude = 0.0;
        latitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")) : 0.0;
        longitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")) : 0.0;

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null && addresses.size() > 0) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            if (city != null && !city.trim().isEmpty() && !city.trim().equalsIgnoreCase("null")) {
                place = city;
            }
            if (state != null && !state.trim().isEmpty() && !state.trim().equalsIgnoreCase("null")) {
                if (place.trim().isEmpty()) {
                    place = state;
                } else {
                    place = place + ", " + state;
                }

            }
            if (country != null && !country.trim().isEmpty() && !country.trim().equalsIgnoreCase("null")) {
                if (country.trim().isEmpty()) {
                    place = country;
                } else {
                    place = place + ", " + country;
                }
            }
        }

        return place;
    }

    public void showAlertDialogTastebudsMessage(String strTitle, String strMessage) {
        new AlertDialog.Builder(mContext).setTitle(strTitle.trim()).setMessage(strMessage.trim())
                .setPositiveButton(getString(R.string.dismiss), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
               /* .setIcon(android.R.drawable.ic_dialog_alert)*/
                .show();
    }

}