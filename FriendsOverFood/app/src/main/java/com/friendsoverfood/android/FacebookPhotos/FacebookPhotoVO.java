package com.friendsoverfood.android.FacebookPhotos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Trushit on 05/07/16.
 */
public class FacebookPhotoVO implements Serializable {
    private ArrayList<FacebookImagesVO> images;
    private String picture = "", id = "";

    public ArrayList<FacebookImagesVO> getImages() {
        return images;
    }

    public void setImages(ArrayList<FacebookImagesVO> images) {
        this.images = images;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
