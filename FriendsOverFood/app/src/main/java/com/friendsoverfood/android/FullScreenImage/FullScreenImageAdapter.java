package com.friendsoverfood.android.FullScreenImage;

/**
 * Created by ashish on 17/07/16.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.friendsoverfood.android.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<String> _imagePaths;
    private LayoutInflater inflater;
    private String  urlProfileImage;

    // constructor
    public FullScreenImageAdapter(Activity activity, ArrayList<String> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        Button btnClose;

        inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_full_screen_image_activity, container, false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);

       /* BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);*/
//        imgDisplay.setImageBitmap(bitmap);

        if (_imagePaths != null) {
            urlProfileImage = _imagePaths.get(position);
//            int width = (BaseActivity.widthScreen - Constants.convertDpToPixels(48)) / 3;
//            int height=(BaseActivity.heightScreen -Constants.convertDpToPixels(16))/6;
            if (!_imagePaths.get(position).trim().isEmpty()) {
                String url = _imagePaths.get(position).trim();

                final String strUrl = url;


//                .resize(width, width).centerCrop()
                Picasso.with(_activity).load(strUrl)
                        .error(R.drawable.ic_placeholder_chat_image).placeholder(R.drawable.ic_placeholder_chat_image).into(imgDisplay, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("GridAdapter", "Success loading profile picture: " + strUrl.trim());
                    }

                    @Override
                    public void onError() {
                        Log.d("GridAdapter", "Error loading profile picture: " + strUrl.trim());
                    }
                });

//            viewHolder.imageView.setImageResource(images[pos]);

            }
        }
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.finish();
            }
        });

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
