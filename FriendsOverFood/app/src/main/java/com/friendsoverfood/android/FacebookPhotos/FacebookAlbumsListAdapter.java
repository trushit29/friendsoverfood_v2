package com.friendsoverfood.android.FacebookPhotos;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.util.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 04/07/16.
 */
public class FacebookAlbumsListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<FacebookAlbumVO> list;
    private Intent intent;
    private FacebookAlbumsListActivity activity;

    public FacebookAlbumsListAdapter(Context context, ArrayList<FacebookAlbumVO> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.activity = (FacebookAlbumsListActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_facebook_albums_list_adapter, parent, false);

            holder.txtName = (TextView) convertView.findViewById(R.id.textViewFacebookAlbumsListAdapterName);
            holder.txtPhotos = (TextView) convertView.findViewById(R.id.textViewFacebookAlbumsListAdapterPhotos);
            holder.imgPhoto = (ImageView) convertView.findViewById(R.id.imageViewFacebookAlbumsListAdapterImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtName.setText(list.get(position).getName().trim());

        if (list.get(position).getPhoto_count() != 0) {
            holder.txtPhotos.setText(list.get(position).getPhoto_count() + " " + context.getResources().getString(R.string.photos));
        } else {
            holder.txtPhotos.setText(list.get(position).getCount() + " " + context.getResources().getString(R.string.photos));
        }

        final int pos = position;

        if (!list.get(position).getUrl().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getUrl().trim()).resize(Constants.convertDpToPixels(60), Constants.convertDpToPixels(60)).centerCrop()
                    .transform(new CircleTransform()).error(R.drawable.ic_user_profile_avatar).placeholder(R.drawable.ic_user_profile_avatar)
                    .into(holder.imgPhoto, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d("FACEBOOK_ALBUMS_COVER_PHOTO", "Success loading profile picture: " + list.get(pos).getUrl().trim());
                        }

                        @Override
                        public void onError() {
                            Log.d("FACEBOOK_ALBUMS_COVER_PHOTO", "Error loading profile picture: " + list.get(pos).getUrl().trim());
                        }
                    });
        }

//        final ViewHolder finalHolder = holder;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Forward to Photos activity here.
                intent = new Intent(context, FacebookPhotosActivity.class);
                intent.putExtra(Constants.INTENT_PARAM_FB_ALBUM, list.get(pos));
                activity.startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_PICTURE);
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public TextView txtName, txtPhotos;
        public ImageView imgPhoto;
    }
}
