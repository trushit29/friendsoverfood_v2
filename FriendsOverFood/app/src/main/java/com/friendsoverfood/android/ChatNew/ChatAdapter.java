package com.friendsoverfood.android.ChatNew;

import android.content.Context;
import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.FullScreenImage.DisplayMediaImageActivity;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantDetails.RestaurantDetailsActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Trushit on 22/03/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ArrayList<MessageVO> list;
    private Context context;
    private ChatDetailsActivity activity;
    private Intent intent;
    private int TextMsgCount = 0;
    private int RestoCount = 0;

    public ChatAdapter(Context context, ArrayList<MessageVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (ChatDetailsActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_chat_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final int pos = position;
        final ViewHolder holder = viewHolder;

        if (list.get(position).getRecieverId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            holder.linearReceiverView.setVisibility(View.GONE);
            holder.linearSenderView.setVisibility(View.VISIBLE);
            if (list.get(position).getContentType().equalsIgnoreCase("text")) {
                holder.relativeSenderChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_sender_white_bg);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeSenderChatRoot.setPadding(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0),
                        Constants.convertDpToPixels(0));*/
                holder.txtSenderMessage.setVisibility(View.VISIBLE);
                holder.textViewChatListAdapterSenderMessageImage.setVisibility(View.GONE);
                holder.linearLayoutRestaurantSender.setVisibility(View.GONE);
                holder.txtSenderMessage.setText(list.get(position).getMsg().trim());
            } else if (list.get(position).getContentType().equalsIgnoreCase("image")) {
//                holder.relativeSenderChatRoot.setBackgroundResource(R.drawable.ic_chat_image_bg_receiver);
//                holder.relativeSenderChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_sender_white_bg);
                holder.relativeSenderChatRoot.setBackgroundResource(0);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(48), Constants.convertDpToPixels(12), Constants.convertDpToPixels(12), Constants.convertDpToPixels(12));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeSenderChatRoot.setPadding(Constants.convertDpToPixels(48), Constants.convertDpToPixels(12), Constants.convertDpToPixels(12),
                        Constants.convertDpToPixels(12));*/
                holder.textViewChatListAdapterSenderMessageImage.setVisibility(View.VISIBLE);
                holder.txtSenderMessage.setVisibility(View.GONE);
                holder.linearLayoutRestaurantSender.setVisibility(View.GONE);
                holder.ProgressBarProfileImage.setVisibility(View.VISIBLE);

                if (!list.get(position).getImgUrl().isEmpty())
                    Picasso.with(context).load(list.get(position).getImgUrl()).resize(Constants.convertDpToPixels(206), Constants.convertDpToPixels(206))
                            .placeholder(R.drawable.ic_placeholder_chat_image).error(R.drawable.ic_placeholder_chat_image)
                            .into(holder.textViewChatListAdapterSenderMessageImage, new Callback() {
                                @Override
                                public void onSuccess() {
                                    holder.ProgressBarProfileImage.setVisibility(View.GONE);

                                }

                                @Override
                                public void onError() {
                                    Log.d("ERROR_LOADING_IMAGE", "Image: " + list.get(pos).getImgUrl().trim());
                                }
                            });
                holder.textViewChatListAdapterSenderMessageImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent i = new Intent(context, FullScreenImageActivity.class);
                        Intent i = new Intent(context, DisplayMediaImageActivity.class);
                        /*ArrayList<String> images = new ArrayList<String>();
                        if (!list.get(pos).getImgUrl().isEmpty())
                            images.add(list.get(pos).getImgUrl());
                        i.putExtra("List", images);*/
                        i.putExtra("image", list.get(pos).getImgUrl().trim());
                        /*context.startActivity(i);*/

                        View sharedViewImage = holder.textViewChatListAdapterSenderMessageImage;
                        String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedViewImage, transitionNameImage);
                            activity.startActivity(i, transitionActivityOptions.toBundle());
                        } else {
                            activity.startActivity(i);
                        }
                    }
                });
            } else if ((list.get(position).getContentType().equalsIgnoreCase("restaurant"))) {
                holder.relativeSenderChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_sender_white_bg);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeSenderChatRoot.setPadding(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0),
                        Constants.convertDpToPixels(0));*/
                holder.txtSenderMessage.setVisibility(View.GONE);
                holder.textViewChatListAdapterSenderMessageImage.setVisibility(View.GONE);
                holder.ProgressBarProfileImageSender.setVisibility(View.GONE);
                holder.linearLayoutRestaurantSender.setVisibility(View.VISIBLE);
                if (list.get(pos).getRestoVo() != null) {
                    holder.textViewRestaurantListAdapterRestaurantNameSender.setText(list.get(position).getRestoVo().getName().trim());
                    holder.textViewRestaurantListAdapterRestaurantTimingsSender.setText(list.get(position).getRestoVo().getTimings().trim());
                    holder.textViewRestaurantListAdapterRestaurantRatingsSender.setText(String.valueOf(list.get(position).getRestoVo().getRatings()).trim() + " " + context.getResources().getString(R.string.rating));

                    holder.textViewRestaurantListAdapterRestaurantAddressSender.setText(list.get(position).getRestoVo().getVicinity().trim());
                    if (!list.get(position).getRestoVo().getRatings().trim().isEmpty())
                        holder.ratingBarRestaurantListAdapterRestaurantsRatingsSender.setRating(Float.valueOf(list.get(position).getRestoVo().getRatings()));

                    if (!list.get(position).getRestoVo().getPhotoReference().trim().isEmpty()) {
                        Picasso.with(context)
                                .load(list.get(position).getRestoVo().getPhotoReference().trim())
                                .resize(Constants.convertDpToPixels(92), Constants.convertDpToPixels(104)).centerCrop()
                                .error(R.drawable.ic_placeholder_restaurant).placeholder(R.drawable.ic_placeholder_restaurant)
                                .into(holder.imageViewRestaurantListAdapterRestaurantPictureSender, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getRestoVo().getPhotoReference().trim());
                                    }
                                });
                    } else {
                        holder.imageViewRestaurantListAdapterRestaurantPictureSender.setImageResource(R.drawable.ic_placeholder_restaurant);
                    }

                }

                holder.linearLayoutRestaurantSender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(context, RestaurantDetailsActivity.class);
                        intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos).getRestoVo());
                        intent.putExtra("isToShowInviteFriends", false);
                        activity.startActivity(intent);
                    }
                });

            }

            if (list.get(pos).getSenderDp() != null && !list.get(pos).getSenderDp().trim().isEmpty()) {
                Picasso.with(context).load(list.get(pos).getSenderDp())
                        .resize(Constants.convertDpToPixels(72), Constants.convertDpToPixels(72)).centerCrop()
                        .placeholder(R.drawable.ic_user_profile_avatar)
                        .error(R.drawable.ic_user_profile_avatar)
                        .into(holder.imgSenderProfilePic, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getSenderDp().trim());
                            }
                        });
            }

            if (list.get(position).getReciept().trim().isEmpty()) {
                Calendar c = Calendar.getInstance();
                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);
                if (list.get(position).getRecieverId().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
                        && list.get(position).getReciept().toString().trim().equalsIgnoreCase("") && list.get(position).getReciept().trim().isEmpty()) {
                    activity.mDatabaseMessages.child(activity.chatSelected.getMatchid().trim()).child
                            (list.get(pos).getKey()).child("reciept").setValue(String.valueOf(timestampToSave));
                }
            }

//
        } else {
            holder.linearSenderView.setVisibility(View.GONE);
            holder.linearReceiverView.setVisibility(View.VISIBLE);

            if (list.get(position).getContentType().equalsIgnoreCase("text")) {
                holder.relativeReceiverChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_receiver_white_bg);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeReceiverChatRoot.setPadding(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0),
                        Constants.convertDpToPixels(0));*/
                holder.txtReceiverMessage.setVisibility(View.VISIBLE);
                holder.textViewChatListAdapterRecieverMessageImage.setVisibility(View.GONE);
                holder.ProgressBarProfileImageSender.setVisibility(View.GONE);
                holder.linearLayoutRestaurantReciever.setVisibility(View.GONE);
                holder.txtReceiverMessage.setText(list.get(position).getMsg().trim());
            } else if (list.get(position).getContentType().equalsIgnoreCase("image")) {
//                holder.relativeReceiverChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_receiver_white_bg);
                holder.relativeReceiverChatRoot.setBackgroundResource(0);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(12), Constants.convertDpToPixels(12), Constants.convertDpToPixels(48), Constants.convertDpToPixels(12));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeReceiverChatRoot.setPadding(Constants.convertDpToPixels(12), Constants.convertDpToPixels(12), Constants.convertDpToPixels(48),
                        Constants.convertDpToPixels(12));*/
                holder.txtReceiverMessage.setVisibility(View.GONE);
                holder.textViewChatListAdapterRecieverMessageImage.setVisibility(View.VISIBLE);
                holder.ProgressBarProfileImageSender.setVisibility(View.GONE);
                holder.linearLayoutRestaurantReciever.setVisibility(View.GONE);

                if (!list.get(pos).getImgUrl().trim().isEmpty()) {
                    Picasso.with(context).load(list.get(position).getImgUrl()).resize(Constants.convertDpToPixels(206), Constants.convertDpToPixels(206))
                            .placeholder(R.drawable.ic_placeholder_chat_image).error(R.drawable.ic_placeholder_chat_image)
                            .into(holder.textViewChatListAdapterRecieverMessageImage, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    Log.d("ERROR_LOADING_IMAGE", "Image: " + list.get(pos).getImgUrl().trim());
                                }
                            });
                }

                holder.textViewChatListAdapterRecieverMessageImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent i = new Intent(context, FullScreenImageActivity.class);
                        Intent i = new Intent(context, DisplayMediaImageActivity.class);
                        /*ArrayList<String> images = new ArrayList<String>();
                        if (!list.get(pos).getImgUrl().isEmpty())
                            images.add(list.get(pos).getImgUrl());
                        i.putExtra("List", images);*/
                        i.putExtra("image", list.get(pos).getImgUrl().trim());
                        /*context.startActivity(i);*/

                        View sharedViewImage = holder.textViewChatListAdapterRecieverMessageImage;
                        String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedViewImage, transitionNameImage);
                            activity.startActivity(i, transitionActivityOptions.toBundle());
                        } else {
                            activity.startActivity(i);
                        }
                    }
                });

            } else if (list.get(position).getContentType().equalsIgnoreCase("dummy")) {
//                holder.relativeReceiverChatRoot.setBackgroundResource(R.drawable.ic_chat_image_bg_sender);
                holder.relativeReceiverChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_receiver_white_bg);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(12), Constants.convertDpToPixels(12), Constants.convertDpToPixels(48), Constants.convertDpToPixels(12));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeReceiverChatRoot.setPadding(Constants.convertDpToPixels(12), Constants.convertDpToPixels(12), Constants.convertDpToPixels(48),
                        Constants.convertDpToPixels(12));*/
                holder.txtReceiverMessage.setVisibility(View.GONE);
                holder.textViewChatListAdapterRecieverMessageImage.setVisibility(View.VISIBLE);
                holder.textViewChatListAdapterRecieverMessageImage.setImageResource(R.drawable.ic_placeholder_chat_image);
                holder.ProgressBarProfileImageSender.setVisibility(View.VISIBLE);
                holder.linearLayoutRestaurantReciever.setVisibility(View.GONE);
            } else {
                holder.relativeReceiverChatRoot.setBackgroundResource(R.drawable.ic_chat_bubble_receiver_white_bg);
                /*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0));
                holder.relativeReceiverChatRoot.setLayoutParams(params);*/
                /*holder.relativeReceiverChatRoot.setPadding(Constants.convertDpToPixels(0), Constants.convertDpToPixels(0), Constants.convertDpToPixels(0),
                        Constants.convertDpToPixels(0));*/
                holder.txtReceiverMessage.setVisibility(View.GONE);
                holder.textViewChatListAdapterRecieverMessageImage.setVisibility(View.GONE);
                holder.ProgressBarProfileImage.setVisibility(View.GONE);
                holder.linearLayoutRestaurantReciever.setVisibility(View.VISIBLE);
                if (list.get(pos).getRestoVo() != null) {
                    holder.textViewRestaurantListAdapterRestaurantName.setText(list.get(position).getRestoVo().getName().trim());
                    holder.textViewRestaurantListAdapterRestaurantTimings.setText(list.get(position).getRestoVo().getTimings().trim());
                    holder.textViewRestaurantListAdapterRestaurantRatings.setText(String.valueOf(list.get(position).getRestoVo().getRatings()).trim() + " " + context.getResources().getString(R.string.rating));

                    holder.textViewRestaurantListAdapterRestaurantAddress.setText(list.get(position).getRestoVo().getVicinity().trim());
                    if (!list.get(position).getRestoVo().getRatings().trim().isEmpty())
                        holder.ratingBarRestaurantListAdapterRestaurantsRatings.setRating(Float.valueOf(list.get(position).getRestoVo().getRatings()));

                    if (!list.get(position).getRestoVo().getPhotoReference().trim().isEmpty()) {
                        Picasso.with(context)
                                .load(list.get(position).getRestoVo().getPhotoReference().trim())
                                .resize(Constants.convertDpToPixels(92), Constants.convertDpToPixels(104)).centerCrop()
                                .error(R.drawable.ic_placeholder_restaurant).placeholder(R.drawable.ic_placeholder_restaurant)
                                .into(holder.imageViewRestaurantListAdapterRestaurantPicture, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getRestoVo().getPhotoReference().trim());
                                    }
                                });
                    } else {
                        holder.imageViewRestaurantListAdapterRestaurantPicture.setImageResource(R.drawable.ic_placeholder_restaurant);
                    }

                }
                holder.linearLayoutRestaurantReciever.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(context, RestaurantDetailsActivity.class);
                        intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos).getRestoVo());
                        intent.putExtra("isToShowInviteFriends", false);
                        activity.startActivity(intent);
                    }
                });

            }

//            holder.txtReceiverReceipt.setText(getDateCurrentTimeZone(list.get(position).getTimeStamp()));
            long timestamp = Constants.convertTimestampTo10DigitsOnly(list.get(position).getTimeStamp(), false);
            Log.d("CHAT_ADAPTER", "Message time: " + new Date(timestamp));
            String strMessageDateTimeRelative = DateUtils.getRelativeDateTimeString(context, timestamp, DateUtils.SECOND_IN_MILLIS,
                    DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            holder.txtReceiverReceipt.setText(strMessageDateTimeRelative);

            // Set drawable for read/delivered messages when sent from current user.
            if (list.get(position).getDelivered().trim().equalsIgnoreCase("")) {
                holder.txtReceiverReceipt.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        context.getResources().getDrawable(R.drawable.ic_right_tick_single_black_small), null);
//                context.getResources().getDrawable(R.drawable.ic_clock_black_small)
            } else if (!list.get(position).getReciept().trim().equalsIgnoreCase("") && !list.get(position).getDelivered().equalsIgnoreCase("")) {
                holder.txtReceiverReceipt.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        context.getResources().getDrawable(R.drawable.ic_right_tick_double_red_small), null);
            } else {
                holder.txtReceiverReceipt.setCompoundDrawablesWithIntrinsicBounds(null, null,
                        context.getResources().getDrawable(R.drawable.ic_right_tick_single_red_small), null);
            }

//            if (position != 0 && list.get(position).get("isFromSender").trim().equalsIgnoreCase(list.get(position - 1).get("isFromSender").trim())) {
//                holder.imgReceiverProfilePic.setImageResource(0);
//            } else {
//                holder.imgReceiverProfilePic.setImageResource(R.drawable.shape_red_primary_circle_small);
//                /*if (!list.get(position).get("url").trim().isEmpty()) {
//                    Picasso.with(context).load(activity.listUsers.get(activity.posSelectedUsersList).get("url").trim())
//                            .resize(Constants.convertDpToPixels(72), Constants.convertDpToPixels(72)).centerCrop()
//                            .into(holder.imgReceiverProfilePic, new Callback() {
//                                @Override
//                                public void onSuccess() {
//
//                                }
//
//                                @Override
//                                public void onError() {
//                                    Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).get("url").trim());
//                                }
//                            });
//                }*/
//            }

        }


      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/

        viewHolder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                list.get(activity.posSelectedUsersList).put("isSelected", "0");
//                notifyItemChanged(activity.posSelectedUsersList);
//                activity.posSelectedUsersList = pos;
//                list.get(pos).put("isSelected", "1");
//                notifyItemChanged(pos);
//                notifyDataSetChanged();
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtSenderMessage, txtReceiverMessage, txtReceiverReceipt;
        public LinearLayout linearRoot, linearSenderView, linearReceiverView;
        public CircularImageView imgSenderProfilePic, imgReceiverProfilePic;
        public ImageView textViewChatListAdapterSenderMessageImage, textViewChatListAdapterRecieverMessageImage;
        public ProgressBar ProgressBarProfileImage, ProgressBarProfileImageSender;

        public LinearLayout linearLayoutRestaurantReciever;
        public TextView textViewRestaurantListAdapterRestaurantTimings, textViewRestaurantListAdapterRestaurantAddress, textViewRestaurantListAdapterRestaurantRatings,
                textViewRestaurantListAdapterRestaurantName;
        public RatingBar ratingBarRestaurantListAdapterRestaurantsRatings;
        public ImageView imageViewRestaurantListAdapterRestaurantPicture;

        public LinearLayout linearLayoutRestaurantSender;
        public TextView textViewRestaurantListAdapterRestaurantTimingsSender, textViewRestaurantListAdapterRestaurantAddressSender,
                textViewRestaurantListAdapterRestaurantRatingsSender, textViewRestaurantListAdapterRestaurantNameSender;
        public RatingBar ratingBarRestaurantListAdapterRestaurantsRatingsSender;
        public ImageView imageViewRestaurantListAdapterRestaurantPictureSender;

        public RelativeLayout relativeSenderChatRoot, relativeReceiverChatRoot;

        public ViewHolder(View view) {
            super(view);
            txtSenderMessage = (TextView) view.findViewById(R.id.textViewChatListAdapterSenderMessage);
            txtReceiverMessage = (TextView) view.findViewById(R.id.textViewChatListAdapterReceiverMessage);
            txtReceiverReceipt = (TextView) view.findViewById(R.id.textViewChatListAdapterReceiverMessageReceipt);
            imgSenderProfilePic = (CircularImageView) view.findViewById(R.id.imageViewChatListAdapterSenderProfilePic);
            imgReceiverProfilePic = (CircularImageView) view.findViewById(R.id.imageViewChatListAdapterReceiverProfilePic);
            linearRoot = (LinearLayout) view.findViewById(R.id.linearLayoutChatListAdapterRoot);
            linearSenderView = (LinearLayout) view.findViewById(R.id.linearLayoutChatListAdapterSenderView);
            linearReceiverView = (LinearLayout) view.findViewById(R.id.linearLayoutChatListAdapterReceiverView);
            textViewChatListAdapterRecieverMessageImage = (ImageView) view.findViewById(R.id.textViewChatListAdapterRecieverMessageImage);
            textViewChatListAdapterSenderMessageImage = (ImageView) view.findViewById(R.id.textViewChatListAdapterSenderMessageImage);
            ProgressBarProfileImage = (ProgressBar) view.findViewById(R.id.ProgressBarProfileImage);
            ProgressBarProfileImageSender = (ProgressBar) view.findViewById(R.id.ProgressBarProfileImageSender);

            textViewRestaurantListAdapterRestaurantName = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantName);
            textViewRestaurantListAdapterRestaurantRatings = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantRatings);
            textViewRestaurantListAdapterRestaurantAddress = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantAddress);
            textViewRestaurantListAdapterRestaurantTimings = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantTimings);
            imageViewRestaurantListAdapterRestaurantPicture = (ImageView) view.findViewById(R.id.imageViewRestaurantListAdapterRestaurantPicture);
            ratingBarRestaurantListAdapterRestaurantsRatings = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListAdapterRestaurantsRatings);
            linearLayoutRestaurantReciever = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantReciever);

            textViewRestaurantListAdapterRestaurantNameSender = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantNameSender);
            textViewRestaurantListAdapterRestaurantRatingsSender = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantRatingsSender);
            textViewRestaurantListAdapterRestaurantAddressSender = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantAddressSender);
            textViewRestaurantListAdapterRestaurantTimingsSender = (TextView) view.findViewById(R.id.textViewRestaurantListAdapterRestaurantTimingsSender);
            imageViewRestaurantListAdapterRestaurantPictureSender = (ImageView) view.findViewById(R.id.imageViewRestaurantListAdapterRestaurantPictureSender);
            ratingBarRestaurantListAdapterRestaurantsRatingsSender = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListAdapterRestaurantsRatingsSender);
            linearLayoutRestaurantSender = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantSender);

            relativeSenderChatRoot = (RelativeLayout) view.findViewById(R.id.relativeSenderChatRoot);
            relativeReceiverChatRoot = (RelativeLayout) view.findViewById(R.id.relativeReceiverChatRoot);

            activity.setAllTypefaceMontserratRegular(view);
        }
    }

    public void setGreyScale(View v, boolean greyscale) {
        if (greyscale) {
            // Create a paint object with 0 saturation (black and white)
            ColorMatrix cm = new ColorMatrix();
            cm.setSaturation(0);
            Paint greyscalePaint = new Paint();
            greyscalePaint.setColorFilter(new ColorMatrixColorFilter(cm));
            // Create a hardware layer with the greyscale paint
            v.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
        } else {
            // Remove the hardware layer
            v.setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("EEE HH:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }
}