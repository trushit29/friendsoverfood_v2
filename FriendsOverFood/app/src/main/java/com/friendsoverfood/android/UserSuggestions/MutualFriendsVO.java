package com.friendsoverfood.android.UserSuggestions;

import java.io.Serializable;

/**
 * Created by ashish on 06/01/17.
 */

public class MutualFriendsVO implements Serializable {
    public String Name="",Url="";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
