package com.friendsoverfood.android.EditProfileNew;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

/**
 * Created by ashish on 05/01/17.
 */

public class PreferencesFragment extends Fragment implements View.OnClickListener {

    public EditProfileNewActivity activity;
    public int mMaxValue = 100, mMinValue = 0, mMinAge = 40, mMaxAge = 80;
    public int distanceSelected = 0;
    public final double KM_TO_MILE = 0.621371;
    public final String TAG = "PreferenceFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated properly.

        activity = (EditProfileNewActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_setting_layout, container, false);

        activity.textViewDistance = (TextView) view.findViewById(R.id.textViewDistance);
        activity.textViewAgeRange = (TextView) view.findViewById(R.id.textViewAgeRange);
        activity.textViewShowDistanceIn = (TextView) view.findViewById(R.id.textViewShowDistanceIn);
        activity.textViewLincenses = (TextView) view.findViewById(R.id.textViewLincenses);
        activity.textViewPrivayPolicy = (TextView) view.findViewById(R.id.textViewPrivayPolicy);
        activity.textViewTerms = (TextView) view.findViewById(R.id.textViewTerms);
        activity.txtViewShowMe = (TextView) view.findViewById(R.id.txtViewShowMe);
        activity.imgDummyPreferences = (ImageView) view.findViewById(R.id.imageViewPreferencesFragmentDummy);

        /*activity.switchMale = (SwitchCompat) view.findViewById(R.id.switchMale);
        activity.switchFeMale = (SwitchCompat) view.findViewById(R.id.switchFeMale);
        activity.switchLgbt = (SwitchCompat) view.findViewById(R.id.switchLgbt);*/
        activity.btnMalePreferences = (Button) view.findViewById(R.id.btnMalePreferences);
        activity.btnFemalePreferences = (Button) view.findViewById(R.id.btnFemalePreferences);
        activity.btnLgbtPreferences = (Button) view.findViewById(R.id.btnLgbtPreferences);
        activity.switchNewMatches = (SwitchCompat) view.findViewById(R.id.switchNewMatches);
        activity.switchMessages = (SwitchCompat) view.findViewById(R.id.switchMessages);
        activity.switchLocationShowHide = (SwitchCompat) view.findViewById(R.id.switchLocationShowHide);

        activity.seekBarActivityDiscoveryPreferencesSearchDistance = (SeekBar) view.findViewById(R.id.seekBarActivityDiscoveryPreferencesSearchDistance);

        activity.llhelp = (LinearLayout) view.findViewById(R.id.llhelp);
        activity.llShareFOF = (LinearLayout) view.findViewById(R.id.llShareFOF);
        activity.llLogout = (LinearLayout) view.findViewById(R.id.llLogout);

        activity.btnKilometer = (Button) view.findViewById(R.id.btnKilometer);
        activity.btnMile = (Button) view.findViewById(R.id.btnMile);

        activity.rangebarAge = (RangeBar) view.findViewById(R.id.rangebarAge);

        activity.btnKilometer.setSelected(true);
        if (activity.profile != null)
            distanceSelected = Integer.valueOf(activity.profile.getSearch_distance());
        // Set max limit of range bar to 161 Kms @MAX_DISTANCE_RANGE_KM
        activity.seekBarActivityDiscoveryPreferencesSearchDistance.setMax(activity.MAX_DISTANCE_RANGE_KM);

        activity.seekBarActivityDiscoveryPreferencesSearchDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) { // If the changes are from the User only then accept the changes
                    /* Instead of -> if (progress != 0) { */
//                    distanceSelected = Integer.valueOf(activity.profile.getSearch_distance());
                    if (activity.isRevertUnsavedChanges) {
                        activity.isRevertUnsavedChanges = false;
                        activity.isUnsavedChanges = false;
                    } else {
                        activity.isUnsavedChanges = true;
                    }

                    distanceSelected = (int) ((float) progress / 100 * (mMaxValue - mMinValue) + mMinValue);
                    distanceSelected *= 10;
                    distanceSelected = Math.round(distanceSelected);
                    distanceSelected /= 10;
//                    Log.i("distanceSelected", "" + distanceSelected);
                }

                if (activity.profile.getDistance_unit().trim().toLowerCase()
                        .contains(getResources().getString(R.string.distance_unit_km_value_server))) {
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                    SharedPreferenceUtil.save();

                    activity.textViewDistance.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));
                    if (!activity.isfromButton)
                        activity.profile.setSearch_distance(String.valueOf(distanceSelected));
                } else {
                    /*convertedtoMiValue = Constants.convertKmtoMi(mCurrentValue);
                    txtSearchDistance.setText(Integer.toString(convertedtoMiValue) + " Mi");*/
                    activity.textViewDistance.setText(Integer.toString(distanceSelected) + " " + getString(R.string.prefer_distance_mi));
                    if (!activity.isfromButton)
                        activity.profile.setSearch_distance(String.valueOf(distanceSelected));
//                    distanceSelected = Constants.convertMitoKm(distanceSelected);
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                    SharedPreferenceUtil.save();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        activity.rangebarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.age1 = leftPinValue;
                activity.age2 = rightPinValue;
                activity.profile.setSearch_min_age(String.valueOf(activity.age1));
                activity.profile.setSearch_max_age(String.valueOf(activity.age2));
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE1, aage1.trim());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE2, age2.trim());
//                SharedPreferenceUtil.save();save
                activity.textViewAgeRange.setText(activity.age1 + " - " + activity.age2);

            }

        });
        Bundle args = getArguments();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideKeyboard(activity);
        activity.btnKilometer.setOnClickListener(activity);
        activity.btnMile.setOnClickListener(activity);
        /*activity.switchMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }

                if (isChecked) {
                    if (activity.switchFeMale.isChecked() && activity.switchLgbt.isChecked()) {
                        activity.profile.setShowme(getString(R.string.gender_all_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_all));
                    } else {
                        if (activity.switchFeMale.isChecked()) {

                        } else if (activity.switchLgbt.isChecked()) {

                        } else {
                            activity.profile.setShowme(getString(R.string.gender_male_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male));
                        }
                    }
                } else {
                    activity.switchFeMale.setChecked(true);
                    activity.txtViewShowMe.setText(getString(R.string.gender_female));
                    activity.profile.setShowme(getString(R.string.gender_female_value));

                }

            }
        });*/

        /*activity.switchFeMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }

                if (isChecked) {
                    if (activity.switchMale.isChecked()) {
                        activity.profile.setShowme(getString(R.string.gender_all_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_all));
                    } else {
                        activity.profile.setShowme(getString(R.string.gender_female_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_female));
                    }
                } else {
                    activity.switchMale.setChecked(true);
                    activity.txtViewShowMe.setText(getString(R.string.gender_male));
                    activity.profile.setShowme(getString(R.string.gender_male_value));
                }
            }
        });*/

        activity.btnMalePreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }

                if (activity.btnMalePreferences.isSelected()) {
                    if (activity.btnFemalePreferences.isSelected() && activity.btnLgbtPreferences.isSelected()) {
                        activity.btnMalePreferences.setSelected(false);

                        activity.profile.setShowme(getString(R.string.gender_female_lgbt_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_female_lgbt));
                    } else {
                        if (activity.btnFemalePreferences.isSelected()) {
                            activity.btnMalePreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_female_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_female));
                        } else if (activity.btnLgbtPreferences.isSelected()) {
                            activity.btnMalePreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_lgbt));
                        }
                    }
                } else {
                    activity.btnMalePreferences.setSelected(true);
                    if (activity.btnFemalePreferences.isSelected() && activity.btnLgbtPreferences.isSelected()) {
                        activity.profile.setShowme(getString(R.string.gender_all_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_all));
                    } else {
                        if (activity.btnFemalePreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_male_female_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male_female));
                        } else if (activity.btnLgbtPreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_male_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male_lgbt));
                        } else {
                            activity.profile.setShowme(getString(R.string.gender_male_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male));
                        }
                    }
                }
            }
        });

        activity.btnFemalePreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }

                if (activity.btnFemalePreferences.isSelected()) {
                    if (activity.btnMalePreferences.isSelected() && activity.btnLgbtPreferences.isSelected()) {
                        activity.btnFemalePreferences.setSelected(false);

                        activity.profile.setShowme(getString(R.string.gender_male_lgbt_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_male_lgbt));
                    } else {
                        if (activity.btnMalePreferences.isSelected()) {
                            activity.btnFemalePreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_male_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male));
                        } else if (activity.btnLgbtPreferences.isSelected()) {
                            activity.btnFemalePreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_lgbt));
                        }
                    }
                } else {
                    activity.btnFemalePreferences.setSelected(true);
                    if (activity.btnMalePreferences.isSelected() && activity.btnLgbtPreferences.isSelected()) {
                        activity.profile.setShowme(getString(R.string.gender_all_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_all));
                    } else {
                        if (activity.btnMalePreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_male_female_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male_female));
                        } else if (activity.btnLgbtPreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_female_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_female_lgbt));
                        } else {
                            activity.profile.setShowme(getString(R.string.gender_female_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_female));
                        }
                    }
                }
            }
        });

        activity.btnLgbtPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }

                if (activity.btnLgbtPreferences.isSelected()) {
                    if (activity.btnMalePreferences.isSelected() && activity.btnFemalePreferences.isSelected()) {
                        activity.btnLgbtPreferences.setSelected(false);

                        activity.profile.setShowme(getString(R.string.gender_male_female_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_male_female));
                    } else {
                        if (activity.btnMalePreferences.isSelected()) {
                            activity.btnLgbtPreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_male_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male));
                        } else if (activity.btnFemalePreferences.isSelected()) {
                            activity.btnLgbtPreferences.setSelected(false);
                            activity.profile.setShowme(getString(R.string.gender_female_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_female));
                        }
                    }
                } else {
                    activity.btnLgbtPreferences.setSelected(true);
                    if (activity.btnMalePreferences.isSelected() && activity.btnLgbtPreferences.isSelected()) {
                        activity.profile.setShowme(getString(R.string.gender_all_value));
                        activity.txtViewShowMe.setText(getString(R.string.gender_all));
                    } else {
                        if (activity.btnMalePreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_male_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_male_lgbt));
                        } else if (activity.btnFemalePreferences.isSelected()) {
                            activity.profile.setShowme(getString(R.string.gender_female_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_female_lgbt));
                        } else {
                            activity.profile.setShowme(getString(R.string.gender_lgbt_value));
                            activity.txtViewShowMe.setText(getString(R.string.gender_lgbt));
                        }
                    }
                }
            }
        });


        activity.switchLocationShowHide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.profile.setIs_show_location(isChecked);
            }
        });

        activity.switchMessages.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.profile.setIs_receive_messages_notifications(isChecked);
            }
        });

        activity.switchNewMatches.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activity.isRevertUnsavedChanges) {
                    activity.isRevertUnsavedChanges = false;
                    activity.isUnsavedChanges = false;
                } else {
                    activity.isUnsavedChanges = true;
                }
                activity.profile.setIs_receive_invitation_notifications(isChecked);
            }
        });

        activity.showValueOfPreferences();
        activity.isUnsavedChanges = false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }
}
