package com.friendsoverfood.android.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Trushit on 07/06/16.
 */
public class DBOpenHelper extends SQLiteOpenHelper {

    // DB Version 1 created on 10/19/2016
    public static final int DB_VERSION = 1;

    /**
     * Database name to be created with.
     */
    public static final String DB_NAME = "FOF";

    // private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
    // + " ( contact TEXT, time TEXT, sender TEXT, body TEXT)";
    // private static final String CREATE_TABLE_CHAT =
    // "CREATE TABLE  chats  ( name TEXT, last TEXT )";

    /*private static final String CREATE_TABLE_CARDS = "CREATE TABLE " + DbConstants.TABLE_NAME_CARDS + " ("
            + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_NAME + " TEXT, "
            + DbConstants.COL_NAME_IMAGE_URL + " TEXT, " + DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT, " + DbConstants.COL_NAME_COMPANIES_ID + " TEXT);";

    private static final String CREATE_TABLE_REWARDS = "CREATE TABLE " + DbConstants.TABLE_NAME_REWARDS + " ("
            + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_NAME + " TEXT, "
            + DbConstants.COL_NAME_IMAGE_URL + " TEXT, "+ DbConstants.COL_NAME_CARD_ID + " TEXT, "  + DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT);";

    private static final String CREATE_TABLE_USER_CARDS = "CREATE TABLE " + DbConstants.TABLE_NAME_USER_CARDS + " ("
            + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_NAME + " TEXT, "
            + DbConstants.COL_NAME_IMAGE_URL + " TEXT, " + DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT, " + DbConstants.COL_REWARD_INFO + " TEXT, " + DbConstants.COL_NAME_COMPANIES_ID + " TEXT);";



   *//* private static final String CREATE_TABLE_USER_REWARDS = "CREATE TABLE " + DbConstants.TABLE_NAME_USER_REWARDS + " ("
            + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_USER_ID + " TEXT, "
            + DbConstants.COL_NAME_CARD_ID + " TEXT, " + DbConstants.COL_NAME_REWARD_ID + " TEXT, "+ DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT);";*//*
   private static final String CREATE_TABLE_USER_REWARDS = "CREATE TABLE " + DbConstants.TABLE_NAME_USER_REWARDS + " ("
           + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_NAME + " TEXT, "
           + DbConstants.COL_NAME_IMAGE_URL + " TEXT, " + DbConstants.COL_NAME_CARD_ID + " TEXT, "+ DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT);";

    private static final String CREATE_TABLE_USER_STAMPS = "CREATE TABLE " + DbConstants.TABLE_NAME_USER_STAMPS + " ("
            + DbConstants.COL_NAME_ID + " TEXT PRIMARY KEY NOT NULL UNIQUE, " + DbConstants.COL_NAME_USER_ID + " TEXT, "
            + DbConstants.COL_NAME_CARD_ID + " TEXT, " + DbConstants.COL_NAME_STATUS + " TEXT, "+ DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT, " + DbConstants.COL_STAMP + " TEXT);";

    private static final String CREATE_TABLE_NOIFICATION = "CREATE TABLE " + DbConstants.TABLE_NAME_USER_NOTIFICATION + " ("
            + DbConstants.COL_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + DbConstants.COL_NAME_CARD_ID + " TEXT, "+   DbConstants.COL_NAME_TYPE + " TEXT, "+ DbConstants.COL_NAME_MESSAGE + " TEXT, "
            + DbConstants.COL_NAME_IMAGE_URL + " TEXT, " + DbConstants.COL_NAME_CREATEATE + " TEXT, " + DbConstants.COL_NAME_UPDATE_AT + " TEXT);";*/

    public DBOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /*sqLiteDatabase.execSQL(CREATE_TABLE_CARDS);
        sqLiteDatabase.execSQL(CREATE_TABLE_REWARDS);
        sqLiteDatabase.execSQL(CREATE_TABLE_USER_CARDS);
        sqLiteDatabase.execSQL(CREATE_TABLE_USER_REWARDS);
        sqLiteDatabase.execSQL(CREATE_TABLE_USER_STAMPS);
        sqLiteDatabase.execSQL(CREATE_TABLE_NOIFICATION);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // http://blog.adamsbros.org/2012/02/28/upgrade-android-sqlite-database/
        // http://stackoverflow.com/questions/8133597/android-upgrading-db-version-and-adding-new-table
        // http://stackoverflow.com/questions/4253804/insert-new-column-into-table-in-sqlite
        // http://stackoverflow.com/questions/19793004/android-sqlite-database-why-drop-table-and-recreate-on-upgrade
        int upgradeTo = oldVersion + 1;
        while (upgradeTo <= newVersion) {
            switch (upgradeTo) {
                // Upgrade database fields here
               /* case 2:
                    Log.d("SQLITE_HELPER", "Upgrading database to version: " + upgradeTo);
                    String strAlterIndicatorTable = "ALTER TABLE " + Constants.TABLE_NAME_INDICATOR + " ADD COLUMN " + Constants.COL_NAME_TARGET_VALUE
                            + " REAL NOT NULL DEFAULT(0)";
                    sqLiteDatabase.execSQL(strAlterIndicatorTable);
                    break;*/
            }
            upgradeTo++;
        }
    }
}
