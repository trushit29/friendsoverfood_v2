package com.friendsoverfood.android.FriendRequest;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Http.HttpCallback;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.UserSuggestions.SingleUserDetailActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Trushit on 27/02/17.
 */

public class PendingRequestListAdapter extends BaseAdapter implements HttpCallback {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<RequestVO> list;
    private Intent intent;
    private PendingRequestActivity activity;
    private final String TAG = "PENDING_REQUESTS_ADAPTER";
    private int posSelected = 0;

    public PendingRequestListAdapter(Context context, ArrayList<RequestVO> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.activity = (PendingRequestActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_pending_request_list_adapter, parent, false);

            holder.linearRoot = (LinearLayout) convertView.findViewById(R.id.linearLayoutPendingRequestListAdapterRoot);
            holder.imgProfilePic = (CircularImageView) convertView.findViewById(R.id.imageViewPendingRequestListAdapterUserProfilePic);
            holder.imgAccept = (ImageView) convertView.findViewById(R.id.imageViewPendingRequestListAdapterAccept);
            holder.imgReject = (ImageView) convertView.findViewById(R.id.imageViewPendingRequestListAdapterReject);
            holder.txtName = (TextView) convertView.findViewById(R.id.textViewPendingRequestListAdapterUserName);
            holder.txtLocation = (TextView) convertView.findViewById(R.id.textViewPendingRequestListAdapterUserLocation);

            activity.setAllTypefaceMontserratRegular(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String age = !list.get(position).getDob().trim().isEmpty() ? Constants.convertAgeFromDateOfBirth(list.get(position).getDob().trim()) : "";

        holder.txtName.setText((!list.get(position).getFirst_name().trim().isEmpty() ? list.get(position).getFirst_name().trim() + " " : "")
                + "" + list.get(position).getLast_name().trim() + (!age.trim().isEmpty() ? ", " + age.trim() : ""));
        holder.txtLocation.setText(list.get(position).getLocation_string().trim());

        final ViewHolder finalHolder = holder;
        final int pos = position;

        if (!list.get(position).getProfilepic1().trim().isEmpty() && !activity.appendUrl(list.get(position).getProfilepic1()).trim().isEmpty()) {
            Picasso.with(context).load(activity.appendUrl(list.get(position).getProfilepic1().trim()))
//                    .error(R.drawable.ic_user_profile_avatar).placeholder(R.drawable.ic_user_profile_avatar)
                    .placeholder(list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                            : R.drawable.ic_placeholder_user_avatar_female_54)
                    .error(list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                            : R.drawable.ic_placeholder_user_avatar_female_54)
                    .resize(Constants.convertDpToPixels(50), Constants.convertDpToPixels(50)).centerCrop()
                    .into(holder.imgProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + activity.appendUrl(list.get(position).getProfilepic1().trim()));
                        }
                    });
        } else {
//            holder.imgProfilePic.setImageResource(R.drawable.ic_user_profile_avatar);
            if (list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                holder.imgProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_male_54);
            } else {
                holder.imgProfilePic.setImageResource(R.drawable.ic_placeholder_user_avatar_female_54);
            }
        }

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Set this restaurant on map as cluster or cluster item.
                intent = new Intent(context, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, list.get(pos).getId());
//                View sharedViewImage = holder.imgProfilePic;
                View sharedViewImage = finalHolder.linearRoot;
                String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedViewImage, transitionNameImage);
                    activity.startActivity(intent, transitionActivityOptions.toBundle());
                } else {
                    activity.startActivity(intent);
                }
            }
        });

        holder.imgAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posSelected = pos;
                callAcceptFriendRequest(list.get(pos).getId().trim());
            }
        });

        holder.imgReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                posSelected = pos;
                callRejectFriendRequest(list.get(pos).getId().trim());
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public LinearLayout linearRoot;
        public CircularImageView imgProfilePic;
        public ImageView imgAccept, imgReject;
        public TextView txtName, txtLocation;
    }

    private void callRejectFriendRequest(String otherUserID) {
        activity.showProgress(context.getResources().getString(R.string.loading));
        activity.params = new HashMap<>();
        activity.params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_deletefriendrequest));
        activity.params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        activity.params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        activity.params.put(context.getResources().getString(R.string.api_param_key_friendid), otherUserID);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), activity.params,
                Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST, PendingRequestListAdapter.this);
    }

    private void callAcceptFriendRequest(String otherUserID) {
        activity.showProgress(context.getResources().getString(R.string.loading));
        activity.params = new HashMap<>();
        activity.params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_acceptfriendrequest));
        activity.params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        activity.params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        activity.params.put(context.getResources().getString(R.string.api_param_key_friendid), otherUserID);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), activity.params,
                Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST, PendingRequestListAdapter.this);
    }

    @Override
    public void onResponse(String response, int action) {
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_ACCEPT_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {

                        }

                        /*// Remove list from
                        list.remove(posSelected);
                        notifyDataSetChanged();*/

                        /*if (list.size() == 0) {
                            activity.listViewPendingRequests.setVisibility(View.GONE);
                            activity.linearPlaceHolder.setVisibility(View.VISIBLE);
                        }*/

                        // activity.stopProgress();
                        callFriendsListApi();

//                        activity.showSnackBarMessageOnly(context.getString(R.string.you_are_now_friends));
                    } else {
                        activity.stopProgress();
                        activity.showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST) {
                Log.d(TAG, "RESPONSE_REJECT_FRIEND_REQUEST_API: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {

                        }

                        // Remove list from
                        list.remove(posSelected);
                        notifyDataSetChanged();

                        if (list.size() == 0) {
                            activity.listViewPendingRequests.setVisibility(View.GONE);
                            activity.linearPlaceHolder.setVisibility(View.VISIBLE);
                        }

                        activity.stopProgress();
//                        showSnackBarMessageOnly(context.getString(R.string.friend_request_rejected));
                    } else {
                        activity.stopProgress();
                        activity.showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.stopProgress();
                    activity.showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                // Fetch all the match details from Shared Preferences here.
                JSONObject jsonObjectResponse = null;
                ArrayList<ChatListVO> matchList = new ArrayList<>();
                try {
                    jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                            ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                    Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                    JSONObject settings = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_settings))
                            ? jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings)) : new JSONObject();
                    boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    if (status) {
                        JSONArray data = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();

                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            ChatListVO item = new ChatListVO();
                            try {
                                dataInner = data.getJSONObject(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String matchId = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            String friend1 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend1))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend1)).trim() : "";
                            String friend2 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend2))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend2)).trim() : "";
                            String statusFriend = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                                    ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";
                            item.setMatchid(matchId);
                            item.setFriend1(friend1);
                            item.setFriend2(friend2);

                            JSONArray details = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_details))
                                    ? dataInner.getJSONArray(context.getResources().getString(R.string.api_response_param_key_details)) : new JSONArray();
                            for (int j = 0; j < details.length(); j++) {

                                JSONObject detailInner = details.getJSONObject(j);
                                String userId = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                String first_name = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_first_name))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_first_name)).trim() : "";
                                String lastName = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_last_name))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_last_name)).trim() : "";
                                String profilePic = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                String gender = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_gender))
                                        ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_gender)).trim() : "";
                                item.setUserId(userId);
                                item.setFirstName(first_name);
                                item.setLastName(lastName);
                                item.setProfilePic(profilePic);
                                item.setGender(gender);
                                item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                            }
                            matchList.add(item);

                        }

                        // Get users VO object here to pass to chat screen using intent.
                        int posUser = -1;
                        for (int i = 0; i < matchList.size(); i++) {
                            Log.d(TAG, "PosSelected Id: " + list.get(posSelected).getId().trim() + ", Match List Id: " + matchList.get(i).getUserId().trim());
                            if (list.get(posSelected).getId().trim().equalsIgnoreCase(matchList.get(i).getUserId().trim())) {
                                posUser = i;
                            }
                        }

                        if (posUser != -1) {

                            final ChatListVO OtherUser = matchList.get(posUser);

                            // Open Chat Screen from here.
                            activity.stopProgress();

                            Intent intent = new Intent(context, ChatDetailsActivity.class);
                            intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                            intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
                            activity.startActivity(intent);
                            activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        } else {
                            activity.stopProgress();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.stopProgress();
                }

                // Remove list from
                list.remove(posSelected);
                notifyDataSetChanged();

                if (list.size() == 0) {
                    activity.listViewPendingRequests.setVisibility(View.GONE);
                    activity.linearPlaceHolder.setVisibility(View.VISIBLE);
                }


                /*stopProgress();*/
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    public void callFriendsListApi() {
//        showProgress(getString(R.string.loading));
        activity.params = new HashMap<>();
        activity.params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        activity.params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        activity.params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), activity.params,
                Constants.ACTION_CODE_API_FRIEND_LIST, PendingRequestListAdapter.this);
    }
}
