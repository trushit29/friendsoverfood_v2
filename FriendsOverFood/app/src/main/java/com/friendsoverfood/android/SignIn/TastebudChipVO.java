package com.friendsoverfood.android.SignIn;

import com.plumillonforge.android.chipview.Chip;

/**
 * Created by Trushit on 26/02/17.
 */

public class TastebudChipVO implements Chip {
    public String name = "", id = "", city = "", zomato_id = "", createdat = "", updatedat = "";
    public int type = 0;


    public TastebudChipVO(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public TastebudChipVO() {
    }

    @Override
    public String getText() {
        return name;
    }

    public TastebudChipVO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZomato_id() {
        return zomato_id;
    }

    public void setZomato_id(String zomato_id) {
        this.zomato_id = zomato_id;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }
}
