package com.friendsoverfood.android;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.google.firebase.database.FirebaseDatabase;
import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.SessionConfiguration;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Trushit on 12/01/16.
 */
public class ApplicationClass extends Application {
    /**
     * Global request queue for Volley
     */
    private RequestQueue mRequestQueue;

    /**
     * Log or request TAG
     */
    public static final String TAG = "VolleyPatterns";

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static ApplicationClass sInstance;
    private static Context context;
    public static SessionConfiguration config;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        context = this;

        // initialize the singleton
        sInstance = this;

        SharedPreferenceUtil.init(this);
        SharedPreferencesTokens.init(this);
        // DatabaseUtil.init(this, getString(R.string.dbname), 1, null);
        generateKey();

        FacebookSdk.sdkInitialize(getApplicationContext());
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        AppEventsLogger.activateApp(this);

        config = new SessionConfiguration.Builder()
                .setClientId("bYx7hSkBXhv8EJSWepYQrwfd9Z4Tsu6k") //This is necessary
                .setRedirectUri("http://localhost")
                .setServerToken("ccgS3UCj7YCN5c69uVclcLe5h9HTnLLup4hDE7nX")
//                .setRedirectUri("YOUR_REDIRECT_URI") //This is necessary if you'll be using implicit grant
                .setEnvironment(SessionConfiguration.Environment.SANDBOX) //Useful for testing your app in the sandbox environment
                .setScopes(Arrays.asList(Scope.PROFILE, Scope.RIDE_WIDGETS)) //Your scopes for authentication here
                .build();

//This is a convenience method and will set the default config to be used in other components without passing it directly.
        UberSdk.initialize(config);

        // TODO Set custom for whole app here. Don't forget to set "android:typeface" as "monospace" in app theme/style for v21 & later.
        FontsOverride.setDefaultFont(this, "DEFAULT", "Montserrat-Regular.otf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Montserrat-Regular.otf");
        FontsOverride.setDefaultFont(this, "SERIF", "Montserrat-Regular.otf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "Montserrat-Regular.otf");
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void generateKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.friendsoverfood.android", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("APPLICATION_CLASS", "Keyhash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized ApplicationClass getInstance() {
        return sInstance;
    }

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
//            mRequestQueue.cancelAll(tag);
            mRequestQueue.cancelAll(tag);
        }
    }

}
