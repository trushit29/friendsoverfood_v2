package com.friendsoverfood.android.ChatNew;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Trushit on 30/08/16.
 */
public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ArrayList<ChatListVO> list;
    private Context context;
    private ChatListActivity activity;
    private Intent intent;

    private final String TAG = "ADAPTER_CHAT_LIST";

    public  ChatListAdapter(Context context, ArrayList<ChatListVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (ChatListActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.adapter_chat_home, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        activity.setAllTypefaceMontserratRegular(viewHolder.LinearLayoutRoot);
        activity.setAllTypefaceMontserratLight(viewHolder.txtLastMessage);
        activity.setAllTypefaceMontserratLight(viewHolder.txtTime);
//        viewHolder.textView.setText(mItems.get(position));
//        viewHolder.txtName.setText(list.get(position).getName().trim());
//        viewHolder.txtDistance.setText(list.get(position).getDistance().trim());
//        viewHolder.txtTasteBuds.setText(list.get(position).getTasteBuds().trim());

      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/
      Log.d(TAG, "Match status: "+list.get(position).getStatus());

        viewHolder.txtViewName.setText(list.get(position).getFirstName() + " " + list.get(position).getLastName());

//        Log.d(TAG, "Last Message sender id: " + list.get(position).getLastMsgSenderId());
        if (list.get(position).getLastMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_a_restaurant))) {
            if (list.get(position).getLastMsgSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                viewHolder.txtLastMessage.setText(context.getResources().getString(R.string.you_sent_a_restaurant));
            } else {
                viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
            }
        } else if (list.get(position).getLastMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_an_image))) {
            if (list.get(position).getLastMsgSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                viewHolder.txtLastMessage.setText(context.getResources().getString(R.string.you_sent_an_image));
            } else {
                viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
            }
        } else {
            viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
        }

        if (list.get(position).getTime() > 0) {
            /*viewHolder.txtTime.setText(getDateCurrentTimeZone(list.get(position).getTime()).trim());*/
            long timestamp = Constants.convertTimestampTo10DigitsOnly(list.get(position).getTime(), false);
//            Log.d(TAG, "Message time: " + new Date(timestamp));
            /*String strMessageDateTimeRelative = DateUtils.getRelativeDateTimeString(context, timestamp, DateUtils.SECOND_IN_MILLIS,
                    DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();*/
            String strMessageDateTimeRelative = DateUtils.getRelativeTimeSpanString(timestamp, new Date().getTime(),
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
            viewHolder.txtTime.setText(strMessageDateTimeRelative);
        } else {
            viewHolder.txtTime.setText("");
        }

        if (list.get(position).getUnreadCount() > 0) {
            viewHolder.txtMessageCount.setVisibility(View.VISIBLE);
            viewHolder.txtMessageCount.setText(String.valueOf(list.get(position).getUnreadCount()));
        } else {
            viewHolder.txtMessageCount.setVisibility(View.GONE);
        }

        final int pos = position;
        final ViewHolder holder = viewHolder;

        if (list.get(position).getProfilePic() != null && !list.get(position).getProfilePic().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getProfilePic())
                    .resize(Constants.convertDpToPixels(54), Constants.convertDpToPixels(54)).centerCrop()
//                    .placeholder(R.drawable.ic_user_profile_avatar)
                    .placeholder(list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                            : R.drawable.ic_placeholder_user_avatar_female_54)
//                    .error(R.drawable.ic_user_profile_avatar)
                    .error(list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male") ? R.drawable.ic_placeholder_user_avatar_male_54
                            : R.drawable.ic_placeholder_user_avatar_female_54)
                    .into(viewHolder.cImageUser, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("CHAT_LIST_ADAPTER", "Error loading profile image: " + list.get(pos).getProfilePic().trim());
                        }
                    });
        } else {
            if (list.get(pos).getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                viewHolder.cImageUser.setImageResource(R.drawable.ic_placeholder_user_avatar_male_54);
            } else {
                viewHolder.cImageUser.setImageResource(R.drawable.ic_placeholder_user_avatar_female_54);
            }
//            viewHolder.cImageUser.setImageResource(R.drawable.ic_user_profile_avatar);
        }

        if (list.get(position).getLastMsg().trim().isEmpty()) {
            holder.txtLastMessage.setVisibility(View.VISIBLE);
            holder.txtLastMessage.setText("");

            if (holder.listenerStatus == null) {
                holder.listenerStatus = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                        Log.d(TAG, "Timestamp snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                        try {
                            StatusVO status = dataSnapshot.getValue(StatusVO.class);
                            if (status.getSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                long lastSeenCurrentUser = Long.valueOf(status.getLastSeen());
//                                Log.d(TAG, "Current user last seen: " + lastSeenCurrentUser);

                                list.get(pos).setTimestampLastSeen(lastSeenCurrentUser);
                                getLastMessageFromChat(list.get(pos).getMatchid().trim());
                                getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                        Log.d(TAG, "Timestamp snapshot: " + dataSnapshot.toString() + ", Key: " + dataSnapshot.getKey().trim());
                        try {
                            StatusVO status = dataSnapshot.getValue(StatusVO.class);
                            if (status.getSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                long lastSeenCurrentUser = Long.valueOf(status.getLastSeen());
//                                Log.d(TAG, "Current user last seen: " + lastSeenCurrentUser);

                                list.get(pos).setTimestampLastSeen(lastSeenCurrentUser);
                                getLastMessageFromChat(list.get(pos).getMatchid().trim());
                                getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                            } else {
                                long lastSeenCurrentUser = list.get(pos).getTimestampLastSeen();
                                getLastMessageFromChat(list.get(pos).getMatchid().trim());
                                getUnreadMessagesFromLastSeen(lastSeenCurrentUser, list.get(pos).getMatchid().trim());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };

                holder.mDatabaseStatus = FirebaseDatabase.getInstance().getReference("status");
                holder.mDatabaseStatus.child(list.get(position).getMatchid().trim()).addChildEventListener(holder.listenerStatus);
            }
//            .child(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
        } else {
            viewHolder.txtLastMessage.setVisibility(View.VISIBLE);
            /*viewHolder.txtLastMessage.setText(list.get(position).getLastMsg().trim());*/
            if (list.get(position).getLastMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_a_restaurant))) {
                if (list.get(position).getLastMsgSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                    viewHolder.txtLastMessage.setText(context.getResources().getString(R.string.you_sent_a_restaurant));
                } else {
                    viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
                }
            } else if (list.get(position).getLastMsg().trim().equalsIgnoreCase(context.getResources().getString(R.string.sent_you_an_image))) {
                if (list.get(position).getLastMsgSenderId().trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                    viewHolder.txtLastMessage.setText(context.getResources().getString(R.string.you_sent_an_image));
                } else {
                    viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
                }
            } else {
                viewHolder.txtLastMessage.setText(list.get(position).getLastMsg());
            }
        }

        viewHolder.LinearLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d(TAG, "match id from adapter: " + list.get(pos).getMatchid());
                // Update last time stamp to current time stamp & reset unread count for current position.
                Calendar cal = Calendar.getInstance();
                list.get(pos).setTimestampLastSeen(Constants.convertTimestampTo10DigitsOnly(cal.getTimeInMillis(), true));
                list.get(pos).setUnreadCount(0);
                notifyItemChanged(pos);

                Log.d(TAG, "Friend id selected status: "+list.get(pos).getStatus());

                intent = new Intent(context, ChatDetailsActivity.class);
                intent.putExtra(Constants.INTENT_CHAT_SELECTED, list.get(pos));
                intent.putExtra(Constants.INTENT_CHAT_LIST, list);
                intent.putExtra("FromChatList", true);
//                    context.startActivity(intent);
                activity.startActivityForResult(intent, Constants.ACTION_CODE_CHAT_USERS_LIST);
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        //        public TextView txtName, txtDistance, txtTasteBuds;
//        public ImageView imgSelectChecked, imgSelectUnChecked, imgRestaurant, imgDummy;
//        public RatingBar ratingBarRestaurant;
        public TextView txtViewName, txtMessageCount, txtLastMessage, txtTime;
        public CircleImageView cImageUser;
        public LinearLayout LinearLayoutRoot;
        public ChildEventListener listenerStatus;
        public DatabaseReference mDatabaseStatus;
//        public RelativeLayout relativeRootSelect;
//        public CardView cardViewRoot;

        public ViewHolder(View view) {
            super(view);
            LinearLayoutRoot = (LinearLayout) view.findViewById(R.id.LinearLayoutRoot);
            cImageUser = (CircleImageView) view.findViewById(R.id.cImageUser);
            txtViewName = (TextView) view.findViewById(R.id.txtViewName);
            txtMessageCount = (TextView) view.findViewById(R.id.textViewChatHomeAdapterMessagesCount);
            txtLastMessage = (TextView) view.findViewById(R.id.textViewChatHomeAdapterLastMessage);
            txtTime = (TextView) view.findViewById(R.id.textViewChatHomeAdapterMessageTime);
        }
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("EEE HH:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

    public void getLastMessageFromChat(final String matchId) {
        Query lastMessageQuery = activity.mDatabaseMessages.child(matchId.trim()).orderByChild("timeStamp").limitToLast(1);
//        .limitToLast(1)
//            Query lastMessageQuery = activity.mDatabaseMessages.child("a3832fdd931de9b778f0bcd2389bca81").limitToLast(1);
        lastMessageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                Log.d(TAG, "Last message added is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            list.get(i).setLastMsg(msg.getMsg().trim());
                            list.get(i).setLastMsgSenderId(msg.getSenderId().trim());
                            list.get(i).setLastMsgReceiverId(msg.getRecieverId().trim());
//                            list.get(i).setTime(getDateCurrentTimeZone(msg.getTimeStamp()));
                            list.get(i).setTime(msg.getTimeStamp());
                            notifyItemChanged(i);
                            notifyDataSetChanged();
                        }
                    }

                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                        // Update adapter & list by applying sort here.
                        /*activity.sortListNotifyChanges();*/
                        int sizeBefore = list.size();
                        Collections.sort(list, new ComparatorLongDescending());
                        /*Collections.sort(list);*/
                        notifyItemRangeChanged(0, sizeBefore);
                        notifyItemRangeChanged(0, sizeBefore, null);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Log.d(TAG, "Last Message changed is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            list.get(i).setLastMsg(msg.getMsg().trim());
                            list.get(i).setLastMsgSenderId(msg.getSenderId().trim());
                            list.get(i).setLastMsgReceiverId(msg.getRecieverId().trim());
                            /*list.get(i).setTime(getDateCurrentTimeZone(msg.getTimeStamp()));*/
                            list.get(i).setTime(msg.getTimeStamp());
                            notifyItemChanged(i);
                            notifyDataSetChanged();
                        }
                    }

                    if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                        // Update adapter & list by applying sort here.
                        /*activity.sortListNotifyChanges();*/
                        int sizeBefore = list.size();
                       /* Collections.sort(list);*/
                        Collections.sort(list, new ComparatorLongDescending());
                        notifyItemRangeChanged(0, sizeBefore);
                        notifyItemRangeChanged(0, sizeBefore, null);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        });
    }

    public void getUnreadMessagesFromLastSeen(final long lastSeenTimeStamp, final String matchId) {
        Query lastMessageQuery = activity.mDatabaseMessages.child(matchId.trim()).orderByChild("timeStamp").startAt(lastSeenTimeStamp);
//        .limitToLast(1)
//            Query lastMessageQuery = activity.mDatabaseMessages.child("a3832fdd931de9b778f0bcd2389bca81").limitToLast(1);
        lastMessageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                Log.d(TAG, "Update count added is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    /*list.get(pos).setLastMsg(msg.getMsg().trim());
                    list.get(pos).setTime(getDateCurrentTimeZone(msg.getTimeStamp()));*/

                    // increase count when message received for chat
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            if (!list.get(i).getUnreadMessageKeys().contains(dataSnapshot.getKey())) {
                                list.get(i).getUnreadMessageKeys().add(dataSnapshot.getKey());
                                list.get(i).setUnreadCount(list.get(i).getUnreadCount() + 1);
                                notifyItemChanged(i);
                                notifyDataSetChanged();

                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    // Update adapter & list by applying sort here.
                            /*activity.sortListNotifyChanges();*/
                                    int sizeBefore = list.size();
                                    Collections.sort(list, new ComparatorLongDescending());
                                    notifyItemRangeChanged(0, sizeBefore);
                                    notifyItemRangeChanged(0, sizeBefore, null);
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Log.d(TAG, "Update count changes is: " + dataSnapshot.toString());
                try {
                    MessageVO msg = dataSnapshot.getValue(MessageVO.class);

                    // increase count when message received for chat
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getMatchid().trim().equalsIgnoreCase(matchId.trim())) {
                            if (!list.get(i).getUnreadMessageKeys().contains(dataSnapshot.getKey())) {
                                list.get(i).getUnreadMessageKeys().add(dataSnapshot.getKey());
                                list.get(i).setUnreadCount(list.get(i).getUnreadCount() + 1);
                                notifyItemChanged(i);
                                notifyDataSetChanged();

                                if (msg != null && msg.getMsg() != null && !msg.getMsg().trim().isEmpty()) {
                                    // Update adapter & list by applying sort here.
                            /*activity.sortListNotifyChanges();*/
                                    int sizeBefore = list.size();
                                    Collections.sort(list, new ComparatorLongDescending());
                                    notifyItemRangeChanged(0, sizeBefore);
                                    notifyItemRangeChanged(0, sizeBefore, null);
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException().printStackTrace();
            }
        });
    }

    public class ComparatorLongDescending implements Comparator<ChatListVO> {

        public int compare(ChatListVO first, ChatListVO second) {
//            Log.d("SORT_CHAT_LIST", "First: " + first.getFirstName() + " " + first.lastName + ", Timestamp: " + first.getTimestampLastSeen() + ", Second: " +
//                    second.getFirstName() + " " + second.lastName + ", Timestamp: " + second.getTimestampLastSeen());

//            long firstValue = first.getTimestampLastSeen();
            long firstValue = first.getTime();
            long secondValue = second.getTime();
            return firstValue > secondValue ? -1 : firstValue < secondValue ? 1 : 0;
        }
    }
}