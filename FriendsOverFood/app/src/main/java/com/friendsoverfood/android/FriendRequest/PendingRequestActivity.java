package com.friendsoverfood.android.FriendRequest;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PendingRequestActivity extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "PENDING_REQUEST_ACTIVITY";
    public ListView listViewPendingRequests;
    public PendingRequestListAdapter adapter;
    public ArrayList<RequestVO> listPendingRequests = new ArrayList<>();
    public LinearLayout linearPlaceHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setTitleSupportActionBar(getString(R.string.pending_request));

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(4); // Set search icon selected
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_pending_request_activity, getMiddleContent());
        listViewPendingRequests = (ListView) view.findViewById(R.id.listViewPendingRequest);
        linearPlaceHolder = (LinearLayout) view.findViewById(R.id.linearLayoutPendingRequestsActivityPlaceholder);

        // TODO Set custom layout to support action bar here.
//        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_list_activity, null);

        setListener();

        setAllTypefaceMontserratRegular(view);

        sendRequestGetPendingFriendRequests();

        /*if (adapter == null) {
            adapter = new PendingRequestListAdapter(context, listPendingRequests);
            listViewPendingRequests.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }*/

    }

    private void setListener() {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void sendRequestGetPendingFriendRequests() {
        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriendrequests));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_USER_PENDING_FRIEND_REQUESTS, PendingRequestActivity.this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_USER_PENDING_FRIEND_REQUESTS) {
                Log.d(TAG, "RESPONSE_PENDING_FREIND_REQUESTS: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean success = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                    int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                    if (success) {
                        listPendingRequests.clear();
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject obj = dataArray.getJSONObject(i);

                            String status = !obj.isNull("status") ? obj.getString("status") : "";
                            String friend1 = !obj.isNull("friend1") ? obj.getString("friend1") : "";
                            String friend2 = !obj.isNull("friend2") ? obj.getString("friend2") : "";
                            if (status.trim().equalsIgnoreCase("0")
                                    && friend2.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                JSONArray arrayDetails = !obj.isNull("details") ? obj.getJSONArray("details") : new JSONArray();
                                RequestVO request = new RequestVO();
                                for (int j = 0; j < arrayDetails.length(); j++) {
                                    JSONObject objDetails = arrayDetails.getJSONObject(j);
                                    request.setId(!objDetails.isNull("id") ? objDetails.getString("id").trim() : "");
                                    request.setFirst_name(!objDetails.isNull("first_name") ? objDetails.getString("first_name").trim() : "");
                                    request.setLast_name(!objDetails.isNull("last_name") ? objDetails.getString("last_name").trim() : "");
                                    request.setProfilepic1(!objDetails.isNull("profilepic1") ? objDetails.getString("profilepic1").trim() : "");
                                    request.setCreatedat(!objDetails.isNull("createdat") ? objDetails.getString("createdat").trim() : "");
                                    request.setUpdatedat(!objDetails.isNull("updatedat") ? objDetails.getString("updatedat").trim() : "");
                                    request.setGender(!objDetails.isNull("gender") ? objDetails.getString("gender").trim() : "");
                                    request.setDob(!objDetails.isNull("dob") ? objDetails.getString("dob").trim() : "");
                                    request.setLocation(!objDetails.isNull("location") ? objDetails.getString("location").trim() : "");
                                    request.setLocation_string(!objDetails.isNull("location_string") ? objDetails.getString("location_string").trim() : "");
                                    request.setStatus("");
                                }

                                if (arrayDetails.length() > 0) {
                                    listPendingRequests.add(request);
                                }
                            }
                        }

                        if (listPendingRequests.size() > 0) {
                            linearPlaceHolder.setVisibility(View.GONE);
                            listViewPendingRequests.setVisibility(View.VISIBLE);

                            if (adapter == null) {
                                adapter = new PendingRequestListAdapter(context, listPendingRequests);
                                listViewPendingRequests.setAdapter(adapter);
                            } else {
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            listViewPendingRequests.setVisibility(View.GONE);
                            linearPlaceHolder.setVisibility(View.VISIBLE);
                        }
                        stopProgress();
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        intent = new Intent(mContext, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        supportFinishAfterTransition();
    }
}
