package com.friendsoverfood.android.RestaurantDetails;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.RestaurantReviewVO;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 22/04/17.
 */

public class ReviewsListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<RestaurantReviewVO> list;
    private Intent intent;
    private ReviewsActivity activity;

    public ReviewsListAdapter(Context context, ArrayList<RestaurantReviewVO> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.activity = (ReviewsActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_reviews_list_adapter, parent, false);

            holder.txtUserName = (TextView) convertView.findViewById(R.id.textViewReviewsListAdapterUserName);
            holder.txtReviewDateTime = (TextView) convertView.findViewById(R.id.textViewReviewsListAdapterReviewDateTime);
            holder.txtReviewDescription = (TextView) convertView.findViewById(R.id.textViewReviewsListAdapterReviewDescription);
            holder.ratingBarReview = (RatingBar) convertView.findViewById(R.id.ratingBarReviewsListAdapterReview);
            holder.imgUserProfilePic = (CircularImageView) convertView.findViewById(R.id.imageViewReviewsListAdapterUserProfilePicture);
            holder.linearRoot = (LinearLayout) convertView.findViewById(R.id.linearLayoutReviewsListAdapterRoot);
            activity.setAllTypefaceMontserratRegular(convertView);
            activity.setAllTypefaceMontserratLight(holder.txtReviewDateTime);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtUserName.setText(list.get(position).getUsername().trim());
        holder.txtReviewDateTime.setText(list.get(position).getDatetime().trim());
        holder.txtReviewDescription.setText(Html.fromHtml(list.get(position).getDescription().trim()));
        holder.ratingBarReview.setRating((float) list.get(position).getReviewRating());

        final ViewHolder finalHolder = holder;
        final int pos = position;

        if (!list.get(position).getProfilePicUrl().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getProfilePicUrl().trim()).resize(Constants.convertDpToPixels(36), Constants.convertDpToPixels(36)).centerCrop()
                    .placeholder(R.drawable.shape_red_primary_circle_small).error(R.drawable.shape_red_primary_circle_small)
                    .into(holder.imgUserProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("REVIEW_LIST_ADAPTER", "Error loading reviewed user profile pic: " + list.get(pos).getProfilePicUrl().trim());
                        }
                    });
        } else {
            holder.imgUserProfilePic.setImageResource(R.drawable.shape_red_primary_circle_small);
        }

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close drawer menu if open
                /*if (activity.drawerMenuRoot.isDrawerOpen(Gravity.RIGHT)) {
                    activity.drawerMenuRoot.closeDrawer(Gravity.RIGHT);
                }*/
            }
        });

        return convertView;
    }

    public class ViewHolder {
        public TextView txtUserName, txtReviewDateTime, txtReviewDescription;
        public LinearLayout linearRoot;
        public CircularImageView imgUserProfilePic;
        public RatingBar ratingBarReview;

    }
}
