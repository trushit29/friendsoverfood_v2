package com.friendsoverfood.android.Settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SettingActivity extends BaseActivity {
    private Context context;
    private LayoutInflater inflater;
    private View view;

    //    ALL Prefernces Variables
    public TextView textViewTerms, textViewPrivayPolicy, textViewLincenses, textViewShowDistanceIn, textViewAgeRange, textViewDistance;
    public SwitchCompat switchMessages, switchNewMatches, switchFeMale, switchMale;
    public SeekBar seekBarActivityDiscoveryPreferencesSearchDistance;
    public LinearLayout llLogout, llShareFOF, llhelp;
    public Button btnMile, btnKilometer;
    public RangeBar rangebarAge;
    public String age1 = "18";
    public String age2 = "100";
    public int mMaxValue = 100, mMinValue = 0, mMinAge = 40, mMaxAge = 80;
    public int distanceSelected = 0;
    public final double KM_TO_MILE = 0.621371;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListners();
        setVisibilityActionBar(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setTitleSupportActionBar(getString(R.string.settings));
        ApiViewProfile();

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(5); // Set search icon selected
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())

        {

            case R.id.llLogout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure,You want to logout?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        resetValuesOnLogout();

                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

               /* alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(COLOR_I_WANT);
                    }
                });*/
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));

                break;

            default:
                break;

        }

    }

    private void setListners() {
        switchMale.setOnClickListener(this);
        switchFeMale.setOnClickListener(this);
        switchMessages.setOnClickListener(this);
        switchNewMatches.setOnClickListener(this);
        btnKilometer.setOnClickListener(this);
        btnMile.setOnClickListener(this);
        llLogout.setOnClickListener(this);
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_setting_activity, getMiddleContent());

        textViewDistance = (TextView) view.findViewById(R.id.textViewDistance);
        textViewAgeRange = (TextView) view.findViewById(R.id.textViewAgeRange);
        textViewShowDistanceIn = (TextView) view.findViewById(R.id.textViewShowDistanceIn);
        textViewLincenses = (TextView) view.findViewById(R.id.textViewLincenses);
        textViewPrivayPolicy = (TextView) view.findViewById(R.id.textViewPrivayPolicy);
        textViewTerms = (TextView) view.findViewById(R.id.textViewTerms);

        switchMale = (SwitchCompat) view.findViewById(R.id.switchMale);
        switchFeMale = (SwitchCompat) view.findViewById(R.id.switchFeMale);
        switchNewMatches = (SwitchCompat) view.findViewById(R.id.switchNewMatches);
        switchMessages = (SwitchCompat) view.findViewById(R.id.switchMessages);

        seekBarActivityDiscoveryPreferencesSearchDistance = (SeekBar) view.findViewById(R.id.seekBarActivityDiscoveryPreferencesSearchDistance);

        llhelp = (LinearLayout) view.findViewById(R.id.llhelp);
        llShareFOF = (LinearLayout) view.findViewById(R.id.llShareFOF);
        llLogout = (LinearLayout) view.findViewById(R.id.llLogout);

        btnKilometer = (Button) view.findViewById(R.id.btnKilometer);
        btnMile = (Button) view.findViewById(R.id.btnMile);

        rangebarAge = (RangeBar) view.findViewById(R.id.rangebarAge);

        btnKilometer.setSelected(true);

        seekBarActivityDiscoveryPreferencesSearchDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) { // If the changes are from the User only then accept the
                    // changes
                    /* Instead of -> if (progress != 0) { */
                    distanceSelected = (int) ((float) progress / 100 * (mMaxValue - mMinValue) + mMinValue);
                    distanceSelected *= 10;
                    distanceSelected = Math.round(distanceSelected);
                    distanceSelected /= 10;
                    Log.i("distanceSelected", "" + distanceSelected);
                }

                if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PREFERED_UNIT, "K").trim().equalsIgnoreCase("K")) {
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                    SharedPreferenceUtil.save();

                    textViewDistance.setText(Integer.toString(distanceSelected) + " " + getResources().getString(R.string.distance_unit_km));
                } else {
                    /*convertedtoMiValue = Constants.convertKmtoMi(mCurrentValue);
                    txtSearchDistance.setText(Integer.toString(convertedtoMiValue) + " Mi");*/
                    textViewDistance.setText(Integer.toString(distanceSelected) + " " + getString(R.string.distance_unit_mi));
//                    distanceSelected = Constants.convertMitoKm(distanceSelected);
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, String.valueOf(distanceSelected));
//                    SharedPreferenceUtil.save();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rangebarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
                age1 = leftPinValue;
                age2 = rightPinValue;
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE1, aage1.trim());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE2, age2.trim());
//                SharedPreferenceUtil.save();save
                textViewAgeRange.setText(age1 + "-" + age2);

            }

        });

    }

    private void setValuesToFields() {
        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").trim().equalsIgnoreCase(getString(R.string.gender_all_value))) {
            switchFeMale.setChecked(true);
            switchMale.setChecked(true);
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").trim().equalsIgnoreCase(getString(R.string.gender_male_value))) {
            switchFeMale.setChecked(false);
            switchMale.setChecked(true);
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SHOWME, "").trim().equalsIgnoreCase(getString(R.string.gender_female_value))) {
            switchFeMale.setChecked(true);
            switchMale.setChecked(false);
        }

        seekBarActivityDiscoveryPreferencesSearchDistance.setProgress(Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "5").trim()));

        textViewDistance.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, "100")
                + " " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PREFERED_UNIT, "Km"));

        // Max & Min Range for Age
        Log.d("SETTINGS_ACTIVITY", "Minimum value: " + Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16"))
                + ", Max value: " + Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100")));
        rangebarAge.setRangePinsByValue(Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16")),
                Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100")));
        textViewAgeRange.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16") + "-" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100"));

        switchMessages.setChecked((SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)));
        switchNewMatches.setChecked(SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true));
        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("km")) {
            btnKilometer.setSelected(true);
            btnMile.setSelected(false);
            textViewShowDistanceIn.setText(getString(R.string.distance_unit_km));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("mi")) {
            btnKilometer.setSelected(false);
            btnMile.setSelected(true);
            textViewShowDistanceIn.setText(getString(R.string.distance_unit_mi));
        }

    }

    private boolean stringToBooleanForSwitch(String s) {
        if (s.equalsIgnoreCase("0")) {
            return false;
        } else if (s.equalsIgnoreCase("1")) {
            return true;
        } else
            return false;
    }

    private void ApiViewProfile() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_profile_api).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_EDIT_PROFILE, SettingActivity.this);
    }

    @Override
    public void onBackPressed() {

        ApiEditProfile();
//        super.onBackPressed();
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (action == Constants.ACTION_CODE_PROFILE || action == Constants.ACTION_CODE_EDIT_PROFILE && response != null) {
            Log.i("Response", "" + response);
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        dataInner = data.getJSONObject(i);
                    }
                    if (dataInner != null) {
                        UserProfileAPI profile = new UserProfileAPI(context, dataInner);

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profile.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().trim().replaceAll(",","."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().trim().replaceAll(",","."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, profile.is_receive_messages_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, profile.is_receive_invitation_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profile.profilepic6.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profile.profilepic7.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profile.profilepic8.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profile.profilepic9.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                        SharedPreferenceUtil.save();
                        setValuesToFields();
                    }

                } else {
                    showSnackBarMessageOnly(errormessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                intent = new Intent(mContext, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();
            }

        }
    }

    private void ApiEditProfile() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
        params.put(getString(R.string.api_response_param_key_showme), getGenderPrefrence());
        params.put(getString(R.string.api_response_param_key_search_distance), String.valueOf(seekBarActivityDiscoveryPreferencesSearchDistance.getProgress()));
        params.put(getString(R.string.api_response_param_key_search_max_age), rangebarAge.getRightPinValue());
        params.put(getString(R.string.api_response_param_key_search_min_age), rangebarAge.getLeftPinValue().toString().trim());
        params.put(getString(R.string.api_response_param_key_distance_unit), textViewShowDistanceIn.getText().toString().trim());
        params.put(getString(R.string.api_response_param_key_is_receive_messages_notifications), switchMessages.isChecked() ? "1" : "0");
        params.put(getString(R.string.api_response_param_key_is_receive_invitation_notifications), switchNewMatches.isChecked() ? "1" : "0");
        params.put(getString(R.string.api_param_key_fields), fields());

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, SettingActivity.this);
    }

    public String getGenderPrefrence() {
        if (switchMale.isActivated() && switchFeMale.isActivated()) {
            return "both";
        } else if (switchMale.isActivated() && !switchFeMale.isActivated()) {
            return "male";
        } else if (switchFeMale.isActivated() && !switchMale.isActivated()) {
            return "female";

        } else
            return "both";
    }

    public String fields() {
        return getString(R.string.api_response_param_key_showme) + "," +
                getString(R.string.api_response_param_key_search_distance) + "," +
                getString(R.string.api_response_param_key_search_max_age) + "," +
                getString(R.string.api_response_param_key_search_min_age) + "," +
                getString(R.string.api_response_param_key_is_receive_invitation_notifications) + "," +
                getString(R.string.api_response_param_key_is_receive_messages_notifications) + "," +
                getString(R.string.api_response_param_key_distance_unit);

    }
}

