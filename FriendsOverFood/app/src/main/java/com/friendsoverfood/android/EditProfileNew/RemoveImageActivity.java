package com.friendsoverfood.android.EditProfileNew;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;

public class RemoveImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_remove_image_activity);
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.INTENT_REMOVE_IMAGE, true);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
