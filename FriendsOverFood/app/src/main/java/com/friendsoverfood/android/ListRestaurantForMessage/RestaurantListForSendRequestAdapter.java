package com.friendsoverfood.android.ListRestaurantForMessage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantDetails.RestaurantDetailsActivity;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Trushit on 04/07/17.
 */

public class RestaurantListForSendRequestAdapter extends RecyclerView.Adapter<RestaurantListForSendRequestAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ArrayList<RestaurantListVO> list;
    private Context context;
    private RestaurantListForSendRequestActivity activity;
    private Intent intent;
    private final String TAG = "RESTAURANT_LIST_ADAPTER";

    public RestaurantListForSendRequestAdapter(Context context, ArrayList<RestaurantListVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (RestaurantListForSendRequestActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public RestaurantListVO getItem(int position) {
        return list.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_restaurant_list_send_request_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        activity.setAllTypefaceMontserratRegular(viewHolder.cardViewRoot);

        viewHolder.txtRestaurantName.setText(list.get(position).getName().trim());

        if (!list.get(position).getTimeToReach().trim().isEmpty()) {
            viewHolder.txtRatings.setText(list.get(position).getTimeToReach().trim());
        } else {
            viewHolder.txtRatings.setText("");
        }
        /*viewHolder.txtRatings.setText(list.get(position).getVicinity().trim());*/

        /*if (!list.get(position).getRatings().trim().isEmpty()) {
//            viewHolder.txtRatings.setVisibility(View.VISIBLE);
            viewHolder.ratingBarRestaurant.setVisibility(View.VISIBLE);
//            viewHolder.txtRatings.setText(String.valueOf(list.get(position).getRatings()).trim() + " " + context.getResources().getString(R.string.rating));
            viewHolder.ratingBarRestaurant.setRating(Float.valueOf(list.get(position).getRatings()));
        } else {
            viewHolder.ratingBarRestaurant.setRating(0.0f);
//            viewHolder.txtRatings.setVisibility(View.GONE);
            viewHolder.ratingBarRestaurant.setVisibility(View.GONE);
        }*/

        final ViewHolder holder = viewHolder;
        final int pos = position;

        int sizePic = (activity.widthScreen - Constants.convertDpToPixels(16)) / 3;

//        viewHolder.cardViewRoot.getLayoutParams().width = sizePic;
        boolean isSelected = false;
        for (int i = 0; i < activity.listRestaurantSelected.size(); i++) {
            if (list.get(position).getPlaceid().trim().equalsIgnoreCase(activity.listRestaurantSelected.get(i).getPlaceid().trim())) {
                isSelected = activity.listRestaurantSelected.get(i).isSelected();
            }
        }

        if (isSelected) {
            int heightRoot = holder.cardViewRoot.getHeight();
            if (heightRoot > 0) {
                activity.heightRootAdapter = heightRoot;
            }
//            Log.d(TAG, "Height is: " + activity.heightRootAdapter);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(sizePic, activity.heightRootAdapter);
            holder.imgOverlay.setLayoutParams(params);

            holder.cbSelect.setSelected(true);
            holder.cbSelect.setVisibility(View.VISIBLE);
            holder.imgOverlay.setVisibility(View.VISIBLE);
        } else {
            holder.cbSelect.setSelected(false);
            holder.cbSelect.setVisibility(View.GONE);
            holder.imgOverlay.setVisibility(View.GONE);
        }

        if (!list.get(position).getPhotoReference().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getPhotoReference().trim())
                    .placeholder(R.drawable.ic_placeholder_restaurant).error(R.drawable.ic_placeholder_restaurant)
                    .resize(sizePic, sizePic).centerCrop()
                    .into(holder.imgRestaurantProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getPhotoReference().trim());
                        }
                    });
        } else {
            holder.imgRestaurantProfilePic.setImageResource(R.drawable.ic_placeholder_restaurant);
        }


      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/

        /*viewHolder.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = false;
                int countPositionInList = -1;
                for (int i = 0; i < activity.listRestaurantSelected.size(); i++) {
                    if (list.get(pos).getPlaceid().trim().equalsIgnoreCase(activity.listRestaurantSelected.get(i).getPlaceid().trim())) {
                        isSelected = activity.listRestaurantSelected.get(i).isSelected();
                        countPositionInList = i;
                    }
                }

                if (isSelected) {
                    if (countPositionInList != -1) {
                        activity.listRestaurantSelected.remove(countPositionInList);
                    }
                    notifyItemChanged(pos);
                    notifyDataSetChanged();
                } else {
                    if (activity.listRestaurantSelected.size() < 5) {
                        activity.listRestaurantSelected.add(list.get(pos));
                        notifyItemChanged(pos);
                        notifyDataSetChanged();
                    }
                }
            }
        });*/

        viewHolder.cardViewRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /*int countSelected = 0;
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).isSelected()) {
                        countSelected++;
                    }
                }*/
                boolean isSelected = false;
                int countPositionInList = -1;
                for (int i = 0; i < activity.listRestaurantSelected.size(); i++) {
                    if (list.get(pos).getPlaceid().trim().equalsIgnoreCase(activity.listRestaurantSelected.get(i).getPlaceid().trim())) {
                        isSelected = activity.listRestaurantSelected.get(i).isSelected();
                        countPositionInList = i;
                    }
                }

                if (isSelected) {
                    if (countPositionInList != -1) {
                        activity.listRestaurantSelected.remove(countPositionInList);
                    }
                    list.get(pos).setSelected(false);
                    /*activity.listRestaurantSelected.add(list.get(pos));*/

                    // Sort the list according to isSelected
//                    Collections.sort(list, new HashMapComparatorBoolean());

                    notifyItemChanged(pos);
                    notifyDataSetChanged();
                } else {
                    if (activity.chatSelected.getStatus().trim().equalsIgnoreCase("0")) {
                        if (activity.listRestaurantSelected.size() < 5) {
                        /*list.get(pos).setSelected(true);*/
                            activity.listRestaurantSelected.add(list.get(pos));
                            list.get(pos).setSelected(true);

                            // Sort the list according to isSelected
//                            Collections.sort(list, new HashMapComparatorBoolean());

                            notifyItemChanged(pos);
                            notifyDataSetChanged();
                        }
                    } else {
                        activity.listRestaurantSelected.add(list.get(pos));
                        list.get(pos).setSelected(true);

                        // Sort the list according to isSelected
//                        Collections.sort(list, new HashMapComparatorBoolean());

                        notifyItemChanged(pos);
                        notifyDataSetChanged();
                    }
                }
            }
        });

        viewHolder.cardViewRoot.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                intent = new Intent(context, RestaurantDetailsActivity.class);
                intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos));
                intent.putExtra("isToShowInviteFriends", false);
                activity.startActivity(intent);
                return false;
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtRestaurantName, txtRatings;
        public RatingBar ratingBarRestaurant;
        public CardView cardViewRoot;
        public ImageView imgRestaurantProfilePic, imgOverlay;
        public CheckBox cbSelect;

        public ViewHolder(View view) {
            super(view);
            cardViewRoot = (CardView) view.findViewById(R.id.cardViewRestaurantListSendRequestAdapterRoot);
            txtRestaurantName = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestAdapterRestaurantName);
            txtRatings = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestAdapterRestaurantRatings);
            imgRestaurantProfilePic = (ImageView) view.findViewById(R.id.imageViewRestaurantListSendRequestAdapterRestaurantPic);
            imgOverlay = (ImageView) view.findViewById(R.id.imageViewRestaurantListSendRequestAdapterOverlay);
            ratingBarRestaurant = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListSendRequestAdapterRestaurantsRatings);
            cbSelect = (CheckBox) view.findViewById(R.id.checkBoxRestaurantListSendRequestAdapterSelect);
            //        tagViewInterests.setTokenListener(activity);
            activity.setAllTypefaceMontserratRegular(view);
        }
    }

    public class HashMapComparatorBoolean implements Comparator<RestaurantListVO> {
        public int compare(RestaurantListVO first, RestaurantListVO second) {
            boolean b1 = first.isSelected();
            boolean b2 = second.isSelected();

            return (b1 && !b2) ? -1 : (!b1 && b2) ? 1 : 0;
            /*return Boolean.compare(b1,b2);*/
        }
    }

}
