package com.friendsoverfood.android.RestaurantDetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;
import com.friendsoverfood.android.RestaurantsList.RestaurantReviewVO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 22/04/17.
 */

public class ReviewsActivity extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "RESTAURANTS_DETAILS_ACTIVITY";

    private ImageView imgRestaurantProfilePic;
    private TextView txtRestaurantName;
    private RatingBar ratingBarRestaurant;
    private View viewHeader;

    private ListView listViewReviews;
    private ArrayList<RestaurantReviewVO> listReviews = new ArrayList<>();
    private ReviewsListAdapter adapter;
    private RestaurantListVO restaurantDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
        setTitleSupportActionBar(getString(R.string.title_all_reviews));
//        setTitleSupportActionBar("");
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_reviews_activity, getMiddleContent());
        viewHeader = inflater.inflate(R.layout.layout_reviews_list_header, null);

        listViewReviews = (ListView) view.findViewById(R.id.listViewReviewsActivity);
        txtRestaurantName = (TextView) viewHeader.findViewById(R.id.textViewReviewsActivityRestaurantName);
        ratingBarRestaurant = (RatingBar) viewHeader.findViewById(R.id.ratingBarReviewsActivityRestaurantReview);
        imgRestaurantProfilePic = (ImageView) viewHeader.findViewById(R.id.imageViewReviewsActivityRestaurantImage);

        listViewReviews.addHeaderView(viewHeader, null, false);


//        listReviews = new ArrayList<>();
//        for (int i = 0; i < 15; i++) {
//            RestaurantReviewVO review = new RestaurantReviewVO();
//            review.setDescription("If your budget is luxurious, then this is the best place in town for a gujarati thali. Beautiful ambience, " +
//                    "this restaurant is set in an old bungalow turned into a heritage hotel known as the House of MG. " +
//                    "There is a LOT to eat in the thali so make sure you come with a good appetite. " +
//                    "The food is all very good right from the appetisers to the main course to the sweet. Nice experience!");
//            review.setReviewRating(4.5);
//            review.setDatetime("one month ago");
//            review.setUsername("Shreya Kanoi");
//            review.setProfilePicUrl("https://b.zmtcdn.com/data/user_profile_pictures/794/61d1073dda8824ea0b2b92b4b0d89794.jpg?fit=around%7C400%3A400&crop=400%3A400%3B%2A%2C%2A&output-format=webp");
//            listReviews.add(review);
//        }

        if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT) && getIntent().getSerializableExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT) != null) {
            restaurantDetails = (RestaurantListVO) getIntent().getSerializableExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT);

        }

        if (restaurantDetails != null) {
            listReviews = restaurantDetails.getListReview();
            if (!restaurantDetails.getPhotoReference().trim().isEmpty()) {
                Picasso.with(context).load(restaurantDetails.getPhotoReference().trim()).resize(0, Constants.convertDpToPixels(114)).into(imgRestaurantProfilePic);
            }

            txtRestaurantName.setText(restaurantDetails.getName().trim());
//            ratingBarRestaurant.setRating((float) restaurantDetails.getRatings());
            ratingBarRestaurant.setRating(Float.valueOf(restaurantDetails.getRatings()));
        }

        // TODO Set adapter for reviews list here.
        adapter = new ReviewsListAdapter(context, listReviews);
        listViewReviews.setAdapter(adapter);

//        setUpRestaurantDetails(false);
        setListener();

        /*setAllTypefaceMontserratRegular(view);
        setAllTypefaceMontserratLight(txtCuisine);
        setAllTypefaceMontserratLight(numberPickerDay);
        setAllTypefaceMontserratLight(numberPickerTime);
        setAllTypefaceMontserratLight(numberPickerPeople);*/
    }

    private void setListener() {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        if (isPickerSelected) {
        super.onBackPressed();
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.textViewRestaurantDetailsActivityRestaurantReviewsReadAll:

                break;

            default:
                break;

        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

        }

        // other 'case' lines to check for other permissions this app might request
    }
}
