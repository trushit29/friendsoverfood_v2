package com.friendsoverfood.android;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.ChatNew.ChatListActivity;
import com.friendsoverfood.android.EditProfileNew.EditProfileNewActivity;
import com.friendsoverfood.android.FriendRequest.PendingRequestActivity;
import com.friendsoverfood.android.Settings.SettingActivity;

import java.util.ArrayList;

/**
 * Created by Trushit on 24/03/17.
 */

public class DrawerMenuAdapter extends BaseAdapter {

    private LayoutInflater inflator;
    private Context context;
    private ArrayList<DrawerMenuVO> list;
    private Intent intent;
    private BaseActivity activity;

    public DrawerMenuAdapter(Context context, ArrayList<DrawerMenuVO> list) {
        this.list = list;
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.activity = (BaseActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public DrawerMenuVO getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        convertView = null;

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.layout_drawer_menu_adapter, null);

            holder.linearRoot = (LinearLayout) convertView.findViewById(R.id.linearLayoutDrawerMenuAdapterRoot);
            holder.txtCount = (TextView) convertView.findViewById(R.id.textViewDrawerMenuAdapterCount);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.textViewDrawerMenuAdapterTitle);
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.imageViewDrawerMenuAdapterIcon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitle.setText(list.get(position).getTitle().trim());
        holder.imgIcon.setImageResource(list.get(position).getIconId());

        if (list.get(position).getCount() > 0) {
            holder.txtCount.setVisibility(View.VISIBLE);
            holder.txtCount.setText(String.valueOf(list.get(position).getCount()));
        } else {
            holder.txtCount.setVisibility(View.GONE);
        }

        final ViewHolder finalHolder = holder;
        final int pos = position;

        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (pos) {
                    //Profile
                    case 0:
                        if (activity.drawerMenuRoot != null) {
                            activity.drawerMenuRoot.closeDrawer(activity.relativeMenuRoot);
                        }
//                        intent = new Intent(context, EditProfile.class);
                        intent = new Intent(context, EditProfileNewActivity.class);
                        context.startActivity(intent);
                        break;
                    //Chat
                    case 1:
                        if (activity.drawerMenuRoot != null) {
                            activity.drawerMenuRoot.closeDrawer(activity.relativeMenuRoot);
                        }
                        intent = new Intent(context, ChatListActivity.class);
                        context.startActivity(intent);
                        break;
                    //FriendRequest
                    case 2:
                        if (activity.drawerMenuRoot != null) {
                            activity.drawerMenuRoot.closeDrawer(activity.relativeMenuRoot);
                        }
                        intent = new Intent(context, PendingRequestActivity.class);
                        context.startActivity(intent);
                        break;

                    //Settings
                    case 3:
                        if (activity.drawerMenuRoot != null) {
                            activity.drawerMenuRoot.closeDrawer(activity.relativeMenuRoot);
                        }
                        intent = new Intent(context, SettingActivity.class);
                        context.startActivity(intent);
                        break;
                    default:
                        break;
                }

            }
        });

        activity.setAllTypefaceMontserratRegular(convertView);
        return convertView;
    }

    public class ViewHolder {
        public LinearLayout linearRoot;
        public TextView txtTitle, txtCount;
        public ImageView imgIcon;
    }

    /*private void showalertOnLogout() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
        alertbox.setTitle("Are you sure you want to Log Out?");
        alertbox.setCancelable(false);
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Constants.clearUserDetailsOnLogout();
                SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, "false");
                SharedPreferenceUtil.save();

                // Call alarm receiver service
                Intent intentAlarm = new Intent(context, AlarmReceiverMaster.class);

                // create the object of @AlarmManager
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

                // set the alarm for particular time
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                        PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

                if (activity != null) {
                    activity.closeDrawerMenu();
                }
                if (!isDeviceTablet) {
                    intent = new Intent(context, WelcomeNewActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    intent = new Intent(context, WelcomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
//                System.exit(0);
            }
        });

        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });

        alertbox.show();
    }*/
}