package com.friendsoverfood.android.Home;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Trushit on 12/05/17.
 */

public class MutualFriendsListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<MutualFriendsParcelableVO> list;
    private Intent intent;
    private HomeActivity activity;

    public MutualFriendsListAdapter(Context context, ArrayList<MutualFriendsParcelableVO> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.activity = (HomeActivity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_mutual_friends_adapter, parent, false);

            holder.txtName = (TextView) convertView.findViewById(R.id.textViewMutualFriendsAdapterName);
            holder.imgPicture = (CircularImageView) convertView.findViewById(R.id.imageViewMutualFriendsAdapterPicture);
            activity.setAllTypefaceMontserratRegular(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtName.setText(list.get(position).getFirst_name().trim() + " " + list.get(position).getLast_name().trim());

        final ViewHolder finalHolder = holder;
        final int pos = position;

        if (!activity.appendUrl(list.get(position).getProfilepic1().trim()).trim().isEmpty()) {
            Picasso.with(context).load(activity.appendUrl(list.get(position).getProfilepic1().trim()).trim())
                    .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48)).centerCrop()
                    .into(holder.imgPicture, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + activity.appendUrl(list.get(pos).getProfilepic1().trim()).trim());
                        }
                    });
        }

        return convertView;
    }

    public class ViewHolder {
        public TextView txtName;
        public CircularImageView imgPicture;
    }
}
