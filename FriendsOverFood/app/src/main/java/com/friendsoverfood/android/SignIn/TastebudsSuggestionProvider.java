package com.friendsoverfood.android.SignIn;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Trushit on 03/05/17.
 */

public class TastebudsSuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.example.MySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public TastebudsSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
