package com.friendsoverfood.android.Fcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.friendsoverfood.android.util.NetworkUtil;

/**
 * Created by Trushit on 17/04/16.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        Log.d("ALARM_RECEIVER", "Start calling background service for receive Messages & Push notifications.");

        //Start Service for Pusher Notification
        try {
            if (NetworkUtil.isOnline(context)) {
                //Start Service for Push Notification
                Log.d("ALARM_RECEIVER", "Calling Background service.");
                Intent intentFirebaseService = new Intent(context, MyFirebaseInstanceIDService.class);
                context.startService(intentFirebaseService);
                Intent intentAlarmService = new Intent(context, AlarmService.class);
                context.startService(intentAlarmService);

            } else {
                Log.d("ALARM_RECEIVER", "No active internet connection found. Skip calling background service.");
//                showToast(context.getString(R.string.internet_not_available));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
