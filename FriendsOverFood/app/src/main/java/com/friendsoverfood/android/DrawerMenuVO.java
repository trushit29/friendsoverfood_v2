package com.friendsoverfood.android;

/**
 * Created by Trushit on 24/03/17.
 */

public class DrawerMenuVO {
    private int IconId = -1;
    private String title = "";
    private int count = 0;

    public int getIconId() {
        return IconId;
    }

    public void setIconId(int iconId) {
        IconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}


