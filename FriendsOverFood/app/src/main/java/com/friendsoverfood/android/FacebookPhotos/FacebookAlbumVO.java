package com.friendsoverfood.android.FacebookPhotos;

import java.io.Serializable;

/**
 * Created by Trushit on 04/07/16.
 */
public class FacebookAlbumVO implements Serializable {
    private long photo_count = 0, count = 0;
    private String url = "", name = "", id = "";

    public long getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(long photo_count) {
        this.photo_count = photo_count;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
