package com.friendsoverfood.android.RestaurantsList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantDetails.RestaurantDetailsActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Trushit on 27/02/17.
 */

//https://lh3.googleusercontent.com/proxy/Tekn01yxOmARrJl7GL4cbHL4fMtjo-FLqe16dqJXDknsj1GtXnamthHomzzwKWZOWgFINrHxVTF3Tbd8WaXCf33rbsYhz0IR3K8hRopuxBoWhBgDH7tV6nuCv_6-gUGV_doPwr5l8dv3V7DOI6YRVoFHVdgnjw=w408-h233
//https://lh4.googleusercontent.com/-cyXTjcaJbes/WEarsEZhmwI/AAAAAAAAF2s/piMUDL7TD2AN7pnV8LwC29q11mo1pSAWwCLIB/s408-k-no/
public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private ArrayList<RestaurantListVO> list;
    private Context context;
    private RestaurantsListActivity activity;
    private Intent intent;
    private final String TAG = "RESTAURANT_LIST_ADAPTER";

    public RestaurantListAdapter(Context context, ArrayList<RestaurantListVO> list) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.list = list;
        this.activity = (RestaurantsListActivity) context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public RestaurantListVO getItem(int position) {
        return list.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_restaurant_list_send_request_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        activity.setAllTypefaceMontserratRegular(viewHolder.cardViewRoot);

        viewHolder.txtRestaurantName.setText(list.get(position).getName().trim());

        if (!list.get(position).getTimeToReach().trim().isEmpty()) {
            viewHolder.txtRatings.setText(list.get(position).getTimeToReach().trim());
        } else {
            viewHolder.txtRatings.setText("");
        }
        /*viewHolder.txtRatings.setText(list.get(position).getVicinity().trim());*/

        /*if (!list.get(position).getRatings().trim().isEmpty()) {
//            viewHolder.txtRatings.setVisibility(View.VISIBLE);
            viewHolder.ratingBarRestaurant.setVisibility(View.VISIBLE);
//            viewHolder.txtRatings.setText(String.valueOf(list.get(position).getRatings()).trim() + " " + context.getResources().getString(R.string.rating));
            viewHolder.ratingBarRestaurant.setRating(Float.valueOf(list.get(position).getRatings()));
        } else {
            viewHolder.ratingBarRestaurant.setRating(0.0f);
//            viewHolder.txtRatings.setVisibility(View.GONE);
            viewHolder.ratingBarRestaurant.setVisibility(View.GONE);
        }*/

        final ViewHolder holder = viewHolder;
        final int pos = position;

        int sizePic = (activity.widthScreen - Constants.convertDpToPixels(16)) / 3;

//        viewHolder.cardViewRoot.getLayoutParams().width = sizePic;
        /*boolean isSelected = false;
        for (int i = 0; i < activity.listRestaurantSelected.size(); i++) {
            if (list.get(position).getPlaceid().trim().equalsIgnoreCase(activity.listRestaurantSelected.get(i).getPlaceid().trim())) {
                isSelected = activity.listRestaurantSelected.get(i).isSelected();
            }
        }*/

        /*if (isSelected) {
            int heightRoot = holder.cardViewRoot.getHeight();
            if (heightRoot > 0) {
                activity.heightRootAdapter = heightRoot;
            }
//            Log.d(TAG, "Height is: " + activity.heightRootAdapter);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(sizePic, activity.heightRootAdapter);
            holder.imgOverlay.setLayoutParams(params);

            holder.cbSelect.setSelected(true);
            holder.cbSelect.setVisibility(View.VISIBLE);
            holder.imgOverlay.setVisibility(View.VISIBLE);
        } else {*/
        holder.cbSelect.setSelected(false);
        holder.cbSelect.setVisibility(View.GONE);
        holder.imgOverlay.setVisibility(View.GONE);
        /*}*/

        if (!list.get(position).getPhotoReference().trim().isEmpty()) {
            Picasso.with(context).load(list.get(position).getPhotoReference().trim())
                    .placeholder(R.drawable.ic_placeholder_restaurant).error(R.drawable.ic_placeholder_restaurant)
                    .resize(sizePic, sizePic).centerCrop()
                    .into(holder.imgRestaurantProfilePic, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getPhotoReference().trim());
                        }
                    });
        } else {
            holder.imgRestaurantProfilePic.setImageResource(R.drawable.ic_placeholder_restaurant);
        }


      /*  int id = context.getResources().getIdentifier(list.get(position).getImgName(), "drawable", context.getPackageName());
        viewHolder.imgItem.setImageResource(id);*/

        /*viewHolder.cbSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelected = false;
                int countPositionInList = -1;
                for (int i = 0; i < activity.listRestaurantSelected.size(); i++) {
                    if (list.get(pos).getPlaceid().trim().equalsIgnoreCase(activity.listRestaurantSelected.get(i).getPlaceid().trim())) {
                        isSelected = activity.listRestaurantSelected.get(i).isSelected();
                        countPositionInList = i;
                    }
                }

                if (isSelected) {
                    if (countPositionInList != -1) {
                        activity.listRestaurantSelected.remove(countPositionInList);
                    }
                    notifyItemChanged(pos);
                    notifyDataSetChanged();
                } else {
                    if (activity.listRestaurantSelected.size() < 5) {
                        activity.listRestaurantSelected.add(list.get(pos));
                        notifyItemChanged(pos);
                        notifyDataSetChanged();
                    }
                }
            }
        });*/

        viewHolder.cardViewRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, RestaurantDetailsActivity.class);
                intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos));
                intent.putExtra("isToShowInviteFriends", true);
                activity.startActivity(intent);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtRestaurantName, txtRatings;
        public RatingBar ratingBarRestaurant;
        public CardView cardViewRoot;
        public ImageView imgRestaurantProfilePic, imgOverlay;
        public CheckBox cbSelect;

        public ViewHolder(View view) {
            super(view);
            cardViewRoot = (CardView) view.findViewById(R.id.cardViewRestaurantListSendRequestAdapterRoot);
            txtRestaurantName = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestAdapterRestaurantName);
            txtRatings = (TextView) view.findViewById(R.id.textViewRestaurantListSendRequestAdapterRestaurantRatings);
            imgRestaurantProfilePic = (ImageView) view.findViewById(R.id.imageViewRestaurantListSendRequestAdapterRestaurantPic);
            imgOverlay = (ImageView) view.findViewById(R.id.imageViewRestaurantListSendRequestAdapterOverlay);
            ratingBarRestaurant = (RatingBar) view.findViewById(R.id.ratingBarRestaurantListSendRequestAdapterRestaurantsRatings);
            cbSelect = (CheckBox) view.findViewById(R.id.checkBoxRestaurantListSendRequestAdapterSelect);
            //        tagViewInterests.setTokenListener(activity);
            activity.setAllTypefaceMontserratRegular(view);
        }
    }

    public class HashMapComparatorBoolean implements Comparator<RestaurantListVO> {
        public int compare(RestaurantListVO first, RestaurantListVO second) {
            boolean b1 = first.isSelected();
            boolean b2 = second.isSelected();

            return (b1 && !b2) ? -1 : (!b1 && b2) ? 1 : 0;
            /*return Boolean.compare(b1,b2);*/
        }
    }

}
//public class RestaurantListAdapter extends BaseAdapter {
//    private Context context;
//    private LayoutInflater inflater;
//    private ArrayList<RestaurantListVO> list;
//    private Intent intent;
//    private RestaurantsListActivity activity;
//
//    public RestaurantListAdapter(Context context, ArrayList<RestaurantListVO> list) {
//        this.context = context;
//        this.list = list;
//        inflater = LayoutInflater.from(context);
//        this.activity = (RestaurantsListActivity) context;
//    }
//
//    @Override
//    public int getCount() {
//        return list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return list.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder = new ViewHolder();
//        if (convertView == null) {
//            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
//            convertView = inflater.inflate(R.layout.layout_restaurant_list_adapter, parent, false);
//
//            holder.txtName = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantName);
//            holder.txtPriceRange = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantPriceRange);
//            holder.txtRating = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantRatings);
//            holder.txtCuisine = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantCuisine);
//            holder.txtAddress = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantAddress);
//            holder.txtTimings = (TextView) convertView.findViewById(R.id.textViewRestaurantListAdapterRestaurantTimings);
//            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingBarRestaurantListAdapterRestaurantsRatings);
//            holder.imgPic = (ImageView) convertView.findViewById(R.id.imageViewRestaurantListAdapterRestaurantPicture);
//            holder.linearRoot = (LinearLayout) convertView.findViewById(R.id.linearLayoutRestaurantListAdapterRoot);
//            activity.setAllTypefaceMontserratRegular(convertView);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        holder.txtName.setText(list.get(position).getName().trim());
//        holder.txtName.setSelected(true);
//        holder.txtName.requestFocus();
//
//        holder.txtTimings.setText(list.get(position).getTimings().trim());
//
//        holder.txtPriceRange.setText("₹₹₹");
//        if (list.get(position).getOpen_now().equalsIgnoreCase("true"))
//            holder.txtCuisine.setText("Open Now");
//        else if (list.get(position).getOpen_now().equalsIgnoreCase("false"))
//            holder.txtCuisine.setText("Closed Now");
//        else {
//            holder.txtCuisine.setText("Timings not Available");
//        }
//        holder.txtAddress.setText(list.get(position).getVicinity().trim());
//        holder.txtAddress.setSelected(true);
//        holder.txtAddress.requestFocus();
//
//        if (!list.get(position).getRatings().trim().isEmpty()) {
//            holder.txtRating.setVisibility(View.VISIBLE);
//            holder.ratingBar.setVisibility(View.VISIBLE);
//            holder.txtRating.setText(String.valueOf(list.get(position).getRatings()).trim() + " " + context.getResources().getString(R.string.rating));
//            holder.ratingBar.setRating(Float.valueOf(list.get(position).getRatings()));
//        } else {
//            holder.ratingBar.setRating(0.0f);
//            holder.txtRating.setVisibility(View.GONE);
//            holder.ratingBar.setVisibility(View.GONE);
//        }
//
//        final ViewHolder finalHolder = holder;
//        final int pos = position;
//
//        if (!list.get(position).getPhotoReference().trim().isEmpty()) {
//            Picasso.with(context).load(list.get(position).getPhotoReference().trim())
//                    .placeholder(R.drawable.ic_placeholder_restaurant).error(R.drawable.ic_placeholder_restaurant)
//                    .resize(Constants.convertDpToPixels(92), Constants.convertDpToPixels(104)).centerCrop()
//                    .into(holder.imgPic, new Callback() {
//                        @Override
//                        public void onSuccess() {
//
//                        }
//
//                        @Override
//                        public void onError() {
//                            Log.d("ERROR_LOADING_IMAGE", "Restaurant pic is not laoded: " + list.get(pos).getPhotoReference().trim());
//                        }
//                    });
//        } else {
//            holder.imgPic.setImageResource(R.drawable.ic_placeholder_restaurant);
//        }
//
//        holder.linearRoot.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Close drawer menu if open
//                if (activity.drawerMenuRoot.isDrawerOpen(Gravity.RIGHT)) {
//                    activity.drawerMenuRoot.closeDrawer(Gravity.RIGHT);
//                }
//
//                intent = new Intent(context, RestaurantDetailsActivity.class);
//                intent.putExtra(Constants.KEY_INTENT_EXTRA_RESTAURANT, list.get(pos));
//                intent.putExtra("isFromList", true);
////                Log.d("LIST_CLICK", "Day Position: " + activity.numberPickerDay.getValue() + ", Value: "
////                        + activity.listDaysPicker.get(activity.numberPickerDay.getValue()).getCalendarDay().getTime());
//                if (activity.isPickerSelected) {
//                    intent.putExtra(Constants.KEY_INTENT_EXTRA_DATETIME, activity.calDateSelected);
//                    intent.putExtra(Constants.KEY_INTENT_EXTRA_PEOPLE, activity.listPeoplePicker.get(activity.numberPickerPeople.getValue()).getPeopleNumber());
//                }
//                View sharedViewImage = finalHolder.imgPic;
//                String transitionNameImage = context.getResources().getString(R.string.transition_activity_image_animation);
//                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
//                            sharedViewImage, transitionNameImage);
//                    activity.startActivity(intent, transitionActivityOptions.toBundle());
//                } else {
//                    activity.startActivity(intent);
//                }
//            }
//        });
//
//        return convertView;
//    }
//
//    public class ViewHolder {
//        public TextView txtName, txtRating, txtPriceRange, txtCuisine, txtAddress, txtTimings;
//        //        public SwitchCompat switchNotifications;
//        public LinearLayout linearRoot;
//        public ImageView imgPic;
//        public RatingBar ratingBar;
//
//    }
//}
