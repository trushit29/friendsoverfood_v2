package com.friendsoverfood.android.Home;

/**
 * Created by Voculus Softwares on 10-Sep-15.
 */
//public class TokenLayout {
//}

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.R;

public class TokenLayout extends LinearLayout {

    public TokenLayout(Context context) {
        super(context);
    }

    public TokenLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        TextView v = (TextView) findViewById(R.id.name);
        if (selected) {
//            v.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_cancel_red_small, 0);
            v.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else {
            v.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }
}
