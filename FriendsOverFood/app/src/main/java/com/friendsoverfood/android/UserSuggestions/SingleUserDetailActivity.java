package com.friendsoverfood.android.UserSuggestions;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.EditProfileNew.EditProfileNewActivity;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Home.TasteBudsCustomTokenView;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.RestaurantsList.DaysPickerVO;
import com.friendsoverfood.android.RestaurantsList.TimePickerVO;
import com.friendsoverfood.android.SignIn.TastebudChipVO;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.util.RoundedImageView;
import com.github.clans.fab.FloatingActionButton;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import at.blogc.android.views.ExpandableTextView;

/**
 * Created by ashish on 21/03/17.
 */

public class SingleUserDetailActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, AdapterView.OnItemSelectedListener, ActionSheet.ActionSheetListener {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private NestedScrollView nestedScrollViewUserSuggestionDetailActivity;
    private LinearLayout linearLayoutLoading;
    private PagerIndicator indicatorCustom;
    private SliderLayout restoImageSlider;
    private ImageView imgPlaceholder;
    public RelativeLayout relativeSlider;
    private ArrayList<String> imgsList = new ArrayList<>();
    private RoundedImageView roundedImageView;
    private TextView txtViewCounter, textViewDatePicker, textViewPlace, txtViewRelationShip, txtviewEthnicity, txtViewOccupation,
            textViewNameAge, txtAbout, txtViewEducation, txtViewGender;
    private ImageView button_toggle, imgViewEditProfile;
    private FloatingActionButton fabEdit;
    private ExpandableTextView expandableTextView;
    private ImageView imgViewSendRequest;
    public NumberPicker numberPickerDay, numberPickerTime;
    public Button btnDone;
    public LinearLayout linearLayout, linearLayoutTimePicker, linearLayoutDatePicker;
    public ArrayList<DaysPickerVO> listDaysPicker = new ArrayList<>();
    public ArrayList<TimePickerVO> listTimePicker = new ArrayList<>();
    private ArrayList<ChatListVO> FriendList = new ArrayList<>();
    private final String TAG = "USER_DETAILS_ACTIVITY";
    public Calendar calDateSelected = Calendar.getInstance();
    public boolean isPickerSelected = false;
    public String otherUserID = "";
    public UserProfileAPI profile = new UserProfileAPI();
    private ChipView text_chip_layout;
    private ChipViewAdapter adapterLayout;
    public int totalPictures = 0;
    private TasteBudsCustomTokenView text_chip_layout_other_user;
    private LinearLayout linearLayoutAcceptOrRejectRequest, linearLayoutSendRequest;
    public ImageView imageViewFooterPeopleDownArrow, imageViewFooterDatePickerDownArrow, imgreject, imgaccept;
    private List<TastebudChipVO> listUserTastebuds = new ArrayList<>();
    public boolean isFriendRequestSent = false;
    public boolean isfriend = false;
    public TextView txtNoTastebuds;

    // Objects for Local Broadcast manager & Push notifications handling in the app.
    public boolean mIsReceiverRegistered = false;
    public MyBroadcastReceiver mReceiver;
    public BroadcastReceiver mNotificationReceiver;

    public CustomSliderView customSliderView;
    public Target target1, target2, target3, target4, target5, target6, target7, target8, target9;

    // Save friend user id while sending request and forwarding user to chat screen directly.
    public String strFriendUserId = "", strFriendId = "";
    /**
     * Action for friend request.
     * 1 ==> Accept friend request
     * 2 ==> Reject friend request.
     */
    public int actionFriendRequestAcceptReject = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListners();

        setVisibilityActionBar(false);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

        // TODO Change home icon color to primary color here
       /* final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.font_grey_light_card), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);*/

        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Now");
        categories.add("Later");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_single_user_detail_activity, getMiddleContent());
        nestedScrollViewUserSuggestionDetailActivity = (NestedScrollView) view.findViewById(R.id.nestedScrollViewUserSuggestionDetailActivity);
        linearLayoutLoading = (LinearLayout) view.findViewById(R.id.linearLayoutSingleUserDetailsActivityLoading);

        restoImageSlider = (SliderLayout) view.findViewById(R.id.slider);
        imgPlaceholder = (ImageView) view.findViewById(R.id.imageViewSingleUserDetailsActivityPlaceholder);
        relativeSlider = (RelativeLayout) view.findViewById(R.id.relativeSlider);
        txtViewCounter = (TextView) view.findViewById(R.id.txtViewCounter);
        textViewNameAge = (TextView) view.findViewById(R.id.textViewNameAge);
        txtViewOccupation = (TextView) view.findViewById(R.id.txtViewOccupation);
        txtViewEducation = (TextView) view.findViewById(R.id.txtViewEducation);
        txtviewEthnicity = (TextView) view.findViewById(R.id.txtviewEthnicity);
        txtAbout = (TextView) view.findViewById(R.id.textViewSingleUserDetailsActivityAbout);
        txtViewRelationShip = (TextView) view.findViewById(R.id.txtViewRelationShip);
        txtViewGender = (TextView) view.findViewById(R.id.txtViewGender);
        textViewPlace = (TextView) view.findViewById(R.id.textViewPlace);
        expandableTextView = (ExpandableTextView) view.findViewById(R.id.expandableTextView);
        button_toggle = (ImageView) view.findViewById(R.id.button_toggle);
        imgViewEditProfile = (ImageView) view.findViewById(R.id.imgViewEditProfile);
        fabEdit = (FloatingActionButton) view.findViewById(R.id.fabEdit);
        imgViewSendRequest = (ImageView) view.findViewById(R.id.imgViewSendRequest);
        imgaccept = (ImageView) view.findViewById(R.id.imgaccept);
        imgreject = (ImageView) view.findViewById(R.id.imgreject);
        linearLayoutSendRequest = (LinearLayout) view.findViewById(R.id.linearLayoutSendRequest);
        linearLayoutAcceptOrRejectRequest = (LinearLayout) view.findViewById(R.id.linearLayoutAcceptOrRejectRequest);

        strFriendId = "";
        strFriendUserId = "";

        // Change height of slider to same as screen width
        relativeSlider.getLayoutParams().height = widthScreen;
        restoImageSlider.getLayoutParams().height = widthScreen;
        imgPlaceholder.getLayoutParams().height = widthScreen;

        text_chip_layout = (ChipView) view.findViewById(R.id.text_chip_layout_other_user);
        txtNoTastebuds = (TextView) view.findViewById(R.id.txtViewNoTastebuds);
        // Adapter
        adapterLayout = new OtherUserChipViewAdapter(this);
        initLayoutSuggestionsChips();

        expandableTextView.setAnimationDuration(1000L);

        // set interpolators for both expanding and collapsing animations
        expandableTextView.setInterpolator(new OvershootInterpolator());

        // or set them separately
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());
//        expandableTextView.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel ex eget nibh sodales interdum ut vel diam. Duis placerat lobortis mi et lobortis. Nam imperdiet nisi ut nisl tempor.");
//
        if (getIntent().hasExtra(Constants.INTENT_USER_ID)) {
//             && !otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))
            otherUserID = getIntent().getStringExtra(Constants.INTENT_USER_ID);

            if (!otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                imgViewEditProfile.setVisibility(View.GONE);
                fabEdit.setVisibility(View.GONE);
            } else {
                imgViewEditProfile.setVisibility(View.GONE);
                fabEdit.setVisibility(View.VISIBLE);
            }
        }

        if (getIntent().hasExtra(Constants.INTENT_USER_ID)
                && !otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            setVisibilityFooterTabsBase(false);
            setVisibilityActionBarBackButton(true);
        } else {
            setVisibilityFooterTabsBase(true);
            setVisibilityActionBarBackButton(false);
            setSelectorFooterTabsBase(1);
        }
    }

    private void initLayoutSuggestionsChips() {

        // Custom layout and background colors
        text_chip_layout.setAdapter(adapterLayout);
        text_chip_layout.setChipLayoutRes(R.layout.chip);
        text_chip_layout.setChipBackgroundColor(getResources().getColor(R.color.red_bg_chip));
        text_chip_layout.setChipBackgroundColorSelected(getResources().getColor(R.color.white_bg_chip));

        ArrayList<Chip> listChips = new ArrayList<>();
        if (listUserTastebuds.size() > 0) {
            for (int i = 0; i < listUserTastebuds.size(); i++) {
                listChips.add(new TastebudChipVO(listUserTastebuds.get(i).getName().trim(), listUserTastebuds.get(i).getType()));
            }

            text_chip_layout.setChipList(listChips);

            text_chip_layout.setVisibility(View.VISIBLE);
            txtNoTastebuds.setVisibility(View.GONE);
        } else {
            text_chip_layout.setVisibility(View.GONE);
            txtNoTastebuds.setVisibility(View.VISIBLE);
        }
        /*text_chip_layout.setChipList(listUserTastebuds);*/

    }

    private void setListners() {
        button_toggle.setOnClickListener(this);
        imgViewEditProfile.setOnClickListener(this);
        fabEdit.setOnClickListener(this);
        imgViewSendRequest.setOnClickListener(this);
        imgreject.setOnClickListener(this);
        imgaccept.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
//            case R.id.imgReject:
//
            case R.id.imgViewSendRequest:

//                if (isFriendRequestSent) {
                if (isFriendRequestSent) {
                    callAcceptFriendRequest();
                } else if (isfriend || (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Friend"))) {
//                    intent = new Intent(context, ChatHomeActivity.class);

                    // Fetch all the match details from Shared Preferences here.
                    JSONObject jsonObjectResponse = null;
                    ArrayList<ChatListVO> matchList = new ArrayList<>();
                    try {
                        jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                        Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                        JSONObject settings = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_settings))
                                ? jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings)) : new JSONObject();
                        boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                                ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                        String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                                ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                        if (status) {
                            JSONArray data = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                                    ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();

                            JSONObject dataInner = null;
                            for (int i = 0; i < data.length(); i++) {
                                ChatListVO item = new ChatListVO();
                                try {
                                    dataInner = data.getJSONObject(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String matchId = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                String friend1 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend1))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend1)).trim() : "";
                                String friend2 = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_friend2))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_friend2)).trim() : "";
                                String statusFriend = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_status))
                                        ? dataInner.getString(context.getResources().getString(R.string.api_response_param_key_status)).trim() : "";
                                item.setMatchid(matchId);
                                item.setFriend1(friend1);
                                item.setFriend2(friend2);

                                JSONArray details = !dataInner.isNull(context.getResources().getString(R.string.api_response_param_key_details))
                                        ? dataInner.getJSONArray(context.getResources().getString(R.string.api_response_param_key_details)) : new JSONArray();
                                for (int j = 0; j < details.length(); j++) {

                                    JSONObject detailInner = details.getJSONObject(j);
                                    String userId = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                                    String first_name = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_first_name))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_first_name)).trim() : "";
                                    String lastName = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_last_name))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_last_name)).trim() : "";
                                    String profilePic = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                    String gender = !detailInner.isNull(context.getResources().getString(R.string.api_response_param_key_gender))
                                            ? detailInner.getString(context.getResources().getString(R.string.api_response_param_key_gender)).trim() : "";
                                    item.setUserId(userId);
                                    item.setFirstName(first_name);
                                    item.setLastName(lastName);
                                    item.setProfilePic(profilePic);
                                    item.setGender(gender);
                                    item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                                }
                                matchList.add(item);

                            }

                            // Get users VO object here to pass to chat screen using intent.
                            int posUser = -1;
                            for (int i = 0; i < matchList.size(); i++) {
                                Log.d(TAG, "PosSelected Id: " + otherUserID.trim() + ", Match List Id: " + matchList.get(i).getUserId().trim());
                                if (otherUserID.trim().equalsIgnoreCase(matchList.get(i).getUserId().trim())) {
                                    posUser = i;
                                }
                            }

                            if (posUser != -1) {
                                final ChatListVO OtherUser = matchList.get(posUser);

                                // Open Chat Screen from here.
                                stopProgress();

                                Intent intent = new Intent(context, ChatDetailsActivity.class);
                                intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            } else {
                                stopProgress();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        stopProgress();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /*intent = new Intent(context, ChatListActivity.class);
                    startActivity(intent);
                    finish();*/
               /* } else if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Sent")) {
                    asdadadad*/
                } else {
                    strFriendUserId = otherUserID.trim();
                    callFriendsListApi();
                    /*sendFriendRequestToUserSuggestion();*/
                    /*ActionSheet.createBuilder(this, getSupportFragmentManager())
                            .setCancelButtonTitle("Cancel")
                            .setOtherButtonTitles("Now", "Later")
                            .setCancelableOnTouchOutside(true)
                            .setListener(this).show();*/
                }
                break;

            case R.id.button_toggle:
                if (expandableTextView.isExpanded()) {
                    expandableTextView.collapse();
                } else {
                    expandableTextView.expand();
                }

                break;

            case R.id.fabEdit:
                intent = new Intent(this, EditProfileNewActivity.class);
                startActivity(intent);
                break;

            case R.id.imgreject:
                callRejectFriendRequest();
                break;
            case R.id.imgaccept:
                callAcceptFriendRequest();
                break;

            default:
                break;

        }
    }

    private void callRejectFriendRequest() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_deletefriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), otherUserID);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST, SingleUserDetailActivity.this);
    }

    private void callAcceptFriendRequest() {
        showProgress(context.getResources().getString(R.string.loading));
        strFriendUserId = otherUserID.trim();
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_acceptfriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), otherUserID);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST, SingleUserDetailActivity.this);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        txtViewCounter.setText((position + 1) + "/" + String.valueOf(totalPictures));
       /* switch (position) {
            case 0:
                txtViewCounter.setText((position+1)+"/" + String.valueOf(totalPictures));
                break;
            case 1:
                txtViewCounter.setText((position+1)+"/" + String.valueOf(totalPictures));
                break;
            case 2:
                txtViewCounter.setText((position+1)+"/" + String.valueOf(totalPictures));
                break;
            case 3:
                txtViewCounter.setText((position+1)+"/" + String.valueOf(totalPictures));
                break;
            case 4:
                txtViewCounter.setText((position+1)+"/" + String.valueOf(totalPictures));
                break;
            default:
                break;

        }*/
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        switch (index) {
            case 0:
//                Send friend request
                /*sendFriendRequestToUserSuggestion();*/
                if (otherUserID.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
                    strFriendUserId = otherUserID.trim();
                }
                callFriendsListApi();
                break;
            case 1:
                showTimePickerDialog();
                break;
            default:
                break;

        }

    }

    private void showTimePickerDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(
                Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);//set height(300) and width(match_parent) here, ie (width,height)

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dislogView = inflater.inflate(R.layout.layout_dialog_date_time_picker, null);
        numberPickerDay = (NumberPicker) dislogView.findViewById(R.id.numberPickerDay);
        numberPickerTime = (NumberPicker) dislogView.findViewById(R.id.numberPickerTime);
        btnDone = (Button) dislogView.findViewById(R.id.btnDone);
        linearLayout = (LinearLayout) dislogView.findViewById(R.id.linearLayout);
        linearLayoutDatePicker = (LinearLayout) dislogView.findViewById(R.id.linearLayoutDatePicker);
        linearLayoutTimePicker = (LinearLayout) dislogView.findViewById(R.id.linearLayoutTimePicker);
        imageViewFooterDatePickerDownArrow = (ImageView) dislogView.findViewById(R.id.imageViewFooterDatePickerDownArrow);
        imageViewFooterPeopleDownArrow = (ImageView) dislogView.findViewById(R.id.imageViewFooterPeopleDownArrow);
        textViewDatePicker = (TextView) dislogView.findViewById(R.id.textViewDatePicker);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        setPickerForDay();

        dialog.setContentView(dislogView, dialogParams);
        dialog.show();
    }

    public void setPickerForTime() {
        Calendar calToday = Calendar.getInstance();
        int posSelectedTime = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar calStartTime = Calendar.getInstance();
        calStartTime.setTime(calDateSelected.getTime());
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR) && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) {
            int currentMinute = calStartTime.get(Calendar.MINUTE);
            int diffToAdd = 30 - (currentMinute % 30);

            calStartTime.add(Calendar.MINUTE, diffToAdd);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        } else {
            calStartTime.set(Calendar.HOUR_OF_DAY, 0);
            calStartTime.set(Calendar.MINUTE, 0);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        }

        Calendar calEndTime = Calendar.getInstance();
        calEndTime.setTime(calDateSelected.getTime());
        calEndTime.set(Calendar.HOUR_OF_DAY, 23);
        calEndTime.set(Calendar.MINUTE, 59);
        calEndTime.set(Calendar.SECOND, 59);
        calEndTime.set(Calendar.MILLISECOND, 999);

        Log.d(TAG, "Start time: " + calStartTime.getTime() + ", End time: " + calEndTime.getTime());

        // Clear list.
        listTimePicker.clear();
        numberPickerTime.setDisplayedValues(null);
        while (calStartTime.getTimeInMillis() <= calEndTime.getTimeInMillis()) {
            String strTime = new SimpleDateFormat("hh:mm aa").format(calStartTime.getTime());
            Log.d(TAG, "Time to add: " + strTime + ", Date: " + calStartTime.getTime());
            TimePickerVO obj = new TimePickerVO();
            obj.setCalendarTime(calStartTime);
            obj.setStrTime(strTime.trim());
            listTimePicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calStartTime.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calStartTime.get(Calendar.DAY_OF_YEAR)
                        && calDateSelected.get(Calendar.HOUR_OF_DAY) == calStartTime.get(Calendar.HOUR_OF_DAY)
                        && calDateSelected.get(Calendar.MINUTE) == calStartTime.get(Calendar.MINUTE)) {
                    posSelectedTime = listTimePicker.size() - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                calStartTime.add(Calendar.MINUTE, 30);
            }
        }

        String[] strDays = new String[listTimePicker.size()];
        for (int i = 0; i < listTimePicker.size(); i++) {
            strDays[i] = listTimePicker.get(i).getStrTime().trim();
        }

        numberPickerTime.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerTime.setMaxValue(strDays.length - 1);
            numberPickerTime.setWrapSelectorWheel(false);
            numberPickerTime.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerTime);
            numberPickerTime.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    calDateSelected.setTime(listTimePicker.get(newVal).getCalendarTime().getTime());
                }
            });
            numberPickerTime.setValue(posSelectedTime);
        }
    }

    public void setPickerForDay() {
        int posSelectedDay = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
                isPickerSelected = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Prepare days list here. Days list will be only for a week.
        listDaysPicker.clear();
        numberPickerDay.setDisplayedValues(null);
        for (int i = 0; i < 7; i++) {
            Calendar calDay = Calendar.getInstance();
            calDay.add(Calendar.DAY_OF_YEAR, i);

            String strDay = new SimpleDateFormat("EEEE").format(calDay.getTime());
            Log.d(TAG, "Day to add: " + strDay + ", Date: " + calDay.getTime());
            DaysPickerVO obj = new DaysPickerVO();
            obj.setCalendarDay(calDay);

            if (i == 0) {
                obj.setStrDay(getString(R.string.today));
            } else if (i == 1) {
                obj.setStrDay(getString(R.string.tomorrow));
            } else {
                obj.setStrDay(strDay.trim());
            }
            listDaysPicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calDay.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calDay.get(Calendar.DAY_OF_YEAR)) {
                    posSelectedDay = i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listDaysPicker.size()];
        for (int i = 0; i < listDaysPicker.size(); i++) {
            strDays[i] = listDaysPicker.get(i).getStrDay().trim();
        }

        numberPickerDay.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerDay.setMaxValue(strDays.length - 1);
            numberPickerDay.setWrapSelectorWheel(false);
            numberPickerDay.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerDay);

            // TODO Set time picker here as 1st position is selected by default for picker.
            if (!isFromIntent) {
                calDateSelected.setTime(listDaysPicker.get(0).getCalendarDay().getTime());
            }
            setPickerForTime();
            updateDateTitleSelectedFromPicker();

            numberPickerDay.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    // TODO Set time picker here depending on date selected.
                    Log.d(TAG, "Day selected at pos " + newVal + " is: " + listDaysPicker.get(newVal).getCalendarDay().getTime());
                    calDateSelected.setTime(listDaysPicker.get(newVal).getCalendarDay().getTime());
                    updateDateTitleSelectedFromPicker();
                    setPickerForTime();
                }
            });

            numberPickerDay.setValue(posSelectedDay);
        }
    }

    public void updateDateTitleSelectedFromPicker() {
        // TODO Set Selected date in action bar
        Calendar calToday = Calendar.getInstance();
        int selectedDayOfYearCalendar = calDateSelected.get(Calendar.DAY_OF_YEAR);
        int tomorrow = calToday.get(Calendar.DAY_OF_YEAR) + 1;
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) { // Selected date is of today
            textViewDatePicker.setText(getString(R.string.today));
        } else if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && tomorrow == selectedDayOfYearCalendar) {
            textViewDatePicker.setText(getString(R.string.tomorrow));
        } else {
            String dayOfMonth = String.valueOf(calDateSelected.get(Calendar.DAY_OF_MONTH));
            int day = calDateSelected.get(Calendar.DAY_OF_MONTH);
            String charAtLastPosition = dayOfMonth.substring(dayOfMonth.length() - 1);
            if (Integer.parseInt(charAtLastPosition) == 1 && day != 11) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>st</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 2 && day != 12) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>nd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 3 && day != 13) {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>rd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else {
                textViewDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>th</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            }
        }
    }

    private void ApiViewProfileOtherUser() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_profileother).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_otheruser_id), otherUserID);
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_OTHER_USER_DETAIL, SingleUserDetailActivity.this);
    }

    public void callFriendsListApi() {
        showProgress(context.getResources().getString(R.string.loading));
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_myfriends));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST, SingleUserDetailActivity.this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (action == Constants.ACTION_CODE_API_OTHER_USER_DETAIL && response != null) {
            Log.i(TAG, "Response other user details: " + response);
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        dataInner = data.getJSONObject(i);
                    }
                    if (dataInner != null) {
                        profile = new UserProfileAPI(context, dataInner);

                        showValuesToFieldsOtherProfile();
                        linearLayoutLoading.setVisibility(View.GONE);
                        nestedScrollViewUserSuggestionDetailActivity.setVisibility(View.VISIBLE);
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();
                    }

                } else {
                    showSnackBarMessageOnly(errormessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
            Log.i("Response", "" + response);
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
            SharedPreferenceUtil.save();
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    FriendList = new ArrayList<>();
                    String statusFriendOfId = "";
                    for (int i = 0; i < data.length(); i++) {
                        ChatListVO item = new ChatListVO();
                        try {
                            dataInner = data.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                        String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                        String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                        String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                        item.setMatchid(matchId);
                        item.setFriend1(friend1);
                        item.setFriend2(friend2);
                        item.setStatus(statusFriend);

                        if (friend1.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                && strFriendUserId.trim().equalsIgnoreCase(friend2.trim())) {
                            strFriendId = matchId.trim();
                            statusFriendOfId = statusFriend.trim();

                            if (statusFriend.trim().equalsIgnoreCase("1")) {
                                if (profile != null) {
                                    profile.setFriendstatus("Friend");
                                }
                            } else if (statusFriend.trim().equalsIgnoreCase("0")) {
                                if (profile != null) {
                                    profile.setFriendstatus("Sent");
                                }
                            }
                        } else if (friend2.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())
                                && strFriendUserId.trim().equalsIgnoreCase(friend1.trim())) {
                            strFriendId = matchId.trim();
                            statusFriendOfId = statusFriend.trim();

                            if (statusFriend.trim().equalsIgnoreCase("1")) {
                                if (profile != null) {
                                    profile.setFriendstatus("Friend");
                                }
                            } else if (statusFriend.trim().equalsIgnoreCase("0")) {
                                if (profile != null) {
                                    profile.setFriendstatus("Pending");
                                }
                            }
                        }

                        // Update view here.
                        showValuesToFieldsOtherProfile();


                        JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                        for (int j = 0; j < details.length(); j++) {

                            JSONObject detailInner = details.getJSONObject(j);
                            String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                            String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                            String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                            String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                            String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                    ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                            item.setUserId(userId);
                            item.setFirstName(first_name);
                            item.setLastName(lastName);
                            item.setProfilePic(profilePic);
                            item.setGender(gender);
                        }

                        FriendList.add(item);

                        /*intent = new Intent(context, ChatDetailsActivity.class);
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, FriendList.get(FriendList.size() - 1));
                        intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
//                        startActivityForResult(intent, Constants.ACTION_CODE_CHAT_USERS_LIST);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);*/
                    }

                    /*// Get users VO object here to pass to chat screen using intent.
                    int posUser = -1;
                    for (int i = 0; i < FriendList.size(); i++) {
                        Log.d(TAG, "PosSelected Id: " + otherUserID.trim() + ", Match List Id: " + FriendList.get(i).getUserId().trim());
                        if (otherUserID.trim().equalsIgnoreCase(FriendList.get(i).getUserId().trim())) {
                            posUser = i;
                        }
                    }*/



                    /*if (posUser != -1) {*/
                    /*    ChatListVO OtherUser = FriendList.get(posUser);*/
                    ChatListVO itemDummy = new ChatListVO();
                    if (strFriendId.trim().isEmpty()) {
                        itemDummy.setMatchid("-1");
                        itemDummy.setStatus("0");
                    } else {
                        itemDummy.setMatchid(strFriendId);
                        itemDummy.setStatus(statusFriendOfId);
                    }
                    itemDummy.setFriend1(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                    itemDummy.setFriend1(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                    itemDummy.setFriend2(profile.getId().trim());

                    itemDummy.setUserId(profile.getId().trim());
                    itemDummy.setFirstName(profile.getFirst_name().trim());
                    itemDummy.setLastName(profile.getLast_name().trim());
                    itemDummy.setProfilePic(profile.getProfilepic1().trim());
                    itemDummy.setGender(profile.getGender().trim());

                    // Open Chat Screen from here.
                    stopProgress();

//                    strFriendId = "";

                    /*Intent intent = new Intent(context, ChatDetailsActivity.class);
                    intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
                    *//*intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);*//*
                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, itemDummy);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);*/

//                    intent.putExtra("fromChatScreen", true);


                    if (actionFriendRequestAcceptReject == 1) {
                        sendLocalBroadcastManagerFriendRequestAction(1);
                    } else if (actionFriendRequestAcceptReject == 2) {
                        sendLocalBroadcastManagerFriendRequestAction(2);
                    } else {
                        // Nothing happens here.
                    }

                    if (getIntent().hasExtra("fromChatScreen") && getIntent().getBooleanExtra("fromChatScreen", false)) {
                        Intent intent = new Intent();
                        intent.putExtra("action", actionFriendRequestAcceptReject); // Action = 1 ==> Friend request accepted, 2 ==> Friend request
                        // rejected.
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED));
                        intent.putExtra(Constants.INTENT_CHAT_LIST, getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST));
                        setResult(RESULT_OK, intent);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        supportFinishAfterTransition();
                    } else {
                        intent = new Intent(context, ChatDetailsActivity.class);
                        intent.putExtra(Constants.INTENT_CHAT_LIST, FriendList);
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, itemDummy);
                        if (profile.getFriendstatus().trim().equalsIgnoreCase("Friend") || profile.getFriendstatus().trim().equalsIgnoreCase("1")
                                || profile.getFriendstatus().trim().equalsIgnoreCase("Sent")) {
                            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, false);
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                        } else {
                            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
                            startActivityForResult(intent, Constants.ACTION_CODE_SEND_NEW_REQUEST);
//                        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                            overridePendingTransition(0, 0);
                        }
                    }
//                        activity.startActivity(intent);

                } else {
                    showSnackBarMessageOnly(errormessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (action == Constants.ACTION_CODE_API_GET_USER_TASTEBUDS) {
            Log.d("RESPONSE_TASTEBUDS_USER", "Response: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                if (status) {
                    listUserTastebuds = new ArrayList<>();
                    JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject data = dataArray.getJSONObject(i);
                        TastebudChipVO chip = new TastebudChipVO();
                        chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
                        chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
                        chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
                        chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
                        chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
                        chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                        /*chip.setType(1);*/
                        listUserTastebuds.add(chip);
                            /*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*/
                    }

                    // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
                    initLayoutSuggestionsChips();
                    stopProgress();
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            }
        } else if (action == Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST) {
            Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                if (status) {
                    JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {

                    }

                    stopProgress();
                    callFriendsListApi();
//                    showSnackBarMessageOnly(context.getString(R.string.friend_request_sent_successfully));

                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            }
        } else if (action == Constants.ACTION_CODE_API_ACCEPT_FRIEND_REQUEST) {
            Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                if (status) {

                    JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {

                    }

                    actionFriendRequestAcceptReject = 1;
                    callFriendsListApi();

                    /*intent = new Intent(context, ChatListActivity.class);
                    startActivity(intent);
                    finish();

                    stopProgress();
                    showSnackBarMessageOnly(context.getString(R.string.you_are_now_friends));*/
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            }
        } else if (action == Constants.ACTION_CODE_API_REJECT_FRIEND_REQUEST) {
            Log.d(TAG, "RESPONSE_SEND_FRIEND_REQUEST_API: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(context.getResources().getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(context.getResources().getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(context.getResources().getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(context.getResources().getString(R.string.api_response_param_key_errormessage)).trim() : "";
                int total = !settings.isNull("total") ? settings.getInt("total") : 0;

                if (status) {
                    JSONArray dataArray = !jsonObjectResponse.isNull(context.getResources().getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(context.getResources().getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {

                    }

                    /*stopProgress();*/
//                    showSnackBarMessageOnly(context.getString(R.string.friend_request_rejected));

                    // Set result ok to chat details screen, if this screen is originated from. Also set action to 2, to perform frien request is
                    // rejected.
                    actionFriendRequestAcceptReject = 2;
                    callFriendsListApi();
                    /*if (getIntent().hasExtra("fromChatScreen") && getIntent().getBooleanExtra("fromChatScreen", false)) {
                        Intent intent = new Intent();
                        intent.putExtra("action", 2); // Action = 1 ==> Friend request rejected.
                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED));
                        intent.putExtra(Constants.INTENT_CHAT_LIST, getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST));
                        setResult(RESULT_OK, intent);
                        supportFinishAfterTransition();
                    }*/
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                showSnackBarMessageOnly(context.getResources().getString(R.string.some_error_occured));
            }
        }
    }

    private String saveImageToInternalAppStorage(Bitmap bitmapImage, String imageName) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, imageName + ".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        return directory.getAbsolutePath();
        return mypath.getAbsolutePath();
    }

    private Bitmap loadImageBitmapFromInternalAppStorage(String imageName) {
        Bitmap bitmapReturn = null;
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, imageName + ".png");
//            File f=new File(path, imageName+".png");
            bitmapReturn = BitmapFactory.decodeStream(new FileInputStream(mypath));
            /*Bitmap b = BitmapFactory.decodeStream(new FileInputStream(mypath));
            ImageView img = (ImageView) findViewById(R.id.imgPicker);
            img.setImageBitmap(b);*/
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return bitmapReturn;
        }

    }

    public File getFileFromLocalStorage(String fileName) {
        File fileImage = null;
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, fileName + ".png");
            fileImage = mypath;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return fileImage;
        }
    }

    private void showValuesToFieldsOtherProfile() {
        totalPictures = 0;
        if (restoImageSlider != null)
            restoImageSlider.removeAllSliders();

        if (!getAge(profile.getDob()).isEmpty() && !getAge(profile.getDob()).equalsIgnoreCase("")
                && !getAge(profile.getDob()).trim().equalsIgnoreCase("0")) {
            textViewNameAge.setText(profile.getFirst_name() + " " + profile.getLast_name() + ", " + getAge(profile.getDob()));
        } else {
            textViewNameAge.setText(profile.getFirst_name() + " " + profile.getLast_name());
        }

        txtAbout.setText(getString(R.string.about) + " " + profile.getFirst_name()); // + " " + profile.getLast_name()
        textViewPlace.setText(getPlaceAddress(profile.location));
        txtViewRelationShip.setText(profile.getRelationship());
        txtviewEthnicity.setText(profile.getEthnicity());
        txtViewOccupation.setText(profile.getOccupation());
        txtViewEducation.setText(profile.getEducation());
        expandableTextView.setText(profile.getAbout_me());

        // Set Gender here.
        if (profile.getGender().trim().equalsIgnoreCase(getString(R.string.gender_male_value))) {
            txtViewGender.setText(getString(R.string.gender_male));
        } else if (profile.getGender().trim().equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
            txtViewGender.setText(getString(R.string.gender_lgbt));
        } else {
            txtViewGender.setText(getString(R.string.gender_female));
        }
//        sendRequestGetTastebudsForUser();

        // Set tastebuds list here.
        listUserTastebuds = new ArrayList<>();
        // Fetch all tastebuds from local.
        if (!profile.getTestbuds().trim().isEmpty()) {
            List<String> listItemsTastebuds = new ArrayList<String>(Arrays.asList(
                    profile.getTestbuds().trim().split("\\s*,\\s*")));
            Log.d(TAG, "All tastebuds for user: " + listItemsTastebuds.toString());
            for (int i = 0; i < listItemsTastebuds.size(); i++) {
                TastebudChipVO chip = new TastebudChipVO();
//            chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
//            chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
//            chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
//            chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
//            chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
//            chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                chip.setName(listItemsTastebuds.get(i).trim());
            /*chip.setType(1);*/
                listUserTastebuds.add(chip);
                            /*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*/
            }
        }
       /* for (int i = 0; i < dataArray.length(); i++) {
            JSONObject data = dataArray.getJSONObject(i);
            TastebudChipVO chip = new TastebudChipVO();
            chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
            chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
            chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
            chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
            chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
            chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");
                        *//*chip.setType(1);*//*
            listUserTastebuds.add(chip);
                            *//*listTastebudsSuggestion.add(new TastebudChipVO("American", 0));*//*
        }*/

        // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
        initLayoutSuggestionsChips();
        stopProgress();

        if (getIntent().hasExtra(Constants.INTENT_USER_ID)
                && !otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Pending")) {
//            TODO Change icon of FriendRequest
                isFriendRequestSent = true;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.VISIBLE);
                linearLayoutSendRequest.setVisibility(View.GONE);
                imgViewSendRequest.setImageResource(R.drawable.ic_request_pending_red_small);
            } else if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Friend")) {
                isFriendRequestSent = false;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.selector_ic_chat_medium_red_selected);
                isfriend = true;

            } else if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Sent")) {
                isFriendRequestSent = false;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.ic_send_request);

            } else {
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.selector_ic_invite_add_friend_red);
            }
        } else {
            linearLayoutSendRequest.setVisibility(View.GONE);
            linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
        }


        if (otherUserID.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim())) {
            customSliderView = new CustomSliderView(this);

            File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1);
            if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic1().isEmpty()) {
                totalPictures++;
                customSliderView.image(fileProfilePic);

                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {

                            }
                        });
                restoImageSlider.addSlider(customSliderView);

                loadProfilePic2();
            } else if (!profile.getProfilepic1().isEmpty()) {
                totalPictures++;
                target1 = new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                        // Save image bitmap in local app data.
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());

                        try {
                            File fileImage = new File(filePath);

                            // initialize a SliderLayout
                            if (fileImage != null && fileImage.exists()) {
                                Log.d(TAG, "Image loaded from app data with path: " + filePath);
                                customSliderView.image(fileImage);
                            } else {
                                // initialize a SliderLayout
                                customSliderView.image(appendUrl(profile.getProfilepic1()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic1()));
                        } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                            } else {
                                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                            }
                            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {

                                        }
                                    });
                            restoImageSlider.addSlider(customSliderView);


                            loadProfilePic2();
                        }
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic1()));

                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);

                        loadProfilePic2();
                    }
                };
                Picasso.with(context)
                        .load(appendUrl(profile.getProfilepic1()))
                        .into(target1);
            } else {
                loadProfilePic2();
            }
        } else {
            if (!profile.getProfilepic1().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic1()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }
            if (!profile.getProfilepic2().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic2()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }
            if (!profile.getProfilepic3().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic3()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType
                        .CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }
            if (!profile.getProfilepic4().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic4()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType
                        .CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }
            if (!profile.getProfilepic5().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic5()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType
                        .CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }

            if (!profile.getProfilepic6().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic6()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }

            if (!profile.getProfilepic7().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic7()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }

            if (!profile.getProfilepic8().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic8()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }

            if (!profile.getProfilepic9().isEmpty()) {
                totalPictures++;
                CustomSliderView customSliderView = new CustomSliderView(this);
                // initialize a SliderLayout
                customSliderView.image(appendUrl(profile.getProfilepic9()));
                if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                } else {
                    customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                }
                customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                        .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                            }
                        });
                restoImageSlider.addSlider(customSliderView);
            }

            loadOtherProfileDetails();
        }


        /*if (profile.getProfilepic1().isEmpty() && profile.getProfilepic2().isEmpty() && profile.getProfilepic3().isEmpty() && profile.getProfilepic4().isEmpty()
                && profile.getProfilepic5().isEmpty()) {
            // Hide slider view and load placeholder image here.
            restoImageSlider.setVisibility(View.GONE);
            imgPlaceholder.setVisibility(View.VISIBLE);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                imgPlaceholder.setImageResource(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                imgPlaceholder.setImageResource(R.drawable.ic_placeholder_user_avatar_female_big);
            }
        } else {
            imgPlaceholder.setVisibility(View.GONE);
            restoImageSlider.setVisibility(View.VISIBLE);
        }

        restoImageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        restoImageSlider.setDuration(8000);
        restoImageSlider.addOnPageChangeListener(this);

        if (!getAge(profile.getDob()).isEmpty() && !getAge(profile.getDob()).equalsIgnoreCase("")
                && !getAge(profile.getDob()).trim().equalsIgnoreCase("0")) {
            textViewNameAge.setText(profile.getFirst_name() + " " + profile.getLast_name() + ", " + getAge(profile.getDob()));
        } else {
            textViewNameAge.setText(profile.getFirst_name() + " " + profile.getLast_name());
        }

        txtAbout.setText(getString(R.string.about) + " " + profile.getFirst_name()); // + " " + profile.getLast_name()
        textViewPlace.setText(getPlaceAddress(profile.location));
        txtViewRelationShip.setText(profile.getRelationship());
        txtviewEthnicity.setText(profile.getEthnicity());
        txtViewOccupation.setText(profile.getOccupation());
        expandableTextView.setText(profile.getAbout_me());
        sendRequestGetTastebudsForUser();

        if (getIntent().hasExtra(Constants.INTENT_USER_ID)
                && !otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Pending")) {
//            TODO Change icon of FriendRequest
                isFriendRequestSent = true;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.VISIBLE);
                linearLayoutSendRequest.setVisibility(View.GONE);
                imgViewSendRequest.setImageResource(R.drawable.ic_request_pending_red_small);
            } else if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Friend")) {
                isFriendRequestSent = false;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.selector_ic_chat_medium_red_selected);
                isfriend = true;

            } else if (!profile.getFriendstatus().isEmpty() && profile.getFriendstatus().equalsIgnoreCase("Sent")) {
                isFriendRequestSent = false;
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.ic_send_request);

            } else {
                linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
                linearLayoutSendRequest.setVisibility(View.VISIBLE);
                imgViewSendRequest.setImageResource(R.drawable.selector_ic_invite_add_friend_red);
            }
        } else {
            linearLayoutSendRequest.setVisibility(View.GONE);
            linearLayoutAcceptOrRejectRequest.setVisibility(View.GONE);
        }*/
    }

    private void loadOtherProfileDetails() {
        if (profile.getProfilepic1().isEmpty() && profile.getProfilepic2().isEmpty() && profile.getProfilepic3().isEmpty()
                && profile.getProfilepic4().isEmpty() && profile.getProfilepic5().isEmpty() && profile.getProfilepic6().isEmpty()
                && profile.getProfilepic7().isEmpty() && profile.getProfilepic8().isEmpty() && profile.getProfilepic9().isEmpty()) {
            // Hide slider view and load placeholder image here.
            restoImageSlider.setVisibility(View.GONE);
            imgPlaceholder.setVisibility(View.VISIBLE);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                imgPlaceholder.setImageResource(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                imgPlaceholder.setImageResource(R.drawable.ic_placeholder_user_avatar_female_big);
            }
        } else {
            imgPlaceholder.setVisibility(View.GONE);
            restoImageSlider.setVisibility(View.VISIBLE);
        }

        restoImageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        restoImageSlider.setDuration(8000);
        restoImageSlider.addOnPageChangeListener(this);
    }

    private void loadProfilePic2() {
        Log.d(TAG, "Profile pic 2 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic2().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic3();
        } else if (!profile.getProfilepic2().isEmpty()) {
            Log.d(TAG, "Profile pic 2 from picasso loading here.");
            totalPictures++;
            target2 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic2()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic2()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);

                        loadProfilePic3();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    customSliderView.image(appendUrl(profile.getProfilepic2()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);

                    loadProfilePic3();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic2()))
                    .into(target2);
        } else {
            loadProfilePic3();
        }
    }

    private void loadProfilePic3() {
        Log.d(TAG, "Profile pic 3 loading here.");
        customSliderView = new CustomSliderView(this);
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3);

        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic3().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic4();
        } else if (!profile.getProfilepic3().isEmpty()) {
            totalPictures++;
            target3 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic3()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic3()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic4();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic3()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic4();
                }
            };
            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic3()))
                    .into(target3);
        } else {
            loadProfilePic4();
        }
    }

    private void loadProfilePic4() {
        Log.d(TAG, "Profile pic 4 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic4().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic5();
        } else if (!profile.getProfilepic4().isEmpty()) {
            totalPictures++;
            target4 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic4()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic4()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic5();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic4()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic5();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic4()))
                    .into(target4);
        } else {
            loadProfilePic5();
        }
    }

    private void loadProfilePic5() {
        Log.d(TAG, "Profile pic 4 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic5().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic6();
        } else if (!profile.getProfilepic5().isEmpty()) {
            totalPictures++;
            target5 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic5()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic5()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic6();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic5()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic6();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic5()))
                    .into(target5);
        } else {
            loadProfilePic6();
        }
    }

    private void loadProfilePic6() {
        Log.d(TAG, "Profile pic 6 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic6().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic7();
        } else if (!profile.getProfilepic6().isEmpty()) {
            totalPictures++;
            target6 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic6()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic6()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic7();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic6()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic7();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic6()))
                    .into(target6);
        } else {
            loadProfilePic7();
        }
    }

    private void loadProfilePic7() {
        Log.d(TAG, "Profile pic 7 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic7().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic8();
        } else if (!profile.getProfilepic7().isEmpty()) {
            totalPictures++;
            target7 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic7()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic7()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic8();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic7()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic8();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic7()))
                    .into(target7);
        } else {
            loadProfilePic8();
        }
    }

    private void loadProfilePic8() {
        Log.d(TAG, "Profile pic 8 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic8().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadProfilePic9();
        } else if (!profile.getProfilepic8().isEmpty()) {
            totalPictures++;
            target8 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic8()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic8()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);
                        loadProfilePic9();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic8()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadProfilePic9();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic8()))
                    .into(target8);
        } else {
            loadProfilePic9();
        }
    }

    private void loadProfilePic9() {
        Log.d(TAG, "Profile pic 9 loading here.");
        customSliderView = new CustomSliderView(this);

        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9);
        if (fileProfilePic != null && fileProfilePic.exists() && !profile.getProfilepic9().isEmpty()) {
            totalPictures++;
            customSliderView.image(fileProfilePic);

            if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
            } else {
                customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
            }
            customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);

            loadOtherProfileDetails();
        } else if (!profile.getProfilepic9().isEmpty()) {
            totalPictures++;
            target9 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */

                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim());

                    try {
                        File fileImage = new File(filePath);

                        // initialize a SliderLayout
                        if (fileImage != null && fileImage.exists()) {
                            Log.d(TAG, "Image loaded from app data with path: " + filePath);
                            customSliderView.image(fileImage);
                        } else {
                            // initialize a SliderLayout
                            customSliderView.image(appendUrl(profile.getProfilepic9()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // initialize a SliderLayout
                        customSliderView.image(appendUrl(profile.getProfilepic9()));
                    } finally {
            /*customSliderView.image(appendUrl(profile.getProfilepic1()));*/
                        if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                        } else {
                            customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                        }
                        customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                    }
                                });
                        restoImageSlider.addSlider(customSliderView);

                        loadOtherProfileDetails();
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
                    customSliderView.image(appendUrl(profile.getProfilepic9()));

                    if (profile.getGender().trim().toLowerCase().equalsIgnoreCase("male")) {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_male_big);
                    } else {
                        customSliderView.empty(R.drawable.ic_placeholder_user_avatar_female_big);
                    }
                    customSliderView.setScaleType(BaseSliderView.ScaleType.CenterInside)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {

                                }
                            });
                    restoImageSlider.addSlider(customSliderView);
                    loadOtherProfileDetails();
                }
            };

            Picasso.with(this)
                    .load(appendUrl(profile.getProfilepic9()))
                    .into(target9);
        } else {
            loadOtherProfileDetails();
        }
    }

    private String getAge(String BirthDate) {
        if (BirthDate.isEmpty()) {
            return "";
        }

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        try {
            date = format.parse(BirthDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int year = date.getYear() + 1900;
        int month = date.getMonth();
        int day = date.getDay();
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    private String getPlaceAddress(String Location) {
        String place = "";
        double latitude = 0.0, longitude = 0.0;
        String[] location = Location.trim().split(",");
        if (location.length == 2) {
            latitude = Double.valueOf(location[0].trim());
            longitude = Double.valueOf(location[1].trim());
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
        }
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();

            if (city != null && !city.trim().isEmpty() && !city.trim().equalsIgnoreCase("null")) {
                place = city;
            }
            if (state != null && !state.trim().isEmpty() && !state.trim().equalsIgnoreCase("null")) {
                if (place.trim().isEmpty()) {
                    place = state;
                } else {
                    place = place + ", " + state;
                }

            }
            if (country != null && !country.trim().isEmpty() && !country.trim().equalsIgnoreCase("null")) {
                if (country.trim().isEmpty()) {
                    place = country;
                } else {
                    place = place + ", " + country;
                }
            }
        }

        return place;
    }

    private void sendRequestGetTastebudsForUser() {
        if (!profile.getTestbuds().trim().isEmpty()) {
//            showProgress(getString(R.string.loading));
            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_gettestbuds));
            params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            params.put(getString(R.string.api_param_key_testbudids), profile.getTestbuds());
            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_GET_USER_TASTEBUDS, SingleUserDetailActivity.this);
        }
    }

    private void sendFriendRequestToUserSuggestion() {
        showProgress(context.getResources().getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(context.getResources().getString(R.string.api_param_key_action), context.getResources().getString(R.string.api_response_param_action_sendfriendrequest));
        params.put(context.getResources().getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(context.getResources().getString(R.string.api_param_key_friendid), otherUserID);

        new HttpRequestSingletonPost(context, context.getResources().getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_SEND_FRIEND_REQUEST, SingleUserDetailActivity.this);
    }

    @Override
    public void onResume() {
        mNotificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, final Intent intent) {
                Log.d(TAG, "Notification register onReceive called...");
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.cancel(Constants.NOTIFICATION_ID_MESSAGE);

               /* SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, "");
                SharedPreferenceUtil.save();

                // Clear all the notifications in notification bar
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancelAll();*/

                /*if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                    UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                    if (user.getId().trim().equalsIgnoreCase(otherUserID.trim())) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ApiViewProfileOtherUser();
                                if (!otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                    imgViewEditProfile.setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                }*/

                if (intent.hasExtra("action") && !intent.getStringExtra("action").trim().isEmpty()) {
                    if (intent.getStringExtra("action").trim().equalsIgnoreCase("1")) { // Friend request accepted. Notify data set changed here.
                        if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                            UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                            // Iterate through user suggestion array and update user card if applicable.
                            Log.d(TAG, "Friend request action accepted. Friend ID: " + user.getId().trim());
                            if (otherUserID.equalsIgnoreCase(user.getId().trim())) {
                                Log.d(TAG, "Friend request action accepted. Friend ID: " + user.getId().trim()
                                        + ", Card ID: " + otherUserID.trim());
                                if (profile != null) {
                                    profile.setFriendstatus("Friend");
                                }

                                ApiViewProfileOtherUser();
                                if (!otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                    imgViewEditProfile.setVisibility(View.GONE);
                                }
                            }
                        }
                    } else if (intent.getStringExtra("action").trim().equalsIgnoreCase("2")) { // Friend request received. Notify data set changed here.
                        if (intent.hasExtra("user") && intent.getSerializableExtra("user") != null) {
                            UserProfileAPI user = (UserProfileAPI) intent.getSerializableExtra("user");

                            // Iterate through user suggestion array and update user card if applicable.
                            Log.d(TAG, "Friend request action received. Friend ID: " + user.getId().trim());
                            if (otherUserID.trim().equalsIgnoreCase(user.getId().trim())) {
                                Log.d(TAG, "Friend request action received. Friend ID: " + user.getId().trim()
                                        + ", Card ID: " + otherUserID.trim());
                                if (profile != null) {
                                    profile.setFriendstatus("Pending");
                                }

                                ApiViewProfileOtherUser();
                                if (!otherUserID.equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
                                    imgViewEditProfile.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(SingleUserDetailActivity.this)
                .registerReceiver(mNotificationReceiver, new IntentFilter(Constants.FILTER_NOTIFICATION_RECEIVED));
        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyBroadcastReceiver();
            registerReceiver(mReceiver, new IntentFilter("refreshUI"));
            mIsReceiverRegistered = true;
        }

        Log.d(TAG, "OnResume. Otheruserid: " + otherUserID);
        if (getIntent().hasExtra(Constants.INTENT_USER_ID)) {
//                && !otherUserID.trim().equalsIgnoreCase(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""))) {
            ApiViewProfileOtherUser();
        }
        super.onResume();


    }

    @Override
    public void onPause() {
        /*try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(SingleUserDetailActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {*/
        super.onPause();
        /*}*/
    }

    @Override
    protected void onDestroy() {
        try {
            if (mNotificationReceiver != null)
                LocalBroadcastManager.getInstance(SingleUserDetailActivity.this).unregisterReceiver(mNotificationReceiver);

            if (mIsReceiverRegistered) {
                unregisterReceiver(mReceiver);
                mReceiver = null;
                mIsReceiverRegistered = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            super.onDestroy();
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("isFromTab") && getIntent().getBooleanExtra("isFromTab", false)) {
            intent = new Intent(mContext, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(0, 0);
            supportFinishAfterTransition();
        } else if (getIntent().hasExtra("fromChatScreen") && getIntent().getBooleanExtra("fromChatScreen", false)) {
            Intent intent = new Intent();
            intent.putExtra(Constants.INTENT_CHAT_SELECTED, getIntent().getSerializableExtra(Constants.INTENT_CHAT_SELECTED));
            intent.putExtra(Constants.INTENT_CHAT_LIST, getIntent().getSerializableExtra(Constants.INTENT_CHAT_LIST));
            setResult(RESULT_CANCELED, intent);
            supportFinishAfterTransition();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ACTION_CODE_SEND_NEW_REQUEST) {
//            intent.putExtra(Constants.INTENT_CHAT_SEND_NEW_REQUEST, true);
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (data.hasExtra(Constants.INTENT_CHAT_SELECTED) && data.getSerializableExtra(Constants.INTENT_CHAT_SELECTED) != null) {
                        ChatListVO chatSelected = (ChatListVO) data.getSerializableExtra(Constants.INTENT_CHAT_SELECTED);
                        if (otherUserID.trim().equalsIgnoreCase(chatSelected.getUserId().trim())) {
                            if (profile != null) {
                                profile.setFriendstatus("Sent");

                                // Update view here.
                                showValuesToFieldsOtherProfile();

                                showProgress(getString(R.string.loading));
                                ApiViewProfileOtherUser();
                            }
                        }
                    }
                    break;

                case Activity.RESULT_CANCELED:

                    break;
            }
        }
    }

    /**
     * Send local broadcast manager to other running activities to update the status of accepted or rejected friend request in the view, if the
     * match or friend is valid.
     *
     * @param action Action taken on friend request.
     *               1 ==> Friend request accepted.
     *               2 ==> Friend request rejected.
     */
    private void sendLocalBroadcastManagerFriendRequestAction(int action) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_FRIEND_REQUEST_ACCEPTED_OR_REJECTED);

        // Update current activity using Local Broadcast manager if needed.
        if (action == 1) { // Friend request accepted.

            i.putExtra("action", String.valueOf(action));
            i.putExtra("otherUserId", otherUserID.trim());
        } else if (action == 2) { // Friend request rejected.
            i.putExtra("action", String.valueOf(action));
//            i.putExtra("user", chatSelected);
            i.putExtra("otherUserId", otherUserID.trim());
        }

        LocalBroadcastManager.getInstance(context).sendBroadcast(i);
    }
}
