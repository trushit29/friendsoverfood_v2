package com.friendsoverfood.android.EditProfileNew;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by trushitshah on 22/07/17.
 */

public class UserProfilePictureVO implements Serializable {
    public UserProfilePictureVO() {
    }

    public String imageUrl = "", imageName = "", imagePath = "";
    public Bitmap imageBitmap = null;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }
}
