package com.friendsoverfood.android.Home;

import java.io.Serializable;

/**
 * Created by Trushit on 24/04/17.
 */

public class TasteBudsToken implements Serializable {

    private String name = "";
    private String id = "";
    private int type = 0;
    private boolean isSelected = false;

    /**
     * Create object of person class.
     *
     * @param name name of the tastebud
     * @param id   id of the tastebud
     * @param type Type used for color of token. By default type ==> 0, color will be primary app color.
     */
    public TasteBudsToken(String name, String id, int type) {
        this.name = name;
        this.id = id;
        this.type = type;
        isSelected = false;
    }

    /**
     * Create object of person class.
     *
     * @param name       name of the tastebud
     * @param id         id of the tastebud
     * @param type       Type used for color of token. By default type ==> 0, color will be primary app color.
     * @param isSelected Whether token is displayed as selected or not.
     */
    public TasteBudsToken(String name, String id, int type, boolean isSelected) {
        this.name = name;
        this.id = id;
        this.type = type;
        this.isSelected = isSelected;
    }

    /**
     * Create object of person class.
     *
     * @param name       name of the tastebud
     * @param id         id of the tastebud
     * @param isSelected Whether token is displayed as selected or not.
     */
    public TasteBudsToken(String name, String id, boolean isSelected) {
        this.name = name;
        this.id = id;
        this.isSelected = isSelected;
    }

    public TasteBudsToken() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    //    public Person(String n, String e) {
//        name = n;
//        id = e;
//    }

    public String getName() {
        return name;
    }

    public void setName(String activityName) {
        name = activityName;
    }

    public String getId() {
        return id;
    }

    public void setId(String Id) {
        id = Id;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
