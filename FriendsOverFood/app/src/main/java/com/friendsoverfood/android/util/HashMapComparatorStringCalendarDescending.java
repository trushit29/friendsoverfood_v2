package com.friendsoverfood.android.util;

import com.friendsoverfood.android.Constants;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Map;

class HashMapComparatorStringCalendarDescending implements
        Comparator<Map<String, String>> {

    private final String key;

    public HashMapComparatorStringCalendarDescending(String key) {
        this.key = key;
    }

    public int compare(Map<String, String> first, Map<String, String> second) {
        Calendar calFirst = Constants.stringToCalendarDate(first.get(key));
        Calendar calSecond = Constants.stringToCalendarDate(second.get(key));

        return calFirst.getTimeInMillis() < calSecond.getTimeInMillis() ? -1
                : calFirst.getTimeInMillis() > calSecond.getTimeInMillis() ? 1
                : 0;
    }
}
