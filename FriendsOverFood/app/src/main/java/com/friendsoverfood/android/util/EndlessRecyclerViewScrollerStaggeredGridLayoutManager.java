package com.friendsoverfood.android.util;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;

/**
 * Created by Trushit on 10/09/15.
 */
public abstract class EndlessRecyclerViewScrollerStaggeredGridLayoutManager extends RecyclerView.OnScrollListener {

    private boolean isListScrolledWithTouch = true;
    private int countListScrolledItems = 0;
    private int countLastItemScrolled = 0;
    private int countCurrentPageIndexForItem = 1;
    private int resultPerPage = 20;

    private int visibleItemCount;
    private int totalItemCount;
    private int[] firstVisibleItem;
    int previousFirstVisibleItem[];

    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    public EndlessRecyclerViewScrollerStaggeredGridLayoutManager(StaggeredGridLayoutManager staggeredGridLayoutManager, int spanCount, int resultPerPage) {
        this.staggeredGridLayoutManager = staggeredGridLayoutManager;
        this.firstVisibleItem = new int[spanCount];
        this.resultPerPage = resultPerPage;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = staggeredGridLayoutManager.getChildCount();
        totalItemCount = staggeredGridLayoutManager.getItemCount();
        firstVisibleItem = staggeredGridLayoutManager.findFirstVisibleItemPositions(firstVisibleItem);

        if (firstVisibleItem != null && firstVisibleItem.length > 0) {
            countListScrolledItems = firstVisibleItem[0];
            onRecyclerViewScrolled(countListScrolledItems, visibleItemCount);
        }



        if (isListScrolledWithTouch) {
            int lastItem = countListScrolledItems + visibleItemCount;
            Log.e("LastItem", "Last item: " + lastItem + ", FirstVisible Item: " + String.valueOf(firstVisibleItem[0]) + ", Total: " + totalItemCount);
            if (lastItem >= totalItemCount) {
                if (countLastItemScrolled != lastItem) { //to avoid multiple calls for last item
                    Log.i("LIST SCROLL AT END", "Position: " + lastItem);
                    Log.i("TOTAL ITEMS", "" + (countCurrentPageIndexForItem * resultPerPage));
                    countLastItemScrolled = lastItem;

                    // Call API for next page items
                    if (lastItem == (countCurrentPageIndexForItem * resultPerPage)) {
                        countCurrentPageIndexForItem++;
//                        Log.e("MESSAGE API", "API to be called");
                        onLoadMore(lastItem);
                    }
                }
            }
        }
    }

    public abstract void onLoadMore(int countCurrentPageIndexForItem);

    public abstract void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount);

    /*public abstract void onLoadMoreTopItems();*/
}
