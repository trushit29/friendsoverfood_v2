package com.friendsoverfood.android.EditProfileNew;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.R;

/**
 * Created by Trushit on 26/06/17.
 */

public class SpinnerCustomAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder holder;
    private LayoutInflater inflator;
    private String[] items;
    private BaseActivity activity;

    public SpinnerCustomAdapter(Context context, String[] items) {
        this.context = context;
        inflator = LayoutInflater.from(context);
        this.items = items;
        this.activity = (BaseActivity) context;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        holder = new ViewHolder();
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflator.inflate(R.layout.layout_custom_spinner_adapter_view, null);
//            holder.imageIcon = (ImageView) convertView.findViewById(R.id.imageIcon);
            holder.textViewSpinner = (TextView) convertView.findViewById(R.id.spinnerViewCustom);
            activity.setAllTypefaceMontserratRegular(holder.textViewSpinner);
            convertView.setTag(holder);
        }

        holder.textViewSpinner.setText(items[position]);

        return convertView;
    }

    @Override
    public View getDropDownView(int positionmy, View convertView, ViewGroup parent) {   // This view starts when we click the spinner.
        holder = new ViewHolder();
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflator.inflate(R.layout.layout_custom_spinner_adapter_dropdown_view, null);
//            holder.imageIcon = (ImageView) convertView.findViewById(R.id.imageIcon);
            holder.textViewSpinner = (TextView) convertView.findViewById(R.id.spinnerCustomDropdownView);
            activity.setAllTypefaceMontserratRegular(holder.textViewSpinner);
            convertView.setTag(holder);
        }
        holder.textViewSpinner.setText(items[positionmy]);

        return convertView;
    }

    class ViewHolder {
        //        private ImageView imageIcon;
        private TextView textViewSpinner;

    }
}