package com.friendsoverfood.android.EditProfileNew;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.AccessToken;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.DemoStaggeredGrid.AbstractDataProvider;
import com.friendsoverfood.android.DemoStaggeredGrid.ExampleDataProviderFragment;
import com.friendsoverfood.android.FacebookPhotos.FacebookAlbumsListActivity;
import com.friendsoverfood.android.FacebookPhotos.FacebookPhotoVO;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.SignIn.TasteBudsSuggestionsActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;
import com.squareup.picasso.Target;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public class EditProfileNewActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, AdapterView.OnItemSelectedListener {

    private Context context;
    private LayoutInflater inflater;
    private View view;

    //    ACTIVITY VARIABLES HERE
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private boolean isFromSave = false;
    private final String TAG = "EDIT_PROFILE_NEW_ACTIVITY";
    public UserProfileAPI profile;
    public FloatingActionButton fabSkip;
//    public android.support.design.widget.FloatingActionButton fabSkipDefault;
//    private RelativeLayout relativeLayoutSkip;
//    public TextView txtSkip;

    //    PHOTO VARIABLES
    public ImageView imgForUploadSelected = null;
    public ImageView imgAddButtonForUploadSelected = null;
    public ImageView imgRemoveButtonForUploadSelected = null;
    public boolean isFromPlus = false;
    /*public ImageView imageViewEditProfileActivityUserImage4, imageViewEditProfileActivityAddUserImage5, imageViewEditProfileActivityUserImage5,
            imageViewEditProfileActivityAddUserImage3, imageViewEditProfileActivityUserImage3, imageViewEditProfileActivityAddUserImage4,
            imageViewEditProfileActivityAddUserImage2, imageViewEditProfileActivityUserImage2, imageViewEditProfileActivityAddUserImage1,
            imageViewEditProfileActivityUserImage1;*/
    public int sizeImageUpload = Constants.convertDpToPixels(158);

    //    PREFERENCES VARIABLES
    public TextView textViewTerms, textViewPrivayPolicy, textViewLincenses, textViewShowDistanceIn, textViewAgeRange, textViewDistance, txtViewShowMe;
    public SwitchCompat switchMessages, switchNewMatches, switchLocationShowHide; //  switchFeMale, switchMale, switchLgbt;
    public Button btnMalePreferences, btnFemalePreferences, btnLgbtPreferences;
    public SeekBar seekBarActivityDiscoveryPreferencesSearchDistance;
    public LinearLayout llLogout, llShareFOF, llhelp;
    public Button btnMile, btnKilometer;
    public RangeBar rangebarAge;
    public String age1 = "18";
    public String age2 = "100";
    public int mMaxValue = 100, mMinValue = 0, mMinAge = 40, mMaxAge = 80;
    public int distanceSelected = 0;
    public final double KM_TO_MILE = 0.621371;
    public final double MILE_TO_KM = 1.60934;
    public boolean isfromButton = false;

    //    PROFILE VARIABLES
//    public ChipView text_chip_layout;
//    public TextView txtViewNoTastebuds;
//    public ChipViewAdapter adapterLayout;
//    public List<TastebudChipVO> listUserTastebuds = new ArrayList<>();
//    public LinearLayout textViewEditTasteBuds;
    public Button btnSelectTastebuds;
    public TextView txtViewCounter;
    public SliderLayout restoImageSlider;
    public RelativeLayout relativeSlider;
    public LinearLayout linearUploadPhotos;

    public Bitmap bitmapCameraUpload = null;
    private Button btnSave;
    public RelativeLayout relativeRootProfileFragment;
    public EditText editTextName, editTextLastName, editTextAbout, editTextRelationShipStatus, editTextEthnicity, editTextOccupation,
            editTextBirthday, editTextEducation, editTextGender;
    public LinearLayout linearLayoutSelectGender;
    public Button btnMale, btnFemale, btnLgbt;
    public TextInputLayout textInputLayoutSignupActivityEthinicity;
    public int totalPictures = 0;
    public ImageView imgDummyProfile, imgDummyPreferences;
    public Spinner spinner;
    public Calendar myCalendar = Calendar.getInstance();
    public boolean isToExpandGender = false;

    // Photos Fragment
    public RecyclerView mRecyclerViewEditPhotos;
    public StaggeredGridLayoutManager mLayoutManagerEditPhotos;
    public RecyclerView.Adapter mAdapterEditPhotos;
    public RecyclerView.Adapter mWrappedAdapterEditPhotos;
    public RecyclerViewDragDropManager mRecyclerViewDragDropManagerEditPhotos;
    //    public ArrayList<HashMap<String, String>> listProfilePictures = new ArrayList<>();
    public ArrayList<UserProfilePictureVO> listProfilePictures = new ArrayList<>();
    //    public PhotosFragment fragmentPhotos;
//    public ExampleDataProviderFragment fragmentDummyProvider;
    private static final String FRAGMENT_TAG_DATA_PROVIDER = "data provider";

    public DatePickerDialog.OnDateSetListener date;
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    public DatePickerDialog dialog;

    public final int MAX_DISTANCE_RANGE_KM = 160; // 100 Mi. * 1.60934
    public final int MAX_DISTANCE_RANGE_MI = 100; // 1 Km * 0.621371

    public boolean isTastebudsEdit = false, isUnsavedChanges = false, isRevertUnsavedChanges = false;

    // Image cropping variables
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    // Permissions variables
    public boolean isPermissionDialogDisplayed = false;
    public boolean isRemoveToDisplay = false;
    public String mCurrentPhotoPath = "";

    public Target target1, target2, target3, target4, target5, target6, target7, target8, target9, targetFacebook;
    public int currentItemInViewPager = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init(savedInstanceState);

        setVisibilityActionBar(true);
        if (getIntent().hasExtra(Constants.INTENT_USER_LOGGED_IN) && getIntent().getBooleanExtra(Constants.INTENT_USER_LOGGED_IN, false)) {
//            setTitleSupportActionBarItalic(getString(R.string.tastebuds_profile));
            setTitleSupportActionBarItalic((!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "").trim().isEmpty()) ?
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "") + getString(R.string.profile_title_suffix)
                    : getString(R.string.your_profile));
//            setVisibilityFooterTabsBase(false);
            setVisibilityActionBarBackButton(false);

            txtToolbarSkipLeft.setVisibility(View.VISIBLE);
            setAllTypefaceMontserratRegular(txtToolbarSkipLeft);
        } else {
//            setTitleSupportActionBarItalic(getString(R.string.tastebuds_profile));
            setTitleSupportActionBarItalic((!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "").trim().isEmpty()) ?
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, "") + getString(R.string.profile_title_suffix)
                    : getString(R.string.your_profile));
//            setVisibilityFooterTabsBase(true);
            setVisibilityActionBarBackButton(true);
            setSelectorFooterTabsBase(1);

            txtToolbarSkipLeft.setVisibility(View.GONE);
        }

        // Customize height to reduce the padding between header and tab layout. - Height is 36 dp.
        toolbarLayoutRoot.setMinimumHeight(Constants.convertDpToPixels(42));


        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) toolbarLayoutRoot.getLayoutParams();
        layoutParams.height = Constants.convertDpToPixels(42);
//        layoutParams.setMargins(0, 0, 0, 0);
        toolbarLayoutRoot.setLayoutParams(layoutParams);

        toolbarLayoutRoot.setPadding(toolbarLayoutRoot.getPaddingLeft(), toolbarLayoutRoot.getPaddingTop(),
                toolbarLayoutRoot.getPaddingRight(), 0);

        // Display app theme red bg with small icons in header toolbar. So that set appropriate bg in the root of base activity and make toolbar &
        // tab layout background transparent programmatically.
        coordinatorRoot.setBackgroundResource(R.drawable.ic_bg_app_red_small_icons);
        toolbarLayoutRoot.setBackgroundResource(R.color.transparent);
        tabLayout.setBackgroundResource(R.color.transparent);
    }

    //ACTIVITY METHODS
    private void init(Bundle savedInstanceState) {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_edit_profile_new_activity, getMiddleContent());
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        fabSkip = (FloatingActionButton) findViewById(R.id.fabSkip);
        /*relativeLayoutSkip = (RelativeLayout) findViewById(R.id.relativeLayoutSkip);
        fabSkipDefault = (android.support.design.widget.FloatingActionButton) findViewById(R.id.fabSkipDefault);
        txtSkip = (TextView) findViewById(R.id.textViewSkip);*/

        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.white_selector));
        toolbarLayoutRoot.setElevation(0);

        /*setAllTypefaceMontserratRegular(txtSkip);*/

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(new ExampleDataProviderFragment(), FRAGMENT_TAG_DATA_PROVIDER)
                    .commit();
        }

        isRevertUnsavedChanges = true;
        ApiViewProfile();
        /*sendRequestGetTastebudsForUser();*/

       /* if (getIntent().hasExtra(Constants.INTENT_USER_LOGGED_IN) && getIntent().getBooleanExtra(Constants.INTENT_USER_LOGGED_IN, false)) {
//            fabSkip.setVisibility(View.GONE);
            txtSkip.setVisibility(View.VISIBLE);
        } else {
//            fabSkip.setVisibility(View.GONE);
            relativeLayoutSkip.setVisibility(View.GONE);
        }*/

//        fabSkip.setOnClickListener(new View.OnClickListener() {
        /*relativeLayoutSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == 0) {
                    viewPager.setCurrentItem(1);
                } else if (viewPager.getCurrentItem() == 1) {
                    viewPager.setCurrentItem(2);
                } else if (viewPager.getCurrentItem() == 2) {
                    *//*onBackPressed();*//*
                    intent = new Intent(mContext, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
            }
        });*/

        txtToolbarSkipLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == 0) {
                    viewPager.setCurrentItem(1);
                } else if (viewPager.getCurrentItem() == 1) {
                    viewPager.setCurrentItem(2);
                } else if (viewPager.getCurrentItem() == 2) {
                    intent = new Intent(mContext, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    supportFinishAfterTransition();
                }
            }
        });
    }

    private void setListners() {
        TextChangeListener(editTextAbout);
        TextChangeListener(editTextBirthday);
//        TextChangeListener(editTextGender);
        TextChangeListener(editTextEthnicity);
        TextChangeListener(editTextLastName);
        TextChangeListener(editTextName);
        TextChangeListener(editTextOccupation);
        TextChangeListener(editTextEducation);
        TextChangeListener(editTextRelationShipStatus);

        editTextName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    editTextLastName.requestFocus();
                }
                return false;
            }
        });

        editTextLastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    editTextAbout.requestFocus();
                }
                return false;
            }
        });

        editTextAbout.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d(TAG, "Event code: " + event.getKeyCode() + ", " + event.getKeyCharacterMap());
                if (editTextAbout.getText().toString().trim().length() >= 250
                        && (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN)) {
                    Log.i(TAG, "Enter pressed");
                    editTextBirthday.requestFocus();
                }
                return false;
            }
        });

        editTextBirthday.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    editTextRelationShipStatus.requestFocus();
                }
                return false;
            }
        });

        editTextRelationShipStatus.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    editTextEthnicity.requestFocus();
                }
                return false;
            }
        });

        editTextEthnicity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    editTextOccupation.requestFocus();
                }
                return false;
            }
        });

        editTextOccupation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);*/
                    editTextEducation.requestFocus();
                }
                return false;
            }
        });

        editTextEducation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    /*Log.i(TAG,"Enter pressed");*/
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                return false;
            }
        });

        relativeSlider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeSlider.setVisibility(View.GONE);
                linearUploadPhotos.setVisibility(View.VISIBLE);

            }
        });
    }

    private void TextChangeListener(final EditText view) {
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }

                switch (view.getId()) {
                    case R.id.editTextName:
                        Log.d(TAG, "On text change. Name: " + editTextName.getText().toString().trim());
                        profile.setFirst_name(editTextName.getText().toString().trim());
                        break;
                    case R.id.editTextLastName:
                        profile.setLast_name(editTextLastName.getText().toString().trim());
                        break;
                    case R.id.editTextAbout:
                        profile.setAbout_me(editTextAbout.getText().toString().trim());
                        break;
                    case R.id.editTextRelationShipStatus:
                        profile.setRelationship(editTextRelationShipStatus.getText().toString().trim());
                        break;
                    case R.id.editTextEthnicity:
                        profile.setEthnicity(editTextEthnicity.getText().toString().trim());
                        break;
                    case R.id.editTextOccupation:
                        profile.setOccupation(editTextOccupation.getText().toString().trim());
                        break;

                    case R.id.editTextEducation:
                        profile.setEducation(editTextEducation.getText().toString().trim());
                        break;

                    case R.id.editTextBirthday:
                        profile.setDob(editTextBirthday.getText().toString().trim());
                        break;

                    case R.id.editTextGender:
                        if (editTextGender.getText().toString().trim().equalsIgnoreCase(getString(R.string.gender_male_value))) {
                            profile.setGender(getString(R.string.gender_male));
                            btnMale.setSelected(true);
                            btnFemale.setSelected(false);
                            btnLgbt.setSelected(false);
                        } else if (editTextGender.getText().toString().trim().equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
                            profile.setGender(getString(R.string.gender_lgbt));
                            btnMale.setSelected(false);
                            btnFemale.setSelected(false);
                            btnLgbt.setSelected(true);
                        } else {
                            profile.setGender(getString(R.string.gender_female));
                            btnMale.setSelected(false);
                            btnFemale.setSelected(true);
                            btnLgbt.setSelected(false);
                        }
                        break;


                    default:
                        break;
                }

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {

//        fragmentDummyProvider = new ExampleDataProviderFragment();
        /*fragmentPhotos = new PhotosFragment();*/
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ProfileFragment(), "DETAILS");
        adapter.addFragment(new PhotosFragment(), "PHOTOS");
        adapter.addFragment(new PreferencesFragment(), "PREFERENCES");
        viewPager.setAdapter(adapter);

        /*int tabIndex = 0;
        LinearLayout layout1 = ((LinearLayout)((LinearLayout) tabLayout.getChildAt(0)).getChildAt(tabIndex));
        layout1.setPadding(0, 0, 0, 0);

        LinearLayout layout2 = ((LinearLayout)((LinearLayout) tabLayout.getChildAt(1)).getChildAt(tabIndex));
        layout2.setPadding(0, 0, 0, 0);

        LinearLayout layout3 = ((LinearLayout)((LinearLayout) tabLayout.getChildAt(2)).getChildAt(tabIndex));
        layout3.setPadding(0, 0, 0, 0);*/

        /*for(int i=0; i < tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 0, 0);
            tab.requestLayout();
        }*/


//        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        hideKeyboard(this);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SELECT_PICTURE) {
                isFromPlus = false;
                FacebookPhotoVO photoFb = null;
                boolean isCamera = false;

                boolean isFacebook = false;
                boolean isDelete = false;
//                if (data == null) {
                if (data != null && data.hasExtra(Constants.INTENT_PARAM_FB_ALBUM)) {
                    isCamera = false;
                    isFacebook = true;

                    photoFb = (FacebookPhotoVO) data.getSerializableExtra(Constants.INTENT_PARAM_FB_ALBUM);
                } else if (data != null && data.hasExtra(Constants.INTENT_REMOVE_IMAGE)) {
                    isDelete = true;
                } else {
                    isFacebook = false;
                    if (data == null || data.getData() == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }
                }

                Uri selectedImageUri;
                if (isFacebook) {
                    // Update selected image from facebook to local profile object.
                    // Get image url here
                    String imageUrlFb = "";
                    for (int i = 0; i < photoFb.getImages().size(); i++) {
                        if (imageUrlFb.trim().isEmpty()) {
                            imageUrlFb = photoFb.getImages().get(i).getSource().trim();
                            break;
                        } else {
                            break;
                        }
                    }

                    if (imageUrlFb.trim().isEmpty()) {
                        imageUrlFb = photoFb.getPicture().trim();
                    }

                    Log.d("IMAGE_URL_FROM_FACEBOOK", "URL: " + imageUrlFb + ", Flag: " + Constants.flagImageUpload);

                    if (Constants.flagImageUpload == 1) {
                        // Load bitmap from facebook and save in local
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 2) {
                        // Image 2
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 3) {
                        // Image 3
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 4) {
                        // Image 4
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 5) {
                        // Image 5
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 6) {
                        // Image 6
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 7) {
                        // Image 7
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 8) {
                        // Image 8
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 9) {
                        // Image 9
                        setImageFromUrl(imgForUploadSelected, imageUrlFb.trim(), imgAddButtonForUploadSelected);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, imageUrlFb.trim());
                        SharedPreferenceUtil.save();
                    }

                    if (isValidFields()) {
//                        ApiEditProfile();
                        /*showProgress(getString(R.string.loading));*/
                        ApiEditProfileAddRemoveProfilePicture();
                    }
                } else if (isCamera) {
                    /*selectedImageUri = Constants.outputFileUri;
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Camera result.");
                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
                    decodeFile(picturePath, 1);

                    if (bitmapCameraUpload != null && imgForUploadSelected != null) {
                        imgForUploadSelected.setImageBitmap(bitmapCameraUpload);
                        imgAddButtonForUploadSelected.setVisibility(View.GONE);
                    } else {
                        Log.d("ON_ACTIVITY_RESULT", (bitmapCameraUpload == null ? "Selected image Bitmap found null." : "Selected image view found null to set bitmap."));
                    }

//                    showProgress(getString(R.string.loading));
                    ImageUploadApi(picturePath);*/
                    // Crop Image here.
//                    final Uri selectedUri = data.getData();

                    // Camera intent here.
                    /*File f = new File(Environment.getExternalStorageDirectory()
                            .toString());
                    for (File temp : f.listFiles()) {
                        if (temp.getName().equals("temp.jpg")) {
                            f = temp;
                            break;
                        }
                    }*/

                    try {
                        /*Bitmap bm;
                        BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

                        bm = BitmapFactory.decodeFile(f.getAbsolutePath(), btmapOptions);
                        bmProfilePic = bm;*/
                        // bm = Bitmap.createScaledBitmap(bm, 70, 70, true);
                        /*profile_image.setImageBitmap(photo);*/
                        /*Bitmap photo = (Bitmap) data.getExtras().get("data");
                        Uri selectedUri = getImageUri(context, photo);*/

                        /*File path = new File(getFilesDir(), mCurrentPhotoPath);
                        if (!path.exists()) path.mkdirs();
                        File imageFile = new File(path, "image.jpg");*/
                        File fileaCamera = new File(mCurrentPhotoPath);
                        if (fileaCamera.exists()) {
                            Log.d(TAG, "Camera image captured. File found.");

                            Uri selectedUri = Uri.fromFile(fileaCamera);
                            if (selectedUri != null) {
                                String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
                                destinationFileName += ".png";

                                UCrop.Options options = new UCrop.Options();
                                // Color palette
                                options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                                options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                                options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
                                options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.white_selector));

                                DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
                                int screenWidth = metrics.widthPixels;
                                UCrop.of(selectedUri, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                        .withAspectRatio(1, 1)
                                        .withOptions(options)
                                        .withMaxResultSize(screenWidth, screenWidth)
                                        .start(EditProfileNewActivity.this);
                            } else {
                                Toast.makeText(context, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d(TAG, "Camera image captured. File not found.");
                        }

//                        final Uri selectedUri = Constants.outputFileUri;


                        /*callUploadPhotoAPI(getRealPathFromURI(imageUri));

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, imageUri.toString());
                        SharedPreferenceUtil.save();*/

                       /* String path = android.os.Environment.getExternalStorageDirectory() + File.separator + "Phoenix" + File.separator + "default";
                        f.delete();
                        OutputStream fOut = null;
                        File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                        try {
                            fOut = new FileOutputStream(file);
                            bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (isDelete) {
                    imgForUploadSelected.setImageDrawable(null);
                    imgAddButtonForUploadSelected.setVisibility(View.VISIBLE);
                    if (imgRemoveButtonForUploadSelected != null) {
                        imgRemoveButtonForUploadSelected.setVisibility(View.GONE);
                    }
                    if (Constants.flagImageUpload == 1) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 2) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2);
                        // Image 2
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 3) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3);
                        // Image 3
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 4) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4);
                        // Image 4
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 5) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5);
                        // Image 5
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 6) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6);
                        // Image 6
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 7) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7);
                        // Image 7
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 8) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8);
                        // Image 8
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, "");
                        SharedPreferenceUtil.save();
                    } else if (Constants.flagImageUpload == 9) {
                        // Delete file from local storage here.
                        deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9);
                        // Image 9
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, "");
                        SharedPreferenceUtil.save();
                    }

                    if (isValidFields()) {
                        /*ApiEditProfile();*/
                        /*showProgress(getString(R.string.loading));*/
                        ApiEditProfileAddRemoveProfilePicture();
                    }
                } else {
                    // Crop Image here.
                    final Uri selectedUri = data.getData();
                    if (selectedUri != null) {
                        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
                        destinationFileName += ".png";

                        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
                        int screenWidth = metrics.widthPixels;

                        UCrop.Options options = new UCrop.Options();
                        // Color palette
                        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));
                        options.setToolbarWidgetColor(ContextCompat.getColor(this, R.color.white_selector));

                        UCrop.of(selectedUri, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withOptions(options)
                                .withMaxResultSize(screenWidth, screenWidth)
                                .start(EditProfileNewActivity.this);
                    } else {
                        Toast.makeText(context, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                    }
                    /*selectedImageUri = data == null ? null : data.getData();
                    Log.d("EDIT_PROFILE_ACTIVITY", "onActivityResult ==> Gallery result.");
                    try {
                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        String picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));

                        decodeFile(picturePath, 1);

                        if (bitmapCameraUpload != null && imgForUploadSelected != null) {
                            imgForUploadSelected.setImageBitmap(bitmapCameraUpload);
                            imgAddButtonForUploadSelected.setVisibility(View.GONE);
                        } else {
                            Log.d("ON_ACTIVITY_RESULT", (bitmapCameraUpload == null ? "Selected image Bitmap found null." : "Selected image view found null to set " +
                                    "bitmap."));
                        }

                        ImageUploadApi(picturePath);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
                }
            } else if (requestCode == 102) {

            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        } else if (resultCode == RESULT_CANCELED) {
            isFromPlus = false;
            if (requestCode == UCrop.REQUEST_CROP) {
                if (data != null)
                    handleCropError(data);
            } else {
                // user cancelled Image capture
                isFromPlus = false;
                /*showSnackBarMessageOnly(getString(R.string.image_upload_cancelled));*/
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position) {
            case 0:
                txtViewCounter.setText("1/" + String.valueOf(totalPictures));
                break;

            case 1:
                txtViewCounter.setText("2/" + String.valueOf(totalPictures));
                break;
            case 2:
                txtViewCounter.setText("3/" + String.valueOf(totalPictures));
                break;

            case 3:
                txtViewCounter.setText("4/" + String.valueOf(totalPictures));
                break;

            case 4:
                txtViewCounter.setText("5/" + String.valueOf(totalPictures));
                break;

            default:
                break;

        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemSave = menu.findItem(R.id.menu_item_toolbar_menu_save);
        itemSave.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_toolbar_menu_save) {

            isFromSave = true;
            isRevertUnsavedChanges = false;
            isUnsavedChanges = false;
            if (isValidFields()) {
                ApiEditProfile();
            } else {
                /*isFromSave = true;
                isRevertUnsavedChanges = false;
                isUnsavedChanges = false;
                intent = new Intent(mContext, SingleUserDetailActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();*/
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO Get tastebuds list of user
        hideKeyboard(this);

        if (isTastebudsEdit) {
           /* if(isRevertUnsavedChanges) {
                isRevertUnsavedChanges = false;
                isUnsavedChanges = false;
            }else{
                isUnsavedChanges = true;
            }*/

            isRevertUnsavedChanges = true;
            isUnsavedChanges = false;
            isTastebudsEdit = false;
            /*sendRequestGetTastebudsForUser();*/
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
//            case R.id.textViewEditTasteBuds:
            case R.id.buttonProfileFragmentSelectTastebuds:
                isTastebudsEdit = true;
                isUnsavedChanges = true;
                isRevertUnsavedChanges = false;
                intent = new Intent(this, TasteBudsSuggestionsActivity.class);
                intent.putExtra("isFromEditProfile", true);
                startActivity(intent);
                break;

            /*case R.id.imageViewEditProfileActivityAddUserImage1:
                Constants.flagImageUpload = 1;
                imgForUploadSelected = imageViewEditProfileActivityUserImage1;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage1;
                isFromPlus = true;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityAddUserImage2:
                imgForUploadSelected = imageViewEditProfileActivityUserImage2;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage2;
                Constants.flagImageUpload = 2;
                isFromPlus = true;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityAddUserImage3:
                imgForUploadSelected = imageViewEditProfileActivityUserImage3;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage3;
                Constants.flagImageUpload = 3;
                isFromPlus = true;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityAddUserImage4:
                imgForUploadSelected = imageViewEditProfileActivityUserImage4;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage4;
                Constants.flagImageUpload = 4;
                isFromPlus = true;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityAddUserImage5:
                Log.d("CAMERA_INTENT_TEST", "Testing intent applied...");
                imgForUploadSelected = imageViewEditProfileActivityUserImage5;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage5;
                Constants.flagImageUpload = 5;
                isFromPlus = true;
                openUploadImageFromDeviceIntent();
                break;*/

            case R.id.btnSaveEditProfile:
                if (isValidFields())
                    ApiEditProfile();
                break;

           /* case R.id.imageViewEditProfileActivityUserImage1:
                isFromPlus = false;
                imgForUploadSelected = imageViewEditProfileActivityUserImage1;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage1;
                Constants.flagImageUpload = 1;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityUserImage2:
                isFromPlus = false;
                imgForUploadSelected = imageViewEditProfileActivityUserImage2;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage2;
                Constants.flagImageUpload = 2;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityUserImage3:
                isFromPlus = false;
                imgForUploadSelected = imageViewEditProfileActivityUserImage3;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage3;
                Constants.flagImageUpload = 3;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityUserImage4:
                isFromPlus = false;
                imgForUploadSelected = imageViewEditProfileActivityUserImage4;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage4;
                Constants.flagImageUpload = 4;
                openUploadImageFromDeviceIntent();
                break;

            case R.id.imageViewEditProfileActivityUserImage5:
                isFromPlus = false;
                imgForUploadSelected = imageViewEditProfileActivityUserImage5;
                imgAddButtonForUploadSelected = imageViewEditProfileActivityAddUserImage5;
                Constants.flagImageUpload = 5;
                openUploadImageFromDeviceIntent();
                break;*/

            case R.id.editTextBirthday:
                Calendar c = Calendar.getInstance();
                myCalendar = stringToCalender(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, ""));
                dialog = new DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                dialog.show();
                break;

            case R.id.editTextGender:
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }

                if (isToExpandGender) {
                    // TODO Close datepicker view
                    isToExpandGender = false;
                    editTextGender.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_arrow_black_animate, 0);
                    Drawable[] myTextViewCompoundDrawables = editTextGender.getCompoundDrawables();
                    int MAX_LEVEL = 10000;
                    for (Drawable drawable : myTextViewCompoundDrawables) {

                        if (drawable == null)
                            continue;
                        else {
                            ObjectAnimator anim = ObjectAnimator.ofInt(drawable, "level", 0, MAX_LEVEL);
                            anim.start();
                        }
                    }

                    collapseView(linearLayoutSelectGender);
                } else {
                    // TODO Open datepicker view
                    isToExpandGender = true;
                    editTextGender.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_arrow_black_animate, 0);
                    Drawable[] myTextViewCompoundDrawables = editTextGender.getCompoundDrawables();
                    int MAX_LEVEL = 10000;
                    for (Drawable drawable : myTextViewCompoundDrawables) {

                        if (drawable == null)
                            continue;
                        else {
                            ObjectAnimator anim = ObjectAnimator.ofInt(drawable, "level", 0, MAX_LEVEL);
                            anim.start();
                        }

                    }
                    expandView(linearLayoutSelectGender);
                }
                break;

            case R.id.btnMale:
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }

                if (btnMale.isSelected()) {

                } else {
                    btnFemale.setSelected(false);
                    btnLgbt.setSelected(false);
                    btnMale.setSelected(true);

                    profile.setGender(getString(R.string.gender_male_value));
                    editTextGender.setText(getString(R.string.gender_male));
                    editTextGender.setSelection(editTextGender.getText().toString().trim().length());
                }
                break;

            case R.id.btnFemale:
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }

                if (btnFemale.isSelected()) {

                } else {
                    btnFemale.setSelected(true);
                    btnLgbt.setSelected(false);
                    btnMale.setSelected(false);

                    profile.setGender(getString(R.string.gender_female_value));
                    editTextGender.setText(getString(R.string.gender_female));
                    editTextGender.setSelection(editTextGender.getText().toString().trim().length());
                }
                break;

            case R.id.btnLgbt:
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }

                if (btnLgbt.isSelected()) {

                } else {
                    btnFemale.setSelected(false);
                    btnLgbt.setSelected(true);
                    btnMale.setSelected(false);

                    profile.setGender(getString(R.string.gender_lgbt_value));
                    editTextGender.setText(getString(R.string.gender_lgbt));
                    editTextGender.setSelection(editTextGender.getText().toString().trim().length());
                }
                break;

            case R.id.btnMile:
                isUnsavedChanges = true;
                isRevertUnsavedChanges = false;
                if (btnKilometer.isSelected()) {
                    btnKilometer.setSelected(false);
                    btnMile.setSelected(true);
                    textViewShowDistanceIn.setText(getResources().getString(R.string.prefer_distance_mi));

//                    int distanceCurrentMiles = Integer.parseInt(profile.getSearch_distance());
                    int distanceCurrentMiles = seekBarActivityDiscoveryPreferencesSearchDistance.getProgress();
//                    int milesToKms = (int) (distanceCurrentMiles * KM_TO_MILE);
                    int milesToKms = (int) Math.round(distanceCurrentMiles * KM_TO_MILE);
                    if (milesToKms > 100) {
                        milesToKms = 100;
                    }
                    Log.d(TAG, "Current distance in miles: " + distanceCurrentMiles + ", Converted to KMs: " + milesToKms);

                    // Set max limit of range bar to 100 miles @MAX_DISTANCE_RANGE_MI
                    seekBarActivityDiscoveryPreferencesSearchDistance.setMax(MAX_DISTANCE_RANGE_MI);

                    profile.setDistance_unit(getResources().getString(R.string.distance_unit_mi_value_server));
                    profile.setSearch_distance(String.valueOf(milesToKms));
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERED_UNIT, "M");
//                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, String.valueOf(milesToKms));
//                    SharedPreferenceUtil.save();

                    // Update view for distance.
                    seekBarActivityDiscoveryPreferencesSearchDistance.setProgress(
                            Integer.parseInt(String.valueOf(milesToKms)));
                    if (profile.getDistance_unit().trim().toLowerCase()
                            .contains(getResources().getString(R.string.distance_unit_km_value_server))) {
                        textViewDistance.setText(String.valueOf(milesToKms) + " " + getResources().getString(R.string.prefer_distance_km));
                    } else {
//                        Log.i(TAG, "Distance: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, "100"));
                        textViewDistance.setText(String.valueOf(milesToKms) + " " + getResources().getString(R.string.prefer_distance_mi));
                    }

                }
                break;

            case R.id.btnKilometer:
                isUnsavedChanges = true;
                isRevertUnsavedChanges = false;
                if (btnMile.isSelected()) {
                    isfromButton = true;
                    btnMile.setSelected(false);
                    btnKilometer.setSelected(true);
                    textViewShowDistanceIn.setText(getResources().getString(R.string.prefer_distance_km));

//                    int distanceCurrentKms = Integer.parseInt(profile.getSearch_distance());
                    int distanceCurrentKms = seekBarActivityDiscoveryPreferencesSearchDistance.getProgress();
//                    int kmToMiles = (int) (distanceCurrentKms * MILE_TO_KM);
                    int kmToMiles = (int) Math.round(distanceCurrentKms * MILE_TO_KM);
                    if (kmToMiles > 160) {
                        kmToMiles = 160;
                    }
                    Log.d(TAG, "Current distance in kms: " + distanceCurrentKms + ", Converted to miles: " + kmToMiles);

                    // Set max limit of range bar to 161 Kms @MAX_DISTANCE_RANGE_KM
                    seekBarActivityDiscoveryPreferencesSearchDistance.setMax(MAX_DISTANCE_RANGE_KM);

                    profile.setDistance_unit(getResources().getString(R.string.distance_unit_km_value_server));
                    profile.setSearch_distance(String.valueOf(kmToMiles));

                    // Update view for distance.
                    seekBarActivityDiscoveryPreferencesSearchDistance.setProgress(
                            Integer.parseInt(String.valueOf(kmToMiles)));

                    if (profile.getDistance_unit().trim().toLowerCase()
                            .contains(getResources().getString(R.string.distance_unit_km_value_server))) {
                        textViewDistance.setText(String.valueOf(kmToMiles) + " " + getResources().getString(R.string.prefer_distance_km));
                    } else {
//                        Log.i(TAG, "Distance: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PREFERENCES_SEARCH_DISTANCE, "100"));
                        textViewDistance.setText(String.valueOf(kmToMiles) + " " + getResources().getString(R.string.prefer_distance_mi));
                    }
                }
                break;

            default:
                break;

        }
    }

    private boolean isValidFields() {
        if (editTextName.getText().toString().trim().length() == 0) {
            showSnackBarMessageOnly(getString(R.string.first_name_is_required));
            return false;

        } else if (editTextLastName.getText().toString().trim().length() == 0) {
            showSnackBarMessageOnly(getString(R.string.last_name_is_required));
            return false;
        } else
            return true;
    }

    @Override
    public void onBackPressed() {
        if (isUnsavedChanges) {
            isRevertUnsavedChanges = false;
            showAlertDialogForUnsavedChanges(getString(R.string.save_changes), getString(R.string.there_are_some_unsaved_changes_found));
        } else {
            if (getIntent().hasExtra(Constants.INTENT_USER_LOGGED_IN) && getIntent().getBooleanExtra(Constants.INTENT_USER_LOGGED_IN, false)) {
                /*intent = new Intent(mContext, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();*/
                /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();*/
                // TODO Forward user to Home screen once tastebuds are saved.
                intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            } else {
                super.onBackPressed();
            }
        }
    }

    private void sendRequestGetTastebudsForUser() {
//        isRevertUnsavedChanges = true;
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_mytestbuds));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_GET_USER_TASTEBUDS, EditProfileNewActivity.this);
    }

    private void ApiViewProfile() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_profile_api).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, EditProfileNewActivity.this);
    }

    /*public String getGenderPrefrence() {
        if (profile.getShowme().equalsIgnoreCase(getString(R.string.gender_all_value))) {
            return "both";
        } else if (switchMale.isActivated() && !switchFeMale.isActivated()) {
            return "male";
        } else if (switchFeMale.isActivated() && !switchMale.isActivated()) {
            return "female";

        } else
            return "both";
    }*/

    private void ApiEditProfile() {
        String fields = fields();
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
        params.put(getString(R.string.api_param_key_first_name), profile.getFirst_name().toString().trim());
        params.put(getString(R.string.api_param_key_last_name), profile.getLast_name().toString().trim());
        params.put(getString(R.string.api_param_key_about), profile.getAbout_me().toString().trim());
        params.put(getString(R.string.api_param_key_occupation), profile.getOccupation().toString().trim());
        params.put(getString(R.string.api_param_key_education), profile.getEducation().toString().trim());
        params.put(getString(R.string.api_param_key_ethnicity), profile.getEthnicity().toString().trim());
        params.put(getString(R.string.api_param_key_relationship), profile.getRelationship().toString().trim());
        params.put(getString(R.string.api_param_key_gender), profile.getGender().toString().trim());
        params.put(getString(R.string.api_response_param_key_showme), profile.getShowme());
        params.put(getString(R.string.api_response_param_key_search_distance), profile.getSearch_distance());
        params.put(getString(R.string.api_response_param_key_search_max_age), profile.getSearch_max_age());
        params.put(getString(R.string.api_response_param_key_search_min_age), profile.getSearch_min_age());
        params.put(getString(R.string.api_response_param_key_distance_unit), profile.getDistance_unit());
        /*params.put(getString(R.string.api_response_param_key_is_receive_messages_notifications), SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true) ? "1" : "0");
        params.put(getString(R.string.api_response_param_key_is_receive_invitation_notifications), SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true) ? "1" : "0");*/
        params.put(getString(R.string.api_response_param_key_is_receive_messages_notifications), profile.is_receive_messages_notifications() ? "1" : "0");
        params.put(getString(R.string.api_response_param_key_is_receive_invitation_notifications), profile.is_receive_invitation_notifications() ? "1" : "0");
        params.put(getString(R.string.api_response_param_key_is_show_location), profile.is_show_location() ? "1" : "0");
        params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
        params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
        params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
        params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
        params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
        params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, ""));
        params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, ""));
        params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, ""));
        params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, ""));


        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_first_name);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_first_name);

        }
        if (!profile.getFirst_name().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_first_name), profile.getFirst_name().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_first_name), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_last_name);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_last_name);

        }
        if (!profile.getLast_name().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_last_name), profile.getLast_name().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_last_name), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_dob);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_dob);

        }
        if (!profile.getDob().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_dob), profile.getDob().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_dob), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_about);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_about);

        }
        if (!profile.getAbout_me().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_about), profile.getAbout_me().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_about), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_occupation);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_occupation);
        }

        if (!profile.getOccupation().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_occupation), profile.getOccupation().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_occupation), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_education);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_education);
        }

        if (!profile.getEducation().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_education), profile.getEducation().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_education), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_gender);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_gender);
        }
        if (!profile.getGender().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_gender), profile.getGender().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_gender), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_relationship);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_relationship);
        }
        if (!profile.getRelationship().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_relationship), profile.getRelationship().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_relationship), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_param_key_ethnicity);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_ethnicity);
        }
        if (!profile.getEthnicity().toString().trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_ethnicity), profile.getEthnicity().toString().trim());
        } else {
            params.put(getString(R.string.api_param_key_ethnicity), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic1);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic1);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic1), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic2);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic2);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic2), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic3);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic3);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic3), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic4);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic4);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic4), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic5);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic5);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic5), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic6);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic6);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic6), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic7);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic7);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic7), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic8);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic8);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic8), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic9);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic9);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic9), "null");
        }

        /*// TODO Get list of tastebuds id and save it to server in comma separated text
        if (listUserTastebuds.size() > 0) {
            String strTastebuds = "";
            for (int i = 0; i < listUserTastebuds.size(); i++) {
                if (strTastebuds.trim().isEmpty()) {
                    strTastebuds = listUserTastebuds.get(i).getId();
                } else {
                    strTastebuds = strTastebuds + "," + listUserTastebuds.get(i).getId();
                }
            }

            params.put(getString(R.string.api_param_key_testbuds), strTastebuds);
            fields = fields + "," + getString(R.string.api_param_key_testbuds);
        } else {
            params.put(getString(R.string.api_param_key_testbuds), "null");
            fields = fields + "," + getString(R.string.api_param_key_testbuds);
        }*/

        params.put(getString(R.string.api_param_key_fields), fields);
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_PROFILE, EditProfileNewActivity.this);
    }

    public void ApiEditProfileAddRemoveProfilePicture() {
        String fields = "";
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_action), getString(R.string.api_action_editprofile).trim());
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, "").trim());
        params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
        params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
        params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
        params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
        params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
        params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, ""));
        params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, ""));
        params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, ""));
        params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, ""));

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic1);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic1);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic1), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic2);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic2);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic2), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic3);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic3);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic3), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic4);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic4);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic4), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic5);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic5);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic5), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic6);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic6);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic6), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic7);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic7);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic7), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic8);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic8);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic8), "null");
        }

        if (fields.trim().isEmpty()) {
            fields = fields + getString(R.string.api_response_param_key_profilepic9);
        } else {
            fields = fields + "," + getString(R.string.api_response_param_key_profilepic9);
        }
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            params.put(getString(R.string.api_response_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim());
        } else {
            params.put(getString(R.string.api_response_param_key_profilepic9), "null");
        }

        params.put(getString(R.string.api_param_key_fields), fields);
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_EDIT_PROFILE_ADD_REMOVE_PICTURE, EditProfileNewActivity.this);
    }

    public String fields() {
        return getString(R.string.api_response_param_key_showme) + "," +
                getString(R.string.api_response_param_key_search_distance) + "," +
                getString(R.string.api_response_param_key_search_max_age) + "," +
                getString(R.string.api_response_param_key_search_min_age) + "," +
                getString(R.string.api_response_param_key_is_receive_invitation_notifications) + "," +
                getString(R.string.api_response_param_key_is_receive_messages_notifications) + "," +
                getString(R.string.api_response_param_key_is_show_location) + "," +
                getString(R.string.api_response_param_key_distance_unit);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if ((action == Constants.ACTION_CODE_PROFILE || action == Constants.ACTION_CODE_EDIT_PROFILE) && response != null) {
            Log.d("ResponseProfile", "" + response);
//            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        dataInner = data.getJSONObject(i);
                    }
                    if (dataInner != null) {
                        profile = new UserProfileAPI(context, dataInner);

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profile.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                profile.is_receive_messages_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                profile.is_receive_invitation_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                profile.is_show_location);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profile.profilepic6.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profile.profilepic7.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profile.profilepic8.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profile.profilepic9.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                        SharedPreferenceUtil.save();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();

                        // Load profile pictures bitmap here in local storage from URL, only if not stored in local storage before.
                        saveLoadProfilePic1();
                    }
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage);
                    if (isFromSave) {
                        /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        supportFinishAfterTransition();*/
                        // TODO Forward user to Home screen once tastebuds are saved.
                        intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        supportFinishAfterTransition();
                    }
                }
            } catch (JSONException e) {
                stopProgress();
                e.printStackTrace();
                if (isFromSave) {
                    /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                    intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                    startActivity(intent);
                    overridePendingTransition(0, 0);*/
                    supportFinishAfterTransition();
                }
            } catch (Exception e) {
                stopProgress();
                e.printStackTrace();
                supportFinishAfterTransition();
            }

            if (action == Constants.ACTION_CODE_EDIT_PROFILE) {
                if (isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                } else {
                    isUnsavedChanges = true;
                }
            } else {
                isRevertUnsavedChanges = false;
                isUnsavedChanges = false;
            }
        } else if (action == Constants.ACTION_CODE_API_GET_USER_TASTEBUDS && response != null) {
            Log.d("RESPONSE_TASTEBUDS_USER", "Response: " + response);

            try {
                // Parse response here
                JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                if (status) {
                    /*listUserTastebuds = new ArrayList<>();
                    JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject data = dataArray.getJSONObject(i);
                        TastebudChipVO chip = new TastebudChipVO();
                        chip.setId(!data.isNull("id") ? data.getString("id").trim() : "0");
                        chip.setName(!data.isNull("name") ? data.getString("name").trim() : "");
                        chip.setCity(!data.isNull("city") ? data.getString("city").trim() : "");
                        chip.setZomato_id(!data.isNull("zomato_id") ? data.getString("zomato_id").trim() : "");
                        chip.setCreatedat(!data.isNull("createdat") ? data.getString("createdat").trim() : "");
                        chip.setUpdatedat(!data.isNull("updatedat") ? data.getString("updatedat").trim() : "");

                        listUserTastebuds.add(chip);
                    }*/

                    // TODO Set list of tastebuds in the screen with user tastebuds as selected tokens
                    /*initLayoutSuggestionsChips();*/
                    /*stopProgress();*/
                } else {
                    stopProgress();
                    showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
                stopProgress();
                /*if(isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                }else{
                    isUnsavedChanges = true;
                }*/
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            } catch (Exception e) {
                e.printStackTrace();
                stopProgress();
                /*if(isRevertUnsavedChanges) {
                    isRevertUnsavedChanges = false;
                    isUnsavedChanges = false;
                }else{
                    isUnsavedChanges = true;
                }*/
                showSnackBarMessageOnly(getString(R.string.some_error_occured));
            }
        } else if ((action == Constants.ACTION_CODE_EDIT_PROFILE_ADD_REMOVE_PICTURE) && response != null) {
            Log.d(TAG, "RESPONSE_PROFILE_PICTURE_EDIT: " + response);
            stopProgress();

            JSONObject jsonObjectResponse = null;
            try {
                jsonObjectResponse = new JSONObject(response);
                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                if (status) {
                    JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                    JSONObject dataInner = null;
                    for (int i = 0; i < data.length(); i++) {
                        dataInner = data.getJSONObject(i);
                    }
                    if (dataInner != null) {
                        PicassoTools.clearCache(Picasso.with(context));
                        profile = new UserProfileAPI(context, dataInner);

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profile.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, profile.is_receive_messages_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, profile.is_receive_invitation_notifications);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                        SharedPreferenceUtil.save();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();

                        /*if (isFromSave) {
                            stopProgress();
                            intent = new Intent(mContext, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            supportFinishAfterTransition();
                        } else {
                            tabLayout.setupWithViewPager(viewPager);
                            setupViewPager(viewPager);
                        }*/
                    }

//                    PicassoTools.clearCache(Picasso.with(context));
                    setValuesPhotos();


                   /* int sizeBefore = listProfilePictures.size();
                    listProfilePictures.clear();

                    // Notify item range removed.
                    if(mAdapterEditPhotos != null) {
                        mAdapterEditPhotos.notifyItemRangeRemoved(0, sizeBefore);
                    }*/

                    setProfilePicturesList();
//                     Load profile pictures bitmap here in local storage from URL, only if not stored in local storage before.
//                    saveLoadProfilePic1();
                    if (mAdapterEditPhotos != null) {
                        /*mAdapterEditPhotos.notifyItemRangeInserted(0, listProfilePictures.size());*/
//                        mAdapterEditPhotos.notifyDataSetChanged();
//                        mAdapterEditPhotos.notifyItemChanged(Constants.flagImageUpload - 1);
                        mAdapterEditPhotos.notifyItemChanged(Constants.flagImageUpload);
                    }
                } else {
                    showSnackBarMessageOnly(errormessage);
                    if (isFromSave) {
                        /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        supportFinishAfterTransition();*/
                        /*// TODO Forward user to Home screen once tastebuds are saved.
                        intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);*/
                        supportFinishAfterTransition();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                if (isFromSave) {
                   /* intent = new Intent(mContext, SingleUserDetailActivity.class);
                    intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                    startActivity(intent);
                    overridePendingTransition(0, 0);*/
                    supportFinishAfterTransition();
                }
            }

        } else if (response == null) {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    //    PROFILE METHODS
    public void showValueOfProfile() {
        setListners();
        totalPictures = 0;

        //SHOW VALUE OF PROFILE
        Log.d(TAG, "First name to set: " + editTextName.getText().toString().trim());
        editTextName.setText(profile.getFirst_name());
        editTextName.setSelection(editTextName.getText().toString().trim().length());

        editTextLastName.setText(profile.getLast_name());
        editTextLastName.setSelection(editTextLastName.getText().toString().trim().length());

//        editTextAbout.setText("");
//        editTextAbout.setHint(getString(R.string.about)+" "+profile.getFirst_name().trim());
        textInputLayoutSignupActivityEthinicity.setHint(getString(R.string.about) + " " + profile.getFirst_name().trim());
        editTextAbout.setText(profile.getAbout_me());
        editTextAbout.setSelection(editTextAbout.getText().toString().trim().length());

        editTextRelationShipStatus.setText(profile.getRelationship());
        editTextRelationShipStatus.setSelection(editTextRelationShipStatus.getText().toString().trim().length());

        editTextOccupation.setText(profile.getOccupation());
        editTextOccupation.setSelection(editTextOccupation.getText().toString().trim().length());

        editTextEducation.setText(profile.getEducation().trim());
        editTextEducation.setSelection(editTextEducation.getText().toString().trim().length());

        editTextEthnicity.setText(profile.getEthnicity());
        editTextEthnicity.setSelection(editTextEthnicity.getText().toString().trim().length());

        editTextBirthday.setText(profile.getDob());
        editTextBirthday.setSelection(editTextBirthday.getText().toString().trim().length());

        if (profile.getGender().trim().equalsIgnoreCase(getString(R.string.gender_male_value))) {
            editTextGender.setText(getString(R.string.gender_male));
            editTextGender.setSelection(editTextGender.getText().toString().trim().length());

            btnMale.setSelected(true);
            btnFemale.setSelected(false);
            btnLgbt.setSelected(false);
        } else if (profile.getGender().trim().equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
            editTextGender.setText(getString(R.string.gender_lgbt));
            editTextGender.setSelection(editTextGender.getText().toString().trim().length());

            btnMale.setSelected(false);
            btnFemale.setSelected(false);
            btnLgbt.setSelected(true);
        } else {
            editTextGender.setText(getString(R.string.gender_female));
            editTextGender.setSelection(editTextGender.getText().toString().trim().length());

            btnMale.setSelected(false);
            btnFemale.setSelected(true);
            btnLgbt.setSelected(false);
        }

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").isEmpty()) {
            totalPictures++;
            CustomSliderView customSliderView = new CustomSliderView(this);
            // initialize a SliderLayout
            customSliderView.image(appendUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "")))
                    .empty(R.mipmap.ic_launcher).setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                        }
                    });
            restoImageSlider.addSlider(customSliderView);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").isEmpty()) {
            totalPictures++;
            CustomSliderView customSliderView = new CustomSliderView(this);
            // initialize a SliderLayout
            customSliderView.image(appendUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""))).empty(R.mipmap.ic_launcher)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            restoImageSlider.addSlider(customSliderView);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").isEmpty()) {
            totalPictures++;
            CustomSliderView customSliderView = new CustomSliderView(this);
            // initialize a SliderLayout
            customSliderView
                    .image(appendUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""))).empty(R.mipmap.ic_launcher)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                        }
                    });
            restoImageSlider.addSlider(customSliderView);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").isEmpty()) {
            totalPictures++;
            CustomSliderView customSliderView = new CustomSliderView(this);
            // initialize a SliderLayout
            customSliderView
                    .image(appendUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""))).empty(R.mipmap.ic_launcher)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                        }
                    });
            restoImageSlider.addSlider(customSliderView);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").isEmpty()) {
            totalPictures++;
            CustomSliderView customSliderView = new CustomSliderView(this);
            // initialize a SliderLayout
            customSliderView
                    .image(appendUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""))).empty(R.mipmap.ic_launcher)
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                        }
                    });
            restoImageSlider.addSlider(customSliderView);
        }

        if (totalPictures > 1) {
            restoImageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
            restoImageSlider.setDuration(8000);
            restoImageSlider.addOnPageChangeListener(this);
        }*/

        if (getIntent().hasExtra(Constants.INTENT_USER_LOGGED_IN) && getIntent().getBooleanExtra(Constants.INTENT_USER_LOGGED_IN, false)) {
            imgDummyProfile.setVisibility(View.VISIBLE);
        } else {
            /*imgDummyProfile.setVisibility(View.GONE);*/
            imgDummyProfile.setVisibility(View.VISIBLE);
        }

        /*initLayoutSuggestionsChips();*/

        relativeRootProfileFragment.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = relativeRootProfileFragment.getRootView().getHeight() - relativeRootProfileFragment.getHeight();

                if (heightDiff > Constants.convertDpToPixels(200)) { // if more than 200 dp, it's probably a keyboard...
                    Log.d(TAG, "Keyboard up. Height: " + heightDiff + ", In dp: " + Constants.convertPixelsToDp(heightDiff));
                    if (btnSelectTastebuds != null && btnSelectTastebuds.getVisibility() == View.VISIBLE) {
                        btnSelectTastebuds.setVisibility(View.GONE);
                        imgDummyProfile.setVisibility(View.GONE);
//                        btnSelectTastebuds.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.d(TAG, "Keyboard down. Height: " + heightDiff + ", In dp: " + Constants.convertPixelsToDp(heightDiff));
                    if (btnSelectTastebuds != null && btnSelectTastebuds.getVisibility() != View.VISIBLE) {
                        btnSelectTastebuds.setVisibility(View.VISIBLE);
                        imgDummyProfile.setVisibility(View.VISIBLE);
//                        btnSelectTastebuds.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        isRevertUnsavedChanges = false;
        isUnsavedChanges = false;
        setValuesPhotos();
    }

    /*public void initLayoutSuggestionsChips() {
        if (text_chip_layout != null) {
            // Custom layout and background colors
            text_chip_layout.setAdapter(adapterLayout);
            text_chip_layout.setChipLayoutRes(R.layout.chip_close_right);
            text_chip_layout.setChipBackgroundColor(getResources().getColor(R.color.colorTextHint));
            text_chip_layout.setChipBackgroundColorSelected(getResources().getColor(R.color.colorTextHint));

            if (listUserTastebuds.size() > 0) {
                ArrayList<Chip> listChips = new ArrayList<>();
                for (int i = 0; i < listUserTastebuds.size(); i++) {
                    listChips.add(new TastebudChipVO(listUserTastebuds.get(i).getName().trim(), listUserTastebuds.get(i).getType()));
                }

                text_chip_layout.setChipList(listChips);
                txtViewNoTastebuds.setVisibility(View.GONE);
                text_chip_layout.setVisibility(View.VISIBLE);
        *//*text_chip_layout.setChipList(listUserTastebuds);*//*
            } else {
                txtViewNoTastebuds.setVisibility(View.VISIBLE);
                text_chip_layout.setVisibility(View.GONE);
            }
        }
    }*/

    //PREFERENCES METHOD
    public void showValueOfPreferences() {
        // SHOW VALUES OF PREFERENCES

        if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_all_value))) {
            /*switchFeMale.setChecked(true);
            switchMale.setChecked(true);
            switchLgbt.setChecked(true);*/
            btnMalePreferences.setSelected(true);
            btnFemalePreferences.setSelected(true);
            btnLgbtPreferences.setSelected(true);
            txtViewShowMe.setText(getString(R.string.gender_all));
        } else if (profile.getShowme().trim().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_male_value))) {
            /*switchFeMale.setChecked(false);
            switchLgbt.setChecked(false);
            switchMale.setChecked(true);*/
            btnMalePreferences.setSelected(true);
            btnFemalePreferences.setSelected(false);
            btnLgbtPreferences.setSelected(false);
            txtViewShowMe.setText(getString(R.string.gender_male));
        } else if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_female_value))) {
            /*switchFeMale.setChecked(true);
            switchMale.setChecked(false);
            switchLgbt.setChecked(false);*/
            btnMalePreferences.setSelected(false);
            btnFemalePreferences.setSelected(true);
            btnLgbtPreferences.setSelected(false);
            txtViewShowMe.setText(getString(R.string.gender_female));
        } else if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_lgbt_value))) {
            /*switchFeMale.setChecked(false);
            switchMale.setChecked(false);
            switchLgbt.setChecked(true);*/
            btnMalePreferences.setSelected(false);
            btnFemalePreferences.setSelected(false);
            btnLgbtPreferences.setSelected(true);
            txtViewShowMe.setText(getString(R.string.gender_lgbt));
        } else if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_male_female_value))) {
            /*switchFeMale.setChecked(true);
            switchMale.setChecked(true);
            switchLgbt.setChecked(false);*/
            btnMalePreferences.setSelected(true);
            btnFemalePreferences.setSelected(true);
            btnLgbtPreferences.setSelected(false);
            txtViewShowMe.setText(getString(R.string.gender_male_female));
        } else if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_male_lgbt_value))) {
            /*switchFeMale.setChecked(false);
            switchMale.setChecked(true);
            switchLgbt.setChecked(true);*/
            btnMalePreferences.setSelected(true);
            btnFemalePreferences.setSelected(false);
            btnLgbtPreferences.setSelected(true);
            txtViewShowMe.setText(getString(R.string.gender_male_lgbt));
        } else if (profile.getShowme().trim().toLowerCase().equalsIgnoreCase(getString(R.string.gender_female_lgbt_value))) {
            /*switchFeMale.setChecked(true);
            switchMale.setChecked(false);
            switchLgbt.setChecked(true);*/
            btnMalePreferences.setSelected(false);
            btnFemalePreferences.setSelected(true);
            btnLgbtPreferences.setSelected(true);
            txtViewShowMe.setText(getString(R.string.gender_female_lgbt));
        } else {
            /*switchFeMale.setChecked(true);
            switchMale.setChecked(true);
            switchLgbt.setChecked(true);*/
            btnMalePreferences.setSelected(true);
            btnFemalePreferences.setSelected(true);
            btnLgbtPreferences.setSelected(true);
            txtViewShowMe.setText(getString(R.string.gender_all));
        }

        if (!profile.getDistance_unit().trim().isEmpty() && profile.getDistance_unit().trim().toLowerCase()
                .contains(getString(R.string.distance_unit_mi_value_server))) {
            seekBarActivityDiscoveryPreferencesSearchDistance.setMax(MAX_DISTANCE_RANGE_MI);
        } else if (!profile.getDistance_unit().trim().isEmpty() && profile.getDistance_unit().trim().toLowerCase()
                .contains(getString(R.string.distance_unit_km_value_server))) {
            seekBarActivityDiscoveryPreferencesSearchDistance.setMax(MAX_DISTANCE_RANGE_KM);
        } else {
            seekBarActivityDiscoveryPreferencesSearchDistance.setMax(MAX_DISTANCE_RANGE_MI);
        }

        seekBarActivityDiscoveryPreferencesSearchDistance.setProgress(Integer.parseInt(profile.getSearch_distance()));

        if (!profile.getDistance_unit().trim().isEmpty() && profile.getDistance_unit().trim().toLowerCase()
                .contains(getString(R.string.distance_unit_mi_value_server))) {
            textViewDistance.setText(profile.getSearch_distance() + " " + getString(R.string.prefer_distance_mi));
        } else if (!profile.getDistance_unit().trim().isEmpty() &&
                profile.getDistance_unit().trim().toLowerCase().contains(getString(R.string.distance_unit_km_value_server))) {
            textViewDistance.setText(profile.getSearch_distance() + " " + getString(R.string.prefer_distance_km));
        } else {
            textViewDistance.setText(profile.getSearch_distance() + " " + getString(R.string.prefer_distance_mi));
        }

        if (Integer.parseInt(profile.getSearch_max_age()) > 100) {
            profile.setSearch_max_age("100");
        }

        if (Integer.parseInt(profile.getSearch_min_age()) < 15) {
            profile.setSearch_min_age("15");
        }

        // Max & Min Range for Age
        Log.d("SETTINGS_ACTIVITY", "Minimum value: " + Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, "16"))
                + ", Max value: " + Integer.parseInt(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, "100")));
        rangebarAge.setRangePinsByValue(Integer.parseInt(profile.getSearch_min_age()),
                Integer.parseInt(profile.getSearch_max_age()));
        textViewAgeRange.setText(profile.getSearch_min_age() + "-" + profile.getSearch_max_age());

        /*switchMessages.setChecked((SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)));
        switchNewMatches.setChecked(SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, true));*/

        switchMessages.setChecked(profile.is_receive_messages_notifications());
        switchNewMatches.setChecked(profile.is_receive_invitation_notifications());
        switchLocationShowHide.setChecked(profile.is_show_location());

        if (profile.getDistance_unit().trim().toLowerCase().contains(getString(R.string.distance_unit_km_value_server))) {
            btnKilometer.setSelected(true);
            btnMile.setSelected(false);
            textViewShowDistanceIn.setText(getString(R.string.prefer_distance_km));

            // Set max limit of range bar to 161 Kms @MAX_DISTANCE_RANGE_KM
//            rangebarAge.setTickEnd(MAX_DISTANCE_RANGE_KM);
        } else if (profile.getDistance_unit().trim().toLowerCase().contains(getString(R.string.distance_unit_mi_value_server))) {
            btnKilometer.setSelected(false);
            btnMile.setSelected(true);
//            textViewShowDistanceIn.setText("Mi");
            textViewShowDistanceIn.setText(getString(R.string.prefer_distance_mi));

            // Set max limit of range bar to 100 Kms @MAX_DISTANCE_RANGE_MI
//            rangebarAge.setTickEnd(MAX_DISTANCE_RANGE_MI);
        } else {
            btnKilometer.setSelected(false);
            btnMile.setSelected(true);
//            textViewShowDistanceIn.setText("Mi");
            textViewShowDistanceIn.setText(getString(R.string.prefer_distance_mi));

            // Set max limit of range bar to 100 Miles @MAX_DISTANCE_RANGE_MI
//            rangebarAge.setTickEnd(MAX_DISTANCE_RANGE_MI);
        }

        if (getIntent().hasExtra(Constants.INTENT_USER_LOGGED_IN) && getIntent().getBooleanExtra(Constants.INTENT_USER_LOGGED_IN, false)) {
            imgDummyPreferences.setVisibility(View.VISIBLE);
        } else {
            imgDummyPreferences.setVisibility(View.GONE);
        }
    }

    //PHOTO METHODS
    public void setValuesPhotos() {

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            setImageFromUrl(imageViewEditProfileActivityUserImage1,
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""), imageViewEditProfileActivityAddUserImage1);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").isEmpty()
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage2,
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""), imageViewEditProfileActivityAddUserImage2);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").isEmpty()
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage3,
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""), imageViewEditProfileActivityAddUserImage3);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").isEmpty()
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage4,
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""), imageViewEditProfileActivityAddUserImage4);
        }*/

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").isEmpty()
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").equalsIgnoreCase("")) {
            setImageFromUrl(imageViewEditProfileActivityUserImage5,
                    SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""), imageViewEditProfileActivityAddUserImage5);
        }*/

    }

    private void openUploadImageFromDeviceIntent() {
        if (isStoragePermissionGranted()) {
//            http://stackoverflow.com/questions/4455558/allow-user-to-select-camera-or-gallery-for-image
        /*// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        root.mkdirs();

        // Generate File name here
        String strDate = (new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)).format(new Date());

        final String fname = "IMG_" + strDate + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);*/

            String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
            Log.d("CAMERA_PICTURE_FILE_NAME", "" + fileName);
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            Constants.outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            // Camera.
            final List<Intent> cameraIntents = new ArrayList<>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for (ResolveInfo res : listCam) {
                String packageName = res.activityInfo.packageName;
                Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra("return-data", true);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Constants.outputFileUri);
                cameraIntents.add(intent);
            }

            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            if (accessToken != null) {
                Intent intent = new Intent(context, FacebookAlbumsListActivity.class);
                Log.i("FAcebookAccesstoken", "Found");
//        intent.setPackage("Facebook");
//        intent.setComponent(new ComponentName("FACEBOOK", "FACEBOOK"));
                cameraIntents.add(intent);
            } else {
                Log.i("FAcebookAccesstoken", "NotFound");
            }

            //REMOVE IMAGE
            if (isFromPlus) {

            } else {
                Intent intent = new Intent(context, RemoveImageActivity.class);
                cameraIntents.add(intent);
            }

            // Filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_PICK);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.upload_image_using));

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            // Facebook Image Intent
            startActivityForResult(chooserIntent, Constants.REQUEST_CODE_SELECT_PICTURE);
            overridePendingTransition(0, 0);
        } else {

            checkStoragePermissionGrantedOrNot();
        }

    }

    private void decodeFile(String filePath, int scale) {
        // Decode image size
        try {
            Log.i("DECODE_FILE", "FilePath: " + filePath);
            /*BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, o);*/

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            bitmapCameraUpload = BitmapFactory.decodeFile(filePath, o2);
            /*imageForMyPhotoUpload.setImageBitmap(bitmap);*/
        } catch (Exception e) {
            e.printStackTrace();
            decodeFile(filePath, scale * 2);
//            showToast("Error");
        }
    }

    private void setImageFromUrl(final ImageView imageView, final String imgUrl, final ImageView addButton) {
//        Constants.convertDpToPixels(102)

        if (imgUrl != null && !imgUrl.isEmpty()) {
            // Delete image file from local if exists.
            if (Constants.flagImageUpload == 1) {
                deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1);
            } else if (Constants.flagImageUpload == 2) {
                deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2);
            } else if (Constants.flagImageUpload == 3) {
                deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3);
            } else if (Constants.flagImageUpload == 4) {
                deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4);
            } else if (Constants.flagImageUpload == 5) {
                deleteFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5);
            }

            Log.d(TAG, "Image loading from facebook: " + imgUrl);
            int sizeImage = Constants.convertDpToPixels(158);
            if (Constants.flagImageUpload == 1) {
                sizeImage = widthScreen - Constants.convertDpToPixels(16);
            } else {
                sizeImage = (widthScreen - Constants.convertDpToPixels(32)) / 4;
            }


            targetFacebook = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
                    Log.d(TAG, "Profile pic bitmap frm facebook loaded.");
                    // Save image bitmap in local app data.
                    if (Constants.flagImageUpload == 1) {
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());
                    } else if (Constants.flagImageUpload == 2) {
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());
                    } else if (Constants.flagImageUpload == 3) {
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
                    } else if (Constants.flagImageUpload == 4) {
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
                    } else if (Constants.flagImageUpload == 5) {
                        String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
                    }

                    imageView.setImageBitmap(bitmap);
                    addButton.setVisibility(View.GONE);
                    if (imgRemoveButtonForUploadSelected != null) {
                        imgRemoveButtonForUploadSelected.setVisibility(View.VISIBLE);
                    }
                    Log.d(TAG, "success loading profile picture from facebook: " + imgUrl);

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    Log.d(TAG, "Profile pic bitmap from facebook failed.");
                }
            };

            Picasso.with(context)
                    .load(appendUrl(imgUrl.trim()))
                    .into(targetFacebook);

//            Picasso.with(context).load(imgUrl.trim()).resize(sizeImage, sizeImage).centerCrop() // sizeImageUpload
//                    .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
//                    .into(imageView, new Callback() {
//                        @Override
//                        public void onSuccess() {
//                            addButton.setVisibility(View.GONE);
//                            if (imgRemoveButtonForUploadSelected != null) {
//                                imgRemoveButtonForUploadSelected.setVisibility(View.VISIBLE);
//                            }
//                            Log.d(TAG, "success loading restaurant pic for Card: " + imgUrl);
//                        }
//
//                        @Override
//                        public void onError() {
//                            Log.d(TAG, "Error loading restaurant pic for Card: " + imgUrl);
//                        }
//                    });
        }

    }

    private interface FileUploader {
        @Multipart
        @POST("/post.php")
        void upload(@Part("action") TypedString action, @Part("userid") TypedString userid, @Part("sessionid") TypedString sessionid, @Part("image") TypedString image,
                    @Part("file") TypedFile photo, retrofit.Callback<Object> callback);
    }

    private void ImageUploadApi(String selectedImagePath) {

        String selectedImage = "";
        try {
            TypedFile photo1 = null, photo2 = null, photo3 = null, photo4 = null, photo5 = null, file = null;
            if (Constants.flagImageUpload == 1) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic1";
            } else if (Constants.flagImageUpload == 2) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic2";
            } else if (Constants.flagImageUpload == 3) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic3";
            } else if (Constants.flagImageUpload == 4) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic4";
            } else if (Constants.flagImageUpload == 5) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic5";
            } else if (Constants.flagImageUpload == 6) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic6";
            } else if (Constants.flagImageUpload == 7) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic7";
            } else if (Constants.flagImageUpload == 8) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic8";
            } else if (Constants.flagImageUpload == 9) {
                photo1 = new TypedFile("multipart/form-data", new File(selectedImagePath));
                selectedImage = "profilepic9";
            } else {
                stopProgress();
                return;
            }

            String strURL = getString(R.string.api_base_url_upload_images_retrofit);
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint((strURL))
                    .setLogLevel(RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("UPLOAD_PHOTO_URL"))
                    .build();

            FileUploader uploader = adapter.create(FileUploader.class);

            TypedString action = new TypedString("uploadimage");
            TypedString userid = new TypedString(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
            TypedString sessionid = new TypedString(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
            TypedString image = new TypedString(selectedImage);
            uploader.upload(action, userid, sessionid, image, photo1,
                    new retrofit.Callback<Object>() {
                        @Override
                        public void success(Object response, Response response2) {
                            try {

                                String gsonResponse = new Gson().toJson(response);
                                Log.d("GsonResponse", "" + gsonResponse.toString());

                                // Parse response here
                                JSONObject jsonObjectResponse = new JSONObject(gsonResponse);
                                JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                                boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                        ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                                String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                        ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                                if (status) {
                                    PicassoTools.clearCache(Picasso.with(getApplicationContext()));

                                    /*switch (Constants.flagImageUpload) {
                                        case 1:
                                            Picasso.with(context).invalidate(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
                                            break;

                                        case 2:
                                            Picasso.with(context).invalidate(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
                                            break;

                                        case 3:
                                            Picasso.with(context).invalidate(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
                                            break;

                                        case 4:
                                            Picasso.with(context).invalidate(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
                                            break;

                                        case 5:
                                            Picasso.with(context).invalidate(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
                                            break;
                                    }*/

                                    JSONArray array = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                            ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject data = array.getJSONObject(i);

                                        if (data != null) {
                                            profile = new UserProfileAPI(context, data);

                                           /* SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                                            // Split location and save current latitude & longitude of user.
                                            String[] location = profile.location.trim().split(",");
                                            if (location.length == 2) {
                                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim());
                                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
                                            }

                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, profile.is_receive_messages_notifications);
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS, profile.is_receive_invitation_notifications);
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());*/
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, appendUrl(profile.profilepic1.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profile.profilepic2.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profile.profilepic3.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profile.profilepic4.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profile.profilepic5.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profile.profilepic6.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profile.profilepic7.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profile.profilepic8.trim()));
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profile.profilepic9.trim()));
                                            /*SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);*/
                                            SharedPreferenceUtil.save();
//                        if(action==Constants.ACTION_CODE_EDIT_PROFILE)
////                        onBackPressed();

                                        }

                                    }
                                }

                                /*setValuesPhotos();*/

                            } catch (JSONException e) {
                                stopProgress();
                                e.printStackTrace();
                            } finally {
                                if (isValidFields()) {
                                    /*showProgress(getString(R.string.loading));*/
                                    ApiEditProfileAddRemoveProfilePicture();
                                }
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("ERROR", "");
                            stopProgress();
                            error.printStackTrace();

                        }
                    });
        } catch (Exception e) {
            stopProgress();
            e.printStackTrace();
        }
    }

    private Calendar stringToCalender(String dates) {
        String string = dates;
        String pattern = "MM/dd/yyyy";
        Date date = null;
        Calendar calendar = Calendar.getInstance();
        try {
            if (!string.trim().isEmpty()) {
                date = new SimpleDateFormat(pattern).parse(string);

                if (date != null) {
                    System.out.println(date); // Wed Mar 09 03:02:10 BOT 2011
                }

                calendar.setTime(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return calendar;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS:
//                isPermissionDialogDisplayed = false;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    showSnackBarMessageOnly(getString(R.string.you_have_successfully_granted_required_permissions));
//                    Log.d(TAG, "Permission granted pos: " + 0);
                    openUploadImageFromDeviceIntent();
                } else {
                    // Permission Denied
                    showSnackBarMessageOnly(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
                }
                break;

            case Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS:
//                isPermissionDialogDisplayed = false;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
//                    showSnackBarMessageOnly(getString(R.string.you_have_successfully_granted_required_permissions));
//                    Log.d(TAG, "Permission granted pos: " + 0);
                    dialogForUploadOption(isRemoveToDisplay);
                } else {
                    // Permission Denied
                    showSnackBarMessageOnly(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            try {
//                String[] projection = {MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(resultUri, projection, null, null, null);
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                String picturePath = cursor.getString(columnIndex);
//                cursor.close();
//                Log.d("FILE_PATH_IMAGE", "" + (picturePath != null ? picturePath : "null"));
//
//                /*decodeFile(picturePath, 1);*/
//                final BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inJustDecodeBounds = true;
//                bitmapCameraUpload = BitmapFactory.decodeFile(picturePath.trim(), options);

                bitmapCameraUpload = getThumbnail(resultUri);

                if (bitmapCameraUpload != null && imgForUploadSelected != null) {
                    imgForUploadSelected.setImageBitmap(null);
                    imgForUploadSelected.setImageResource(0);
                    imgForUploadSelected.setImageBitmap(bitmapCameraUpload);
                    imgAddButtonForUploadSelected.setVisibility(View.GONE);

                    // Save image bitmap to local storage here.
                    String imageName = "";
                    if (Constants.flagImageUpload == 1) {
                        imageName = Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1;
                    } else if (Constants.flagImageUpload == 2) {
                        imageName = Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2;
                    } else if (Constants.flagImageUpload == 3) {
                        imageName = Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3;
                    } else if (Constants.flagImageUpload == 4) {
                        imageName = Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4;
                    } else if (Constants.flagImageUpload == 5) {
                        imageName = Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5;
                    }

                    if (!imageName.trim().isEmpty()) {
                        saveImageToInternalAppStorage(bitmapCameraUpload, imageName.trim());
                    }

                    if (imgRemoveButtonForUploadSelected != null) {
                        imgRemoveButtonForUploadSelected.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.d("ON_ACTIVITY_RESULT", (bitmapCameraUpload == null ? "Selected image Bitmap found null." : "Selected image view found null to set " +
                            "bitmap."));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ImageUploadApi(resultUri.getPath().trim());
        } else {
            Toast.makeText(context, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e(TAG, "handleCropError: ", cropError);
            Toast.makeText(context, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
//        final int THUMBNAIL_SIZE = Constants.convertDpToPixels(102);
        final int THUMBNAIL_SIZE = sizeImageUpload;

        double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    public void showAlertDialogForUnsavedChanges(String strTitle, String strMessage) {
        new AlertDialog.Builder(mContext).setTitle(strTitle.trim()).setMessage(strMessage.trim())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        isFromSave = true;
                        isUnsavedChanges = false;
                        isRevertUnsavedChanges = false;
                        if (isValidFields()) {
                            ApiEditProfile();
                        } else {
//                        intent = new Intent(mContext, EditProfileNewActivity.class);
                           /* intent = new Intent(mContext, SingleUserDetailActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                            supportFinishAfterTransition();*/
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                        isUnsavedChanges = false;
                        isRevertUnsavedChanges = false;
                        onBackPressed();
                    }
                })
               /* .setIcon(android.R.drawable.ic_dialog_alert)*/
                .show();
    }

    public void showSnackBarMessageOnly(String strMessage) {
        Snackbar.make(coordinatorRoot, strMessage, Snackbar.LENGTH_LONG).show();
//                .setAction(R.string.snackbar_action_undo, clickListener)
    }

    public void dialogForUploadOption(boolean isRemoveToDisplay) {
        if (isCameraStoragePermissionGranted()) {
            // Create custom dialog object
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.layout_upload_option_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            // Include dialog.xml file


            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
            lp.width = (int) (BaseActivity.widthScreen * 0.80) + Constants.convertDpToPixels(16);
//        lp.height = (int) (BaseActivity.heightScreen * 0.65) + Constants.convertDpToPixels(24);
            window.setAttributes(lp);

            // Set dialog title
            dialog.setTitle("");

            dialog.show();

            ImageView imgPicture = (ImageView) dialog.findViewById(R.id.imageViewUploadOptionDialogPicture);
            Button buttonUploadDialogRemove = (Button) dialog.findViewById(R.id.buttonUploadDialogRemove);
            Button buttonUploadDialogCamera = (Button) dialog.findViewById(R.id.buttonUploadDialogCamera);
            Button buttonUploadDialogFacebook = (Button) dialog.findViewById(R.id.buttonUploadDialogFacebook);
            Button buttonUploadDialogGallery = (Button) dialog.findViewById(R.id.buttonUploadDialogGallery);

            if (isRemoveToDisplay) {
                buttonUploadDialogRemove.setVisibility(View.VISIBLE);
            } else {
                buttonUploadDialogRemove.setVisibility(View.GONE);
            }

            AccessToken accessToken = AccessToken.getCurrentAccessToken();
            if (accessToken != null && !accessToken.isExpired()) {
                buttonUploadDialogFacebook.setVisibility(View.VISIBLE);
            } else {
                buttonUploadDialogFacebook.setVisibility(View.GONE);
            }

            if (!listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim().isEmpty()) {
                Picasso.with(context).load(listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim())
                        .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48))
                        .centerCrop()
                        .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                        .into(imgPicture, new Callback() {
                            @Override
                            public void onSuccess() {
//                            Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).get("url").trim());
                            }

                            @Override
                            public void onError() {
                                Log.d(TAG, "Error loading restaurant pic for delete: "
                                        + listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim());
                            }
                        });
            }

            buttonUploadDialogGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();


                /*// Filesystem.
                final Intent galleryIntent = new Intent();
                galleryIntent.setType("image*//*");
                galleryIntent.setAction(Intent.ACTION_PICK);*/

                    // Chooser of filesystem options.
//                final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.upload_image_using));


                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.upload_image_using)), Constants.REQUEST_CODE_SELECT_PICTURE);
                }
            });

            buttonUploadDialogFacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    Intent intent = new Intent(context, FacebookAlbumsListActivity.class);
                    startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_PICTURE);
                }
            });

            buttonUploadDialogCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
                Log.d("CAMERA_PICTURE_FILE_NAME", "" + fileName);
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, fileName);
                Constants.outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);*/

                /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(Constants.outputFileUri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photo));
                imageUri = Uri.fromFile(photo);
                startActivityForResult(intent, TAKE_PICTURE);*/
                    dialog.dismiss();

                    /*Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_PICTURE);*/
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri photoURI = FileProvider.getUriForFile(context, "com.friendsoverfood.android.fileprovider", photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                            startActivityForResult(takePictureIntent, Constants.REQUEST_CODE_SELECT_PICTURE);
                        }
                    }
                }
            });

            buttonUploadDialogRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    openConfirmDeleteDialog();
                }
            });
        } else {
            checkCameraStoragePermissionGrantedOrNot();
        }
    }

    public void openConfirmDeleteDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(context);
        // Include dialog.xml file
        dialog.setContentView(R.layout.layout_confirm_delete_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = (int) (BaseActivity.widthScreen * 0.80) + Constants.convertDpToPixels(16);
//        lp.height = (int) (BaseActivity.heightScreen * 0.5) + Constants.convertDpToPixels(24);
        window.setAttributes(lp);
        // Set dialog title
        dialog.setTitle("");

        Button buttonUploadDialogNo = (Button) dialog.findViewById(R.id.buttonUploadDialogNo);
        ImageView picture = (ImageView) dialog.findViewById(R.id.imgViewConfirmDeletePicture);
        Button buttonUploadDialogYes = (Button) dialog.findViewById(R.id.buttonUploadDialogYes);

        // Set picture clicked here.
//        if(Constants.flagImageUpload == 1){
        if (!listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim().isEmpty()) {
            Picasso.with(context).load(listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim())
                    .resize(Constants.convertDpToPixels(48), Constants.convertDpToPixels(48))
                    .centerCrop()
                    .transform(new RoundedCornersTransformation(Constants.convertDpToPixels(8), 0))
                    .into(picture, new Callback() {
                        @Override
                        public void onSuccess() {
//                            Log.d(TAG, "success loading restaurant pic for Card: " + listProfilePictures.get(pos).get("url").trim());
                        }

                        @Override
                        public void onError() {
                            Log.d(TAG, "Error loading restaurant pic for delete: "
                                    + listProfilePictures.get(Constants.flagImageUpload - 1).getImageUrl().trim());
                        }
                    });
        }
//        }


        buttonUploadDialogYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = new Intent(context, RemoveImageActivity.class);
                startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_PICTURE);
            }
        });

        buttonUploadDialogNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void setProfilePicturesList() {
        listProfilePictures.clear();
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());
            listProfilePictures.add(data);
        } else {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());
            listProfilePictures.add(data);
        } else {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
            listProfilePictures.add(data);
        } else {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
            listProfilePictures.add(data);
        } else {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
            listProfilePictures.add(data);
        } else {
            /*HashMap<String, String> data = new HashMap<>();
            data.put("url", SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
            data.put("path", getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5).trim());
            data.put("name", Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
            listProfilePictures.add(data);*/
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim());
            listProfilePictures.add(data);
        } else {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim());
            listProfilePictures.add(data);
        } else {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim());
            listProfilePictures.add(data);
        } else {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim());
            listProfilePictures.add(data);
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim());
            listProfilePictures.add(data);
        } else {
            UserProfilePictureVO data = new UserProfilePictureVO();
            data.setImageUrl(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, ""));
            data.setImagePath(getPathFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9).trim());
            data.setImageBitmap(loadImageBitmapFromInternalAppStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim()));
            data.setImageName(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim());
            listProfilePictures.add(data);
        }
    }

    public AbstractDataProvider getDataProvider() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG_DATA_PROVIDER);
//        final Fragment fragment = fragmentDummyProvider;
        return ((ExampleDataProviderFragment) fragment).getDataProvider();
    }

    /*public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }*/

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        tempDir.mkdir();
        File tempFile = null;
        try {
            tempFile = File.createTempFile("profile", ".png", tempDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        byte[] bitmapData = bytes.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(tempFile);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.fromFile(tempFile);
    }

    public void checkCameraStoragePermissionGrantedOrNot() {
//        if (!isPermissionDialogDisplayed) {
//            isPermissionDialogDisplayed = true;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                        && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialogAlertRequestCameraStoragePermissions(getString(R.string.request_permission),
                            getString(R.string.you_need_to_grant_camera_storage_permissions));
                    return;
                }
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}
                        , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
            } else {
                // All permissions are granted. Forward user to home screen of the app.

            }
        } else {

        }
//        }
    }

    public void showDialogAlertRequestCameraStoragePermissions(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
                }
            }
        });
                /*.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Snackbar.make(view, getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly), Snackbar.LENGTH_LONG).show();
//                showSn(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
            }
        });*/
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkStoragePermissionGrantedOrNot() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        !shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showDialogAlertRequestPermissionStorage(getString(R.string.request_permission),
                            getString(R.string.storage_permission_is_required));
                    /*requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);*/
                    return;
                }

                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS);
            } else {

            }
        } else {

        }
    }

    protected void showDialogAlertRequestPermissionStorage(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA},
                            Constants.REQUEST_CODE_ASK_FOR_STORAGE_PERMISSIONS);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isCameraStoragePermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public boolean isCameraPermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public boolean isStoragePermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    public synchronized String saveImageToInternalAppStorage(Bitmap bitmapImage, String imageName) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, imageName + ".png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        return directory.getAbsolutePath();
        return mypath.getAbsolutePath();
    }

    public Bitmap loadImageBitmapFromInternalAppStorage(String imageName) {
        Bitmap bitmapReturn = null;
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, imageName + ".png");
//            File f=new File(path, imageName+".png");
            if (mypath.exists()) {
                bitmapReturn = BitmapFactory.decodeStream(new FileInputStream(mypath));
            }
            /*Bitmap b = BitmapFactory.decodeStream(new FileInputStream(mypath));
            ImageView img = (ImageView) findViewById(R.id.imgPicker);
            img.setImageBitmap(b);*/
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return bitmapReturn;
        }

    }

    public File getFileFromLocalStorage(String fileName) {
        File fileImage = null;
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, fileName + ".png");
            fileImage = mypath;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return fileImage;
        }
    }

    public String getPathFromLocalStorage(String fileName) {
        String fileImage = "";
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, fileName + ".png");
            if (mypath != null && mypath.exists()) {
                fileImage = mypath.getAbsolutePath().trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return fileImage;
        }
    }

    public void deleteFileFromLocalStorage(String fileName) {
        try {
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath = new File(directory, fileName + ".png");
            if (mypath.exists()) {
                mypath.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveLoadProfilePic1() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic2();
        } else if (!profile.getProfilepic1().isEmpty()) {
            target1 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_1.trim());

                    saveLoadProfilePic2();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic2();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic1()))
                    .into(target1);
        } else {
            saveLoadProfilePic2();
        }
    }

    public void saveLoadProfilePic2() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic3();
        } else if (!profile.getProfilepic2().isEmpty()) {
            target2 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_2.trim());

                    saveLoadProfilePic3();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic3();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic2()))
                    .into(target2);
        } else {
            saveLoadProfilePic3();
        }
    }

    public void saveLoadProfilePic3() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic4();
        } else if (!profile.getProfilepic3().isEmpty()) {
            target3 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_3.trim());

                    saveLoadProfilePic4();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic4();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic3()))
                    .into(target3);
        } else {
            saveLoadProfilePic4();
        }
    }

    public void saveLoadProfilePic4() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic5();
        } else if (!profile.getProfilepic4().isEmpty()) {
            target4 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_4.trim());

                    saveLoadProfilePic5();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic5();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic4()))
                    .into(target4);
        } else {
            saveLoadProfilePic5();
        }
    }

    public void saveLoadProfilePic5() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic6();
        } else if (!profile.getProfilepic5().isEmpty()) {
            target5 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_5.trim());

                    saveLoadProfilePic6();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic6();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic5()))
                    .into(target5);
        } else {
            saveLoadProfilePic6();
        }
    }

    public void saveLoadProfilePic6() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic7();
        } else if (!profile.getProfilepic6().isEmpty()) {
            target6 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_6.trim());

                    saveLoadProfilePic7();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic7();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic6()))
                    .into(target6);
        } else {
            saveLoadProfilePic7();
        }
    }

    public void saveLoadProfilePic7() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic8();
        } else if (!profile.getProfilepic7().isEmpty()) {
            target7 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_7.trim());

                    saveLoadProfilePic8();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic8();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic7()))
                    .into(target7);
        } else {
            saveLoadProfilePic8();
        }
    }

    public void saveLoadProfilePic8() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            saveLoadProfilePic9();
        } else if (!profile.getProfilepic8().isEmpty()) {
            target8 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_8.trim());

                    saveLoadProfilePic9();
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    saveLoadProfilePic9();
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic8()))
                    .into(target8);
        } else {
            saveLoadProfilePic9();
        }
    }

    public void saveLoadProfilePic9() {
//        Log.d(TAG, "Profile pic 2 loading here.");
        File fileProfilePic = getFileFromLocalStorage(Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9);
        if (fileProfilePic != null && fileProfilePic.exists()) {
            // make list of profile pictures here.
            stopProgress();
            setProfilePicturesList();

            if (isFromSave) {
                stopProgress();
                /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();*/
                // TODO Forward user to Home screen once tastebuds are saved.
                intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            } else {


                if (currentItemInViewPager != 0) {
                    viewPager.setCurrentItem(currentItemInViewPager);
                    currentItemInViewPager = 0;
                } else {
                    tabLayout.setupWithViewPager(viewPager);
                    setupViewPager(viewPager);
                }
            }
        } else if (!profile.getProfilepic9().isEmpty()) {
            target9 = new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                /* Save the bitmap or do something with it here */
//                    Log.d(TAG, "Profile pic 2 bitmap loaded.");
                    // Save image bitmap in local app data.
                    String filePath = saveImageToInternalAppStorage(bitmap, Constants.LOGGED_IN_USER_FILE_NAME_PROFILE_PIC_9.trim());

                    // make list of profile pictures here.
                    stopProgress();
                    setProfilePicturesList();

                    if (isFromSave) {
                        stopProgress();
                        /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        supportFinishAfterTransition();*/
                        // TODO Forward user to Home screen once tastebuds are saved.
                        intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        supportFinishAfterTransition();
                    } else {
                        tabLayout.setupWithViewPager(viewPager);
                        setupViewPager(viewPager);
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                    Log.d(TAG, "Profile pic 2 on prepare load.");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    // initialize a SliderLayout
//                    Log.d(TAG, "Profile pic 2 bitmap failed.");
                    // make list of profile pictures here.
                    stopProgress();
                    setProfilePicturesList();

                    if (isFromSave) {
                        stopProgress();
                        /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                        intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        supportFinishAfterTransition();*/
                        // TODO Forward user to Home screen once tastebuds are saved.
                        intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        supportFinishAfterTransition();
                    } else {
                        tabLayout.setupWithViewPager(viewPager);
                        setupViewPager(viewPager);
                    }
                }
            };

            Picasso.with(context)
                    .load(appendUrl(profile.getProfilepic9()))
                    .into(target9);
        } else {
            // make list of profile pictures here.
            stopProgress();
            setProfilePicturesList();

            if (isFromSave) {
                stopProgress();
                /*intent = new Intent(mContext, SingleUserDetailActivity.class);
                intent.putExtra(Constants.INTENT_USER_ID, SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
                startActivity(intent);
                overridePendingTransition(0, 0);
                supportFinishAfterTransition();*/
                // TODO Forward user to Home screen once tastebuds are saved.
                intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            } else {
                tabLayout.setupWithViewPager(viewPager);
                setupViewPager(viewPager);
            }
        }
    }

    public void expandView(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        final int targetHeight = heightForExpandAnimation == 0 ? v.getMeasuredHeight() : heightForExpandAnimation;
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapseView(final View v) {
        final int initialHeight = v.getMeasuredHeight();
//        heightForExpandAnimation = initialHeight;
//        Log.d("initialHeight", "initialHeight: "+initialHeight);

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}

