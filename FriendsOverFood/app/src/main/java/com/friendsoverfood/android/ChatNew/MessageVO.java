package com.friendsoverfood.android.ChatNew;


import com.friendsoverfood.android.RestaurantsList.RestaurantListVO;

import java.io.Serializable;

/**
 * Created by ashish on 25/11/16.
 */

public class MessageVO implements Serializable {
    String senderId = "";
    String msg = "";
    String recieverId = "";
    String reciept = "";
    String key = "";
    String delivered = "";
    String senderDp = "";
    String recieverDP = "";
    long timeStamp = 0;
    String contentType ="";
    String imgUrl="";
   private RestaurantListVO restoVo = new RestaurantListVO();

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }


    public MessageVO() {
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public RestaurantListVO getRestoVo() {
        return restoVo;
    }

    public void setRestoVo(RestaurantListVO restoVo) {
        this.restoVo = restoVo;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getSenderDp() {
        return senderDp;
    }

    public void setSenderDp(String senderDp) {
        this.senderDp = senderDp;
    }

    public String getRecieverDP() {
        return recieverDP;
    }

    public void setRecieverDP(String recieverDP) {
        this.recieverDP = recieverDP;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(String recieverId) {
        this.recieverId = recieverId;
    }

    public String getReciept() {
        return reciept;
    }

    public void setReciept(String reciept) {
        this.reciept = reciept;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}


