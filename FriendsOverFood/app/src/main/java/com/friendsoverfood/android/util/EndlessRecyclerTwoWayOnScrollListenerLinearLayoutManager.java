package com.friendsoverfood.android.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

/**
 * Created by Trushit on 04/12/15.
 * <p>
 * Endless two way scroll listener for RecyclerView with LinearLayoutManager.
 * <p>
 * This will notify when RecyclerView reaches to top and end with different methods. Also notifies when scroll position is changed to update header (sticky header).
 */
public abstract class EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager extends RecyclerView.OnScrollListener {
    private boolean isListScrolledWithTouch = true;
    private int countListScrolledItems = 0;
    private int countLastItemScrolled = 0;

    private int visibleItemCount;
    private int totalItemCount;
    private int firstVisibleItem = 0;
    private int previousFirstVisibleItem = 0;

    private LinearLayoutManager linearLayoutManager;

    public EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = linearLayoutManager.getItemCount();
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        countListScrolledItems = firstVisibleItem;

        if (firstVisibleItem != -1 && previousFirstVisibleItem != firstVisibleItem) {
            onRecyclerViewScrolled(firstVisibleItem, visibleItemCount);
            previousFirstVisibleItem = firstVisibleItem;
        }

        if (firstVisibleItem <= visibleItemCount) {
            onLoadMoreTopItems();
        }

        if (isListScrolledWithTouch) {
            int lastItem = firstVisibleItem + visibleItemCount;
            if (lastItem == totalItemCount) {
                if (countLastItemScrolled != lastItem) {
                    //to avoid multiple calls for last item
                    Log.i("LIST SCROLL AT END", "Position: " + lastItem + " TOTAL ITEMS: " + recyclerView.getAdapter().getItemCount());
                    countLastItemScrolled = lastItem;

                    // Call API for next page items
                    if (lastItem == (recyclerView.getAdapter().getItemCount())) {
//                        Log.e("MESSAGE API", "API to be called");
                        onLoadMore(lastItem);
                    }
                }
            }
        }
    }

    public abstract void onLoadMore(int countCurrentPageIndexForItem);

    public abstract void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount);

    public abstract void onLoadMoreTopItems();
}