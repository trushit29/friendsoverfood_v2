package com.friendsoverfood.android.util;

import java.util.Comparator;
import java.util.Map;

/**
 * This will be used for comparing string values in the HashMap for ArrayList
 * 
 * @author Trushit
 * 
 */
class HashMapComparator implements Comparator<Map<String, String>> {

	private final String key;

	public HashMapComparator(String key) {
		this.key = key;
	}

	public int compare(Map<String, String> first, Map<String, String> second) {
		String firstValue = first.get(key).toLowerCase();
		String secondValue = second.get(key).toLowerCase();
		return firstValue.compareTo(secondValue);
	}
}