package com.friendsoverfood.android.FullScreenImage;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Trushit on 21/06/17.
 */

public class DisplayMediaImageActivity extends BaseActivity {

    private Context context;
    private View view;
    private LayoutInflater inflater;
    private TouchImageView imgMedia;
    private String strUrlImage = "";
    private Button btnClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListener();
        setVisibilityActionBar(false);
//        Constants.setAllTypefaceOpenSansRegular(context, view);
    }

    public void init() {
        context = this;
        inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.layout_display_media_image_activity, getMiddleContent());

        imgMedia = (TouchImageView) view.findViewById(R.id.imageViewDisplayMediaImageActivityImage);
        btnClose = (Button) view.findViewById(R.id.btnClose);

        strUrlImage = getIntent().getStringExtra("image").trim();

        Picasso.with(context)
                .load(strUrlImage)
                .error(R.drawable.ic_placeholder_chat_image).placeholder(R.drawable.ic_placeholder_chat_image)
                .resize(widthScreen, 0)
                .into(imgMedia, new Callback() {

                    @Override
                    public void onSuccess() {
                        Log.i("IMAGE_MEDIA", "Image displayed successfully.");
                    }

                    @Override
                    public void onError() {
                        Log.e("IMAGE_MEDIA", "Error loading image: " + strUrlImage);
                    }
                });

        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportFinishAfterTransition();
            }
        });

//        imgMedia.setZoom(2);
    }

    public void setListener() {

    }

    @Override
    public void onResponse(String response, int action) {

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//
//    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
////        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////            Transition returnTransition = getWindow().getReturnTransition();
////            returnTransition.setDuration(1000);
////        }
////        finish();
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            onBackPressed();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }
}