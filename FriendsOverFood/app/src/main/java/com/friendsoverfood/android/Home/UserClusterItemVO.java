package com.friendsoverfood.android.Home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Trushit on 22/04/17.
 */

public class UserClusterItemVO implements ClusterItem, Parcelable {
    public UserClusterItemVO() {
    }

    public LatLng mPosition;
    public String mSnippet = "";
    public int posInList = 0;
    public boolean isSelected = false;

    private String name = "";
    private String id = "";
    private String email = "";
    private String address = "";
    private String distance = "";
    private String firstname = "";
    private String lastname = "";
    private String sex = "";
    private String birthdate = "";
    private String latitude = "";
    private String longitude = "";
    private String locationString = "";
    private String timeToReach = "";
    private String cuisines = "";
    private String testbuds = "";
    private String favrestaurants = "";

    private String image1 = "";
    private String image2 = "";
    private String image3 = "";
    private String image4 = "";
    private String image5 = "";
    private String image6 = "";
    private String image7 = "";
    private String image8 = "";
    private String religion = "";
    private String education = "";
    private String employer = "";
    private String ilike = "";
    private String iPrefer = "";
    private String about = "";

    private String cuisinesdetails = "";
    private String socialId = "";
    private String account_type = "";

    private String relationshipStatus = "";
    public ArrayList<MutualFriendsParcelableVO> MutualFriendList = new ArrayList<>();

    private String tastebudsIds = "";
    private int mutualFriends = 0;
    /**
     * Percentage Level of tastebuds common between users.
     * <br/>
     * 0% - 29% ==> Least common tastebuds, or no common tastebuds at all, Red/Dark Red color is used for indication<br/>
     * 30% - 69% ==> More than few common tastebuds, Blue color is used for indication<br/>
     * 70% - 100% ==> Most of the tastebuds are common, Green color is used for indication<br/>
     */
    private int tastebudsmatchlevel = 0;
    private Date birthDate;
    private ArrayList<String> listTasteBuds = new ArrayList<>();

    private String username = "";
    //    private String BirthDateString = "";
    private String mobile = "", dob = "", occupation = "", relationship = "", about_me = "", ethnicity = "", profilepic1 = "", profilepic2 = "", profilepic3 = "",
            profilepic4 = "", profilepic5 = "", profilepic6 = "", profilepic7 = "", profilepic8 = "", profilepic9 = "",
            createdat = "", updatedat = "", friendstatus = "";

    protected UserClusterItemVO(Parcel in) {
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
        mSnippet = in.readString();
        posInList = in.readInt();
        isSelected = in.readByte() != 0;
        name = in.readString();
        id = in.readString();
        email = in.readString();
        address = in.readString();
        distance = in.readString();
        firstname = in.readString();
        lastname = in.readString();
        sex = in.readString();
        birthdate = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        locationString = in.readString();
        timeToReach = in.readString();
        cuisines = in.readString();
        testbuds = in.readString();
        favrestaurants = in.readString();
        image1 = in.readString();
        image2 = in.readString();
        image3 = in.readString();
        image4 = in.readString();
        image5 = in.readString();
        image6 = in.readString();
        image7 = in.readString();
        image8 = in.readString();
        religion = in.readString();
        education = in.readString();
        employer = in.readString();
        ilike = in.readString();
        iPrefer = in.readString();
        about = in.readString();
        cuisinesdetails = in.readString();
        socialId = in.readString();
        account_type = in.readString();
        relationshipStatus = in.readString();
        MutualFriendList = in.createTypedArrayList(MutualFriendsParcelableVO.CREATOR);
        tastebudsIds = in.readString();
        mutualFriends = in.readInt();
        tastebudsmatchlevel = in.readInt();
        listTasteBuds = in.createStringArrayList();
        username = in.readString();
//        BirthDateString = in.readString();
        mobile = in.readString();
        dob = in.readString();
        occupation = in.readString();
        relationship = in.readString();
        about_me = in.readString();
        ethnicity = in.readString();
        profilepic1 = in.readString();
        profilepic2 = in.readString();
        profilepic3 = in.readString();
        profilepic4 = in.readString();
        profilepic5 = in.readString();
        profilepic6 = in.readString();
        profilepic7 = in.readString();
        profilepic8 = in.readString();
        profilepic9 = in.readString();
        createdat = in.readString();
        updatedat = in.readString();
        friendstatus = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPosition, flags);
        dest.writeString(mSnippet);
        dest.writeInt(posInList);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(distance);
        dest.writeString(firstname);
        dest.writeString(lastname);
        dest.writeString(sex);
        dest.writeString(birthdate);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(locationString);
        dest.writeString(timeToReach);
        dest.writeString(cuisines);
        dest.writeString(testbuds);
        dest.writeString(favrestaurants);
        dest.writeString(image1);
        dest.writeString(image2);
        dest.writeString(image3);
        dest.writeString(image4);
        dest.writeString(image5);
        dest.writeString(image6);
        dest.writeString(image7);
        dest.writeString(image8);
        dest.writeString(religion);
        dest.writeString(education);
        dest.writeString(employer);
        dest.writeString(ilike);
        dest.writeString(iPrefer);
        dest.writeString(about);
        dest.writeString(cuisinesdetails);
        dest.writeString(socialId);
        dest.writeString(account_type);
        dest.writeString(relationshipStatus);
        dest.writeTypedList(MutualFriendList);
        dest.writeString(tastebudsIds);
        dest.writeInt(mutualFriends);
        dest.writeInt(tastebudsmatchlevel);
        dest.writeStringList(listTasteBuds);
        dest.writeString(username);
//        dest.writeString(BirthDateString);
        dest.writeString(mobile);
        dest.writeString(dob);
        dest.writeString(occupation);
        dest.writeString(relationship);
        dest.writeString(about_me);
        dest.writeString(ethnicity);
        dest.writeString(profilepic1);
        dest.writeString(profilepic2);
        dest.writeString(profilepic3);
        dest.writeString(profilepic4);
        dest.writeString(profilepic5);
        dest.writeString(profilepic6);
        dest.writeString(profilepic7);
        dest.writeString(profilepic8);
        dest.writeString(profilepic9);
        dest.writeString(createdat);
        dest.writeString(updatedat);
        dest.writeString(friendstatus);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserClusterItemVO> CREATOR = new Creator<UserClusterItemVO>() {
        @Override
        public UserClusterItemVO createFromParcel(Parcel in) {
            return new UserClusterItemVO(in);
        }

        @Override
        public UserClusterItemVO[] newArray(int size) {
            return new UserClusterItemVO[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getTestbuds() {
        return testbuds;
    }

    public void setTestbuds(String testbuds) {
        this.testbuds = testbuds;
    }

    public String getFavrestaurants() {
        return favrestaurants;
    }

    public void setFavrestaurants(String favrestaurants) {
        this.favrestaurants = favrestaurants;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage6() {
        return image6;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    public String getImage7() {
        return image7;
    }

    public void setImage7(String image7) {
        this.image7 = image7;
    }

    public String getImage8() {
        return image8;
    }

    public void setImage8(String image8) {
        this.image8 = image8;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getIlike() {
        return ilike;
    }

    public void setIlike(String ilike) {
        this.ilike = ilike;
    }

    public String getiPrefer() {
        return iPrefer;
    }

    public void setiPrefer(String iPrefer) {
        this.iPrefer = iPrefer;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCuisinesdetails() {
        return cuisinesdetails;
    }

    public void setCuisinesdetails(String cuisinesdetails) {
        this.cuisinesdetails = cuisinesdetails;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getRelationshipStatus() {
        return relationshipStatus;
    }

    public void setRelationshipStatus(String relationshipStatus) {
        this.relationshipStatus = relationshipStatus;
    }

    public ArrayList<MutualFriendsParcelableVO> getMutualFriendList() {
        return MutualFriendList;
    }

    public void setMutualFriendList(ArrayList<MutualFriendsParcelableVO> mutualFriendList) {
        MutualFriendList = mutualFriendList;
    }

    public String getTastebudsIds() {
        return tastebudsIds;
    }

    public void setTastebudsIds(String tastebudsIds) {
        this.tastebudsIds = tastebudsIds;
    }

    public int getMutualFriends() {
        return mutualFriends;
    }

    public void setMutualFriends(int mutualFriends) {
        this.mutualFriends = mutualFriends;
    }

    public int getTastebudsmatchlevel() {
        return tastebudsmatchlevel;
    }

    public void setTastebudsmatchlevel(int tastebudsmatchlevel) {
        this.tastebudsmatchlevel = tastebudsmatchlevel;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public ArrayList<String> getListTasteBuds() {
        return listTasteBuds;
    }

    public void setListTasteBuds(ArrayList<String> listTasteBuds) {
        this.listTasteBuds = listTasteBuds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

   /* public String getBirthDateString() {
        return BirthDateString;
    }

    public void setBirthDateString(String birthDateString) {
        BirthDateString = birthDateString;
    }*/

    public LatLng getmPosition() {
        return mPosition;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public String getmSnippet() {
        return mSnippet;
    }

    public void setmSnippet(String mSnippet) {
        this.mSnippet = mSnippet;
    }

    public int getPosInList() {
        return posInList;
    }

    public void setPosInList(int posInList) {
        this.posInList = posInList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getLocationString() {
        return locationString;
    }

    public void setLocationString(String locationString) {
        this.locationString = locationString;
    }

    public String getTimeToReach() {
        return timeToReach;
    }

    public void setTimeToReach(String timeToReach) {
        this.timeToReach = timeToReach;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getAbout_me() {
        return about_me;
    }

    public void setAbout_me(String about_me) {
        this.about_me = about_me;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getProfilepic1() {
        return profilepic1;
    }

    public void setProfilepic1(String profilepic1) {
        this.profilepic1 = profilepic1;
    }

    public String getProfilepic2() {
        return profilepic2;
    }

    public void setProfilepic2(String profilepic2) {
        this.profilepic2 = profilepic2;
    }

    public String getProfilepic3() {
        return profilepic3;
    }

    public void setProfilepic3(String profilepic3) {
        this.profilepic3 = profilepic3;
    }

    public String getProfilepic4() {
        return profilepic4;
    }

    public void setProfilepic4(String profilepic4) {
        this.profilepic4 = profilepic4;
    }

    public String getProfilepic5() {
        return profilepic5;
    }

    public void setProfilepic5(String profilepic5) {
        this.profilepic5 = profilepic5;
    }

    public String getProfilepic6() {
        return profilepic6;
    }

    public void setProfilepic6(String profilepic6) {
        this.profilepic6 = profilepic6;
    }

    public String getProfilepic7() {
        return profilepic7;
    }

    public void setProfilepic7(String profilepic7) {
        this.profilepic7 = profilepic7;
    }

    public String getProfilepic8() {
        return profilepic8;
    }

    public void setProfilepic8(String profilepic8) {
        this.profilepic8 = profilepic8;
    }

    public String getProfilepic9() {
        return profilepic9;
    }

    public void setProfilepic9(String profilepic9) {
        this.profilepic9 = profilepic9;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getFriendstatus() {
        return friendstatus;
    }

    public void setFriendstatus(String friendstatus) {
        this.friendstatus = friendstatus;
    }
}
