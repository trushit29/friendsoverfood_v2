package com.friendsoverfood.android.EditProfileNew;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.friendsoverfood.android.DemoStaggeredGrid.AbstractDataProvider;
import com.friendsoverfood.android.R;
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;

/**
 * Created by ashish on 04/01/17.
 */

public class PhotosFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_OBJECT = "object";
    public EditProfileNewActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.

        activity = (EditProfileNewActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_edit_photos, container, false);

        /*activity.imageViewEditProfileActivityUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage1);
        activity.imageViewEditProfileActivityUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage2);
        activity.imageViewEditProfileActivityUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage3);
        activity.imageViewEditProfileActivityUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage4);
        activity.imageViewEditProfileActivityUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityUserImage5);
        activity.imageViewEditProfileActivityAddUserImage1 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage1);
        activity.imageViewEditProfileActivityAddUserImage2 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage2);
        activity.imageViewEditProfileActivityAddUserImage3 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage3);
        activity.imageViewEditProfileActivityAddUserImage4 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage4);
        activity.imageViewEditProfileActivityAddUserImage5 = (ImageView) view.findViewById(R.id.imageViewEditProfileActivityAddUserImage5);*/
        Bundle args = getArguments();
//        ((TextView) rootView.findViewById(android.R.id.text1)).setText(
//                Integer.toString(args.getInt(ARG_OBJECT)));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //noinspection ConstantConditions
        activity.mRecyclerViewEditPhotos = (RecyclerView) getView().findViewById(R.id.recyclerViewEditPhotos);
        activity.mLayoutManagerEditPhotos = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);

        // drag & drop manager
        activity.mRecyclerViewDragDropManagerEditPhotos = new RecyclerViewDragDropManager();
        activity.mRecyclerViewDragDropManagerEditPhotos.setDraggingItemShadowDrawable(
                (NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z3));
        // Start dragging after long press
        activity.mRecyclerViewDragDropManagerEditPhotos.setInitiateOnLongPress(true);
        activity.mRecyclerViewDragDropManagerEditPhotos.setInitiateOnMove(false);
        activity.mRecyclerViewDragDropManagerEditPhotos.setLongPressTimeout(250);

        //adapter
        final EditPhotosStaggeredGridAdapter myItemAdapter = new EditPhotosStaggeredGridAdapter(activity, activity.listProfilePictures, getDataProvider());
        activity.mAdapterEditPhotos = myItemAdapter;

        activity.mWrappedAdapterEditPhotos = activity.mRecyclerViewDragDropManagerEditPhotos.createWrappedAdapter(myItemAdapter);      // wrap for dragging

        final GeneralItemAnimator animator = new DraggableItemAnimator();

        activity.mRecyclerViewEditPhotos.setLayoutManager(activity.mLayoutManagerEditPhotos);
        activity.mRecyclerViewEditPhotos.setAdapter(activity.mWrappedAdapterEditPhotos);  // requires *wrapped* adapter
        activity.mRecyclerViewEditPhotos.setItemAnimator(animator);
        activity.mRecyclerViewEditPhotos.setHasFixedSize(false);

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            activity.mRecyclerViewEditPhotos.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(),
                    R.drawable.material_shadow_z1)));
        }

        activity.mRecyclerViewDragDropManagerEditPhotos.attachRecyclerView(activity.mRecyclerViewEditPhotos);

        // for debugging
//        animator.setDebug(true);
//        animator.setMoveDuration(2000);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())

        {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        activity.hideKeyboard(activity);
        /*activity.setValuesPhotos();
        activity.imageViewEditProfileActivityAddUserImage1.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage2.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage3.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage4.setOnClickListener(activity);
        activity.imageViewEditProfileActivityAddUserImage5.setOnClickListener(activity);

        activity.imageViewEditProfileActivityUserImage1.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage2.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage3.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage4.setOnClickListener(activity);
        activity.imageViewEditProfileActivityUserImage5.setOnClickListener(activity);*/
    }

    @Override
    public void onPause() {
        activity.mRecyclerViewDragDropManagerEditPhotos.cancelDrag();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (activity.mRecyclerViewDragDropManagerEditPhotos != null) {
            activity.mRecyclerViewDragDropManagerEditPhotos.release();
            activity.mRecyclerViewDragDropManagerEditPhotos = null;
        }

        if (activity.mRecyclerViewEditPhotos != null) {
            activity.mRecyclerViewEditPhotos.setItemAnimator(null);
            activity.mRecyclerViewEditPhotos.setAdapter(null);
            activity.mRecyclerViewEditPhotos = null;
        }

        if (activity.mWrappedAdapterEditPhotos != null) {
            WrapperAdapterUtils.releaseAll(activity.mWrappedAdapterEditPhotos);
            activity.mWrappedAdapterEditPhotos = null;
        }
        activity.mAdapterEditPhotos = null;
        activity.mLayoutManagerEditPhotos = null;

        super.onDestroyView();
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    public AbstractDataProvider getDataProvider() {
        return ((EditProfileNewActivity) getActivity()).getDataProvider();
    }

}
