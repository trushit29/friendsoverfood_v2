package com.friendsoverfood.android.Welcome;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.friendsoverfood.android.R;

/**
 * Created by Trushit on 11/07/17.
 */

public class AppIntroFragment2 extends Fragment {
    private Context context;
    private WelcomeActivity activity;
    private ViewGroup rootView;
    private TextView txtDescription; // textViewAppName

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.layout_app_intro_fragment_2, container, false);
        context = getActivity();
        activity = (WelcomeActivity) getActivity();

        txtDescription = (TextView) rootView.findViewById(R.id.textViewDescription);

        activity.setAllTypefaceMontserratRegular(rootView);
        activity.setAllTypefaceMontserratRegularItalic(txtDescription);
//        textViewAppName = (TextView) rootView.findViewById(R.id.textViewAppName);
//        activity. setAllTypefaceMilkShake(textViewAppName);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
