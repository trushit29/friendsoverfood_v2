package com.friendsoverfood.android.ContactUs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.R;

public class ContactUs extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    public LinearLayout llLogout, llhelp, llShareFOF;

    private TextView txtAppVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();
        setListners();
        setVisibilityActionBar(true);
        setTitleSupportActionBar("Contact us");
        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(5);
    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//
        view = inflater.inflate(R.layout.layout_contact_us_activity, getMiddleContent());

        txtAppVersion = (TextView) view.findViewById(R.id.textViewContactUsActivityAppVersion);
        llLogout = (LinearLayout) view.findViewById(R.id.llLogout);
        llShareFOF = (LinearLayout) view.findViewById(R.id.llShareFOF);
        llhelp = (LinearLayout) view.findViewById(R.id.llhelp);

        // Set app version here.
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            txtAppVersion.setText("Version " + version + " (Android)");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setListners() {
        llLogout.setOnClickListener(this);
        llhelp.setOnClickListener(this);
        llShareFOF.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId())

        {

            case R.id.llLogout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure you want to logout?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        resetValuesOnLogout();

                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();

               /* alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(COLOR_I_WANT);
                    }
                });*/
                alertDialog.show();
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));

                break;

            case R.id.llShareFOF:
                setupShareIntent();
                break;

            case R.id.llhelp:

                break;

            default:
                break;

        }

    }

    // Gets the image URI and setup the associated share intent to hook into the provider
    public void setupShareIntent() {

        /*if (uriBitmapMedia != null) {
            try {
                Log.d("REMOVE_FLYER_FROM_MEDIASTORE_ONDESTROY", "URI: " + uriBitmapMedia);
                getContentResolver().delete(uriBitmapMedia, null, null);
                uriBitmapMedia = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/

        try {
            /*// Get image bitmap loaded from ImageView
            ImageView imgFlyer = (ImageView) mPager.findViewWithTag(mPager.getCurrentItem());

            int currentImage = mPager.getCurrentItem();
            String FlyerName = listFlyers.get(currentImage).getFlyerName().trim();

            Log.d("SHARE_INTENT_FLYER_NAME ", "" + FlyerName);
            imgFlyer.setDrawingCacheEnabled(true);
            imgFlyer.buildDrawingCache(true);

            Bitmap bitmap = ((BitmapDrawable) imgFlyer.getDrawable()).getBitmap();

            // Insert flyer image into gallery and generate bitmap URI to share
            strMediaPath = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, FlyerName, FlyerName);
            uriBitmapMedia = Uri.parse(strMediaPath);
            Log.d("URI_IMAGE_PATH ", "URI IMAGE PATH : " + uriBitmapMedia);*/

            // Create share intent as described above
            intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("text/*");
            intent.putExtra("sms_body", getString(R.string.share_message).trim());
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message).trim());
            /*intent.putExtra(Intent.EXTRA_STREAM, uriBitmapMedia);*/
            startActivity(Intent.createChooser(intent, "Share via"));
            // Attach share event to the menu item provider
                /*Log.d("SHARE_ACTION", "" + mShareAction);
                if (mShareAction != null) {
                    mShareAction.setShareIntent(intent);
                }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
