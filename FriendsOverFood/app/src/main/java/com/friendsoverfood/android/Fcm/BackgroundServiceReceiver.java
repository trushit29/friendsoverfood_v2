package com.friendsoverfood.android.Fcm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.friendsoverfood.android.Constants;

/**
 * Created by Trushit on 18/05/16.
 */
public class BackgroundServiceReceiver extends BroadcastReceiver {
    private AlarmManager alarams;
    public final String TAG = "BACKGROUND_SERVICE_RECEIVER";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Background service receiver called.");

        //Start Service for Push Notification & Receive messages in background
        try {
            intent = new Intent(context, AlarmReceiver.class);
            // create the object of @AlarmManager
            alarams = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            // set the alarm for particular time
            alarams.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                    PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
