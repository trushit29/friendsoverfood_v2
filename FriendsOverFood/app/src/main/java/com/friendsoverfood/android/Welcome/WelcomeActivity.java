package com.friendsoverfood.android.Welcome;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.ChatNew.ChatDetailsActivity;
import com.friendsoverfood.android.ChatNew.ChatListVO;
import com.friendsoverfood.android.ChatNew.MessageVO;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Fcm.AlarmReceiver;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.SignIn.SignInActivity;
import com.friendsoverfood.android.SignIn.TasteBudsSuggestionsActivity;
import com.friendsoverfood.android.WebViewActivity;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.friendsoverfood.android.util.NetworkUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Trushit on 23/02/17.
 */

public class WelcomeActivity extends BaseActivity {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "WELCOME_ACTIVITY";

    public AlarmManager alarams;

    private LinearLayout linearWelcome, linearSignUpSignIn;
    private Button btnSignIn, btnSignUp;
    private TextView textViewAppName;
    private CircleIndicator indicatorIntro;
    private ViewPager viewPagerIntro;
    private PagerAdapter mPagerAdapter;

    private boolean isPermissionDialogDisplayed = false;
    private String strReferCode = "", strMessageId = "", strFirebaseMessageReceived = "", strFriendIdReceived = "";
    public ArrayList<ChatListVO> matchList = new ArrayList<>();

    // Facebook Login
    private LoginButton btnLoginFb;
    // Facebook Login Variables
    private CallbackManager callbackManager;
    public UserProfileAPI profileSocialSignIn = null;
    public boolean isFromReferCode = false, isReferCodeFromSocialSignIn = false;
    public ChatListVO OtherUsers = null;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    public DatabaseReference mDatabaseMessages;
    private DatabaseReference mDatabase;

    public Target loadtarget;
    public VideoView videoVieWelcome;
    public int currentPos = 0;

    private Handler handlerAnim = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if (currentPos >= 3) {
                currentPos = 0;
            } else {
                currentPos = currentPos + 1;
            }

            if (viewPagerIntro != null) {
                viewPagerIntro.setCurrentItem(currentPos, true);
                handlerAnim.postDelayed(runnable, 5000);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(false);
        setTitleSupportActionBar(getString(R.string.title_welcome));

        /*setDrawerMenu();*/

        /*setDrawerMenu();
        lockDrawerMenu();*/

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_welcome_activity, getMiddleContent());

        linearWelcome = (LinearLayout) view.findViewById(R.id.linearLayoutWelcomeActivityContent);
        linearSignUpSignIn = (LinearLayout) view.findViewById(R.id.linearLayoutWelcomeActivitySignUpSignIn);
        btnSignIn = (Button) view.findViewById(R.id.buttonWelcomeActivitySignIn);
        btnSignUp = (Button) view.findViewById(R.id.buttonWelcomeActivitySignUp);
        btnLoginFb = (LoginButton) view.findViewById(R.id.buttonSignInActivityFacebook);
        textViewAppName = (TextView) view.findViewById(R.id.textViewAppName);
        viewPagerIntro = (ViewPager) view.findViewById(R.id.viewPagerWelcomeActivity);
        indicatorIntro = (CircleIndicator) view.findViewById(R.id.pagerIndicatorWelcomeActivity);
        videoVieWelcome = (VideoView) view.findViewById(R.id.videoViewWelcome);


        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPagerIntro.setAdapter(mPagerAdapter);
        indicatorIntro.setViewPager(viewPagerIntro);


        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");

        if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false)) {
            Intent intentAlarm = new Intent(context, AlarmReceiver.class);
            // create the object of @AlarmManager
            alarams = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            // set the alarm for particular time
            alarams.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                    PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
        }

//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
//        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, "");
        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
        SharedPreferenceUtil.save();

        videoVieWelcome = (VideoView) findViewById(R.id.videoViewWelcome);
        // use this to play video from raw resource
        videoVieWelcome.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.fof_welcome_animation));
        // use this to play video from url (streaming)
//        videoVieWelcome.setVideoURI(Uri.parse("http://..."));
        // if possible do not use height to match_parent. It may create problem. I am not sure about this. play video from asset
//        videoVieWelcome.setVideoURI(Uri.parse("file:///android_asset/path/to/your.mp4"));
        // play video from sdcard
//        videoVieWelcome.setVideoURI(Uri.fromFile(new File("/sdcard/cats.jpg")));


        /*videoVieWelcome.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // video play complete

            }
        });*/
        videoVieWelcome.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                videoVieWelcome.start();
            }
        });
//        videoVieWelcome.start();

        //get uri data for refer code if exists
        Uri data = getIntent().getData();
        if (data != null) {
            //get schma
            String scheme = data.getScheme() != null ? data.getScheme() : null; // "http"
            //get server name
            String host = data.getHost() != null ? data.getHost() : null; // Ip address or domain name
            //get parameter
            strReferCode = data.getQueryParameter(getString(R.string.api_response_param_key_refercode)) != null
                    ? data.getQueryParameter(getString(R.string.api_response_param_key_refercode)) : "";
            strMessageId = data.getQueryParameter(getString(R.string.api_response_param_key_messageid)) != null
                    ? data.getQueryParameter(getString(R.string.api_response_param_key_messageid)) : "";

            Log.d(TAG, "Data from intent ==> Scheme: " + scheme + ", Host: " + host + ", ReferCode: " + strReferCode + ", Message id: " + strMessageId);
        }

        if (SharedPreferenceUtil.getBoolean(Constants.IS_USER_LOGGED_IN, false)) {
            callFriendsListApiUpdateOnly();

            if (strReferCode.trim().isEmpty() && strMessageId.trim().isEmpty()) {
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                SharedPreferenceUtil.save();

                // TODO Forward user to MAP screen
                intent = new Intent(context, HomeActivity.class);
                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                supportFinishAfterTransition();
            } else {
                // Check if user is already friend. Otherwise user will be added with the use of refer code and friend request will be sent
                // automatically.
                // Send request for add refer code.
                linearSignUpSignIn.setVisibility(View.GONE);
                sendRequestAddReferCode();
            }
        } else {
            linearSignUpSignIn.setVisibility(View.VISIBLE);
            linearWelcome.setVisibility(View.VISIBLE);
        }

        initFacebookLogin();

//        setAllTypefaceDINMedium(btnRegister);
//        setAllTypefaceDINMedium(btnSignIn);
        setAllTypefaceMontserratRegular(view);
        setAllTypefaceMilkShakeItalic(textViewAppName);
        setListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseMessages = FirebaseDatabase.getInstance().getReference("messages");
    }

    private void initLayout() {

    }

    private void setListener() {
        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
    }

    private void showAllAppPermissionsEnabled() {
        if (!isPermissionDialogDisplayed) {
            isPermissionDialogDisplayed = true;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (!shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            && !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showDialogAlertRequestAllPermissions(getString(R.string.request_permission), getString(R.string.you_need_to_grant_required_permissions));
                        return;
                    }
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                                    , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
                } else {
                    // All permissions are granted. Forward user to home screen of the app.
                    initLayout();
                }
            } else {
                initLayout();
            }
        }
    }

    protected void showDialogAlertRequestAllPermissions(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                                    , Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS);
                }
            }
        });
                /*.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                Snackbar.make(view, getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly), Snackbar.LENGTH_LONG).show();
//                showSn(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
            }
        });*/
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            currentPos = position;
            switch (position) {
                case 0:
                    return new AppIntroFragment1();

                case 1:
                    return new AppIntroFragment2();

                case 2:
                    return new AppIntroFragment3();

                default:
                    return new AppIntroFragment1();

            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "On resume called for alarm receiver to start.");

        handlerAnim.postDelayed(runnable, 5000);

        // TODO Clear notification message list here.
        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, "");
        SharedPreferenceUtil.save();

        /*if (!checkIsAllAppPermissionsEnabled()) {
            showAllAppPermissionsEnabled();
        }*/

    }

    @Override
    public void onPause() {
        if (handlerAnim != null) {
            handlerAnim.removeCallbacks(runnable);
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.buttonWelcomeActivitySignIn:
                intent = new Intent(context, SignInActivity.class);
                intent.putExtra("isSignIn", true);
                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                startActivity(intent);
                break;

            case R.id.buttonWelcomeActivitySignUp:
                /*intent = new Intent(context, SignInActivity.class);
                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                intent.putExtra("isSignIn", false);
                startActivity(intent);*/

//                http://friendsoverfoods.com/
                intent = new Intent(context, WebViewActivity.class);
                intent.putExtra("link", getString(R.string.fof_website_url));
                startActivity(intent);
                break;

            default:
                break;

        }
    }

    public void sendRequestAddReferCode() {
        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_addreference));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_reference), strReferCode.trim());
        params.put(getString(R.string.api_param_key_messageid), strMessageId.trim());

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_ADDREFERENCE, this);
    }

    public void callFriendsListApi() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params, Constants.ACTION_CODE_API_FRIEND_LIST, WelcomeActivity.this);
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {
            if (action == Constants.ACTION_CODE_API_ADDREFERENCE && response != null) {
                Log.d(TAG, "ACTION_CODE_API_ADD_REFERENCE: " + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        String email = "", id = "", profilepic1 = "";
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            email = !data.isNull(context.getResources().getString(R.string.api_response_param_key_email))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_email)).trim() : "";
                            id = !data.isNull(context.getResources().getString(R.string.api_response_param_key_id))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_id)).trim() : "";
                            profilepic1 = !data.isNull(context.getResources().getString(R.string.api_response_param_key_profilepic1))
                                    ? data.getString(context.getResources().getString(R.string.api_response_param_key_profilepic1)).trim() : "";


                            // Save Firebae Message & Friend object here.
//                            firebase_message
                            strFirebaseMessageReceived = !data.isNull(getString(R.string.api_response_param_key_firebase_message))
                                    ? data.getString(getString(R.string.api_response_param_key_firebase_message)).trim() : "";
                            JSONObject friends = !data.isNull(getString(R.string.api_response_param_key_friends))
                                    ? data.getJSONObject(getString(R.string.api_response_param_key_friends)) : new JSONObject();
                            strFriendIdReceived = !friends.isNull(getString(R.string.api_response_param_key_id))
                                    ? friends.getString(getString(R.string.api_response_param_key_id)).trim() : "";

                            // If friend id is valid, send message to firebase here and forward user to chat screen directly.
                            try {
                                // Send a message to firebase
                                if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                                        && !strFriendIdReceived.trim().isEmpty()) {
                                    JSONObject jsonObjectResponseFriend = null;

                                    ChatListVO OtherUsers = null;
                                    jsonObjectResponseFriend = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                            ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                                    Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponseFriend.toString().trim());
                                    JSONObject settingsFriend = !jsonObjectResponseFriend.isNull(getString(R.string.api_response_param_key_settings))
                                            ? jsonObjectResponseFriend.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                                    boolean statusFriend = !settingsFriend.isNull(getString(R.string.api_response_param_key_success))
                                            ? settingsFriend.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                                    String errormessageFriend = !settingsFriend.isNull(getString(R.string.api_response_param_key_errormessage))
                                            ? settingsFriend.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                                    if (statusFriend) {
                                        matchList = new ArrayList<>();
                                        JSONArray dataFriend = !jsonObjectResponseFriend.isNull(getString(R.string.api_response_param_key_data))
                                                ? jsonObjectResponseFriend.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                                        JSONObject dataInner = null;
                                        for (int x = 0; x < dataFriend.length(); x++) {
                                            ChatListVO item = new ChatListVO();
                                            try {
                                                dataInner = dataFriend.getJSONObject(x);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                            String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                            String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                            String statusFriendInner = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                                    ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                            item.setMatchid(matchId);
                                            item.setFriend1(friend1);
                                            item.setFriend2(friend2);

                                            JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                                    ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();

                                            for (int j = 0; j < details.length(); j++) {
                                                JSONObject detailInner = details.getJSONObject(j);
                                                String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                                String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                                String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                                String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                                String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                                        ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                                item.setUserId(userId);
                                                item.setFirstName(first_name);
                                                item.setLastName(lastName);
                                                item.setProfilePic(profilePic);
                                                item.setGender(gender);
                                                item.setStatus(!statusFriendInner.trim().isEmpty() ? statusFriendInner.trim() : "0");
                                            }
                                            matchList.add(item);

                                        }

                                    }

                                    // Get user object here.
                                    boolean isFriendExists = false;
                                    for (int x = 0; x < matchList.size(); x++) {
                                        if (strFriendIdReceived.trim().equalsIgnoreCase(matchList.get(x).getMatchid())) {
                                            isFriendExists = true;
                                            OtherUsers = matchList.get(x);
                                        }
                                    }


                                    // Prepare POJO Message VO from JSON Array
                                    if (isFriendExists) {
                                        JSONArray array = new JSONArray(strFirebaseMessageReceived);
                                        for (int x = 0; x < array.length(); x++) {
                                            String strMessage = array.getString(x);
                                            Calendar c = Calendar.getInstance();
                                            long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                            Gson gson = new Gson();
                                            MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                                            message.setRecieverId(SharedPreferenceUtil.getString(Constants
                                                    .LOGGED_IN_USER_USER_ID, "").trim());
                                            message.setRecieverDP(SharedPreferenceUtil.getString(Constants
                                                    .LOGGED_IN_USER_PROFILEPIC1, "").trim());
                                            message.setDelivered(String.valueOf(timestampToSave));

//                                                                Log.d(TAG, "Set sender details here: ID: " + senderId + ", senderDp: " + senderDp);

                                            // Insert message to firebase here.
                                            String key = mDatabase.child("posts").push().getKey();
                                            mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                                            // Add key here
                                            message.setKey(key);


                                                /*// Get details of sender user here.
                                                ChatListVO OtherUsers = new ChatListVO();
                                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                OtherUsers.setUserId(message.getSenderId().trim());
//                                                                OtherUsers.setFirstName(message.get);*/
                                            if (OtherUsers != null) {
                                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                                OtherUsers.setUserId(message.getSenderId().trim());
                                            }
                                        }

                                        // Forward user to chat screen from here.
                                        Intent intent = new Intent(WelcomeActivity.this, ChatDetailsActivity.class);
                                        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUsers);
                                        intent.putExtra("isFromNotification", true);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        supportFinishAfterTransition();
                                    } else {
                                        callFriendsListApi();
                                    }
                                }
                            } catch (JSONException e) {
                                stopProgress();
                                e.printStackTrace();

                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                                SharedPreferenceUtil.save();

                                // TODO Forward user to MAP screen
                                intent = new Intent(context, HomeActivity.class);
                                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } catch (Exception e) {
                                stopProgress();
                                e.printStackTrace();

                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                                SharedPreferenceUtil.save();

                                // TODO Forward user to MAP screen
                                intent = new Intent(context, HomeActivity.class);
                                intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                                intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } finally {

                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST && response != null) {
//                Log.i("Response", "" + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();


                try {
                    // Send a message to firebase
                    if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                            && !strFriendIdReceived.trim().isEmpty()) {
                        JSONObject jsonObjectResponse = null;

                        ChatListVO OtherUsers = null;
                        jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                        Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                        JSONObject settings = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_settings))
                                ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                        boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                        String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                        if (status) {
                            matchList = new ArrayList<>();
                            JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                    ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                            JSONObject dataInner = null;
                            for (int i = 0; i < data.length(); i++) {
                                ChatListVO item = new ChatListVO();
                                try {
                                    dataInner = data.getJSONObject(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                item.setMatchid(matchId);
                                item.setFriend1(friend1);
                                item.setFriend2(friend2);

                                JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                        ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                                for (int j = 0; j < details.length(); j++) {

                                    JSONObject detailInner = details.getJSONObject(j);
                                    String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                    String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                    String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                    String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                    String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                    item.setUserId(userId);
                                    item.setFirstName(first_name);
                                    item.setLastName(lastName);
                                    item.setProfilePic(profilePic);
                                    item.setGender(gender);
                                    item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                                }
                                matchList.add(item);

                            }

                        }

                        // Get user object here.
                        boolean isFriendExists = false;
                        for (int i = 0; i < matchList.size(); i++) {
                            if (strFriendIdReceived.trim().equalsIgnoreCase(matchList.get(i).getMatchid().trim())) {
                                isFriendExists = true;
                                OtherUsers = matchList.get(i);
                            }
                        }


                        if (isFriendExists) {
                            // Prepare POJO Message VO from JSON Array
                            JSONArray array = new JSONArray(strFirebaseMessageReceived);
                            for (int i = 0; i < array.length(); i++) {
                                String strMessage = array.getString(i);
                                Calendar c = Calendar.getInstance();
                                long timestampToSave = Constants.convertTimestampTo10DigitsOnly(c.getTimeInMillis(), true);

                                Gson gson = new Gson();
                                MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                                message.setRecieverId(SharedPreferenceUtil.getString(Constants
                                        .LOGGED_IN_USER_USER_ID, "").trim());
                                message.setRecieverDP(SharedPreferenceUtil.getString(Constants
                                        .LOGGED_IN_USER_PROFILEPIC1, "").trim());
                                message.setDelivered(String.valueOf(timestampToSave));

//                                                                Log.d(TAG, "Set sender details here: ID: " + senderId + ", senderDp: " + senderDp);

                                // Insert message to firebase here.
                                String key = mDatabase.child("posts").push().getKey();
                                mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                                // Add key here
                                message.setKey(key);


                                if (OtherUsers != null) {
                                    OtherUsers.setProfilePic(message.getSenderDp().trim());
                                    OtherUsers.setUserId(message.getSenderId().trim());
                                }

                                /*Log.d(TAG, "NOtification on invite called.");
                                sendNotification(message.getMsg().trim(), message.getMsg().trim(), "message", OtherUsers);*/
                            }

                            // Forward user to chat screen from here.
                            Intent intent = new Intent(WelcomeActivity.this, ChatDetailsActivity.class);
                            intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                            intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUsers);
                            intent.putExtra("isFromNotification", true);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            supportFinishAfterTransition();
                        } else {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                            SharedPreferenceUtil.save();

                            // TODO Forward user to MAP screen
                            intent = new Intent(context, HomeActivity.class);
                            intent.putExtra(getString(R.string.api_response_param_key_refercode), strReferCode.trim());
                            intent.putExtra(getString(R.string.api_response_param_key_messageid), strMessageId.trim());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            supportFinishAfterTransition();
                        }
                    }
                } catch (JSONException e) {
                    stopProgress();
                    e.printStackTrace();
                } catch (Exception e) {
                    stopProgress();
                    e.printStackTrace();
                }
            } else if (action == Constants.ACTION_CODE_API_GOOGLE_SIGN_IN || action == Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN) {
                Log.d("RESPONSE_LOGIN_SOCIAL", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        // path to /data/data/yourapp/app_data/imageDir
                        ContextWrapper cw = new ContextWrapper(getApplicationContext());
                        File directory = cw.getDir("profileimages", Context.MODE_PRIVATE);
                        deleteRecursive(directory);

                        JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        JSONObject dataInner = null;
                        for (int i = 0; i < data.length(); i++) {
                            dataInner = data.getJSONObject(i);
                        }

                        if (dataInner != null) {
                            profileSocialSignIn = new UserProfileAPI(context, dataInner);

                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profileSocialSignIn.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profileSocialSignIn.sessionid.trim());

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profileSocialSignIn.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profileSocialSignIn.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profileSocialSignIn.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profileSocialSignIn.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profileSocialSignIn.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profileSocialSignIn.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profileSocialSignIn.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profileSocialSignIn.testbuds.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profileSocialSignIn.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profileSocialSignIn.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profileSocialSignIn.isregistered);
                            SharedPreferenceUtil.save();

                            // Save Firebae Message & Friend object here.
//                            firebase_message
                            strFirebaseMessageReceived = !dataInner.isNull(getString(R.string.api_response_param_key_firebase_message))
                                    ? dataInner.getString(getString(R.string.api_response_param_key_firebase_message)).trim() : "";
                            JSONObject friends = !dataInner.isNull(getString(R.string.api_response_param_key_friends))
                                    ? dataInner.getJSONObject(getString(R.string.api_response_param_key_friends)) : new JSONObject();
                            strFriendIdReceived = !friends.isNull(getString(R.string.api_response_param_key_id))
                                    ? friends.getString(getString(R.string.api_response_param_key_id)).trim() : "";

                            Intent intentAlarm = new Intent(context, AlarmReceiver.class);
                            // create the object of @AlarmManager
                            alarams = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            // set the alarm for particular time
                            alarams.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), Constants.TIMER_ALARM_SERVICE,
                                    PendingIntent.getBroadcast(context, 1, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));

                            // Call friend list api here, if refer id, message id & friend id are fetched.
                            if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()) {
                                if (strFriendIdReceived.trim().isEmpty()) {
                                    Log.d(TAG, "Friend id is found empty. Call add refer code api here.");
                                    isReferCodeFromSocialSignIn = true;
                                    sendRequestAddReferCode();
                                } else {
                                    callFriendsListApiForSocialSignIn();
                                }
                            } else {
                                callFriendsListApiForSocialSignIn();
                            }
                        } else {
                            stopProgress();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            } else if (action == Constants.ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN && response != null) {
                Log.i(TAG, "ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN: " + response);
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FRIEND_LIST, response);
                SharedPreferenceUtil.save();

                try {
                    // Send a message to firebase
                    if (!strReferCode.trim().isEmpty() && !strMessageId.trim().isEmpty()
                            && !strFriendIdReceived.trim().isEmpty()) {
                        JSONObject jsonObjectResponse = null;

                        OtherUsers = null;
                        jsonObjectResponse = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "").trim().isEmpty()
                                ? new JSONObject(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FRIEND_LIST, "")) : new JSONObject();
                        Log.d(TAG, "Friends list response from SharedPreferences: " + jsonObjectResponse.toString().trim());
                        JSONObject settings = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_settings))
                                ? jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings)) : new JSONObject();
                        boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                                ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                        String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                                ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";
                        if (status) {
                            matchList = new ArrayList<>();
                            JSONArray data = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                    ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();

                            JSONObject dataInner = null;
                            for (int i = 0; i < data.length(); i++) {
                                ChatListVO item = new ChatListVO();
                                try {
                                    dataInner = data.getJSONObject(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String matchId = !dataInner.isNull(getString(R.string.api_response_param_key_id))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                String friend1 = !dataInner.isNull(getString(R.string.api_response_param_key_friend1))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend1)).trim() : "";
                                String friend2 = !dataInner.isNull(getString(R.string.api_response_param_key_friend2))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_friend2)).trim() : "";
                                String statusFriend = !dataInner.isNull(getString(R.string.api_response_param_key_status))
                                        ? dataInner.getString(getString(R.string.api_response_param_key_status)).trim() : "";
                                item.setMatchid(matchId);
                                item.setFriend1(friend1);
                                item.setFriend2(friend2);

                                JSONArray details = !dataInner.isNull(getString(R.string.api_response_param_key_details))
                                        ? dataInner.getJSONArray(getString(R.string.api_response_param_key_details)) : new JSONArray();
                                for (int j = 0; j < details.length(); j++) {

                                    JSONObject detailInner = details.getJSONObject(j);
                                    String userId = !detailInner.isNull(getString(R.string.api_response_param_key_id))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_id)).trim() : "";
                                    String first_name = !detailInner.isNull(getString(R.string.api_response_param_key_first_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_first_name)).trim() : "";
                                    String lastName = !detailInner.isNull(getString(R.string.api_response_param_key_last_name))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_last_name)).trim() : "";
                                    String profilePic = !detailInner.isNull(getString(R.string.api_response_param_key_profilepic1))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_profilepic1)).trim() : "";
                                    String gender = !detailInner.isNull(getString(R.string.api_response_param_key_gender))
                                            ? detailInner.getString(getString(R.string.api_response_param_key_gender)).trim() : "";
                                    item.setUserId(userId);
                                    item.setFirstName(first_name);
                                    item.setLastName(lastName);
                                    item.setProfilePic(profilePic);
                                    item.setGender(gender);
                                    item.setStatus(!statusFriend.trim().isEmpty() ? statusFriend.trim() : "0");
                                }
                                matchList.add(item);

                            }

                        }

                        // Get user object here.
                        for (int i = 0; i < matchList.size(); i++) {
                            OtherUsers = matchList.get(i);
                        }

                        // Prepare POJO Message VO from JSON Array
                        JSONArray array = new JSONArray(strFirebaseMessageReceived);
                        for (int i = 0; i < array.length(); i++) {
                            String strMessage = array.getString(i);

                            Gson gson = new Gson();
                            MessageVO message = gson.fromJson(strMessage, MessageVO.class);
                            message.setRecieverId(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, "").trim());
                            message.setRecieverDP(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());

                            // Insert message to firebase here.
                            String key = mDatabase.child("posts").push().getKey();
                            mDatabaseMessages.child(strFriendIdReceived.trim()).child(key).setValue(message);

                            // Add key here
                            message.setKey(key);

                           /* // Get details of sender user here.
                            ChatListVO OtherUsers = new ChatListVO();
                            OtherUsers.setUserId(message.getSenderId().trim());
                            OtherUsers.setProfilePic(message.getSenderDp().trim());*/
//                                                                OtherUsers.setFirstName(message.get);

                            if (OtherUsers != null) {
                                OtherUsers.setProfilePic(message.getSenderDp().trim());
                                OtherUsers.setUserId(message.getSenderId().trim());
                            }

//                                        if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS, true)) {
                            if (!isFromReferCode) {
                                Log.d(TAG, "NOtification on invite called.");
                                sendNotification(message.getMsg().trim(), message.getMsg().trim(), "message", OtherUsers);
                            }
//                                        }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {

                    if (!profileSocialSignIn.isregistered) {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profileSocialSignIn.gender.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profileSocialSignIn.first_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profileSocialSignIn.last_name.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profileSocialSignIn.mobile.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profileSocialSignIn.dob.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profileSocialSignIn.occupation.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profileSocialSignIn.education.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profileSocialSignIn.relationship.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profileSocialSignIn.about_me.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profileSocialSignIn.ethnicity.trim());

                        // Split location and save current latitude & longitude of user.
                        String[] location = profileSocialSignIn.location.trim().split(",");
                        if (location.length == 2) {
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().trim().replaceAll(",", "."));
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().trim().replaceAll(",", "."));
                        }

                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profileSocialSignIn.location_string.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profileSocialSignIn.account_type.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profileSocialSignIn.access_token.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profileSocialSignIn.social_id.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1,
                                !profileSocialSignIn.profilepic1.trim().isEmpty() ? appendUrl(profileSocialSignIn.profilepic1.trim()) :
                                        (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim().isEmpty()
                                                ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim() : ""));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, appendUrl(profileSocialSignIn.profilepic2.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, appendUrl(profileSocialSignIn.profilepic3.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, appendUrl(profileSocialSignIn.profilepic4.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, appendUrl(profileSocialSignIn.profilepic5.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, appendUrl(profileSocialSignIn.profilepic6.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, appendUrl(profileSocialSignIn.profilepic7.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, appendUrl(profileSocialSignIn.profilepic8.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, appendUrl(profileSocialSignIn.profilepic9.trim()));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profileSocialSignIn.refercode.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profileSocialSignIn.reference.trim());
                        SharedPreferenceUtil.save();
//                                showToast(getString(R.string.you_are_successfully_logged_in));

                        // TODO Save Profile of user, when user logs in. So that device token and otehr info is always updated in db.
                        sendRequestSaveSocialProfileAPI();

                                /*// TODO Forward user to TasteBuds profile screen.
                                intent = new Intent(context, TasteBudsSuggestionsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                supportFinishAfterTransition();*/
                    } else {
//                                showToast(getString(R.string.you_are_successfully_logged_in));

//                        if (action == Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN) { // TODO Check if all pictures are empty,
                        // then look for all facebook profile pictures and then save user profile to server.
                        Log.d(TAG, "Social profile pic: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim()
                                + ", Profile pic: " + profileSocialSignIn.profilepic1.trim());
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1,
                                !profileSocialSignIn.profilepic1.trim().isEmpty() ? appendUrl(profileSocialSignIn.profilepic1.trim()) :
                                        (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim().isEmpty()
                                                ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, "").trim() : ""));
                        SharedPreferenceUtil.save();
                        Log.d(TAG, "Social profile pic: " + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());

                        if (profileSocialSignIn.profilepic2.trim().isEmpty() &&
                                profileSocialSignIn.profilepic3.trim().isEmpty() && profileSocialSignIn.profilepic4.trim().isEmpty()
                                && profileSocialSignIn.profilepic4.trim().isEmpty() && profileSocialSignIn.profilepic5.trim().isEmpty()
                                && profileSocialSignIn.profilepic6.trim().isEmpty() && profileSocialSignIn.profilepic7.trim().isEmpty()
                                && profileSocialSignIn.profilepic8.trim().isEmpty() && profileSocialSignIn.profilepic9.trim().isEmpty()) {
                            showProgress(getString(R.string.loading));
                            getFacebookAlbumsForProfilePicture("");
                        }
                        /*} else { // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        }*/
                    }
                }
            } else if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d("RESPONSE_EDIT_PROFILE_SOCIAL_SIGN_IN", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim());
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, profile.profilepic6.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, profile.profilepic7.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, profile.profilepic8.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, profile.profilepic9.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            stopProgress();
                            // TODO Forward user to TasteBuds profile screen or Home Screen according to preferences saved.
                            if (SharedPreferenceUtil.getBoolean(Constants.LOGGED_IN_USER_ISREGISTERED, false)) {
                               /* intent = new Intent(context, EditProfileNewActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                                startActivity(intent);
                                supportFinishAfterTransition();*/

                                intent = new Intent(this, TasteBudsSuggestionsActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("isFromRegister", true);
                                startActivity(intent);
                                supportFinishAfterTransition();
                            } else {
                                if (isFromReferCode) {
                                    // Forward user to chat screen from here.
                                    Intent intent = new Intent(WelcomeActivity.this, ChatDetailsActivity.class);
                                    intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
                                    intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUsers);
                                    intent.putExtra("isFromNotification", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    supportFinishAfterTransition();
                                } else {
                                    intent = new Intent(context, HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.putExtra(Constants.INTENT_USER_LOGGED_IN, true);
                                    startActivity(intent);
                                    supportFinishAfterTransition();
                                }
                            }
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }
        } else {
            Log.d(TAG, "Null response received for action: " + action);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_ALL_PERMISSIONS:
//                isPermissionDialogDisplayed = false;
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    showSnackBarMessageOnly(getString(R.string.you_have_successfully_granted_required_permissions));
                    initLayout();
//                    Log.d(TAG, "Permission granted pos: " + 0);
                } else {
                    // Permission Denied
                    showSnackBarMessageOnly(getString(R.string.you_have_not_enabled_the_required_permissions_the_app_may_not_behave_properly));
                    initLayout();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initFacebookLogin() {
        /*if (txtPrivacyPolicy.isChecked()) {*/
        callbackManager = CallbackManager.Factory.create();
//        btnLoginFb.setReadPermissions("email, public_profile, user_friends", "user_birthday","user_education_history","user_about_me","user_religion_politics","user_work_history","user_relationships");
        btnLoginFb.setReadPermissions("email, public_profile, user_friends", "user_birthday", "user_education_history", "user_about_me",
                "user_work_history", "user_relationships", "user_photos");
        btnLoginFb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final LoginResult loginResult1 = loginResult;
                showProgress(getString(R.string.loading));
                Log.i("FACEBOOK_LOGIN_RESPONSE", "Access Token: " + loginResult.getAccessToken().getToken() + ", Session ACCESS_TOKEN: " + AccessToken
                        .getCurrentAccessToken().getToken());

                handleFacebookAccessToken(AccessToken.getCurrentAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Log.d(TAG, "FB Login Respone: " + response.toString());
                            // Application code
                            final String birthday = !object.isNull("birthday") ? object.getString("birthday") : ""; // 09/29/1992 - MM/dd/yyyy

                            final String lastName = !object.isNull("last_name") ? object.getString("last_name") : "";
                            final String bio = !object.isNull("bio") ? object.getString("bio") : "";
                            /*final String location = !object.isNull("location") && !object.getJSONObject("location").isNull("name")
                                    ? object.getJSONObject("location").getString("name") : "";*/
                            final String about = !object.isNull("about") ? object.getString("about") : "";
                            final String gender = !object.isNull("gender") ? object.getString("gender") : "";
//                            final String work = !object.isNull("work") ? object.getString("work") : "";
                            final String name = !object.isNull("name") ? object.getString("name") : "";
                            final String religion = !object.isNull("religion") ? object.getString("religion") : "";
                            final String relationship_status = !object.isNull("relationship_status") ? object.getString("relationship_status") : "";
                            String pictureFb = !object.isNull("picture") && !object.getJSONObject("picture").isNull("data")
                                    && !object.getJSONObject("picture").getJSONObject("data").isNull("url")
                                    ? object.getJSONObject("picture").getJSONObject("data").getString("url").trim() : "";
                            String id = !object.isNull("id") ? object.getString("id") : "";
//                            final String email = !object.isNull("email") ? object.getString("email") : id;
                            final String email = !object.isNull("email") ? object.getString("email") : id;
                            final String firstName = !object.isNull("first_name") ? object.getString("first_name") : "";
                            if (!id.trim().isEmpty()) {
                                pictureFb = "https://graph.facebook.com/" + id + "/picture?type=large";
                            } else {
                                pictureFb = !object.isNull("picture") && !object.getJSONObject("picture").isNull("data")
                                        && !object.getJSONObject("picture").getJSONObject("data").isNull("url")
                                        ? object.getJSONObject("picture").getJSONObject("data").getString("url").trim() : "";
                            }

                            // Get formatted bitrh date here.
                            /*String dob = "";
                            if (!birthday.trim().isEmpty()) {
                                try {
                                    Date birthdate = new SimpleDateFormat("MM/dd/yyyy", Locale.US).parse(birthday);
                                   *//* dob = new SimpleDateFormat("dd-MM-yyyy").format(birthdate).trim();*//*
                                    dob = String.valueOf(Constants.convertTimestampTo10DigitsOnly(birthdate.getTime(), true));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }*/

                            String fullName = "";
                            if (!firstName.trim().isEmpty() && !firstName.trim().equalsIgnoreCase("null")) {
                                fullName = firstName.trim();
                            }

                            if (!lastName.trim().isEmpty() && !lastName.trim().equalsIgnoreCase("null")) {
                                fullName = fullName + " " + lastName.trim();
                            }

                            JSONArray work = !object.isNull("work") ? object.getJSONArray("work") : new JSONArray();

                            for (int i = 0; i < work.length(); i++) {

                                JSONObject work1 = work.getJSONObject(i);

                                JSONObject employer = !work1.isNull("employer")
                                        ? work1.getJSONObject("employer") : new JSONObject();
                                String nameEmployer = !employer.isNull("name") ? employer.getString("name") : "";

                                if (i == 0) {

                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMPLOYER, nameEmployer);
                                    SharedPreferenceUtil.save();
                                }

                                JSONObject position = !work1.isNull("position")
                                        ? work1.getJSONObject("position") : new JSONObject();
                                String Positionname = !position.isNull("name") ? position.getString("name") : "";

                                if (i == 0) {
                                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, Positionname);
                                    SharedPreferenceUtil.save();
                                }

                                JSONArray education = !object.isNull("education") ? object.getJSONArray("education") : new JSONArray();

                                for (int j = 0; j < education.length(); j++) {
                                    JSONObject educationdetail = education.getJSONObject(j);

                                    String type = !educationdetail.isNull("type") ? educationdetail.getString("type") : "";

                                    JSONObject school = !educationdetail.isNull("school")
                                            ? educationdetail.getJSONObject("school") : new JSONObject();
                                    String nameSchool = !school.isNull("name") ? school.getString("name") : "";
                                    if (type.equalsIgnoreCase("College")) {
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, nameSchool);
                                        SharedPreferenceUtil.save();
                                    } else if (j == education.length()) {
                                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, nameSchool);
                                        SharedPreferenceUtil.save();
                                    }
                                }
                            }

                            if (!email.trim().isEmpty()) {
                                SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, birthday);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EMAIL, email);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, gender);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, firstName);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, lastName);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC_SOCIAL, pictureFb);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "Facebook");
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, loginResult1.getAccessToken().getToken());
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, about);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, religion);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, relationship_status);
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, id.trim());
//                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USERNAME, id.trim());
                                SharedPreferenceUtil.save();
                                stopProgress();

                                onSuccessRegisterOrLogin();
                            } else {
                                stopProgress();

                                if (LoginManager.getInstance() != null) {
                                    LoginManager.getInstance().logOut();
                                }
                                SharedPreferenceUtil.clear();
                                SharedPreferenceUtil.save();
                                showAlertDialogPositiveButtonOnly(getString(R.string.sign_in), getString(R.string.email_permission_is_required_to_sign_in));
                            }
                        } catch (JSONException e) {
                            stopProgress();
                            e.printStackTrace();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));
                            SharedPreferenceUtil.clear();
                            SharedPreferenceUtil.save();

                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        } catch (Exception e) {
                            stopProgress();
                            e.printStackTrace();
                            showSnackBarMessageOnly(getString(R.string.some_error_occured));

                            if (LoginManager.getInstance() != null) {
                                LoginManager.getInstance().logOut();
                            }

                        }
                    }
                });
                Bundle parameters = new Bundle();
//                parameters.putString("fields", "bio,about,birthday,email,first_name,gender,id,last_name,name,link,picture");
                parameters.putString("fields", "about,birthday,email,first_name,gender,id,last_name,name,link,age_range,education,religion,work,relationship_status");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(context, "You have cancelled login with facebook", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
               /* Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();*/
                showSnackBarMessageOnly(e.getMessage());
            }
        });
        /*} else {
            showAlertDialogPositiveButtonOnly(getString(R.string.alert), getString(R.string.you_must_have_to_agree_with_terms_and_privacy_policy));
        }*/
    }

    public void handleFacebookAccessToken(AccessToken token) {
        Log.d("SIGNUP_FRAGMENT", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d("", "signInWithCredential:onComplete:" + task.isSuccessful());

                // If sign in fails, display a message to the user. If sign in succeeds the auth state listener will be notified and logic to
                // handle the signed in user can be handled in the listener.
                if (!task.isSuccessful()) {
                    Log.w("", "signInWithCredential", task.getException());
                    Toast.makeText(getApplicationContext(), "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void onSuccessRegisterOrLogin() {
        loginWithFaceBookApi();
    }

    private void loginWithFaceBookApi() {
        if (NetworkUtil.isOnline(context)) {
            showProgress(getString(R.string.loading));

            params = new HashMap<>();
            params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_socialsignin));
            params.put(getString(R.string.api_param_key_last_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_LAST_NAME, ""));
            params.put(getString(R.string.api_param_key_first_name), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_FIRST_NAME, ""));
            params.put(getString(R.string.api_param_key_email), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMAIL, ""));
            params.put(getString(R.string.api_param_key_password), getString(R.string.api_param_value_password_default));
            params.put(getString(R.string.api_param_key_mobile), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_MOBILE, "123"));
            params.put(getString(R.string.api_param_key_reference), !strReferCode.trim().isEmpty() ? strReferCode.trim() : "");
            params.put(getString(R.string.api_param_key_messageid), !strMessageId.trim().isEmpty() ? strMessageId.trim() : "");
            params.put(getString(R.string.api_param_key_devicetype), "android");
//            params.put(getString(R.string.api_param_key_devicetype), "Android");
//            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, ""));
//            params.put(getString(R.string.api_param_key_accounttype), "Facebook");
//            params.put(getString(R.string.api_param_key_profilepic), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILE_IMAGE_URL, ""));
//            params.put(getString(R.string.api_param_key_birthdate), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "")); // Use facebook id as username
//            params.put(getString(R.string.api_param_key_sex), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, ""));
//            params.put(getString(R.string.api_param_key_username), !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USERNAME, "").trim().isEmpty()
//                    ? SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USERNAME, "").trim()
//                    : SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim());
//            params.put(getString(R.string.api_param_key_social_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, ""));
//            params.put(getString(R.string.api_param_key_education), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, ""));
//            params.put(getString(R.string.api_param_key_religion), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELIGION, ""));
//            params.put(getString(R.string.api_param_key_employer), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EMPLOYER, ""));
//            params.put(getString(R.string.api_param_key_relationshipstatus), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP_STATUS, ""));
//            params.put(getString(R.string.api_param_key_occupation), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, ""));
//            params.put(getString(R.string.api_param_key_about), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT, ""));

            new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                    Constants.ACTION_CODE_API_FACEBOOK_SIGN_IN, WelcomeActivity.this);
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));

            if (LoginManager.getInstance() != null) {
                LoginManager.getInstance().logOut();
            }
        }
    }

    public void callFriendsListApiForSocialSignIn() {
//        showProgress(getString(R.string.loading));
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_myfriends));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));

        new HttpRequestSingletonPost(mContext, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_FRIEND_LIST_SOCIAL_SING_IN, WelcomeActivity.this);
    }

    private void sendNotification(String text, String messageBody, String type, ChatListVO OtherUser) {
        List<String> listTemp = new ArrayList<>(Arrays.asList(SharedPreferenceUtil.getString(Constants.NOTIFICATION_MESSAGES, "").trim().split(",")));
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < listTemp.size(); i++) {
            if (!listTemp.get(i).trim().isEmpty()) {
                list.add(listTemp.get(i).trim());
            }
        }

        if (!text.trim().isEmpty()) {
            list.add(text.trim());
        } else {
            list.add(text.trim());
        }

        SharedPreferenceUtil.putValue(Constants.NOTIFICATION_MESSAGES, android.text.TextUtils.join(",", list));
        SharedPreferenceUtil.save();

//            Intent intent = new Intent(this, ListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_UPDATE_CURRENT);

//        Intent intent = new Intent(SignInActivity.this, ChatListActivity.class);
        Intent intent = new Intent(WelcomeActivity.this, ChatDetailsActivity.class);
        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList);
        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser);
        intent.putExtra("isFromNotification", true);
//        intent.putExtra(Constants.INTENT_CHAT_SELECTED, OtherUser); 
//        intent.putExtra(Constants.INTENT_CHAT_LIST, matchList); 
//        intent.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());

        PendingIntent contentIntent = null;
        contentIntent = PendingIntent.getActivity(getApplicationContext(), (int) (Math.random() * 100), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setTicker(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setSmallIcon(R.mipmap.ic_notification_big)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(OtherUser.getFirstName() + " " + OtherUser.getLastName())
                .setContentText(!messageBody.trim().isEmpty() ? messageBody.trim() : getString(R.string.you_have_a_new_notification))
                .setAutoCancel(true)
                .setNumber(list.size())
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_HIGH)
                .setContentIntent(contentIntent)
                .setColor(getResources().getColor(R.color.colorPrimaryDark));

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle(notificationBuilder).setBigContentTitle(getString(R.string.app_name));
        if (list.size() == 1) {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notification));
            pictureStyleNotification(inboxStyle, text, type, notificationBuilder, OtherUser);

        } else {
            inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

            Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
            notification.defaults |= Notification.DEFAULT_VIBRATE;
            notification.defaults |= Notification.DEFAULT_LIGHTS;
            notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

            Log.d(TAG, "NOtification on invite. Sending notification.");

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
            notificationManager.notify(0, notification);

            // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
            sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/
        }

        for (int i = 0; i < list.size(); i++) {
            inboxStyle.addLine(list.get(i));
        }
        inboxStyle.setSummaryText(list.size() + " " + getString(R.string.new_notifications));

        Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
        notificationManager.notify(0, notification);

        // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
        sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

    }

    public void pictureStyleNotification(final NotificationCompat.InboxStyle inboxStyle, final String text, final String type,
                                         final NotificationCompat.Builder notificationBuilder, final ChatListVO OtherUser) {
        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                notificationBuilder.setLargeIcon(bitmap);

                Notification notification = inboxStyle.build();
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
                notification.defaults |= Notification.DEFAULT_VIBRATE;
                notification.defaults |= Notification.DEFAULT_LIGHTS;
                notification.defaults |= Notification.FLAG_SHOW_LIGHTS;

                Log.d(TAG, "NOtification on invite. Sending notification.");

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0  ID of notification, notificationBuilder.build());
                notificationManager.notify(0, notification);

                // Update current activity using Local Broadcast manager if needed.
       /* if (type.trim().equalsIgnoreCase("like")) {*/
                sendLocalBroadcastManager(text.trim(), type.trim());
        /*}*/

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };

        if (!OtherUser.getProfilePic().trim().isEmpty()) {
            Picasso.with(getApplicationContext())
                    .load(OtherUser.getProfilePic())
                    .into(loadtarget);
        }
    }

    private void sendLocalBroadcastManager(String body, String type) {
        Log.d(TAG, "Update notification to activity using Local Broadcast Manager");
        Intent i = new Intent(Constants.FILTER_NOTIFICATION_RECEIVED);
        i.putExtra("type", type);
        i.putExtra("message", body.trim());
      /*  if (action == ACTION_TABLE_INVITE || action == ACTION_ACCEPTED_INVITE || action == ACTION_DINING_STARTED) {
            i.putExtra("restaurant", restaurantDetails);
            i.putExtra("isDineStarted", restaurantDetails.getTableBooking().isdining());
        }*/
        LocalBroadcastManager.getInstance(WelcomeActivity.this).sendBroadcast(i);
    }

    private void sendRequestSaveSocialProfileAPI() {
        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_devicetype), "android");
        fields = fields + getString(R.string.api_param_key_devicetype);

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_gender);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_gender);
            }
            params.put(getString(R.string.api_param_key_gender), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_GENDER, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_dob);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_dob);
            }
            params.put(getString(R.string.api_param_key_dob), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DOB, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_occupation);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_occupation);
            }
            params.put(getString(R.string.api_param_key_occupation), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_OCCUPATION, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_education);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_education);
            }
            params.put(getString(R.string.api_param_key_education), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_EDUCATION, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_relationship);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_relationship);
            }
            params.put(getString(R.string.api_param_key_relationship), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RELATIONSHIP, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_about_me);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_about_me);
            }
            params.put(getString(R.string.api_param_key_about_me), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ABOUT_ME, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_ethnicity);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_ethnicity);
            }
            params.put(getString(R.string.api_param_key_ethnicity), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ETHNICITY, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_access_token);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_access_token);
            }
            params.put(getString(R.string.api_param_key_access_token), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCESS_TOKEN, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_social_id);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_social_id);
            }
            params.put(getString(R.string.api_param_key_social_id), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SOCIAL_ID, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic1);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic1);
            }
            params.put(getString(R.string.api_param_key_profilepic1), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC1, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic2);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic2);
            }
            params.put(getString(R.string.api_param_key_profilepic2), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC2, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic3);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic3);
            }
            params.put(getString(R.string.api_param_key_profilepic3), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC3, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic4);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic4);
            }
            params.put(getString(R.string.api_param_key_profilepic4), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC4, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic5);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic5);
            }
            params.put(getString(R.string.api_param_key_profilepic5), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC5, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic6);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic6);
            }
            params.put(getString(R.string.api_param_key_profilepic6), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC6, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic7);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic7);
            }
            params.put(getString(R.string.api_param_key_profilepic7), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC7, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic8);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic8);
            }
            params.put(getString(R.string.api_param_key_profilepic8), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC8, "").trim());
        }

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_profilepic9);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_profilepic9);
            }
            params.put(getString(R.string.api_param_key_profilepic9), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_PROFILEPIC9, "").trim());
        }

        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "");
        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_account_type);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_account_type);
            }
            params.put(getString(R.string.api_param_key_account_type), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, "").trim());
        }

        if (!SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim().isEmpty()) {
            if (fields.trim().isEmpty()) {
                fields = fields + getString(R.string.api_param_key_devicetoken);
            } else {
                fields = fields + "," + getString(R.string.api_param_key_devicetoken);
            }
            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").trim());
        }
        params.put(getString(R.string.api_param_key_fields), fields.trim());
        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, WelcomeActivity.this);
    }

    /**
     * Get list of all facebook albums on user registration by facebook
     *
     * @param after
     */
    private void getFacebookAlbumsForProfilePicture(String after) {
        GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/me/albums", new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse response) {
                Log.d(TAG, "Facebook albums response: " + response.toString().trim());
                // Insert your code here
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                                /*stopProgress();*/
                            JSONObject jsonObjResponse = response.getJSONObject();

                            JSONArray data = !jsonObjResponse.isNull("data") ? jsonObjResponse.getJSONArray("data") : new JSONArray();
                            JSONObject paging = !jsonObjResponse.isNull("paging") ? jsonObjResponse.getJSONObject("paging") : null;

                            String albumId = "";
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject objData = data.getJSONObject(i);
                                String name = !objData.isNull("name") ? objData.getString("name").trim() : "";
                                if (name.trim().equalsIgnoreCase("Profile Pictures")) {
                                    albumId = !objData.isNull("id") ? objData.getString("id").trim() : "";
                                }
                            }

                            if (!albumId.trim().isEmpty()) {
                                callPhotosAPI(albumId.trim(), "");
                            } else {
                                if (paging != null) {
                                    String next = !paging.isNull("next") ? paging.getString("next").trim() : "";
                                    if (!next.trim().isEmpty()) {
                                        Uri uri = Uri.parse(next.trim());
                                        String after = uri.getQueryParameter("after").trim();
                                        if (after != null && !after.trim().isEmpty()) {
                                            getFacebookAlbumsForProfilePicture(after.trim());
                                        } else {
                                            stopProgress();
                                            // TODO Save Profile of user, as user is registered for the first time.
                                            sendRequestSaveSocialProfileAPI();
                                        }
                                    } else {
                                        stopProgress();
                                        // TODO Save Profile of user, as user is registered for the first time.
                                        sendRequestSaveSocialProfileAPI();
                                    }
                                } else {
                                    stopProgress();
                                    // TODO Save Profile of user, as user is registered for the first time.
                                    sendRequestSaveSocialProfileAPI();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            stopProgress();
                            // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        } catch (Exception e) {
                            e.printStackTrace();
                            stopProgress();
                            // TODO Save Profile of user, as user is registered for the first time.
                            sendRequestSaveSocialProfileAPI();
                        }
                    }
                });
            }
        });

        Bundle parameters = new Bundle();
        if (!after.trim().isEmpty())
            parameters.putString("after", after.trim());
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Get all photos of Facebook album, by album Id
     *
     * @param albumId Id of album to fetch photos for.
     * @param after   after
     */
    private void callPhotosAPI(String albumId, String after) {
/*//        *//*showProgress(getString(R.string.loading));*//**//**/
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "/" + albumId.trim() + "/photos", new GraphRequest.Callback() {
                @Override
                public void onCompleted(final GraphResponse response) {
                    // Insert your code here
                    Log.d(TAG, "Response photos by album id: " + response.toString().trim());
                    // Insert your code here
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                /*stopProgress();*/
                                JSONObject jsonObjResponse = response.getJSONObject();

                                JSONArray data = !jsonObjResponse.isNull("data") ? jsonObjResponse.getJSONArray("data") : new JSONArray();
                                JSONObject paging = !jsonObjResponse.isNull("paging") ? jsonObjResponse.getJSONObject("paging") : null;
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject objData = data.getJSONObject(i);
                                    JSONArray images = !objData.isNull("images") ? objData.getJSONArray("images") : new JSONArray();

                                    String source = "";
                                    for (int j = 0; j < images.length(); j++) {
                                        JSONObject objImage = images.getJSONObject(j);
//                                        image.setHeight(!objImage.isNull("height") ? objImage.getLong("height") : 0);
//                                        image.setWidth(!objImage.isNull("width") ? objImage.getLong("width") : 0);
//                                        image.setSource(!objImage.isNull("source") ? objImage.getString("source") : "");
                                        if (source.trim().isEmpty()) {
                                            source = !objImage.isNull("source") ? objImage.getString("source") : "";
                                            break;
                                        }
                                    }

                                    if (source.trim().isEmpty()) {
                                        source = !objData.isNull("picture") ? objData.getString("picture").trim() : "";
                                    }

                                    if (!source.trim().isEmpty()) {
                                        if (i == 1) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 2) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 3) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 4) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 5) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC6, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 6) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC7, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 7) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC8, source.trim());
                                            SharedPreferenceUtil.save();
                                        } else if (i == 8) {
                                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC9, source.trim());
                                            SharedPreferenceUtil.save();
                                        }
//                                     else if (i == 5) {
//                                            objProfile.setImage5(source.trim());
//                                        } else if (i == 6) {
//                                            objProfile.setImage6(source.trim());
//                                        }
                                    }
//                                    photo.setId(!objData.isNull("id") ? objData.getString("id").trim() : "");
//                                    photo.setPicture(!objData.isNull("picture") ? objData.getString("picture").trim() : "");
                                }

                                // TODO Save Profile of user, as user is registered for the first time.
                                sendRequestSaveSocialProfileAPI();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "images,picture");
            if (!after.trim().isEmpty())
                parameters.putString("after", after.trim());
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
