package com.friendsoverfood.android.UserSuggestions;

import android.content.Context;
import android.view.View;

import com.friendsoverfood.android.R;
import com.friendsoverfood.android.SignIn.TastebudChipVO;
import com.plumillonforge.android.chipview.ChipViewAdapter;

/**
 * Created by Trushit on 26/02/17.
 */

public class OtherUserChipViewAdapter extends ChipViewAdapter {
    public OtherUserChipViewAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutRes(int position) {
        TastebudChipVO tag = (TastebudChipVO) getChip(position);

        switch (tag.getType()) {
            default:
            case 2:
            case 4:
                return 0;

            case 1:
            case 5:
                return R.layout.chip_close_selected;

            case 3:
                return R.layout.chip_close;
        }
    }

    @Override
    public int getBackgroundColor(int position) {
        TastebudChipVO tag = (TastebudChipVO) getChip(position);

        switch (tag.getType()) {
            default:
                return 0;

            case 0:
                return getColor(R.color.red_bg_chip);

            case 1:
                return getColor(R.color.white_bg_chip);
        }
    }

    @Override
    public int getBackgroundColorSelected(int position) {
        return 0;
    }

    @Override
    public int getBackgroundRes(int position) {
        return 0;
    }

    @Override
    public void onLayout(View view, int position) {
        TastebudChipVO tag = (TastebudChipVO) getChip(position);
    }
}