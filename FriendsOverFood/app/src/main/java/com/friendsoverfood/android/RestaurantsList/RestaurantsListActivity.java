package com.friendsoverfood.android.RestaurantsList;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.friendsoverfood.android.APIParsing.UserProfileAPI;
import com.friendsoverfood.android.BaseActivity;
import com.friendsoverfood.android.Constants;
import com.friendsoverfood.android.Home.HomeActivity;
import com.friendsoverfood.android.Http.HttpRequestSingleton;
import com.friendsoverfood.android.Http.HttpRequestSingletonPost;
import com.friendsoverfood.android.R;
import com.friendsoverfood.android.storage.SharedPreferenceUtil;
import com.friendsoverfood.android.storage.SharedPreferencesTokens;
import com.friendsoverfood.android.util.EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager;
import com.friendsoverfood.android.util.NetworkUtil;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Trushit on 27/02/17.
 */

public class RestaurantsListActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, ResultCallback<LocationSettingsResult> {

    private Context context;
    private LayoutInflater inflater;
    private View view;
    private final String TAG = "RESTAURANTS_LIST_ACTIVITY";


    // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
//    private RestaurantListAdapter adapter;
//    private ListView listViewRestaurants;
    public RecyclerView rvRestaurants;
    public GridLayoutManager llmRecyclerView;
    private RestaurantListAdapter adapter;
    private int posRestaurantListMatrixAPI = -1;
    private boolean isMatrixAPICalled = false;
    private ArrayList<RestaurantListVO> listRestaurant = new ArrayList<>();
    //    public DatePicker datePicker;
    public NumberPicker numberPickerDay, numberPickerTime, numberPickerPeople;
    public ArrayList<DaysPickerVO> listDaysPicker = new ArrayList<>();
    public ArrayList<TimePickerVO> listTimePicker = new ArrayList<>();
    public ArrayList<PeoplePickerVO> listPeoplePicker = new ArrayList<>();
    public LinearLayout linearViewDatePicker;
    public Calendar calDateSelected = Calendar.getInstance();
    public PeoplePickerVO peopleSelected = new PeoplePickerVO();
    public Button btnDoneDatePicker;
    public boolean isPickerSelected = false;
    private int Count = 0;
    public int posRestaurantList = -1;
    private int posToScrollRestaurants;

    // Filter & Map options
    public LinearLayout linearDatePicker, linearPeoplePicker; // linearMap, linearFilter,
    public TextView txtDatePicker;
    public ImageView imgDownArrowDatePicker, imgDownArrowPeoplePicker;
    public ImageButton imgBackground;
    public View viewCustomActionBar;
    public boolean isToExpandDatePicker = false;
    public boolean isCurrentLocationEnabled = true;

    // Floating action menu variables
    public FloatingActionMenu menuFabListFilter;
    public FloatingActionButton fabNearBy;
    public FloatingActionButton fabFilter;
    public ImageView imgGradientFab;

    // Search View Variables for Tastebuds & Location
//    public SearchView searchViewTastebuds; //, searchViewLocation;
//    public SearchManager searchManagerTasteBuds; //, searchManagerLocation;
//    public TextView txtChangeLocation;
    public final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    public LatLng selectedLocationLatLong = null;
    private String placeName = "", strSearchLocationQuery = "";
    private boolean isEndOfResult = false;
    private String searchKeyWord = "";
    public String strPageTokenRestaurantList = "";
    public EditText autoCompleteTextViewChangeLocation, txtSearchTastebuds;
    public LinearLayout linearSearchTastebuds, linearSearchLocation;
    public ImageView imgSearchTastebudsCancel, imgSearchLocationCancel, imgSearchCurrentLocaiton;
    public ListView listViewGoogleSearchLocation;
    public SearchLocationListAdapter adapterSearchLocation;
    public boolean isRestaurantListCalled = false;

    /**
     * Current results returned by google place autocomplete.
     */
    private ArrayList<AutocompletePrediction> mResultList = new ArrayList<>();
    public TextView txtSearchCommon;
    public LinearLayout linearSearchCustom;
    public RelativeLayout relativeSearchPopup;
    public LinearLayout linearSearchCancel;
    public TextView txtSearch, txtCancel;
    private String strSearchRestaurantQueryPrevious = "";
    /**
     * The bounds used for Places Geo Data autocomplete API requests.
     */
    private LatLngBounds mBounds;
    /**
     * The autocomplete filter used to restrict queries to a specific set of place types.
     */
    private AutocompleteFilter mPlaceFilter;

    // Timer for search
    public CountDownTimer timerSearchTastebuds, timerSearchLocation;
    public String strQuerySearchTastebuds = "", strQuerySearchLocation = "";

    // Variables for Google API Client & Location services.
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;
    public boolean isLocationReceivedOnce = false;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    public boolean isTimeDistanceRequested = false;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    public LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Constant used in the location settings dialog.
     */
    public final int REQUEST_CHECK_SETTINGS = 0x1;

    public double currentLatitude = 0.0, currentLongitude = 0.0, locationChangeLatitude = 0.0, locationChangeLongitude = 0.0,
            currentLatitudeDevice = 0.0, currentLongitudeDevice = 0.0;
    public String price1 = "18";
    public String price2 = "100";
    public String distance1 = "0";
    public String distance2 = "100";
    public float preferRatings = 0;
    public double distanceSelectedInKm = Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
            .trim().replaceAll(",", "."));
    public int mMaxValue = 50, mMinValue = 0;

    public CountDownTimer timerSearchSuggestions;
    public boolean isTimerCalled = false;

    // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
//    public AbsListView.OnScrollListener scrollListenerRestaurants;
    public EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager scrollListenerRestaurants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 1);
        init();

        setVisibilityActionBar(true);
//        setTitleSupportActionBar(getString(R.string.title_restaurants_list));
        setTitleSupportActionBar("");
       /* setDrawerMenu();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);*/

        setVisibilityFooterTabsBase(true);
        setSelectorFooterTabsBase(3); // Set search icon selected

        // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
        checkLocationPermissionGrantedOrNot();

        /*setDrawerMenu();
        lockDrawerMenu();*/

    }

    private void init() {
        context = this;
        inflater = LayoutInflater.from(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        view = inflater.inflate(R.layout.layout_restaurant_list_activity, getMiddleContent());

        // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
//        listViewRestaurants = (ListView) view.findViewById(R.id.listViewRestaurantsListActivity);
        rvRestaurants = (RecyclerView) view.findViewById(R.id.recyclerViewRestaurantListSendRequestActivity);
        /*linearFilter = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListActivityFilter);
        linearMap = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListActivityMap);*/
        imgGradientFab = (ImageView) view.findViewById(R.id.imageViewListActivityGradient);
        menuFabListFilter = (FloatingActionMenu) view.findViewById(R.id.floatingActionMenuListFilter);
        fabNearBy = (FloatingActionButton) view.findViewById(R.id.fabNearby);
        fabFilter = (FloatingActionButton) view.findViewById(R.id.fabFilter);
        linearViewDatePicker = (LinearLayout) view.findViewById(R.id.linearLayoutRestaurantListActivityDatePicker);
        imgBackground = (ImageButton) view.findViewById(R.id.imageViewRestaurantListActivityBackground);
        relativeSearchPopup = (RelativeLayout) view.findViewById(R.id.relativeLayoutHomeActivitySearchPopup);
        listViewGoogleSearchLocation = (ListView) view.findViewById(R.id.listViewGoogleSearchLocation);

        // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
        llmRecyclerView = new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
        rvRestaurants.setLayoutManager(llmRecyclerView);
        rvRestaurants.setHasFixedSize(false);

        // TODO Set custom layout to support action bar here.
//        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_list_activity, null);
        viewCustomActionBar = inflater.inflate(R.layout.layout_custom_toolbar_restaurant_list_activity, linearToolBarCustomLayout);
        linearToolBarCustomLayout.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
//        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setCustomView(viewCustomActionBar);
//        Toolbar parent =(Toolbar) viewCustomActionBar.getParent();
        toolbarLayoutRoot.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        toolbarLayoutRoot.setContentInsetsAbsolute(0, 0);
        toolbarLayoutRoot.setContentInsetsRelative(0, 0);
        toolbarLayoutRoot.setContentInsetStartWithNavigation(0);

        linearDatePicker = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivityDatePicker);
        linearPeoplePicker = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivityPeoplePicker);
        txtDatePicker = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityDatePicker);
        imgDownArrowDatePicker = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityDatePickerDownArrow);
        imgDownArrowPeoplePicker = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityPeopleDownArrow);

//        datePicker = (DatePicker) view.findViewById(R.id.datePickerRestaurantListActivity);
        numberPickerDay = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantListActivityDay);
        numberPickerTime = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantListActivityTime);
        numberPickerPeople = (NumberPicker) view.findViewById(R.id.numberPickerRestaurantListActivityPeople);
        btnDoneDatePicker = (Button) view.findViewById(R.id.buttonRestaurantListActivityDatePickerDone);

        createCustomAnimationFab();

        if (getIntent().hasExtra("querySearchRestaurants") && getIntent().getStringExtra("querySearchRestaurants") != null
                && !getIntent().getStringExtra("querySearchRestaurants").trim().isEmpty()) {
            searchKeyWord = getIntent().getStringExtra("querySearchRestaurants").trim();
        }

        setUpSearchViewTastebuds();
        setUpSearchViewLocation();

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            isCurrentLocationEnabled = false;
            String strLocationStringChanged = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, "");
            if (!strLocationStringChanged.trim().isEmpty() && !autoCompleteTextViewChangeLocation.getText().toString().trim().equalsIgnoreCase(strLocationStringChanged)) {
                currentLatitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim()
                        .replaceAll(",", "."));
                currentLongitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim()
                        .replaceAll(",", "."));
                autoCompleteTextViewChangeLocation.setText(strLocationStringChanged.trim());
                strQuerySearchLocation = strLocationStringChanged.trim();
                autoCompleteTextViewChangeLocation.setSelection(strLocationStringChanged.trim().length());
            }
        }

        setUpRestaurantList(false);
        setListener();

        setAllTypefaceMontserratRegular(view);
        setAllTypefaceMontserratLight(numberPickerDay);
        setAllTypefaceMontserratLight(numberPickerTime);
        setAllTypefaceMontserratLight(numberPickerPeople);
    }

    public void setAdapterRestaurantListGoogle() {
        if (adapter == null) {
            // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
            /*adapter = new RestaurantListAdapter(context, listRestaurant);
            listViewRestaurants.setAdapter(adapter);*/
            adapter = new RestaurantListAdapter(context, listRestaurant);
            rvRestaurants.setAdapter(adapter);

            setScrollListenerFriendsList();
        } else {
            /*adapter.notifyDataSetChanged();*/
            adapter.notifyItemChanged(posRestaurantListMatrixAPI);
            adapter.notifyDataSetChanged();
        }
    }

    public void GoogleRestaurentListApi() {
        if (NetworkUtil.isOnline(context)) {
            if (!isEndOfResult) {
                if (!isRestaurantListCalled) {

                    double radiusInMeters = Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                            .trim().replaceAll(",", ".").trim()) * 1000;
                    isRestaurantListCalled = true;
                    showProgress(getString(R.string.loading));
                    params = new HashMap<>();
                    params.put(getString(R.string.google_api_param_key_), getString(R.string.google_api_param_key_value));
                    params.put(getString(R.string.google_api_param_key_location), "" + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")
                            + "," + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
                    params.put(getString(R.string.google_api_param_key_radius), String.valueOf(radiusInMeters));
//            params.put(getString(R.string.google_api_param_key_rankby), "distance");
//            params.put(getString(R.string.google_api_param_key_keyword), "restaurant");
                    params.put(getString(R.string.google_api_param_key_type), "restaurant");
                    if (!strPageTokenRestaurantList.trim().isEmpty()) {
                        params.put(getString(R.string.google_api_param_key_pagetoken), strPageTokenRestaurantList.trim());
                    }

                    if (!searchKeyWord.isEmpty()) {
                        params.put(getString(R.string.google_api_param_key_keyword), searchKeyWord.trim());
                    }

                    new HttpRequestSingleton(context, getString(R.string.google_api_list_url), params,
                            Constants.ACTION_CODE_GOOGLE_LIST_API, RestaurantsListActivity.this);
                } else {
                    stopProgress();
                }
            }
        } else {
            stopProgress();
            showSnackBarMessageOnly(getString(R.string.internet_not_available));
        }
    }

    private void setUpRestaurantList(boolean isListToReset) {
//        if (isListToReset) {
//            listRestaurant.clear();
//        }
//
//
//        try {
//            InputStream inputStream = getResources().openRawResource(R.raw.radar_search);
//            List<RestaurantListVO> items = new MyRestaurantReader().read(inputStream);
//            for (int i = 0; i < 10; i++) {
//                double offset = i / 60d;
//                for (RestaurantListVO item : items) {
//                    listRestaurant.add(item);
//
//                }
//            }
//        } catch (JSONException e) {
//            /*Toast.makeText(this, "Problem reading list of markers.", Toast.LENGTH_LONG).show();*/
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (adapter == null) {
//            adapter = new RestaurantListAdapter(context, listRestaurant);
//            listViewRestaurants.setAdapter(adapter);
//        } else {
//            adapter.notifyDataSetChanged();
//        }
    }

    private void resetAndCallRestaurantListAPI() {
        Count = 0;
        posToScrollRestaurants = 0;
        posRestaurantList = -1;
        strPageTokenRestaurantList = "";
        isEndOfResult = false;
        posRestaurantListMatrixAPI = -1;
        isMatrixAPICalled = false;

        int posTemp = listRestaurant.size();

        listRestaurant.clear();

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

//        ZomatoSearchApi(Count);
        GoogleRestaurentListApi();
    }

    private void setListener() {
        /*linearFilter.setOnClickListener(this);
        linearMap.setOnClickListener(this);*/
        fabFilter.setOnClickListener(this);
        fabNearBy.setOnClickListener(this);
        linearDatePicker.setOnClickListener(this);
        linearPeoplePicker.setOnClickListener(this);
        btnDoneDatePicker.setOnClickListener(this);
        imgBackground.setOnClickListener(this);
        imgGradientFab.setOnClickListener(this);

        /*searchViewTastebuds.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!query.trim().isEmpty()) {

                    if (!query.trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                        searchKeyWord = query.trim();
                    }

                    Log.d(TAG, "Tastebuds Search timer finished. Text: " + query.trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();

                    // TODO Add all the matching query items to search list from original list.

                    // Begin search here.
                            *//*if (listViewSearch != null) {
                                listViewSearch.setVisibility(View.GONE);
                            }

                            if (txtNoResults != null) {
                                txtNoResults.setVisibility(View.GONE);
                            }

                            if (progressBarLoading != null) {
                                progressBarLoading.setVisibility(View.VISIBLE);
                            }*//*
                } else {
                    searchKeyWord = "";
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
//                Log.i("SEARCH_QUERY_STRING", "" + newText);

                return true;
            }
        });*/


        // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
        /*if (scrollListenerRestaurants == null) {
            scrollListenerRestaurants = new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                    Log.d("ON_LOAD_MORE", "firstVisibleItem: " + firstVisibleItem + ", visibleItemCount: " + visibleItemCount
//                            + ", totalItemCount: " + totalItemCount);
                    if (totalItemCount - visibleItemCount == firstVisibleItem && !isEndOfResult) {
                    *//*GoogleRestaurentListApi();*//*
                        if (!isTimerCalled) {
                            showProgress(getString(R.string.loading));
                            timerSearchSuggestions = new CountDownTimer(2500, 2500) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    Log.d("ON_LOAD_MORE", "On Finish called: ");
                                    isTimerCalled = false;
                                    GoogleRestaurentListApi();
                                }
                            };
                            timerSearchSuggestions.start();
                            isTimerCalled = true;
                        }
                    }
                }
            };
            listViewRestaurants.setOnScrollListener(scrollListenerRestaurants);

        }*/
        // Set scroll listener for restaurant list
//        if (scrollListenerRestaurants == null) {
//            scrollListenerRestaurants = new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerView) {
//                @Override
//                public void onLoadMore(int lastItemScrolled) {
//                    Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);
//                /*GoogleRestaurentListApi();*/
//                    if (!isTimerCalled) {
//                        timerSearchSuggestions = new CountDownTimer(2500, 2500) {
//                            @Override
//                            public void onTick(long millisUntilFinished) {
//
//                            }
//
//                            @Override
//                            public void onFinish() {
//                                Log.d("ON_LOAD_MORE", "On Finish called: ");
//                                isTimerCalled = false;
//                                GoogleRestaurentListApi();
//                            }
//                        };
//                        timerSearchSuggestions.start();
//                        isTimerCalled = true;
//                    }
//                }
//
//                @Override
//                public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
//                    if (firstVisibleItem != -1) {
//                    /*Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount);
//                    if (rvRestaurants.getAdapter().getItemCount() - visibleItemCount == firstVisibleItem && !isEndOfResult) {
//                        Log.d(TAG, "Calling paging for restaurants list.");
//                        GoogleRestaurentListApi();
//                    }*/
//                    }
//                }
//
//                @Override
//                public void onLoadMoreTopItems() {
//                }
//            };
//            rvRestaurants.addOnScrollListener(scrollListenerRestaurants);
//        }

//        searchViewLocation.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(final String newText) {
////                Log.i("SEARCH_QUERY_STRING", "" + newText);
//                if (timerSearchLocation != null) {
//                    timerSearchLocation.cancel();
//                }
//
//                timerSearchLocation = new CountDownTimer(1000, 500) {
//                    @Override
//                    public void onTick(long millisUntilFinished) {
//
//                    }
//
//                    @Override
//                    public void onFinish() {
//                        if (!newText.trim().isEmpty()) {
//                            if (!newText.trim().equalsIgnoreCase(strQuerySearchLocation.trim())) {
//                                strQuerySearchLocation = newText.trim();
//                            }
//
//                            Log.d(TAG, "Location Search timer finished. Text: " + newText.trim() + ", SearchQuery: " + strQuerySearchLocation.trim());
//
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                            // TODO Add all the matching query items to search list from original list.
//
//                            // Begin search here.
//                            /*if (listViewSearch != null) {
//                                listViewSearch.setVisibility(View.GONE);
//                            }
//
//                            if (txtNoResults != null) {
//                                txtNoResults.setVisibility(View.GONE);
//                            }
//
//                            if (progressBarLoading != null) {
//                                progressBarLoading.setVisibility(View.VISIBLE);
//                            }*/
//                        } else {
//                            strQuerySearchLocation = "";
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                        }
//                    }
//                };
//                timerSearchLocation.start();
//                return true;
//            }
//        });
    }

    public void setScrollListenerFriendsList() {
        // Scroll listener to call paging at the end of scroll
        if (scrollListenerRestaurants == null) {
            scrollListenerRestaurants = new EndlessRecyclerTwoWayOnScrollListenerLinearLayoutManager(llmRecyclerView) {
                @Override
                public void onLoadMore(int lastItemScrolled) {
                    Log.d("ON_LOAD_MORE", "LAST_ITEM_SCROLLED: " + lastItemScrolled);
                /*GoogleRestaurentListApi();*/
                    if (!isTimerCalled) {
                        timerSearchSuggestions = new CountDownTimer(2000, 2000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                Log.d("ON_LOAD_MORE", "On Finish called: ");
                                isTimerCalled = false;
                                GoogleRestaurentListApi();
                            }
                        };
                        timerSearchSuggestions.start();
                        isTimerCalled = true;
                    }
                }

                @Override
                public void onRecyclerViewScrolled(int firstVisibleItem, int visibleItemCount) {
                    if (firstVisibleItem != -1) {
                    /*Log.d("ON_SCROLL", "First visible item: " + firstVisibleItem + ", Visible items count: " + visibleItemCount);
                    if (rvRestaurants.getAdapter().getItemCount() - visibleItemCount == firstVisibleItem && !isEndOfResult) {
                        Log.d(TAG, "Calling paging for restaurants list.");
                        GoogleRestaurentListApi();
                    }*/
                    }
                }

                @Override
                public void onLoadMoreTopItems() {
                }
            };
            rvRestaurants.addOnScrollListener(scrollListenerRestaurants);
        }
    }

    private void createCustomAnimationFab() {
        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(menuFabListFilter.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (menuFabListFilter.isOpened()) {
                    imgGradientFab.setVisibility(View.VISIBLE);
                } else {
                    imgGradientFab.setVisibility(View.GONE);
                }

                menuFabListFilter.getMenuIconView().setImageResource(menuFabListFilter.isOpened()
                        ? R.drawable.ic_close_white : R.drawable.ic_logo_welcome_white_small);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        menuFabListFilter.setIconToggleAnimatorSet(set);
    }

    private void setUpSearchViewTastebuds() {
//        searchViewTastebuds = (SearchView) viewCustomActionBar.findViewById(R.id.searchViewCustomToolbarRestaurantListActivityTasteBuds);
//        searchViewTastebuds.setVisibility(View.VISIBLE);
//        searchViewTastebuds.requestFocusFromTouch();
////        searchViewTastebuds.setActivated(true);
//        searchViewTastebuds.onActionViewExpanded();
//        searchViewTastebuds.setIconifiedByDefault(false);
////        searchViewSuggestions.setFocusable(false);
//        searchViewTastebuds.setIconified(false);
//        searchViewTastebuds.clearFocus();
//        searchViewTastebuds.requestFocusFromTouch();
//        searchViewTastebuds.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search) + "</font>"));
//
//        searchManagerTasteBuds = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
////        searchManagerLocation = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//
//        // TODO Change text color of search view
//        EditText searchEditText = (EditText) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setHint(getString(R.string.tastebuds_search_hint));
//        searchEditText.setTextSize(14);
//        searchEditText.setPadding(0, 0, 0, 0);
//        searchEditText.setTextColor(getResources().getColor(R.color.white_selector));
//        searchEditText.setHintTextColor(getResources().getColor(R.color.white_selector));
//
//        // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
//        try {
//            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
//            f.setAccessible(true);
//            f.set(searchEditText, R.drawable.shape_edittext_cursor_green);
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // TODO Change search magnification icons here.
//        ImageView magImage = (ImageView) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//        magImage.setImageResource(R.drawable.ic_search_edittext_white);
//
//        // TODO Customize search view close button here
//        ImageView closeImage = (ImageView) searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
////        closeImage.setBackgroundResource(R.drawable.desired_icon);
//        closeImage.setColorFilter(getResources().getColor(R.color.white_selector));
//
//        // TODO Set bottom line to search view for all OS versions.
////        searchViewTastebuds.findViewById(android.support.v7.appcompat.R.id.search_src_text).setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
////        searchViewTastebuds.setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
////        searchViewTastebuds.getBackground().setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
//        searchViewTastebuds.setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
//        searchViewTastebuds.getBackground().setColorFilter(getResources().getColor(R.color.white_selector), PorterDuff.Mode.SRC_ATOP);
//
//        searchViewTastebuds.setSearchableInfo(searchManagerTasteBuds.getSearchableInfo(getComponentName()));
//        setAllTypefaceMontserratLight(searchEditText);


        txtSearchTastebuds = (EditText) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebuds);
        linearSearchTastebuds = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivityTastebuds);
        imgSearchTastebudsCancel = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityTastebudsCancel);

        txtCancel = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebudsCancel);
        txtSearch = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityTastebudsSearch);


        if (getIntent().hasExtra("querySearchRestaurants") && getIntent().getStringExtra("querySearchRestaurants") != null
                && !getIntent().getStringExtra("querySearchRestaurants").trim().isEmpty()) {
            Log.d(TAG, "Restaurant query search tastebuds from intent: " + getIntent().getStringExtra("querySearchRestaurants"));
            searchKeyWord = getIntent().getStringExtra("querySearchRestaurants").trim();
            txtSearchTastebuds.setText(searchKeyWord.trim());
            txtSearchTastebuds.setSelection(searchKeyWord.trim().length());
        } else {
            /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {*/
            if (txtSearchTastebuds != null) {
                Log.d(TAG, "Restaurant query search tastebuds from shared preferences: " +
                        SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
                searchKeyWord = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim();
                txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
                txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
            }
            /*}*/
        }

        txtSearch.setEnabled(false);
        txtSearchTastebuds.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty()) {
                    imgSearchTastebudsCancel.setVisibility(View.VISIBLE);
                    txtSearch.setEnabled(true);
                    /*imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                    if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                        searchKeyWord = s.toString().trim();
                    }

                    Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();*/
                } else {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                    SharedPreferenceUtil.save();

                    if (!strSearchRestaurantQueryPrevious.trim().equalsIgnoreCase(s.toString().trim())) {
                        txtSearch.setEnabled(true);
                    } else {
                        txtSearch.setEnabled(false);
                    }

                    imgSearchTastebudsCancel.setVisibility(View.GONE);
                    searchKeyWord = "";
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                strSearchRestaurantQueryPrevious = s.toString().trim();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtSearchTastebuds.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getAction() == KeyEvent.ACTION_DOWN))
                        || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    Log.d(TAG, "Keyboard done button pressed.");
                    /*editTextLastName.requestFocus();*/

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    final String s = txtSearchTastebuds.getText().toString().trim();

                    txtSearch.setEnabled(false);

                    relativeSearchPopup.setVisibility(View.GONE);

                    if (menuFabListFilter.isOpened()) {
                        menuFabListFilter.close(true);
                    }

                    if (!s.toString().trim().isEmpty()) {
                        imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                        if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                            searchKeyWord = s.toString().trim();
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, searchKeyWord.trim());
                            SharedPreferenceUtil.save();
                        }

                        Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());


                        resetAndCallRestaurantListAPI();
                    } else {
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                        SharedPreferenceUtil.save();

                        imgSearchTastebudsCancel.setVisibility(View.GONE);
                        searchKeyWord = "";

                        resetAndCallRestaurantListAPI();
                    }
                }
                return false;
            }
        });

        imgSearchTastebudsCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearchTastebuds.setText("");
            }
        });
    }

    private void setUpSearchViewLocation() {
        /*txtChangeLocation = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarUserMapsActivityChangeLocation);

        txtChangeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(RestaurantsListActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });*/
//        searchViewLocation = (SearchView) viewCustomActionBar.findViewById(R.id.searchViewCustomToolbarRestaurantListActivityLocation);
//        searchViewLocation.setVisibility(View.VISIBLE);
//        searchViewLocation.requestFocusFromTouch();
////        searchViewLocation.setActivated(true);
//        searchViewLocation.onActionViewExpanded();
//        searchViewLocation.setIconifiedByDefault(false);
////        searchViewSuggestions.setFocusable(false);
//        searchViewLocation.setIconified(false);
//        searchViewLocation.clearFocus();
//        searchViewLocation.setQueryHint(Html.fromHtml("<font color = #ffffff>" + getResources().getString(R.string.search) + "</font>"));
//
//        searchManagerLocation = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//
//        // TODO Change text color of search view
//        EditText searchEditTextLocation = (EditText) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditTextLocation.setHint(getString(R.string.current_location));
////        searchEditTextLocation.setText(getString(R.string.current_location));
//        searchEditTextLocation.setTextSize(14);
//        searchEditTextLocation.setPadding(0, 0, 0, 0);
//        searchEditTextLocation.setTextColor(getResources().getColor(R.color.white_selector));
//        searchEditTextLocation.setHintTextColor(getResources().getColor(R.color.white_selector));
//
//        // https://github.com/android/platform_frameworks_base/blob/kitkat-release/core/java/android/widget/TextView.java#L562-564
//        try {
//            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
//            f.setAccessible(true);
//            f.set(searchEditTextLocation, R.drawable.shape_edittext_cursor_green);
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // TODO Change search magnification icons here.
//        ImageView magImage = (ImageView) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
//        magImage.setImageResource(R.drawable.ic_location_white_small);
//
//        // TODO Customize search view close button here
//        ImageView closeImage = (ImageView) searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
////        closeImage.setBackgroundResource(R.drawable.desired_icon);
//        closeImage.setColorFilter(getResources().getColor(R.color.white_selector));
//
//        // TODO Set bottom line to search view for all OS versions.
////        searchViewLocation.findViewById(android.support.v7.appcompat.R.id.search_src_text).setBackgroundResource(R.drawable.abc_textfield_search_default_mtrl_alpha);
//
//        searchViewLocation.setSearchableInfo(searchManagerLocation.getSearchableInfo(getComponentName()));
//        setAllTypefaceMontserratLight(searchEditTextLocation);

        linearSearchCancel = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivitySearchCancel);
        linearSearchLocation = (LinearLayout) viewCustomActionBar.findViewById(R.id.linearLayoutCustomToolbarRestaurantListActivityLocation);
        imgSearchLocationCancel = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityLocationCancel);
        imgSearchCurrentLocaiton = (ImageView) viewCustomActionBar.findViewById(R.id.imageViewCustomToolbarRestaurantListActivityCurrentLocation);

//        txtChangeLocation = (TextView) viewCustomActionBar.findViewById(R.id.textViewCustomToolbarRestaurantListActivityChangeLocation);
        autoCompleteTextViewChangeLocation = (EditText) viewCustomActionBar.findViewById(R.id.autoCompleteTextViewCustomToolbarRestaurantListActivityChangeLocation);

       /* autoCompleteTextViewChangeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeSearchPopup.setVisibility(View.VISIBLE);
            }
        });*/

        autoCompleteTextViewChangeLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (relativeSearchPopup.getVisibility() != View.VISIBLE) {
                        relativeSearchPopup.setVisibility(View.VISIBLE);
                    }
                } else {
                    relativeSearchPopup.setVisibility(View.GONE);

                    if (menuFabListFilter.isOpened()) {
                        menuFabListFilter.close(true);
                    }
                }
            }
        });

        relativeSearchPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompleteTextViewChangeLocation.clearFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });

        autoCompleteTextViewChangeLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "TEXT CHANGED: " + s.toString().trim() + ", Query: " + strSearchLocationQuery);
                if (!s.toString().trim().isEmpty() && !s.toString().trim().equalsIgnoreCase(strSearchLocationQuery)) {
                    imgSearchLocationCancel.setVisibility(View.VISIBLE);
                    strSearchLocationQuery = s.toString().trim();
                    new GetPlacesResultsInBackground().execute(strSearchLocationQuery);
                } else if (s.toString().trim().isEmpty()) {
                    imgSearchLocationCancel.setVisibility(View.GONE);
                    if (mResultList != null) {
                        mResultList.clear();
                        if (adapterSearchLocation != null) {
                            adapterSearchLocation.notifyDataSetChanged();
                        } else {
                            adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                            listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        autoCompleteTextViewChangeLocation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getAction() == KeyEvent.ACTION_DOWN))
                        || (actionId == EditorInfo.IME_ACTION_DONE) || (actionId == EditorInfo.IME_ACTION_NEXT)) {
                    Log.d(TAG, "Keyboard done button pressed.");
                    /*editTextLastName.requestFocus();*/

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    String s = autoCompleteTextViewChangeLocation.getText().toString().trim();
                    if (!s.toString().trim().isEmpty() && !s.toString().trim().equalsIgnoreCase(strSearchLocationQuery)) {
                        imgSearchLocationCancel.setVisibility(View.VISIBLE);
                        strSearchLocationQuery = s.toString().trim();
                        new GetPlacesResultsInBackground().execute(strSearchLocationQuery);
                    } else if (s.toString().trim().isEmpty()) {
                        imgSearchLocationCancel.setVisibility(View.GONE);
                        if (mResultList != null) {
                            mResultList.clear();
                            if (adapterSearchLocation != null) {
                                adapterSearchLocation.notifyDataSetChanged();
                            } else {
                                adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                                listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                            }
                        }
                    }
                }
                return false;
            }
        });

        imgSearchLocationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompleteTextViewChangeLocation.setText("");
                autoCompleteTextViewChangeLocation.setSelection(autoCompleteTextViewChangeLocation.getText().toString().trim().length());
            }
        });

        imgSearchCurrentLocaiton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCurrentLocationEnabled = true;
                startLocationUpdates();

                currentLatitude = locationChangeLatitude;
                currentLongitude = locationChangeLongitude;

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                SharedPreferenceUtil.save();

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.save();

                getFullAddressForCurrentLocation(currentLatitude, currentLongitude);
            }
        });

        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String s = txtSearchTastebuds.getText().toString().trim();

                txtSearch.setEnabled(false);

                relativeSearchPopup.setVisibility(View.GONE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }

                if (!s.toString().trim().isEmpty()) {
                    imgSearchTastebudsCancel.setVisibility(View.VISIBLE);

                    if (!s.toString().trim().equalsIgnoreCase(strQuerySearchTastebuds.trim())) {
                        searchKeyWord = s.toString().trim();
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, searchKeyWord.trim());
                        SharedPreferenceUtil.save();
                    }

                    Log.d(TAG, "Tastebuds Search timer finished. Text: " + s.toString().trim() + ", SearchQuery: " + strQuerySearchTastebuds.trim());

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();
                } else {
                    SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "");
                    SharedPreferenceUtil.save();

                    imgSearchTastebudsCancel.setVisibility(View.GONE);
                    searchKeyWord = "";
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    resetAndCallRestaurantListAPI();
                }

            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });
    }

    public void setPickerForDay() {
        int posSelectedDay = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO Prepare days list here. Days list will be only for a week.
        listDaysPicker.clear();
        numberPickerDay.setDisplayedValues(null);
        for (int i = 0; i < 7; i++) {
            Calendar calDay = Calendar.getInstance();
            calDay.add(Calendar.DAY_OF_YEAR, i);
            String strDay = new SimpleDateFormat("EEEE").format(calDay.getTime());
            Log.d(TAG, "Day to add: " + strDay + ", Date: " + calDay.getTime());
            DaysPickerVO obj = new DaysPickerVO();
            obj.setCalendarDay(calDay);
            if (i == 0) {
                obj.setStrDay(getString(R.string.today));
            } else if (i == 1) {
                obj.setStrDay(getString(R.string.tomorrow));
            } else {
                obj.setStrDay(strDay.trim());
            }
            listDaysPicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calDay.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calDay.get(Calendar.DAY_OF_YEAR)) {
                    posSelectedDay = i;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listDaysPicker.size()];
        for (int i = 0; i < listDaysPicker.size(); i++) {
            strDays[i] = listDaysPicker.get(i).getStrDay().trim();
        }

        numberPickerDay.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerDay.setMaxValue(strDays.length - 1);
            numberPickerDay.setWrapSelectorWheel(false);
            numberPickerDay.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerDay);

            // TODO Set time picker here as 1st position is selected by default for picker.
            calDateSelected.setTime(listDaysPicker.get(0).getCalendarDay().getTime());
            setPickerForTime();
            updateDateTitleSelectedFromPicker();

            numberPickerDay.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    // TODO Set time picker here depending on date selected.
                    Log.d(TAG, "Day selected at pos " + newVal + " is: " + listDaysPicker.get(newVal).getCalendarDay().getTime());
                    calDateSelected.setTime(listDaysPicker.get(newVal).getCalendarDay().getTime());
                    updateDateTitleSelectedFromPicker();
                    setPickerForTime();
                }
            });
            numberPickerDay.setValue(posSelectedDay);
        }
       /* datePicker.setMinDate(calMinDate.getTimeInMillis());
        datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker view, int year, int month, int day) {
                calDateSelected.set(Calendar.YEAR, year);
                calDateSelected.set(Calendar.MONTH, month);
                calDateSelected.set(Calendar.DAY_OF_MONTH, day);

                // TODO Set Selected date in action bar
                int selectedDayOfYearCalendar = calDateSelected.get(Calendar.DAY_OF_YEAR);
                int tomorrow = calToday.get(Calendar.DAY_OF_YEAR) + 1;
                if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                        && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) { // Selected date is of today
                    txtDatePicker.setText(getString(R.string.today));
                } else if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                        && tomorrow == selectedDayOfYearCalendar) {
                    txtDatePicker.setText(getString(R.string.tomorrow));
                } else {
                    txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>th</sup> "
                            + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
                }
            }
        });*/
    }

    public void setPickerForTime() {
        Calendar calToday = Calendar.getInstance();
        int posSelectedTime = 0;
        boolean isFromIntent = false;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_DATETIME) && getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME) != null) {
                calDateSelected.setTime(((Calendar) getIntent().getExtras().getSerializable(Constants.KEY_INTENT_EXTRA_DATETIME)).getTime());
                isFromIntent = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Calendar calStartTime = Calendar.getInstance();
        calStartTime.setTime(calDateSelected.getTime());
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR) && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) {
            int currentMinute = calStartTime.get(Calendar.MINUTE);
            int diffToAdd = 30 - (currentMinute % 30);

            calStartTime.add(Calendar.MINUTE, diffToAdd);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        } else {
            calStartTime.set(Calendar.HOUR_OF_DAY, 0);
            calStartTime.set(Calendar.MINUTE, 0);
            calStartTime.set(Calendar.SECOND, 0);
            calStartTime.set(Calendar.MILLISECOND, 0);
        }

        Calendar calEndTime = Calendar.getInstance();
        calEndTime.setTime(calDateSelected.getTime());
        calEndTime.set(Calendar.HOUR_OF_DAY, 23);
        calEndTime.set(Calendar.MINUTE, 59);
        calEndTime.set(Calendar.SECOND, 59);
        calEndTime.set(Calendar.MILLISECOND, 999);

        Log.d(TAG, "Start time: " + calStartTime.getTime() + ", End time: " + calEndTime.getTime());

        // Clear list.
        listTimePicker.clear();
        numberPickerTime.setDisplayedValues(null);
        while (calStartTime.getTimeInMillis() <= calEndTime.getTimeInMillis()) {
            String strTime = new SimpleDateFormat("hh:mm aa").format(calStartTime.getTime());
            Log.d(TAG, "Time to add: " + strTime + ", Date: " + calStartTime.getTime());
            TimePickerVO obj = new TimePickerVO();
            obj.setCalendarTime(calStartTime);
            obj.setStrTime(strTime.trim());
            listTimePicker.add(obj);

            try {
                if (isFromIntent && calDateSelected.get(Calendar.YEAR) == calStartTime.get(Calendar.YEAR)
                        && calDateSelected.get(Calendar.DAY_OF_YEAR) == calStartTime.get(Calendar.DAY_OF_YEAR)
                        && calDateSelected.get(Calendar.HOUR_OF_DAY) == calStartTime.get(Calendar.HOUR_OF_DAY)
                        && calDateSelected.get(Calendar.MINUTE) == calStartTime.get(Calendar.MINUTE)) {
                    posSelectedTime = listTimePicker.size() - 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                calStartTime.add(Calendar.MINUTE, 30);
            }
        }

        String[] strDays = new String[listTimePicker.size()];
        for (int i = 0; i < listTimePicker.size(); i++) {
            strDays[i] = listTimePicker.get(i).getStrTime().trim();
        }

        numberPickerTime.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerTime.setMaxValue(strDays.length - 1);
            numberPickerTime.setWrapSelectorWheel(false);
            numberPickerTime.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerTime);
            numberPickerTime.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    calDateSelected.setTime(listTimePicker.get(newVal).getCalendarTime().getTime());
                }
            });
            numberPickerTime.setValue(posSelectedTime);
        }
    }

    public void setPickerForPeople() {
        int posSelectedPeople = 0;
        int peopleFromIntent = 0;
        try {
            if (getIntent().hasExtra(Constants.KEY_INTENT_EXTRA_PEOPLE)) {
                peopleFromIntent = getIntent().getIntExtra(Constants.KEY_INTENT_EXTRA_PEOPLE, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Clear list.
        listPeoplePicker.clear();
        numberPickerPeople.setDisplayedValues(null);
        for (int i = 0; i < 9; i++) {
            PeoplePickerVO obj = new PeoplePickerVO();
            obj.setPeopleDisplay(String.valueOf(i + 1) + " " + getString(R.string.people));
            obj.setPeopleNumber((i + 1));
            listPeoplePicker.add(obj);

            try {
                if (peopleFromIntent == (i + 1)) {
                    posSelectedPeople = (i + 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] strDays = new String[listPeoplePicker.size()];
        for (int i = 0; i < listPeoplePicker.size(); i++) {
            strDays[i] = listPeoplePicker.get(i).getPeopleDisplay().trim();
        }

        numberPickerPeople.setMinValue(0);
        if (strDays.length > 0) {
            numberPickerPeople.setMaxValue(strDays.length - 1);
            numberPickerPeople.setMaxValue(strDays.length - 1);
            numberPickerPeople.setWrapSelectorWheel(false);
            numberPickerPeople.setDisplayedValues(strDays);
            setAllTypefaceMontserratLight(numberPickerPeople);
            numberPickerPeople.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    peopleSelected = listPeoplePicker.get(newVal);
                }
            });
            numberPickerPeople.setValue(posSelectedPeople);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        registerReceiver(gpsReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        setDividerColorNumberPicker(numberPickerDay);
        setDividerColorNumberPicker(numberPickerTime);
        setDividerColorNumberPicker(numberPickerPeople);
        setPickerForPeople();
        setPickerForDay();

        if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().equalsIgnoreCase("0.0")
                && !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().equalsIgnoreCase("0.0")) {
            isCurrentLocationEnabled = false;
            String strLocationStringChanged = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, "");
            if (!strLocationStringChanged.trim().isEmpty() && !autoCompleteTextViewChangeLocation.getText().toString().trim().equalsIgnoreCase(strLocationStringChanged)) {
                currentLatitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0").trim().replaceAll(",", "."));
                currentLongitude = Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0").trim().replaceAll(",", "."));
                autoCompleteTextViewChangeLocation.setText(strLocationStringChanged.trim());
                strQuerySearchLocation = strLocationStringChanged.trim();
                autoCompleteTextViewChangeLocation.setSelection(strLocationStringChanged.trim().length());
            }

        }

        /*if (!SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().isEmpty()) {*/
        if (txtSearchTastebuds != null) {
            txtSearchTastebuds.setText(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim());
            txtSearchTastebuds.setSelection(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_RESTAURANT_SEARCH_QUERY, "").trim().length());
        }
        /*}*/
    }

    public void updateDateTitleSelectedFromPicker() {
        // TODO Set Selected date in action bar
        Calendar calToday = Calendar.getInstance();
        int selectedDayOfYearCalendar = calDateSelected.get(Calendar.DAY_OF_YEAR);
        int tomorrow = calToday.get(Calendar.DAY_OF_YEAR) + 1;
        if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && calToday.get(Calendar.DAY_OF_YEAR) == calDateSelected.get(Calendar.DAY_OF_YEAR)) { // Selected date is of today
            txtDatePicker.setText(getString(R.string.today));
        } else if (calToday.get(Calendar.YEAR) == calDateSelected.get(Calendar.YEAR)
                && tomorrow == selectedDayOfYearCalendar) {
            txtDatePicker.setText(getString(R.string.tomorrow));
        } else {
            String dayOfMonth = String.valueOf(calDateSelected.get(Calendar.DAY_OF_MONTH));
            int day = calDateSelected.get(Calendar.DAY_OF_MONTH);
            String charAtLastPosition = dayOfMonth.substring(dayOfMonth.length() - 1);
            if (Integer.parseInt(charAtLastPosition) == 1 && day != 11) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>st</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 2 && day != 12) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>nd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else if (Integer.parseInt(charAtLastPosition) == 3 && day != 13) {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>rd</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            } else {
                txtDatePicker.setText(Html.fromHtml(new SimpleDateFormat("EEE, dd").format(calDateSelected.getTime()) + "<sup>th</sup> "
                        + new SimpleDateFormat("MMM").format(calDateSelected.getTime())));
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            /*case R.id.linearLayoutRestaurantListActivityMap:
//                intent = new Intent(context, RestaurantsMapsActivity.class);
                intent = new Intent(context, HomeActivity.class);
                if (isPickerSelected) {
                    intent.putExtra(Constants.KEY_INTENT_EXTRA_DATETIME, calDateSelected);
                    intent.putExtra(Constants.KEY_INTENT_EXTRA_PEOPLE, listPeoplePicker.get(numberPickerPeople.getValue()).getPeopleNumber());
                }
                startActivity(intent);
                break;

            case R.id.linearLayoutRestaurantListActivityFilter:
                showFilterDialog();

                break;*/

            case R.id.fabNearby:
                intent = new Intent(context, HomeActivity.class);
                if (isPickerSelected) {
                    intent.putExtra(Constants.KEY_INTENT_EXTRA_DATETIME, calDateSelected);
                    intent.putExtra(Constants.KEY_INTENT_EXTRA_PEOPLE, listPeoplePicker.get(numberPickerPeople.getValue()).getPeopleNumber());
                }
                startActivity(intent);
                break;

            case R.id.fabFilter:
                showFilterDialog();
                break;

            case R.id.imageViewListActivityGradient:
                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                } else {
                    menuFabListFilter.open(true);
                }
                break;

            case R.id.linearLayoutCustomToolbarRestaurantListActivityDatePicker:
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.VISIBLE);*/
                    imgBackground.setVisibility(View.GONE);
                    collapseView(linearViewDatePicker);
                } else {
                    // TODO Open datepicker view
                    isToExpandDatePicker = true;
                    if (imgDownArrowDatePicker.getRotation() != -180) {
                        imgDownArrowDatePicker.animate().rotationBy(-180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != -180) {
                        imgDownArrowPeoplePicker.animate().rotationBy(-180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.GONE);*/
                    imgBackground.setVisibility(View.VISIBLE);
                    expandView(linearViewDatePicker);
                }
                break;

            case R.id.buttonRestaurantListActivityDatePickerDone:
                isPickerSelected = true;
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    /*linearViewDatePicker.setVisibility(View.VISIBLE);*/
                    imgBackground.setVisibility(View.GONE);
                    collapseView(linearViewDatePicker);
                }
                break;

            case R.id.linearLayoutCustomToolbarRestaurantListActivityPeoplePicker:
                if (isToExpandDatePicker) {
                    // TODO Close datepicker view
                    isToExpandDatePicker = false;
                    if (imgDownArrowDatePicker.getRotation() != 0) {
                        imgDownArrowDatePicker.animate().rotationBy(180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != 0) {
                        imgDownArrowPeoplePicker.animate().rotationBy(180f).start();
                    }
                    imgBackground.setVisibility(View.GONE);
                    collapseView(linearViewDatePicker);
                } else {
                    // TODO Open datepicker view
                    isToExpandDatePicker = true;
                    if (imgDownArrowDatePicker.getRotation() != -180) {
                        imgDownArrowDatePicker.animate().rotationBy(-180f).start();
                    }
                    if (imgDownArrowPeoplePicker.getRotation() != -180) {
                        imgDownArrowPeoplePicker.animate().rotationBy(-180f).start();
                    }
                    imgBackground.setVisibility(View.VISIBLE);
                    expandView(linearViewDatePicker);
                }
                break;

            case R.id.imageViewRestaurantListActivityBackground:

                break;

            default:
                break;

        }
    }

    private void showFilterDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LinearLayout.LayoutParams dialogParams = new LinearLayout.LayoutParams(
                Constants.convertDpToPixels(300), LinearLayout.LayoutParams.WRAP_CONTENT);//set height(300) and width(match_parent) here, ie (width,height)

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dislogView = inflater.inflate(R.layout.layout_dialog_resto_filter, null);

        Button btnSearch = (Button) dislogView.findViewById(R.id.btnSearch);
        final TextView textViewGender = (TextView) dislogView.findViewById(R.id.textViewGender);
        final TextView textViewAgeRange = (TextView) dislogView.findViewById(R.id.textViewAgeRange);
        final TextView textViewDistance = (TextView) dislogView.findViewById(R.id.textViewDistance);
        final RangeBar rangebarAge = (RangeBar) dislogView.findViewById(R.id.rangebarAge);
        final SeekBar seekBarSearchDistance = (SeekBar) dislogView.findViewById(R.id.seekBarSearchDistance);
        final RangeBar rangebarDistance = (RangeBar) dislogView.findViewById(R.id.rangebarDistance);
        final RatingBar ratingBar = (RatingBar) dislogView.findViewById(R.id.ratingBar);
        final ImageView imageViewClose = (ImageView) dislogView.findViewById(R.id.imageViewClose);


        if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("mi")) {
            seekBarSearchDistance.setMax(30);

            double kmToMiles = 1 * 0.621371;
            int milesDistance = (int) (Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                    .trim().replaceAll(",", ".")) * kmToMiles);

            if (milesDistance > 30) {
                milesDistance = 30;
            } else if (milesDistance < 1) {
                milesDistance = 1;
            }

            seekBarSearchDistance.setProgress(milesDistance);
            textViewDistance.setText(String.valueOf(milesDistance) + " " + getResources().getString(R.string.prefer_distance_mi));
        } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                .trim().toLowerCase().contains("km")) {
            seekBarSearchDistance.setMax(50);

            double kmDistance = Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                    .trim().replaceAll(",", "."));
            if (kmDistance > 50) {
                kmDistance = 50;
            } else if (kmDistance < 1) {
                kmDistance = 1;
            }

            seekBarSearchDistance.setProgress((int) kmDistance);
            textViewDistance.setText(String.valueOf((int) kmDistance) + " " + getResources().getString(R.string.prefer_distance_km));
        } else {
            seekBarSearchDistance.setMax(30);

            double kmToMiles = 1 * 0.621371;
            int milesDistance = (int) (Double.parseDouble(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, "10")
                    .trim().replaceAll(",", ".")) * kmToMiles);

            if (milesDistance > 30) {
                milesDistance = 30;
            } else if (milesDistance < 1) {
                milesDistance = 1;
            }

            seekBarSearchDistance.setProgress(milesDistance);
            textViewDistance.setText(String.valueOf(milesDistance) + " " + getResources().getString(R.string.prefer_distance_mi));
        }


        ratingBar.setRating(Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_PREFER_RATING, "0")));
        seekBarSearchDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) { // If the changes are from the User only then accept the
                    // changes
                    /* Instead of -> if (progress != 0) { */
                    int distanceSelected = (int) ((float) progress / 50 * (mMaxValue - mMinValue) + mMinValue);
                    distanceSelected *= 10 / 2;
                    distanceSelected = Math.round(distanceSelected);
                    distanceSelected /= 10 / 2;
                    if (distanceSelected == 0) {
                        distanceSelected++;
                    }

                    if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                            .trim().toLowerCase().contains("mi")) {
                        Log.d(TAG, "Restauratn filter distance selected: " + distanceSelected + ", Unit: "
                                + getResources().getString(R.string.prefer_distance_mi));
//                        textViewDistance.setText(distanceSelected + " Km");
                        textViewDistance.setText(String.valueOf(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));

                        double milesToKm = 1 * 1.60934;
                        distanceSelectedInKm = (distanceSelected * milesToKm);
                    } else if (SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_DISTANCE_UNIT, getString(R.string.distance_unit_mi_value_server))
                            .trim().toLowerCase().contains("km")) {
                        Log.d(TAG, "Restauratn filter distance selected: " + distanceSelected + ", Unit: "
                                + getResources().getString(R.string.prefer_distance_km));
                        textViewDistance.setText(String.valueOf(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_km));

                        distanceSelectedInKm = distanceSelected;
                    } else {
                        Log.d(TAG, "Restauratn filter distance selected: " + distanceSelected + ", Unit: "
                                + getResources().getString(R.string.prefer_distance_mi));
//                        textViewDistance.setText(distanceSelected + " Km");
                        textViewDistance.setText(String.valueOf(distanceSelected) + " " + getResources().getString(R.string.prefer_distance_mi));

                        double milesToKm = 1 * 1.60934;
                        distanceSelectedInKm = (distanceSelected * milesToKm);
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        rangebarAge.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
                price1 = leftPinValue;
                price2 = rightPinValue;
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE1, aage1.trim());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE2, age2.trim());
//                SharedPreferenceUtil.save();savetextViewAgeRange.setText(age1 + "-" + age2);
                textViewAgeRange.setText(price1 + "-" + price2 + "Rs");
            }
        });
        rangebarDistance.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                Log.i("Range Left: " + leftPinValue, ", Right: " + rightPinValue);
                distance1 = leftPinValue;
                distance2 = rightPinValue;
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE1, aage1.trim());
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PREFERENCES_AGE2, age2.trim());
//                SharedPreferenceUtil.save();savetextViewAgeRange.setText(age1 + "-" + age2);
                textViewDistance.setText(distance1 + "-" + distance2 + "KM");
            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                preferRatings = rating;
                textViewGender.setText(rating + " Star");
            }
        });

        btnSearch.setSelected(true);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceUtil.putValue(Constants.USER_RESTO_FILTER_MIN_DISTANCE, distance1);
                SharedPreferenceUtil.putValue(Constants.USER_RESTO_FILTER_MAX_DISTANCE_KM, String.valueOf(distanceSelectedInKm)
                        .trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.USER_RESTO_FILTER_PREFER_RATING, String.valueOf(preferRatings));
                SharedPreferenceUtil.save();
                resetAndCallRestaurantListAPI();
                dialog.dismiss();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });
        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                relativeSearchPopup.setVisibility(View.GONE);

                if (menuFabListFilter.isOpened()) {
                    menuFabListFilter.close(true);
                }
            }
        });

        dialog.setContentView(dislogView, dialogParams);
        dialog.show();

//                Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
//                // if decline button is clicked, close the custom dialog
//                declineButton.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Close dialog
//                        dialog.dismiss();
//                    }
//                });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "User agreed to make required location settings changes.");
                startLocationUpdates();
            } else {
                Log.d(TAG, "User chose not to make required location settings changes.");
                stopProgress();
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(context, data);
                setPlaceDetails(place);
            } else {
                // The user canceled the operation.
                Place placeCancelled = null;
                setPlaceDetails(placeCancelled);
            }
        }
    }

    // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
   /* public void GetTimeMeasureBetweenCoOrdinates(int posItemAt) {
        String destination = "";
        if (posItemAt != -1 && posItemAt < listRestaurant.size() - 1) {
            destination = listRestaurant.get(posItemAt).getLatitude() + "," + listRestaurant.get(posItemAt).getLongitude();
        }

        String origin = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");
        Log.d(TAG, "Origin: " + origin + ", Destination: " + destination + ", POS: " + posItemAt);

        if (!destination.trim().isEmpty() && !destination.trim().equalsIgnoreCase(",") && !origin.trim().equalsIgnoreCase(",") && !isTimeDistanceRequested) {
            if (NetworkUtil.isOnline(context)) {
                if (!isTimeDistanceRequested)
                    isTimeDistanceRequested = true;
                params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
                params.put(getString(R.string.api_param_key_origins), origin.trim());
                params.put(getString(R.string.api_param_key_destinations), destination.trim());
                params.put(getString(R.string.api_param_key_units), getString(R.string.api_param_value_units));
                params.put(getString(R.string.api_param_key_key), getString(R.string.google_api_key));

                new HttpRequestSingleton(context, getString(R.string.google_distance_matrix_api_url), params,
                        Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API, RestaurantsListActivity.this);
            } else {
                stopProgress();
                showSnackBarMessageOnly(getString(R.string.internet_not_available));

            }
        } else {
            listRestaurant.get(posItemAt).setTimeToReach(getString(R.string.not_available));
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }

            if (!origin.trim().equalsIgnoreCase(",")) {
                int posNew = -1;

                for (int i = 0; i < listRestaurant.size(); i++) {
                    if (listRestaurant.get(i).getTimeToReach().trim().isEmpty() && !String.valueOf(listRestaurant.get(i).getLatitude()).isEmpty()
                            && !String.valueOf(listRestaurant.get(i).getLongitude()).isEmpty()) {
                        posNew = i;
                        break;
                    }
                }

                if (isTimeDistanceRequested) {
                    isTimeDistanceRequested = false;
                }

                if (posNew != posItemAt && posNew != -1) {
                    posItemAt = posNew;
                    GetTimeMeasureBetweenCoOrdinates(posItemAt);
                }
            }
        }
    }*/

    // TODO Change design of eatery screen same as FoodQuest screen - Trushit - 9-Oct-2017
    public void GetTimeMeasureBetweenCoOrdinates() {
        if (posRestaurantListMatrixAPI == -1) {
            posRestaurantListMatrixAPI = 0;
        }

        if (posRestaurantListMatrixAPI < listRestaurant.size()) {
            if (listRestaurant.get(posRestaurantListMatrixAPI).getTimeToReach().trim().isEmpty()) {
                String destination = "";

                destination = listRestaurant.get(posRestaurantListMatrixAPI).getLatitude() + "," + listRestaurant.get(posRestaurantListMatrixAPI).getLongitude();

                String origin = SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                        + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0");

                if (!destination.trim().isEmpty() && !destination.trim().equalsIgnoreCase(",") && !origin.trim().equalsIgnoreCase(",")) {
                    if (NetworkUtil.isOnline(context)) {
                        HashMap<String, String> params = new HashMap<>();
//            params.put(getString(R.string.zomato_api_param_key_user_key), getString(R.string.zomato_api_key));
                        params.put(getString(R.string.api_param_key_origins), origin.trim());
                        params.put(getString(R.string.api_param_key_destinations), destination.trim());
                        params.put(getString(R.string.api_param_key_units), getString(R.string.api_param_value_units));
                        params.put(getString(R.string.google_api_param_key_departure_time),
                                String.valueOf(Constants.convertTimestampTo10DigitsOnly(new Date().getTime(), true)));
                        params.put(getString(R.string.google_api_param_key_traffic_model), getString(R.string.api_param_value_best_guess));
                        params.put(getString(R.string.api_param_key_key), getString(R.string.google_api_key));

                        new HttpRequestSingleton(context, getString(R.string.google_distance_matrix_api_url), params,
                                Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API, RestaurantsListActivity.this);
                    } else {
//                activity.stopProgress();
//                showSnackBarMessageOnly(getString(R.string.internet_not_available));
                    }
                } else {

                }
            }
        }
    }

    @Override
    public void onResponse(String response, int action) {
        super.onResponse(response, action);
        if (response != null) {

            if (action == Constants.ACTION_CODE_GOOGLE_LIST_API) {
                Log.d(TAG, "ACTION_CODE_GOOGLE_LIST_API: " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";
                    String next_page_token = !jsonObjectResponse.isNull("next_page_token") ? jsonObjectResponse.getString("next_page_token").trim() : "";
                    strPageTokenRestaurantList = next_page_token;

                    if (status.trim().equalsIgnoreCase("OK")) {
                        JSONArray Results = !jsonObjectResponse.isNull("results") ? jsonObjectResponse.getJSONArray("results") : new JSONArray();

                        for (int i = 0; i < Results.length(); i++) {
                            RestaurantListVO restovo = new RestaurantListVO();
                            JSONObject data = Results.getJSONObject(i);
                            JSONObject geomatry = !data.isNull(getString(R.string.google_response_param_key_geometry))
                                    ? data.getJSONObject(getString(R.string.google_response_param_key_geometry)) : new JSONObject();
                            JSONObject location = !geomatry.isNull(getString(R.string.google_response_param_key_location))
                                    ? geomatry.getJSONObject(getString(R.string.google_response_param_key_location)) : new JSONObject();

                            restovo.setLatitude(!location.isNull(getString(R.string.google_response_param_key_lat))
                                    ? location.getString(getString(R.string.google_response_param_key_lat)).trim() : "");
                            restovo.setLongitude(!location.isNull(getString(R.string.google_response_param_key_lng))
                                    ? location.getString(getString(R.string.google_response_param_key_lng)).trim() : "");

                            Log.d(TAG, "Location: " + location.toString() + ", Lat: " + location.getString(getString(R.string.google_response_param_key_lat))
                                    + ", Long: " + location.getString(getString(R.string.google_response_param_key_lng)));

                            restovo.setPlaceid(!data.isNull("place_id") ? data.getString("place_id").trim() : "");

                            restovo.setName(!data.isNull(getString(R.string.google_response_param_key_name))
                                    ? data.getString(getString(R.string.google_response_param_key_name)).trim() : "");
                            JSONObject opening_hours = !data.isNull(getString(R.string.google_response_opening_hours))
                                    ? data.getJSONObject(getString(R.string.google_response_opening_hours)) : new JSONObject();
                            restovo.setOpen_now(!opening_hours.isNull(getString(R.string.google_response_open_now))
                                    ? opening_hours.getString(getString(R.string.google_response_open_now)).trim() : "");
                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");

                            JSONArray photos = !data.isNull("photos") ? data.getJSONArray("photos") : new JSONArray();

                            for (int j = 0; j < photos.length(); j++) {
                                JSONObject photosObj = photos.getJSONObject(j);

                                String photo_reference = !photosObj.isNull(getString(R.string.google_response_param_key_photo_reference))
                                        ? photosObj.getString(getString(R.string.google_response_param_key_photo_reference)).trim() : "";
//                                Log.i("photo_reference", "photo_reference" + photo_reference);
                                restovo.setPhotoReference("https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + widthScreen + "&photoreference="
                                        + photo_reference + "&key=AIzaSyCwErSCCBZN2fL0y9znT3wCY7qrhBxd0JI");

                            }

                            restovo.setVicinity(!data.isNull(getString(R.string.google_response_param_key_vicinity))
                                    ? data.getString(getString(R.string.google_response_param_key_vicinity)).trim() : "");
                            restovo.setRatings(!data.isNull(getString(R.string.google_response_param_key_ratings))
                                    ? data.getString(getString(R.string.google_response_param_key_ratings)).trim() : "");
                            JSONArray types = !data.isNull("types") ? data.getJSONArray("types") : new JSONArray();

                            restovo.setReference(!data.isNull(getString(R.string.google_response_param_key_reference))
                                    ? data.getString(getString(R.string.google_response_param_key_reference)).trim() : "");

                            Location locationA = new Location("point A");

                            locationA.setLatitude(Double.valueOf(restovo.getLatitude()));
                            locationA.setLongitude(Double.valueOf(restovo.getLongitude()));

                            Location locationB = new Location("point B");

                            locationB.setLatitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")));
                            locationB.setLongitude(Double.valueOf(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")));

                            float distance = locationA.distanceTo(locationB);
                            Log.i("distnce", "" + distance);

                            boolean isRestoVoExists = false;

                            if (distance > Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_MIN_DISTANCE, "1")) * 1000) {
//                                && Float.valueOf(restovo.getRatings().isEmpty() ? "0" : restovo.getRatings()) >=
//                                        Float.valueOf(SharedPreferenceUtil.getString(Constants.USER_RESTO_FILTER_PREFER_RATING, "0"))
                                for (int j = 0; j < listRestaurant.size(); j++) {
                                    if (listRestaurant.get(j).getPlaceid().trim().equalsIgnoreCase(restovo.getPlaceid().trim())) {

                                        isRestoVoExists = true;
                                    }
                                }

                                if (!isRestoVoExists) {
                                    if (types.toString().contains("lodging") || types.toString().contains("spa")) {

                                    } else {
                                        listRestaurant.add(restovo);
                                    }
                                }
                            }

//                        int posNew = -1;
//                        for (int i = 0; i < listRestaurant.size(); i++) {
//                            if (listRestaurant.get(i).getTimeToReach().trim().isEmpty()) {
//                                posNew = i;
//                                break;
//                            }
//                        }
//                        if (posNew != posRestaurantList && posNew != -1) {
//                            posRestaurantList = posNew;
//                            GetTimeMeasureBetweenCoOrdinates(posRestaurantList);
//                        }

                        }

                        if (posRestaurantListMatrixAPI < listRestaurant.size()) {
//                        if (!isMatrixAPICalled) {
                            isMatrixAPICalled = true;
                            GetTimeMeasureBetweenCoOrdinates();
//                                }
                        } else {
                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                            /*if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            } else {
                                adapter = new ChatUsersListAdapter(context, listRestaurant);
                            }*/
                        }

                        if (Results.length() > 0 && !strPageTokenRestaurantList.trim().isEmpty()) {
                            isEndOfResult = false;
                        } else {
                            isEndOfResult = true;
                        }

                        isRestaurantListCalled = false;
                        setAdapterRestaurantListGoogle();
                        stopProgress();
                    } else {
                        stopProgress();
                        isRestaurantListCalled = false;
//                        showSnackBarMessageOnly(getString(R.string.some_error_occured));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    isRestaurantListCalled = false;
                    showSnackBarMessageOnly(e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    isRestaurantListCalled = false;
                    showSnackBarMessageOnly(e.getMessage());
                }

            } else if (action == Constants.ACTION_CODE_GOOGLE_DISTANCE_MATRIX_API) {
                stopProgress();
                Log.d(TAG, "DISTANCE MATRIX API RESPONSE: " + response);

                try {
                    // Parse API response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = !jsonObjectResponse.isNull("status") ? jsonObjectResponse.getString("status").trim() : "";

                    if (status.trim().equalsIgnoreCase("OK")) {
                        // Success. Update result for time travel in list here.
                        JSONArray rows = !jsonObjectResponse.isNull("rows") ? jsonObjectResponse.getJSONArray("rows") : new JSONArray();
                        for (int i = 0; i < rows.length(); i++) {
                            JSONObject row = rows.getJSONObject(i);
                            JSONArray elements = !row.isNull("elements") ? row.getJSONArray("elements") : new JSONArray();
                            for (int j = 0; j < elements.length(); j++) {
                                JSONObject element = elements.getJSONObject(j);
                                String statusElement = !element.isNull("status") ? element.getString("status").trim() : "";
                                if (statusElement.trim().equalsIgnoreCase("OK")) {
                                    JSONObject duration = !element.isNull("duration") ? element.getJSONObject("duration") : new JSONObject();
                                    String text = !duration.isNull("text") ? duration.getString("text").trim() : "";
                                    if (!text.trim().isEmpty()) {
                                        /*txtTimeToReach.setText(text);*/
                                        listRestaurant.get(posRestaurantListMatrixAPI).setTimeToReach(text);

                                        // Update adapter here.
                                        setAdapterRestaurantListGoogle();

                                        if (posRestaurantListMatrixAPI != -1) {
                                            posRestaurantListMatrixAPI++;

                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        } else {
                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        }
                                    } else {
                                        /*txtTimeToReach.setText("");*/
                                        if (posRestaurantListMatrixAPI != -1) {
                                            posRestaurantListMatrixAPI++;

                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        } else {
                                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                                GetTimeMeasureBetweenCoOrdinates();
                                            else {
                                                Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                                // Sort the list according to isSelected
//                                                Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                            }
                                        }
                                    }
                                } else {
                                    /*txtTimeToReach.setText("");*/
                                    if (posRestaurantListMatrixAPI != -1) {
                                        posRestaurantListMatrixAPI++;

                                        if (posRestaurantListMatrixAPI < listRestaurant.size())
                                            GetTimeMeasureBetweenCoOrdinates();
                                        else {
                                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                            // Sort the list according to isSelected
//                                            Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                        }
                                    } else {
                                        if (posRestaurantListMatrixAPI < listRestaurant.size())
                                            GetTimeMeasureBetweenCoOrdinates();
                                        else {
                                            Collections.sort(listRestaurant, new ComparatorDistanceAscending());
                                            // Sort the list according to isSelected
//                                            Collections.sort(listRestaurant, new HashMapComparatorBoolean());
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if (posRestaurantListMatrixAPI != -1) {
                            posRestaurantListMatrixAPI++;

                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                GetTimeMeasureBetweenCoOrdinates();
                            else {

                            }
                        } else {
                            if (posRestaurantListMatrixAPI < listRestaurant.size())
                                GetTimeMeasureBetweenCoOrdinates();
                            else {

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));

                } finally {

                    int posNew = -1;

                }
            } else if (action == Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN) {
                Log.d("RESPONSE_EDIT_PROFILE_SOCIAL_SIGN_IN", "" + response);

                try {
                    // Parse response here
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject settings = jsonObjectResponse.getJSONObject(getString(R.string.api_response_param_key_settings));
                    boolean status = !settings.isNull(getString(R.string.api_response_param_key_success))
                            ? settings.getBoolean(getString(R.string.api_response_param_key_success)) : false;
                    String errormessage = !settings.isNull(getString(R.string.api_response_param_key_errormessage))
                            ? settings.getString(getString(R.string.api_response_param_key_errormessage)).trim() : "";

                    if (status) {
                        JSONArray dataArray = !jsonObjectResponse.isNull(getString(R.string.api_response_param_key_data))
                                ? jsonObjectResponse.getJSONArray(getString(R.string.api_response_param_key_data)) : new JSONArray();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject data = dataArray.getJSONObject(i);

                            UserProfileAPI profile = new UserProfileAPI(context, data);

                   /* if (!profile.isregistered) {*/
                            SharedPreferenceUtil.putValue(Constants.IS_USER_LOGGED_IN, true);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_USER_ID, profile.id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SESSION_ID, profile.sessionid.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_GENDER, profile.gender.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_FIRST_NAME, profile.first_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LAST_NAME, profile.last_name.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_MOBILE, profile.mobile.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DOB, profile.dob.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_OCCUPATION, profile.occupation.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_EDUCATION, profile.education.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_RELATIONSHIP, profile.relationship.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ABOUT_ME, profile.about_me.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ETHNICITY, profile.ethnicity.trim());

                            // Split location and save current latitude & longitude of user.
                            String[] location = profile.location.trim().split(",");
                            if (location.length == 2) {
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, location[0].trim().replaceAll(",", "."));
                                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, location[1].trim().replaceAll(",", "."));
                            }

                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, profile.location_string.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCOUNT_TYPE, profile.account_type.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ACCESS_TOKEN, profile.access_token.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SOCIAL_ID, profile.social_id.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SHOWME, profile.showme.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_DISTANCE, profile.search_distance.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MIN_AGE, profile.search_min_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_SEARCH_MAX_AGE, profile.search_max_age.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_MESSAGES_NOTIFICATIONS,
                                    profile.is_receive_messages_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_RECEIVE_INVITATION_NOTIFICATIONS,
                                    profile.is_receive_invitation_notifications);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_IS_SHOW_LOCATION,
                                    profile.is_show_location);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DISTANCE_UNIT, profile.distance_unit.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC1, profile.profilepic1.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC2, profile.profilepic2.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC3, profile.profilepic3.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC4, profile.profilepic4.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_PROFILEPIC5, profile.profilepic5.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFER_CODE, profile.refercode.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_REFERENCE, profile.reference.trim());
//                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_DEVICETOKEN, profile.devicetoken.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_TASTEBUDS, profile.testbuds.trim());
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CREATEDAT, profile.createdat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_UPDATEDAT, profile.updatedat);
                            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_ISREGISTERED, profile.isregistered);
                            SharedPreferenceUtil.save();

                            stopProgress();

                            // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                            resetAndCallRestaurantListAPI();
                        }
                    } else {
                        stopProgress();
                        showSnackBarMessageOnly(errormessage.trim());
                   /* isToFetchProfileDetails = true;
                    sendRequestLoginToDb();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                } catch (Exception e) {
                    e.printStackTrace();
                    stopProgress();
                    showSnackBarMessageOnly(getString(R.string.some_error_occured));
                }
            }

        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    /**
     * Sets up the location request. Android has two location request settings: {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update interval (5 seconds), the Fused Location Provider API returns location updates
     * that are accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location updates.
     */
    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(5 * 5000); // 5 second, in milliseconds
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    protected void checkLocationSettings() {
        if (!isLocationReceivedOnce) {
            Log.d(TAG, "Checking location settings...");
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest);
            result.setResultCallback(RestaurantsListActivity.this);

        }
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private boolean isLocationPermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;

            }
        } else {
            return true;
        }
    }

    private void checkLocationPermissionGrantedOrNot() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION) ||
                        !shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    showDialogAlertRequestPermissionLocation(getString(R.string.request_permission),
                            getString(R.string.location_permission_is_required));
                    /*requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);*/
                    return;
                }

                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
            } else {
                checkLocationSettings();
            }
        } else {
            checkLocationSettings();
        }
    }

    protected void showDialogAlertRequestPermissionLocation(String strTitle, String strMessage) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
        builder.setMessage(strMessage).setTitle(strTitle).setCancelable(false).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION);
                }
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_ASK_FOR_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the  camera related task you need to do.
                   /* showSnackBar(getString(R.string.you_have_successfully_granted_required_permissions));*/
                    checkLocationSettings();
                    resetAndCallRestaurantListAPI();
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    /*supportFinishAfterTransition();*/
                    Toast.makeText(context, getString(R.string.you_have_not_granted_location_permission), Toast.LENGTH_SHORT).show();
                    setUpRestaurantList(true);
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Log.d(TAG, "Google API Client connected");
            if (isLocationPermissionGranted()) {
                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                if (location == null) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, RestaurantsListActivity.this);
                } else {
                    //If everything went fine lets get latitude and longitude
                    Log.d(TAG, "Location using last location. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude());
                    locationChangeLatitude = location.getLatitude();
                    locationChangeLongitude = location.getLongitude();

                    if (isCurrentLocationEnabled) {
                        currentLatitude = location.getLatitude();
                        currentLongitude = location.getLongitude();
                        String lat = String.valueOf(currentLatitude);
                        String lng = String.valueOf(currentLongitude);

//                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, "0.0");
//                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, "0.0");
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, lat.trim().replaceAll(",", "."));
                        SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, lng.trim().replaceAll(",", "."));
                        SharedPreferenceUtil.save();

                        getFullAddressForCurrentLocation(currentLatitude, currentLongitude);

                        stopProgress();

                        if (!isLocationReceivedOnce) {
                            isLocationReceivedOnce = true;
                        }
                        setUpRestaurantList(true);
                        GoogleRestaurentListApi();
                    } else {
                        stopProgress();

                        if (!isLocationReceivedOnce) {
                            isLocationReceivedOnce = true;
                        }
                        setUpRestaurantList(true);
                        GoogleRestaurentListApi();
                    }
                }
            } else {
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            stopProgress();
        } catch (Exception e) {
            e.printStackTrace();
            stopProgress();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Suspended");

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location updated. Lat:" + location.getLatitude() + ", Long: " + location.getLongitude());
        stopProgress();

        locationChangeLatitude = location.getLatitude();
        locationChangeLongitude = location.getLongitude();

        if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude() && isCurrentLocationEnabled) {
//        if (currentLatitude != location.getLatitude() && currentLongitude != location.getLongitude()) {
            Location loc1 = new Location("");
            loc1.setLongitude(currentLongitude);
            loc1.setLatitude(currentLatitude);

            Location loc2 = new Location("");
            loc2.setLatitude(locationChangeLatitude);
            loc2.setLongitude(locationChangeLongitude);

            float distanceInMeters = loc1.distanceTo(loc2);
            if (distanceInMeters >= 250) {
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.save();

                getFullAddressForCurrentLocation(currentLatitude, currentLongitude);


            /*setUpRestaurantList(true);
            GoogleRestaurentListApi();*/
            /*// Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails();*/

                stopProgress();
            }

            if (!isLocationReceivedOnce) {
                isLocationReceivedOnce = true;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Failed");
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(RestaurantsListActivity.this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
                stopProgress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
            stopProgress();
        }
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        Log.d(TAG, "Result received: " + locationSettingsResult.getStatus());
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.d(TAG, "All location settings are satisfied.");
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.d(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                    status.startResolutionForResult(RestaurantsListActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                    Log.d(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.d(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (gpsReceiver != null) {
            unregisterReceiver(gpsReceiver);
        }

        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                }
            });
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        if (isLocationPermissionGranted()) {
            if (mGoogleApiClient != null && mLocationRequest != null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, RestaurantsListActivity.this)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                if (!isLocationReceivedOnce) {
                                    setUpRestaurantList(false);
                                    resetAndCallRestaurantListAPI();
                                }
                            }
                        });
            }
        }
    }

    public void expandView(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? LinearLayout.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapseView(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void colorizeDatePicker(NumberPicker datePicker) {
        Resources system = Resources.getSystem();
        int dayId = system.getIdentifier("day", "id", "android");
        int monthId = system.getIdentifier("month", "id", "android");
        int yearId = system.getIdentifier("year", "id", "android");

        NumberPicker dayPicker = (NumberPicker) datePicker.findViewById(dayId);
        NumberPicker monthPicker = (NumberPicker) datePicker.findViewById(monthId);
        NumberPicker yearPicker = (NumberPicker) datePicker.findViewById(yearId);

        setDividerColorNumberPicker(dayPicker);
        setDividerColorNumberPicker(monthPicker);
        setDividerColorNumberPicker(yearPicker);
    }

    private void setDividerColorNumberPicker(NumberPicker picker) {
        if (picker == null)
            return;

        final int count = picker.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                Field dividerField = picker.getClass().getDeclaredField("mSelectionDivider");
                dividerField.setAccessible(true);
                ColorDrawable colorDrawable = new ColorDrawable(picker.getResources().getColor(R.color.colorPrimary));
                dividerField.set(picker, colorDrawable);
                picker.invalidate();
            } catch (Exception e) {
                Log.w("setDividerColor", e);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "On back pressed...");
        super.onBackPressed();
    }

    public void setPlaceDetails(final Place place) {
        if (place != null) {
            isCurrentLocationEnabled = false;
            stopLocationUpdates();

            selectedLocationLatLong = place.getLatLng();
            placeName = place.getName().toString().trim();
            Log.d("NAME_PLACE_DETAILS", "Place: " + place.getAddress().toString().trim());

            /*txtChangeLocation.setText(place.getName().toString().trim());*/
            strQuerySearchLocation = place.getAddress().toString().trim();
            autoCompleteTextViewChangeLocation.setText(place.getAddress().toString().trim());
            autoCompleteTextViewChangeLocation.setSelection(autoCompleteTextViewChangeLocation.getText().toString().trim().length());


            /*SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(selectedLocationLatLong.latitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(selectedLocationLatLong.longitude).trim().replaceAll(",", "."));
            SharedPreferenceUtil.save();*/

            // TODO Save Location Lat Lng to profile
            if (currentLatitude != selectedLocationLatLong.latitude && currentLongitude != selectedLocationLatLong.longitude && !isCurrentLocationEnabled) {
                currentLatitude = selectedLocationLatLong.latitude;
                currentLongitude = selectedLocationLatLong.longitude;
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
//                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, place.toString().trim());
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LATITUDE, String.valueOf(currentLatitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_LONGITUDE, String.valueOf(currentLongitude).trim().replaceAll(",", "."));
                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CHANGED_LOCATION_STRING, place.getAddress().toString().trim());
                SharedPreferenceUtil.save();

            /*// Save user latitude & longitude in background using edit profile API.
            sendRequestEditProfileDetails();*/

                stopProgress();

                if (!isLocationReceivedOnce) {
                    isLocationReceivedOnce = true;
                }

                autoCompleteTextViewChangeLocation.clearFocus();
                // Save user latitude & longitude in background using edit profile API.
                showProgress(getString(R.string.loading));
                sendRequestSaveProfileLocationAPI(place.getAddress().toString().trim());
                    /*setUpMapOnScreen(true);*/
            }

            /*resetAndCallRestaurantListAPI();*/

            /*if (mMap != null) {
                mMap.clear();
            }

            mMapView.getMapAsync(CreateChallengesFirstStepActivity.this);*/

//            Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getId())
//                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
//                        @Override
//                        public void onResult(PlaceBuffer places) {
//                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
//                                Place myPlace = places.get(0);
//
//                                // Setup Geocoder
//                                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                                List<Address> addresses;
//
//                                // Attempt to Geocode from place lat & long
//                                try {
//                                    addresses = geocoder.getFromLocation(myPlace.getLatLng().latitude, myPlace.getLatLng().longitude, 1);
//
//                                   /* if (addresses.size() > 0) {*/
//                                    for (int j = 0; j < addresses.size(); j++) {
//                                        Address addr = addresses.get(j);
//                                        Log.d("LOCATION_ADDRESS", "Address: " + addresses.get(0).toString().trim());
//                                        String addressLines = "";
//                                        for (int i = 0; i <= addr.getMaxAddressLineIndex(); i++) {
//                                            if (i == 0) {
//                                                addressLines = addr.getAddressLine(i);
//                                            } else {
//                                                addressLines = addressLines + ", " + addr.getAddressLine(i);
//                                            }
//                                        }
////                                        Log.d("LOCATION_ADDRESS_LINES", "Address: " + addressLines);
//
//                                        /*txtChangeLocation.setText(addressLines.trim());*/
//
//                                        if (addr.getPostalCode() != null) {
//
//                                        }
//
//                                        if (addr.getLocality() != null) {
//
//                                        }
//
//                                        if (addr.getAdminArea() != null && addr.getLocality() != null) {
//
//                                        }
//
//                                        if (addr.getCountryName() != null) {
//
//                                        }
//                                    }
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                Log.i("PLACE NOT FOUND ", "Place not found");
//                            }
//
//                            places.release();
//                        }
//                    });

        } else {

        }
    }

    protected class GetPlacesResultsInBackground extends AsyncTask<String, Void, ArrayList<AutocompletePrediction>> {

        protected ArrayList<AutocompletePrediction> doInBackground(String... urls) {
            try {
                String strSearhLocationQuery = urls[0].trim();
                ArrayList<AutocompletePrediction> mResultList = getAutocomplete(strSearchLocationQuery);
                return mResultList;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(ArrayList<AutocompletePrediction> result) {
            if (result != null) {
                /*autoCompleteTextViewChangeLocation.clearFocus();*/
                Log.d(TAG, "Notify Location search change. List size: " + TextUtils.join(", ", mResultList));
                if (mResultList != null) {
                    mResultList.clear();
                    mResultList.addAll(result);
                }

                if (mResultList != null && mResultList.size() > 0) {
                    if (adapterSearchLocation != null) {
                        adapterSearchLocation.notifyDataSetChanged();
                    } else {
                        adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                        listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                    }
                } else {
                    if (adapterSearchLocation != null) {
                        adapterSearchLocation.notifyDataSetChanged();
                    } else {
                        if (mResultList != null) {
                            adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                            listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                        }
                    }
                }
                return;
            } else {
                /*stopProgress();*/
                Log.d(TAG, "Null results received from Place Autocomplete Adapter.");
                mResultList = new ArrayList<>();
                if (adapterSearchLocation != null) {
                    adapterSearchLocation.notifyDataSetChanged();
                } else {
                    if (mResultList != null) {
                        adapterSearchLocation = new SearchLocationListAdapter(context, mResultList);
                        listViewGoogleSearchLocation.setAdapter(adapterSearchLocation);
                    }
                }
                return;
            }
        }

    }

    public void fetchPlaceDetailsSelected(AutocompletePrediction item) {
        final String placeId = item.getPlaceId();
        final CharSequence primaryText = item.getPrimaryText(null);

        Log.d(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
        placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        /*Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
                Toast.LENGTH_SHORT).show();*/
        Log.d(TAG, "Called getPlaceById to get Place details for " + placeId);
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.d(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // TODO Fetch place details and update API & View
            selectedLocationLatLong = place.getLatLng();

            // Hide soft keyboard here.
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            /*SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, String.valueOf(place.getLatLng().latitude).trim().replace(",", "."));
            SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, String.valueOf(place.getLatLng().longitude).trim().replace(",", "."));
            SharedPreferenceUtil.save();*/

            relativeSearchPopup.setVisibility(View.GONE);

            if (menuFabListFilter.isOpened()) {
                menuFabListFilter.close(true);
            }

            /*// Save user latitude & longitude in background using edit profile API.
            showProgress(getString(R.string.loading));
            sendRequestSaveProfileLocationAPI();*/

            setPlaceDetails(place);

            Log.d(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     *
     * @param constraint Autocomplete query string
     * @return Results from the autocomplete API or null if the query was not successful.
     * @see Places#GEO_DATA_API#getAutocomplete(CharSequence)
     * @see AutocompletePrediction#freeze()
     */
    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Starting autocomplete query for: " + constraint);

            setLatlngBounds(new LatLng(currentLatitude, currentLongitude));

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);

//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), BOUNDS_GREATER_SYDNEY, mPlaceFilter);
                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), mBounds, mPlaceFilter);
//                    Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, constraint.toString(), null, null);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                /*handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Error contacting API: " + status.toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/

                Log.d(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.d(TAG, "Query completed. Received " + autocompletePredictions.getCount() + " predictions.");

            /*Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(HomeActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);*/

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        Log.d(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    public void setLatlngBounds(LatLng center) {
        double radiusDegrees = 0.10;
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        mBounds = LatLngBounds.builder().include(northEast).include(southWest).build();
    }

    private void sendRequestSaveProfileLocationAPI(String location_string) {
//        showProgress(getString(R.string.loading));
        String fields = "";
        params = new HashMap<>();
        params.put(getString(R.string.api_param_key_action), getString(R.string.api_response_param_action_editprofile));
        params.put(getString(R.string.api_param_key_userid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_USER_ID, ""));
        params.put(getString(R.string.api_param_key_sessionid), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_SESSION_ID, ""));
        params.put(getString(R.string.api_param_key_location), SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0") + ","
                + SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0"));
        params.put(getString(R.string.api_param_key_devicetype), "android");
        fields = fields + getString(R.string.api_param_key_devicetype);
        String address = getPlaceAddress().trim();
        if (address.trim().isEmpty()) {
            address = location_string;
        }

        if (!address.trim().isEmpty()) {
            params.put(getString(R.string.api_param_key_location_string), address.trim());
        } else {
            params.put(getString(R.string.api_param_key_location_string), "null");
        }
        /*params.put(getString(R.string.api_param_key_location_string), location_string.trim().isEmpty() ? getPlaceAddress() : location_string.trim());*/
        if (SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, "").equalsIgnoreCase("")) {
            fields = fields + "," + getString(R.string.api_param_key_location) + "," + getString(R.string.api_param_key_location_string);
            params.put(getString(R.string.api_param_key_fields), fields);
        } else {
            fields = fields + "," + getString(R.string.api_param_key_location) + "," + getString(R.string.api_param_key_location_string) + ","
                    + getString(R.string.api_param_key_devicetoken);
            params.put(getString(R.string.api_param_key_fields), fields);
            params.put(getString(R.string.api_param_key_devicetoken), SharedPreferencesTokens.getString(Constants.LOGGED_IN_USER_DEVICETOKEN, ""));
        }

        new HttpRequestSingletonPost(context, getString(R.string.api_base_url), params,
                Constants.ACTION_CODE_API_EDIT_PROFILE_SIGN_IN, RestaurantsListActivity.this);
    }

    private String getPlaceAddress() {
        String place = "";
        double latitude = 0.0, longitude = 0.0;
        latitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LATITUDE, "0.0")) : 0.0;
        longitude = !SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0").trim().isEmpty()
                ? Double.parseDouble(SharedPreferenceUtil.getString(Constants.LOGGED_IN_USER_CURRENT_LONGITUDE, "0.0")) : 0.0;

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses.size() > 0) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            if (city != null && !city.trim().isEmpty() && !city.trim().equalsIgnoreCase("null")) {
                place = city;
            }
            if (state != null && !state.trim().isEmpty() && !state.trim().equalsIgnoreCase("null")) {
                if (place.trim().isEmpty()) {
                    place = state;
                } else {
                    place = place + ", " + state;
                }

            }
            if (country != null && !country.trim().isEmpty() && !country.trim().equalsIgnoreCase("null")) {
                if (country.trim().isEmpty()) {
                    place = country;
                } else {
                    place = place + ", " + country;
                }
            }
        }

        return place;
    }

    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                //Do your stuff on GPS status change
                if (mGoogleApiClient == null) {
                    // Kick off the process of building the GoogleApiClient, LocationRequest, and LocationSettingsRequest objects.
                    buildGoogleApiClient();
                }

                if (!mGoogleApiClient.isConnected()) {
                    createLocationRequest();
                    buildLocationSettingsRequest();
                    checkLocationPermissionGrantedOrNot();

                    if (!mGoogleApiClient.isConnected())
                        mGoogleApiClient.connect();
                } else {
                    if (isCurrentLocationEnabled) {
                        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                        setUpRestaurantList(true);
                        resetAndCallRestaurantListAPI();
                    } else {
                        // TODO Restrict user suggestion call due to duplicate calls in life cycle.
                        setUpRestaurantList(true);
                        resetAndCallRestaurantListAPI();
                    }
                }

            }
        }
    };

    private void getFullAddressForCurrentLocation(double currentLatitude, double currentLongitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            Log.d(TAG, "Address current location: " + addresses.get(0).toString().trim());

            String address = addresses.get(0).getAddressLine(0);
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() : "";
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String locality = addresses.get(0).getLocality() != null ? addresses.get(0).getLocality() : "";
            String thoroughfare = addresses.get(0).getThoroughfare() != null ? addresses.get(0).getThoroughfare() : "";
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                   /* // Save user latitude & longitude in background using edit profile API.
                    sendRequestEditProfileDetails();*/

            if (isCurrentLocationEnabled) {
                String location_string = (!thoroughfare.trim().isEmpty() ? thoroughfare : "")
                        + (!thoroughfare.trim().isEmpty() && !city.trim().isEmpty() ? ", " : "")
                        + (!city.trim().isEmpty() ? city : "");

                autoCompleteTextViewChangeLocation.setText("");
                autoCompleteTextViewChangeLocation.setHint(getString(R.string.current_location)
                        + (thoroughfare.trim().isEmpty() && city.trim().isEmpty() ? "" : " - ")
                        + location_string);

                SharedPreferenceUtil.putValue(Constants.LOGGED_IN_USER_LOCATION_STRING, location_string.trim());
                SharedPreferenceUtil.save();

                sendRequestSaveProfileLocationAPI(location_string.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ComparatorDistanceAscending implements Comparator<RestaurantListVO> {
        public int compare(RestaurantListVO first, RestaurantListVO second) {
//            Log.d("SORT_CHAT_LIST", "First: " + first.getFirstName() + " " + first.lastName + ", Timestamp: " + first.getTimestampLastSeen() + ", Second: " +
//                    second.getFirstName() + " " + second.lastName + ", Timestamp: " + second.getTimestampLastSeen());

//            long firstValue = first.getTimestampLastSeen();
            double firstTime = -1;
            double secondTime = -1;
            int returnPos = 0;
            try {
//                if (!first.isSelected() && !second.isSelected()) {
                if (!first.getTimeToReach().trim().isEmpty() && !second.getTimeToReach().trim().isEmpty()) {
                    String[] timeToReachFirst = first.getTimeToReach().trim().split(" ");
                    String[] timeToReachSecond = second.getTimeToReach().trim().split(" ");

                    if (timeToReachFirst.length > 0)
                        firstTime = Double.parseDouble(timeToReachFirst[0].trim().replaceAll(",", "."));

                    if (timeToReachSecond.length > 0)
                        secondTime = Double.parseDouble(timeToReachSecond[0].trim().replaceAll(",", "."));

                    if (firstTime != -1 && secondTime != -1) {
                        returnPos = firstTime > secondTime ? 1 : firstTime < secondTime ? -1 : 0;
                    } else {
                        returnPos = 0;
                    }
                }
//                }
            } catch (Exception e) {
                e.printStackTrace();
                returnPos = 0;
            } finally {
                return returnPos;
            }
        }
    }

}
